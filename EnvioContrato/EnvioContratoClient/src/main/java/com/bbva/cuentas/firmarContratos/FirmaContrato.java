/**
 * FirmaContrato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class FirmaContrato  implements java.io.Serializable {
    private com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems;

    private com.bbva.cuentas.firmarContratos.Cliente[] listaClientes;

    private java.lang.String numeroContrato;

    private java.lang.String oficinaGestora;

    private java.lang.String procedencia;

    private java.lang.Object idContrato;  // attribute

    public FirmaContrato() {
    }

    public FirmaContrato(
           com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems,
           com.bbva.cuentas.firmarContratos.Cliente[] listaClientes,
           java.lang.String numeroContrato,
           java.lang.String oficinaGestora,
           java.lang.String procedencia,
           java.lang.Object idContrato) {
           this.listaItems = listaItems;
           this.listaClientes = listaClientes;
           this.numeroContrato = numeroContrato;
           this.oficinaGestora = oficinaGestora;
           this.procedencia = procedencia;
           this.idContrato = idContrato;
    }


    /**
     * Gets the listaItems value for this FirmaContrato.
     * 
     * @return listaItems
     */
    public com.bbva.cuentas.firmarContratos.ItemContrato[] getListaItems() {
        return listaItems;
    }


    /**
     * Sets the listaItems value for this FirmaContrato.
     * 
     * @param listaItems
     */
    public void setListaItems(com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems) {
        this.listaItems = listaItems;
    }

    public com.bbva.cuentas.firmarContratos.ItemContrato getListaItems(int i) {
        return this.listaItems[i];
    }

    public void setListaItems(int i, com.bbva.cuentas.firmarContratos.ItemContrato _value) {
        this.listaItems[i] = _value;
    }


    /**
     * Gets the listaClientes value for this FirmaContrato.
     * 
     * @return listaClientes
     */
    public com.bbva.cuentas.firmarContratos.Cliente[] getListaClientes() {
        return listaClientes;
    }


    /**
     * Sets the listaClientes value for this FirmaContrato.
     * 
     * @param listaClientes
     */
    public void setListaClientes(com.bbva.cuentas.firmarContratos.Cliente[] listaClientes) {
        this.listaClientes = listaClientes;
    }

    public com.bbva.cuentas.firmarContratos.Cliente getListaClientes(int i) {
        return this.listaClientes[i];
    }

    public void setListaClientes(int i, com.bbva.cuentas.firmarContratos.Cliente _value) {
        this.listaClientes[i] = _value;
    }


    /**
     * Gets the numeroContrato value for this FirmaContrato.
     * 
     * @return numeroContrato
     */
    public java.lang.String getNumeroContrato() {
        return numeroContrato;
    }


    /**
     * Sets the numeroContrato value for this FirmaContrato.
     * 
     * @param numeroContrato
     */
    public void setNumeroContrato(java.lang.String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }


    /**
     * Gets the oficinaGestora value for this FirmaContrato.
     * 
     * @return oficinaGestora
     */
    public java.lang.String getOficinaGestora() {
        return oficinaGestora;
    }


    /**
     * Sets the oficinaGestora value for this FirmaContrato.
     * 
     * @param oficinaGestora
     */
    public void setOficinaGestora(java.lang.String oficinaGestora) {
        this.oficinaGestora = oficinaGestora;
    }


    /**
     * Gets the procedencia value for this FirmaContrato.
     * 
     * @return procedencia
     */
    public java.lang.String getProcedencia() {
        return procedencia;
    }


    /**
     * Sets the procedencia value for this FirmaContrato.
     * 
     * @param procedencia
     */
    public void setProcedencia(java.lang.String procedencia) {
        this.procedencia = procedencia;
    }


    /**
     * Gets the idContrato value for this FirmaContrato.
     * 
     * @return idContrato
     */
    public java.lang.Object getIdContrato() {
        return idContrato;
    }


    /**
     * Sets the idContrato value for this FirmaContrato.
     * 
     * @param idContrato
     */
    public void setIdContrato(java.lang.Object idContrato) {
        this.idContrato = idContrato;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FirmaContrato)) return false;
        FirmaContrato other = (FirmaContrato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.listaItems==null && other.getListaItems()==null) || 
             (this.listaItems!=null &&
              java.util.Arrays.equals(this.listaItems, other.getListaItems()))) &&
            ((this.listaClientes==null && other.getListaClientes()==null) || 
             (this.listaClientes!=null &&
              java.util.Arrays.equals(this.listaClientes, other.getListaClientes()))) &&
            ((this.numeroContrato==null && other.getNumeroContrato()==null) || 
             (this.numeroContrato!=null &&
              this.numeroContrato.equals(other.getNumeroContrato()))) &&
            ((this.oficinaGestora==null && other.getOficinaGestora()==null) || 
             (this.oficinaGestora!=null &&
              this.oficinaGestora.equals(other.getOficinaGestora()))) &&
            ((this.procedencia==null && other.getProcedencia()==null) || 
             (this.procedencia!=null &&
              this.procedencia.equals(other.getProcedencia()))) &&
            ((this.idContrato==null && other.getIdContrato()==null) || 
             (this.idContrato!=null &&
              this.idContrato.equals(other.getIdContrato())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getListaItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getListaClientes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaClientes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaClientes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumeroContrato() != null) {
            _hashCode += getNumeroContrato().hashCode();
        }
        if (getOficinaGestora() != null) {
            _hashCode += getOficinaGestora().hashCode();
        }
        if (getProcedencia() != null) {
            _hashCode += getProcedencia().hashCode();
        }
        if (getIdContrato() != null) {
            _hashCode += getIdContrato().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FirmaContrato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "firmaContrato"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("idContrato");
        attrField.setXmlName(new javax.xml.namespace.QName("", "idContrato"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "listaItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "itemContrato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaClientes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "listaClientes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "cliente"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroContrato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "numeroContrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oficinaGestora");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "oficinaGestora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("procedencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "procedencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
