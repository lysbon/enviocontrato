/**
 * FirmarContratosPortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class FirmarContratosPortServiceLocator extends org.apache.axis.client.Service implements com.bbva.cuentas.firmarContratos.FirmarContratosPortService {

    public FirmarContratosPortServiceLocator() {
    }


    public FirmarContratosPortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FirmarContratosPortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FirmarContratosPortSoap11
    private java.lang.String FirmarContratosPortSoap11_address = "http://118.180.34.113:9080/envioContratoWS/ws/envioContratoWS";

    public java.lang.String getFirmarContratosPortSoap11Address() {
        return FirmarContratosPortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FirmarContratosPortSoap11WSDDServiceName = "FirmarContratosPortSoap11";

    public java.lang.String getFirmarContratosPortSoap11WSDDServiceName() {
        return FirmarContratosPortSoap11WSDDServiceName;
    }

    public void setFirmarContratosPortSoap11WSDDServiceName(java.lang.String name) {
        FirmarContratosPortSoap11WSDDServiceName = name;
    }

    public com.bbva.cuentas.firmarContratos.FirmarContratosPort getFirmarContratosPortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FirmarContratosPortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFirmarContratosPortSoap11(endpoint);
    }

    public com.bbva.cuentas.firmarContratos.FirmarContratosPort getFirmarContratosPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.bbva.cuentas.firmarContratos.FirmarContratosPortSoap11Stub _stub = new com.bbva.cuentas.firmarContratos.FirmarContratosPortSoap11Stub(portAddress, this);
            _stub.setPortName(getFirmarContratosPortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFirmarContratosPortSoap11EndpointAddress(java.lang.String address) {
        FirmarContratosPortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.bbva.cuentas.firmarContratos.FirmarContratosPort.class.isAssignableFrom(serviceEndpointInterface)) {
                com.bbva.cuentas.firmarContratos.FirmarContratosPortSoap11Stub _stub = new com.bbva.cuentas.firmarContratos.FirmarContratosPortSoap11Stub(new java.net.URL(FirmarContratosPortSoap11_address), this);
                _stub.setPortName(getFirmarContratosPortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FirmarContratosPortSoap11".equals(inputPortName)) {
            return getFirmarContratosPortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "FirmarContratosPortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "FirmarContratosPortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FirmarContratosPortSoap11".equals(portName)) {
            setFirmarContratosPortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
