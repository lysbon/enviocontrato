package com.bbva.cuentas.dto;

import java.util.List;

public class DocumentoCliente {
	private String codCliente;
	private List<Documento> documentos;
	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	public List<Documento> getDocumentos() {
		return documentos;
	}
	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}
	
	
}
