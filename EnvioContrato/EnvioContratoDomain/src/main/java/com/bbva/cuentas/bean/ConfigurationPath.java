package com.bbva.cuentas.bean;

public class ConfigurationPath {
	
	private String basename;
	private String rutaSalidaPDF;
	private String rutaSalidaFirma;
	private String rutaSalidaTempFileUnicoPDF;
	private String rutaCSV;
	private String rutaSalidaTempFileUnicoCSV;
	private Integer step;
	private String stFileunico;
	private String stEnvCorreo;
	private String nbNombreCorreo;
	private String nbTipoFileunico;
	private String numeroContrato;

	public String getBasename() {
		return basename;
	}

	public void setBasename(String basename) {
		this.basename = basename;
	}

	public String getRutaSalidaPDF() {
		return rutaSalidaPDF;
	}

	public void setRutaSalidaPDF(String rutaSalidaPDF) {
		this.rutaSalidaPDF = rutaSalidaPDF;
	}

	public String getRutaSalidaFirma() {
		return rutaSalidaFirma;
	}

	public void setRutaSalidaFirma(String rutaSalidaFirma) {
		this.rutaSalidaFirma = rutaSalidaFirma;
	}

	public String getRutaCSV() {
		return rutaCSV;
	}

	public void setRutaCSV(String rutaCSV) {
		this.rutaCSV = rutaCSV;
	}
	
	
	public String getRutaSalidaTempFileUnicoPDF() {
		return rutaSalidaTempFileUnicoPDF;
	}

	public void setRutaSalidaTempFileUnicoPDF(String rutaSalidaTempFileUnicoPDF) {
		this.rutaSalidaTempFileUnicoPDF = rutaSalidaTempFileUnicoPDF;
	}
	
	public String getRutaSalidaTempFileUnicoCSV() {
		return rutaSalidaTempFileUnicoCSV;
	}

	public void setRutaSalidaTempFileUnicoCSV(String rutaSalidaTempFileUnicoCSV) {
		this.rutaSalidaTempFileUnicoCSV = rutaSalidaTempFileUnicoCSV;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public String getStFileunico() {
		return stFileunico;
	}

	public void setStFileunico(String stFileunico) {
		this.stFileunico = stFileunico;
	}

	public String getStEnvCorreo() {
		return stEnvCorreo;
	}

	public void setStEnvCorreo(String stEnvCorreo) {
		this.stEnvCorreo = stEnvCorreo;
	}

	public String getNbNombreCorreo() {
		return nbNombreCorreo;
	}

	public void setNbNombreCorreo(String nbNombreCorreo) {
		this.nbNombreCorreo = nbNombreCorreo;
	}

	public String getNbTipoFileunico() {
		return nbTipoFileunico;
	}
	
	public void setNbTipoFileunico(String nbTipoFileunico) {
		this.nbTipoFileunico = nbTipoFileunico;
	}
	
	public String getNumeroContrato() {
		return numeroContrato;
	}
	
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}	
	
}
