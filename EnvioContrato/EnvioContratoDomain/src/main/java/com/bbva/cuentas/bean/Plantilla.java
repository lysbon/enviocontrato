package com.bbva.cuentas.bean;

import java.util.List;

import java.util.List;

public class Plantilla extends Generic{
	
	private Long   cdPlantilla;
	private String cdKitPlantilla;
	private String nbNombreFormato;
	private String nbDescripcion;
	private String stCondicion;
	private String stFileunico;
	private String stEnvCorreo;
	private String stTipoConstr;
	private String stSigned;
	private String stActivo;
	private String nbTipoFileunico;
	private String nbNombreCorreo;
	private String chTipoPlantilla;
	private String lbPdfPlantilla;
	private String nbDimension;
	
	private byte[] resultPDF;
	private boolean valida;
	
	private boolean encontroFirma;
	private boolean encontroCSV;
	private boolean asociado;
	
	private String pathTemplate;
	
	private List<ItemSection> secciones;

	/*
	public String getTxtCondicion(){
		if(Constants.PLANTILLA_FIJA.equals(stCondicion)){
			return Constants.TXT_FIJO;
		}else{
			return Constants.TXT_VARIABLE;
		}
	}
	*/
	public Plantilla() {
		
	}
			
	public Plantilla(String nbNombreFormato, String nbDescripcion, String nbNombreCorreo) {
		super();
		this.nbNombreFormato = nbNombreFormato;
		this.nbDescripcion = nbDescripcion;
		this.nbNombreCorreo = nbNombreCorreo;
	}
	
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	public byte[] getResultPDF() {
		return resultPDF;
	}
	public void setResultPDF(byte[] resultPDF) {
		this.resultPDF = resultPDF;
	}
	public String getNbNombreFormato() {
		return nbNombreFormato;
	}
	public void setNbNombreFormato(String nbNombreFormato) {
		this.nbNombreFormato = nbNombreFormato;
	}
	public String getNbNombreCorreo() {
		return nbNombreCorreo;
	}
	public void setNbNombreCorreo(String nbNombreCorreo) {
		this.nbNombreCorreo = nbNombreCorreo;
	}
	public String getNbTipoFileunico() {
		return nbTipoFileunico;
	}
	public void setNbTipoFileunico(String nbTipoFileunico) {
		this.nbTipoFileunico = nbTipoFileunico;
	}
	public String getCdKitPlantilla() {
		return cdKitPlantilla;
	}
	public void setCdKitPlantilla(String cdKitPlantilla) {
		this.cdKitPlantilla = cdKitPlantilla;
	}
	
	public String getStCondicion() {
		return stCondicion;
	}
	public void setStCondicion(String stCondicion) {
		this.stCondicion = stCondicion;
	}
	public String getStFileunico() {
		return stFileunico;
	}
	public void setStFileunico(String stFileunico) {
		this.stFileunico = stFileunico;
	}
	public boolean isValida() {
		return valida;
	}
	public void setValida(boolean valida) {
		this.valida = valida;
	}
	public String getStTipoConstr() {
		return stTipoConstr;
	}
	public void setStTipoConstr(String stTipoConstr) {
		this.stTipoConstr = stTipoConstr;
	}
	public String getStEnvCorreo() {
		return stEnvCorreo;
	}
	public void setStEnvCorreo(String stEnvCorreo) {
		this.stEnvCorreo = stEnvCorreo;
	}
	public String getStSigned() {
		return stSigned;
	}
	public void setStSigned(String stSigned) {
		this.stSigned = stSigned;
	}
	
	public String getStActivo() {
		return stActivo;
	}
	public void setStActivo(String stActivo) {
		this.stActivo = stActivo;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((cdPlantilla == null) ? 0 : cdPlantilla.hashCode());
		result = prime * result
				+ ((nbNombreFormato == null) ? 0 : nbNombreFormato.hashCode());
		result = prime * result
				+ ((stActivo == null) ? 0 : stActivo.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plantilla other = (Plantilla) obj;
		if (cdPlantilla == null) {
			if (other.cdPlantilla != null)
				return false;
		} else if (!cdPlantilla.equals(other.cdPlantilla))
			return false;
		if (nbNombreFormato == null) {
			if (other.nbNombreFormato != null)
				return false;
		} else if (!nbNombreFormato.equals(other.nbNombreFormato))
			return false;
		if (stActivo == null) {
			if (other.stActivo != null)
				return false;
		} else if (!stActivo.equals(other.stActivo))
			return false;
		return true;
	}
	public boolean isEncontroFirma() {
		return encontroFirma;
	}
	public void setEncontroFirma(boolean encontroFirma) {
		this.encontroFirma = encontroFirma;
	}
	public boolean isEncontroCSV() {
		return encontroCSV;
	}
	public void setEncontroCSV(boolean encontroCSV) {
		this.encontroCSV = encontroCSV;
	}
	public boolean isAsociado() {
		return asociado;
	}
	public void setAsociado(boolean asociado) {
		this.asociado = asociado;
	}
	public String getChTipoPlantilla() {
		return chTipoPlantilla;
	}
	public void setChTipoPlantilla(String chTipoPlantilla) {
		this.chTipoPlantilla = chTipoPlantilla;
	}
	public String getPathTemplate() {
		return pathTemplate;
	}
	public void setPathTemplate(String pathTemplate) {
		this.pathTemplate = pathTemplate;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public String getNbDimension() {
		return nbDimension;
	}
	public void setNbDimension(String nbDimension) {
		this.nbDimension = nbDimension;
	}
	public String getLbPdfPlantilla() {
		return lbPdfPlantilla;
	}
	public void setLbPdfPlantilla(String lbPdfPlantilla) {
		this.lbPdfPlantilla = lbPdfPlantilla;
	}
	
	@Override
	public String toString() {
		return "Plantilla ["
				+ (cdPlantilla != null ? "cdPlantilla=" + cdPlantilla + ", "
						: "")
				+ (nbNombreFormato != null ? "nbNombreFormato="
						+ nbNombreFormato + ", " : "")
				+ (stActivo != null ? "stActivo=" + stActivo : "") + "]";
	}

	public List<ItemSection> getSecciones() {
		return secciones;
	}
	public void setSecciones(List<ItemSection> secciones) {
		this.secciones = secciones;
	}
	
}
