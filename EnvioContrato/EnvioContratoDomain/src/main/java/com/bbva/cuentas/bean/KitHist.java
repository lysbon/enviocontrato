package com.bbva.cuentas.bean;

import java.sql.Date;

import com.bbva.cuentas.bean.Enviroment;

public class KitHist extends Generic{
	
	private Long cdKit;
	private Date fhInicio;
	private Date fhFin;
	private String cdCodigo;
	private String nbCorreoAsunto;
	private String lbCorreoPlantilla;
	private String stEnvCorreo;
	private String stFileunico;
	private String flValHuella;
	private String nbCorreoEngine;
	private String nbNombreKit;
	private String stSignBox;
	private String stHuella;
	private Enviroment grupo;
	
	public Long getCdKit() {
		return cdKit;
	}
	public void setCdKit(Long cdKit) {
		this.cdKit = cdKit;
	}
	public Date getFhInicio() {
		return fhInicio;
	}
	public void setFhInicio(Date fhInicio) {
		this.fhInicio = fhInicio;
	}
	public Date getFhFin() {
		return fhFin;
	}
	public void setFhFin(Date fhFin) {
		this.fhFin = fhFin;
	}
	public String getCdCodigo() {
		return cdCodigo;
	}
	public void setCdCodigo(String cdCodigo) {
		this.cdCodigo = cdCodigo;
	}
	public String getNbCorreoAsunto() {
		return nbCorreoAsunto;
	}
	public void setNbCorreoAsunto(String nbCorreoAsunto) {
		this.nbCorreoAsunto = nbCorreoAsunto;
	}
	public String getLbCorreoPlantilla() {
		return lbCorreoPlantilla;
	}
	public void setLbCorreoPlantilla(String lbCorreoPlantilla) {
		this.lbCorreoPlantilla = lbCorreoPlantilla;
	}
	public String getStEnvCorreo() {
		return stEnvCorreo;
	}
	public void setStEnvCorreo(String stEnvCorreo) {
		this.stEnvCorreo = stEnvCorreo;
	}
	public String getStFileunico() {
		return stFileunico;
	}
	public void setStFileunico(String stFileunico) {
		this.stFileunico = stFileunico;
	}
	public String getFlValHuella() {
		return flValHuella;
	}
	public void setFlValHuella(String flValHuella) {
		this.flValHuella = flValHuella;
	}
	public String getNbCorreoEngine() {
		return nbCorreoEngine;
	}
	public void setNbCorreoEngine(String nbCorreoEngine) {
		this.nbCorreoEngine = nbCorreoEngine;
	}
	public String getNbNombreKit() {
		return nbNombreKit;
	}
	public void setNbNombreKit(String nbNombreKit) {
		this.nbNombreKit = nbNombreKit;
	}
	public String getStSignBox() {
		return stSignBox;
	}
	public void setStSignBox(String stSignBox) {
		this.stSignBox = stSignBox;
	}
	public String getStHuella() {
		return stHuella;
	}
	public void setStHuella(String stHuella) {
		this.stHuella = stHuella;
	}

	public Enviroment getGrupo() {
		return grupo;
	}

	public void setGrupo(Enviroment grupo) {
		this.grupo = grupo;
	}
}
