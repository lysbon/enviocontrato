package com.bbva.cuentas.bean;

import java.util.ArrayList;
import java.util.List;

public class ItemSection extends Generic{
	
	private Long cdSeccion;
	private Long cdPlantilla;
	private Integer nuPagina;
	private List<ItemStamp> listItems;
	
	public ItemSection(){
		listItems = new ArrayList<ItemStamp>();
	}	
	public List<ItemStamp> getListItems() {
		return listItems;
	}
	public void setListItems(List<ItemStamp> listItems) {
		this.listItems = listItems;
	}
	public Long getCdSeccion() {
		return cdSeccion;
	}
	public void setCdSeccion(Long cdSeccion) {
		this.cdSeccion = cdSeccion;
	}
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	public Integer getNuPagina() {
		return nuPagina;
	}
	public void setNuPagina(Integer nuPagina) {
		this.nuPagina = nuPagina;
	}
	
	@Override
	public String toString() {
		return "ItemSection [cdSeccion=" + cdSeccion + ", nuPagina=" + nuPagina + "]";
	}
	
}
