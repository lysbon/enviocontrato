package com.bbva.cuentas.bean;

public class FlagPlantilla extends Generic{
	
	private Long cdFlag;
	private Long cdPlantilla;
	private String stValor;
	private String stActivo;
	
	private Flag flag;
	
	public Long getCdFlag() {
		return cdFlag;
	}
	public void setCdFlag(Long cdFlag) {
		this.cdFlag = cdFlag;
	}
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	public String getStValor() {
		return stValor;
	}
	public void setStValor(String stValor) {
		this.stValor = stValor;
	}
	public String getStActivo() {
		return stActivo;
	}
	public void setStActivo(String stActivo) {
		this.stActivo = stActivo;
	}
	public Flag getFlag() {
		return flag;
	}
	public void setFlag(Flag flag) {
		this.flag = flag;
	}
	
}