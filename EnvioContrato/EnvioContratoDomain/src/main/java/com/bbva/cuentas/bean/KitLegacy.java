package com.bbva.cuentas.bean;

import java.util.List;

public class KitLegacy {
	
	private String cdKit;
	private String lbCorreoPlantilla;
	private String nbCorreoAsunto;
	private String stEnvCorreo;
	private String stFileunico;
	private String flValHuella;
	private String nbCorreoEngine;
	private String nbNombreKit;
	private String stSignBox;
	
	
	public String getStSignBox() {
		return stSignBox;
	}

	public void setStSignBox(String stSignBox) {
		this.stSignBox = stSignBox;
	}

	private List<Plantilla> listTemplates;
	
	public String getNbNombreKit() {
		return nbNombreKit;
	}

	public void setNbNombreKit(String nbNombreKit) {
		this.nbNombreKit = nbNombreKit;
	}
	
	public KitLegacy(String string) {
		cdKit = string;
	}
	
	public KitLegacy() {
	}
	
	public String getCdKit() {
		return cdKit;
	}
	public String getNbCorreoEngine() {
		return nbCorreoEngine;
	}
	public void setNbCorreoEngine(String nbCorreoEngine) {
		this.nbCorreoEngine = nbCorreoEngine;
	}
	public void setCdKit(String cdKitPlantilla) {
		this.cdKit = cdKitPlantilla;
	}

	public List<Plantilla> getListTemplates() {
		return listTemplates;
	}
	public void setListTemplates(List<Plantilla> listTemplates) {
		this.listTemplates = listTemplates;
	}
	public String getLbCorreoPlantilla() {
		return lbCorreoPlantilla;
	}

	public void setLbCorreoPlantilla(String lbCorreoPlantilla) {
		this.lbCorreoPlantilla = lbCorreoPlantilla;
	}

	public String getNbCorreoAsunto() {
		return nbCorreoAsunto;
	}

	public void setNbCorreoAsunto(String nbCorreoAsunto) {
		this.nbCorreoAsunto = nbCorreoAsunto;
	}
	
	public String getStEnvCorreo() {
		return stEnvCorreo;
	}

	public void setStEnvCorreo(String stEnvCorreo) {
		this.stEnvCorreo = stEnvCorreo;
	}

	public String getFlValHuella() {
		return flValHuella;
	}

	public void setFlValHuella(String flValHuella) {
		this.flValHuella = flValHuella;
	}

	public String getStFileunico() {
		return stFileunico;
	}

	public void setStFileunico(String stFileunico) {
		this.stFileunico = stFileunico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdKit == null) ? 0 : cdKit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KitLegacy other = (KitLegacy) obj;
		if (cdKit == null) {
			if (other.cdKit != null)
				return false;
		} else if (!cdKit.equals(other.cdKit))
			return false;
		return true;
	}

	
	
}
