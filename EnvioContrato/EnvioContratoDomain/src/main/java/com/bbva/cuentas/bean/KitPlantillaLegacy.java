package com.bbva.cuentas.bean;

public class KitPlantillaLegacy extends Generic{
	
	private String cdKit;
	private Long cdPlantilla;
	
	public String getCdKit() {
		return cdKit;
	}
	public void setCdKit(String cdKit) {
		this.cdKit = cdKit;
	}
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	
}
