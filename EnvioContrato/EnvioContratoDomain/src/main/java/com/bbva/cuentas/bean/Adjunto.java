package com.bbva.cuentas.bean;

public class Adjunto {
	
	private String nombreCorreo;
	private String ruta;
	
	public String getNombreCorreo() {
		return nombreCorreo;
	}
	public void setNombreCorreo(String nombreCorreo) {
		this.nombreCorreo = nombreCorreo;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
}
