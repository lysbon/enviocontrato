package com.bbva.cuentas.dto;

public class ClienteHuellaDigital {
	
	private ClienteHuellaDetalle cliente;

	public ClienteHuellaDetalle getCliente() {
		return cliente;
	}

	public void setCliente(ClienteHuellaDetalle cliente) {
		this.cliente = cliente;
	}
	
}
