package com.bbva.cuentas.bean;

public class EnvKit extends Generic{
	
	private Long cdKit;
	private Integer cdEnv;
	
	private String stEnvCorreo;
	private String lbCorreoPlantilla;
	private String nbCorreoAsunto;
	private String nbCorreoEngine;
	private String nbRemitente;
	private String nbRemitenteCC;
	
	public Long getCdKit() {
		return cdKit;
	}
	public void setCdKit(Long cdKit) {
		this.cdKit = cdKit;
	}
	public Integer getCdEnv() {
		return cdEnv;
	}
	public void setCdEnv(Integer cdEnv) {
		this.cdEnv = cdEnv;
	}
	public String getNbCorreoEngine() {
		return nbCorreoEngine;
	}
	public void setNbCorreoEngine(String nbCorreoEngine) {
		this.nbCorreoEngine = nbCorreoEngine;
	}
	public String getLbCorreoPlantilla() {
		return lbCorreoPlantilla;
	}
	public void setLbCorreoPlantilla(String lbCorreoPlantilla) {
		this.lbCorreoPlantilla = lbCorreoPlantilla;
	}
	public String getNbCorreoAsunto() {
		return nbCorreoAsunto;
	}
	public void setNbCorreoAsunto(String nbCorreoAsunto) {
		this.nbCorreoAsunto = nbCorreoAsunto;
	}
	public String getStEnvCorreo() {
		return stEnvCorreo;
	}
	public void setStEnvCorreo(String stEnvCorreo) {
		this.stEnvCorreo = stEnvCorreo;
	}
	public String getNbRemitente() {
		return nbRemitente;
	}
	public void setNbRemitente(String nbRemitente) {
		this.nbRemitente = nbRemitente;
	}
	public String getNbRemitenteCC() {
		return nbRemitenteCC;
	}
	public void setNbRemitenteCC(String nbRemitenteCC) {
		this.nbRemitenteCC = nbRemitenteCC;
	}
		
}
