package com.bbva.cuentas.bean;

import com.bbva.cuentas.util.Constants;

public class Flag extends Generic{
	
	private Long cdFlag;
	private String nbCondicionNombre;
	private String nbCondicionValor;
	private Integer nuOrden;
	private String chTipo;
	private boolean valor;

	public String getDesOperador(){
		if(Constants.NOEQUAL.intValue() == nuOrden.intValue()){
			return "!=";
		}else if(Constants.EQUAL.intValue() == nuOrden.intValue()){
			return "=";
		}else{
			return Constants.TYPE_DES_UNDEFINED;
		}
	}
	
	public void setDesOperador(String desOperador){
		
	}
	
	public Long getCdFlag() {
		return cdFlag;
	}
	public void setCdFlag(Long cdFlag) {
		this.cdFlag = cdFlag;
	}
	public String getNbCondicionNombre() {
		return nbCondicionNombre;
	}
	public void setNbCondicionNombre(String nbCondicionNombre) {
		this.nbCondicionNombre = nbCondicionNombre;
	}
	public String getNbCondicionValor() {
		return nbCondicionValor;
	}
	public void setNbCondicionValor(String nbCondicionValor) {
		this.nbCondicionValor = nbCondicionValor;
	}
	public boolean isValor() {
		return valor;
	}
	public void setValor(boolean valor) {
		this.valor = valor;
	}
	public Integer getNuOrden() {
		return nuOrden;
	}
	public void setNuOrden(Integer nuOrden) {
		this.nuOrden = nuOrden;
	}
	public String getChTipo() {
		return chTipo;
	}
	public void setChTipo(String chTipo) {
		this.chTipo = chTipo;
	}
	
}
