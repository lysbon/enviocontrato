package com.bbva.cuentas.bean;

public class ArchivoFileUnico {
	
	private String ruta;
	private String nombreBase;
	
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getNombreBase() {
		return nombreBase;
	}
	public void setNombreBase(String nombreBase) {
		this.nombreBase = nombreBase;
	}
	
}
