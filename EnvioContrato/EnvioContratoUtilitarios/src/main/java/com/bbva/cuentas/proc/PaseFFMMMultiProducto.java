package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseFFMMMultiProducto extends PaseGenerico{
	
	public static void run(){
		PaseFFMMMultiProducto proc = new PaseFFMMMultiProducto();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("FFMM");
			if(contarKITS>0){
				proc.revertirKits("FFMM");
			}
			proc.ejecutarScriptsKitsFFMM();
			proc.ejecutarScriptsKitsM();
			proc.ejecutarScriptsEnvKitFFMM();
			proc.ejecutarScriptsEnvSubKitsFFMM();
			proc.ejecutarScriptsKitPlantillaFFMM();
			proc.ejecutarScriptsKitPlantillaM();
			
			proc.actualizarFfmmHtml();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsEnvKitFFMM() throws IOException {
		for(String kit : consultarScriptKit("FFMM")){
			ejecutarScriptsEnvKit(kit,"BXI","!Conoce más de tu(s) Fondo(s) Mutuo(s)!","A","STRTPL",false);
			ejecutarScriptsEnvKit(kit,"BM", "!Conoce más de tu(s) Fondo(s) Mutuo(s)!","A","STRTPL",false);
		}
	}
	
	private void ejecutarScriptsEnvSubKitsFFMM() throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT) "+
				"SELECT 2,CD_KIT "+
				"FROM   APDIGB.TBG010_KIT WHERE NB_NOMBRE_KIT LIKE '%FM011%' AND CD_CODIGO LIKE 'M%'"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsKitsFFMM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'FFMM',NULL,NULL,NULL,NULL,'A','A','KIT FM011 FFMM MULTIPRODUCTO','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	private void ejecutarScriptsKitsM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG010_KIT "+
				"SELECT APDIGB.KIT_SEQ.NEXTVAL,'M'||CD_CODIGO, "+
				"       CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION, "+
				"	   ST_FILEUNICO,FL_VAL_HUELLA, "+
				"	   NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA "+
				"FROM   APDIGB.TBG010_KIT K1 "+
				"WHERE  CD_CODIGO LIKE '80%' "+
				"  AND  NOT EXISTS (SELECT 1 "+
				"                   FROM   APDIGB.TBG010_KIT K2 "+
				"                   WHERE  K2.CD_CODIGO = 'M'||K1.CD_CODIGO)"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsKitPlantillaM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT (SELECT CD_KIT FROM APDIGB.TBG010_KIT "+
				"        WHERE CD_CODIGO = 'M'||(SELECT CD_CODIGO FROM APDIGB.TBG010_KIT K WHERE K.CD_KIT = KP.CD_KIT)) CD_KIT, "+
				"       CD_PLANTILLA "+ 
				"FROM   APDIGB.TBG011_KIT_PLANTILLA KP "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '80%') "+
				"  AND  (CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') OR "+
				"       CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%'))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	private void ejecutarScriptsKitPlantillaFFMM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT DISTINCT (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = 'FFMM'),CD_PLANTILLA "+
				"FROM   APDIGB.TBG011_KIT_PLANTILLA "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '80%') "+ 
				"  AND  (CD_PLANTILLA NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') AND "+
				"       CD_PLANTILLA  NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%'))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\pi10\\ffmm_multiproducto\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20190124.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("FFMM",null,rutaDestino+"ffmm20190124.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
	
}
