package com.bbva.cuentas.apemasivas;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.proc.PaseGenerico;

public class Respuesta  extends PaseGenerico{
	
	RecibirServiceImpl recibirService;
	
	public Respuesta() {
		recibirService = new RecibirServiceImpl();
	}
	
	public static void main(String args[]){
		Respuesta proc = new Respuesta();
		String carpetaDestino = "C:\\log\\";
//		proc.recibirService.recibir("dsadasdsadsa");		
//		
		try {
			
			ConfigureApp.setEnv("PRD");
//			//descargo archivos respuesta T
//			String nombreOk = proc.obtenerNombre(true);
//			String nombreNOk = proc.obtenerNombre(false);
//			File fileOk = ConfigureApp.descargarArchivo(
//					"/mnt/xcomntip/OF_0408/"+nombreOk,
//					carpetaDestino,"");
//			File fileNOk = ConfigureApp.descargarArchivo(
//					"/mnt/xcomntip/OF_0408/"+nombreNOk,
//					carpetaDestino,"");
			/*
			String nombreOk =  "APERMASCTAOK_T29031519.TXT.DONE";
			String nombreNOk = "APERMASCTANOK_T29031519.TXT.DONE";
			File fileOk = new File(carpetaDestino+nombreOk);
			File fileNOk = new File(carpetaDestino+nombreNOk);
			*/
//			if(fileOk!=null && fileOk.exists()) {
//				String ruta = proc.crearCarpeta();
//				proc.desplegarArchivo(fileOk,ruta+"/"+nombreOk);
//				proc.desplegarArchivo(fileNOk,ruta+"/"+nombreNOk);
//				
//				proc.recibirService.recibir(proc.obtenerCarpeta());
//			}			
			
			proc.recibirService.recibir("20190318/184000");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public Date getDate() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//		return sdf.parse("20190315");
		return new Date();
	}
	
	public String obtenerCarpeta() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfHHmm = new SimpleDateFormat("HHmm");		
		Calendar date = Calendar.getInstance();
		date.setTime(getDate());
		//18:09->18:05
		//18:30->18:05
		//18:39->18:35
		//18:59->18:35
		int minuto = date.get(Calendar.MINUTE);
		if(minuto<=5) {
			date.set(Calendar.HOUR_OF_DAY,date.get(Calendar.HOUR_OF_DAY)-1);
			date.set(Calendar.MINUTE,35);
		}else if(minuto<=35) {
			date.set(Calendar.MINUTE,05);
		}else{
			date.set(Calendar.MINUTE,35);
		}		
		return sdf.format(getDate())+"/"+sdfHHmm.format(date.getTime())+"00";
	}
	
	public String crearCarpeta() throws ParseException {
		String base = "/mnt/compartido/altasmasivas/workspace/";
		String ruta = base + obtenerCarpeta();
		ConfigureApp.crearCarpetaRemota(ruta);
		return ruta;
	}
	
	public String obtenerNombre(boolean ok) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm");
		SimpleDateFormat sdfday = new SimpleDateFormat("MMddyy");
		
		Calendar date = Calendar.getInstance();
		date.setTime(new Date());
		
		Calendar dateBase = Calendar.getInstance();
		dateBase.setTime(new Date());
		dateBase.set(Calendar.HOUR_OF_DAY, 7);
		dateBase.set(Calendar.MINUTE, 35);
		
		boolean found = false;
		Integer t = 0;
		while(!found) {
			t++;
			if(date.compareTo(dateBase)<0) {
				found = true;
			}
			dateBase.add(Calendar.MINUTE, 30);
			System.out.println(sdf.format(dateBase.getTime()));
		}
		
		//t = 29;
		
		System.out.println("T es " + t);
		
		String sday = sdfday.format(new Date());
		String st = String.format("%02d",t);
		if(ok) {
			return "APERMASCTAOK_T" +st+sday+".TXT.DONE";
		}else {
			return "APERMASCTANOK_T"+st+sday+".TXT.DONE";
		}
	}
	
	public void desplegarArchivo(File origen, String nombreDestino){
		List<Ejecucion> integrado = new ArrayList<Ejecucion>();
		integrado.add(new Ejecucion(origen.getAbsolutePath(),nombreDestino));
		ConfigureApp.cargarArchivos(integrado,false);
	}
	
}
