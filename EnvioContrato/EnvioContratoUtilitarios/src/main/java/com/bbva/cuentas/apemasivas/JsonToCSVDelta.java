package com.bbva.cuentas.apemasivas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonToCSVDelta {

	public static void main(String args[]){
		
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<ColectivoDelta>> typeReference = new TypeReference<List<ColectivoDelta>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream("/json/delta.json");
		try {
			List<ColectivoDelta> registros = mapper.readValue(inputStream,typeReference);
			
			try (PrintWriter writer = new PrintWriter(new File("outputDelta.csv"))) {
				
				StringBuilder sb = new StringBuilder();
				sb.append("Empresa");
				sb.append(",");
				sb.append("RUC");
				sb.append(",");
				sb.append("Tipo Carga");
				sb.append(",");
				sb.append("Producto");
				sb.append(",");
				sb.append("Fecha Carga");
				sb.append(",");
				sb.append("FechaHora Carga");
				sb.append(",");
				sb.append("FechaHora Respuesta");
				sb.append(",");
				sb.append("Tiempo de respuesta (min)");
				sb.append(",");
				sb.append("Moneda");
				sb.append(",");
				sb.append("Estado Colectivo");
				sb.append(",");
				sb.append("Estado Detalle");
				sb.append('\n');
								
				for(ColectivoDelta registro : registros) {
					sb.append(StringUtils.replace(registro.getEmpresa(), ",",""));
					sb.append(",");
					sb.append(registro.getRuc());
					sb.append(",");
					sb.append(registro.getTipoCarga());
					sb.append(",");
					sb.append(registro.getCodigoProducto());
					sb.append(",");
					sb.append(registro.getFechaCarga());
					sb.append(",");
					sb.append(registro.getFechaHoraCarga());
					sb.append(",");
					sb.append(registro.getFechaHoraRespuesta());
					sb.append(",");
					sb.append(registro.getDelta());
					sb.append(",");
					sb.append(registro.getMoneda());
					sb.append(",");
					sb.append(registro.getEstadoColectivo());
					sb.append(",");
					sb.append(registro.getEstadoDetalle());
					sb.append('\n');
					
				}
				writer.write(sb.toString());
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			    System.out.println(e.getMessage());
		    }
			
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Unable to save users: " + e.getMessage());
		}
		
	}
	
}
