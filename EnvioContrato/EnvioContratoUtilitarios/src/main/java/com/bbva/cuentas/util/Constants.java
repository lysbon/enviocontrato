package com.bbva.cuentas.util;

public class Constants {

	/****************** Multiplica tu interes 04122017***************/
	/*
	public static final String COD_EXITO_WS = "1";
	
	public static final String COD_EXITO = "01";
	public static final String COD_NO_EXITO = "00";
	public static final String COD_NO_EXITO_SERVICE = "02";
	
	public static final String COD_PARAM_IDFORMATO   = "CTR0|P21";
	//public static final String COD_PARAM_IDFORMATO   = "CTR0|P00";
	public static final String COD_PARAM_NROCONTRATO = "CTR0|P01";
	public static final String COD_PARAM_OFIGESTOR   = "CTR0|P02";
	public static final String COD_PARAM_TIPDOC_TIT1 = "CTR0|P03";
	public static final String COD_PARAM_TIPDOC_TIT2 = "CTR0|P04";
	public static final String COD_PARAM_NRODOC_TIT1 = "CTR0|P05";
	public static final String COD_PARAM_NRODOC_TIT2 = "CTR0|P06";
	public static final String COD_PARAM_CODCEN_TIT1 = "CTR0|P07";
	public static final String COD_PARAM_CODCEN_TIT2 = "CTR0|P08";
	public static final String COD_PARAM_CODPROCEDEN = "CTR0|P09";
	public static final String COD_PARAM_CODDIVISA   = "CTR0|P10";
	public static final String COD_PARAM_EMAIL_TIT1  = "CTR0|P11";
	public static final String COD_PARAM_EMAIL_TIT2  = "CTR0|P12";
	public static final String COD_PARAM_TRX_HUELLA  = "CTR0|P17";
	
	public static final int LABEL_LENGTH = 8;
	public static final String URL_WEB_SERVICE = "http://118.216.51.55:8090/envioContratoWS/ws";
	public static final String RUTA_TRAMA = "D:\\opt\\tmp\\tramaNacarPruebaMasiva.txt";
	public static final String RUTA_TRAMAMASIVA = "D:\\opt\\tmp\\input_3_1_parte1.txt";
	public static final String RUTA_RESULTADOTRAMA = "D:\\opt\\tmp\\input_contratodigital.txt";
	*/
	/****************** Multiplica tu interes 04122017***************/
	
	public static final String COD_EXITO_WS = "1";
	
	public static final String COD_EXITO = "01";
	public static final String COD_NO_EXITO = "00";
	public static final String COD_NO_EXITO_SERVICE = "02";
	
	
	
	/****** Datos Configuracion Cabecera *******/
  //public static final String COD_PARAM_IDFORMATO   = "CFG0|P00";
	public static final String COD_PARAM_NROCONTRATO = "CFG0|P01";
	public static final String COD_PARAM_OFIGESTOR   = "CFG0|P02";
	public static final String COD_PARAM_CODPROCEDEN = "CFG0|P09";
	public static final String COD_PARAM_CODDIVISA   = "CFG0|P10";
	public static final String COD_PARAM_IDFORMATO   = "CFG0|P21";
	public static final String COD_PARAM_NROCONTRATO_METADATA = "CFG0|P27";
	public static final String COD_PARAM_GESTDOC_FILEUNICO   = "CFG0|P28";
	/****** Datos Configuracion Cabecera *******/
	
	/****** Datos Configuracion Cliente *******/
	public static final String COD_PARAM_CODCEN_TIT1 = "CFG0|P07";
	public static final String COD_PARAM_TIPDOC_TIT1 = "CFG0|P03";
	public static final String COD_PARAM_NRODOC_TIT1 = "CFG0|P04";
	public static final String COD_PARAM_EMAIL_TIT1  = "CFG0|P11";
	public static final String COD_PARAM_TRX_HUELLA  = "CFG0|P17";
	public static final String COD_PARAM_NOMBREAPELIDO_TIT1  = "CFG0|P05";
	
	/************* Actualizacion 24052017 **************/
	

	public static final int LABEL_LENGTH = 8;
	
	//public static final String URL_WEB_SERVICE = "http://118.216.104.94:8095/EnvioContratoWS/ws"; //Local
	public static final String URL_WEB_SERVICE = "http://118.180.14.22:8080/envioContratoWS/ws"; //Test
    //public static final String URL_WEB_SERVICE = "http://118.180.14.23:8080/envioContratoWS/ws"; // Calidad
	//public static final String URL_WEB_SERVICE = "http://118.180.13.221:8080/envioContratoWS/ws"; // Produccion
	
	
/************ Activado para pruebas  ****************/	
	//public static final String RUTA_TRAMAMASIVA_FILE_UNICO = "C:\\tramaNacarMasivaFileUnico.txt";
	
	//public static final String RUTA_RESULTADOTRAMA_FILE_UNICO = "C:\\input_contratodigital.txt";

	public static final String RUTA_TRAMAMASIVA = "D:\\Gianfranco-Manrique\\Pruebas\\input_multiplicadigital.txt";// input_multiplicadigital.txt se activa para pruebas en desarrollo
	public static final String RUTA_TRAMAMASIVA_OUTPUT = "D:\\Gianfranco-Manrique\\Pruebas\\output_multiplicadigital.txt";// se activa para pruebas en desarrollo
	/************ Activado para pruebas  ****************/	
	
	public static final String RUTA_RESULTADOTRAMA = "input_procesacontratodigital.txt";
	
	public static final String LABEL_CABECERA_INICIO ="<CAB>";
	public static final String LABEL_CABECERA_FIN = "</CAB>";
	
	public static final String LABEL_GENERAL_INICIO = "<GEN>";
	public static final String LABEL_GENERAL_FIN = "</GEN>";
	
	public static final String LABEL_PARTICIPE_INICIO = "<PAR>";
	public static final String LABEL_PARTICIPE_FIN = "</PAR>";
	
	public static final String RUTASALIDA_DEFAULT = "output_contratodigital.txt";
	
}
