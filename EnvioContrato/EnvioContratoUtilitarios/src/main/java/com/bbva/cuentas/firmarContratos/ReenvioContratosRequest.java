/**
 * ReenvioContratosRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class ReenvioContratosRequest  implements java.io.Serializable {
    private com.bbva.cuentas.firmarContratos.ReenvioContrato[] firmaContrato;

    private java.lang.String trama;

    private java.lang.String forzar;

    public ReenvioContratosRequest() {
    }

    public ReenvioContratosRequest(
           com.bbva.cuentas.firmarContratos.ReenvioContrato[] firmaContrato,
           java.lang.String trama,
           java.lang.String forzar) {
           this.firmaContrato = firmaContrato;
           this.trama = trama;
           this.forzar = forzar;
    }


    /**
     * Gets the firmaContrato value for this ReenvioContratosRequest.
     * 
     * @return firmaContrato
     */
    public com.bbva.cuentas.firmarContratos.ReenvioContrato[] getFirmaContrato() {
        return firmaContrato;
    }


    /**
     * Sets the firmaContrato value for this ReenvioContratosRequest.
     * 
     * @param firmaContrato
     */
    public void setFirmaContrato(com.bbva.cuentas.firmarContratos.ReenvioContrato[] firmaContrato) {
        this.firmaContrato = firmaContrato;
    }

    public com.bbva.cuentas.firmarContratos.ReenvioContrato getFirmaContrato(int i) {
        return this.firmaContrato[i];
    }

    public void setFirmaContrato(int i, com.bbva.cuentas.firmarContratos.ReenvioContrato _value) {
        this.firmaContrato[i] = _value;
    }


    /**
     * Gets the trama value for this ReenvioContratosRequest.
     * 
     * @return trama
     */
    public java.lang.String getTrama() {
        return trama;
    }


    /**
     * Sets the trama value for this ReenvioContratosRequest.
     * 
     * @param trama
     */
    public void setTrama(java.lang.String trama) {
        this.trama = trama;
    }


    /**
     * Gets the forzar value for this ReenvioContratosRequest.
     * 
     * @return forzar
     */
    public java.lang.String getForzar() {
        return forzar;
    }


    /**
     * Sets the forzar value for this ReenvioContratosRequest.
     * 
     * @param forzar
     */
    public void setForzar(java.lang.String forzar) {
        this.forzar = forzar;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReenvioContratosRequest)) return false;
        ReenvioContratosRequest other = (ReenvioContratosRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.firmaContrato==null && other.getFirmaContrato()==null) || 
             (this.firmaContrato!=null &&
              java.util.Arrays.equals(this.firmaContrato, other.getFirmaContrato()))) &&
            ((this.trama==null && other.getTrama()==null) || 
             (this.trama!=null &&
              this.trama.equals(other.getTrama()))) &&
            ((this.forzar==null && other.getForzar()==null) || 
             (this.forzar!=null &&
              this.forzar.equals(other.getForzar())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFirmaContrato() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFirmaContrato());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFirmaContrato(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTrama() != null) {
            _hashCode += getTrama().hashCode();
        }
        if (getForzar() != null) {
            _hashCode += getForzar().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReenvioContratosRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", ">reenvio-contratos-request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firmaContrato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "firmaContrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "reenvioContrato"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trama");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "trama"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forzar");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "forzar"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
