package com.bbva.cuentas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.io.FilenameUtils;

public class KarinaCeroApp {
	
	public static void main(String args[]){
		try {
			
			ConfigureApp.setEnv("PRD");
			
			String rutaOrigen  = "/mnt/compartido/apdig/outputSigned/";
			String rutaDestino = "D:\\opt\\share\\karinacero\\";
			
			List<Ejecucion> plantillas = obtenerLista(rutaOrigen,rutaDestino);
			ConfigureApp.obtenerArchivos(plantillas, null, true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static List<Ejecucion> obtenerLista(String rutaOrigen,String rutaDestino) {
		List<Ejecucion> result = new ArrayList<Ejecucion>();
		File file    = new File("C:\\Users\\p023915\\Downloads\\2.txt");
		String fecha = null;
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			for (String line = reader.readLine();  line != null;  line = reader.readLine()) {
				if(line.length()>45){
					StringTokenizer st = new StringTokenizer(line);
					int i=0;
					for(i=0;st.hasMoreTokens();i++){
						String token = st.nextToken();
						if(i==4){
							if("pdf".equalsIgnoreCase(FilenameUtils.getExtension(token))){
								
								fecha = token.substring(30,38);
								
								result.add(
										new Ejecucion(rutaOrigen+fecha+"/"+token,
												rutaDestino+token));
								
							}
						}
					}
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
}
