package com.bbva.cuentas.util;

import com.bbva.cuentas.firmarContratos.ItemContrato;

public class Util {


	public static ItemContrato newIC(String label,String value){
		ItemContrato ic = new ItemContrato();
		ic.setLabel(label);
		ic.setValue(value);
		return ic;
	}
	
	public static String obtieneStringPosicional(String cadena,int posicionInicial,int posicionFinal){
		
		return cadena.substring(posicionInicial,posicionFinal);
		
	}
	
}
