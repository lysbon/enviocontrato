package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseFFMM8 extends PaseGenerico{
	
	public static void run(){
		PaseFFMM8 proc = new PaseFFMM8();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("FM371");
			if(contarKITS>0){
				proc.revertirKits("FM371");
				proc.revertirPlantilla("FM371");
			}
			proc.ejecutarScriptsFfmmKit();
			proc.ejecutarScriptsFfmmPlantilla();
			proc.ejecutarScriptsFfmmEnvKit();
			proc.ejecutarScriptsFfmmKitPlantillaBase();
			proc.ejecutarScriptsFfmmKitPlantillaProspecto();
			//FORMATOS
			proc.actualizarFfmmHtml();
			proc.actualizarFfmmPdf();
			
			////////////////////////////////////////////////
			List<String> contarKITSM = proc.consultarScriptCodigoKit("M800462");
			if(contarKITSM!=null && !contarKITSM.isEmpty()){
				for(String cdKit :contarKITSM) {
					proc.revertirKIT(cdKit);
				}
			}
			proc.ejecutarScriptsKitsM();
			proc.ejecutarScriptsEnvSubKitMFFMM();
			proc.ejecutarScriptsKitPlantillaMFFMM();
			proc.ejecutarScriptsKitPlantillaM();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsFfmmEnvKit() throws IOException {
		for(String kit : consultarScriptKit("FM371")){
			ejecutarScriptsEnvKit(kit,"BXI","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
	}
			
	public void ejecutarScriptsFfmmKit(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'800462',NULL,NULL,NULL,NULL,'I','A','KIT FM011 FM371 SUPER DOLARES 6 FMIV','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsFfmmPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','I','A','P','A','FM371','0000','Prospecto BBVA SUPER DOLARES 6 FMIV','G','Prospecto SUPER6FMIV',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void ejecutarScriptsFfmmKitPlantillaBase(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='FM011'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600' AND ST_CONDICION = 'V'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTA'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_R'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PE281' AND NB_DESCRIPCION ='PE281 - LPDP - CONTRATOS'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600_FATCA_FORM_AUTO_CERT_PN'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTAMEN_RBA_LINE'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_RBA')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%FM011 FM371%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsFfmmKitPlantillaProspecto(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,P.CD_PLANTILLA         "+
				"FROM   APDIGB.TBG010_KIT K,            "+
				"       APDIGB.TBG002_PLANTILLA P       "+
				"WHERE  K.NB_NOMBRE_KIT LIKE '%FM011 FM371%'"+
				"  AND  SUBSTR(K.NB_NOMBRE_KIT,11,5) = P.NB_NOMBRE_FORMATO"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20190422.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("800462",null,rutaDestino+"ffmm20190422.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
		
	public void actualizarFfmmPdf() throws InterruptedException, IOException{
		final File folder = new File("D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("FM371")
		};
		
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
	
	/////////////////////////////////////////////////////////////////////
	private void ejecutarScriptsEnvSubKitMFFMM() throws IOException {
		for(String kit : consultarScriptCodigoKit("M800462")){
			ejecutarScriptsEnvKit(kit,"BXI","","","",false);
		}
	}
	
	private void ejecutarScriptsKitsM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG010_KIT "+
				"SELECT APDIGB.KIT_SEQ.NEXTVAL,'M'||CD_CODIGO, "+
				"       CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION, "+
				"	   ST_FILEUNICO,FL_VAL_HUELLA, "+
				"	   NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA "+
				"FROM   APDIGB.TBG010_KIT K1 "+
				"WHERE  CD_CODIGO LIKE '800462' "+
				"  AND  NOT EXISTS (SELECT 1 "+
				"                   FROM   APDIGB.TBG010_KIT K2 "+
				"                   WHERE  K2.CD_CODIGO = 'M'||K1.CD_CODIGO)"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsKitPlantillaM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT (SELECT CD_KIT FROM APDIGB.TBG010_KIT "+
				"        WHERE CD_CODIGO = 'M'||(SELECT CD_CODIGO FROM APDIGB.TBG010_KIT K WHERE K.CD_KIT = KP.CD_KIT)) CD_KIT, "+
				"       CD_PLANTILLA "+ 
				"FROM   APDIGB.TBG011_KIT_PLANTILLA KP "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '800462') "+
				"  AND  (CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') OR "+
				"       CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%'))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	private void ejecutarScriptsKitPlantillaMFFMM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT DISTINCT (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = 'FFMM'),CD_PLANTILLA "+
				"FROM   APDIGB.TBG011_KIT_PLANTILLA "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '800462') "+ 
				"  AND  (CD_PLANTILLA NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') AND "+
				"       CD_PLANTILLA  NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%') AND "+
				"       CD_PLANTILLA  NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG011_KIT_PLANTILLA KP WHERE KP.CD_KIT = (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = 'FFMM')))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
}
