package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseFFMMMultiProductoAdd extends PaseGenerico{
	
	public static void run(){
		PaseFFMMMultiProductoAdd proc = new PaseFFMMMultiProductoAdd();
		try {
			
			List<String> contarKITS = proc.consultarScriptCodigoKit("M800301");
			if(contarKITS!=null && !contarKITS.isEmpty()){
				for(String cdKit :contarKITS) {
					proc.revertirKIT(cdKit);
				}
			}
			
			proc.ejecutarScriptsKitsM();
			proc.ejecutarScriptsEnvSubKitFFMM();
			proc.ejecutarScriptsKitPlantillaFFMM();
			proc.ejecutarScriptsKitPlantillaM();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsEnvSubKitFFMM() throws IOException {
		for(String kit : consultarScriptCodigoKit("M800301")){
			ejecutarScriptsEnvKit(kit,"BXI","","","",false);
		}
	}
	
	private void ejecutarScriptsKitsM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG010_KIT "+
				"SELECT APDIGB.KIT_SEQ.NEXTVAL,'M'||CD_CODIGO, "+
				"       CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION, "+
				"	   ST_FILEUNICO,FL_VAL_HUELLA, "+
				"	   NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA "+
				"FROM   APDIGB.TBG010_KIT K1 "+
				"WHERE  CD_CODIGO LIKE '800301' "+
				"  AND  NOT EXISTS (SELECT 1 "+
				"                   FROM   APDIGB.TBG010_KIT K2 "+
				"                   WHERE  K2.CD_CODIGO = 'M'||K1.CD_CODIGO)"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsKitPlantillaM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT (SELECT CD_KIT FROM APDIGB.TBG010_KIT "+
				"        WHERE CD_CODIGO = 'M'||(SELECT CD_CODIGO FROM APDIGB.TBG010_KIT K WHERE K.CD_KIT = KP.CD_KIT)) CD_KIT, "+
				"       CD_PLANTILLA "+ 
				"FROM   APDIGB.TBG011_KIT_PLANTILLA KP "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '800301') "+
				"  AND  (CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') OR "+
				"       CD_PLANTILLA IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%'))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	private void ejecutarScriptsKitPlantillaFFMM() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT DISTINCT (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = 'FFMM'),CD_PLANTILLA "+
				"FROM   APDIGB.TBG011_KIT_PLANTILLA "+
				"WHERE  CD_KIT IN (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '800301') "+ 
				"  AND  (CD_PLANTILLA NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = 'FM011') AND "+
				"       CD_PLANTILLA  NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE UPPER(NB_DESCRIPCION) LIKE '%PROSP%') AND "+
				"       CD_PLANTILLA  NOT IN (SELECT CD_PLANTILLA FROM APDIGB.TBG011_KIT_PLANTILLA KP WHERE KP.CD_KIT = (SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = 'FFMM')))"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
}
