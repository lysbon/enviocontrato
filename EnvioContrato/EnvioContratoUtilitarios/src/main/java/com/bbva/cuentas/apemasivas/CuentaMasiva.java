package com.bbva.cuentas.apemasivas;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CuentaMasiva {
	
	//C.COD_EJECUTIVO,C.EMAIL_EJECUTIVO,C.NOMBRE_EMPRESA,C.RUC,C.TIPO_CUENTA,
	//DC.NUM_DOCUMENTO,DC.NOMBRES,DC.NUM_CUENTA,
    //C.MONEDA,C.FECHA_REGISTRO,
    //C.NOMINA_CENTRALIZADA,C.OFICINA
	
	@JsonProperty("COD_EJECUTIVO")
	private String codEjecutivo;
	@JsonProperty("EMAIL_EJECUTIVO")
    private String emailEjecutivo;
	@JsonProperty("NOMBRE_EMPRESA")
	private String nombreEmpresa;
	@JsonProperty("RUC")
    private String ruc;
	@JsonProperty("TIPO_CUENTA")
    private String tipoCuenta;
	@JsonProperty("NUM_DOCUMENTO")
    private String numDocumento;
	@JsonProperty("NOMBRES")
    private String nombres;
	@JsonProperty("NUM_CUENTA")
    private String numCuenta;
	@JsonProperty("MONEDA")
    private String moneda;
	@JsonProperty("FECHA_REGISTRO")
    private String fechaRegistro;
	@JsonProperty("NOMINA_CENTRALIZADA")
    private String nominaCentralizada;
	@JsonProperty("OFICINA")
    private String oficina;
    
    public CuentaMasiva() {
    }

	public String getCodEjecutivo() {
		return codEjecutivo;
	}

	public void setCodEjecutivo(String codEjecutivo) {
		this.codEjecutivo = codEjecutivo;
	}

	public String getEmailEjecutivo() {
		return emailEjecutivo;
	}

	public void setEmailEjecutivo(String emailEjecutivo) {
		this.emailEjecutivo = emailEjecutivo;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getNumDocumento() {
		return numDocumento;
	}

	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNominaCentralizada() {
		return nominaCentralizada;
	}

	public void setNominaCentralizada(String nominaCentralizada) {
		this.nominaCentralizada = nominaCentralizada;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}    
    
}
