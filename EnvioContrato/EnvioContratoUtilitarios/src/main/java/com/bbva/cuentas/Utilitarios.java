package com.bbva.cuentas;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.proc.PaseActivarFileUnico;
import com.bbva.cuentas.proc.PaseDepPlazo2;
import com.bbva.cuentas.proc.PaseEECCUpdate;
import com.bbva.cuentas.proc.PaseFFMM2UPDATED;
import com.bbva.cuentas.proc.PaseFFMM4;
import com.bbva.cuentas.proc.PaseFFMM5;
import com.bbva.cuentas.proc.PaseFFMM6;
import com.bbva.cuentas.proc.PaseFFMM7;
import com.bbva.cuentas.proc.PaseFFMM8;
import com.bbva.cuentas.proc.PaseFFMM9_DJ;
import com.bbva.cuentas.proc.PaseFFMMMultiProductoAdd;
import com.bbva.cuentas.proc.PaseIC961VidaRentaUpdate;
import com.bbva.cuentas.proc.PasePrestamosCarretera;
import com.bbva.cuentas.proc.PasePrestamosCarreteraV0;

public class Utilitarios {

	List<File> archivosBase;
	String rutaDestino;
	String extension;
	
	public static void main(String args[]){
		try {
			
//			ConfigureApp.setEnv("LOCAL2");
//			ConfigureApp.setEnv("LOCAL");
//			ConfigureApp.setEnv("TEST");
			ConfigureApp.setEnv("CALIDAD2");
//			ConfigureApp.setEnv("PRD");
//			generaRuta("/mnt/compartido/apdig/output/");
//			actualizarFormatoBG871Fatca();
//			actualizarFormatoBG871();
//			actualizarFormatoCorreosStrTpl();
//			actualizarFormatoCorreosStrTplCte();
		
//			cargarTrama();
//			ejecutarTrama();
//			descargarBG871();
	
//			descargarTramaReenvio();
//			ConfigureApp.obtenerLogs(false);
//			ConfigureApp.obtenerDocumentos("90016480");
//			ConfigureApp.obtenerDocumentos("20062919");
//			ConfigureApp.obtenerArchivo("/mnt/compartido/apdig/output/BGC41_2.html","C:\\Users\\p023915\\Downloads\\");
			
//			actualizarFormatoCorreosVidaRenta();
//			actualizarCorreosAhorroAlToque();
			
			descargarLogs("2");
//			descargarFile();
//			descargarLogsPercta("1");

//			actualizarFormatosPDF();
//			actualizarFormatoCorreos();
			
//			descargarLogs("2");
//			buscarArchivos();
			
//			PaseFFMM.run();
//			PaseFFMM2UPDATED.run();
//			PaseFFMM3.run();			
//			PaseAhorrador.run();
//			PaseAhorrador2.run();
//			PaseDepPlazo.run();
//			PaseGlomo.run();
//			PaseBiometriaFacial.run();
//			PaseCambioArticulo.run();
//			PaseCorrientesCorrectivo.run();
//			PaseSeguroProtecTarjeta.run();
//			PaseActivarFileUnico.run();
//			PaseDepPlazo2.run();
//			PaseIC961VidaRentaUpdate.run();
//			PaseFFMMMultiProducto.run();
//			PaseActivarFileUnico.run();
//			PaseEECCUpdate.run();
			
//			PaseFFMM4.run();
//			PaseFFMMMultiProductoAdd.run();
			
//			PaseFFMM5.run();
			
//			PasePrestamosCarretera.run();
			
//			PaseFFMM6.run();
//			PaseFFMM7.run();
//			PaseFFMM8.run();
//			PaseFFMM9_DJ.run();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void actualizarFormatoCorreosVidaRenta(){
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\pi8\\cambio_articulos_lpd\\vidarenta\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"400822.html",rutaDestino+"400822LPD.html"));
		ConfigureApp.cargarArchivos(plantillas,false);
	}
	
	public static List<File> listFilesForFolder(final File folder) {
	    List<File> filenames = new ArrayList<File>();
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            filenames.addAll(listFilesForFolder(fileEntry));
	        } else {
	        	filenames.add(fileEntry);
	        }
	    }
		return filenames;
	}
	
	public File findFileByName(String contains){
		for(File file : this.getArchivosBase()){
			if(StringUtils.containsIgnoreCase(file.getName(), contains) &&
			   StringUtils.containsIgnoreCase(file.getName(), this.getExtension())){
				return file;
			}
		}
		return null;
	}
	
	public void imprimir(List<Ejecucion> datos){
		if(datos==null) return;
		System.out.println("Size "+datos.size());
		for(Ejecucion e : datos){
			System.out.println(e.toString());
		}
	}
	
	public Ejecucion carga(String plantilla){
		return new Ejecucion(
				findFileByName(plantilla).getAbsolutePath(),
				this.getRutaDestino()+plantilla+"."+this.getExtension());
	}
	
	public List<Ejecucion> carga(Plantilla plantillas[]) throws JsonParseException, JsonMappingException, IOException{
		return this.carga(plantillas,"_20180702");
	}
	
	public List<Ejecucion> carga(Plantilla plantillas[],String date) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> lista = new ArrayList<Ejecucion>();
		for(Plantilla plantilla : plantillas){
			if(plantilla.getCdPlantilla() == null){
				String cdPlantilla = consultarScriptPlantillaFormato(plantilla.getNbNombreFormato());
				plantilla.setCdPlantilla(new Long(cdPlantilla));
			}
			
			lista.add(new Ejecucion(
				plantilla.getCdPlantilla().toString(),
				findFileByName(plantilla.getNbNombreFormato()).getAbsolutePath(),
				this.getRutaDestino()+plantilla.getNbNombreFormato()+date+"."+this.getExtension()));
			
		}
		for(Plantilla plantilla : plantillas){
			System.out.println(plantilla.getCdPlantilla()+","+plantilla.getNbNombreFormato());
		}
		return lista;
	}
	
	public void validarcarga(Plantilla plantillas[]) throws JsonParseException, JsonMappingException, IOException{
		for(Plantilla plantilla : plantillas){
			String formatoReal = consultarScriptFormato(plantilla.getCdPlantilla());
			System.out.println(plantilla.getCdPlantilla()+"-"+formatoReal+"    "+plantilla.getNbNombreFormato());
		}
	}
	
	@SuppressWarnings("unchecked")
	public String consultarScriptPlantillaFormato(String nombreFormato) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = '"+nombreFormato+"'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return dataList[0].get("CD_PLANTILLA").toString();
	}
	
	@SuppressWarnings("unchecked")
	public String consultarScriptFormato(Long cdPlantilla) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT NB_NOMBRE_FORMATO FROM APDIGB.TBG002_PLANTILLA WHERE CD_PLANTILLA = "+cdPlantilla.toString()));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return dataList[0].get("NB_NOMBRE_FORMATO").toString();
	}
	public Plantilla plantilla(Long codigo,String nombreFormato){
		Plantilla plantilla = new Plantilla();
		plantilla.setCdPlantilla(codigo);
		plantilla.setNbNombreFormato(nombreFormato);
		return plantilla;
	}
	public Plantilla plantilla(String nombreFormato){
		Plantilla plantilla = new Plantilla();
		plantilla.setNbNombreFormato(nombreFormato);
		return plantilla;
	}
	
	public static void actualizarCorreosAhorroAlToque(){
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\pi8\\cambio_articulos_lpd\\tpl_prd_updated\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"010001.html",rutaDestino+"010001LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010002.html",rutaDestino+"010002LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010008.html",rutaDestino+"010008LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010031.html",rutaDestino+"010031LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010032.html",rutaDestino+"010032LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010034.html",rutaDestino+"010034LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010041.html",rutaDestino+"010041LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010042.html",rutaDestino+"010042LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010091.html",rutaDestino+"010091LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010092.html",rutaDestino+"010092LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010093.html",rutaDestino+"010093LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"010094.html",rutaDestino+"010094LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020008.html",rutaDestino+"020008LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020009.html",rutaDestino+"020009LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020010.html",rutaDestino+"020010LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020023.html",rutaDestino+"020023LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020024.html",rutaDestino+"020024LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020025.html",rutaDestino+"020025LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020037.html",rutaDestino+"020037LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020038.html",rutaDestino+"020038LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020043.html",rutaDestino+"020043LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020044.html",rutaDestino+"020044LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020046.html",rutaDestino+"020046LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020047.html",rutaDestino+"020047LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020049.html",rutaDestino+"020049LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020050.html",rutaDestino+"020050LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020088.html",rutaDestino+"020088LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020089.html",rutaDestino+"020089LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020090.html",rutaDestino+"020090LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020091.html",rutaDestino+"020091LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020092.html",rutaDestino+"020092LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020093.html",rutaDestino+"020093LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020109.html",rutaDestino+"020109LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"020110.html",rutaDestino+"020110LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"070001.html",rutaDestino+"070001LPD.html"));
		plantillas.add(new Ejecucion(rutaOrigen+"070002.html",rutaDestino+"070002LPD.html"));
		ConfigureApp.cargarArchivos(plantillas,false);
	}
	
	public static void actualizarFormatoCorreos(){
		//String rutaOrigen  = "D:\\ws\\ahorroaltoque\\repos\\ahorro-al-toque\\fuentes\\formatos\\";
		String rutaOrigen  = "C:\\Jairo\\Proyectos\\SERVICIO_CORREO\\actualizarFormatos\\Actualizar\\";
		
		String rutaDestino = "/mnt/compartido/apdig/output/"; //CALIDAD
		
		//String rutaDestino ="/mnt/compartido/apdig/formatos/"; //CALIDAD
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		
		/******* Esta version sera usada para el pase a produccion de cancelacion de cuentas  *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"CancelacionCTS.html",rutaDestino+"CancelacionCTS.html"));
		//plantillas.add(new Ejecucion(rutaOrigen+"CancelacionAHORRO.html",rutaDestino+"CancelacionAHORRO.html"));
		//plantillas.add(new Ejecucion(rutaOrigen+"CancelacionCTACTE.html",rutaDestino+"CancelacionCTACTE.html"));
		/******* Esta version sera usada para el pase a produccion de cancelacion de cuentas  *********/
		
		/******* Esta version es para reestrablecer calidad *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"BG011.html",rutaDestino+"BG011.html"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BGC41.html",rutaDestino+"BGC41.html"));
		/******* Esta version es para reestrablecer calidad *********/
		
		/******* Esta version es para multiplica tu interes *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"KIT_BG561_AFILIACION.html",rutaDestino+"KIT_BG561_AFILIACION.html"));
		//plantillas.add(new Ejecucion(rutaOrigen+"KIT_BG571_DESAFILIACION.html",rutaDestino+"KIT_BG571_DESAFILIACION.html"));
		/******* Esta version es para multiplica tu interes *********/
		
		/******* Esta version para homologar *********/
		plantillas.add(new Ejecucion(rutaOrigen+"BG511.html",rutaDestino+"BG511.html"));
		//plantillas.add(new Ejecucion(rutaOrigen+"KIT_BG571_DESAFILIACION.html",rutaDestino+"KIT_BG571_DESAFILIACION.html"));
		/******* Esta version para homologar *********/
		
		ConfigureApp.cargarArchivos(plantillas,false);
	}
		
	public static void descargarLogs(){
		descargarLogs("");
	}
	
	public static void generaRuta(String ruta){
		ConfigureApp.generarRuta(ruta);
	}
	
	public static void descargarFile(){
		String file     = "/pr/apdig/online/pe/web/j2ee/EnvioContratoWS_war.ear/EnvioContratoWS.war/WEB-INF/classes/log4j2.xml";
		String carpetaDestino = "C:\\log\\";
		List<Ejecucion> archivos = new ArrayList<Ejecucion>();
		archivos.add(new Ejecucion(file,carpetaDestino));
		ConfigureApp.obtenerArchivos(archivos,"",false);
	}
	
	public static void buscarArchivos() {
		String ruta = "/pr/apdig/online/pe/web/log/";
		String filtro = "";
		ConfigureApp.invocarBuscarArchivo(ruta,filtro);
	}
	
	public static void descargarLogs(String sufijo){
		String rutaOrigen     = "/pr/apdig/online/pe/web/log/";
//		String rutaOrigen     = "/home/lysbon/logs/pr/apdig/online/pe/web/log/";
//		String rutaOrigen     = "/pr/sgbxct/online/pe/web/log/";
//		String rutaOrigen     = "/log/sign/service/";
		
//		String carpetaDestino = "C:\\Users\\p023915\\Downloads\\";
		String carpetaDestino = "C:\\log\\";
		
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"mylog.log",carpetaDestino));
//		plantillas.add(new Ejecucion(rutaOrigen+"mylog-2018-30-10-1.log.gz",carpetaDestino));
//		plantillas.add(new Ejecucion(rutaOrigen+"SBC.log",carpetaDestino));
		ConfigureApp.obtenerArchivos(plantillas,sufijo+".",false);
	}
	
	public static void descargarBG871(){
		String rutaOrigen     = "/mnt/compartido/apdig/output/";
		String carpetaDestino = "C:\\Users\\p023915\\Downloads\\";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"JetForm_BG871_V7.PDF",carpetaDestino));
		ConfigureApp.obtenerArchivos(plantillas,null,false);
	}
	
	public static void descargarTramaReenvio(){
		String rutaOrigen     = "/mnt/compartido/apdig/formatos/";
		String carpetaDestino = "C:\\Users\\p023915\\Downloads\\";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"trama_12.txt",carpetaDestino));
		ConfigureApp.obtenerArchivos(plantillas,"recep",false);
	}
	
	public static void cargarTrama(){
		String rutaOrigen  = "C:\\Users\\p023915\\Downloads\\";
		String rutaDestino = "/mnt/compartido/apdig/formatos/";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"trama_12_proc.txt",rutaDestino));
		ConfigureApp.cargarArchivos(plantillas,true);
	}
	public static void ejecutarTrama(){
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion("trama_12_proc.txt",null));
		ConfigureApp.invocarEjecutarReenvio(plantillas);
	}
	
	public static void descargarLogsPercta(String suffix){
		String rutaOrigen     = "/pr/perfilador_servicios/online/pe/web/log/";
		//String rutaOrigen     = "/pr/percta/online/pe/web/log/";
		//String rutaOrigen     = "/pr/perfilador/online/pe/web/log/";
		//String rutaOrigen     = "/pr/cuentaideal/online/pe/web/log/";
		
		String carpetaDestino = "C:\\Users\\p018814\\Downloads\\";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		plantillas.add(new Ejecucion(rutaOrigen+"rest.log",carpetaDestino));
		//plantillas.add(new Ejecucion(rutaOrigen+"percta.log",carpetaDestino));
		//plantillas.add(new Ejecucion(rutaOrigen+"perfilador.log",carpetaDestino));
		//plantillas.add(new Ejecucion(rutaOrigen+"cuentaideal.log",carpetaDestino));
		ConfigureApp.obtenerArchivos(plantillas,suffix,false);
	}
	
	/******* Esta version sera usada para el pase a produccion de john 7072017314-01 P110 - Eliminar fondo de protección tarjeta de contratos *********/
	public static void actualizarFormatosPDF() {
		//String rutaOrigen  = "C:\\pase\\PDF\\";
		String rutaOrigen  = "D:\\Gianfranco-Manrique\\Requerimientos\\Historica-Reprocesos\\Plantillas\\";
	
		
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillas = new ArrayList<Ejecucion>();
		
		/******* Esta version sera usada para Multiplica tu interes  *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"BG561.pdf",rutaDestino+"BG561.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG571.pdf",rutaDestino+"BG571.pdf"));
		/******* Esta version sera usada para Multiplica tu interes  *********/
		
		/******* Esta version sera usada para el pase a produccion de cancelacion de cuentas  *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"VOUCHER_CANCELACION.pdf",rutaDestino+"VOUCHER_CANCELACION.pdf"));
		/******* Esta version sera usada para el pase a produccion de cancelacion de cuentas  *********/		
		
		/******* Esta version sera usada para el pase a produccion de john 7072017314-01 P110 - Eliminar fondo de protección tarjeta de contratos *********/
		/*
		plantillas.add(new Ejecucion(rutaOrigen+"BG011.pdf",rutaDestino+"BG011.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG111.pdf",rutaDestino+"BG111.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG151.pdf",rutaDestino+"BG151.pdf"));*/
	/*	plantillas.add(new Ejecucion(rutaOrigen+"BG281.pdf",rutaDestino+"BG281.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG351.pdf",rutaDestino+"BG351.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG451.pdf",rutaDestino+"BG451.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG461.pdf",rutaDestino+"BG461.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG871.pdf",rutaDestino+"BG871.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BG971.pdf",rutaDestino+"BG971.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BGB81.pdf",rutaDestino+"BGB81.pdf"));
		plantillas.add(new Ejecucion(rutaOrigen+"BGC41.pdf",rutaDestino+"BGC41.pdf"));
		*/
		/******* Esta version sera usada para el pase a produccion de john 7072017314-01 P110 - Eliminar fondo de protección tarjeta de contratos *********/		

		
		/******* Esta version es para reestrablecer calidad *********/
		//plantillas.add(new Ejecucion(rutaOrigen+"BG281.pdf",rutaDestino+"BG281.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG011.pdf",rutaDestino+"BG011.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG481.pdf",rutaDestino+"BG481.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG0C1.pdf",rutaDestino+"BG0C1.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG511.pdf",rutaDestino+"BG511.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG461.pdf",rutaDestino+"BG461.pdf"));
		/******* Esta version es para reestrablecer calidad *********/
		
		
		/******* Version de John BG461 *********/
		
		//plantillas.add(new Ejecucion(rutaOrigen+"BG461.pdf",rutaDestino+"BG461.pdf"));
		//plantillas.add(new Ejecucion(rutaOrigen+"BG0D1.pdf",rutaDestino+"BG0D1.pdf"));
	   //	plantillas.add(new Ejecucion(rutaOrigen+"BG0C1.pdf",rutaDestino+"BG0C1.pdf"));
		
		/******* Version de John BG461 *********/
		
		/******* Version BG461 v58 *********/
		plantillas.add(new Ejecucion(rutaOrigen+"BG461.pdf",rutaDestino+"BG461.pdf"));
		
		/******* Version BG461 v58 *********/
		
		
		ConfigureApp.cargarArchivos(plantillas,true);
	}

	public List<File> getArchivosBase() {
		return archivosBase;
	}

	public void setArchivosBase(List<File> archivosBase) {
		this.archivosBase = archivosBase;
	}

	public String getRutaDestino() {
		return rutaDestino;
	}

	public void setRutaDestino(String rutaDestino) {
		this.rutaDestino = rutaDestino;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	
	
}
