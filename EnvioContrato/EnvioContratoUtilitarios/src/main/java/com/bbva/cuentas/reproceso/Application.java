package com.bbva.cuentas.reproceso;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang.StringUtils;

import com.bbva.cuentas.firmarContratos.Cliente;
import com.bbva.cuentas.firmarContratos.FirmaContrato;
import com.bbva.cuentas.firmarContratos.FirmarContratosPort;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortService;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortServiceLocator;
import com.bbva.cuentas.firmarContratos.FirmarContratosRequest;
import com.bbva.cuentas.firmarContratos.FirmarContratosResponse;
import com.bbva.cuentas.firmarContratos.ItemContrato;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Util;

public class Application {

	public static void main(String[] args) throws Exception {
		 String pathFile = null;
		 Application parser=null;
		
		 args = new String[2];                     // Se comenta cuando mandamos a produccion
		 args[0]= Constants.RUTA_TRAMAMASIVA;      // Se comenta cuando mandamos a produccion
		 args[1]= Constants.RUTA_TRAMAMASIVA_OUTPUT;   // Se comenta cuando mandamos a produccion
		
	
		 
		FirmaContrato firmaContrato = new FirmaContrato();
		ArrayList listClientes = new ArrayList();
		
		int estado = 0; // inicio =1 y fin=2
		// -- Para ambientes --//
		int estadoFormato = 1; // CAB=1 o PAR=2

		if (args != null && args.length > 0) {
			pathFile = args[0];
		} else {
			pathFile = "input.txt";
		}
		
		parser = new Application(pathFile);
		
		obtieneRutaSalida(args,pathFile);
		
		BufferedReader br = null;
		String line;
		int index = 0;
		File fileDir = new File(pathFile);
		br = new BufferedReader(new InputStreamReader(new FileInputStream(
				fileDir)));

		br.mark(1);
		if (br.read() != 0xFEFF)
			br.reset();

		while ((line = br.readLine()) != null) {

			line = line.trim();
			
			switch (estadoFormato) {
			case 1:

				if (line.equals(Constants.LABEL_CABECERA_INICIO)) {
					estadoFormato = 1;
					estado = 1;

					
					listaItems = new ArrayList();
					firmaContrato = new FirmaContrato();
					
					
				} else {
					if (line.equals(Constants.LABEL_CABECERA_FIN)) {
						estado = 2;
						estadoFormato = 2;

						ItemContrato[] itemsContrato = new ItemContrato[listaItems
								.size()];
						listaItems.toArray(itemsContrato);
						
						if (validarConfiguracionFirmaContrato(itemsContrato)) {
							firmaContrato
									.setIdContrato(buscarNombreFormato(itemsContrato));
							firmaContrato
									.setNumeroContrato(buscarNumeroContrato(itemsContrato));
							firmaContrato
									.setOficinaGestora(buscarOficinaGestora(itemsContrato));
							firmaContrato
									.setProcedencia(buscarProcedencia(itemsContrato));
							
							firmaContrato.setGestDoc(buscarParametro(itemsContrato,Constants.COD_PARAM_GESTDOC_FILEUNICO));
							
						}
						// imprimirParametria(itemsContrato);
					} else {
						if (estado == 1) {

							
							processLineItem(line, index);
							index++;
							
						}
					}
				}
				break;

			case 2:

				if (line.equals(Constants.LABEL_GENERAL_INICIO)) {
					estadoFormato = 2;
					estado = 1;

					
					listaItems = new ArrayList();
					
				} else {
					if (line.equals(Constants.LABEL_GENERAL_FIN)) {
						estado = 2;

						estadoFormato = 3;

						
						ItemContrato[] itemsContrato = new ItemContrato[listaItems
								.size()];
						listaItems.toArray(itemsContrato);
						firmaContrato.setListaItems(itemsContrato);
						// imprimirParametria(itemsContrato);
					} else {
						if (estado == 1) {

							
							processLineItem(line, index);
							index++;
							
						}
					}
				}
				break;

			case 3:

				if (line.equals(Constants.LABEL_PARTICIPE_INICIO)) {
					estadoFormato = 3;
					estado = 1;
					listaItems = new ArrayList();
				} else {
					if (line.equals(Constants.LABEL_PARTICIPE_FIN)) {
						estado = 2;

						
						ItemContrato[] itemsContrato = new ItemContrato[listaItems
								.size()];
						listaItems.toArray(itemsContrato);

						if (validarConfiguracionCliente(itemsContrato)) {

							Cliente cliente = construirCliente1(itemsContrato);
							cliente.setListaItems(itemsContrato);
							listClientes.add(cliente);
						}
						// imprimirParametria(itemsContrato);
					} else {
						if (estado == 1) {

							
							processLineItem(line, index);
							index++;
							

						}
					}
				}
				break;
			}

		}

		br.close();

		boolean result = parser.processLine(firmaContrato, listClientes);

		if (result) {
			System.out.println("Invocar servicio apertura digital");
			parser.invocarServicio();
			System.out.println("envio al servicio");
		}
	}

	public Application(String aFileName) {
		fFilePath = aFileName;
	}
	
	

	public void invocarServicio() {
		FirmarContratosPortService EnviarContratoService = new FirmarContratosPortServiceLocator();
		((FirmarContratosPortServiceLocator) EnviarContratoService)
				.setFirmarContratosPortSoap11EndpointAddress(obtenerEndpoint());
		try {
			FirmarContratosPort port = EnviarContratoService
					.getFirmarContratosPortSoap11();

			System.out.println(obj.getFirmaContrato().getListaClientes(0)
					.getNroDocumento());
			System.out.println(obj.getFirmaContrato().getListaClientes(0)
					.getEmail());

			FirmarContratosResponse response = port.firmarContratos(obj);
			if (Constants.COD_EXITO_WS.equals(response.getCodigoResultado())) {
				output14(Constants.COD_EXITO,
						"El proceso se realizo correctamente.");
			} else {
				output14(Constants.COD_NO_EXITO_SERVICE,
						"Error al invocar servicio web de apertura digital. "
								+ response.getMensajeResultado());
			}

		} catch (ServiceException e) {
			output14(Constants.COD_NO_EXITO_SERVICE,
					"Error al conectar con servicio web de apertura digital. "
							+ e.getMessage());
		} catch (RemoteException e) {
			output14(
					Constants.COD_NO_EXITO_SERVICE,
					"Error al invocar servicio web de apertura digital. "
							+ e.getMessage());
		}
	}

	private String obtenerEndpoint() {
		/*
		 * BufferedReader br = null; try { String rutaProperties = null; try {
		 * File fileInput = new File(fFilePath); rutaProperties =
		 * fileInput.getAbsoluteFile().getParent()+"/EnvioContrato.properties";
		 * }catch(Exception e){ rutaProperties = "EnvioContrato.properties"; }
		 * System.out.println(rutaProperties); br = new BufferedReader(new
		 * FileReader(rutaProperties)); String line = br.readLine();
		 * if(line!=null && line.length()>0){ br.close();
		 * System.out.println("<"+line+">"); return line.trim(); } } catch
		 * (Exception e) { System.out.println(e.getMessage());
		 * output14(Constants
		 * .COD_NO_EXITO,"Error al leer archivo properties. "+e.getMessage()); }
		 */
		return Constants.URL_WEB_SERVICE;
	}

	
	public void output14(String mensaje, String mensaje2) {

		FileOutputStream fop = null;
		File file;

		try {

			file = new File(rutaSalida);
			fop = new FileOutputStream(file);
			if (!file.exists()) {
				file.createNewFile();
			}
			String output = mensaje;
			if (mensaje2 != null) {
				output += mensaje2;
			}
			byte[] contentInBytes = output.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	public static void obtieneRutaSalida(String[] args,String pathFile) {
		String pathFileOutPut;
		try {
		
				if(args.length>=2)
				{
						pathFileOutPut = args[1];
				}
				else
				{
						pathFileOutPut=pathFile;
				}
			
			    File fileInput = new File(pathFileOutPut);
			    
			    if(args.length>=2)
				{
			    	nombreArchivoSalida=fileInput.getName();
					rutaSalida=pathFileOutPut;
				}
				else
				{
					nombreArchivoSalida=Constants.RUTASALIDA_DEFAULT;
					rutaSalida = fileInput.getAbsoluteFile().getParent()
							+ File.separator + Constants.RUTASALIDA_DEFAULT;
				}
				
			} catch (Exception e) {
				rutaSalida = nombreArchivoSalida;
			}
	}
	
	
	public final boolean processLine() {
		obj = new FirmarContratosRequest();
		BufferedReader br = null;
		listaItems = new ArrayList();
		try {
			String line;
			int index = 0;
			br = new BufferedReader(new FileReader(fFilePath));
			while ((line = br.readLine()) != null) {

				processLineItem(line, index);
				index++;
			}
			br.close();

			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);

			if (validarListaItems(itemsContrato)) {

				List listClientes = new ArrayList();
				Cliente c1 = construirCliente1(itemsContrato);
				c1.setListaItems(itemsContrato);
				if (c1 != null)
					listClientes.add(c1);

				Cliente[] listaClientes = new Cliente[listClientes.size()];
				listaClientes = (Cliente[]) listClientes.toArray(listaClientes);

				FirmaContrato firmaContrato = new FirmaContrato();
				firmaContrato.setListaClientes(listaClientes);
				firmaContrato.setListaItems(itemsContrato);
				firmaContrato.setIdContrato(buscarNombreFormato(itemsContrato));
				firmaContrato
						.setNumeroContrato(buscarNumeroContrato(itemsContrato));
				firmaContrato
						.setOficinaGestora(buscarOficinaGestora(itemsContrato));
				firmaContrato.setProcedencia(buscarProcedencia(itemsContrato));
				obj.setFirmaContrato(firmaContrato);
			}

		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return false;
		}
		return true;
	}


	public FirmaContrato processLineFirmaContrato() {

		BufferedReader br = null;
		listaItems = new ArrayList();
		FirmaContrato firmaContrato = new FirmaContrato();
		try {
			String line;
			int index = 0;
			br = new BufferedReader(new FileReader(fFilePath));
			while ((line = br.readLine()) != null) {

				processLineItem(line, index);
				index++;
			}
			br.close();

			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);

			if (validarConfiguracionFirmaContrato(itemsContrato)) {
				firmaContrato.setIdContrato(buscarNombreFormato(itemsContrato));
				firmaContrato
						.setNumeroContrato(buscarNumeroContrato(itemsContrato));
				firmaContrato
						.setOficinaGestora(buscarOficinaGestora(itemsContrato));
				firmaContrato.setProcedencia(buscarProcedencia(itemsContrato));
			}

		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return null;
		}
		return firmaContrato;
	}

	public FirmaContrato processLineItemFirmaContrato(
			FirmaContrato firmaContrato) {

		BufferedReader br = null;
		listaItems = new ArrayList();

		try {
			String line;
			int index = 0;
			br = new BufferedReader(new FileReader(fFilePath));
			while ((line = br.readLine()) != null) {

				processLineItem(line, index);
				index++;
			}
			br.close();

			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);
			firmaContrato.setListaItems(itemsContrato);

		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return null;
		}
		return firmaContrato;
	}

	public FirmaContrato processLineItemFirmaContrato(
			FirmaContrato firmaContrato, String line, int index) {

		try {
			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);
			firmaContrato.setListaItems(itemsContrato);

		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return null;
		}
		return firmaContrato;
	}

	public ArrayList processClientes(ArrayList listClientes) {
		BufferedReader br = null;
		listaItems = new ArrayList();
		try {
			String line;
			int index = 0;
			br = new BufferedReader(new FileReader(fFilePath));
			while ((line = br.readLine()) != null) {

				processLineItem(line, index);
				index++;
			}
			br.close();

			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);

			if (validarConfiguracionCliente(itemsContrato)) {

				Cliente cliente = construirCliente1(itemsContrato);
				cliente.setListaItems(itemsContrato);
				listClientes.add(cliente);
			}

		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return null;
		}
		return listClientes;
	}

	public final boolean processLine(FirmaContrato firmaContrato,
			ArrayList listClientes) {
		obj = new FirmarContratosRequest();

		listaItems = new ArrayList();
		try {
			Cliente[] listaClientes = new Cliente[listClientes.size()];
			listaClientes = (Cliente[]) listClientes.toArray(listaClientes);
			firmaContrato.setListaClientes(listaClientes);
			obj.setFirmaContrato(firmaContrato);
		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,
					"Error al procesar archivo de entrada. " + e.getMessage());
			return false;
		}
		return true;
	}

	

	private static void imprimirParametria(ItemContrato[] itemsContrato) {
		for (int i = 0; i < itemsContrato.length; i++) {
			System.out.println("<fir:listaItems>");
			System.out.println("  <fir:label>" + itemsContrato[i].getLabel()
					+ "</fir:label>");
			System.out.println("  <fir:value>" + itemsContrato[i].getValue()
					+ "</fir:value>");
			System.out.println("</fir:listaItems>");
		}
	}

	private static boolean validarConfiguracionFirmaContrato(
			ItemContrato[] itemsContrato) throws Exception {
		if (itemsContrato == null || itemsContrato.length == 0) {
			throw new Exception("La lista no contiene parametros");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_IDFORMATO) == null) {
			throw new Exception("Debe enviar nombre de formato");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_OFIGESTOR) == null) {
			throw new Exception("Debe enviar oficina gestora");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO) == null) {
			throw new Exception("Debe enviar numero de contrato");
		}

		return true;
	}

	private static boolean validarConfiguracionCliente(
			ItemContrato[] itemsContrato) throws Exception {
		if (itemsContrato == null || itemsContrato.length == 0) {
			throw new Exception("La lista no contiene parametros");
		}

		if (buscarParametro(itemsContrato, Constants.COD_PARAM_TRX_HUELLA) == null) {
			throw new Exception(
					"Debe enviar numero de transaccion huella digital");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_CODCEN_TIT1) == null) {
			throw new Exception("Debe enviar codigo central de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_TIPDOC_TIT1) == null) {
			throw new Exception("Debe enviar tipo de documento de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_NRODOC_TIT1) == null) {
			throw new Exception("Debe enviar numero de documento de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_EMAIL_TIT1) == null) {
			throw new Exception("Debe enviar email de titular");
		}
		return true;
	}

	private boolean validarListaItems(ItemContrato[] itemsContrato)
			throws Exception {
		if (itemsContrato == null || itemsContrato.length == 0) {
			throw new Exception("La lista no contiene parametros");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_IDFORMATO) == null) {
			throw new Exception("Debe enviar nombre de formato");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_OFIGESTOR) == null) {
			throw new Exception("Debe enviar oficina gestora");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO) == null) {
			throw new Exception("Debe enviar numero de contrato");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_TRX_HUELLA) == null) {
			throw new Exception(
					"Debe enviar numero de transaccion huella digital");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_CODCEN_TIT1) == null) {
			throw new Exception("Debe enviar codigo central de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_TIPDOC_TIT1) == null) {
			throw new Exception("Debe enviar tipo de documento de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_NRODOC_TIT1) == null) {
			throw new Exception("Debe enviar numero de documento de titular");
		}
		if (buscarParametro(itemsContrato, Constants.COD_PARAM_EMAIL_TIT1) == null) {
			throw new Exception("Debe enviar email de titular");
		}
		return true;
	}

	

	private static Cliente construirCliente1(ItemContrato[] itemsContrato) {
		return construirCliente(itemsContrato, Constants.COD_PARAM_CODCEN_TIT1,
				Constants.COD_PARAM_TIPDOC_TIT1,
				Constants.COD_PARAM_NRODOC_TIT1,
				Constants.COD_PARAM_EMAIL_TIT1, Constants.COD_PARAM_TRX_HUELLA,
				Constants.COD_PARAM_NOMBREAPELIDO_TIT1);
	}



	

	
	private static Cliente construirCliente(ItemContrato[] itemsContrato,
			String paramCodCen, String paramTipDoc, String paramNroDoc,
			String paramEmail, String paramTrxHuella, String paramNombreApellido) {
		Cliente cliente = new Cliente();
		for (int i = 0; i < itemsContrato.length; i++) {
			if (paramCodCen.equals(itemsContrato[i].getLabel())) {
				cliente.setCodigoCentral(itemsContrato[i].getValue());
			} else if (paramTipDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setTipoDocumento(itemsContrato[i].getValue());
			} else if (paramNroDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setNroDocumento(itemsContrato[i].getValue());
			} else if (paramEmail.equals(itemsContrato[i].getLabel())) {
				cliente.setEmail(itemsContrato[i].getValue());
			} else if (paramTrxHuella.equals(itemsContrato[i].getLabel())) {
				cliente.setIdTrxHuellaDigital(itemsContrato[i].getValue());
			} else if (paramNombreApellido.equals(itemsContrato[i].getLabel())) {
				cliente.setNombreApellido(itemsContrato[i].getValue());
			}
		}
		if (cliente.getCodigoCentral() == null
				|| cliente.getCodigoCentral().length() == 0) {
			return null;
		}
		return cliente;
	}

	

	private static String buscarNombreFormato(ItemContrato[] itemsContrato) {
		return buscarParametro(itemsContrato, Constants.COD_PARAM_IDFORMATO);
	}

	private static String buscarNumeroContrato(ItemContrato[] itemsContrato) {
       
		String nroEntidad=null;
		String nroCtaCentroOficina=null;
		String nroCtaCuenta=null;
		
		if(buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO_METADATA)!=null)
		{
			return buscarParametro(itemsContrato,Constants.COD_PARAM_NROCONTRATO_METADATA);
		}
		else
		{
			nroEntidad=Util.obtieneStringPosicional(buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO),0,4);
			nroCtaCentroOficina=Util.obtieneStringPosicional(buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO),4,8);
			nroCtaCuenta=Util.obtieneStringPosicional(buscarParametro(itemsContrato, Constants.COD_PARAM_NROCONTRATO),10,20);
	     	return nroEntidad+nroCtaCentroOficina + nroCtaCuenta ;
		}
	}

	private static String buscarOficinaGestora(ItemContrato[] itemsContrato) {
		return buscarParametro(itemsContrato, Constants.COD_PARAM_OFIGESTOR);
	}

	private static String buscarProcedencia(ItemContrato[] itemsContrato) {
		return buscarParametro(itemsContrato, Constants.COD_PARAM_CODPROCEDEN);
	}
	
	
	

	private static String buscarParametro(ItemContrato[] itemsContrato,
			String param) {
		for (int i = 0; i < itemsContrato.length; i++) {
			if (param.equals(itemsContrato[i].getLabel())) {
				return itemsContrato[i].getValue();
			}
		}
		return null;
	}

	public static void processLineItem(String aLine, int index)
			throws Exception {
		if (aLine != null && aLine.length() > 0) {
			if (aLine.length() > (Constants.LABEL_LENGTH - 1)) {
				String label = aLine.substring(0, Constants.LABEL_LENGTH);
				String value = aLine.substring(Constants.LABEL_LENGTH);
				if (value != null && !value.equals("\\")) {
					value = StringUtils.replace(value, "\\", "");
					listaItems.add(Util.newIC(label, value));
				}
			} else {
				throw new Exception(
						"La linea debe tener longitud mayor de 5 caracteres. Linea "
								+ (index + 1));
			}
		} else {
			throw new Exception("La linea se encuentra vacia. Linea "
					+ (index + 1));
		}
	}

	private final String fFilePath;
	private FirmarContratosRequest obj;
	public static List listaItems;
	private static  String rutaSalida;
	private static  String nombreArchivoSalida;

	
}