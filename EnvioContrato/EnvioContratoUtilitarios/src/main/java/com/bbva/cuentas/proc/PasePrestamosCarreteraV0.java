package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PasePrestamosCarreteraV0 extends PaseGenerico{
	
	public static void run(){
		PasePrestamosCarreteraV0 proc = new PasePrestamosCarreteraV0();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("PRESTAMO DIGITAL");
			if(contarKITS>0){
				proc.revertirKits("PRESTAMO DIGITAL");
				proc.revertirPlantilla("PLT00711");
				proc.revertirPlantilla("PLT00105");
				proc.revertirPlantilla("PLT00106");
				proc.revertirPlantilla("PLT00107");
			}
			proc.ejecutarScriptsFfmmKit();
			proc.ejecutarScriptsFfmmPlantilla();
			proc.ejecutarScriptsFfmmEnvKit();
			proc.ejecutarScriptsFfmmKitPlantillaBase();
			//FORMATOS
			proc.actualizarFfmmHtml();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsFfmmEnvKit() throws IOException {
		for(String kit : consultarScriptKit("PRESTAMO DIGITAL")){
			ejecutarScriptsEnvKit(kit,"ZP","<C004> - PRÉSTAMO DIGITAL","A","STRTPL",false);
		}
	}
	
	public void ejecutarScriptsFfmmKit(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'PDESEM04',NULL,NULL,NULL,NULL,'I','A','KIT - PRESTAMO DIGITAL','A','A')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'PDESEM05',NULL,NULL,NULL,NULL,'I','A','KIT - PRESTAMO DIGITAL','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsFfmmPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','S','A','PLT00711','0000','Contrato de  Prestamo','G','Hoja de Resumen Informativa (HRI)',NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','S','A','PLT00105','0000','HRI','G','Hoja de Resumen Informativa (HRI)',NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','S','A','PLT00106','0000','Cronograma de prestamo','G','Cronograma de prestamo',NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','S','A','PLT00107','0000','Seguro de Desgravamen','G','Seguro de Desgravamen',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void ejecutarScriptsFfmmKitPlantillaBase(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PLT00711'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PLT00105' AND ST_CONDICION = 'V'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PLT00106'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PLT00107'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PE281' AND NB_DESCRIPCION ='PE281 - LPDP - CONTRATOS')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%PRESTAMO DIGITAL%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi01\\carreteraunica\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"html.html",rutaDestino+"html20190319.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("PDESEM04",null,rutaDestino+"html20190319.html"));
		kits.add(new Ejecucion("PDESEM05",null,rutaDestino+"html20190319.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
		
}
