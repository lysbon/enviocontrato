package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseFFMM4 extends PaseGenerico{
	
	public static void run(){
		PaseFFMM4 proc = new PaseFFMM4();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("E5006");
			if(contarKITS>0){
				proc.revertirKits("E5006");
				proc.revertirPlantilla("E5006");
			}
			proc.ejecutarScriptsFfmmKit();
			proc.ejecutarScriptsFfmmPlantilla();
			proc.ejecutarScriptsFfmmEnvKit();
			proc.ejecutarScriptsFfmmKitPlantillaBase();
			proc.ejecutarScriptsFfmmKitPlantillaProspecto();
			//FORMATOS
			proc.actualizarFfmmHtml();
			proc.actualizarFfmmPdf();
			
//			proc.ejecutarScriptsFfmmEnvKitParche();
//			proc.actualizarFfmmHtml();
//			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsFfmmEnvKit() throws IOException {
		for(String kit : consultarScriptKit("E5006")){
			ejecutarScriptsEnvKit(kit,"BXI","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
	}
	
	private void ejecutarScriptsFfmmEnvKitParche() throws IOException {
		for(String kit : consultarScriptKit("E5006")){
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
	}
		
	public void ejecutarScriptsFfmmKit(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'800301',NULL,NULL,NULL,NULL,'I','A','KIT FM011 E5006 OportEspSol4FMIV','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsFfmmPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','I','A','P','A','E5006','0000','Prospecto BBVA Oportunidad Soles 4 FMIV','G','Prospecto OportSol4FMIV',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void ejecutarScriptsFfmmKitPlantillaBase(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='FM011'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600' AND ST_CONDICION = 'V'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTA'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_R'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PE281' AND NB_DESCRIPCION ='PE281 - LPDP - CONTRATOS'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600_FATCA_FORM_AUTO_CERT_PN'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTAMEN_RBA_LINE'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_RBA')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%FM011 E5006%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsFfmmKitPlantillaProspecto(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,P.CD_PLANTILLA         "+
				"FROM   APDIGB.TBG010_KIT K,            "+
				"       APDIGB.TBG002_PLANTILLA P       "+
				"WHERE  K.NB_NOMBRE_KIT LIKE '%FM011 E5006%'"+
				"  AND  SUBSTR(K.NB_NOMBRE_KIT,11,5) = P.NB_NOMBRE_FORMATO"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20190306.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("800301",null,rutaDestino+"ffmm20190306.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
		
	public void actualizarFfmmPdf() throws InterruptedException, IOException{
		final File folder = new File("D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("E5006")
		};
		
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
		
}
