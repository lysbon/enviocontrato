package com.bbva.cuentas.firmarContratos;

public class FirmarContratosPortProxy implements com.bbva.cuentas.firmarContratos.FirmarContratosPort {
  private String _endpoint = null;
  private com.bbva.cuentas.firmarContratos.FirmarContratosPort firmarContratosPort = null;
  
  public FirmarContratosPortProxy() {
    _initFirmarContratosPortProxy();
  }
  
  public FirmarContratosPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initFirmarContratosPortProxy();
  }
  
  private void _initFirmarContratosPortProxy() {
    try {
      firmarContratosPort = (new com.bbva.cuentas.firmarContratos.FirmarContratosPortServiceLocator()).getFirmarContratosPortSoap11();
      if (firmarContratosPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)firmarContratosPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)firmarContratosPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (firmarContratosPort != null)
      ((javax.xml.rpc.Stub)firmarContratosPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.bbva.cuentas.firmarContratos.FirmarContratosPort getFirmarContratosPort() {
    if (firmarContratosPort == null)
      _initFirmarContratosPortProxy();
    return firmarContratosPort;
  }
  
  public com.bbva.cuentas.firmarContratos.ReenvioContratosResponse reenvioContratos(com.bbva.cuentas.firmarContratos.ReenvioContratosRequest reenvioContratosRequest) throws java.rmi.RemoteException{
    if (firmarContratosPort == null)
      _initFirmarContratosPortProxy();
    return firmarContratosPort.reenvioContratos(reenvioContratosRequest);
  }
  
  public com.bbva.cuentas.firmarContratos.ConfigureResponse configure(com.bbva.cuentas.firmarContratos.ConfigureRequest configureRequest) throws java.rmi.RemoteException{
    if (firmarContratosPort == null)
      _initFirmarContratosPortProxy();
    return firmarContratosPort.configure(configureRequest);
  }
  
  public com.bbva.cuentas.firmarContratos.FirmarContratosResponse firmarContratos(com.bbva.cuentas.firmarContratos.FirmarContratosRequest firmarContratosRequest) throws java.rmi.RemoteException{
    if (firmarContratosPort == null)
      _initFirmarContratosPortProxy();
    return firmarContratosPort.firmarContratos(firmarContratosRequest);
  }
  
  
}