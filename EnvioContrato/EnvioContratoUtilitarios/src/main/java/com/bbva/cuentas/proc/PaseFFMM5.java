package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseFFMM5 extends PaseGenerico{
	
	public static void run(){
		PaseFFMM5 proc = new PaseFFMM5();
		try {
			//SQL
			
			proc.ejecutarScriptsFfmmEnvKit();
			proc.actualizarFfmmHtml();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsFfmmEnvKit() throws IOException {
		for(String kit : consultarScriptKit("FM011")){
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
	}
		
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi01\\ffmm\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20190312.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("800011",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800031",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800061",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800121",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800131",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800141",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800241",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800401",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800421",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800431",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800022",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800042",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800052",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800152",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800202",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800212",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800232",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800252",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800262",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800282",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800312",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800322",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800332",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800342",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800382",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800392",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800352",null,rutaDestino+"ffmm20190312.html"));
		kits.add(new Ejecucion("800291",null,rutaDestino+"ffmm20190312.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
		
}
