package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseFFMM9_DJ extends PaseGenerico{
	
	public static void run(){
		PaseFFMM9_DJ proc = new PaseFFMM9_DJ();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("FMDJS");
			if(contarKITS>0){
				proc.revertirPlantilla("FMDJS");
			}
			proc.ejecutarScriptsFfmmPlantilla();
			proc.ejecutarScriptsFfmmKitPlantilla();
			//FORMATOS
			proc.actualizarFfmmHtml();
			proc.actualizarFfmmPdf();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsFfmmPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','V','A','A','P','A','FMDJS','0529','Declaracion Jurada para apertura de FFMM','C','Declaracion Jurada FFMM',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void ejecutarScriptsFfmmKitPlantilla(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='FMDJS')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%FM011%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarFfmmPdf() throws InterruptedException, IOException{
		final File folder = new File("D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm_dj\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = {
				util.plantilla("FMDJS")
		};
		
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
	
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm_dj\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20190506.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("800011",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800031",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800061",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800121",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800131",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800141",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800241",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800401",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800421",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800431",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800022",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800042",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800052",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800152",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800202",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800212",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800232",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800252",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800262",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800282",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800312",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800322",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800332",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800342",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800382",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800392",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800352",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800291",null,rutaDestino+"ffmm20190506.html"));
		kits.add(new Ejecucion("800301",null,rutaDestino+"ffmm20190506.html"));		
		ConfigureApp.actualizarArchivos("HTML",kits);
	}

	public void actualizarFfmmHtmlMulti(){
		String rutaOrigen  = "D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm_dj\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm_multi.html",rutaDestino+"ffmm20190506multi.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("FFMM",null,rutaDestino+"ffmm20190506multi.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
	
}
