package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseBiometriaFacial extends PaseGenerico{
	
	public static void run(){
		PaseBiometriaFacial proc = new PaseBiometriaFacial();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("BIOMETRICO");
			if(contarKITS>0){
				proc.revertirKits("BIOMETRICO");
				proc.revertirPlantillas("BIOMETRICO");
			}
			proc.ejecutarScriptsKit();
			proc.ejecutarScriptsPlantilla();
			proc.ejecutarScriptsEnvKit();
			proc.ejecutarScriptsKitPlantilla();
			proc.ejecutarScriptsSeccion();
			proc.ejecutarScriptsItems();
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsEnvKit() throws IOException {
		for(String kit : consultarScriptCodigoKit("DNIBIO")){
			ejecutarScriptsEnvKit(kit,"BXI","","A","STRTPL",false);
		}
	}
		
	public void ejecutarScriptsKit(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES "
				+ "(APDIGB.KIT_SEQ.NEXTVAL,'DNIBIO',NULL,NULL,NULL,NULL,'A','I','KIT DNI BIOMETRICO','I','I')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES "
				+ "("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','N','A','DNIBIO','0000','DNI BIOMETRICO','G','DNI BIOMETRICO',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsKitPlantilla() throws IOException{
		List<String> plantillas = new ArrayList<String>();
		plantillas.add("DNIBIO");
		ejecutarScriptsKitPlantilla("DNIBIO",plantillas);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsSeccion() throws IOException{
		
		Integer dniBio = consultarPlantillaPorFormato("DNIBIO");
		
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_SECCION)+1 NEWID FROM APDIGB.TBG003_SECCION"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("NEWID");
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+dniBio+",1)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsItems() throws IOException{
		List<Ejecucion> scriptsPage1 = new ArrayList<Ejecucion>();
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) VALUES"
				+ "([CDITEM],[CDSECCION],'A',104,745,'dniFrente',NULL,'null','0','J',360,360,195,NULL,NULL,NULL,NULL,NULL,NULL)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) VALUES"
				+ "([CDITEM],[CDSECCION],'A',104,445,'dniPosterior',NULL,'null','0','J',360,360,195,NULL,NULL,NULL,NULL,NULL,NULL)"));
		ejecutarScriptsItems("DNIBIO","1",scriptsPage1);
	}
		
}
