package com.bbva.cuentas.apemasivas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonToCSVCuentaMasiva {

	public static void main(String args[]){
		
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<List<CuentaMasiva>> typeReference = new TypeReference<List<CuentaMasiva>>(){};
		InputStream inputStream = TypeReference.class.getResourceAsStream("/json/responseutf.json");
		try {
			List<CuentaMasiva> cuentas = mapper.readValue(inputStream,typeReference);
			
			try (PrintWriter writer = new PrintWriter(new File("output.csv"))) {
				
				StringBuilder sb = new StringBuilder();
				sb.append("Registro de Ejecutivo");
				sb.append(",");
				sb.append("Email Ejecutivo");
				sb.append(",");
				sb.append("Nombre de la Empresa");
				sb.append(",");
				sb.append("RUC");
				sb.append(",");
				sb.append("Tipo Producto");
				sb.append(",");
				sb.append("DNI");
				sb.append(",");
				sb.append("Nombres y Apellidos");
				sb.append(",");
				sb.append("N° Cuenta");
				sb.append(",");
				sb.append("Moneda");
				sb.append(",");
				sb.append("Fecha Carga");
				sb.append(",");
				sb.append("Apertura");
				sb.append(",");
				sb.append("OF. A CENTRALIZAR");
				sb.append('\n');
				
				
				
				for(CuentaMasiva cuenta : cuentas) {
					sb.append(cuenta.getCodEjecutivo());
					sb.append(",");
					sb.append(StringUtils.replace(cuenta.getEmailEjecutivo(), ",",""));
					sb.append(",");
					sb.append(StringUtils.replace(cuenta.getNombreEmpresa(), ",",""));
					sb.append(",");
					sb.append(cuenta.getRuc());
					sb.append(",");
					sb.append(cuenta.getTipoCuenta());
					sb.append(",");
					sb.append(cuenta.getNumDocumento());
					sb.append(",");
					sb.append(StringUtils.replace(cuenta.getNombres(), ",",""));
					sb.append(",");
					sb.append(cuenta.getNumCuenta());
					sb.append(",");
					sb.append(cuenta.getMoneda());
					sb.append(",");
					sb.append(cuenta.getFechaRegistro());
					sb.append(",");
					sb.append(cuenta.getNominaCentralizada());
					sb.append(",");
					sb.append(cuenta.getOficina());
					sb.append('\n');
					
				}
				
				//C.COD_EJECUTIVO,C.EMAIL_EJECUTIVO,C.NOMBRE_EMPRESA,C.RUC,C.TIPO_CUENTA,
				//DC.NUM_DOCUMENTO,DC.NOMBRES,DC.NUM_CUENTA,
			    //C.MONEDA,C.FECHA_REGISTRO,
			    //C.NOMINA_CENTRALIZADA,C.OFICINA
				
				writer.write(sb.toString());
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			    System.out.println(e.getMessage());
		    }
			
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Unable to save users: " + e.getMessage());
		}
		
	}
	
}
