package com.bbva.cuentas.jasper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;

public class JasperExec {

	public static void main(String[] args) {
		
		Map<String,Object> parametros = new HashMap<String, Object>();
		parametros.put("HUELLA",null);
		
		parametros.put("importeDeposito","S/. 50,000");
		parametros.put("fechaHora",new Date().toString());
		parametros.put("titular","Juan Tipismana");
		
		//Registro Vacio
		Collection<Map<String,?>> contenido = new ArrayList<Map<String,?>>();
		contenido.add(new HashMap<String,Object>());
		JRMapCollectionDataSource datasource = new JRMapCollectionDataSource(contenido);
		JasperReport jasperReport;
		
		try {
			jasperReport = JasperCompileManager.compileReport("C:\\temp\\JP0002.jrxml");
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parametros, datasource);
			JasperExportManager.exportReportToPdfFile(print, "C:\\temp\\output.pdf");
		} catch (JRException e) {
			e.printStackTrace();
		}
		
	}
	
}
