package com.bbva.cuentas.apemasivas;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TokenServiceImpl{

	private String urlTokenService;
	private String clientId;
	private String clientSecret;
	
	public TokenServiceImpl() {
		urlTokenService = "http://118.180.47.11:8280/apertura-masiva-rest/oauth/token";
		clientId = "aperturaMasivaRest";
		clientSecret = "hu67fg84er67ub92kj47vw51op";
	}
	
	public RestTemplate getTemplate(){
		OAuth2RestTemplate template = null;		
		try {
			if(!StringUtils.isEmpty(urlTokenService)){
				ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
				resource.setClientId(clientId);
				resource.setClientSecret(clientSecret);
				resource.setGrantType("client_credentials");
				resource.setAccessTokenUri(urlTokenService);
				List<String> scopeList = new ArrayList<String>();
				scopeList.add("read");
				scopeList.add("write");
				scopeList.add("trust");
				resource.setScope(scopeList);
				template = new OAuth2RestTemplate(resource);
				System.out.println("OAuth2RestTemplate");
			}else{
				return new RestTemplate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return template;
	}
	
}
