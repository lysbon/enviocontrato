package com.bbva.cuentas.apemasivas;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ColectivoDelta {

	//EMPRESA,    RUC,    TCTA,    PRO,
    //FEC_CARGA,    FEC_HORA_CRGA,    FEC_HORA_RPTA,    DELTA,
    //MONEDA,    EC,    ED,
	
	@JsonProperty("EMPRESA")
	private String empresa;
	
	@JsonProperty("RUC")
	private String ruc;
	
	@JsonProperty("TCTA")
	private String tipoCarga;
	
	@JsonProperty("PRO")
	private String codigoProducto;
	
	@JsonProperty("FEC_CARGA")
	private String fechaCarga;
	
	@JsonProperty("FEC_HORA_CRGA")
	private String fechaHoraCarga;
	
	@JsonProperty("FEC_HORA_RPTA")
	private String fechaHoraRespuesta;
	
	@JsonProperty("DELTA")
	private String delta;
	
	@JsonProperty("MONEDA")
	private String moneda;
	
	@JsonProperty("EC")
	private String estadoColectivo;
	
	@JsonProperty("ED")
	private String estadoDetalle;

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getFechaHoraCarga() {
		return fechaHoraCarga;
	}

	public void setFechaHoraCarga(String fechaHoraCarga) {
		this.fechaHoraCarga = fechaHoraCarga;
	}

	public String getFechaHoraRespuesta() {
		return fechaHoraRespuesta;
	}

	public void setFechaHoraRespuesta(String fechaHoraRespuesta) {
		this.fechaHoraRespuesta = fechaHoraRespuesta;
	}

	public String getDelta() {
		return delta;
	}

	public void setDelta(String delta) {
		this.delta = delta;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getEstadoColectivo() {
		return estadoColectivo;
	}

	public void setEstadoColectivo(String estadoColectivo) {
		this.estadoColectivo = estadoColectivo;
	}

	public String getEstadoDetalle() {
		return estadoDetalle;
	}

	public void setEstadoDetalle(String estadoDetalle) {
		this.estadoDetalle = estadoDetalle;
	}
	
}
