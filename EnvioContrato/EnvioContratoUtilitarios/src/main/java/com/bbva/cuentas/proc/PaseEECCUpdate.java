package com.bbva.cuentas.proc;

import java.util.ArrayList;
import java.util.List;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseEECCUpdate extends PaseGenerico{

	public static void run(){
		PaseEECCUpdate proc = new PaseEECCUpdate();
		try {
			
			proc.actualizarFormato();
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void actualizarFormato() {
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\eecc\\";
		String rutaDestino = "/mnt/compartido/apdig/formatos/jasper/";
		List<Ejecucion> plantillaJRXML = new ArrayList<Ejecucion>();
		plantillaJRXML.add(new Ejecucion(rutaOrigen+"JP0001_20181204.jrxml",rutaDestino+"JP0001.jrxml"));
		ConfigureApp.cargarArchivos(plantillaJRXML,false);
	}
}
