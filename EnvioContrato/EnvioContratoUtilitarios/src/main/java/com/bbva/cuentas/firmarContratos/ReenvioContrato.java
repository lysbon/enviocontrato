/**
 * ReenvioContrato.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class ReenvioContrato  implements java.io.Serializable {
    private java.lang.String clienteCodCentral;

    private java.lang.String clienteTipoDoc;

    private java.lang.String clienteNroDoc;

    private java.lang.String clienteEmail;

    private java.lang.String clienteNombre;

    private java.lang.String numeroContrato;

    private java.lang.String oficinaGestora;

    private java.lang.String procedencia;

    private java.lang.String fechaApertura;

    private java.lang.String codProducto;

    private java.lang.String subProducto;

    private java.lang.String codDivisa;

    private java.lang.String ctaInterBancaria;

    private java.lang.Object idContrato;  // attribute

    public ReenvioContrato() {
    }

    public ReenvioContrato(
           java.lang.String clienteCodCentral,
           java.lang.String clienteTipoDoc,
           java.lang.String clienteNroDoc,
           java.lang.String clienteEmail,
           java.lang.String clienteNombre,
           java.lang.String numeroContrato,
           java.lang.String oficinaGestora,
           java.lang.String procedencia,
           java.lang.String fechaApertura,
           java.lang.String codProducto,
           java.lang.String subProducto,
           java.lang.String codDivisa,
           java.lang.String ctaInterBancaria,
           java.lang.Object idContrato) {
           this.clienteCodCentral = clienteCodCentral;
           this.clienteTipoDoc = clienteTipoDoc;
           this.clienteNroDoc = clienteNroDoc;
           this.clienteEmail = clienteEmail;
           this.clienteNombre = clienteNombre;
           this.numeroContrato = numeroContrato;
           this.oficinaGestora = oficinaGestora;
           this.procedencia = procedencia;
           this.fechaApertura = fechaApertura;
           this.codProducto = codProducto;
           this.subProducto = subProducto;
           this.codDivisa = codDivisa;
           this.ctaInterBancaria = ctaInterBancaria;
           this.idContrato = idContrato;
    }


    /**
     * Gets the clienteCodCentral value for this ReenvioContrato.
     * 
     * @return clienteCodCentral
     */
    public java.lang.String getClienteCodCentral() {
        return clienteCodCentral;
    }


    /**
     * Sets the clienteCodCentral value for this ReenvioContrato.
     * 
     * @param clienteCodCentral
     */
    public void setClienteCodCentral(java.lang.String clienteCodCentral) {
        this.clienteCodCentral = clienteCodCentral;
    }


    /**
     * Gets the clienteTipoDoc value for this ReenvioContrato.
     * 
     * @return clienteTipoDoc
     */
    public java.lang.String getClienteTipoDoc() {
        return clienteTipoDoc;
    }


    /**
     * Sets the clienteTipoDoc value for this ReenvioContrato.
     * 
     * @param clienteTipoDoc
     */
    public void setClienteTipoDoc(java.lang.String clienteTipoDoc) {
        this.clienteTipoDoc = clienteTipoDoc;
    }


    /**
     * Gets the clienteNroDoc value for this ReenvioContrato.
     * 
     * @return clienteNroDoc
     */
    public java.lang.String getClienteNroDoc() {
        return clienteNroDoc;
    }


    /**
     * Sets the clienteNroDoc value for this ReenvioContrato.
     * 
     * @param clienteNroDoc
     */
    public void setClienteNroDoc(java.lang.String clienteNroDoc) {
        this.clienteNroDoc = clienteNroDoc;
    }


    /**
     * Gets the clienteEmail value for this ReenvioContrato.
     * 
     * @return clienteEmail
     */
    public java.lang.String getClienteEmail() {
        return clienteEmail;
    }


    /**
     * Sets the clienteEmail value for this ReenvioContrato.
     * 
     * @param clienteEmail
     */
    public void setClienteEmail(java.lang.String clienteEmail) {
        this.clienteEmail = clienteEmail;
    }


    /**
     * Gets the clienteNombre value for this ReenvioContrato.
     * 
     * @return clienteNombre
     */
    public java.lang.String getClienteNombre() {
        return clienteNombre;
    }


    /**
     * Sets the clienteNombre value for this ReenvioContrato.
     * 
     * @param clienteNombre
     */
    public void setClienteNombre(java.lang.String clienteNombre) {
        this.clienteNombre = clienteNombre;
    }


    /**
     * Gets the numeroContrato value for this ReenvioContrato.
     * 
     * @return numeroContrato
     */
    public java.lang.String getNumeroContrato() {
        return numeroContrato;
    }


    /**
     * Sets the numeroContrato value for this ReenvioContrato.
     * 
     * @param numeroContrato
     */
    public void setNumeroContrato(java.lang.String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }


    /**
     * Gets the oficinaGestora value for this ReenvioContrato.
     * 
     * @return oficinaGestora
     */
    public java.lang.String getOficinaGestora() {
        return oficinaGestora;
    }


    /**
     * Sets the oficinaGestora value for this ReenvioContrato.
     * 
     * @param oficinaGestora
     */
    public void setOficinaGestora(java.lang.String oficinaGestora) {
        this.oficinaGestora = oficinaGestora;
    }


    /**
     * Gets the procedencia value for this ReenvioContrato.
     * 
     * @return procedencia
     */
    public java.lang.String getProcedencia() {
        return procedencia;
    }


    /**
     * Sets the procedencia value for this ReenvioContrato.
     * 
     * @param procedencia
     */
    public void setProcedencia(java.lang.String procedencia) {
        this.procedencia = procedencia;
    }


    /**
     * Gets the fechaApertura value for this ReenvioContrato.
     * 
     * @return fechaApertura
     */
    public java.lang.String getFechaApertura() {
        return fechaApertura;
    }


    /**
     * Sets the fechaApertura value for this ReenvioContrato.
     * 
     * @param fechaApertura
     */
    public void setFechaApertura(java.lang.String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }


    /**
     * Gets the codProducto value for this ReenvioContrato.
     * 
     * @return codProducto
     */
    public java.lang.String getCodProducto() {
        return codProducto;
    }


    /**
     * Sets the codProducto value for this ReenvioContrato.
     * 
     * @param codProducto
     */
    public void setCodProducto(java.lang.String codProducto) {
        this.codProducto = codProducto;
    }


    /**
     * Gets the subProducto value for this ReenvioContrato.
     * 
     * @return subProducto
     */
    public java.lang.String getSubProducto() {
        return subProducto;
    }


    /**
     * Sets the subProducto value for this ReenvioContrato.
     * 
     * @param subProducto
     */
    public void setSubProducto(java.lang.String subProducto) {
        this.subProducto = subProducto;
    }


    /**
     * Gets the codDivisa value for this ReenvioContrato.
     * 
     * @return codDivisa
     */
    public java.lang.String getCodDivisa() {
        return codDivisa;
    }


    /**
     * Sets the codDivisa value for this ReenvioContrato.
     * 
     * @param codDivisa
     */
    public void setCodDivisa(java.lang.String codDivisa) {
        this.codDivisa = codDivisa;
    }


    /**
     * Gets the ctaInterBancaria value for this ReenvioContrato.
     * 
     * @return ctaInterBancaria
     */
    public java.lang.String getCtaInterBancaria() {
        return ctaInterBancaria;
    }


    /**
     * Sets the ctaInterBancaria value for this ReenvioContrato.
     * 
     * @param ctaInterBancaria
     */
    public void setCtaInterBancaria(java.lang.String ctaInterBancaria) {
        this.ctaInterBancaria = ctaInterBancaria;
    }


    /**
     * Gets the idContrato value for this ReenvioContrato.
     * 
     * @return idContrato
     */
    public java.lang.Object getIdContrato() {
        return idContrato;
    }


    /**
     * Sets the idContrato value for this ReenvioContrato.
     * 
     * @param idContrato
     */
    public void setIdContrato(java.lang.Object idContrato) {
        this.idContrato = idContrato;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReenvioContrato)) return false;
        ReenvioContrato other = (ReenvioContrato) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.clienteCodCentral==null && other.getClienteCodCentral()==null) || 
             (this.clienteCodCentral!=null &&
              this.clienteCodCentral.equals(other.getClienteCodCentral()))) &&
            ((this.clienteTipoDoc==null && other.getClienteTipoDoc()==null) || 
             (this.clienteTipoDoc!=null &&
              this.clienteTipoDoc.equals(other.getClienteTipoDoc()))) &&
            ((this.clienteNroDoc==null && other.getClienteNroDoc()==null) || 
             (this.clienteNroDoc!=null &&
              this.clienteNroDoc.equals(other.getClienteNroDoc()))) &&
            ((this.clienteEmail==null && other.getClienteEmail()==null) || 
             (this.clienteEmail!=null &&
              this.clienteEmail.equals(other.getClienteEmail()))) &&
            ((this.clienteNombre==null && other.getClienteNombre()==null) || 
             (this.clienteNombre!=null &&
              this.clienteNombre.equals(other.getClienteNombre()))) &&
            ((this.numeroContrato==null && other.getNumeroContrato()==null) || 
             (this.numeroContrato!=null &&
              this.numeroContrato.equals(other.getNumeroContrato()))) &&
            ((this.oficinaGestora==null && other.getOficinaGestora()==null) || 
             (this.oficinaGestora!=null &&
              this.oficinaGestora.equals(other.getOficinaGestora()))) &&
            ((this.procedencia==null && other.getProcedencia()==null) || 
             (this.procedencia!=null &&
              this.procedencia.equals(other.getProcedencia()))) &&
            ((this.fechaApertura==null && other.getFechaApertura()==null) || 
             (this.fechaApertura!=null &&
              this.fechaApertura.equals(other.getFechaApertura()))) &&
            ((this.codProducto==null && other.getCodProducto()==null) || 
             (this.codProducto!=null &&
              this.codProducto.equals(other.getCodProducto()))) &&
            ((this.subProducto==null && other.getSubProducto()==null) || 
             (this.subProducto!=null &&
              this.subProducto.equals(other.getSubProducto()))) &&
            ((this.codDivisa==null && other.getCodDivisa()==null) || 
             (this.codDivisa!=null &&
              this.codDivisa.equals(other.getCodDivisa()))) &&
            ((this.ctaInterBancaria==null && other.getCtaInterBancaria()==null) || 
             (this.ctaInterBancaria!=null &&
              this.ctaInterBancaria.equals(other.getCtaInterBancaria()))) &&
            ((this.idContrato==null && other.getIdContrato()==null) || 
             (this.idContrato!=null &&
              this.idContrato.equals(other.getIdContrato())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getClienteCodCentral() != null) {
            _hashCode += getClienteCodCentral().hashCode();
        }
        if (getClienteTipoDoc() != null) {
            _hashCode += getClienteTipoDoc().hashCode();
        }
        if (getClienteNroDoc() != null) {
            _hashCode += getClienteNroDoc().hashCode();
        }
        if (getClienteEmail() != null) {
            _hashCode += getClienteEmail().hashCode();
        }
        if (getClienteNombre() != null) {
            _hashCode += getClienteNombre().hashCode();
        }
        if (getNumeroContrato() != null) {
            _hashCode += getNumeroContrato().hashCode();
        }
        if (getOficinaGestora() != null) {
            _hashCode += getOficinaGestora().hashCode();
        }
        if (getProcedencia() != null) {
            _hashCode += getProcedencia().hashCode();
        }
        if (getFechaApertura() != null) {
            _hashCode += getFechaApertura().hashCode();
        }
        if (getCodProducto() != null) {
            _hashCode += getCodProducto().hashCode();
        }
        if (getSubProducto() != null) {
            _hashCode += getSubProducto().hashCode();
        }
        if (getCodDivisa() != null) {
            _hashCode += getCodDivisa().hashCode();
        }
        if (getCtaInterBancaria() != null) {
            _hashCode += getCtaInterBancaria().hashCode();
        }
        if (getIdContrato() != null) {
            _hashCode += getIdContrato().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReenvioContrato.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "reenvioContrato"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("idContrato");
        attrField.setXmlName(new javax.xml.namespace.QName("", "idContrato"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clienteCodCentral");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "clienteCodCentral"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clienteTipoDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "clienteTipoDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clienteNroDoc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "clienteNroDoc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clienteEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "clienteEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clienteNombre");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "clienteNombre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroContrato");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "numeroContrato"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oficinaGestora");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "oficinaGestora"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("procedencia");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "procedencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fechaApertura");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "fechaApertura"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "codProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subProducto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "subProducto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codDivisa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "codDivisa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ctaInterBancaria");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "ctaInterBancaria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
