package com.bbva.cuentas.apemasivas;

import java.util.HashMap;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.util.Result;

@Service
public class RecibirServiceImpl{

	private String urlServices;

	private TokenServiceImpl tokenService;
	
	public RecibirServiceImpl() {
		tokenService = new TokenServiceImpl(); 
		urlServices = "http://118.180.47.11:8280/apertura-masiva-rest";
	}
	
	public void recibir(String folder){
		Result res = new Result();
		try {
			final String uri = urlServices+"/rest/recibir?foldername="+folder;
		    Map<String, Object> params = new HashMap<String, Object>();
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<String> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, params);
		    res.setEntity(grupoResponse.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
}
