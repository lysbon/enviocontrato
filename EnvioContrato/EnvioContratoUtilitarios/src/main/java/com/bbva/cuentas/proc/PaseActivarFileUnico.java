package com.bbva.cuentas.proc;

import java.util.ArrayList;
import java.util.List;


import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseActivarFileUnico extends PaseGenerico{
	
	public static void run(){
		PaseActivarFileUnico proc = new PaseActivarFileUnico();
		try {
			
//			proc.actualizarFileUnicoDepPlazo();
//			proc.actualizarFileUnicoFFMMNuevos();
			proc.actualizarFileUnicoFFMMMultiproducto();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		
	public void actualizarFileUnicoDepPlazo(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("UPDATE APDIGB.TBG010_KIT SET ST_FILEUNICO = 'A' WHERE  NB_NOMBRE_KIT LIKE '%BG031%'"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarFileUnicoFFMMNuevos(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("UPDATE APDIGB.TBG010_KIT SET ST_FILEUNICO = 'A' WHERE CD_CODIGO IN ('800352','800291')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarFileUnicoFFMMMultiproducto(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("UPDATE APDIGB.TBG010_KIT SET ST_FILEUNICO = 'A' WHERE CD_CODIGO IN ('FFMM')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
}
