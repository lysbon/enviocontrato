package com.bbva.cuentas.apemasivas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.proc.PaseGenerico;

public class Sincronizar  extends PaseGenerico{
	
	public static void main(String args[]){
		Sincronizar proc = new Sincronizar();
		try {
			ConfigureApp.setEnv("PRD");
			String nombre = proc.obtenerNombre();
			String carpetaDestino = "C:\\log\\";
			//nombre = "ALTAMASI_20190221_1855";
			File fileOld = ConfigureApp.descargarArchivo(
					"/mnt/xcomntip/OF_0408/"+nombre+".TXT",
					carpetaDestino, "_old.");
			File fileNew = ConfigureApp.descargarArchivo(
					"/mnt/compartido/altasmasivas/input/"+nombre+".txt",
					carpetaDestino, "_new.");
			
			if(fileNew!=null && fileNew.exists()) {
				File file = consolidarArchivos(
						fileOld,fileNew,
						carpetaDestino+nombre+"_merge.txt");
				if(file!=null && file.exists()) {
					String nameOld = "/mnt/xcomntip/OF_0408/"+nombre+".TXT";
					proc.desplegarArchivo(file,nameOld);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static File consolidarArchivos(File fileOld, File fileNew, String dest) throws IOException {
		FileInputStream fis = new FileInputStream(fileOld);
		BufferedReader in = new BufferedReader(new InputStreamReader(fis));
		FileInputStream fis2 = new FileInputStream(fileNew);
		BufferedReader in2 = new BufferedReader(new InputStreamReader(fis2));
		
		FileWriter fstream = new FileWriter(dest, true);
		BufferedWriter out = new BufferedWriter(fstream);
 
		String aLine = null;
		while ((aLine = in.readLine()) != null) {
			out.write(aLine);
			out.newLine();
		}
		while ((aLine = in2.readLine()) != null) {
			out.write(aLine);
			out.newLine();
		}
 
		in.close();
 
		out.close();
		
		return new File(dest);
	}

	public String obtenerNombre() {
		SimpleDateFormat sdfday = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfHH  = new SimpleDateFormat("HH");
		SimpleDateFormat sdfmm  = new SimpleDateFormat("mm");
		Integer hora = new Integer(sdfHH.format(new Date()));
		Integer min  = new Integer(sdfmm.format(new Date()));
		String fecha  = sdfday.format(new Date());
		
		String smin = "";
		if(min<5) {
			smin="55";
			hora--;
		}else if(min<35 && min>25){
			smin="25";
		}
		String shora = String.format("%02d",hora);
		String salida = "ALTAMASI_"+fecha+"_"+shora+smin;
		return salida;
	}
	
	public void desplegarArchivo(File origen, String nombreDestino){
		List<Ejecucion> integrado = new ArrayList<Ejecucion>();
		integrado.add(new Ejecucion(origen.getAbsolutePath(),nombreDestino));
		ConfigureApp.cargarArchivos(integrado,false);
	}
	
}
