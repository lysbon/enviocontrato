package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseFFMM6 extends PaseGenerico{
	
	public static void run(){
		PaseFFMM6 proc = new PaseFFMM6();
		try {
			//SQL
			proc.actualizarFfmmPdf();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actualizarFfmmPdf() throws InterruptedException, IOException{
		final File folder = new File("D:\\opt\\workspace\\Proyectos\\2019\\pi01\\ffmm_fm011\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("FM011")
		};
		
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
		
}
