package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseGenerico {

	protected void ejecutarScriptsEnvKit(
			String cdKit,String canal,String asunto,String envCorreo,String engine,boolean tieneCopia) {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		
		String codCanal = "";
		String remitente = "";
		String remitenteCC = "";
		
		if(StringUtils.equals("BXI",canal)){
			codCanal = "2";
			remitente="BBVA Continental <postmaster@bbvacontinental.pe>";
			remitenteCC="apertura_digital@bbva.com";
		}else if(StringUtils.equals("BM",canal)){
			codCanal = "3";
			remitente="BBVA Continental <postmaster@bbvacontinental.pe>";
			remitenteCC="apertura_digital@bbva.com";
		}else if(StringUtils.equals("NACAR",canal)){
			codCanal = "1";
			remitente="ahorroaltoque@bbva.com";
			remitenteCC="ahorroaltoque@bbva.com";
		}else if(StringUtils.equals("PIC",canal)){
			codCanal = "4";
			remitente="ahorroaltoque@bbva.com";
			remitenteCC="ahorroaltoque@bbva.com";
		}else if(StringUtils.equals("ZP",canal)){
			codCanal = "6";
			remitente="BBVA Continental <postmaster@bbvacontinental.pe>";
			remitenteCC="apertura_digital@bbva.com";
		}
		if(tieneCopia) {
			remitenteCC="";
		}
		
		if(StringUtils.isEmpty(remitenteCC)) {
			scripts.add(new Ejecucion(
					"INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT,NB_CORREO_ASUNTO,LB_CORREO_PLANTILLA,ST_ENV_CORREO,NB_CORREO_ENGINE,NB_REMITENTE,NB_REMITENTE_CC,FH_MODIFICACION) VALUES "
					+ "("+codCanal+","+cdKit+",'"+asunto+"',NULL,'"+envCorreo+"','"+engine+"','"+remitente+"',NULL,SYSDATE)"));
		}else {
			scripts.add(new Ejecucion(
					"INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT,NB_CORREO_ASUNTO,LB_CORREO_PLANTILLA,ST_ENV_CORREO,NB_CORREO_ENGINE,NB_REMITENTE,NB_REMITENTE_CC,FH_MODIFICACION) VALUES "
					+ "("+codCanal+","+cdKit+",'"+asunto+"',NULL,'"+envCorreo+"','"+engine+"','"+remitente+"','"+remitenteCC+"',SYSDATE)"));
		}
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	protected List<Ejecucion> cargaCarpeta(String rutaOrigen, String extension) {
		final File folder = new File(rutaOrigen);
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> carga = new ArrayList<Ejecucion>();
		List<File> listFiles = Utilitarios.listFilesForFolder(folder);
		Date fecha = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		for(File file : listFiles) {
			String rutaCompletaOrigen  = rutaOrigen+FilenameUtils.getName(file.getAbsolutePath());
			String rutaCompletaDestino = rutaDestino+FilenameUtils.getBaseName(file.getAbsolutePath())+"_"+sdf.format(fecha)+"."+extension;
			carga.add(new Ejecucion(rutaCompletaOrigen,rutaCompletaDestino));
//			System.out.println(rutaCompletaOrigen);
//			System.out.println(rutaCompletaDestino);
		}
		ConfigureApp.cargarArchivos(carga,false);
		return carga;
	}
	
	protected void actualizarArchivos(List<Ejecucion> archivosCargados,String extension) {
		List<Ejecucion> archivosActualizar = new ArrayList<Ejecucion>();
		for(Ejecucion ejecucion: archivosCargados) {
			String codigo = FilenameUtils.getBaseName(ejecucion.getNombreOrigen());
			archivosActualizar.add(new Ejecucion(codigo,null,ejecucion.getNombreDestino()));
		}
		ConfigureApp.actualizarArchivos(extension.toUpperCase(),archivosActualizar);
	}
	
	@SuppressWarnings("unchecked")
	protected Integer consultarScriptCountKit(String codigoKit) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT COUNT(CD_KIT) CANT FROM APDIGB.TBG010_KIT WHERE NB_NOMBRE_KIT LIKE '%"+codigoKit+"%'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return (Integer)dataList[0].get("CANT");
	}
	
	@SuppressWarnings("unchecked")
	protected List<Integer> consultarScriptKitPorProducto(String producto) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT * FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '"+producto+"%' AND LENGTH(CD_CODIGO)=6"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<Integer> listaKits = new ArrayList<Integer>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add((Integer)obj.get("CD_KIT"));
		}
		return listaKits;
	}
	
	@SuppressWarnings("unchecked")
	protected List<String> consultarScriptKit(String nombreKit) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE NB_NOMBRE_KIT LIKE '%"+nombreKit+"%'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<String> listaKits = new ArrayList<String>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add(obj.get("CD_KIT").toString());
		}
		return listaKits;
	}
	
	@SuppressWarnings("unchecked")
	protected List<String> consultarScriptCodigoKit(String codigoKit) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO LIKE '%"+codigoKit+"%'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<String> listaKits = new ArrayList<String>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add(obj.get("CD_KIT").toString());
		}
		return listaKits;
	}
	
	@SuppressWarnings("unchecked")
	protected List<String> consultarScriptEnv() throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_ENV FROM APDIGB.TBG009_ENV "));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<String> listaKits = new ArrayList<String>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add(obj.get("CD_ENV").toString());
		}
		return listaKits;
	}
	
	
	
	@SuppressWarnings("unchecked")
	protected List<String> consultarScriptPlantilla(String descripcion) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_DESCRIPCION LIKE '%"+descripcion+"%'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<String> listaKits = new ArrayList<String>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add(obj.get("CD_PLANTILLA").toString());
		}
		return listaKits;		
	}
	
	@SuppressWarnings("unchecked")
	public Integer consultarFlagPorParametro(String parametro,String valor) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_FLAG FROM APDIGB.TBG005_FLAG WHERE NB_CONDICION_NOMBRE='"+parametro+"' AND NB_CONDICION_VALOR='"+valor+"'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return (Integer)dataList[0].get("CD_FLAG");
	}
	
	@SuppressWarnings("unchecked")
	public Integer consultarPlantillaPorFormato(String codigoPlantilla) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = '"+codigoPlantilla+"'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return (Integer)dataList[0].get("CD_PLANTILLA");
	}
	
	@SuppressWarnings("unchecked")
	protected Plantilla[] consultarScriptPlantillas(String codPlantilla) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO FROM APDIGB.TBG002_PLANTILLA WHERE NB_DESCRIPCION LIKE '%"+codPlantilla+"%'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		Plantilla[] plantillas = new Plantilla[dataList.length];
		for(int i=0; i<dataList.length;i++){
			HashMap<String,Object> obj = dataList[i];
			Plantilla plantilla = new Plantilla();
			plantilla.setCdPlantilla(new Long(obj.get("CD_PLANTILLA").toString()));
			plantilla.setNbNombreFormato(obj.get("NB_NOMBRE_FORMATO").toString());
			plantillas[i] = plantilla;
		}
		return plantillas;		
	}
	
	protected void revertirKits(String codKit) throws JsonParseException, JsonMappingException, IOException{
		List<String> kits = consultarScriptKit(codKit);
		for(String kit : kits){
			revertirKIT(kit);
		}
	}
	
	protected void revertirPlantillas(String descripcion) throws IOException{
		List<String> plantillas = consultarScriptPlantilla(descripcion);
		if(plantillas==null) return;
		for(String plantilla : plantillas){
			Integer cdPlantilla = new Integer(plantilla);
			revertirPlantilla(cdPlantilla);
		}
	}
	
	protected void revertirPlantilla(String codigo) throws IOException{
		Integer cdPlantilla = consultarPlantillaPorFormato(codigo);
		revertirPlantilla(cdPlantilla);
	}
	
	protected void revertirPlantillaContenido(String codigo) throws IOException{
		Integer cdPlantilla = consultarPlantillaPorFormato(codigo);
		revertirPlantillaContenido(cdPlantilla);
	}
	
	protected void revertirKIT(String codKit){
		if(!StringUtils.isEmpty(codKit)) {
			List<Ejecucion> scripts = new ArrayList<Ejecucion>();
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG012_ENV_KIT WHERE CD_KIT = "+codKit));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG011_KIT_PLANTILLA WHERE CD_KIT = "+codKit));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG010_KIT WHERE CD_KIT = "+codKit));
			ConfigureApp.ejecutarSQL(scripts);
		}
	}
	
	protected void revertirPlantilla(Integer codPlantilla){
		if(codPlantilla!=null) {
			List<Ejecucion> scripts = new ArrayList<Ejecucion>();
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG004_ITEM WHERE CD_SECCION IN (SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codPlantilla+")"));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codPlantilla));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG011_KIT_PLANTILLA WHERE CD_PLANTILLA = "+codPlantilla));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG006_FLAG_PLANTILLA WHERE CD_PLANTILLA = "+codPlantilla));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG002_PLANTILLA WHERE CD_PLANTILLA = "+codPlantilla));
			ConfigureApp.ejecutarSQL(scripts);
		}
	}
	
	protected void revertirPlantillaContenido(Integer codPlantilla){
		if(codPlantilla!=null) {
			List<Ejecucion> scripts = new ArrayList<Ejecucion>();
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG004_ITEM WHERE CD_SECCION IN (SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codPlantilla+")"));
			scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codPlantilla));
			ConfigureApp.ejecutarSQL(scripts);
		}
	}
	
	protected void revertirFlag(String parametro,String valor) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		Integer cdFlag = consultarFlagPorParametro(parametro, valor);
		scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG006_FLAG_PLANTILLA WHERE CD_FLAG = "+cdFlag));
		scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG005_FLAG WHERE CD_FLAG = "+cdFlag));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	protected void asegurarSecuenciaKit() throws Exception{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_KIT)+1 NEWID FROM APDIGB.TBG010_KIT"));
		String cdItem = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdItem, HashMap[].class);
		Integer cdItemIntMax = (Integer)dataList[0].get("NEWID");
		System.out.println("cdItemIntMax "+cdItemIntMax);
		query.add(new Ejecucion("SELECT APDIGB.KIT_SEQ.NEXTVAL NEWID FROM DUAL"));
		String cdItemSeq = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdItemSeq, HashMap[].class);
		Integer cdItemIntSeq = (Integer)dataList[0].get("NEWID");
		System.out.println("cdItemIntSeq "+cdItemIntSeq);
		if(cdItemIntSeq.intValue()<cdItemIntMax.intValue()){
			throw new Exception("Se cancela. Indices desincronizados.");
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void ejecutarScriptsItems(String codPlantilla, String nuPagina,List<Ejecucion> scripts) throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_ITEM)+1 NEWID FROM APDIGB.TBG004_ITEM"));
		String cdItem = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdItem, HashMap[].class);
		Integer cdItemInt = (Integer)dataList[0].get("NEWID");
		
		Integer cdPlantilla = consultarPlantillaPorFormato(codPlantilla);
		
		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+cdPlantilla+" AND NU_PAGINA = "+nuPagina));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("CD_SECCION");
		
		for(Ejecucion script : scripts){
			script.setScript(StringUtils.replace(script.getScript(), "[CDSECCION]", cdSeccionInt.toString()));
			script.setScript(StringUtils.replace(script.getScript(), "[CDITEM]", cdItemInt.toString()));
			cdItemInt++;
		}
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	protected void actualizarFileUnico(String nombreKit,String estadoFileUnico){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("UPDATE APDIGB.TBG010_KIT K SET ST_FILEUNICO='"+estadoFileUnico+"' WHERE K.NB_NOMBRE_KIT LIKE '%"+nombreKit+"%'"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	protected void ejecutarScriptsKitPlantilla(String codigoKit,List<String> plantillas) throws IOException{
		List<String> codigosKit = consultarScriptCodigoKit(codigoKit);
		String codigoKitFinal = codigosKit.get(0);
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		for(String nombreFormato : plantillas){
			Integer cdPlantilla = consultarPlantillaPorFormato(nombreFormato);
			if (cdPlantilla!=null) {
				scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT, CD_PLANTILLA, CD_USU_CREA, CD_USU_MODI, FH_CREACION, FH_MODIFICACION) VALUES("+codigoKitFinal+", "+cdPlantilla+", NULL, NULL, SYSDATE, SYSDATE)"));
			}
		}
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	protected void ejecutarScriptsKitPlantilla(Integer cdKit,Integer cdPlantilla) throws IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		if (cdPlantilla!=null) {
			scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT, CD_PLANTILLA, CD_USU_CREA, CD_USU_MODI, FH_CREACION, FH_MODIFICACION) VALUES("+cdKit+", "+cdPlantilla+", NULL, NULL, SYSDATE, SYSDATE)"));
			ConfigureApp.ejecutarSQL(scripts);
		}
	}
	
	protected void bindFlag(Integer cdPlantilla, String condicion,String valor) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG005_FLAG VALUES(APDIGB.FLG_SEQ.NEXTVAL,'"+condicion+"','"+valor+"',1,NULL,NULL,NULL,NULL,'T')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG005_FLAG VALUES(APDIGB.FLG_SEQ.NEXTVAL,'"+condicion+"','"+valor+"',1,NULL,NULL,NULL,NULL,'E')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG006_FLAG_PLANTILLA VALUES()"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
}
