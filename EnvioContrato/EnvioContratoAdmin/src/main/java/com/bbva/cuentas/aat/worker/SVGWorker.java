package com.bbva.cuentas.aat.worker;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.dto.ItemStampDTO;
import com.bbva.cuentas.aat.util.SystemUnit;
import com.bbva.cuentas.bean.ItemStamp;

@Component
public interface SVGWorker {
	
	public List<ItemStampDTO> procesarSVG(Long cdSeccion,String pathFile);

	public void generarSVG(String fileOriginal,String fileSalida, List<ItemStamp> items);
		
	public void toSystemUnit(String fileOriginal, String fileSalida,SystemUnit from, SystemUnit to);
}
