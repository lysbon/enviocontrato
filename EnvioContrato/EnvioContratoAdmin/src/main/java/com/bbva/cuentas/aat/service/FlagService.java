package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.util.Result;

public interface FlagService {
	
	public Result obtenerFlagPorCodigo(Long id) throws ServiceException;

	public Result obtenerFlagsPorTipo(String tipo) throws ServiceException;
	
	public Result obtenerFlags() throws ServiceException;
	
	public Result guardarFlag(Flag flag) throws ServiceException;
	
	public Result actualizarFlag(Flag flag) throws ServiceException;
	
	public Result eliminarFlag(Long id) throws ServiceException;
	
}
