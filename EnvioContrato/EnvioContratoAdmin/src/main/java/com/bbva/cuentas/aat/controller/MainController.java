package com.bbva.cuentas.aat.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;

@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	@Autowired
	private EnviromentService envService;
		
	@SuppressWarnings("unchecked")
	public void init(ModelAndView mav){
		List<Enviroment> envs;
		try {
			Result res = envService.obtenerEnvs();
			envs = (List<Enviroment>)res.getEntities(); 
			mav.addObject("envs",envs);
		} catch (ServiceException e) {
			logger.error(e.getMessage(),e);
		}
	}
	
	@RequestMapping(value = { "/","/index**","/welcome**" }, method = RequestMethod.GET)
	public ModelAndView printWelcome() {
		ModelAndView mav = new ModelAndView("main");		
		init(mav);
		mav.addObject("envDefault","1");
		return mav;
	}
	
	@RequestMapping(value = "/kit/{cdEnv}", method = RequestMethod.GET)
	public String kits(ModelMap model,@PathVariable("cdEnv") Integer cdEnv) {
		model.addAttribute("cdEnv", cdEnv);
		return "kits";
	}
	
	@RequestMapping(value = "/plantilla/{cdEnv}", method = RequestMethod.GET)
	public String plantillas(ModelMap model,@PathVariable("cdEnv") Integer cdEnv) {
		
		model.addAttribute("cdEnv", cdEnv);
		
		Map<String, String> cond = new HashMap<String,String>();
		cond.put(Constants.PLANTILLA_FIJA,Constants.TXT_FIJO);
		cond.put(Constants.PLANTILLA_VARIABLE,Constants.TXT_VARIABLE);
		model.addAttribute("cond",cond);		
		model.addAttribute("condFijo",Constants.PLANTILLA_FIJA);
		
		Map<String, String> estados = new HashMap<String,String>();
		estados.put(Constants.ST_ACTIVO,Constants.TXT_ACTIVO);
		estados.put(Constants.ST_INACTIVO,Constants.TXT_INACTIVO);
		model.addAttribute("estados",estados);
		
		Map<String, String> tipoConst = new HashMap<String,String>();
		tipoConst.put(Constants.TIPO_CONSTR_PDF,Constants.TXT_TIPO_CONST_PDF);
		tipoConst.put(Constants.TIPO_CONSTR_NUEVO,Constants.TXT_TIPO_CONST_NUEVO);
		tipoConst.put(Constants.TIPO_CONSTR_JASPER,Constants.TXT_TIPO_CONST_JASPER);
		tipoConst.put(Constants.TIPO_CONSTR_JASPER_C,Constants.TXT_TIPO_CONST_JASPERC);
		
		
		model.addAttribute("tipoConst",tipoConst);
		
		Map<String, String> nivelPlantilla = new HashMap<String,String>();
		nivelPlantilla.put(Constants.TIPO_PLANTILLA_GENERICA,Constants.TXT_TIPO_GENERICA);
		nivelPlantilla.put(Constants.TIPO_PLANTILLA_PERSONAL,Constants.TXT_TIPO_PERSONAL);
		model.addAttribute("nivelPlantilla",nivelPlantilla);
		
		return "plantillas";
	}	
	
	@RequestMapping(value = "/inicio", method = RequestMethod.POST)
	public String ingresar(@RequestParam("selectEnv") Integer cdEnv) {
		return "redirect:kit/"+cdEnv+"/";
	}
	
	@RequestMapping(value = "/sql", method = RequestMethod.GET)
	public String sql(ModelMap model) {
		model.addAttribute("admin", true);
		return "sql";
	}
	
}