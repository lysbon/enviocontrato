package com.bbva.cuentas.aat.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.ResourceService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.aat.util.ErrorCodes;
import com.bbva.cuentas.bean.ResourceItem;
import com.bbva.cuentas.util.Result;

@Service
public class ResourceServiceImpl implements ResourceService{
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceServiceImpl.class);

	@Autowired
	TokenService tokenService;
	
	@Autowired
	SeccionService seccionService;
	
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	@Override
	public byte[] obtenerImagen(Long id) throws ServiceException{
		byte[] pdf = null;
		try {
			logger.info("Obteniendo imagen item");
			final String uri = urlServices+"/rest/recurso/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<String> pdfResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, params);
		    pdf = Base64.decodeBase64(pdfResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return pdf;
	}
	
	@Override
	public Result obtenerImagenes() throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo recursos");
			final String uri = urlServices+"/rest/recurso/all";
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<ResourceItem>> kitResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ResourceItem>>() {});
		    res.setEntities(kitResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public byte[] obtenerSvgSeccion(Long id) throws ServiceException {
		try{
			seccionService.generarSVGfromSeccion(id);
			Result result = seccionService.generarSVG(id);
			String filename = (String) result.getEntity();
			File file = new File(filename);
			InputStream is = new FileInputStream(file);
			return IOUtils.toByteArray(is);
		} catch (FileNotFoundException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_PROCESAR_SVG,"No se pudo procesar svg.");
		} catch (IOException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_PROCESAR_SVG,"No se pudo procesar svg.");
		} catch (Exception e){
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_PROCESAR_SVG,"No se pudo procesar svg.");
		}
	}

	@Override
	public Result guardarRecurso(ResourceItem recurso) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando recurso");
			final String uri = urlServices+"/rest/recurso";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ResourceItem> request = new HttpEntity<ResourceItem>(recurso, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarRecurso(ResourceItem recurso) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando recurso");
			final String uri = urlServices+"/rest/recurso/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ResourceItem> request = new HttpEntity<ResourceItem>(recurso, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarRecurso(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar recurso: " + id);
			final String uri = urlServices+"/rest/recurso/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
}
