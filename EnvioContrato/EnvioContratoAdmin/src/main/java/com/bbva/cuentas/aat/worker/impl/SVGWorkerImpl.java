package com.bbva.cuentas.aat.worker.impl;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.StringTokenizer;
import java.io.File;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.dto.ItemStampDTO;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.aat.util.SystemUnit;
import com.bbva.cuentas.aat.worker.SVGWorker;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.util.Constants;

@Component
public class SVGWorkerImpl implements SVGWorker{
	
	private Float factor;
	private Float factorReverso;
	private DecimalFormat df;
	
	@Value("${item.huella.label}")
	private String labelHuella;
	@Value("${item.barcode.label}")
	private String labelBarcode;
	@Value("${item.font.size.default}")
	private float fontSizeDefault;

	@Value("${item.huella.sx}")
	private int huellaAnchoDefecto;
	@Value("${item.huella.sy}")
	private int huellaAltoDefecto;
	@Value("${item.huella.ancho}")
	private int huellaTablaAnchoDefecto;
	
	@Value("${item.barcode.sx}")
	private int barcodeAnchoDefecto;
	@Value("${item.barcode.sy}")
	private int barcodeAltoDefecto;
	@Value("${item.barcode.ancho}")
	private int barcodeTablaAnchoDefecto;
	
	@PostConstruct
	public void init(){
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setDecimalSeparator('.');
		df = new DecimalFormat("#########.##",symbols);
		factor = 1.25f;
		factorReverso = 0.8f; 
	}
	
	public Attribute getAttribute(Element node, String name, Namespace namespace){
		if(namespace!=null){
			Attribute attr = node.getAttribute(name, namespace);
			if(attr!=null) return attr;
		}
		return node.getAttribute(name);
	}
	
	public String getChildText(Element node, String name, Namespace namespace){
		if(namespace!=null){
			String child = node.getChildText(name, namespace);
			if(child != null) return child;
		}
		return node.getChildText(name);
	}
	
	public String getChildAttribute(Element node, String name, String attribute, Namespace namespace){
		if(namespace!=null){
			Element child = node.getChild(name,namespace);
			if(child != null){
				String value = child.getAttributeValue(attribute,namespace);
				if(StringUtils.isNotEmpty(value)){
					return value;
				}else{
					return child.getAttributeValue(attribute);
				}
			}
		}
		Element child = node.getChild(name);
		return child.getAttributeValue(attribute);
	}
	
	public List<ItemStampDTO> procesarSVG(Long cdSeccion,String pathFile) {
		List<ItemStampDTO> lista = new ArrayList<ItemStampDTO>(); 
		SAXBuilder builder = new SAXBuilder();
		File xmlFile = new File(pathFile);
		try {
			Document document = (Document) builder.build(xmlFile);
			Element rootNode = document.getRootElement();
			Namespace nsSodipodi = rootNode.getNamespace("sodipodi");
			Namespace nsInkscape = rootNode.getNamespace("inkscape");
			Namespace nsSVG      = rootNode.getNamespace();
			Namespace nsBBVA     = Namespace.getNamespace("bbva","https://www.bbvacontinental.pe");
			if(nsInkscape==null){
				nsInkscape = Namespace.getNamespace("inkscape","http://www.inkscape.org/namespaces/inkscape");
			}
			if(nsSodipodi==null){
				nsSodipodi = Namespace.getNamespace("sodipodi","http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd");
			}
			String height = rootNode.getAttributeValue("height");
			float heightPT = new Float(StringUtils.replace(height,"pt",""));
			List<Element> list = (List<Element>)rootNode.getChildren();
			for (int i = 0; i < list.size(); i++) {
				Element node = (Element) list.get(i);
				if("g".equals(node.getName())){
					Attribute labelCapa = getAttribute(node,"label",nsInkscape);
					if(labelCapa!=null && "datos".equals(labelCapa.getValue())){
						List<Element> elementos = (List<Element>)node.getChildren();
						for (int j = 0; j < elementos.size(); j++) {
							Element e = (Element)elementos.get(j);
							if("text".equals(e.getName())){
								lista.add(procesarTexto(e, cdSeccion, nsSVG, nsInkscape, nsBBVA, heightPT));
							}else if("g".equals(e.getName())){
								lista.add(procesarCapa(e, cdSeccion, nsSVG, nsInkscape, nsBBVA, heightPT));
							}else if("rect".equals(e.getName())){
								lista.add(procesarRectangulo(e, cdSeccion, nsSVG, nsInkscape, nsBBVA, heightPT));
							}
						}
					}
				}
			}
			return lista;
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private ItemStampDTO procesarTexto(Element e,Long cdSeccion,Namespace nsSVG, Namespace nsInkscape,Namespace nsBBVA,float heightPT){
//		String id = e.getAttributeValue("id");
		String x = e.getAttributeValue("x");
		String y = e.getAttributeValue("y");
		String valueText = getChildText(e,"tspan",nsSVG);
		Attribute attrLabel = getAttribute(e, "label", nsInkscape);
		String idLabel = "";
		if(attrLabel!=null && !StringUtils.isEmpty(attrLabel.getValue())){
			idLabel = attrLabel.getValue();
		}else{
			idLabel = valueText;
		}
		String styleNodo  = getChildAttribute(e,"tspan","style",nsSVG);
		if(StringUtils.isEmpty(styleNodo)){
			styleNodo = e.getAttributeValue("style");
		}
		StringTokenizer st = new StringTokenizer(styleNodo,";");
		String fontWeight=null,fontSize=null,fontFamily=null,textAlign=null;
		while(st.hasMoreTokens()){
			String token = st.nextToken();
			if (token.indexOf(":") > 0) {
                String label = token.substring(0, token.indexOf(":"));
                if("font-weight".equals(label)){
                	fontWeight = token.substring(token.indexOf(":")+1);
                }else if("font-size".equals(label)){
                	fontSize = token.substring(token.indexOf(":")+1);
                }else if("font-family".equals(label)){
                	fontFamily = token.substring(token.indexOf(":")+1);
                }else if("text-anchor".equals(label)){
                	textAlign = token.substring(token.indexOf(":")+1);
                }
            }
		}
		String stActivo     = e.getAttributeValue("stActivo",nsBBVA);
		String nuIdxCliente = e.getAttributeValue("nuIdxCliente",nsBBVA);
		String nbMask       = e.getAttributeValue("nbMask",nsBBVA);
		String id           = e.getAttributeValue("id",nsBBVA);
		String label        = e.getAttributeValue("label",nsBBVA);
		
		ItemStampDTO item = new ItemStampDTO();
		item.setNuCoorX(Math.round(new Float(x)*factorReverso));
		item.setNuCoorY(Math.round(heightPT-new Float(y)*factorReverso));
		item.setNbFuente(obtenerFuente(fontFamily,fontWeight,fontSize));
		item.setNuAlinear(obtenerAlineamiento(Constants.TYPE_TEXT,textAlign));
		item.setNbNombreVar(StringUtils.replace(idLabel,"#",""));
		item.setChTipo(Constants.TYPE_TEXT);
		if(StringUtils.contains(id,"ID_")) {
			item.setCdItem(StringUtils.replace(id, "ID_", ""));
		}
		item.setCdSeccion(cdSeccion);
		item.setStActivo(stActivo);
		item.setNuIdxCliente((StringUtils.isEmpty(nuIdxCliente)?null:new Integer(nuIdxCliente)));
		item.setNbMask(nbMask);
		return item;
	}
	
	private ItemStampDTO procesarCapa(Element e,Long cdSeccion,Namespace nsSVG, Namespace nsInkscape,Namespace nsBBVA,float heightPT){
		Attribute labelGrupo = getAttribute(e,"label",nsInkscape);
		if(labelGrupo!=null && 
				(labelHuella.equals(labelGrupo.getValue())||
				labelBarcode.equals(labelGrupo.getValue()))){
			String x     = StringUtils.EMPTY;
			String y     = StringUtils.EMPTY;
			String sx    = StringUtils.EMPTY;
			String sy    = StringUtils.EMPTY;
			String ancho = StringUtils.EMPTY;
			
			ItemStampDTO itTxt  = null;
			
			String stActivo     = e.getAttributeValue("stActivo",nsBBVA);
			String nuIdxCliente = e.getAttributeValue("nuIdxCliente",nsBBVA);
			String nbMask       = e.getAttributeValue("nbMask",nsBBVA);
			String id           = e.getAttributeValue("id",nsBBVA);
			String label        = e.getAttributeValue("label",nsBBVA);
			
			List<Element> huellaItems = e.getChildren();
			if(huellaItems!=null){
				for(Element item : huellaItems){
					String itemId = item.getAttributeValue("id");
					if(StringUtils.contains(itemId,"rectout")){
						x = item.getAttributeValue("x");
						y = item.getAttributeValue("y");
						ancho = item.getAttributeValue("width");
					}else if(StringUtils.contains(itemId,"rectin")){
						sx = item.getAttributeValue("width");
						sy = item.getAttributeValue("height");
					}else if(StringUtils.contains(itemId, "text")){
						itTxt = procesarTexto(item, cdSeccion, nsSVG, nsInkscape, nsBBVA, heightPT);
					}
				}
			}
			if(labelHuella.equals(labelGrupo.getValue())){
				return procesarItem(cdSeccion,id,x,y,sx,sy,ancho,label,Constants.TYPE_IMAGE,heightPT,stActivo,nuIdxCliente,nbMask,itTxt.getNuAlinear());
			}else if(labelBarcode.equals(labelGrupo.getValue())){
				return procesarItem(cdSeccion,id,x,y,sx,sy,ancho,label,Constants.TYPE_BAR_CODE,heightPT,stActivo,nuIdxCliente,nbMask,itTxt.getNuAlinear());
			}
		}
		return null;
	}
	
	private ItemStampDTO procesarRectangulo(Element e,Long cdSeccion,Namespace nsSVG, Namespace nsInkscape,Namespace nsBBVA,float heightPT){
		Attribute labelGrupo = getAttribute(e,"label",nsInkscape);
		if(labelGrupo!=null && 
				(labelHuella.equals(labelGrupo.getValue())||
				labelBarcode.equals(labelGrupo.getValue()))){
			
			String stActivo     = e.getAttributeValue("stActivo",nsBBVA);
			String nuIdxCliente = e.getAttributeValue("nuIdxCliente",nsBBVA);
			String nbMask       = e.getAttributeValue("nbMask",nsBBVA);
			String id           = e.getAttributeValue("id",nsBBVA);
			String label        = e.getAttributeValue("label",nsBBVA);
			
			String x           	= e.getAttributeValue("x");
			String y        	= e.getAttributeValue("y");
			String sx    		= StringUtils.EMPTY;
			String sy    		= StringUtils.EMPTY;
			String ancho 		= StringUtils.EMPTY;
			Integer nuAlinear   = null;
			
			if(labelHuella.equals(labelGrupo.getValue())){
				return procesarItem(cdSeccion,id,x,y,sx,sy,ancho,label,Constants.TYPE_IMAGE,heightPT,stActivo,nuIdxCliente,nbMask,nuAlinear);
			}else if(labelBarcode.equals(labelGrupo.getValue())){
				return procesarItem(cdSeccion,id,x,y,sx,sy,ancho,label,Constants.TYPE_BAR_CODE,heightPT,stActivo,nuIdxCliente,nbMask,nuAlinear);
			}
		}
		return null;
	}
		
	private ItemStampDTO procesarItem(
			Long cdSeccion,String id,String x,String y,String sx,String sy,String ancho,
			String label,String tipo,float heightDocumento,
			String stActivo,String nuIdxCliente,String nbMask,Integer nuAlinear){
		
		ItemStampDTO item = new ItemStampDTO();
		item.setNuCoorX(Math.round(new Float(x)*factorReverso));
		item.setNuCoorY(Math.round(heightDocumento - new Float(y)*factorReverso));
		
		Integer isx = null;
		Integer isy = null;
		Integer isw = null;
		if(!StringUtils.isEmpty(sx)){
			isx = Math.round(new Float(sx)*factorReverso);
		}else{
			if(Constants.TYPE_IMAGE.equals(tipo) && labelHuella.equals(label)){
				isx = Math.round(new Float(huellaAnchoDefecto));
			}else if(Constants.TYPE_BAR_CODE.equals(tipo)){
				isx = Math.round(new Float(barcodeAnchoDefecto));
			}
		}
		if(!StringUtils.isEmpty(sy)){
			isy = Math.round(new Float(sy)*factorReverso);
		}else{
			if(Constants.TYPE_IMAGE.equals(tipo) && labelHuella.equals(label)){
				isy = Math.round(new Float(huellaAltoDefecto));
			}else if(Constants.TYPE_BAR_CODE.equals(tipo)){
				isy = Math.round(new Float(barcodeAltoDefecto));
			}
		}
		if(!StringUtils.isEmpty(sx)){
			isw = Math.round(new Float(ancho)*factorReverso);
		}else{
			if(Constants.TYPE_IMAGE.equals(tipo) && labelHuella.equals(label)){
				isw = Math.round(new Float(huellaTablaAnchoDefecto));
			}else if(Constants.TYPE_BAR_CODE.equals(tipo)){
				isw = Math.round(new Float(barcodeTablaAnchoDefecto));
			}
		}
		
		item.setNuScX(isx);
		item.setNuScY(isy);
		item.setNuAncho(isw);
		
		item.setNbNombreVar(label);
		item.setChTipo(tipo);
		item.setCdItem(StringUtils.replace(id,label+"_",""));
		item.setStActivo(StringUtils.isEmpty(stActivo)?Constants.ST_ACTIVO:stActivo);
		item.setNuIdxCliente((StringUtils.isEmpty(nuIdxCliente)?null:new Integer(nuIdxCliente)));
		item.setNbMask(StringUtils.isEmpty(nbMask)?StringUtils.EMPTY:nbMask);
		item.setCdSeccion(cdSeccion);
		item.setNuAlinear((nuAlinear==null)?Constants.ALIGN_LEFT:nuAlinear);
		return item;
	}
	
	private String obtenerEstilo(ItemStamp is){
		StringBuilder sb = new StringBuilder();
		sb.append(obtenerFuente(is));
		sb.append(obtenerAlineamiento(is));
		return sb.toString();
	}
	
	public String obtenerAlineamiento(ItemStamp is){
		if(com.itextpdf.text.Element.ALIGN_LEFT == is.getNuAlinear().intValue()){
			return "text-anchor:start;";
		}else if(com.itextpdf.text.Element.ALIGN_CENTER == is.getNuAlinear().intValue()){
			return "text-align:center;text-anchor:middle;";
		}else if(com.itextpdf.text.Element.ALIGN_RIGHT == is.getNuAlinear().intValue()){
			return "text-align:end;text-anchor:end;";
		}
		return StringUtils.EMPTY;
	}
	
	public Integer obtenerAlineamiento(String type,String align){
		if(StringUtils.contains(align,"center")|| StringUtils.contains(align,"middle")){
			return com.itextpdf.text.Element.ALIGN_CENTER;
		}else if(StringUtils.contains(align,"end")){
			return com.itextpdf.text.Element.ALIGN_RIGHT;
		}else if(StringUtils.contains(align,"start")){
			return com.itextpdf.text.Element.ALIGN_LEFT;
		}else{
			return null;
		}
	}
	
	private String obtenerFuente(ItemStamp is){
		StringBuilder sb = new StringBuilder("font-variant:normal;");
		sb.append("font-style:normal;");
		sb.append("font-stretch:normal;");
		sb.append("font-size:"+obtenerFuenteSize(is.getNbFuente())+"px;");
		if(!StringUtils.isEmpty(is.getNbFuente())){
			if(StringUtils.contains(is.getNbFuente(), Constants.FONT_COD_ARIAL)){
				sb.append("font-family:Arial;");
			}else if(StringUtils.contains(is.getNbFuente(), Constants.FONT_COD_CALIBRI)){
				sb.append("font-family:Calibri;");
			}else if(StringUtils.contains(is.getNbFuente(), Constants.FONT_COD_COURIER_NEW)){
				sb.append("font-family:Courier New;");
			}else{
				sb.append("font-family:sans-serif;");
			}
			if(StringUtils.contains(is.getNbFuente(), Constants.FONT_COD_BOLD)){
				sb.append("font-weight:bold;");
			}else{
				sb.append("font-weight:normal;");
			}
		}else{
			sb.append("font-family:sans-serif;");
			sb.append("font-weight:normal;");
		}
		
		return sb.toString();
	}
	
	private String obtenerFuenteSize(String data){
		if(!StringUtils.isEmpty(data)){
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < data.length(); i++) {
		        if (Character.isDigit(data.charAt(i))) {
		            sb.append(data.charAt(i));
		        }
		    }
			try {
				float sizeAdjust = new Float(sb.toString());
				return df.format(sizeAdjust*factor); 
			} catch (Exception e) {
				return df.format(fontSizeDefault*factor);
			}
		}
		return StringUtils.EMPTY;
	}
	
	public String obtenerFuente(String fontFamily, String fontWeight,String fontSize){
		StringBuilder sb = new StringBuilder("");
		if(StringUtils.contains(fontFamily,"sans-serif")){
			sb.append(Constants.FONT_COD_CALIBRI);
		}else if(StringUtils.contains(fontFamily,"Arial")){
			sb.append(Constants.FONT_COD_ARIAL);
		}else if(StringUtils.contains(fontFamily,"Calibri")){
			sb.append(Constants.FONT_COD_CALIBRI);
		}else if(StringUtils.contains(fontFamily,"Courier")){
			sb.append(Constants.FONT_COD_COURIER_NEW);
		}else{
			sb.append(Constants.FONT_COD_CALIBRI);
		}
		
		if(StringUtils.contains(fontWeight,"normal")){
			sb.append(StringUtils.EMPTY);
		}else if(StringUtils.contains(fontWeight,"bold")){
			sb.append(Constants.FONT_COD_BOLD);
		}
		if(StringUtils.contains(fontSize,"px")){
			String size = fontSize.replace("px", "");
			float sz = fontSizeDefault;
			try {
				sz = Float.parseFloat(size)*factorReverso;
			} catch (Exception e) {
				e.printStackTrace();
			}
			sb.append(df.format(sz));
		}else{
			sb.append(Constants.FONT_COD_11);
		}
		return sb.toString();
	}
	
	public void toSystemUnit(
			String fileOriginal, String fileSalida,
			SystemUnit from, SystemUnit to) {
		SAXBuilder builder = new SAXBuilder();
		File xmlFileInput  = new File(fileOriginal);
		try {
			Document document = (Document) builder.build(xmlFileInput);
			Element rootNode = document.getRootElement();
			
			String width = rootNode.getAttributeValue("width");
			String height = rootNode.getAttributeValue("height");
			
			float fheight = new Float(StringUtils.replace(height,"pt",""));
			float fwidth  = new Float(StringUtils.replace(width,"pt",""));
			String heightFinal="",widthFinal = "";
			if(from.equals(SystemUnit.PX)){
				if(to.equals(SystemUnit.PT)){
					heightFinal = df.format(fheight*factorReverso)+"pt";
					widthFinal  = df.format(fwidth*factorReverso)+"pt";
				}
			}
			rootNode.getAttribute("height").setValue(heightFinal);
			rootNode.getAttribute("width").setValue(widthFinal);
			
			writeXMLFile(document, fileSalida);
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}

	public void generarSVG(String fileOriginal,String fileSalida, List<ItemStamp> items) {
		SAXBuilder builder = new SAXBuilder();
		File xmlFileInput  = new File(fileOriginal);
		try {
			Document document = (Document) builder.build(xmlFileInput);
			Element rootNode = document.getRootElement();
			Namespace nsInkscape = rootNode.getNamespace("inkscape");
			Namespace nsSodipodi = rootNode.getNamespace("sodipodi");
			Namespace nsXML      = rootNode.getNamespace("xml");
			Namespace nsSVG      = rootNode.getNamespace();
			List<Element> list = (List<Element>)rootNode.getChildren();
			
			if(nsInkscape==null){
				nsInkscape = Namespace.getNamespace("inkscape","http://www.inkscape.org/namespaces/inkscape");
			}
			if(nsSodipodi==null){
				nsSodipodi = Namespace.getNamespace("sodipodi","http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd");
			}
			
			Namespace nsBBVA     = Namespace.getNamespace("bbva","https://www.bbvacontinental.pe");
			rootNode.addNamespaceDeclaration(nsBBVA);
			
			String width = rootNode.getAttributeValue("width");
			String height = rootNode.getAttributeValue("height");
			String viewBox = rootNode.getAttributeValue("viewBox");
			
			System.out.println(width);
			System.out.println(height);
			System.out.println(viewBox);
			
			float heightPT = new Float(StringUtils.replace(height,"pt",""));
			
			for (int i = 0; i < list.size(); i++) {
				Element node = (Element) list.get(i);
				if("g".equals(node.getName())){
					if(nsInkscape!=null){
						Attribute attr = node.getAttribute("label", nsInkscape);
						if(attr!=null && "datos".equals(attr.getValue())){
							node.detach();
						}
					}else{
						Attribute att = node.getAttribute("label");
						if(att!=null && "datos".equals(att.getValue())){
							node.detach();
						}
					}
				}
			}
			
			Element datos = createLayer("datos","layer1",nsInkscape,nsSVG,nsBBVA); 
			for(ItemStamp is : items){
				if(Constants.TYPE_TEXT.equals(is.getChTipo())){
					datos.addContent(createTextElement(is, nsInkscape, nsSVG, nsXML, nsSodipodi,nsBBVA, heightPT));
				}else if(Constants.TYPE_IMAGE.equals(is.getChTipo())){
					if(labelHuella.equals(is.getNbNombreVar())){
						datos.addContent(createHuellaElement(is, nsInkscape, nsSVG, nsXML, nsSodipodi,nsBBVA, heightPT));
					}else{
						datos.addContent(createImageElement(is, nsInkscape, nsSVG, nsXML, nsSodipodi,nsBBVA, heightPT));
					}
				}else if(Constants.TYPE_BAR_CODE.equals(is.getChTipo())){
					datos.addContent(createBarCodeElement(is, nsInkscape, nsSVG, nsXML, nsSodipodi,nsBBVA, heightPT));
				}
			}
			list.add(datos);
			writeXMLFile(document, fileSalida);
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}

	public void writeXMLFile(Document document, String filename) throws IOException {
		XMLOutputter outputter = new XMLOutputter();
	    Format format = Format.getPrettyFormat();
	    //format.setLineSeparator(System.getProperty("line.separator"));
	    //format.setIndent("    ");
	    outputter.setFormat(format);
	    // FileWriter can't be used here, because default encoding on Win32 isn't UTF-8
	    OutputStreamWriter osWriter = new OutputStreamWriter(new FileOutputStream(filename), "UTF-8");
	    outputter.output(document, osWriter);
	    osWriter.flush();
	}
	
	private Element createImageElement(ItemStamp is,Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			float height) {
		return createTextElement(is, nsInkscape, nsSVG, nsXML, nsSodipodi,bbva, height);
	}
	
	private Element createTableElement(String name,ItemStamp is,Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi, Namespace bbva,
			float heightDocumento) {
		
		Element objeto = createLayer(name,name+"_"+is.getCdItem(),nsInkscape,nsSVG,bbva); 
		
		objeto.setAttribute("stActivo",(is.getStActivo()==null)?"":is.getStActivo(),bbva);
		objeto.setAttribute("nuIdxCliente",(is.getNuIdxCliente()==null)?"":is.getNuIdxCliente().toString(),bbva);
		objeto.setAttribute("nbMask",(is.getNbMask()==null)?"":is.getNbMask(),bbva);
		
		float x      = is.getNuCoorX()*factor;
		float y      = (heightDocumento - is.getNuCoorY())*factor;
		float width  = is.getNuAncho()*factor;
		float height = is.getNuScY()*factor;
		
		Element rectOut = createSquareElement(nsInkscape, nsSVG, nsXML, nsSodipodi, bbva,
				df.format(x),df.format(y),df.format(width),df.format(height),"rectout_"+is.getCdItem());
		
		float paddingX = 5;
		float paddingY = -10;
		x      = (is.getNuCoorX()+(is.getNuAncho()-is.getNuScX())/2 + paddingX)*factor;
		y      = (heightDocumento - (is.getNuCoorY() + paddingY))*factor;
		
		Element text = createTextElement(nsInkscape, nsSVG, nsXML, nsSodipodi, bbva,
				df.format(x),df.format(y),"text_"+is.getCdItem(),"#"+is.getCdItem(),
				"tspan_"+is.getCdItem(),is.getNbNombreVar(),obtenerEstilo(is),null,null,null);
		
		x      = (is.getNuCoorX()+(is.getNuAncho()-is.getNuScX())/2)*factor;
		y      = (heightDocumento - is.getNuCoorY())*factor;
		width  = is.getNuScX()*factor;
		height = is.getNuScY()*factor;
		
		Element rectIn  = createSquareElement(nsInkscape, nsSVG, nsXML, nsSodipodi, bbva,
				df.format(x),df.format(y),df.format(width),df.format(height),"rectin_"+is.getCdItem());
		
		objeto.addContent(text);
		objeto.addContent(rectOut);
		objeto.addContent(rectIn);
		return objeto;
	}
	
	private Element createHuellaElement(ItemStamp is,Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			float heightDocumento) {
		return createTableElement(labelHuella, is, nsInkscape, nsSVG, nsXML, nsSodipodi,bbva, heightDocumento);
	}
	
	private Element createBarCodeElement(ItemStamp is,Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			float heightDocumento) {
		return createTableElement(labelBarcode, is, nsInkscape, nsSVG, nsXML, nsSodipodi,bbva, heightDocumento);
	}
	
	private Element createSquareElement(Namespace nsInkscape,Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			String x, String y, String ancho, String alto, String id) {
		Element e = new Element("rect",nsSVG);
		e.setAttribute("space","preserve", nsXML);
		e.setAttribute("style","opacity:1;fill:none;fill-opacity:1;stroke:#000000;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1");
		e.setAttribute("x",x);
		e.setAttribute("y",y);
		e.setAttribute("width",ancho);
		e.setAttribute("height",alto);
		e.setAttribute("id",id);
		return e;
	}
	/*
	 * String label = token.substring(0, token.indexOf(":"));
        if("font-weight".equals(label)){
        	fontWeight = token.substring(token.indexOf(":")+1);
        }else if("font-size".equals(label)){
        	fontSize = token.substring(token.indexOf(":")+1);
        }else if("font-family".equals(label)){
        	fontFamily = token.substring(token.indexOf(":")+1);
        }
	 * 
	 */
	
	private Element createTextElement(Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			String x, String y, String id, String label,String tspanId,String value,String style,
			Integer nuIdxCliente,String stActivo,String nbMask) {
		Element e = new Element("text",nsSVG);
		e.setAttribute("space","preserve", nsXML);
		e.setAttribute("style","font-style:normal;font-weight:normal;font-size:40px;line-height:125%;font-family:sans-serif;letter-spacing:0px;word-spacing:0px;display:inline;fill:#000000;fill-opacity:1;stroke:none;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1");
		e.setAttribute("x",x);
		e.setAttribute("y",y);
		e.setAttribute("id",id);
		e.setAttribute("label",label,nsInkscape);
		e.setAttribute("linespacing", "125%", nsSodipodi);
		
		e.setAttribute("stActivo",(stActivo==null)?"":stActivo,bbva);
		e.setAttribute("nuIdxCliente",(nuIdxCliente==null)?"":nuIdxCliente.toString(),bbva);
		e.setAttribute("nbMask",(nbMask==null)?"":nbMask,bbva);
		e.setAttribute("id",id,bbva);
		e.setAttribute("label",label,bbva);
		
		Element tspan = new Element("tspan",nsSVG);
		tspan.setAttribute("role", "line", nsSodipodi);
		tspan.setAttribute("x",x);
		tspan.setAttribute("y",y);
		tspan.setAttribute("id",tspanId);
		tspan.setAttribute("style",style);
		tspan.setText(value);
		e.addContent(tspan);
		return e;
	}

	private Element createTextElement(ItemStamp is, Namespace nsInkscape,
			Namespace nsSVG, Namespace nsXML, Namespace nsSodipodi,Namespace bbva,
			float height) {
		
		float x = is.getNuCoorX()*factor;
		float y = (height - is.getNuCoorY())*factor;
		String id    = "ID_"+is.getCdItem();
		String label = "#"+is.getNbNombreVar();
		String tspanId = "tspan"+is.getCdItem();
		String value = StringUtils.EMPTY;
		if(is.getNuAncho() == null){
			value = AppUtil.getValuePrint(is);
		}else if(is.getNuAncho()>0){
			value = StringUtils.substring(AppUtil.getValuePrint(is),0,is.getNuAncho());
		}
		return createTextElement(nsInkscape,nsSVG,nsXML,nsSodipodi,bbva,
				df.format(x),df.format(y),id,label,
				tspanId,value,obtenerEstilo(is),
				is.getNuIdxCliente(),is.getStActivo(),is.getNbMask());
	}
	
	private Element createLayer(String label, String id, Namespace nsInkscape, Namespace nsSVG, Namespace nsBBVA) {
		Element e = new Element("g",nsSVG);
		e.setAttribute("groupmode","layer", nsInkscape);
		e.setAttribute("label",label,nsInkscape);
		e.setAttribute("id",id,nsBBVA);
		e.setAttribute("label",label,nsBBVA);
		return e;
	}
	
}
