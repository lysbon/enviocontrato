package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.util.Result;

@Service
public class ItemServiceImpl implements ItemService{

	private static final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	public Result obtenerItemPorCodigo(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo item por id");
			final String uri = urlServices+"/rest/item/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<ItemStamp> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<ItemStamp>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerItemsPorSeccion(Long cdSeccion) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo items por seccion");
			final String uri = urlServices+"/rest/seccion/{id}/items";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdSeccion);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<ItemStamp>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ItemStamp>>() {}, params);
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarItem(ItemStamp itemStamp) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando item");
			final String uri = urlServices+"/rest/item";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ItemStamp> request = new HttpEntity<ItemStamp>(itemStamp, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarItem(ItemStamp itemStamp) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando item");
			final String uri = urlServices+"/rest/item/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ItemStamp> request = new HttpEntity<ItemStamp>(itemStamp, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarItem(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar item : " + id);
			final String uri = urlServices+"/rest/item/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerItems() throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo items");
			final String uri = urlServices+"/rest/flag/all";
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<ItemStamp>> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ItemStamp>>() {});
		    res.setEntities(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
}
