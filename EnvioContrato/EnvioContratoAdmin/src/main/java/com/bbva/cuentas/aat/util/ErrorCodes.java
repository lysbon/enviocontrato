package com.bbva.cuentas.aat.util;

public class ErrorCodes {

	//Data errors
	public final static String D_INSERT_ERROR = "D-100";
    public final static String D_UPDATE_ERROR = "D-101";
    public final static String D_DELETE_ERROR = "D-102";
    public final static String D_PK_NOT_DEFINED = "D-103";    
    public final static String D_QUERY_ERROR = "D-104";
    public final static String D_SQL_ERROR = "D-199";
    
    //Service errors
    public final static String S_DB_ERROR = "S-100";
	public final static String S_OTHER_ERROR = "S-101";
	public final static String S_ERROR_OBTENER_MENSAJE = "S-102";
	public final static String S_ERROR_OBTENER_PARAMETRO = "S-103";
	public final static String S_ERROR_OBTENER_MESTRO = "S-104";
	public final static String S_ERROR_MAIL = "S-105";
	
	public final static String S_ERROR_KIT_OBTENER_KITS          = "S-140";
	public final static String S_ERROR_KIT_OBTENER_KIT           = "S-141";
	public final static String S_ERROR_KIT_GUARDAR_KIT           = "S-142";
	public final static String S_ERROR_KIT_ACTUALIZAR_KIT        = "S-143";
	public final static String S_ERROR_KIT_ELIMINAR_KIT          = "S-144";
	public static final String S_ERROR_KIT_OBTENER_MAIL_TEMPLATE = "S-145";
	public final static String S_ERROR_KIT_ACTUALIZAR_KIT_EMAIL  = "S-146";
	
	public final static String S_ERROR_KIT_PLANTILLA_OBTENER_KPS          = "S-130";
	public final static String S_ERROR_KIT_PLANTILLA_OBTENER_KP           = "S-131";
	public final static String S_ERROR_KIT_PLANTILLA_GUARDAR_KP           = "S-132";
	public final static String S_ERROR_KIT_PLANTILLA_ACTUALIZAR_KP        = "S-133";
	public final static String S_ERROR_KIT_PLANTILLA_ELIMINAR_KP          = "S-134";
	
	public final static String S_ERROR_PLT_OBTENER_PLANTILLAS    = "S-150";
	public final static String S_ERROR_PLT_OBTENER_PLANTILLA     = "S-151";
	public final static String S_ERROR_PLT_OBTENER_PDF           = "S-152";
	public final static String S_ERROR_PLT_GUARDAR_PLANTILLA     = "S-153";
	public final static String S_ERROR_PLT_ACTUALIZAR_PLANTILLA  = "S-154";
	public final static String S_ERROR_PLT_ELIMINAR_PLANTILLA    = "S-155";
	public final static String S_ERROR_PLT_ACTUALIZAR_PDF        = "S-156";
	
	public static final String S_ERROR_SEC_OBTENER_POR_PLANTILLA = "S-160";
	public static final String S_ERROR_SEC_OBTENER_SECCIONES     = "S-161";
	public static final String S_ERROR_SEC_OBTENER_SECCION       = "S-162";
	public static final String S_ERROR_SEC_GUARDAR_SECCION       = "S-163";
	public static final String S_ERROR_SEC_ACTUALIZAR_SECCION    = "S-164";
	public static final String S_ERROR_SEC_ELIMINAR_SECCION      = "S-165";
	public static final String S_ERROR_SEC_GENERAR_SVG           = "S-166";
	public static final String S_ERROR_SEC_PROCESAR_SVG          = "S-167";
	public static final String S_ERROR_SEC_GENERAR_SCRIPT        = "S-168";
	public static final String S_ERROR_SEC_APLICAR_CAMBIOS       = "S-169";
	public static final String S_ERROR_SEC_GENERAR_SVG_PAGE      = "S-170";
	
	public static final String S_ERROR_ITM_OBTENER_POR_SECCION   = "S-170";
	public static final String S_ERROR_ITM_OBTENER_ITEMS         = "S-171";
	public static final String S_ERROR_ITM_OBTENER_ITEM          = "S-172";
	public static final String S_ERROR_ITM_GUARDAR_ITEM          = "S-173";
	public static final String S_ERROR_ITM_ACTUALIZAR_ITEM       = "S-174";
	public static final String S_ERROR_ITM_ELIMINAR_ITEM         = "S-175";
	
	public static final String S_ERROR_FPT_OBTENER_FLAG_PLANTILLAS    = "S-180";
	public static final String S_ERROR_FPT_OBTENER_FLAG_PLANTILLA     = "S-181";
	public static final String S_ERROR_FPT_OBTENER_FLAG_POR_PLANTILLA = "S-182";
	public static final String S_ERROR_FPT_GUARDAR_FLAG_PLANTILLA     = "S-183";
	public static final String S_ERROR_FPT_ACTUALIZAR_FLAG_PLANTILLA  = "S-184";
	public static final String S_ERROR_FPT_ELIMINAR_FLAG_PLANTILLA    = "S-185";

	public static final String S_ERROR_FLG_OBTENER_FLAGS            = "S-190";
	public static final String S_ERROR_FLG_OBTENER_FLAG             = "S-191";
	public static final String S_ERROR_FLG_OBTENER_FLAG_POR_TIPO    = "S-192";
	public static final String S_ERROR_FLG_GUARDAR_FLAG             = "S-193";
	public static final String S_ERROR_FLG_ACTUALIZAR_FLAG          = "S-194";
	public static final String S_ERROR_FLG_ELIMINAR_FLAG            = "S-195";
	
	public static final String S_ERROR_RES_OBTENER_RECURSOS            = "S-200";
	public static final String S_ERROR_RES_OBTENER_RECURSO             = "S-201";
	public static final String S_ERROR_RES_OBTENER_RECURSO_POR_TIPO    = "S-202";
	public static final String S_ERROR_RES_GUARDAR_RECURSO             = "S-203";
	public static final String S_ERROR_RES_ACTUALIZAR_RECURSO          = "S-204";
	public static final String S_ERROR_RES_ELIMINAR_RECURSO            = "S-205";
	
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENTS            = "S-200";
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENT             = "S-201";
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENT_POR_TIPO    = "S-202";
	public static final String S_ERROR_ENV_GUARDAR_ENVIROMENT             = "S-203";
	public static final String S_ERROR_ENV_ACTUALIZAR_ENVIROMENT          = "S-204";
	public static final String S_ERROR_ENV_ELIMINAR_ENVIROMENT            = "S-205";
	
	public static final String S_ERROR_INIT            = "S-210";
	public static final String S_ERROR_INIT_ENV        = "S-211";
	
	//View Validation errors
	public static final String V_ERROR_INTERNO="V-100";
	
}
