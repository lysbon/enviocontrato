package com.bbva.cuentas.aat.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.ResourceService;

@Controller
@RequestMapping("/resource")
public class ResourceController {

	@Autowired
	private PlantillaService plantillaService;
	@Autowired
	private ResourceService resourceService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String plantillas(ModelMap model) {
		/*
		try {
			/*model.addAttribute("lists", resourceService.obtenerImagenes());
		} catch (ServiceException e) {
			logger.error(e.getMessage(),e);
		}*/
		return "recursos";
	}
	
	@RequestMapping(value = "/pdf/{id}", method = RequestMethod.GET)
    public void getpdf(@PathVariable Long id,HttpServletResponse response) throws Exception {
        byte[] data = plantillaService.obtenerPDF(id);
        streamReport(response, data, id+".pdf");
    }
	
	@RequestMapping(value = "/img/{id}", method = RequestMethod.GET)
    public void getjpg(@PathVariable Long id,HttpServletResponse response) throws Exception {
        byte[] data = resourceService.obtenerImagen(id);
        streamImg(response, data, id+".jpg");
    }
	
	@RequestMapping(value = "/svg/{id}", method = RequestMethod.GET)
    public void getsvg(@PathVariable Long id,HttpServletResponse response) throws Exception {
        byte[] data = resourceService.obtenerSvgSeccion(id);
        streamImg(response, data, id+".svg");
    }

    protected void streamReport(HttpServletResponse response, byte[] data, String name)
            throws IOException {
    	stream(response,data,name,"pdf");
    }
    
    protected void streamImg(HttpServletResponse response, byte[] data, String name)
            throws IOException {
        stream(response,data,name,"jpg");
    }
    
    protected void stream(HttpServletResponse response, byte[] data, String name, String type)
            throws IOException {
        response.setContentType("application/"+type);
        //response.setHeader("Content-disposition", "attachment; filename=" + name);
        if(data==null){
        	response.setContentLength(0);
            response.getOutputStream().write(null);
        }else{
        	response.setContentLength(data.length);
            response.getOutputStream().write(data);
        }
        response.getOutputStream().flush();
    }
    
    
}
