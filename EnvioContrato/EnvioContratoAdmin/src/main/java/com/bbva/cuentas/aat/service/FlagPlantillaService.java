package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.util.Result;

public interface FlagPlantillaService {
	
	public Result obtenerFlagsPorPlantilla(Long cdPlantilla) throws ServiceException;
	
	public Result obtenerFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException;
	
	public Result guardarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException;
	
	public Result actualizarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException;
	
	public Result eliminarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException;
	
}
