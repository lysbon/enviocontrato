package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.util.Result;

@Service
public class FlagServiceImpl implements FlagService{

	private static final Logger logger = LoggerFactory.getLogger(FlagServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;

	@Override
	public Result obtenerFlagPorCodigo(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo grupo por id");
			final String uri = urlServices+"/rest/flag/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Flag> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Flag>() {}, params);
		    res.setEntity(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerFlagsPorTipo(String tipo) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo flags por tipo");
			final String uri = urlServices+"/rest/flag/all/{tipo}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("tipo", tipo);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Flag>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Flag>>() {}, params);
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	@Override
	public Result obtenerFlags() throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo flags por grupo");
			final String uri = urlServices+"/rest/flag/all";
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Flag>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Flag>>() {});
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarFlag(Flag flag) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando flag");
			final String uri = urlServices+"/rest/flag";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Flag> request = new HttpEntity<Flag>(flag, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarFlag(Flag flag) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando flag");
			final String uri = urlServices+"/rest/flag/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Flag> request = new HttpEntity<Flag>(flag, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarFlag(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar grupo : " + id);
			final String uri = urlServices+"/rest/flag/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.POST, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
		
}
