package com.bbva.cuentas.aat.service.impl;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;

@Service
public class TokenServiceImpl implements TokenService{

	private static final Logger logger = LoggerFactory.getLogger(TokenServiceImpl.class);
	
	@Value("${security.oauth.tokenService}")
	private String urlTokenService;
	
	@Value("${security.oauth.clientId}")
	private String clientId;
	
	@Value("${security.oauth.clientSecret}")
	private String clientSecret;	
	
	public RestTemplate getTemplate(){
		OAuth2RestTemplate template = null;
		try {
			if(!StringUtils.isEmpty(urlTokenService)){
				ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
				resource.setClientId(clientId);
				resource.setClientSecret(clientSecret);
				resource.setGrantType("client_credentials");
				resource.setAccessTokenUri(urlTokenService);
				List<String> scopeList = new ArrayList<String>();
				scopeList.add("read");
				scopeList.add("write");
				scopeList.add("trust");
				resource.setScope(scopeList);
				template = new OAuth2RestTemplate(resource);
			}else{
				return new RestTemplate();
			}			
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		
		return template;
	}
	
}
