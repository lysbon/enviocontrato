package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.AbstractService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.util.Result;

@Service
public class AbstractServiceImpl<S,T> implements AbstractService <S,T>{

	private static final Logger logger = LoggerFactory.getLogger(AbstractServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;

	protected String entidadRest;
	protected String entidadNombre;
	protected String recurso;
	
	@Override
	public Result obtenerEntidades() throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo "+entidadNombre);
			final String uri = urlServices+"/rest/"+entidadRest+"/all";
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<T>> response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<T>>() {}, params);
		    res.setEntities(response.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerEntidadesPorGrupo(Integer cdEnv) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo "+entidadNombre+" por grupo");
			final String uri = urlServices+"/rest/"+entidadRest+"/g/{cdEnv}";
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    params.put("cdEnv", cdEnv);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<T> response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<T>() {}, params);
		    res.setEntity(response.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerEntidadPorId(S id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo "+entidadNombre+" por grupo");
			final String uri = urlServices+"/rest/"+entidadRest+"/{id}";
		    Map<String, S> params = new HashMap<String, S>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<T> response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<T>() {}, params);
		    res.setEntity(response.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	@Override
	public byte[] obtenerRecursoPorId(S id) throws ServiceException {
		byte[] recursoDecoded = null;
		try {
			logger.info("Obteniendo recurso de "+entidadNombre+": "+recurso);
			final String uri = urlServices+"/rest/"+entidadRest+"/{id}/"+recurso;
		    Map<String, S> params = new HashMap<String, S>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<String> recursoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, params);
		    recursoDecoded = Base64.decodeBase64(recursoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return recursoDecoded;
	}

	@Override
	public Result obtenerEntidadPorTipo(Integer cdEnv, String tipo) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo "+entidadNombre+" por tipo");
			final String uri = urlServices+"/rest/"+entidadRest+"/g/{cdEnv}/t/{tipo}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("cdEnv", cdEnv);
		    params.put("tipo", tipo);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<T> response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<T>() {}, params);
		    res.setEntity(response.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerEntidadPorEstado(Integer cdEnv, String estado) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo "+entidadNombre+" por estado");
			final String uri = urlServices+"/rest/"+entidadRest+"/g/{cdEnv}/s/{estado}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("cdEnv", cdEnv);
		    params.put("estado", estado);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<T> response = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<T>() {}, params);
		    res.setEntity(response.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarEntidad(T entidad) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando "+entidadNombre);
			final String uri = urlServices+"/rest/"+entidadRest;
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<T> request = new HttpEntity<T>(entidad, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    //TODO use generics
		    ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarEntidad(T entidad) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando "+entidadNombre);
			final String uri = urlServices+"/rest/"+entidadRest+"/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<T> request = new HttpEntity<T>(entidad, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarEntidad(T id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar "+entidadNombre);
			final String uri = urlServices+"/rest/"+entidadRest+"/delete/{id}";
		    Map<String, T> params = new HashMap<String, T>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
}
