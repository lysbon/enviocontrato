package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Result;

public interface PlantillaService {

	public Result obtenerPlantillas() throws ServiceException;

	public Result obtenerPlantillaPorCodigo(Long id) throws ServiceException;
		
	public byte[] obtenerPDF(Long id) throws ServiceException;
		
	public Result obtenerPlantillasAsociadas(Long cdKit) throws ServiceException;
	
	public Result guardarPlantilla(Plantilla plantilla) throws ServiceException;
	
	public Result actualizarPlantilla(Plantilla plantilla) throws ServiceException;
	
	public Result eliminarPlantilla(Long id) throws ServiceException;
	
	public void generarSVGfromPlantilla(Long id) throws ServiceException;
	
	public void generarSVGPagefromPlantilla(Long id, int numPage) throws ServiceException;

	public Result actualizarPlantillaPDF(Long cdPlantilla, String filename) throws ServiceException;
	
	public Result actualizarPlantillaPDFCadena(Long cdPlantilla, String filename) throws ServiceException;
	
}
