<div class="bandabbva"></div>
<section>
	<div class="container">
		<div class="header clearfix">
			<nav>
				<ul class="nav nav-pills pull-right">
					<c:if test="${admin}">
						<li class="navIni" role="presentation"><a href="${contextPath}/">SQL</a></li>
					</c:if>
					<c:if test="${not empty cdEnv}">
						<li class="navIni" role="presentation"><a href="${contextPath}/">Inicio</a></li>					
	    				<li class="navKit" role="presentation"><a href="${contextPath}/kit/${cdEnv}">      Kits</a></li>
						<li class="navTpl" role="presentation"><a href="${contextPath}/plantilla/${cdEnv}">Plantillas</a></li>
						<li class="navRes" role="presentation"><a href="${contextPath}/resource/${cdEnv}"> Recursos</a></li>
					</c:if>
				</ul>
			</nav>
			<h3 class="text-muted">Env�o de Contratos</h3>
		</div>
	</div>
	
	<input type="hidden" name="cdEnv"   id="cdEnv"   value="${cdEnv}"/>
	<input type="hidden" name="baseUrl" id="baseUrl" value="${contextPath}"/>
	
</section>