<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>

<%@include file="base.jsp"%>

</head>
 
<body>	

<%@include file="header.jsp"%>

<div class="container">	
      <ol class="breadcrumb">
		  <li><a href="<%=request.getContextPath()%>">Inicio</a></li>
		  <li><a href="<%=request.getContextPath()%>/plantilla/">Plantillas</a></li>
		  <li><a href="#">Secci�n</a></li>
		</ol>      
</div>

<div class="container"> 

	<div class="panel panel-default">
	<div class="panel-heading">
		<c:if test="${not empty seccion}">
    		<h3 class="panel-title">Editar Secci�n</h3>
    	</c:if>
    	<c:if test="${empty seccion}">
    		<h3 class="panel-title">Nueva Secci�n</h3>
    	</c:if>
  	</div>
	<div class="panel-body">
    	<form>
    		<div class="row">
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="txtCodigo" class="control-label">C�digo</label>
						<input type="hidden" value="${seccion.cdSeccion}" id="txtCodigo1">
						<c:if test="${empty seccion}">
							<input type="text" class="form-control" value="${seccion.cdSeccion}" id="txtCodigo2" placeholder="C�digo de Secci�n">
						</c:if>
						<c:if test="${not empty seccion}">
							<input type="text" disabled class="form-control" value="${seccion.cdSeccion}" id="txtCodigo2" placeholder="C�digo de Secci�n">
						</c:if>
					</div>
    			</div>
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="txtNombre" class="control-label">P�gina</label>
						<input type="text" class="form-control" value="${seccion.nuPagina}" id="txtNombre" placeholder="N�mero de P�gina">			
					</div>
    			</div>
    		</div>
			<button type="submit" class="btn btn-default">Guardar</button>
			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalSVG" id="btnCargaSVG">Carga SVG</button>
			<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modalGenSVG" id="btnGenerarSVG">Generar SVG</button>
		</form>
  	</div>
	</div>

	<c:if test="${not empty seccion}">
		<div class="panel panel-default">
		<div class="panel-heading">
	    	<h3 class="panel-title">Elementos</h3>
	  	</div>
		<div class="panel-body">		
		<table id="tblItems" class="table table-striped" cellspacing="0" width="100%">
			<thead>
	            <tr>
	                <th>C�digo</th>
	                <th>Nombre</th>
	                <th>Activo</th>
	                <th>Tipo</th>
	                <th>X</th>
	                <th>Y</th>
	            </tr>
	        </thead>
	        <tbody>
			</tbody>
		</table>
		</div>
		</div>
	</c:if> 
  <hr>
  <footer>
	<p>&copy; BBVA Continental 2017</p>
  </footer>
</div>

<div id="modalSVG" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="POST" action="../uploadFile/" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Cargar SVG</h4>
      </div>
      <div class="modal-body">
      	<div class="fileSetup">
      		Archivo a subir: <input type="file" name="file" id="txtFileName">		
        	<input type="hidden" name="name">
      	</div>
      	<div class="rptaSetup hidden">
      	</div>
      	<div class="selectSetup hidden">
      	
      		<table id="tblItemsCarga" class="table table-condensed table-hover" cellspacing="0" width="100%">
				<thead>
		            <tr>
		            	<th></th>
		                <th>Cod.</th>
		                <th>Nombre</th>
		                <th>Tipo</th>
		                <th>X</th>
		                <th>Y</th>
		                <th>Estado</th>
		                <th class="hidden">Resultado</th>
		            </tr>
		        </thead>
		        <tbody>				
				</tbody>
			</table>
      	</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btnSubir">Subir</button>
        <!--<button type="submit" class="btn btn-primary">Submit</button>-->
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="modalGenSVG" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Generar SVG</h4>
      </div>
      <div class="modal-body">
        	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="${seccionJs}"></script>
 
</body>
</html>