<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>
 
<%@include file="base.jsp"%>

</head>

<body>	

<%@include file="header.jsp"%>
 
<div class="container">	
	<ol class="breadcrumb">
		<li><a href="<%=request.getContextPath()%>">Inicio</a></li>
		<li><a href="#">Recursos</a></li>
	</ol>
</div>

<div class="container"> 
	<button type="submit" class="btn btn-default" id="txtNuevoRecurso">Nuevo Recurso</button>
	<c:if test="${not empty lists}">
		<table id="tblRecursos" class="table table-striped" cellspacing="0" width="100%">
			<thead>
	            <tr>
	                <th>Cod.</th>
	                <th>Ver</th>
	            </tr>
	        </thead>
	        <tbody>
				<c:forEach var="listValue" items="${lists}">
					<tr>
					<td>${listValue.cdItem}</td>
					<td>
						<button type="button" class="btn btn-default btn-xs" data-id="${listValue.cdItem}" data-toggle="modal" data-target="#modalRecurso" id="btnRecurso">
							<span class="glyphicon glyphicon-new-window" aria-hidden="true"></span>
						</button>
					</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</c:if> 
  <hr>
  <footer>
	<p>&copy; BBVA Continental 2017</p>
  </footer>
</div>

<div id="modalRecurso" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Recurso</h4>
      </div>
      <div class="modal-body">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="${recursosJs}"></script>
 
</body>
</html>