<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>

<%@include file="base.jsp"%>

</head>

<body>	

<%@include file="header.jsp"%>
 
<div class="container">	
      <ol class="breadcrumb">
		  <li><a href="<%=request.getContextPath()%>">Inicio</a></li>
		  <li><a href="<%=request.getContextPath()%>/plantilla/">Plantillas</a></li>
		  <li><a href="<%=request.getContextPath()%>/seccion/${item.cdSeccion}">Secci�n</a></li>
		  <li><a href="#">Elemento</a></li>
		</ol>
</div>

<div class="container"> 

	<div class="panel panel-default">
	<div class="panel-heading">
		<c:if test="${not empty item}">
    		<h3 class="panel-title">Editar Elemento</h3>
    	</c:if>
    	<c:if test="${empty item}">
    		<h3 class="panel-title">Nuevo Elemento</h3>
    	</c:if>
  	</div>
	<div class="panel-body">
    	<form>
    		<div class="row">
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="txtCodigo" class="control-label">C�digo</label>
						<input type="hidden" value="${item.cdItem}" id="txtCodigo1">
						<c:if test="${empty item}">
							<input type="text" class="form-control" value="${item.cdItem}" id="txtCodigo2" placeholder="C�digo de Item">
						</c:if>
						<c:if test="${not empty item}">
							<input type="text" class="form-control" disabled value="${item.cdItem}" id="txtCodigo2" placeholder="C�digo de Item">
						</c:if>
					</div>
    			</div>
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="txtNombre" class="control-label">Nombre</label>
						<input type="text" class="form-control" value="${item.nbNombreVar}" id="txtNombre" placeholder="Nombre de Variable">
					</div>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="selTipo" class="control-label">Tipo</label>
						<select class="form-control" id="selTipo">
							<c:forEach var="it" items="${tipos}">
		        				<option value="${it.key}" ${it.key == item.chTipo ? 'selected="selected"' : ''}>${it.value}</option>
		    				</c:forEach>
						</select>
					</div>
    			</div>
    			<div class="col-sm-6">
    				<div class="form-group">
						<label for="txtValor" class="control-label">Valor</label>
						<input type="text" class="form-control" value="${item.nbValorVar}" id="txtValor" placeholder="Valor por defecto">
					</div>
    			</div>
    		</div>
    		<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="txtX" class="control-label">X</label>
						<input type="text" class="form-control" value="${item.nuCoorX}" id="txtX" placeholder="">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="txtY" class="control-label">Y</label>
						<input type="text" class="form-control" value="${item.nuCoorY}" id="txtY" placeholder="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="txtAncho" class="control-label">Longitud m�xima</label>
						<input type="text" class="form-control" value="${item.nuAncho}" id="txtAncho" placeholder="Ancho">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="txtMascara" class="control-label">M�scara</label>
						<input type="text" class="form-control" value="${item.nbMask}" id="txtMascara" placeholder="M�scara">
					</div>
				</div>
			</div>
					
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group" style="display:hidden">
						<label for="txtX" class="control-label">Escala X</label>
						<input type="text" class="form-control" value="${item.nuScX}" id="txtScX" placeholder="">
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group" style="display:hidden">
						<label for="txtY" class="control-label">Escala Y</label>
						<input type="text" class="form-control" value="${item.nuScY}" id="txtScY" placeholder="">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="selFuente" class="control-label">Fuente</label>
						<select class="form-control" id="selFuente">
							<c:forEach var="it" items="${fuentes}">
		        				<option value="${it.key}" ${it.key == item.nbFuente ? 'selected="selected"' : ''}>${it.value}</option>
		    				</c:forEach>
						</select>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label for="selAlinear" class="control-label">Alinear</label>
						<select class="form-control" id="selAlinear">
							<c:forEach var="it" items="${alinear}">
		        				<option value="${it.key}" ${it.key == item.nuAlinear ? 'selected="selected"' : ''}>${it.value}</option>
		    				</c:forEach>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<label for="selEstado" class="control-label">Estado</label>
						<select class="form-control" id="selEstado">
							<c:forEach var="it" items="${estados}">
		        				<option value="${it.key}" ${it.key == item.stActivo ? 'selected="selected"' : ''}>${it.value}</option>
		    				</c:forEach>
						</select>
					</div>	
				</div>
				<div class="col-sm-6">
					<div class="form-group" style="display:hidden">
						<label for="txtIdxCliente" class="control-label">�ndice Cliente</label>
						<input type="text" class="form-control" value="${item.nuIdxCliente}" id="txtIdxCliente" placeholder="�ndice de cliente">
					</div>
				</div>
			</div>
			<button type="submit" class="btn btn-default">Guardar</button>
		</form>
  	</div>
	</div>
  <hr>
  <footer>
	<p>&copy; BBVA Continental 2017</p>
  </footer>
</div>

<script src="${itemJs}"></script>

</body>
</html>