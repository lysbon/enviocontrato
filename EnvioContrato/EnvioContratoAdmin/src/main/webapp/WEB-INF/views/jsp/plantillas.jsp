<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>

<%@include file="base.jsp"%>

</head>

<body>

	<%@include file="header.jsp"%>

	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<%=request.getContextPath()%>">Inicio</a></li>
			<li><a href="">Plantillas</a></li>
		</ol>
	</div>
	
	<input type="hidden" id="txtCondFijo" value="${condFijo}">

	<div class="container">

		<table id="tblPlantillas" class="table table-striped" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Cod.</th>
					<th>Nombre</th>
					<th class="plantillaDes">Descripci&oacute;n</th>
					<th>Activo</th>
					<th>Correo</th>
					<th>FU</th>
					<th>Firma</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
		<hr>
		<footer>
			<p>&copy; BBVA Continental 2017</p>
		</footer>
	</div>

	<div id="modalPlantilla" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width: 700px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 id="plantilla-title" class="modal-title">Nueva Plantilla</h4>
				</div>
				<div class="modal-body">
					<ul class="nav nav-tabs">
						<li id="tabHome" class="active"><a data-toggle="tab" href="#home">Datos</a></li>
						<li id="tabFlags"> <a data-toggle="tab" href="#flags">Flags</a></li>
						<li id="tabPDF">   <a data-toggle="tab" href="#pdf">Plantilla</a></li>
						<li id="tabDes">   <a data-toggle="tab" href="#des">Dise�o</a></li>
						<li id="tabItems" class="disabled"><a>Par�metros</a></li>
						<li id="tabCarga" class="disabled"><a>Carga</a></li>
					</ul>
					<br/>
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="txtCodigo" class="col-sm-2 control-label">C�digo</label>
									<div class="col-sm-10">
										<input type="hidden" id="accion">
										<input type="hidden" id="txtCdPlantilla">
										<input type="text" class="form-control" id="txtCodigo"
											placeholder="C�digo de Plantilla">
									</div>
								</div>
								<div class="form-group">
									<label for="txtNombre" class="col-sm-2 control-label">Nombre</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="txtNombre" placeholder="Nombre de Plantilla">
									</div>
								</div>
								<div class="form-group">
									<label for="txtDescripcion" class="col-sm-2 control-label">Descrici�n</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="txtDescripcion" placeholder="Descripci�n de Plantilla" maxlength="40">
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="form-group">
							    			<div class="col-sm-4">
												<div class="checkbox">
													<label>
														<input type="hidden" id="txtEnvCorreo">
														<input type="checkbox" id="cbEnvCorreo" value="A"> Env�a Correo														
													</label>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="checkbox">
													<label>
														<input type="hidden" id="txtFileUnico">
														<input type="checkbox" id="cbFileUnico" value="A"> Env�a FileUnico
													</label>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="checkbox">
													<label>
														<input type="hidden" id="txtSigned">
														<input type="checkbox" id="cbSigned" value="A"> Firma Digital														
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="txtNombreCorreo" class="col-sm-2 control-label">Nom. Correo</label>
									<div class="col-sm-10">
										<input type="text" disabled class="form-control" id="txtNombreCorreo" placeholder="Nombre de adjunto por correo">									
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<label for="selEstado" class="control-label">Estado</label>
										<select class="form-control" id="selEstado">
											<c:forEach var="it" items="${estados}">
						        				<option value="${it.key}">${it.value}</option>
						    				</c:forEach>
										</select>
									</div>
									<div class="col-sm-6">
										<label for="selTipoCond" class="control-label">Condici�n</label>
										<select class="form-control" id="selTipoCond">
											<c:forEach var="it" items="${cond}">
						        				<option value="${it.key}">${it.value}</option>
						    				</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<label for="selTipoConstr" class="control-label">Tipo de Construcci�n</label>
										<select class="form-control" id="selTipoConstr">
											<c:forEach var="it" items="${tipoConst}">
						        				<option value="${it.key}">${it.value}</option>
						    				</c:forEach>
										</select>
									</div>
									<div class="col-sm-6">
										<label for="selTipoPlantilla" class="control-label">Nivel de Plantilla</label>
										<select class="form-control" id="selTipoPlantilla">
											<c:forEach var="it" items="${nivelPlantilla}">
						        				<option value="${it.key}">${it.value}</option>
						    				</c:forEach>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<label for="txtTipoFileunico" class="control-label">FileUnico</label>
										<input type="text" class="form-control" id="txtTipoFileunico" placeholder="C&oacute;digo de Tipo Fileunico" onchange="actualizarDesFileunico()">
									</div>
									<div class="col-sm-6">	
										<label for="txtTipoFileunico" class="control-label">Descripci&oacute;n</label>
										<input type="text" class="form-control" id="txtDesTipoFileunico" disabled>
									</div>
								</div>
							</form>
						</div>
						<div id="flags" class="tab-pane fade">
							<table id="tblFlagsPlantilla" class="table table-striped" cellspacing="0" width="100%">
								<thead>
						            <tr>
						                <th>C�digo</th>
						                <th>Par�metro</th>
						                <th>Valor</th>
						                <th>Grupo</th>
						                <th>Operador</th>
						                <th>Acciones</th>
						            </tr>
						        </thead>
						        <tbody>
								</tbody>
							</table>
						</div>
						<div id="pdf" class="tab-pane fade">
							<form method="POST" action="../../uploadFile/"
								enctype="multipart/form-data">
								<input type="hidden" id="txtPDFPlantilla">							
								<div class="form-group fileSetup">
								    <label for="txtFileNamePDF">Archivo PDF:</label>
								    <input type="file" name="file" id="txtFileNamePDF" onchange="validateFilename(this)">
									<input type="hidden" name="namePDF">
								</div>
								<div class="rptaSetupPDF hidden"></div>
								<br/>
							</form>
						</div>
						<div id="des" class="tab-pane fade">
							<table id="tblSecciones" class="table table-striped" cellspacing="0" width="100%">
								<thead>
						            <tr>
						                <th>C�digo</th>
						                <th>P�gina</th>
						                <th>Visor</th>
						                <th>Acciones</th>
						            </tr>
						        </thead>
						        <tbody>
								</tbody>
							</table>
							<form class="form-horizontal">
								<div id="panelSeccion" class="form-group hidden">
									<label for="txtCodigo" class="col-sm-2 control-label">P&aacute;gina</label>
									<div class="col-sm-4">
										<input type="hidden" id="accionSeccion">
										<input type="hidden" id="txtCdSeccion">
										<input type="hidden" id="txtCdSeccionPlantilla">
										<input type="text" class="form-control" id="txtNuPagina"
											placeholder="N&uacute;mero de p&aacute;gina">
									</div>
								</div>
							</form>
						</div>
						<div id="par" class="tab-pane fade">
							<table id="tblItems" class="table table-striped" cellspacing="0" width="100%">
								<thead>
						            <tr>
						            	<th>C�digo</th>
						                <th>Nombre</th>
						                <th>Activo</th>
						                <th>Tipo</th>
						                <th>X</th>
						                <th>Y</th>
						                <th>Acciones</th>
						            </tr>
						        </thead>
						        <tbody>
								</tbody>
							</table>
						</div>
						<div id="car" class="tab-pane fade">							
							<form method="POST" action="../../uploadFile/"
								enctype="multipart/form-data">
								<input type="hidden" id="txtSeccion">								
								<div class="fileSetup">
									Archivo a subir: <input type="file" name="file" id="txtFileNameSVG">
									<input type="hidden" name="name">
								</div>
								<div class="rptaSetup hidden"></div>
							</form>
							
							<div class="selectSetup hidden">			
								<table id="tblItemsCarga"
									class="table table-condensed table-hover" cellspacing="0"
									width="100%">
									<thead>
										<tr>
											<th></th>
											<th>Cod.</th>
											<th>Nombre</th>
											<th>Tipo</th>
											<th>X</th>
											<th>Y</th>
											<th>Estado</th>
											<th class="hidden">Resultado</th>
											<th class="hidden">json</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<br/>
							<div class="panelScripts panel-group hidden">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h5 class="panel-title">
											<a data-toggle="collapse" href="#collapseSQL">Scripts SQL</a>
										</h5>
									</div>
									<div id="collapseSQL" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="scriptSetup">
												<div class="form-group">
												    <label for="scriptActualizar">Script de Actualizacion</label>
												    <textarea class="form-control" id="scriptActualizar" rows="3"></textarea>
												</div>
												<div class="form-group">
												    <label for="scriptAlta">Script de Alta</label>
												    <textarea class="form-control" id="scriptAlta" rows="3"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					
					<button type="button" class="btn btn-primary btn-home" id="btnGuardarPlantilla">Guardar</button>
					
					<button type="button" class="btn btn-primary btn-des hidden" id="btnGuardarSeccion">Guardar</button>
					
					<button type="button" class="btn btn-primary btn-pdf hidden" id="btnActPDF">Actualizar PDF</button>
					
					<button type="button" class="btn btn-primary btn-car2 hidden" id="btnScript">Generar Script</button>
					<button type="button" class="btn btn-primary btn-car2 hidden" id="btnAplicar">Aplicar</button>
					<button type="button" class="btn btn-primary btn-car  hidden" id="btnSubir">Subir</button>
					
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<div id="modalNuevoFlag" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width: 500px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Nuevo Flag/Plantilla</h4>
				</div>
				<div class="modal-body">
					<table id="tblFlags" class="table table-striped" cellspacing="0" width="100%">
						<thead>
				            <tr>
				                <th>C�digo</th>
				                <th>Variable</th>
				                <th>Operador</th>
				                <th>Valor</th>
				            </tr>
				        </thead>
				        <tbody>
						</tbody>
					</table>
					<br/>
					<form class="form-horizontal">
						<div class="form-group">
							<label for="txtValorGrupo" class="col-sm-2 control-label">Grupo</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="txtValorGrupo"
									placeholder="Valor/Grupo">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary" id="btnGuardarFlagPlantilla">Guardar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h4 class="modal-title" id="myModalLabel">Confirmar eliminaci�n</h4>
                </div>
                <div class="modal-body">
                    <p>Est&aacute;s a punto de eliminar un(a) <b><i class="title"></i></b>.</p>
                    <p>�Deseas continuar?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-danger btn-ok">Eliminar</button>
                </div>
            </div>
        </div>
    </div>

	<script src="${plantillasJs}"></script>

</body>
</html>