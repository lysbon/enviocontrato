$(document).ready(function() {
    
	var table = dataTable('#tblRecursos');
	
	$('#modalRecurso').on('shown.bs.modal', function (e) {
		var cdItem = $(e.relatedTarget).data('id');
		$('#modalRecurso .modal-body').html('<img src="img/'+cdItem+'.jpg"/>');
	});
	
    $('#tblRecursos tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
    
} );