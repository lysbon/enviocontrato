function format ( d ) {
    return 'Resultado: '+d[d.length-1];
}

$(document).ready(function() {
	
	var cdSeccion = $('#txtCodigo1').val();
	
	$('#modalGenSVG').on('shown.bs.modal', function () {
		$.getJSON( 'gensvg', {id : cdSeccion} )
    	.done(function( data, textStatus, jqXHR ) {
    		$('#modalGenSVG .modal-body').html('Se gener&oacute; el archivo correctamente.');
    	})
    	.fail(function( jqXHR, textStatus, errorThrown ) {
    		$('#modalGenSVG .modal-body').html('Hubo un error al generar el archivo.');
    		if ( console && console.log ) {
    			console.log( "Algo ha fallado: " +  textStatus);
    		}
    	});
	});
	
	$('#btnSubir').on('click',function () {
		var data = new FormData();
		$.each($('#txtFileName')[0].files, function(i, file) {
		    data.append('file', file);
		});
		data.append('name','prueba');
		$.ajax({
		    url: '../uploadFile/',
		    data: data,
		    cache: false,
		    contentType: false,
		    processData: false,
		    type: 'POST',
		    success: function(data){
		        $('#modalSVG .rptaSetup').html('Se subi&oacute; el archivo correctamente. Procesando ...');
		        $('#modalSVG .rptaSetup').removeClass('hidden');
		        $('#modalSVG .fileSetup').addClass('hidden');
		        $('#btnSubir').text('Procesando...');
		        $('#btnSubir').addClass('disabled');
		        $.getJSON( 'svg', {id : cdSeccion, file : data} )
		    	.done(function( data, textStatus, jqXHR ) {
		    		$('#modalSVG .rptaSetup').addClass('hidden');
		    		$('#btnSubir').removeClass('disabled');
		    		$('#btnSubir').text('Confirmar');
		    		$.each(data, function( index, value ) {
		    			var detailClass = '';
		    			if(value.stSVGCarga == 'M'){
		    				detailClass = 'details-control';
		    			}
		    			var htmlRow=
		    				'<tr data-id="'+index+'">'+
		    				'<td><input id="cbItem'+index+'" type="checkbox" value="A"></td>'+
		    				'<td>'+value.cdItem+'</td>'+
		    				'<td>'+value.nbNombreVar+'</td>'+
		    				'<td>'+value.desTipo+'</td>'+
		    				'<td>'+value.nuCoorX+'</td>'+
		    				'<td>'+value.nuCoorY+'</td>'+		    				
		    				'<td class="'+detailClass+'">'+value.desEstadoSvg+'</td>'+
		    				'<td class="hidden">'+value.resultado+'</td>'+
		    				'</tr>';
		    			$('#tblItemsCarga tbody').append(htmlRow);
		    			$('#cbItem'+index).attr('checked', true);
		    		});
		    		
		    		var table = dataTableDefecto('#tblItemsCarga',10);
		    		
		    		/*
		    		$('#tblItemsCarga tbody').on( 'click', 'tr', function () {
		    	        if ( $(this).hasClass('selected') ) {
		    	            $(this).removeClass('selected');
		    	            $('#txtDescripcion').val('');
		    	        } else {
		    	            table.$('tr.selected').removeClass('selected');
		    	            $(this).addClass('selected');  
		    	            var desc = $(this).find('p').html();
		    	            $('#txtDescripcion').val(desc);
		    	        }
		    	    } );*/
		    		
		    		// Array to track the ids of the details displayed rows
		    	    var detailRows = [];
		    	    $('#tblItemsCarga tbody').on('click','tr td.details-control', function () {
		    	        var tr = $(this).closest('tr');
		    	        var row = table.row(tr);
		    	        var idx = $.inArray(tr.attr('id'),detailRows);		    	 
		    	        if(row.child.isShown()) {
		    	            tr.removeClass('details');
		    	            row.child.hide();
		    	            detailRows.splice(idx,1);
		    	        }else{
		    	            tr.addClass('details');
		    	            row.child(format(row.data())).show();
		    	            //row.child(format(row.)).show();
		    	            if (idx === -1) {
		    	                detailRows.push(tr.attr('id'));
		    	            }
		    	        }
		    	    });
		    	    table.on('draw', function () {
		    	        $.each(detailRows, function (i,id) {
		    	            $('#'+id+' td.details-control').trigger('click');
		    	        });
		    	    });
		    		
		    		$('#modalSVG .selectSetup').removeClass('hidden');
		    	})
		    	.fail(function( jqXHR, textStatus, errorThrown ) {
		    		if ( console && console.log ) {
		    			console.log( "Algo ha fallado: " +  textStatus);
		    		}
		    	});
		    }
		});
    });
	
	if(cdSeccion !=null){
    	
    	$.getJSON( 'items/'+cdSeccion, {} )
    	.done(function( data, textStatus, jqXHR ) {    		
    		$.each(data, function( index, value ) {
    			var htmlRow=
    				'<tr>'+
    				'<td><a href="../item/'+value.cdItem+'">'+value.cdItem+'</a></td>'+
    				'<td>'+value.nbNombreVar+'</td>'+
    				'<td>'+value.symActivo+'</td>'+
    				'<td>'+value.desTipo+'</td>'+  				
    				'<td>'+value.nuCoorX+'</td>'+
    				'<td>'+value.nuCoorY+'</td>'+
    				'</tr>';
    			$('#tblItems tbody').append(htmlRow);
    		});
    		dataTable('#tblItems');
    	})
    	.fail(function( jqXHR, textStatus, errorThrown ) {
    		if ( console && console.log ) {
    			console.log( "Algo ha fallado: " +  textStatus);
    		}
    	});
    }
} );