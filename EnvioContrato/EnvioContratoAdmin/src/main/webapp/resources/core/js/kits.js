var kitSelected=null;
var tableKit = null;

$(document).ready(function() {
	$('.navKit').addClass('active');
	
	binds();
	
	cargarListaKits();
	
} );

function binds(){
	
	$('#tblKits tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected'); 
            limpiarBarra();
        } else {
        	$('#btnEditarKit').prop("disabled", false);
        	$('#btnEliminarKit').prop("disabled", false);
        	tableKit.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            kitSelected = $(this).find('input').val();
        }
    });
	
	$('#btnGuardarKit').on( 'click', function () {
		guardarKit();
	});
	
}

function initTablaKit(){
	
	tableKit = $('#tblKits').DataTable({
    	"ordering"     : false,
    	"lengthChange" : false,
        "info"         : false,
        "dom"          : '<"toolbar">frtip',
        "language"     : {  "paginate": {
			             		"next": "Sig.",
			             		"previous": "Ant."
        					},
        					"search": "Buscar:"
        				 },
		"columns": [
		            { "width": "10%" },
		            { "width": "50%" },
		            { "width": "10%" },
		            { "width": "10%" },
		            { "width": "10%" },
		            { "width": "10%" }
		          ]
    });
	
	var butonera = 
		'<div class="btn-group" role="group" aria-label="...">'+
		'<button type="button" class="btn btn-sm btn-default"        id="btnNuevoKit">'+
			'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo'+
		'</button> '+
		'<button type="button" disabled="disabled" class="btn btn-sm btn-default" id="btnEditarKit">'+
			'<span class="btnedit glyphicon glyphicon-cog" aria-hidden="true"></span>'+
		'</button> '+
		'<button type="button" disabled="disabled" class="btn btn-sm btn-default" id="btnEliminarKit">'+
			'<span class="btnremove glyphicon glyphicon-minus" aria-hidden="true"></span>'+
		'</button> '+			
		'</div>';
	
	$("div.toolbar").html(butonera);
	
//	$('#btnEditarKit').on( 'click', function () {
//		editarKit();
//	});
//	$('#btnNuevoKit').on( 'click', function () {
//		nuevoKit();
//	});
	
	$('#btnNuevoKit:not(.bound)').addClass('bound').on('click',  function () {
		nuevoKit();
	});
	$('#btnEditarKit:not(.bound)').addClass('bound').on('click',  function () {
		editarKit();
	});
	
}

function cargarListaKits(){
	var cdEnv = $('#cdEnv').val();
	var baseUrl = $('#baseUrl').val();
	
	destruirTabla('#tblKits');
	
	$.getJSON( baseUrl+'/rest/kit/all/'+cdEnv, {} )
	.done(function( data, textStatus, jqXHR ) {
		
		$.each(data, function( index, value ) {
			var htmlRow =
				'<tr>'+
				'<td><input type="hidden" value="'+value.cdKit+'"/>'+value.cdCodigo+'</td>'+
				'<td>'+value.nbNombreKit+'</td>'+
				'<td>'+check(value.stEnvCorreo)+'</td>'+
				'<td>'+check(value.stFileunico)+'</td>'+
				'<td>'+check(value.flValHuella)+'</td>'+
				'<td>'+
				'<span class="btnedit   glyphicon glyphicon-cog" aria-hidden="true"></span> '+
				'<span class="btnremove glyphicon glyphicon-minus" aria-hidden="true"></span>'+
				'</td>'+
				'</tr>';
			$('#tblKits tbody').append(htmlRow);
		});
		initTablaKit();
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function nuevoKit(){
	limpiarModal();
	$("#modalKit").modal();	
	cargarListaPlantillas();
	$('#accion').val('nuevo');
}

function cargarListaPlantillas(){
	var baseUrl = $('#baseUrl').val();
	destruirTabla('#tblPlantillas');
	$.getJSON( baseUrl+'/rest/plantilla/all', {} )
	.done(function( data, textStatus, jqXHR ) {
		$.each(data, function( index, value ) {
			var htmlRow = '<tr>';
			htmlRow = htmlRow +				
				'<td><input id="cbPlantilla'+index+'" type="checkbox" value="'+value.cdPlantilla+'"></td>'+
				'<td>'+value.nbNombreFormato+'</td>'+
				'<td>'+value.nbDescripcion+'</td>'+
				'<td>'+check(value.stActivo)+'</td>'+
				'</tr>';
			$('#tblPlantillas tbody').append(htmlRow);
		});
		dataTable('#tblPlantillas',7);
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function resetearTabs(){
	$('#modalKit ul.nav-tabs li').removeClass('active').removeClass('in');
	$('#tabDatos').addClass('active').addClass('in');
	$('#modalKit div.tab-content').children().removeClass('active').removeClass('in');
	$('#home').addClass('active').addClass('in');
}

function editarKit(){
	
	limpiarModal();
	$('#accion').val('editar');
	
	var baseUrl = $('#baseUrl').val();
	var cdKit = kitSelected;
	var cdEnv = $('#cdEnv').val();
	
	$.getJSON( baseUrl+'/rest/kit/'+cdKit, {} )
	.done(function( data, textStatus, jqXHR ) {
		$('.panel-title').html("Editar Kit");
		$('#txtCdKit').val(data.cdKit);
		$('#txtCodigo').val(data.cdCodigo);
		$('#txtNombre').val(data.nbNombreKit);
		if(data.stEnvCorreo=='A'){
			$('#cbEnvCorreo').prop('checked', true);
		}
		if(data.stFileunico=='A'){
			$('#cbFileUnico').prop('checked', true);
		}
		if(data.flValHuella=='A'){
			$('#cbValHuella').prop('checked', true);
		}
		$('#selPlantillaCorreo').val(data.nbCorreoEngine);
		$('#txtAsuntoCorreo').val(data.nbCorreoAsunto);
		$('#btnEditarCorreo').removeClass('hidden');
		$('#btnEditarPlantillas').removeClass('hidden');
		$('#btnLimpiar').removeClass('hidden');
		$('#txtCodigo').attr('disabled', true);
		
		$("#modalKit").modal();
		
		//cargamos lista de plantillas
		$.getJSON( baseUrl+'/rest/kit/correo/'+cdEnv+'/'+cdKit, {} )
    	.done(function( data, textStatus, jqXHR ) {
    		$('#txtAreaCorreo').val(data);
    	})
    	.fail(function( jqXHR, textStatus, errorThrown ) {
    		if ( console && console.log ) {
    			console.log( "Algo ha fallado: " +  textStatus);
    		}
    	});
		
		destruirTabla('#tblPlantillas');
		//cargamos plantilla de correo
		$.getJSON( baseUrl+'/rest/kit/'+cdKit+'/plantillas', {} )
    	.done(function( data, textStatus, jqXHR ) {
    		$.each(data, function( index, value ) {
    			var htmlRow = '<tr>';
    			if(value.asociado){
    				htmlRow = '<tr class="selectedItem">';
    			}
    			htmlRow = htmlRow +		
    				'<td><input id="cbPlantilla'+index+'" type="checkbox" value="'+value.cdPlantilla+'"></td>'+
    				'<td>'+value.nbNombreFormato+'</td>'+
    				'<td>'+value.nbDescripcion+'</td>'+
    				'<td>'+check(value.stActivo)+'</td>'+
    				'</tr>';
    			$('#tblPlantillas tbody').append(htmlRow);
    			if(value.asociado){
    				$('#cbPlantilla'+index).attr('checked', true);
    			}
    			
    		});
    		
    		$('#tblPlantillas tbody').on('click','tr', function () {
    	        $(this).toggleClass('selectedItem');
    	        if($(this).hasClass('selectedItem')){
    	        	var chbxs = $(this).find(':checkbox');
    	        	chbxs.prop('checked', true);
    	        }else{
    	        	var chbxs = $(this).find(':checkbox');
    	        	chbxs.prop('checked', false);
    	        }
    	    } );
    		
    		dataTable('#tblPlantillas',7);
    	})
    	.fail(function( jqXHR, textStatus, errorThrown ) {
    		if ( console && console.log ) {
    			console.log( "Algo ha fallado: " +  textStatus);
    		}
    	});
	}).
	fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function limpiarBarra(){
	$('#btnEditarKit').prop("disabled", true);
	$('#btnEliminarKit').prop("disabled", true);
}
function limpiarModal(){
	$('.panel-title').html("Nuevo Kit");
	$('#txtCodigo').val('');
	$('#txtNombre').val('');
	$('#cbEnvCorreo').attr('checked', false);
	$('#cbFileUnico').attr('checked', false);
	$('#cbValHuella').attr('checked', false);
	$('#selPlantillaCorreo').val('');
	$('#txtAsuntoCorreo').val('');
	$('#btnEditarCorreo').addClass('hidden');
	$('#btnEditarPlantillas').addClass('hidden');
	$('#btnLimpiar').addClass('hidden');
	$('#txtCodigo').attr('disabled', false);
	$('#txtAreaCorreo').val('');
	$('#tblPlantillas tbody').html('');
	resetearTabs();
};

function validarKit(){
	if($('#txtCodigo').val().length > 8){
		alert('El campo código debe tener un tamaño de 8 carácteres.');
		return false;
	}
	return true;
}

function obtenerListaPlantillas(){
	var tableItems = $('#tblPlantillas').DataTable();
	var data = tableItems.rows('.selectedItem').data();
	var result = [];
	$(data).each(function(){
		result.push($(this)[0]);
	});
	return result;
}

function guardarKit(){
	var cdEnv = $('#cdEnv').val();
	var baseUrl = $('#baseUrl').val();
	if(validarKit()){
		var listPlantillas = [];
		var plantillas2 = obtenerListaPlantillas();
		$.each(plantillas2, function( index, value ) {
			listPlantillas[index] = {cdPlantilla:$(value).val()};
		});
		var url = baseUrl+'/rest/kit';
		if($('#accion').val()=='editar'){
			url = baseUrl+'/rest/kit/update';
		}
		var data = {
			cdKit : $('#txtCdKit').val(),
			cdCodigo : $('#txtCodigo').val(),
			stFileunico : $("#cbFileUnico").is(':checked') ? $('#cbFileUnico').val() : '',
			flValHuella : $("#cbValHuella").is(':checked') ? $('#cbValHuella').val() : '',
			nbNombreKit : $('#txtNombre').val(),
			listTemplates : listPlantillas,			
			grupoKit: {
				cdEnv : cdEnv,
				cdKit : $('#txtCdKit').val(),
				stEnvCorreo : $("#cbEnvCorreo").is(':checked') ? $('#cbEnvCorreo').val() : '',
				nbCorreoEngine : $('#selPlantillaCorreo').val(),
				nbCorreoAsunto : $('#txtAsuntoCorreo').val(),
				lbCorreoPlantilla : $('#txtAreaCorreo').val()
			}
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: url,
			data: JSON.stringify(data),
			success: function( data, textStatus, jqXHR ) {
				if($('#accion').val()=='editar'){
					cargarListaKits();
					alert('Se guardo correctamente');
					$("#modalKit").modal('hide');
				}else{
					var dataAsoc = {
						cdKit : data,
						cdEnv : cdEnv
					};
					$.ajax({
						type: "POST",
						contentType: "application/json",
						url: baseUrl+'/rest/grupo/kit/add',
						data: JSON.stringify(dataAsoc),
						success: function( data, textStatus, jqXHR ) {
							cargarListaKits();
							alert('Se guardo correctamente');
							$("#modalKit").modal('hide');
						},
						dataType: 'json'
					});
				}
			},
			dataType: 'json'
		});
	}	
}