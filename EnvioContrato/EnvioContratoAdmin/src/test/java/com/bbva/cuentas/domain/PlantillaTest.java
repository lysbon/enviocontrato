package com.bbva.cuentas.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.comparator.PlantillaComparator;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Constants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class PlantillaTest {

	@Test
	public void testOrderPlantilla(){
		List<Plantilla> listPlantillas = new ArrayList<Plantilla>();
		
		List<String> estados = new ArrayList<String>();
		List<String> nombreFormatos = new ArrayList<String>();
		List<Long> codigos = new ArrayList<Long>();
		estados.add(Constants.ST_INACTIVO);
		estados.add(Constants.ST_ACTIVO);
		nombreFormatos.add("BG999");
		nombreFormatos.add("BG777");
		codigos.add(2l);
		codigos.add(1l);
		
		for(Long codigo : codigos){
			for(String nombreFormato : nombreFormatos){
				for(String estado : estados){
					Plantilla plantilla = new Plantilla();
					plantilla.setCdPlantilla(codigo);
					plantilla.setNbNombreFormato(nombreFormato);
					plantilla.setStActivo(estado);
					listPlantillas.add(plantilla);
				}
			}
		}
		System.out.println("BEFORE");
		print(listPlantillas);
		System.out.println("AFTER");
		Collections.sort(listPlantillas,new PlantillaComparator());
		print(listPlantillas);
		
	}
	
	public void print(List<Plantilla> plantillas){
		if(plantillas!=null){
			System.out.println("\nSize - "+plantillas.size());
			for(Plantilla plantilla : plantillas){
				System.out.format("%4s%24s%4s\n", 
						plantilla.getCdPlantilla(),
						plantilla.getNbNombreFormato(),
						plantilla.getStActivo());
			}
		}
		
	}
	
}
