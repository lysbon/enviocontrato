package com.bbva.cuentas.service.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.util.TestParams;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class KitServiceTest {

	@Autowired
	KitService kitService;
	@Autowired
	EnviromentService envService;
	
	@Test
	@SuppressWarnings("unchecked")
	public void obtenerKitsTest() throws ServiceException{
		Result result = null;
		result = envService.obtenerEnvs();
		List<Enviroment> envs = (List<Enviroment>)result.getEntities();
		for(Enviroment env: envs) {
			result = kitService.obtenerKits(env.getCdEnv());
			assertThat(result.getEntities().isEmpty(),is(false));
			for(Kit kit : (List<Kit>)result.getEntities()) {
				validateKit(kit);
			}
		}
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void obtenerKitPorIdTest() throws ServiceException{
		Result result = kitService.obtenerKits(TestParams.CD_ENV_PROD);
		assertThat(result.getEntities().isEmpty(),is(false));
		List<Kit> kits = (List<Kit>)result.getEntities();
		result = kitService.obtenerKitPorId(kits.get(0).getCdKit());
		Kit kit = (Kit)result.getEntity();
		validateKit(kit);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void obtenerKitPorCodigoTest() throws ServiceException{
		Result result = kitService.obtenerKits(TestParams.CD_ENV_PROD);
		assertThat(result.getEntities().isEmpty(),is(false));
		List<Kit> kits = (List<Kit>)result.getEntities();
		result = kitService.obtenerKitPorCodigo(TestParams.CD_ENV_PROD, kits.get(0).getCdCodigo());
		Kit kit = (Kit)result.getEntity();
		validateKitCompleto(kit);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void obtenerMailTemplateTest() throws ServiceException{
		Result result = null;
		result = envService.obtenerEnvs();
		List<Enviroment> envs = (List<Enviroment>)result.getEntities();
		for(Enviroment env: envs) {
			result = kitService.obtenerKits(env.getCdEnv());
			assertThat(result.getEntities().isEmpty(),is(false));
			for(Kit kit : (List<Kit>)result.getEntities()) {
				result = kitService.obtenerKitPorCodigo(env.getCdEnv(),kit.getCdCodigo());
				Kit kitCompleto = (Kit)result.getEntity();
				result = kitService.obtenerMailTemplate(env.getCdEnv(),kitCompleto.getCdKit());
				String plantilla = (String)result.getEntity();
				if(StringUtils.equals(kitCompleto.getGrupoKit().getStEnvCorreo(),Constants.ST_ACTIVO)) {
					assertThat(StringUtils.isEmpty(plantilla),is(false));
				}
			}
		}
	}
	
	private void validateKitCompleto(Kit kit) {
		validateKit(kit);
		assertThat(kit.getGrupoKit()==null,is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbCorreoAsunto()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbCorreoEngine()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbRemitente()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbRemitenteCC()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getStEnvCorreo()),is(false));
	}
	
	private void validateKit(Kit kit) {
		assertThat(kit.getCdKit()==null,is(false));
		assertThat(kit.getCdCodigo()==null||kit.getCdCodigo().isEmpty(),is(false));
		assertThat(kit.getNbNombreKit()==null||kit.getNbNombreKit().isEmpty(),is(false));
		assertThat(kit.getStFileunico()==null||kit.getStFileunico().isEmpty(),is(false));
		
		assertThat(
				kit.getStSignBox()==null||
				kit.getStSignBox().isEmpty()||
				StringUtils.equals(kit.getStSignBox(),Constants.ST_ACTIVO)||
				StringUtils.equals(kit.getStSignBox(),Constants.ST_INACTIVO),is(true));
		
		assertThat(
				kit.getStHuella()==null||
				kit.getStHuella().isEmpty()||
				StringUtils.equals(kit.getStHuella(),Constants.ST_ACTIVO)||
				StringUtils.equals(kit.getStHuella(),Constants.ST_INACTIVO),is(true));
	}
	
}
