package com.bbva.cuentas.scripts;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class PaseFFMMDeclaracionJurada extends ScriptService{
	
	private static final Logger logger = LoggerFactory.getLogger(PaseFFMMDeclaracionJurada.class);
	
	@Autowired
	PlantillaService plantillaService;
	
	@Autowired
	SeccionService seccionService;
	
	@Autowired
	ItemService itemService;
	
	@Autowired
	KitService kitService;
	
	@Test
	public void agregarPlantillaVariable() throws ServiceException, JsonParseException, JsonMappingException, IOException{
		
		revertir();
		
		Integer codCanal = 2;
		Plantilla plantilla = crearPlantilla();
		actualizarPdf(plantilla);
		List<Kit> kitsFFMM = consultarKitsPorNombre(codCanal,"FM011");
		agregarPlantillaKits(plantilla,kitsFFMM);		
		actualizarCorreoKits(codCanal,kitsFFMM);
	}
	
	private void revertir() throws ServiceException {
		Result result = plantillaService.obtenerPlantillas();		
		List<Plantilla> plantillas = (List<Plantilla>) result.getEntities();
		for(Plantilla plantilla : plantillas) {
			if(plantilla.getNbNombreFormato().equals("FMDJS")) {
				revertirPlantilla(plantilla.getCdPlantilla());				
			}
		}
		revertirBandera("CFG0|P35");
	}

	private void actualizarCorreoKits(Integer codcanal,List<Kit> kitsFFMM) throws ServiceException {
		actualizarCorreoKits(codcanal,kitsFFMM,
				"D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm_dj\\html\\ffmm.html");
	}

	private void actualizarPdf(Plantilla plantilla) throws ServiceException {
		plantillaService.actualizarPlantillaPDFCadena(plantilla.getCdPlantilla(),
				"D:\\opt\\workspace\\Proyectos\\2019\\pi02\\ffmm_dj\\pdf\\FMDJS.pdf");
	}

	private Plantilla crearPlantilla() throws ServiceException, JsonParseException, JsonMappingException, IOException {
		
		Plantilla plantilla = new Plantilla();
		plantilla.setStActivo("A");
		plantilla.setStCondicion("V");
		plantilla.setStFileunico("A");
		plantilla.setStEnvCorreo("A");
		plantilla.setStTipoConstr("P");
		plantilla.setStSigned("A");
		plantilla.setNbNombreFormato("FMDJS");
		plantilla.setNbTipoFileunico("0529");
		plantilla.setNbNombreCorreo("Declaracion Jurada para apertura de FFMM");
		plantilla.setChTipoPlantilla("C");
		plantilla.setNbDescripcion("Declaracion Jurada FFMM");
		plantilla.setFechaRegistro(new Date());
		plantilla.setUsuarioRegistro("P023915");
		
		//Result result = plantillaService.guardarPlantilla(plantilla);		
		//plantilla.setCdPlantilla((Long)result.getEntity());
		Long cdPlantilla = guardarPlantillaTactico(plantilla);
		plantilla.setCdPlantilla(cdPlantilla);
		
		assertThat(plantilla.getCdPlantilla()!=null,is(true));
		
		ItemSection itemSection = new ItemSection();
		itemSection.setCdPlantilla(plantilla.getCdPlantilla());
		itemSection.setFechaRegistro(new Date());
		itemSection.setNuPagina(1);
		itemSection.setUsuarioRegistro("P023915");
		
		//Result result = seccionService.guardarSeccion(itemSection);		
		//itemSection.setCdSeccion((Long)result.getEntity());
		//Long cdSeccion = itemSection.getCdSeccion();
		
		Long cdSeccion = guardarSeccionTactico(itemSection);
		itemSection.setCdSeccion(cdSeccion);		
		
		assertThat(cdSeccion!=null,is(true));
		
		List<ItemStamp> items = new ArrayList<ItemStamp>();
		items.add(nuevoItem(134,682,null,null,"A","Calibri12","FMD0|P01","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(179,682,null,null,"A","Calibri12","FMD0|P02","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(299,682,null,null,"A","Calibri12","FMD0|P03","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(361,682,null,null,"A","Calibri12","FMD0|P04","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(123,555,null,null,"A","Calibri12","FMD0|P05","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(148,533,null,null,"A","Calibri12","FMD0|P06","T",cdSeccion,null,null,null,null,null,0));
		items.add(nuevoItem(122,439,null,null,"A","Calibri12","FMD0|P07","T",cdSeccion,null,null,null,null,null,0));
		for(ItemStamp item : items) {
			//itemService.guardarItem(item);
			guardarItemTactico(item);
		}
		
		if(plantilla.getStCondicion().equals("V")) {
			crearPlantillaBandera(plantilla,"CFG0|P35","S");
		}
		return plantilla;
	}
	
}
