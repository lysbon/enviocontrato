package com.bbva.cuentas.scripts;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.stringtemplate.v4.ST;

import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.util.TestParams;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class KitServiceCambioArticuloNuevoTest {

	private static final Logger logger = LoggerFactory.getLogger(KitServiceCambioArticuloNuevoTest.class);
	
	@Autowired
	private KitService kitService;
	
	private String[] patterns = {"SSS","SSN","SNS","SNN","NSS","NSN","NNS","NNN"};
	
	@SuppressWarnings("unchecked")
	@Test
	public void descargaPlantillas() throws ServiceException{
		Integer cdEnvNacar = 1;
		Integer cdEnvBXI   = 2;
		List<Kit> listaKits = null;
		Map<String,Kit> kitsFiltered = new HashMap<String,Kit>();
		
		try {
			Result result = kitService.obtenerKits(cdEnvNacar);
			listaKits = (List<Kit>)result.getEntities();
			for(Kit kit : listaKits){
				kitsFiltered.put(kit.getCdCodigo(),kit);
				Result resCorreo = kitService.obtenerMailTemplate(TestParams.CD_ENV_PROD,kit.getCdKit());
				kit.getGrupoKit().setLbCorreoPlantilla((String)resCorreo.getEntity());
			}
			result = kitService.obtenerKits(cdEnvBXI);
			listaKits = (List<Kit>)result.getEntities();
			for(Kit kit : listaKits){
				if(!kitsFiltered.containsKey(kit.getCdCodigo())) {
					kitsFiltered.put(kit.getCdCodigo(),kit);
					Result resCorreo = kitService.obtenerMailTemplate(TestParams.CD_ENV_PROD,kit.getCdKit());
					kit.getGrupoKit().setLbCorreoPlantilla((String)resCorreo.getEntity());
				}
			}
			
			List<Kit> kitsOut = new ArrayList<Kit>();
			for (Map.Entry<String, Kit> entry : kitsFiltered.entrySet()){
			    
				Kit kit = entry.getValue();
				String tplCorreo = kit.getGrupoKit().getLbCorreoPlantilla();
				
				if(StringUtils.contains(tplCorreo,"cumplimiento")) {
					
					String oldParrafo1 = " la Solicitud de Apertura de Cuenta y Reglamento de Tarjeta de D&eacute;bito";
					String oldParrafo2_1 = "las Cl&oacute;usulas Espec&iacute;ficas";
					String oldParrafo2_2 = "las Cl&aacute;usulas Espec&iacute;ficas";
					String oldParrafo3 = "y Cartilla de Informaci&oacute;n,";
					String oldParrafo4_1 = "y (iv) la Declaraci&oacute;n Jurada de Residencia Fiscal para Personas Naturales.";
					String oldParrafo4_2 = ", (iv) la Declaraci&oacute;n Jurada de Residencia Fiscal para Personas Naturales;";
					
					if(StringUtils.contains(tplCorreo,oldParrafo1) &&
					  (StringUtils.contains(tplCorreo,oldParrafo2_1)||StringUtils.contains(tplCorreo,oldParrafo2_2))&&
					   StringUtils.contains(tplCorreo, oldParrafo3) &&
					   StringUtils.contains(tplCorreo, oldParrafo4_1)||
					   StringUtils.contains(tplCorreo, oldParrafo4_2)){
						
//						logger.info("kit("+kit.getCdCodigo()+")"+"      ("+kit.getNbNombreKit()+")");
						
						tplCorreo = StringUtils.replace(tplCorreo,"</endif>","<endif>");
						
						int posIni  = tplCorreo.indexOf("<if(FCFG0P15&&FCFG0P18)><!-- KIT1 + RBA -->!>",0);
						int posFin  = 0;
						if(StringUtils.contains(tplCorreo, "<endif>\n")) {
							posFin  = tplCorreo.lastIndexOf("<endif>\n") + "<endif>".length();
						}
						if(StringUtils.contains(tplCorreo, "<endif>\r\n")) {
							posFin  = tplCorreo.lastIndexOf("<endif>\r\n") + "<endif>".length();
						}
						
//						logger.info("POS["+posIni+","+posFin+"]");
						
						String bloqueNuevo = 
								"\\<tr>\r\n" + 
								"	\\<td>\\<p style=\"padding-bottom: 10px;text-align: justify;line-height: 120%\">\r\n" + 
								"		Asimismo, en cumplimiento del art&iacute;culo 49&#x00b0; del Reglamento de Gesti&oacute;n de Conducta de Mercado del Sistema Financiero, te adjuntamos (i) la Solicitud de Apertura de Cuenta Persona Natural, (ii) las Cl&aacute;usulas Generales de Contrataci&oacute;n, y (iii) las Condiciones Espec&iacute;ficas y Cartilla de Informaci&oacute;n.\r\n" + 
								"		<if(FCFG0P15||FCFG0P18||FCFG0P33)>\r\n" + 
								"			\\<br>\\<br>Adicionalmente, se anexan los siguientes documentos:\r\n" + 
								"		<endif>\r\n" + 
								"		\\<ul>\r\n" + 
								"			<if(FCFG0P15)>	<!-- KIT1 -->!>\r\n" + 
								"				\\<li style=\"color:#0070C0;\">\\<span style=\"color:#7f7f7f;font-family:'Lucida Grande', Arial, sans-serif;font-size:13px;line-height:21px;\">La Declaraci&oacute;n Jurada de Residencia Fiscal para Personas Naturales.\\</li>\r\n" + 
								"			<endif>\r\n" + 
								"			<if(FCFG0P18)>	<!-- RBA  -->!>\r\n" + 
								"				\\<li style=\"color:#0070C0;\">\\<span style=\"color:#7f7f7f;font-family:'Lucida Grande', Arial, sans-serif;font-size:13px;line-height:21px;\">La Declaraci&oacute;n de Actividad Econ&oacute;mica. El documento deber&aacute; ser completado y entregado en cualquiera de nuestras oficinas en un plazo m&aacute;ximo de 30 d&iacute;as calendario.\\</li>\r\n" + 
								"			<endif>\r\n" + 
								"			<if(FCFG0P33)>	<!-- LPDP -->!>\r\n" + 
								"				\\<li style=\"color:#0070C0;\">\\<span style=\"color:#7f7f7f;font-family:'Lucida Grande', Arial, sans-serif;font-size:13px;line-height:21px;\">La constancia de tu aceptaci&oacute;n para el Tratamiento de tus Datos Personales.\\</li>\r\n" + 
								"			<endif>\r\n" + 
								"		\\</ul>\r\n" + 
								"	\\</p>\\</td>\r\n" + 
								"\\</tr>";
						
						String newTplCorreo = 
								tplCorreo.substring(0,posIni)+
								bloqueNuevo+
								tplCorreo.substring(posFin,tplCorreo.length());
						
//						System.out.println(tplCorreo);
//						System.out.println("-XXXXXX-");
//						System.out.println(newTplCorreo);
						
						int repetidos = cuantasVeces(newTplCorreo, "<endif>"); 
						
						logger.info(repetidos+"");
						assertThat(repetidos==5,is(true));

						writeFile(newTplCorreo,"d:\\tpl\\formato\\"+kit.getCdCodigo()+".html");
						
						for(int i=0;i<patterns.length;i++){
							Map<String,String> parametros = obtenerParametros(patterns[i]);
							List<Flag> flags = obtenerFlagsCorreo();
							
							String correoSalida = procesarPlantillaStrTpl(newTplCorreo,parametros,flags,true);
							
							//presentacion funcional
							correoSalida = StringUtils.replace(correoSalida, "cid:image1.jpg", "image1.jpg");
							correoSalida = StringUtils.replace(correoSalida, "cid:image2.jpg", "image2.jpg");
							
							writeFile(correoSalida,"d:\\tpl\\salida\\"+kit.getCdCodigo()+"_"+obtenerSufijo(patterns[i])+".html");
						}
					}else {
						kitsOut.add(kit);
					}
				}
			}
			
			for(Kit kit : kitsOut){
				logger.info("OUT kit("+kit.getCdCodigo()+")"+"   ("+kit.getNbNombreKit()+")");
			}
			
			if(kitsOut.isEmpty()) {
				logger.info("La lista se encuentra vacia.");
			}
						
		} catch (Exception e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e.getMessage());
		}
	}
	
	public String buscarPosicion(String texto,int line, int position) throws IOException{
		Reader inputString = new StringReader(texto);
	    BufferedReader br = new BufferedReader(inputString);
	    String strLine;
	    int i = 1;
	    while ((strLine = br.readLine()) != null)   {
	    	if(i==(line)){
	    		return strLine.substring(position,position+1);
	    	}
	    	i++;
	    }
	    inputString.close();
	    return "";
	}
	
	public List<Flag> obtenerFlagsCorreo(){
		List<Flag> flags = new ArrayList<Flag>();
		Flag flag = new Flag();
		flag.setNbCondicionNombre("CFG0|P15");
		flag.setNbCondicionValor("1");
		flags.add(flag);
		flag = new Flag();
		flag.setNbCondicionNombre("CFG0|P18");
		flag.setNbCondicionValor("I");
		flags.add(flag);
		flag = new Flag();
		flag.setNbCondicionNombre("CFG0|P33");
		flag.setNbCondicionValor("S");
		flags.add(flag);
		return flags;
	}
	
	public Map<String,String> obtenerParametros(String pattern){
		Map<String,String> map = new HashMap<String, String>();
		map.put("CFG0|P05", "VITOR FERREIRA GOMES");
		map.put("CFG0|P23", "2017-05-26");
		map.put("CFG0|P10", "SOLES");
		map.put("CFG0|P01", "00110191410200324870");
		map.put("CFG0|P16", "01119100020032487041");
		map.put("CFG0|P15", (pattern.substring(0,1).equals("S")?"1":"0"));
		map.put("CFG0|P18", (pattern.substring(1,2).equals("S")?"I":"III"));
		map.put("CFG0|P33", (pattern.substring(2,3).equals("S")?"S":"N"));
		return map;
	}
	
	public String procesarPlantillaStrTpl(
    		String plantilla,
    		Map<String,String> parametrosCliente,
    		List<Flag> listFlags,
    		boolean escape){
		ST st = new ST(plantilla);
		if(listFlags!=null){
			for(Flag flag:listFlags){
				if(parametrosCliente.containsKey(flag.getNbCondicionNombre()) &&
						parametrosCliente.get(flag.getNbCondicionNombre()).equals(flag.getNbCondicionValor())){
					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),true);
				}else{
					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),false);
				}
			}
		}
		for (Map.Entry<String, String> entry : parametrosCliente.entrySet()){
		    String value = entry.getValue();
			if(escape){
				value = StringEscapeUtils.escapeHtml4(value);
			}
			st.add(StringUtils.replace(entry.getKey(),"|",""),value);
		}
		return st.render();
	}
	
	private void writeFile(String text, String filename) throws FileNotFoundException{
		try {
			
			PrintWriter out = new PrintWriter(filename);		
		    out.println(text);
		    
		}finally{
			
		}
	}
	
	private int cuantasVeces(String first,String second){
		int ind = 0;
		int cnt = 0;
		while (true) {
		    int pos = first.indexOf(second, ind);
		    if (pos < 0) break;
		    cnt++;
		    ind = pos + 1; // Advance by second.length() to avoid self repetitions
		}
		return cnt;
	}
	
	public String obtenerSufijo(String pattern) {
		if ("SSS".equals(pattern))return "LPD_FATCA1_DAE";
		else if  ("SSN".equals(pattern))return "____FATCA1_DAE";
		else if  ("SNS".equals(pattern))return "LPD FATCA1____";
		else if  ("SNN".equals(pattern))return "____FATCA1____";
		else if  ("NSS".equals(pattern))return "LPD________DAE";
		else if  ("NSN".equals(pattern))return "___________DAE";
		else if  ("NNS".equals(pattern))return "LPD___________";
		else if  ("NNN".equals(pattern))return "______________";
		else  return "";
		
		
//		switch (pattern) {
//		case "SSS": return "LPD_FATCA1_DAE";
//		case "SSN": return "____FATCA1_DAE";
//		case "SNS": return "LPD FATCA1____";
//		case "SNN": return "____FATCA1____";
//		case "NSS": return "LPD________DAE";
//		case "NSN": return "___________DAE";
//		case "NNS": return "LPD___________";
//		case "NNN": return "______________";
//		default:    return "";
//		}
	}
	
}
