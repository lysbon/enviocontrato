package com.bbva.cuentas.scripts;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class PlantillaServiceTest {

	@Autowired
	private PlantillaService plantillaService;
	
	@SuppressWarnings("unchecked")
//	@Test
	public void actualizarPlantillas() throws ServiceException{
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi8\\fondos_mutuos\\pdf");
		List<File> formatosPDF = listFilesForFolder(folder);
		
		Result result = plantillaService.obtenerPlantillas();
		List<Plantilla> plantillas = (List<Plantilla>) result.getEntities();
		
		for(Plantilla plantilla : plantillas){
			if(StringUtils.contains(plantilla.getNbDescripcion(),"Prospecto")){
				System.out.println("Actualizando "+plantilla.getNbDescripcion());
				File file = findFileByName(formatosPDF, plantilla.getNbNombreFormato());
				if(file!=null){
					Result rpta = plantillaService.actualizarPlantillaPDFCadena(plantilla.getCdPlantilla(),file.getAbsolutePath());
					System.out.println(rpta.getEntity());
				}else{
					System.out.println("No se encontro formato para "+plantilla.getNbNombreFormato());
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void listarPlantillas() throws ServiceException{
		Result result = plantillaService.obtenerPlantillas();
		List<Plantilla> plantillas = (List<Plantilla>) result.getEntities();
		for(Plantilla plantilla : plantillas){
			if( StringUtils.contains(plantilla.getNbDescripcion(),"Prospecto") ||
				StringUtils.contains(plantilla.getNbDescripcion(),"FM011")){
				System.out.println("util.plantilla("+plantilla.getCdPlantilla()+"l,\""+plantilla.getNbNombreFormato()+"\"),");
			}
		}
	}
	
	public List<File> listFilesForFolder(final File folder) {
	    List<File> filenames = new ArrayList<File>();
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            filenames.addAll(listFilesForFolder(fileEntry));
	        } else {
	        	filenames.add(fileEntry);
	        }
	    }
		return filenames;
	}
	
	public File findFileByName(List<File> files, String contains){
		for(File file : files){
			if(StringUtils.contains(file.getName(), contains)){
				return file;
			}
		}
		return null;
	}
	
}
