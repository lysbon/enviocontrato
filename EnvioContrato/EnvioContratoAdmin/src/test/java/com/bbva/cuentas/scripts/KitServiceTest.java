//package com.bbva.cuentas.scripts;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.apache.commons.lang3.StringUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.bbva.cuentas.aat.service.KitService;
//import com.bbva.cuentas.service.exception.ServiceException;
//import com.bbva.cuentas.bean.Kit;
//import com.bbva.cuentas.util.Result;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("/app-config-nodb.xml")
//public class KitServiceTest {
//
//	@Autowired
//	private KitService kitService;
//	
//	@SuppressWarnings("unchecked")
////	@Test
//	public void actualizarPlantillas() throws ServiceException, IOException{
//		final int envBXI = 2;
//		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi8\\fondos_mutuos\\html");
//		List<File> formatosHtml = listFilesForFolder(folder);
//		
//		Result result = kitService.obtenerKits(envBXI);
//		List<Kit> kits = (List<Kit>) result.getEntities();
//		
//		for(Kit kit : kits){
//			if(StringUtils.contains(kit.getNbNombreKit(),"FM011")){
//				System.out.println("Actualizando "+kit.getNbNombreKit());
//				File file = findFileByName(formatosHtml, kit.getCdCodigo());
//				String html = readFile(file.getAbsolutePath(),Charset.defaultCharset());
//				if(StringUtils.isNotEmpty(html)){
//					kit.setLbCorreoPlantilla(html);
//					Result rpta = kitService.actualizarKit(kit);
//					System.out.println(rpta.getEntity());
//				}else{
//					System.out.println("No se encontro formato para "+kit.getCdCodigo());
//				}
//			}
//		}
//	}
//	
//	public List<File> listFilesForFolder(final File folder) {
//	    List<File> filenames = new ArrayList<File>();
//		for (final File fileEntry : folder.listFiles()) {
//	        if (fileEntry.isDirectory()) {
//	            filenames.addAll(listFilesForFolder(fileEntry));
//	        } else {
//	        	filenames.add(fileEntry);
//	        }
//	    }
//		return filenames;
//	}
//	
//	public File findFileByName(List<File> files, String contains){
//		/*
//		for(File file : files){
//			if(StringUtils.contains(file.getName(), contains)){
//				return file;
//			}
//		}
//		return null;*/
//		return files.get(0);
//	}
//	
//	static String readFile(String path, Charset encoding) throws IOException {
//		byte[] encoded = Files.readAllBytes(Paths.get(path));
//		return new String(encoded, encoding);
//	}
//	
//}
