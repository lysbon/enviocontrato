package com.bbva.cuentas.scripts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.SqlService;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Result;

public class ScriptService {

	private static final Logger logger = LoggerFactory.getLogger(ScriptService.class);
	
	@Autowired
	SqlService sqlService;
	
	@Autowired
	SeccionService seccionService;
	
	@Autowired
	ItemService itemService;
	
	@Autowired
	PlantillaService plantillaService;
	
	@Autowired
	KitService kitService;
	
	@Autowired
	FlagPlantillaService flagPlantillaService;
	
	@Autowired
	FlagService flagService;
	
	protected void revertirBandera(String banderaNombre) throws ServiceException {
		Result result = flagService.obtenerFlags();
		List<Flag> flags = (List<Flag>) result.getEntities();
		for(Flag flag : flags) {
			if(flag.getNbCondicionNombre().equals(banderaNombre)) {
				flagService.eliminarFlag(flag.getCdFlag());
			}
		}
	}
	
	protected void revertirPlantilla(Long cdPlantilla) throws ServiceException {
		Result result = seccionService.obtenerSeccionesPorPlantilla(cdPlantilla);
		for(ItemSection section : (List<ItemSection>)result.getEntities()) {
			revertirSeccion(section);
		}
		eliminarKitPlantillaByPlantilla(cdPlantilla);
		eliminarFlagPlantillaByPlantilla(cdPlantilla);
		plantillaService.eliminarPlantilla(cdPlantilla);
	}
	
	protected void revertirSeccion(ItemSection section) throws ServiceException {
		Result result = itemService.obtenerItemsPorSeccion(section.getCdSeccion());
		for(ItemStamp item : (List<ItemStamp>)result.getEntities()) {
			itemService.eliminarItem(item.getCdItem());
		}
		//seccionService.eliminarSeccion(section.getCdSeccion());
		eliminarSeccion(section.getCdSeccion());
	}
	
	protected void eliminarKitPlantillaByPlantilla(Long cdPlantilla) throws ServiceException {
		sqlService.execute("DELETE FROM APDIGB.TBG011_KIT_PLANTILLA WHERE CD_PLANTILLA = "+cdPlantilla);
	}
	protected void eliminarFlagPlantillaByPlantilla(Long cdPlantilla) throws ServiceException {
		sqlService.execute("DELETE FROM APDIGB.TBG006_FLAG_PLANTILLA WHERE CD_PLANTILLA = "+cdPlantilla);
	}
	protected void eliminarSeccion(Long cdSeccion) throws ServiceException {
		sqlService.execute("DELETE FROM APDIGB.TBG003_SECCION WHERE CD_SECCION = "+cdSeccion);
	}
	
	protected List<Kit> consultarKitsPorNombre(Integer canal,String nombre) throws ServiceException{
		List<Kit> kitsResultado = new ArrayList<Kit>();
		Result result = kitService.obtenerKits(canal);
		List<Kit> kits = (List<Kit>) result.getEntities();
		for(Kit kit : kits) {
			if(StringUtils.contains(kit.getNbNombreKit(),nombre)) {
				kitsResultado.add(kit);
			}
		}
		return kitsResultado;
	}
	
	protected void agregarPlantillaKits(Plantilla plantilla,List<Kit> kits) throws ServiceException{
		for(Kit kit : kits) {
			KitPlantilla kitPlantilla = new KitPlantilla();
			kitPlantilla.setCdKit(kit.getCdKit());
			kitPlantilla.setCdPlantilla(plantilla.getCdPlantilla());
			kitPlantilla.setFechaRegistro(new Date());
			kitPlantilla.setUsuarioRegistro("P023915");
			kitService.guardarKitPlantilla(kitPlantilla);
		}
	}
	
	protected void actualizarCorreoKits(Integer canal,List<Kit> kits,String filename) throws ServiceException{
		String plantillaHTML = leerPlantilla(filename);
		for(Kit kit : kits) {
			kit.getGrupoKit().setLbCorreoPlantilla(plantillaHTML);
			kit.getGrupoKit().setUsuarioRegistro("P023915");
			kit.getGrupoKit().setFechaRegistro(new Date());
			kitService.actualizarKit(kit);
		}
	}
	
	public String leerPlantilla(String filePlantilla){
		BufferedReader reader = null;
		String result = null;
		try {
			InputStream in = new FileInputStream(new File(filePlantilla));
			reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder out = new StringBuilder();
		    String line;
		    while ((line = reader.readLine()) != null) {
		        out.append(line);
		    }
		    result = out.toString();
		} catch (IOException e) {
			logger.error("Error al leer plantilla de correo. "+e);
		} finally {
			try {
				if(reader!=null) reader.close();
			} catch (IOException ex) {
				logger.error("Error al cerrar plantilla de correo. "+ex);
			}
		}
		return result;
	}
	
	protected void crearPlantillaBandera(Plantilla plantilla, String nombre, String valor) throws ServiceException {
		Flag flag = new Flag();
		flag.setChTipo("T");//template
		flag.setFechaRegistro(new Date());
		flag.setNbCondicionNombre(nombre);
		flag.setNbCondicionValor(valor);
		flag.setNuOrden(1);
		flag.setUsuarioRegistro("P023915");
		Result result = flagService.guardarFlag(flag);
		flag.setCdFlag((Long)result.getEntity());
		
		FlagPlantilla flagPlantilla = new FlagPlantilla();
		flagPlantilla.setFlag(flag);
		flagPlantilla.setCdFlag(flag.getCdFlag());
		flagPlantilla.setCdPlantilla(plantilla.getCdPlantilla());
		flagPlantilla.setFechaRegistro(new Date());
		flagPlantilla.setStActivo("A");
		flagPlantilla.setStValor("A");
		flagPlantilla.setUsuarioRegistro("P023915");
		
		flagPlantillaService.guardarFlagPlantilla(flagPlantilla);
		
		Flag flagEmail = new Flag();
		flagEmail.setChTipo("E");//mail
		flagEmail.setFechaRegistro(new Date());
		flagEmail.setNbCondicionNombre(nombre);
		flagEmail.setNbCondicionValor(valor);
		flagEmail.setNuOrden(1);
		flagEmail.setUsuarioRegistro("P023915");
		flagService.guardarFlag(flagEmail);
	}
	
	protected Long guardarPlantillaTactico(Plantilla plantilla) throws ServiceException, JsonParseException, JsonMappingException, IOException {
		
		Result result = sqlService.query("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA");
		List<Map<String, Object>> trama = (List<Map<String, Object>>)result.getEntities();
		Integer id = (Integer)trama.get(0).get("NEWIDPL");
		Long cdPlantillaInt = Long.parseLong(id.toString());
		
		sqlService.execute("INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) "
				+ "VALUES ("+cdPlantillaInt.toString()+",NULL,'"+plantilla.getStActivo()+"','"+plantilla.getStCondicion()+"','"+plantilla.getStFileunico()+"','"+plantilla.getStEnvCorreo()+"','"+plantilla.getStTipoConstr()+"','"+plantilla.getStSigned()+"','"+plantilla.getNbNombreFormato()+"','"+plantilla.getNbTipoFileunico()+"','"+plantilla.getNbNombreCorreo()+"','"+plantilla.getChTipoPlantilla()+"','"+plantilla.getNbDescripcion()+"','"+plantilla.getUsuarioRegistro()+"',NULL,SYSDATE,NULL)");
		
		return cdPlantillaInt;
		
	}
	
	protected Long guardarSeccionTactico(ItemSection section) throws ServiceException, JsonParseException, JsonMappingException, IOException {
		//TODO otorgar grant a sequence para administrador
		
		Result result = sqlService.query("SELECT MAX(CD_SECCION)+1 NEWID FROM APDIGB.TBG003_SECCION");
		List<Map<String, Object>> trama = (List<Map<String, Object>>)result.getEntities();
		Integer id = (Integer)trama.get(0).get("NEWID");
		Long cdSeccionInt = Long.parseLong(id.toString());
		
		sqlService.execute("INSERT INTO APDIGB.TBG003_SECCION (CD_SECCION,CD_PLANTILLA,NU_PAGINA) "
				+ "VALUES("+cdSeccionInt+","+section.getCdPlantilla()+","+section.getNuPagina()+")");
		return cdSeccionInt;
	}
	
	protected Long guardarItemTactico(ItemStamp item) throws ServiceException, JsonParseException, JsonMappingException, IOException {
		//TODO otorgar grant a sequence para administrador
		
		Result result = sqlService.query("SELECT MAX(CD_ITEM)+1 NEWID FROM APDIGB.TBG004_ITEM");
		List<Map<String, Object>> trama = (List<Map<String, Object>>)result.getEntities();
		Integer id = (Integer)trama.get(0).get("NEWID");
		Long cdItemInt = Long.parseLong(id.toString());
		
		sqlService.execute("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) "
				+ "VALUES("+item.getNuCoorX()+","+item.getNuCoorY()+",null,null,'"+item.getStActivo()+"','"+item.getNbFuente()+"','"+item.getNbNombreVar()+"','"+item.getChTipo()+"',"+item.getCdSeccion()+",null,null,null,null,"+cdItemInt+","+item.getNuAlinear()+")");
		
		return cdItemInt;
		
	}
	
	protected ItemStamp nuevoItem(Integer x, Integer y, Integer idxCliente, 
			Integer ancho, String activo, String fuente,
			String nombreVariable, String tipo,
			Long cdSeccion,Integer scX,Integer scY,String mascara,
			String valorVariable,Long cdItem,Integer alinear) {
		ItemStamp item = new ItemStamp(nombreVariable, valorVariable, x, y);
		item.setCdSeccion(cdSeccion);
		item.setChTipo(tipo);
		item.setFechaRegistro(new Date());
		item.setNbFuente(fuente);
		item.setNbMask(mascara);
		item.setNuAlinear(alinear);
		item.setNuAncho(ancho);
		item.setNuIdxCliente(idxCliente);
		item.setNuScX(scX);
		item.setNuScY(scY);
		item.setStActivo(activo);
		item.setUsuarioRegistro("P023915");
		return item;
	}
	
}
