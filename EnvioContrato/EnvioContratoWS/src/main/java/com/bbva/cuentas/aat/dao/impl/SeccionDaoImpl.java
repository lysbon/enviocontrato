package com.bbva.cuentas.aat.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.SeccionDao;
import com.bbva.cuentas.aat.dao.mapper.SeccionMapper;
import com.bbva.cuentas.bean.ItemSection;

@Component
public class SeccionDaoImpl extends BaseDaoImpl<ItemSection, Long> implements SeccionDao{
	
	private static final Logger logger = LoggerFactory.getLogger(SeccionDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG003_SECCION";
	static final String TABLE_PK = "CD_SECCION";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"sec_seq";

	@Autowired
	public SeccionDaoImpl(DataSource datasource, SeccionMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}	
	
	@Override
	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla)	throws DaoException {
		List<ItemSection> sections=null;		
		try{
			SqlCondicion[] condiciones = new SqlCondicion[1];
			condiciones[0] = new SqlCondicion(SeccionMapper.CD_PLANTILLA,cdPlantilla);
			sections = this.getEntities(condiciones);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return sections;
	}
	
	@Override
	public Long insertAndReturnId(ItemSection seccion) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(SeccionMapper.CD_PLANTILLA, seccion.getCdPlantilla());
		args.put(SeccionMapper.NU_PAGINA, seccion.getNuPagina());
		return super.insertAndReturnId(args);
	}
	
	@Override
	public int update(ItemSection seccion) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(SeccionMapper.CD_PLANTILLA, seccion.getCdPlantilla());
		args.put(SeccionMapper.NU_PAGINA, seccion.getNuPagina());

		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(SeccionMapper.CD_SECCION, seccion.getCdSeccion());
		return super.update(args, condiciones);
	}
	
}
