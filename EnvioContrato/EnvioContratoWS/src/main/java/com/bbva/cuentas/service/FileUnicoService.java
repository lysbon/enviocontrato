package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.ArchivoFileUnico;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;

public interface FileUnicoService {
	
	public void procesarArchivos(Kit kit, RequestDTO firmaContrato, ClienteDTO cliente) throws ServiceException;
	
	public void procesarArchivos(Kit kit, RequestDTO firmaContrato, ClienteDTO cliente, boolean metadata, String rutaOpcional) throws ServiceException;
	
	public List<ArchivoFileUnico> obtenerListaArchivosPDF(List<Plantilla> plantillas,Map<Long, ConfigurationPath> cfgs) throws ServiceException;
	 
	public void procesarArchivosWithTemp(Kit kit, RequestDTO firmaContrato, ClienteDTO cliente) throws ServiceException;
	
}
