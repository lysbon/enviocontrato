package com.bbva.cuentas.service;

import java.sql.Timestamp;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public interface ErrorService {
    
    public Trama guardarTrama(
    		FirmaContrato firmaContrato,
    		ServiceException error,
			Integer cdEnv,
			String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso) throws ServiceException;
        
	public Trama guardarTrama(
			FirmaContrato firmaContrato, 
			ClienteDTO cliente, 
			ServiceException error,Integer cdEnv,String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso) throws ServiceException;
	
	public void guardarDetTrama(
			ClienteDTO cliente,
			Trama trama,
			ServiceException error,
			String estadoProceso) throws ServiceException;

    public void updateTramaGeneral(
    		FirmaContrato firmaContrato,
    		ClienteDTO cliente,
    		ServiceException error,
    		Integer cdEnv,
    		String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso
    )throws ServiceException;
    
    public void updateTramaGeneral(
    		FirmaContrato firmaContrato,
    		ServiceException error,
    		Integer cdEnv,String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso
    		)throws ServiceException;
    
    public void procesaError(
    		Trama trama,
    		String indicadorProceso
    		)throws ServiceException;
    
	public Trama generaTrama(
    		FirmaContrato firmaContrato,
    		String cliente,
    		ServiceException error,
    		Integer cdEnv,
    		String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso
    		)throws ServiceException;
    
 	public String getMensaje(Throwable error);
    
 	public RequestDTO getRequestJson(String trama) throws ServiceException;
 	
}