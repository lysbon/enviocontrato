package com.bbva.cuentas.service.loader.impl;

import java.sql.*;

import javax.sql.DataSource;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.service.loader.PDFTemplateLoader;

import java.io.*;

@Component
public class PDFTemplateLoaderImpl implements PDFTemplateLoader {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${datasource.schema}")
	private String schema;
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public PDFTemplateLoaderImpl(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }
    
	public void writeBLOBPut(final int cdPlantilla, String inputTextFileName) 
            throws IOException, SQLException {
		
		String sqlText = null;
		InputStream blobIs =null;
		try {        	
        	sqlText = "UPDATE "+schema+".TBG002_PLANTILLA SET LB_PDF_PLANTILLA = ?, FH_MODIFICACION = SYSDATE WHERE CD_PLANTILLA = ? ";
        	final File blobIn = new File(inputTextFileName);
        	blobIs = new FileInputStream(blobIn);
        	final InputStream tmpBlobIs=blobIs; 
        	final LobHandler lobHandler = new DefaultLobHandler();
			jdbcTemplate.execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	        lobCreator.setBlobAsBinaryStream(ps, 1, tmpBlobIs, (int)blobIn.length());
        	        ps.setInt(2, cdPlantilla);
        	      }
        	  }
        	);
        	blobIs.close();
        	
        } catch (IOException e) {
        	logger.error(e);
            throw e;
        }finally {
        	try {if (blobIs!=null) blobIs.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        }
    }
	
}
