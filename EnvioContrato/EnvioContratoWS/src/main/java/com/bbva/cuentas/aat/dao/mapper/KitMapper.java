package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Kit;

@Component
public class KitMapper implements RowMapper<Kit>{

	public static final String CD_KIT="CD_KIT";
	public static final String CD_CODIGO="CD_CODIGO";
	public static final String NB_NOMBRE_KIT="NB_NOMBRE_KIT";
	public static final String ST_FILEUNICO="ST_FILEUNICO";
	public static final String FL_VAL_HUELLA="FL_VAL_HUELLA";
	public static final String ST_SIGNBOX="ST_SIGNBOX";
	public static final String ST_HUELLA="ST_HUELLA";
	
	@Override
	public Kit mapRow(ResultSet rs, int rowNum) throws SQLException {
		Kit kit = new Kit();
		kit.setCdKit(rs.getLong(CD_KIT));
		kit.setCdCodigo(rs.getString(CD_CODIGO));
		kit.setNbNombreKit(rs.getString(NB_NOMBRE_KIT));
		kit.setStFileunico(rs.getString(ST_FILEUNICO));
		kit.setFlValHuella(rs.getString(FL_VAL_HUELLA));
		kit.setStSignBox(rs.getString(ST_SIGNBOX));
		kit.setStHuella(rs.getString(ST_HUELLA));
		return kit;
	}

}
