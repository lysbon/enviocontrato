package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ResourceItem;

@Component
public class ResourceItemMapper implements RowMapper<ResourceItem>{

	//TODO crear clases data
	public static final String CD_ITEM="CD_ITEM";
	public static final String LB_IMG="LB_IMG";
	public static final String CD_ENV="CD_ENV";
	
	@Override
	public ResourceItem mapRow(ResultSet rs, int rowNum) throws SQLException {
		ResourceItem res = new ResourceItem();
		res.setCdItem(rs.getLong(CD_ITEM));
		res.setFile(rs.getBytes(LB_IMG));
		return res;
	}

}
