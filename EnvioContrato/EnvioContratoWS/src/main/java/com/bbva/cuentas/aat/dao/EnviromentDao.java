package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.Enviroment;

public interface EnviromentDao extends BaseDao<Enviroment, Integer>{

	public List<Enviroment> obtenerEnvPorTipo(String tipo) throws DaoException;
	
	public Enviroment obtenerEnvPorDefecto() throws DaoException;
	
}
