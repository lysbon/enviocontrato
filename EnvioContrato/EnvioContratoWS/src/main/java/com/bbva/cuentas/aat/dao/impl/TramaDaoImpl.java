package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.TramaDao;
import com.bbva.cuentas.aat.dao.mapper.TramaMapper;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.util.Constants;

@Component
public class TramaDaoImpl extends BaseDaoImpl<Trama, Long> implements TramaDao{
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TramaDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG008_TRAMA";
	static final String TABLE_PK = "ID_TRAMA";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"TRAMA_SEQ";
	
	
	
	@Autowired
	TramaMapper tramaMapper;
	
	@Autowired
	public TramaDaoImpl(DataSource datasource, TramaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public int insert(Trama trama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(TramaMapper.CD_ENV,         trama.getCdEnv());
		args.put(TramaMapper.CH_TIPO,        trama.getChTipo());
		args.put(TramaMapper.CD_STEP,        trama.getCdStep());
		args.put(TramaMapper.CH_MOTI,        trama.getChMoti());
		args.put(TramaMapper.CH_PROCEDENCIA, trama.getChProcedencia());
		args.put(TramaMapper.CH_TRAMA,       trama.getChTrama().trim());
		args.put(TramaMapper.CD_CLIENTE,     trama.getCdCliente());
		args.put(TramaMapper.CD_DOC,         trama.getCdDoc());
		args.put(TramaMapper.FH_CREACION,    new Date());
		args.put(TramaMapper.DET_STEP,    trama.getDetStep());
		
		/********04122017**********/
		args.put(TramaMapper.ESTADO,    trama.getEstado());
		args.put(TramaMapper.NUM_REPROCESO,    trama.getNumReproceso());
		args.put(TramaMapper.TIME_REPROCESO,    trama.getTimeReproceso());
		//args.put(TramaMapper.ID_TRAMA,    trama.getIdTrama());
		//args.put(TramaMapper.CH_TRAMA_2,    trama.getChTrama2().trim());
		/********04122017**********/
		
		
		return super.insert(args);
	}
	
	
	@Override
	public Long insertAndReturnId(Trama trama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(TramaMapper.CD_ENV,         trama.getCdEnv());
		args.put(TramaMapper.CH_TIPO,        trama.getChTipo());
		args.put(TramaMapper.CD_STEP,        trama.getCdStep());
		args.put(TramaMapper.CH_MOTI,        trama.getChMoti());
		args.put(TramaMapper.CH_PROCEDENCIA, trama.getChProcedencia());
		args.put(TramaMapper.CH_TRAMA,       trama.getChTrama());
		args.put(TramaMapper.CD_CLIENTE,     trama.getCdCliente());
		args.put(TramaMapper.CD_DOC,         trama.getCdDoc());
		args.put(TramaMapper.FH_CREACION,    new Date());
		args.put(TramaMapper.DET_STEP,    trama.getDetStep());
		
		/********04122017**********/
		args.put(TramaMapper.ESTADO,    trama.getEstado());
		args.put(TramaMapper.NUM_REPROCESO,    trama.getNumReproceso());
		args.put(TramaMapper.TIME_REPROCESO,    trama.getTimeReproceso());
		//args.put(TramaMapper.CH_TRAMA_2,    trama.getChTrama2().toString() );
		//args.put(TramaMapper.ID_TRAMA,    trama.getIdTrama());
		/********04122017**********/
		
		
		return super.insertAndReturnId(args);
	}
	
	
	@Override
	public int update(Trama trama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(TramaMapper.CD_ENV,         trama.getCdEnv());
		args.put(TramaMapper.CH_TIPO,        trama.getChTipo());
		args.put(TramaMapper.CD_STEP,        trama.getCdStep());
		args.put(TramaMapper.CH_MOTI,        trama.getChMoti());
		args.put(TramaMapper.CH_PROCEDENCIA, trama.getChProcedencia());
		args.put(TramaMapper.CH_TRAMA,       trama.getChTrama());
		args.put(TramaMapper.CD_CLIENTE,     trama.getCdCliente());
		args.put(TramaMapper.CD_DOC,         trama.getCdDoc());
		args.put(TramaMapper.DET_STEP,    trama.getDetStep());
		
		/********04122017**********/
		args.put(TramaMapper.ESTADO,    trama.getEstado());
		args.put(TramaMapper.NUM_REPROCESO,    trama.getNumReproceso());
		args.put(TramaMapper.TIME_REPROCESO,    trama.getTimeReproceso());
		args.put(TramaMapper.ID_TRAMA,    trama.getIdTrama());
		//args.put(TramaMapper.CH_TRAMA_2,    trama.getChTrama2());
		
		/********04122017**********/
		
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(TramaMapper.ID_TRAMA , trama.getIdTrama());
		return super.update(args, condiciones);
	}
	
	@Override
	public List<Trama> obtenerTrama(String cdEnv,String procedencia, String tipo)
			throws DaoException {
		if(StringUtils.isEmpty(cdEnv)){
			return super.getAllEntities();
		}else{
			if(StringUtils.isEmpty(tipo)){		
				SqlCondicion[] condiciones = new SqlCondicion[2];
				condiciones[0] = new SqlCondicion(TramaMapper.CD_ENV, cdEnv);
				condiciones[1] = new SqlCondicion(SqlOperador.AND,TramaMapper.CH_PROCEDENCIA, procedencia);
				return super.getEntities(condiciones);
			}else{
				SqlCondicion[] condiciones = new SqlCondicion[3];
				condiciones[0] = new SqlCondicion(TramaMapper.CD_ENV, cdEnv);
				condiciones[1] = new SqlCondicion(SqlOperador.AND,TramaMapper.CH_PROCEDENCIA, procedencia);
				condiciones[2] = new SqlCondicion(SqlOperador.AND,TramaMapper.CH_TIPO, tipo);
				return super.getEntities(condiciones);
			}
		}
	}

	@Override
	public Integer limpiarTramas() throws DaoException {
		SqlCondicion[] condiciones = null;
		return super.delete(condiciones);
	}
	
	@Override
	public List<Trama> obtenerTramasParaReproceso(String Estado)
			throws DaoException {
		List<Trama> items= new ArrayList<Trama>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  ESTADO = ? "+
					"ORDER  BY TIME_REPROCESO";
			
			String[] args = new String[1];
			args[0] = Estado;
			items = getJdbcTemplate().query(sql,args,tramaMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
}