package com.bbva.cuentas.ws;

import com.bbva.cuentas.firmarcontratos.ReenvioContratosRequest;
import com.bbva.cuentas.firmarcontratos.ReenvioContratosResponse;

public interface MarshallingReevioContratosService {

	public final static String NAMESPACE = "http://bbva.com/cuentas/firmarContratos";
    public final static String REENVIO_CONTRATOS_REQUEST = "reenvio-contratos-request";

    /**
     * Gets person list.
     */
    public ReenvioContratosResponse reenviarContratos(ReenvioContratosRequest request);
    
}
