//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para firmaContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="firmaContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listaItems" type="{http://bbva.com/cuentas/firmarContratos}itemContrato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaClientes" type="{http://bbva.com/cuentas/firmarContratos}cliente" maxOccurs="unbounded"/>
 *         &lt;element name="numeroContrato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinaGestora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="procedencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plantillaCorreo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="grupoCorreo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *       &lt;attribute name="idContrato" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="idGrupo" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="async" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="gestDoc" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "firmaContrato", propOrder = {
    "listaItems",
    "listaClientes",
    "numeroContrato",
    "oficinaGestora",
    "procedencia",
    "plantillaCorreo",
    "grupoCorreo"
})
public class FirmaContrato {

    protected List<ItemContrato> listaItems;
    @XmlElement(required = true)
    protected List<Cliente> listaClientes;
    @XmlElement(required = true)
    protected String numeroContrato;
    @XmlElement(required = true)
    protected String oficinaGestora;
    @XmlElement(required = true)
    protected String procedencia;
    @XmlElement(required = true, nillable = true)
    protected String plantillaCorreo;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer grupoCorreo;
    @XmlAttribute(name = "idContrato")
    @XmlSchemaType(name = "anySimpleType")
    protected String idContrato;
    @XmlAttribute(name = "idGrupo")
    @XmlSchemaType(name = "anySimpleType")
    protected String idGrupo;
    @XmlAttribute(name = "async")
    @XmlSchemaType(name = "anySimpleType")
    protected String async;
    @XmlAttribute(name = "gestDoc")
    @XmlSchemaType(name = "anySimpleType")
    protected String gestDoc;

    /**
     * Gets the value of the listaItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemContrato }
     * 
     * 
     */
    public List<ItemContrato> getListaItems() {
        if (listaItems == null) {
            listaItems = new ArrayList<ItemContrato>();
        }
        return this.listaItems;
    }

    /**
     * Gets the value of the listaClientes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaClientes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaClientes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cliente }
     * 
     * 
     */
    public List<Cliente> getListaClientes() {
        if (listaClientes == null) {
            listaClientes = new ArrayList<Cliente>();
        }
        return this.listaClientes;
    }

    /**
     * Obtiene el valor de la propiedad numeroContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Define el valor de la propiedad numeroContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContrato(String value) {
        this.numeroContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaGestora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaGestora() {
        return oficinaGestora;
    }

    /**
     * Define el valor de la propiedad oficinaGestora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaGestora(String value) {
        this.oficinaGestora = value;
    }

    /**
     * Obtiene el valor de la propiedad procedencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedencia() {
        return procedencia;
    }

    /**
     * Define el valor de la propiedad procedencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedencia(String value) {
        this.procedencia = value;
    }

    /**
     * Obtiene el valor de la propiedad plantillaCorreo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlantillaCorreo() {
        return plantillaCorreo;
    }

    /**
     * Define el valor de la propiedad plantillaCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlantillaCorreo(String value) {
        this.plantillaCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad grupoCorreo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGrupoCorreo() {
        return grupoCorreo;
    }

    /**
     * Define el valor de la propiedad grupoCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGrupoCorreo(Integer value) {
        this.grupoCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad idContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdContrato() {
        return idContrato;
    }

    /**
     * Define el valor de la propiedad idContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdContrato(String value) {
        this.idContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad idGrupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGrupo() {
        return idGrupo;
    }

    /**
     * Define el valor de la propiedad idGrupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGrupo(String value) {
        this.idGrupo = value;
    }

    /**
     * Obtiene el valor de la propiedad async.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsync() {
        return async;
    }

    /**
     * Define el valor de la propiedad async.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsync(String value) {
        this.async = value;
    }

    /**
     * Obtiene el valor de la propiedad gestDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGestDoc() {
        return gestDoc;
    }

    /**
     * Define el valor de la propiedad gestDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGestDoc(String value) {
        this.gestDoc = value;
    }

}
