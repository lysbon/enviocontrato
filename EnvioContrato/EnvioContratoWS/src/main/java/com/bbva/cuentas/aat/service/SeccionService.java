package com.bbva.cuentas.aat.service;

import java.util.Date;
import java.util.List;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.service.exception.ServiceException;

public interface SeccionService {
	
	public List<ItemSection> obtenerSecciones() throws ServiceException;
	
	public ItemSection obtenerSeccionPorId(Long id) throws ServiceException;

	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla,Date fecHoraCreacion) throws ServiceException;

	public List<ItemSection> obtenerSeccionesItemPorPlantilla(Long cdPlantilla,Date fecHoraCreacion) throws ServiceException;
	
	public Long guardarSeccion(ItemSection itemSection) throws ServiceException;
	
	public Integer actualizarSeccion(ItemSection itemSection) throws ServiceException;
	
	public Integer eliminarSeccion(Long id) throws ServiceException;
	
}
