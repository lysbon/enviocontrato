package com.bbva.cuentas.service;

import com.bbva.cuentas.service.exception.ServiceException;

public interface ReprocesarService {
	
    void process() throws ServiceException;
    void process(Long from,Long to) throws ServiceException;

}