package com.bbva.cuentas.dto;

import java.util.List;

import com.bbva.cuentas.bean.DetTrama;

public class ClienteReprocesoDTO {

	private String codCliente;
	private boolean procesarFileUnico;
	private boolean procesarMail;
	private List<DetTrama> detalleTrama;
	
	public boolean isProcesarFileUnico() {
		return procesarFileUnico;
	}
	public void setProcesarFileUnico(boolean procesarFileUnico) {
		this.procesarFileUnico = procesarFileUnico;
	}
	public boolean isProcesarMail() {
		return procesarMail;
	}
	public void setProcesarMail(boolean procesarMail) {
		this.procesarMail = procesarMail;
	}
	public String getCodCliente() {
		return codCliente;
	}
	public void setCodCliente(String codCliente) {
		this.codCliente = codCliente;
	}
	public List<DetTrama> getDetalleTrama() {
		return detalleTrama;
	}
	public void setDetalleTrama(List<DetTrama> detalleTrama) {
		this.detalleTrama = detalleTrama;
	}
	
	
}
