package com.bbva.cuentas.service.exception;

import org.apache.commons.lang3.StringUtils;

public class BaseException extends /*RuntimeException*/Exception{
	
	private static final long serialVersionUID = 1L;
	
    private final String errorCode;
        
    public BaseException(String errorCode) {
        this(errorCode,StringUtils.EMPTY);
    }
    
    public BaseException(String errorCode,String message) {
        super(message);
    	this.errorCode = errorCode;
    }
    
    public BaseException(String errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}
    
    public BaseException(String errorCode,String message, Throwable cause) {
        super(message,cause);
    	this.errorCode = errorCode;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

}
