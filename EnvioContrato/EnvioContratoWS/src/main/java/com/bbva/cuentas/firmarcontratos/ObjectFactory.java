//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bbva.cuentas.firmarcontratos package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bbva.cuentas.firmarcontratos
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReprocesarContratosResponse }
     * 
     */
    public ReprocesarContratosResponse createReprocesarContratosResponse() {
        return new ReprocesarContratosResponse();
    }

    /**
     * Create an instance of {@link ConfigureRequest }
     * 
     */
    public ConfigureRequest createConfigureRequest() {
        return new ConfigureRequest();
    }

    /**
     * Create an instance of {@link Configuration }
     * 
     */
    public Configuration createConfiguration() {
        return new Configuration();
    }

    /**
     * Create an instance of {@link ConfigureResponse }
     * 
     */
    public ConfigureResponse createConfigureResponse() {
        return new ConfigureResponse();
    }

    /**
     * Create an instance of {@link ReenvioContratosRequest }
     * 
     */
    public ReenvioContratosRequest createReenvioContratosRequest() {
        return new ReenvioContratosRequest();
    }

    /**
     * Create an instance of {@link ReenvioContrato }
     * 
     */
    public ReenvioContrato createReenvioContrato() {
        return new ReenvioContrato();
    }

    /**
     * Create an instance of {@link ReprocesarContratosRequest }
     * 
     */
    public ReprocesarContratosRequest createReprocesarContratosRequest() {
        return new ReprocesarContratosRequest();
    }

    /**
     * Create an instance of {@link ReprocesarContrato }
     * 
     */
    public ReprocesarContrato createReprocesarContrato() {
        return new ReprocesarContrato();
    }

    /**
     * Create an instance of {@link FirmarContratosRequest }
     * 
     */
    public FirmarContratosRequest createFirmarContratosRequest() {
        return new FirmarContratosRequest();
    }

    /**
     * Create an instance of {@link FirmaContrato }
     * 
     */
    public FirmaContrato createFirmaContrato() {
        return new FirmaContrato();
    }

    /**
     * Create an instance of {@link FirmarContratosResponse }
     * 
     */
    public FirmarContratosResponse createFirmarContratosResponse() {
        return new FirmarContratosResponse();
    }

    /**
     * Create an instance of {@link ReenvioContratosResponse }
     * 
     */
    public ReenvioContratosResponse createReenvioContratosResponse() {
        return new ReenvioContratosResponse();
    }

    /**
     * Create an instance of {@link Cliente }
     * 
     */
    public Cliente createCliente() {
        return new Cliente();
    }

    /**
     * Create an instance of {@link ItemContrato }
     * 
     */
    public ItemContrato createItemContrato() {
        return new ItemContrato();
    }

    /**
     * Create an instance of {@link ConfigurationItem }
     * 
     */
    public ConfigurationItem createConfigurationItem() {
        return new ConfigurationItem();
    }

}
