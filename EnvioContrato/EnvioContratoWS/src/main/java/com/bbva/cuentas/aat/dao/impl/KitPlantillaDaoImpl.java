package com.bbva.cuentas.aat.dao.impl;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.KitPlantillaDao;
import com.bbva.cuentas.aat.dao.mapper.KitPlantillaMapper;
import com.bbva.cuentas.bean.KitPlantilla;

@Component
public class KitPlantillaDaoImpl extends BaseDaoImpl<KitPlantilla, String> implements KitPlantillaDao{
	
	private static final Logger logger = LoggerFactory.getLogger(KitPlantillaDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG011_KIT_PLANTILLA";
	static final String TABLE_NAME_HIST = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG011_KIT_PLANTILLA_HIST";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	public KitPlantillaDaoImpl(DataSource datasource, KitPlantillaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public Integer delete(KitPlantilla kitPlantilla) throws DaoException {
		Integer result = null;
		try {
			String sql = "DELETE FROM " + TABLE_NAME + 
					     " WHERE "+KitDaoImpl.TABLE_PK+" = ? "+
			             "   AND "+PlantillaDaoImpl.TABLE_PK + " = ?";
			Object[] args = new Object[2];
			args[0] = kitPlantilla.getCdKit();
			args[1] = kitPlantilla.getCdPlantilla();
			result = getJdbcTemplate().update(sql, args);			
		} catch (DataAccessException dae) {
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return result;
	}
	
	@Override
	public Integer deletePorKit(Long cdKit) throws DaoException {
		Integer result = null;
		try {
			String sql = "DELETE FROM " + TABLE_NAME + 
					     " WHERE "+KitDaoImpl.TABLE_PK+" = ? ";
			Object[] args = new Object[1];
			args[0] = cdKit;
			result = getJdbcTemplate().update(sql, args);			
		} catch (DataAccessException dae) {
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return result;
	}
	
	@Override
	public int insert(KitPlantilla kitPlantilla) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(KitPlantillaMapper.CD_KIT,kitPlantilla.getCdKit());
		args.put(KitPlantillaMapper.CD_PLANTILLA, kitPlantilla.getCdPlantilla());
		return super.insert(args);
	}
	
}
