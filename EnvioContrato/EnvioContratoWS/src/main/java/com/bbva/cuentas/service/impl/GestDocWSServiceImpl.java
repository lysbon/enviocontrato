package com.bbva.cuentas.service.impl;

import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.xml.bind.JAXBElement;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.GestDocWSService;
import com.bbva.gesdoc.basics.wsdl.AltaDocumentoRequest;
import com.bbva.gesdoc.basics.wsdl.AltaDocumentoResponse;
import com.bbva.gesdoc.basics.wsdl.ObjectFactory;
import com.bbva.gesdoc.basics.wsdl.PropiedadVO;
import com.bbva.gesdoc.basics.wsdl.RespuestaAltaDocumento;

@Service
public class GestDocWSServiceImpl extends WebServiceGatewaySupport implements GestDocWSService{
	
	private static final Logger mylogger = org.apache.logging.log4j.LogManager.getLogger();

	@Value("${fileunico.gestdoc.uri}")
	private String uri;
	@Value("${fileunico.gestdoc.callback}")
	private String callback;
	
	@Override
	public RespuestaAltaDocumento altaDocumento(
			String nombreAdjunto,
			String rutaAdjunto,
			List<PropiedadVO> propiedades
)
			throws ServiceException {

		ObjectFactory factory = new ObjectFactory();
		AltaDocumentoRequest request = factory.createAltaDocumentoRequest();

		request.setNombreContenidoAdjunto(nombreAdjunto);
		DataSource source = new FileDataSource(rutaAdjunto);
		request.setContenidoAdjunto(new DataHandler(source));
		
		StringBuilder reqPropiedades = new StringBuilder();
		for (PropiedadVO propiedad : propiedades) {
			request.getPropiedades().add(propiedad);
			reqPropiedades.append(propiedad.getPropiedad().name());
			reqPropiedades.append(":");
			reqPropiedades.append(propiedad.getValor());
			reqPropiedades.append(",");
		}
		mylogger.info("Valores de entrada: " + reqPropiedades.toString());

		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.bbva.gesdoc.basics.wsdl");
		getWebServiceTemplate().setMarshaller(marshaller);
		getWebServiceTemplate().setUnmarshaller(marshaller);
		getWebServiceTemplate().afterPropertiesSet();

		@SuppressWarnings("rawtypes")
		JAXBElement response = (JAXBElement) getWebServiceTemplate().marshalSendAndReceive(uri, request, null	/* new SoapActionCallback(callback) */);
		RespuestaAltaDocumento result = ((AltaDocumentoResponse) response.getValue()).getAltaDocumentoResponse();

		mylogger.info("Requesting AltaDocumento nombreAdjunto: " + nombreAdjunto);
		mylogger.info("RespuestaAltaDocumento result.getDocumentoId():  "
				+ result.getDocumentoId());
		mylogger.info("RespuestaAltaDocumento result.getGlobalCode():  "
				+ result.getGlobalCode());
		mylogger.info("RespuestaAltaDocumento result.getGlobalDescription():  "
				+ result.getGlobalDescription());

		if (result.getGlobalCode().intValue() != 0) {
			throw new ServiceException(
					"Error en alta de documento a fileunico."
							+ result.getGlobalDescription());
		}
		return result;
	}
		
}
