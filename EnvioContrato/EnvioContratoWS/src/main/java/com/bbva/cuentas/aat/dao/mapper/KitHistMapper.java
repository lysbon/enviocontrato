package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.KitHist;

@Component
public class KitHistMapper implements RowMapper<KitHist>{

	public static final String CD_KIT="CD_KIT";
	public static final String FH_INICIO="FH_INICIO";
	public static final String FH_FIN="FH_FIN";
	public static final String CD_CODIGO="CD_CODIGO";
	public static final String NB_CORREO_ASUNTO="NB_CORREO_ASUNTO";
	public static final String LB_CORREO_PLANTILLA="LB_CORREO_PLANTILLA";
	public static final String ST_ENV_CORREO="ST_ENV_CORREO";
	public static final String ST_FILEUNICO="ST_FILEUNICO";
	public static final String FL_VAL_HUELLA="FL_VAL_HUELLA";
	public static final String NB_CORREO_ENGINE="NB_CORREO_ENGINE";
	public static final String NB_NOMBRE_KIT="NB_NOMBRE_KIT";
	public static final String ST_SIGNBOX="ST_SIGNBOX";
	public static final String ST_HUELLA="ST_HUELLA";
	
	
	@Override
	public KitHist mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		KitHist kitHist = new KitHist();
		kitHist.setCdKit(rs.getLong(CD_KIT));
		kitHist.setFhInicio(rs.getDate(FH_INICIO));
		kitHist.setFhInicio(rs.getDate(FH_FIN));
		kitHist.setCdCodigo(rs.getString(CD_CODIGO));
		kitHist.setNbCorreoAsunto(rs.getString(NB_CORREO_ASUNTO));
		kitHist.setStEnvCorreo(rs.getString(ST_ENV_CORREO));
		kitHist.setStFileunico(rs.getString(ST_FILEUNICO));
		kitHist.setFlValHuella(rs.getString(FL_VAL_HUELLA));
		kitHist.setNbCorreoEngine(rs.getString(NB_CORREO_ENGINE));
		kitHist.setNbNombreKit(rs.getString(NB_NOMBRE_KIT));
		kitHist.setStSignBox(rs.getString(ST_SIGNBOX));
		kitHist.setStHuella(rs.getString(ST_HUELLA));
		
		return kitHist;
	}

}
