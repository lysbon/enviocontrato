package com.bbva.cuentas.aat.service;

import java.util.List;
import java.sql.Timestamp;

import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.service.exception.ServiceException;

public interface KitService {

	public Kit obtenerKitPorId(Integer cdEnv, Long cdKit) throws ServiceException;
	
	public List<Kit> obtenerKits(Integer cdEnv) throws ServiceException;

	public List<Kit> obtenerKits() throws ServiceException;
	
	public List<Kit> obtenerKitsPorCodigo(String codigo) throws ServiceException;
	
	public Kit obtenerKitPorId(Long cdKit) throws ServiceException;
	
	public Kit obtenerKitPorCodigo(Integer cdEnv, String codigo) throws ServiceException;
	
	public Kit obtenerKitHistPorCodigo(Integer cdEnv, String codigo,Timestamp fechaCreacion) throws ServiceException;
	
	public String obtenerMailTemplate(Long cdKit, Integer cdEnv) throws ServiceException;
	
	public Integer actualizarEmailTemplate(Long cdKit, String filename) throws ServiceException;
	
	public Integer actualizarEmailTemplate(Long cdKit, Integer cdEnv, String filename) throws ServiceException;
	
	public Long guardarKit(Kit kit) throws ServiceException;
	
	public Integer actualizarKit(Kit kit) throws ServiceException;
	
	public Integer eliminarKit(Long id) throws ServiceException;
	
	public Integer guardarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException;
	
	public Integer eliminarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException;
	
	public Integer actualizarEnvKit(EnvKit envKit) throws ServiceException;
	
	public Integer eliminarEnvKit(EnvKit envKit) throws ServiceException;
	
	public Integer guardarEnvKit(EnvKit envKit) throws ServiceException;
}
