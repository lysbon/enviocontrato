package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.EnvKit;

@Component
public class EnvKitMapper implements RowMapper<EnvKit>{

	public static final String CD_KIT="CD_KIT";
	public static final String CD_ENV="CD_ENV";
	
	public static final String NB_CORREO_ENGINE="NB_CORREO_ENGINE";
	public static final String NB_CORREO_ASUNTO="NB_CORREO_ASUNTO";
	public static final String LB_CORREO_PLANTILLA="LB_CORREO_PLANTILLA";
	public static final String ST_ENV_CORREO="ST_ENV_CORREO";
	public static final String NB_REMITENTE="NB_REMITENTE";
	public static final String NB_REMITENTE_CC="NB_REMITENTE_CC";
	
	@Override
	public EnvKit mapRow(ResultSet rs, int rowNum) throws SQLException {
		EnvKit envKit = new EnvKit();
		envKit.setCdKit(rs.getLong(CD_KIT));
		envKit.setCdEnv(rs.getInt(CD_ENV));
		envKit.setStEnvCorreo(rs.getString(ST_ENV_CORREO));
		envKit.setNbCorreoEngine(rs.getString(NB_CORREO_ENGINE));
		envKit.setNbCorreoAsunto(rs.getString(NB_CORREO_ASUNTO));
		envKit.setNbRemitente(rs.getString(NB_REMITENTE));
		envKit.setNbRemitenteCC(rs.getString(NB_REMITENTE_CC));
		return envKit;
	}

}
