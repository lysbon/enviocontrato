package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.DetTramaDao;
import com.bbva.cuentas.aat.service.DetTramaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class DetTramaServiceImpl implements DetTramaService {
	
private static final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
	private DetTramaDao detTramaDao;
	
	
	@Override
	public List<DetTrama> obtenerDetTramasReprocesar(String estado)
			throws ServiceException {
		try {
			return detTramaDao.obtenerDetTramasParaReproceso(estado);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener seccios nes de plantilla.");
		}
	}
	
	@Override
	public List<DetTrama> obtenerDetTramasReprocesar(Long idTrama)
			throws ServiceException {
		try {
			/******** Multiplica tu Interes 23112017 *********/
			//"return itemDao.obtenerItemsPorSeccion(cdSeccion);"
			/******** Multiplica tu Interes 23112017 *********/
			return detTramaDao.obtenerDetTramasPorTramaReproceso(idTrama);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener secciones de plantilla.");
		}
	}
	
	
	@Override
	public List<DetTrama> obtenerDetTramasReprocesar(String estado,Long idTrama)
			throws ServiceException {
		try {
			/******** Multiplica tu Interes 23112017 *********/
			//"return itemDao.obtenerItemsPorSeccion(cdSeccion);"
			/******** Multiplica tu Interes 23112017 *********/
			return detTramaDao.obtenerDetTramasPorTramaReproceso(estado, idTrama);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener secciones de plantilla.");
		}
	}
	
	
	@Override
	public Integer actualizarTrama(DetTrama detTrama) throws ServiceException {
		try {
			return detTramaDao.update(detTrama);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");
		}
	}


}
