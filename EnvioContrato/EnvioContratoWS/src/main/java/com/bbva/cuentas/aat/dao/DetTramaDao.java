package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.DetTrama;

public interface DetTramaDao extends BaseDao<DetTrama, Long> {

	public List<DetTrama> obtenerDetTramasParaReproceso(String estado) throws DaoException;

	public List<DetTrama> obtenerDetTramasPorTramaReproceso(String estado, Long idTrama) throws DaoException;

	public List<DetTrama> obtenerDetTramasPorTramaReproceso(Long idTrama) throws DaoException;

	public int updateCliente(DetTrama detTrama) throws DaoException;

}