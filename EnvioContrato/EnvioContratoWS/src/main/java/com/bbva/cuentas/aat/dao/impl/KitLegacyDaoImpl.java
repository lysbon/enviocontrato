package com.bbva.cuentas.aat.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.KitLegacyDao;
import com.bbva.cuentas.aat.dao.mapper.EnvKitMapper;
import com.bbva.cuentas.aat.dao.mapper.KitLegacyMapper;
import com.bbva.cuentas.bean.KitLegacy;

@Component
public class KitLegacyDaoImpl extends BaseDaoImpl<KitLegacy, String> implements KitLegacyDao{
	
	private static final Logger logger = LoggerFactory.getLogger(KitLegacyDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG000_KIT";
	static final String TABLE_PK = "CD_KIT";
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	public KitLegacyDaoImpl(DataSource datasource, KitLegacyMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	
	public String obtenerMailTemplate(String cdKitPlantilla) {
		final LobHandler lobHandler = new DefaultLobHandler();
		List<String> lista = getJdbcTemplate().query(
				"SELECT  "+ EnvKitMapper.LB_CORREO_PLANTILLA
				+" FROM  "+ TABLE_NAME
				+" WHERE "+ TABLE_PK+" = ?", new Object[] { cdKitPlantilla },
				new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet rs, int rownumber) throws SQLException {
						//"String clobCorreoPlantilla = lobHandler.getClobAsString(rs, KitMapper.LB_CORREO_PLANTILLA);"
						
						return lobHandler.getClobAsString(rs, EnvKitMapper.LB_CORREO_PLANTILLA);
					}
				}
        );
		if(lista!=null && !lista.isEmpty()) return lista.get(0);
		
		return null;
	}
	
	public void actualizarEmailTemplate(final String cdKit, String filename) 
            throws DaoException {
		
		String sqlText = null;
		InputStream clobIs=null;
		InputStreamReader clobReader=null;
		
		try {        	
        	sqlText = "UPDATE "+TABLE_NAME+" SET "+EnvKitMapper.LB_CORREO_PLANTILLA+" = ? WHERE "+TABLE_PK+" = ? ";
        	final File clobIn = new File(filename);
        	clobIs = new FileInputStream(clobIn);
        	final LobHandler lobHandler = new DefaultLobHandler();
        	clobReader = new InputStreamReader(clobIs);
        	final InputStreamReader tmpClobReader=clobReader;
        	
        	getJdbcTemplate().execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	        lobCreator.setClobAsCharacterStream(ps, 1, tmpClobReader, (int)clobIn.length());
        	        ps.setString(2, cdKit);
        	      }
        	  }
        	);
			
        	//"clobReader.close();"
        	
        } catch (IOException e) {
        	logger.error("Caught I/O Exception: (Write CLOB value - Put Method).",e);
            throw new DaoException(ErrorCodes.D_UPDATE_ERROR,"Error al actualizar plantilla de correo");
        } finally {
        	try {if (clobIs!=null) clobIs.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        	try {if (clobReader!=null) clobReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        }
    }
	
}
