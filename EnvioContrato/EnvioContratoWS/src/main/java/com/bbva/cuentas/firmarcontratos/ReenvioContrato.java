//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reenvioContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reenvioContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clienteCodCentral" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clienteTipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clienteNroDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clienteEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clienteNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroContrato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinaGestora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="procedencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaApertura" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subProducto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codDivisa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ctaInterBancaria" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="plantillaCorreo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="grupoCorreo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *       &lt;attribute name="idContrato" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="idGrupo" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reenvioContrato", propOrder = {
    "clienteCodCentral",
    "clienteTipoDoc",
    "clienteNroDoc",
    "clienteEmail",
    "clienteNombre",
    "numeroContrato",
    "oficinaGestora",
    "procedencia",
    "fechaApertura",
    "codProducto",
    "subProducto",
    "codDivisa",
    "ctaInterBancaria",
    "plantillaCorreo",
    "grupoCorreo"
})
public class ReenvioContrato {

    @XmlElement(required = true)
    protected String clienteCodCentral;
    @XmlElement(required = true)
    protected String clienteTipoDoc;
    @XmlElement(required = true)
    protected String clienteNroDoc;
    @XmlElement(required = true)
    protected String clienteEmail;
    @XmlElement(required = true)
    protected String clienteNombre;
    @XmlElement(required = true)
    protected String numeroContrato;
    @XmlElement(required = true)
    protected String oficinaGestora;
    @XmlElement(required = true)
    protected String procedencia;
    @XmlElement(required = true)
    protected String fechaApertura;
    @XmlElement(required = true)
    protected String codProducto;
    @XmlElement(required = true)
    protected String subProducto;
    @XmlElement(required = true)
    protected String codDivisa;
    @XmlElement(required = true)
    protected String ctaInterBancaria;
    @XmlElement(required = true)
    protected String plantillaCorreo;
    protected int grupoCorreo;
    @XmlAttribute(name = "idContrato")
    @XmlSchemaType(name = "anySimpleType")
    protected String idContrato;
    @XmlAttribute(name = "idGrupo")
    @XmlSchemaType(name = "anySimpleType")
    protected String idGrupo;

    /**
     * Obtiene el valor de la propiedad clienteCodCentral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteCodCentral() {
        return clienteCodCentral;
    }

    /**
     * Define el valor de la propiedad clienteCodCentral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteCodCentral(String value) {
        this.clienteCodCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad clienteTipoDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteTipoDoc() {
        return clienteTipoDoc;
    }

    /**
     * Define el valor de la propiedad clienteTipoDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteTipoDoc(String value) {
        this.clienteTipoDoc = value;
    }

    /**
     * Obtiene el valor de la propiedad clienteNroDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteNroDoc() {
        return clienteNroDoc;
    }

    /**
     * Define el valor de la propiedad clienteNroDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteNroDoc(String value) {
        this.clienteNroDoc = value;
    }

    /**
     * Obtiene el valor de la propiedad clienteEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteEmail() {
        return clienteEmail;
    }

    /**
     * Define el valor de la propiedad clienteEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteEmail(String value) {
        this.clienteEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad clienteNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClienteNombre() {
        return clienteNombre;
    }

    /**
     * Define el valor de la propiedad clienteNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClienteNombre(String value) {
        this.clienteNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Define el valor de la propiedad numeroContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContrato(String value) {
        this.numeroContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaGestora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaGestora() {
        return oficinaGestora;
    }

    /**
     * Define el valor de la propiedad oficinaGestora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaGestora(String value) {
        this.oficinaGestora = value;
    }

    /**
     * Obtiene el valor de la propiedad procedencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedencia() {
        return procedencia;
    }

    /**
     * Define el valor de la propiedad procedencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedencia(String value) {
        this.procedencia = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaApertura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaApertura() {
        return fechaApertura;
    }

    /**
     * Define el valor de la propiedad fechaApertura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaApertura(String value) {
        this.fechaApertura = value;
    }

    /**
     * Obtiene el valor de la propiedad codProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProducto() {
        return codProducto;
    }

    /**
     * Define el valor de la propiedad codProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProducto(String value) {
        this.codProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad subProducto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubProducto() {
        return subProducto;
    }

    /**
     * Define el valor de la propiedad subProducto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubProducto(String value) {
        this.subProducto = value;
    }

    /**
     * Obtiene el valor de la propiedad codDivisa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodDivisa() {
        return codDivisa;
    }

    /**
     * Define el valor de la propiedad codDivisa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodDivisa(String value) {
        this.codDivisa = value;
    }

    /**
     * Obtiene el valor de la propiedad ctaInterBancaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCtaInterBancaria() {
        return ctaInterBancaria;
    }

    /**
     * Define el valor de la propiedad ctaInterBancaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCtaInterBancaria(String value) {
        this.ctaInterBancaria = value;
    }

    /**
     * Obtiene el valor de la propiedad plantillaCorreo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlantillaCorreo() {
        return plantillaCorreo;
    }

    /**
     * Define el valor de la propiedad plantillaCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlantillaCorreo(String value) {
        this.plantillaCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad grupoCorreo.
     * 
     */
    public int getGrupoCorreo() {
        return grupoCorreo;
    }

    /**
     * Define el valor de la propiedad grupoCorreo.
     * 
     */
    public void setGrupoCorreo(int value) {
        this.grupoCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad idContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdContrato() {
        return idContrato;
    }

    /**
     * Define el valor de la propiedad idContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdContrato(String value) {
        this.idContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad idGrupo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGrupo() {
        return idGrupo;
    }

    /**
     * Define el valor de la propiedad idGrupo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGrupo(String value) {
        this.idGrupo = value;
    }

}
