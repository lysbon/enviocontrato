package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.Flag;

public interface FlagDao extends BaseDao<Flag, Long>{

	public List<Flag> obtenerFlagsPorTipo(String tipo) throws DaoException;

	public List<Flag> obtenerFlags() throws DaoException;
	
}