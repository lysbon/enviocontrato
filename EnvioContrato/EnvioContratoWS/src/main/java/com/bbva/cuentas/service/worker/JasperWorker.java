package com.bbva.cuentas.service.worker;

import java.util.Collection;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ConfigurationPath;

public interface JasperWorker {
	
	
	public void process(Plantilla plantilla, 
			ConfigurationPath cfg,
			Map<String,Object> parametros,
			Collection<Map<String,?>> contenido) throws ServiceException;


	public void processStream(
			Plantilla plantilla, 
			ConfigurationPath cfg,
			byte[] is,
			Map<String,Object> parametros,
			Collection<Map<String,?>> contenido) throws ServiceException;

}
