package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.service.exception.ServiceException;

public interface TramaService {
	
	List<Trama> obtenerTramasReprocesar(String Estado) throws ServiceException;
	
	Integer actualizarTrama(Trama trama) throws ServiceException; 
	
	Long insertaTrama(Trama trama);

	void updateTrama(Trama trama);

	void updateDetTrama(DetTrama detTrama);

	Long insertaDetTrama(DetTrama detTrama);

	Integer limpiarTramas() throws ServiceException;

	List<DetTrama> obtenerDetTramasReprocesar(String estado,Long idTrama) throws ServiceException;

	Trama obtenerTramasPorId(Long i) throws ServiceException;

}
