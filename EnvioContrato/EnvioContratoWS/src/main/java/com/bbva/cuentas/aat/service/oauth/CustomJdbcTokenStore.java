package com.bbva.cuentas.aat.service.oauth;

import javax.sql.DataSource;

import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import com.bbva.cuentas.util.Constants;

public class CustomJdbcTokenStore extends JdbcTokenStore{
	
	private static final String WHERE_TOKEN_ID = "oauth_access_token where token_id = ?";
	
	public CustomJdbcTokenStore(DataSource dataSource) {
		super(dataSource);
		super.setInsertAccessTokenSql("insert into "+Constants.SCHEMA+Constants.SCHEMA_POINT+"oauth_access_token (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) values (?, ?, ?, ?, ?, ?, ?)");
		super.setSelectAccessTokenSql("select token_id, token from "+Constants.SCHEMA+Constants.SCHEMA_POINT+WHERE_TOKEN_ID);
		super.setSelectAccessTokenFromAuthenticationSql("select token_id, token from "+Constants.SCHEMA+Constants.SCHEMA_POINT+"oauth_access_token where authentication_id = ?");
		super.setSelectAccessTokenAuthenticationSql("select token_id, authentication from "+Constants.SCHEMA+Constants.SCHEMA_POINT+WHERE_TOKEN_ID);
		super.setDeleteAccessTokenSql("delete from "+Constants.SCHEMA+Constants.SCHEMA_POINT+WHERE_TOKEN_ID);
	}	

}
