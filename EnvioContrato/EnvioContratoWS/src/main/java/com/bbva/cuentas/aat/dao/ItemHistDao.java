package com.bbva.cuentas.aat.dao;

import java.util.List;
import java.sql.Timestamp;
import com.bbva.cuentas.bean.ItemStamp;

public interface ItemHistDao extends BaseDao<ItemStamp, Long>{
	
	public List<ItemStamp> obtenerItemsPorSeccionAscendente(Long cdSeccion,Timestamp fecHoraCreacion) throws DaoException;
	
}
