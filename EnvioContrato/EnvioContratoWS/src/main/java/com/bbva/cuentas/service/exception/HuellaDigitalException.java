package com.bbva.cuentas.service.exception;

import java.text.MessageFormat;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class HuellaDigitalException extends ServiceException{
	
	private static final long serialVersionUID = 1659667236074434395L;

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final String MENSAJE = "Error en la carga de huellas digitales. ";
	private static final String FORMATO_LOG = MENSAJE + " ''{0}''.";
		
	public HuellaDigitalException(String mensaje) {
		super(CODIGO_SERVICE_HUELLA_CARGA, MENSAJE);
		setPaso(ProcesoEnvioContrato.OBTENER_HUELLA_DIGITAL);
		logger.info(MessageFormat.format(FORMATO_LOG, mensaje));
	}
	
}
