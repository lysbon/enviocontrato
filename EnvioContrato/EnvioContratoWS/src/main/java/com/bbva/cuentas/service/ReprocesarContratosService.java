package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;
import java.sql.Timestamp;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.ClienteHuellaDigital;
import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public interface ReprocesarContratosService {

	void cargarPlantillasContratoCliente(Kit kit,boolean flgReproceso,Timestamp fecHoraCreacion) throws ServiceException;
	
	void generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,
			boolean imprimirHuellas,FirmaContrato firmaContrato) throws ServiceException;
	
    void generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,FirmaContrato firmaContrato,boolean imprimirHuellas) throws ServiceException;
    
    int generarContratosResutl(
    		List<Plantilla> plantillas,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,FirmaContrato firmaContrato,boolean imprimirHuellas,Trama trama,
    		boolean flgReproceso,Timestamp fecHoraCreacion) throws ServiceException;
    
    Plantilla generarContrato(
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,
    		Plantilla plantilla,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ConfigurationPath cfg,
    		boolean imprimirHuellas,FirmaContrato firmaContrato) throws ServiceException;
    
    
    void generarContratoTrama(
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,
    		Plantilla plantilla,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ConfigurationPath cfg,
    		boolean imprimirHuellas,FirmaContrato firmaContrato,Trama trama,boolean flgReproceso,Timestamp fecHoraCreacion) throws ServiceException;
    
    Kit obtenerKitPorGrupo(String idGrupo,String idContrato) throws ServiceException;

    Kit obtenerKitPorPlantilla(String idGrupo, String idContrato) throws ServiceException;
    
	List<Documento> generarDocumentosBase64(
			List<Plantilla> plantillas,
			FirmaContrato firmaContrato,
			ClienteDTO cliente,
			List<ClienteDTO> clientes,
			Map<String,String> parametros) throws ServiceException;
	
	boolean evaluarPlantilla(Plantilla template,Map<String,String> parametros) throws DaoException, ServiceException;
	
	int generarContratosTrama(
    		List<Plantilla> plantillas,
    		Map<String,String> parametros,
    		List<ClienteHuellaDigital> listaHuellas,
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,
			boolean imprimirHuellas,FirmaContrato firmaContrato,Trama trama,boolean flgReproceso,Timestamp fecHoraCreacion) throws ServiceException;

	Kit obtenerKitHistPorGrupo(String idGrupo,String idContrato,Timestamp fechaCreacion) throws ServiceException;

}