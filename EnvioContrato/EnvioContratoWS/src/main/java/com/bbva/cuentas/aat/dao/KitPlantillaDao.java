package com.bbva.cuentas.aat.dao;

import com.bbva.cuentas.bean.KitPlantilla;

public interface KitPlantillaDao extends BaseDao<KitPlantilla, String>{
	
	public Integer delete(KitPlantilla kitPlantilla) throws DaoException;
	
	public Integer deletePorKit(Long cdKit) throws DaoException;
	
}
