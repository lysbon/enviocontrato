package com.bbva.cuentas.service.worker;

import java.util.List;

import com.bbva.cuentas.bean.ArchivoFileUnico;

public interface SFTPWorker {
	
	public void enviarArchivos(List<ArchivoFileUnico> archivos, String prefijo, String ext) throws Exception;
	
	public void moverArchivos(List<ArchivoFileUnico> archivos, String rutaFileunico) throws Exception;
	
	public void moverArchivo(String rutaOrigenFileunico, String rutaDestinoFileunico) throws Exception;
	
	public void copiarArchivos(List<ArchivoFileUnico> archivos, String rutaFileunico) throws Exception;
	
	public void copiarArchivos(List<ArchivoFileUnico> archivos, String rutaFileunico, String prefijo, String ext) throws Exception;
	
	public void copiarArchivo(String rutaOrigen,String rutaFileunico) throws Exception;
	
	public void eliminarArchivos(List<ArchivoFileUnico> archivos) throws Exception;
	
}
