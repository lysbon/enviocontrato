package com.bbva.cuentas.enums;

public enum FileUnicoProcedencia {
	BPOD("BPO-D"),
    BPOS("BPO-S"),
    CONTRATACION_SENCILLA("CS"),
    PRESTO("CP"),
    BANCA_DIGITAL("BD"),
    WEBPYMES("WP"),
    FILEUNICO("FU"),
    NACAR("NA");
	private String codigo;
	FileUnicoProcedencia(String codigo) {
        this.codigo = codigo;
    }
    public String value() {
        return codigo;
    }
}
