package com.bbva.cuentas.enums;

public enum TipoDoi {
	CARNETEXTR("E"),
    DNI("L"),
    PASAPORTE("P"),
    RUC("R");
	private String codigo;
	TipoDoi(String codigo) {
        this.codigo = codigo;
    }
    public String value() {
        return codigo;
    }
}
