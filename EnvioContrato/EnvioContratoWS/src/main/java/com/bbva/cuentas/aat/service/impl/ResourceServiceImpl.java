package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ImgDao;
import com.bbva.cuentas.aat.service.ResourceService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.ResourceItem;

@Service
public class ResourceServiceImpl implements ResourceService{
	
	private static final Logger logger = LoggerFactory.getLogger(ResourceServiceImpl.class);

	@Autowired
	private ImgDao imgDao;
	
	@Override
	public byte[] obtenerImagen(Long id) throws ServiceException{
		try{
			return imgDao.obtenerImagen(id);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,"No se pudo obtener items.");
		}
	}
	
	@Override
	public String obtenerImagenBase64(Long id) throws ServiceException{
		try{
			byte[] img = imgDao.obtenerImagen(id);
			String encodedBase64 = new String(Base64.encodeBase64(img));
			return encodedBase64;
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,"No se pudo obtener items.");
		}
	}
	
	@Override
	public List<ResourceItem> obtenerImagenes() throws ServiceException{
		try{
			return imgDao.obtenerImagenes();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,"No se pudo obtener items.");
		}
	}
	
	@Override
	public Long guardarRecurso(ResourceItem recurso) throws ServiceException {
		try {
			return imgDao.insertAndReturnId(recurso);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_RES_GUARDAR_RECURSO,"No se pudo guardar recurso.");
		}
	}

	@Override
	public Integer actualizarRecurso(ResourceItem recurso)
			throws ServiceException {
		try {
			return imgDao.update(recurso);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_RES_ACTUALIZAR_RECURSO,"No se pudo actualizar recurso.");
		}
	}

	@Override
	public Integer eliminarRecurso(Long id) throws ServiceException {
		try {
			return imgDao.delete(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_RES_ELIMINAR_RECURSO,"No se pudo eliminar recurso.");
		}
	}
	
}
