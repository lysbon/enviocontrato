package com.bbva.cuentas.ws;

import com.bbva.cuentas.firmarcontratos.ReprocesarContratosRequest;
import com.bbva.cuentas.firmarcontratos.ReprocesarContratosResponse;

public interface MarshallingReprocesarContratosService {

	public static final String NAMESPACE = "http://bbva.com/cuentas/firmarContratos";
    public static final String REPROCESAR_CONTRATOS_REQUEST = "reprocesar-contratos-request";

    /**
     * Gets person list.
     */
    public ReprocesarContratosResponse reprocesarContratos(ReprocesarContratosRequest request);
    
}
