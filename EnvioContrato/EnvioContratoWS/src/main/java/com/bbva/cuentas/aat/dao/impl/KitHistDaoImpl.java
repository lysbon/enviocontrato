package com.bbva.cuentas.aat.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.KitHistDao;
import com.bbva.cuentas.aat.dao.mapper.KitMapper;
import com.bbva.cuentas.bean.Kit;

@Component
public class KitHistDaoImpl extends BaseDaoImpl<Kit, Long> implements KitHistDao{
	
	private static final Logger logger = LoggerFactory.getLogger(KitHistDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG010_KIT_HIST";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	KitMapper kitMapper;
	
	@Autowired
	public KitHistDaoImpl(DataSource datasource, KitMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	

	@Override
	public Kit obtenerKitPorCodigo(Integer cdEnv, String codigo,Timestamp fechaCreacion) throws DaoException {
		
		List<Kit> listaKits = null;
		try{
			String sql = 
					"SELECT P.CD_KIT,P.CD_CODIGO,P.NB_NOMBRE_KIT,P.ST_ENV_CORREO,P.ST_FILEUNICO,P.FL_VAL_HUELLA,"+
					"P.NB_CORREO_ENGINE,P.NB_CORREO_ASUNTO,P.ST_SIGNBOX,P.ST_HUELLA,P.FH_INICIO,P.FH_FIN "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+EnvKitDaoImpl.TABLE_NAME+" EP "+
					"ON     P.CD_KIT = EP.CD_KIT "+
					"WHERE  EP.CD_ENV = ? "+
					"AND    P.CD_CODIGO = ? "+
					"AND    CAST(P.FH_INICIO AS TIMESTAMP)<=? "+
					"AND    ?<=CAST(NVL(P.FH_FIN,SYSDATE) AS TIMESTAMP) "+
					"ORDER  BY P.CD_KIT";
			Object[] args = new Object[4];
			args[0] = cdEnv;
			args[1] = codigo;
			args[2] = fechaCreacion;
			args[3] = fechaCreacion;
			listaKits = getJdbcTemplate().query(sql,args,kitMapper);
			if(listaKits!=null && !listaKits.isEmpty()){
				return listaKits.get(0);
			}
			return null;
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
	}
	
}
