package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.FlagPlantilla;

public interface FlagPlantillaDao extends BaseDao<FlagPlantilla, Long>{

	public List<FlagPlantilla> obtenerFlagsPorPlantilla(Long id) throws DaoException;

	public FlagPlantilla obtenerFlagPlantilla(Long cdPlantilla, Long cdFlag) throws DaoException;

	public Integer delete(Long cdPlantilla, Long cdFlag) throws DaoException;
	
}