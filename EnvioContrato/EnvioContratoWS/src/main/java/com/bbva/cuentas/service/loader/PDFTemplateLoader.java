package com.bbva.cuentas.service.loader;

import java.sql.*;

import java.io.*;

public interface PDFTemplateLoader  {

	public void writeBLOBPut(final int cdPlantilla, String inputTextFileName) 
            throws IOException, SQLException;
	
}
