package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.KitPlantillaLegacy;

@Component
public class KitPlantillaLegacyMapper implements RowMapper<KitPlantillaLegacy>{

	public static final String CD_KIT="CD_KIT";
	public static final String CD_PLANTILLA="CD_PLANTILLA";
	
	@Override
	public KitPlantillaLegacy mapRow(ResultSet rs, int rowNum) throws SQLException {
		KitPlantillaLegacy kitPlantilla = new KitPlantillaLegacy();
		kitPlantilla.setCdKit(rs.getString(CD_KIT));
		kitPlantilla.setCdPlantilla(rs.getLong(CD_PLANTILLA));
		return kitPlantilla;
	}

}
