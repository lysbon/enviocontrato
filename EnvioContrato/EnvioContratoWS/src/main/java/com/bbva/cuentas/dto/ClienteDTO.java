package com.bbva.cuentas.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.firmarcontratos.Cliente;

public class ClienteDTO {

	private Cliente cliente;
	private Map<Long,List<ConfigurationPath>> listCfg;
	
	private boolean procesarFileUnico;
	private boolean procesarMail;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Map<Long, List<ConfigurationPath>> getListCfg() {
		if(listCfg==null)
			listCfg = new HashMap<Long, List<ConfigurationPath>>();
		return listCfg;
	}
	public void setListCfg(Map<Long,List<ConfigurationPath>> listCfg) {
		this.listCfg = listCfg;
	}
	
	public void addCfg(Long cdPlantilla,ConfigurationPath cfg) {
		if(getListCfg().containsKey(cdPlantilla)) {
			getListCfg().get(cdPlantilla).add(cfg);
		}else{
			getListCfg().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			getListCfg().get(cdPlantilla).add(cfg);
		}
	}
	
	public void addCfg(Long cdPlantilla,List<ConfigurationPath> listCfg) {
		if(getListCfg().containsKey(cdPlantilla)) {
			for(ConfigurationPath cfg : listCfg) {
				getListCfg().get(cdPlantilla).add(cfg);
			}
		}else{
			getListCfg().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			for(ConfigurationPath cfg : listCfg) {
				getListCfg().get(cdPlantilla).add(cfg);
			}
		}
	}
	
	public List<ConfigurationPath> getListCfg(Long cdPlantilla) {
		if(listCfg==null)
			listCfg = new HashMap<Long, List<ConfigurationPath>>();
		return listCfg.get(cdPlantilla);
	}
	public boolean isProcesarFileUnico() {
		return procesarFileUnico;
	}
	public void setProcesarFileUnico(boolean procesarFileUnico) {
		this.procesarFileUnico = procesarFileUnico;
	}
	public boolean isProcesarMail() {
		return procesarMail;
	}
	public void setProcesarMail(boolean procesarMail) {
		this.procesarMail = procesarMail;
	}
	
}
