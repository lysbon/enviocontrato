package com.bbva.cuentas.service.exception;

import java.text.MessageFormat;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class PlantillaNoEncuentraException extends ServiceException{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final long serialVersionUID = 2834674696845011801L;
	private static final String MENSAJE = "La plantilla/kit no existe.";
	private static final String FORMATO_LOG = "La plantilla/kit ''{0}'' no existe.";
	
	public PlantillaNoEncuentraException(String codigo) {
		super(CODIGO_PLANTILLA_NO_ENCUENTRA, MENSAJE);
		setPaso(ProcesoEnvioContrato.CARGA_PLANTILLAS);
		logger.info(MessageFormat.format(FORMATO_LOG, codigo));
	}
	
}
