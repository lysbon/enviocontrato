package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public interface CoreService {
	
	void process(RequestDTO request,Kit kit) throws ServiceException;

	Documentos processPreviewKit(RequestDTO request) throws ServiceException;

	Documentos processPreviewPlantilla(RequestDTO request) throws ServiceException;

	List<ClienteDTO> procesarRutas(Kit kit, RequestDTO request);
	
	ClienteDTO procesarRutasSimple(String rutaGenerada,List<Plantilla> listTemplates, FirmaContrato firmaContrato);
	
	List<ClienteDTO> processGenerarContratos(RequestDTO request, Kit kit, Map<String, Object> parametros) throws ServiceException;
	
	void processMail(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros);
	
	void processFileUnico(RequestDTO request, Kit kit, ClienteDTO cliente);
	
	void depurarArchivos(List<ClienteDTO> listaClientes);
	
	boolean huellaActivo(Kit kit);
	
}