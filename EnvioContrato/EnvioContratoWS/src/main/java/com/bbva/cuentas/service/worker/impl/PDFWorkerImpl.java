package com.bbva.cuentas.service.worker.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.service.worker.PDFWorker;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.BarcodePDF417;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Element;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PDFWorkerImpl implements PDFWorker{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${item.label.huella}")
	private String labelHuella;
	
	@Value("${fonts.carga}")
	private String[] fonts;
	@Value("${fonts.carga.dir}")
	private String fontsDir;
	
	@PostConstruct
	public void init(){		
		logger.info("PDFWorker.init() - Cargando fuentes2");		
		installPropertiesFonts();
	}
	
	private void installPropertiesFonts() {
		FontFactory.registerDirectory(fontsDir);
		Set<String> fonts = new TreeSet<String>(FontFactory.getRegisteredFonts());
		for (String fontname : fonts) {
            showFontInfo(fontname);
        }
	}
	
	protected void showFontInfo(String fontname) {
		System.out.println(fontname);
//        Font font = null;
//        try {
//            font = FontFactory.getFont(fontname, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//        }
//        catch(Exception e) {
//            return;
//        }
//        System.out.println(String.format("Postscript name for %s: %s", fontname, font.getBaseFont().getPostscriptFontName()));
//        System.out.println("  FamilyFont->"+font.getFamilyname());
//		System.out.println("  Size-"+font.getSize());
//		System.out.println("  Base-"+font.getBaseFont());
    }

	private void prepare(PDFBean pdfBean, byte[] is, String rutaFormatoOut) throws ServiceException{
		try {
			pdfBean.prepare(is, rutaFormatoOut);
		} catch (Exception e) {
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"No se pudo inicializar reader. "+e.getMessage());
		}
	}
	
	public void prepare(PDFBean pdfBean, String rutaFormatoOut) throws ServiceException{
		try {
			pdfBean.prepare(rutaFormatoOut);			
		} catch (Exception e) {
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"No se pudo inicializar PDFWorker. "+e.getMessage());
		}
	}
	
	public void prepare(PDFBean pdfBean, String rutaFormato,String rutaFormatoOut) throws ServiceException{
		try {
			pdfBean.prepare(rutaFormato, rutaFormatoOut);
		} catch (Exception e) {
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"No se pudo inicializar reader. "+e.getMessage());
		}
	}
	
	private void prepareOverride(PDFBean pdfBean, String rutaFormato) throws ServiceException{
		try {
			pdfBean.prepare(rutaFormato, rutaFormato);
		} catch (Exception e) {
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"No se pudo inicializar reader. "+e.getMessage());
		}
	}
	
	public ByteArrayOutputStream prepareOnMemory(PDFBean pdfBean, String rutaFormato) throws ServiceException{
		try {
			return pdfBean.prepareOnMemory(rutaFormato);
		} catch (Exception e) {
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"No se pudo inicializar reader. "+e.getMessage());
		}
	}
	
	public Font obtenerFuente(String fuente){
		if(StringUtils.isEmpty(fuente)){
			return FontFactory.getFont(Constants.FONT_ITEXT_CALIBRI,BaseFont.WINANSI, BaseFont.EMBEDDED, 11); 
		}else{
			int style = Font.NORMAL;
			if(StringUtils.contains(fuente, Constants.FONT_COD_BOLD)){
				style = Font.BOLD;
			}
			int size  = 11;
			if(StringUtils.contains(fuente, Constants.FONT_COD_9)){
				size = 9;
			}else if(StringUtils.contains(fuente, Constants.FONT_COD_10)){
				size = 10;
			}else if(StringUtils.contains(fuente, Constants.FONT_COD_8)){
				size = 8;
			}
			if(StringUtils.contains(fuente, Constants.FONT_COD_CALIBRI)){
				return FontFactory.getFont(Constants.FONT_ITEXT_CALIBRI,BaseFont.WINANSI, BaseFont.EMBEDDED, size, style);
			}else if(StringUtils.contains(fuente, Constants.FONT_COD_ARIAL)){
				return FontFactory.getFont(Constants.FONT_ITEXT_ARIAL,BaseFont.WINANSI, BaseFont.EMBEDDED, size, style);
			}else if(StringUtils.contains(fuente, Constants.FONT_COD_COURIER_NEW)){
				return FontFactory.getFont(Constants.FONT_ITEXT_COURIER_NEW,BaseFont.WINANSI, BaseFont.EMBEDDED, size, style);
			}else{
				return FontFactory.getFont(Constants.FONT_ITEXT_CALIBRI,BaseFont.WINANSI, BaseFont.EMBEDDED, size, style);
			}
		}
	}
	
	public void itemTexto(PdfContentByte content,ItemStamp item) throws ServiceException{
		try {
			ColumnText.showTextAligned(
					content,
					item.getNuAlinear(), 
					new Paragraph(
						StringUtils.isEmpty(item.getNbValorVar())?StringUtils.EMPTY:item.getNbValorVar(),
						obtenerFuente(item.getNbFuente())),
						item.getNuCoorX(), item.getNuCoorY(), 0);
		} catch (Exception e) {
			logger.error("Error en impresion de texto."+item,e);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
		}
	}
	
	public void itemBase64(PdfContentByte content,ItemStamp item) throws ServiceException{
		try{
			if(!StringUtils.isEmpty(item.getNbValorVar())){
				byte[] fpDecoded = Base64.decodeBase64(item.getNbValorVar());
				Image imgHuella = Image.getInstance(fpDecoded);
				imgHuella.scaleAbsolute(item.getNuScX(),item.getNuScY());
				PdfPTable tableImagesTCHuella = new PdfPTable(1);
				tableImagesTCHuella.setTotalWidth(item.getNuAncho());
				tableImagesTCHuella.setHorizontalAlignment(Element.ALIGN_LEFT);
				PdfPCell cellImagesTCHuella;
				cellImagesTCHuella = new PdfPCell(imgHuella);
				cellImagesTCHuella.setBorder(0);
				cellImagesTCHuella.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cellImagesTCHuella.setHorizontalAlignment(Element.ALIGN_CENTER);
				tableImagesTCHuella.addCell(cellImagesTCHuella);
				tableImagesTCHuella.writeSelectedRows(0,1,item.getNuCoorX(),item.getNuCoorY(),content);
			}
		}catch(com.itextpdf.text.BadElementException e){
			logger.error("Error en impresion de imagen. ",e);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
		}catch(java.net.MalformedURLException e){
			logger.error("Error en impresion de imagen. ",e);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
		}catch(java.io.IOException e){
			logger.error("Error en impresion de imagen. ",e);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
		}
	}
	
	public void itemImagen(PdfContentByte content,ItemStamp item) throws ServiceException{
		try{
			if(!StringUtils.isEmpty(item.getNbValorVar())){
				byte[] fpDecoded = item.getImg();
				Image img = Image.getInstance(fpDecoded);
				img.scaleAbsolute(item.getNuScX(),item.getNuScY());
				PdfPTable tableImagesTCHuella = new PdfPTable(1);
				tableImagesTCHuella.setTotalWidth(item.getNuAncho());
				tableImagesTCHuella.setHorizontalAlignment(Element.ALIGN_LEFT);
				PdfPCell cellImagesTCHuella;
				cellImagesTCHuella = new PdfPCell(img);
				cellImagesTCHuella.setBorder(0);
				cellImagesTCHuella.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cellImagesTCHuella.setHorizontalAlignment(Element.ALIGN_CENTER);
				tableImagesTCHuella.addCell(cellImagesTCHuella);
				tableImagesTCHuella.writeSelectedRows(0,1,item.getNuCoorX(),item.getNuCoorY(),content);
			}
		}catch(com.itextpdf.text.BadElementException e){
			logger.error("Error en impresion de imagen. ",e);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
		}catch(java.net.MalformedURLException e1){
			logger.error("Error en impresion de imagen. ",e1);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e1.getMessage());
		}catch(java.io.IOException e2){
			logger.error("Error en impresion de imagen. ",e2);
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e2.getMessage());
		}
	}
	
	public void itemBarcode(PdfContentByte content,ItemStamp item) throws ServiceException{
		if(!StringUtils.isEmpty(item.getNbValorVar())){
			PdfPTable table = new PdfPTable(1);
			table.setTotalWidth(item.getNuAncho());
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
			try {
				BarcodePDF417 pdf417 = new BarcodePDF417();
		        pdf417.setText(item.getNbValorVar());
		        Image imgBarcode;
				imgBarcode = pdf417.getImage();
				imgBarcode.setAlignment(Element.ALIGN_CENTER);
				imgBarcode.scaleAbsolute(item.getNuScX(),item.getNuScY());
				PdfPCell cell = new PdfPCell(imgBarcode);
		        cell.setBorder(0);
		        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			    table.addCell(cell);						        
		        table.setTotalWidth(100f);
			    table.writeSelectedRows(0,1,item.getNuCoorX(),item.getNuCoorY(),content);
			} catch (BadElementException e) {
				logger.error("Error al dibujar codigo de barras.",e);
				throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,e.getMessage());
			}
		}
	}
	
	public void buildSections(PDFBean pdfBean, List<ItemSection> listaSections) throws ServiceException{
		if(listaSections!=null){
			for(ItemSection itemSection : listaSections){
				List<ItemStamp> items = itemSection.getListItems();
				if(items!=null && items.size()>0){
					PdfContentByte content1TC = null;
					try {
						content1TC = pdfBean.crearContenidoAbsoluto(itemSection.getNuPagina());
					} catch (ServiceException e) {
						logger.error("Error al inicializar pagina PDF."+e.getMessage());
						throw e;
					}
					for(ItemStamp item : items){
						if(Constants.TYPE_TEXT.equals(item.getChTipo())){
							itemTexto(content1TC,item);
						}else if(Constants.TYPE_IMAGE.equals(item.getChTipo())){
							itemImagen(content1TC, item);
						}else if(Constants.TYPE_IMAGE_BASE64.equals(item.getChTipo())||
								Constants.TYPE_IMAGE_HUELLA.equals(item.getChTipo())){
							itemBase64(content1TC, item);
						}else if(Constants.TYPE_BAR_CODE.equals(item.getChTipo())){
							itemBarcode(content1TC, item);
						}
					}
				}
			}
		}
	}
	
	public void finish(PDFBean pdfBean) throws ServiceException{
		try {
			pdfBean.finish();
		} catch (DocumentException e) {
			logger.error("Error al finalizar PDF.");
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"Error al finalizar PDF.");
		} catch (IOException e) {
			logger.error("Error al finalizar PDF.");
			throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"Error al finalizar PDF.");
		} 
	}
	
	public void processStream(Plantilla plantilla, 
			List<ItemSection> listaSections,
			byte[] is, 
			ConfigurationPath cfg) throws ServiceException{
		PDFBean pdfBean = new PDFBean();
		try {
			prepare(pdfBean,is, cfg.getRutaSalidaPDF());
			buildSections(pdfBean,listaSections);
			finish(pdfBean);
		} catch (ServiceException e) {
			logger.error("Error al generar PDF."+plantilla,e);
			throw e;
		}
	}
	
	public void reprocess(Plantilla plantilla, 
			List<ItemSection> listaSections, 
			ConfigurationPath cfg) throws ServiceException{
		PDFBean pdfBean = new PDFBean();
		try {
			prepareOverride(pdfBean,cfg.getRutaSalidaPDF());
			buildSections(pdfBean,listaSections);
			finish(pdfBean);
		} catch (ServiceException e) {
			logger.error("Error al reprocesar PDF."+plantilla,e);
			throw e;
		}
	}
	
	public void process(Plantilla plantilla, 
			List<ItemSection> listaSections, 
			ConfigurationPath cfg) throws ServiceException{
		PDFBean pdfBean = new PDFBean();
		try {
			prepare(pdfBean,plantilla.getPathTemplate(), cfg.getRutaSalidaPDF());
			buildSections(pdfBean,listaSections);
			finish(pdfBean);
		} catch (ServiceException e) {
			logger.error("Error al generar PDF."+plantilla,e);
			throw e;
		}
	}

	public void processNuevo(Plantilla plantilla, 
			List<ItemSection> listaSections, 
			ConfigurationPath cfg) throws ServiceException {
		PDFBean pdfBean = new PDFBean();
		try {
			prepare(pdfBean,cfg.getRutaSalidaPDF());
			buildSections(pdfBean,listaSections);
			finish(pdfBean);
		} catch (ServiceException e) {
			logger.error("Error al generar PDF."+plantilla,e);
			throw e;
		}
	}
}
