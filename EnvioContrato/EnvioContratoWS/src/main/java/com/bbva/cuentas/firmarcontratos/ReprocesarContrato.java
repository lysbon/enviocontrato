//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para reprocesarContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="reprocesarContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listaItems" type="{http://bbva.com/cuentas/firmarContratos}itemContrato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaClientes" type="{http://bbva.com/cuentas/firmarContratos}cliente" maxOccurs="unbounded"/>
 *         &lt;element name="numeroContrato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idContrato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="oficinaGestora" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="procedencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaOrigen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaFirma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaCSV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaFileUnico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *       &lt;attribute name="flagHuellaDigital" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="flagFirmaDigital" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="flagFileUnico" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *       &lt;attribute name="flagEmail" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reprocesarContrato", propOrder = {
    "listaItems",
    "listaClientes",
    "numeroContrato",
    "idContrato",
    "oficinaGestora",
    "procedencia",
    "rutaOrigen",
    "rutaFirma",
    "rutaCSV",
    "rutaFileUnico"
})
public class ReprocesarContrato {

    protected List<ItemContrato> listaItems;
    @XmlElement(required = true)
    protected List<Cliente> listaClientes;
    @XmlElement(required = true)
    protected String numeroContrato;
    @XmlElement(required = true)
    protected String idContrato;
    @XmlElement(required = true)
    protected String oficinaGestora;
    @XmlElement(required = true)
    protected String procedencia;
    @XmlElement(required = true)
    protected String rutaOrigen;
    @XmlElement(required = true)
    protected String rutaFirma;
    @XmlElement(required = true)
    protected String rutaCSV;
    @XmlElement(required = true)
    protected String rutaFileUnico;
    @XmlAttribute(name = "flagHuellaDigital")
    @XmlSchemaType(name = "anySimpleType")
    protected String flagHuellaDigital;
    @XmlAttribute(name = "flagFirmaDigital")
    @XmlSchemaType(name = "anySimpleType")
    protected String flagFirmaDigital;
    @XmlAttribute(name = "flagFileUnico")
    @XmlSchemaType(name = "anySimpleType")
    protected String flagFileUnico;
    @XmlAttribute(name = "flagEmail")
    @XmlSchemaType(name = "anySimpleType")
    protected String flagEmail;

    /**
     * Gets the value of the listaItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemContrato }
     * 
     * 
     */
    public List<ItemContrato> getListaItems() {
        if (listaItems == null) {
            listaItems = new ArrayList<ItemContrato>();
        }
        return this.listaItems;
    }

    /**
     * Gets the value of the listaClientes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaClientes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaClientes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cliente }
     * 
     * 
     */
    public List<Cliente> getListaClientes() {
        if (listaClientes == null) {
            listaClientes = new ArrayList<Cliente>();
        }
        return this.listaClientes;
    }

    /**
     * Obtiene el valor de la propiedad numeroContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Define el valor de la propiedad numeroContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContrato(String value) {
        this.numeroContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad idContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdContrato() {
        return idContrato;
    }

    /**
     * Define el valor de la propiedad idContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdContrato(String value) {
        this.idContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaGestora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaGestora() {
        return oficinaGestora;
    }

    /**
     * Define el valor de la propiedad oficinaGestora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaGestora(String value) {
        this.oficinaGestora = value;
    }

    /**
     * Obtiene el valor de la propiedad procedencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcedencia() {
        return procedencia;
    }

    /**
     * Define el valor de la propiedad procedencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcedencia(String value) {
        this.procedencia = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaOrigen() {
        return rutaOrigen;
    }

    /**
     * Define el valor de la propiedad rutaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaOrigen(String value) {
        this.rutaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaFirma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaFirma() {
        return rutaFirma;
    }

    /**
     * Define el valor de la propiedad rutaFirma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaFirma(String value) {
        this.rutaFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaCSV.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaCSV() {
        return rutaCSV;
    }

    /**
     * Define el valor de la propiedad rutaCSV.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaCSV(String value) {
        this.rutaCSV = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaFileUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaFileUnico() {
        return rutaFileUnico;
    }

    /**
     * Define el valor de la propiedad rutaFileUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaFileUnico(String value) {
        this.rutaFileUnico = value;
    }

    /**
     * Obtiene el valor de la propiedad flagHuellaDigital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagHuellaDigital() {
        return flagHuellaDigital;
    }

    /**
     * Define el valor de la propiedad flagHuellaDigital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagHuellaDigital(String value) {
        this.flagHuellaDigital = value;
    }

    /**
     * Obtiene el valor de la propiedad flagFirmaDigital.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagFirmaDigital() {
        return flagFirmaDigital;
    }

    /**
     * Define el valor de la propiedad flagFirmaDigital.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagFirmaDigital(String value) {
        this.flagFirmaDigital = value;
    }

    /**
     * Obtiene el valor de la propiedad flagFileUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagFileUnico() {
        return flagFileUnico;
    }

    /**
     * Define el valor de la propiedad flagFileUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagFileUnico(String value) {
        this.flagFileUnico = value;
    }

    /**
     * Obtiene el valor de la propiedad flagEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagEmail() {
        return flagEmail;
    }

    /**
     * Define el valor de la propiedad flagEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagEmail(String value) {
        this.flagEmail = value;
    }

}
