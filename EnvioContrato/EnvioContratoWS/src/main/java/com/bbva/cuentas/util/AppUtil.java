package com.bbva.cuentas.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ItemStamp;

/**
 * 
 * Clase utilitaria para la aplicacion
 *  
 * @author christian.chavez
 *
 */

@Component
public class AppUtil {
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(AppUtil.class);
	
	private AppUtil() {
	    throw new IllegalStateException("Utility class");
	}



	/**
	 * Permite obtener una lista de los mensajes de error para una excepcion ademas del stacktrace en fornato String
	 * 
	 * @param throwable Objecto que contiene excepcion
	 * @return Retorna lista de mensajes de error en String 
	 */
	public static List<String> getDetailedException(Throwable throwable) {
	    List<String> result = new ArrayList<String>();
	    
	    String stacktrace=null;
	    
	    if (throwable!=null){
		    StringWriter errors = new StringWriter();
		    throwable.printStackTrace(new PrintWriter(errors));
		    stacktrace = errors.toString();   
	    }	    
	    
	    while (throwable != null) {
	        result.add(throwable.getMessage());
	        throwable = throwable.getCause();
	    }
	    
	    if (stacktrace!=null) result.add(stacktrace); //add stack trace at the end of list
	    
	    return result;
	}

	
	/**
	 * Función que elimina acentos y caracteres especiales de
	 * una cadena de texto.
	 * @param input
	 * @return cadena de texto limpia de acentos y caracteres especiales.
	 */
	public static String eliminaCaracteresEspeciales(String input) {
	    // Cadena de caracteres original a sustituir.
	    String original = "��������������u�������������������'&";
	    // Cadena de caracteres ASCII que reemplazar�n los originales.
	    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC  ";
	    String output = input;
	    for (int i=0; i<original.length(); i++) {
	        // Reemplazamos los caracteres especiales.
	        output = output.replace(original.charAt(i), ascii.charAt(i));
	    }//for i
	    return output;
	}
	
	public static String getValuePrint(ItemStamp item){
		if(!StringUtils.isEmpty(item.getNbValorVar())){
			return item.getNbValorVar();
		}
		return item.getNbNombreVar();
	}
	
}
