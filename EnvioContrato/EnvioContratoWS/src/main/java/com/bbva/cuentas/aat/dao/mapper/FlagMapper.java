package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Flag;

@Component
public class FlagMapper implements RowMapper<Flag>{

	public static final String CD_FLAG="CD_FLAG";
	public static final String NB_CONDICION_NOMBRE="NB_CONDICION_NOMBRE";
	public static final String NB_CONDICION_VALOR="NB_CONDICION_VALOR";
	public static final String NU_ORDEN="NU_ORDEN";
	public static final String CH_TIPO="CH_TIPO";
	public static final String CD_ENV="CD_ENV";
	
	@Override
	public Flag mapRow(ResultSet rs, int rowNum) throws SQLException {
		Flag flag = new Flag();
		flag.setCdFlag(rs.getLong(CD_FLAG));
		flag.setNbCondicionNombre(rs.getString(NB_CONDICION_NOMBRE));
		flag.setNbCondicionValor(rs.getString(NB_CONDICION_VALOR));
		flag.setNuOrden(rs.getInt(NU_ORDEN));
		flag.setChTipo(rs.getString(CH_TIPO));
		return flag;
	}

}
