package com.bbva.cuentas.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;

@Service
public class KitCoreServiceImpl implements KitCoreService{

	private static final Logger logger = LoggerFactory.getLogger(KitCoreServiceImpl.class);

	@Value("${ruta.formatoPDF}")
	private String rutaFormatoPDF;
	@Value("${ruta.formatoJasper}")
	private String rutaFormatoJasper;
	
	@Autowired
	private EnviromentService envService;
	@Autowired
	private KitService kitService;
	@Autowired
	private PlantillaService plantillaService;
	@Autowired
	private SeccionService seccionService;
	@Autowired
	private ItemService itemService;
	
    public Kit obtenerKitPorGrupo(String idGrupo,String idContrato) throws ServiceException {
		try {
			Enviroment env = envService.obtenerGrupoEnv(idGrupo);
			Kit kit = kitService.obtenerKitPorCodigo(env.getCdEnv(),idContrato);
			if(kit!=null) kit.setGrupo(env);
			return kit;
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_OBTENER_KIT_GRUPO,"No se pudo obtener kit grupo.");
		}
    }
	
	public Kit obtenerKitHistPorGrupo(String idGrupo,String idContrato,Date fechaCreacion) throws ServiceException {
		try {
			Enviroment env = null;
			if(StringUtils.isEmpty(idGrupo)){
				env = envService.obtenerEnvPorDefecto();
			}else{
				Integer cdEnv = new Integer(idGrupo);
				env = envService.obtenerEnvPorCodigo(cdEnv);
			}
			Timestamp fechaCreacionTimestamp = new Timestamp(fechaCreacion.getTime());
			Kit kit = kitService.obtenerKitHistPorCodigo(env.getCdEnv(),idContrato,fechaCreacionTimestamp); 
			kit.setGrupo(env);
			return kit;
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_OBTENER_KIT_GRUPO,"No se pudo obtener kit grupo.");
		}
	}
	
	public void cargarPlantillasContratoCliente(Kit kit,List<Plantilla> plantillas) throws ServiceException{
		List<Plantilla> plantillasGeneral = new ArrayList<Plantilla>();
		List<Plantilla> plantillasCliente = new ArrayList<Plantilla>();
		for(Plantilla plantilla : plantillas){
			
			if(Constants.TIPO_PLANTILLA_GENERICA.equals(plantilla.getChTipoPlantilla())){
				plantillasGeneral.add(plantilla);
			}else{
				plantillasCliente.add(plantilla);
			}
			
			if(Constants.TIPO_CONSTR_JASPER.equals(plantilla.getStTipoConstr())){
				plantilla.setPathTemplate(FilenameUtils.concat(rutaFormatoJasper,
						plantilla.getNbNombreFormato() + ".jrxml"));
			}else if(Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())){
				plantilla.setPathTemplate(FilenameUtils.concat(rutaFormatoJasper,
						plantilla.getNbNombreFormato() + ".jasper"));
			}else{
				plantilla.setPathTemplate(FilenameUtils.concat(rutaFormatoPDF,
						plantilla.getNbNombreFormato() + ".pdf"));
			}
			
			try {
				List<ItemSection> listaSections = obtenerSeccionesPorPlantilla(
					plantilla.getCdPlantilla(),
					kit.getFechaEjecucion());
				plantilla.setSecciones(listaSections);
				plantilla.setFechaRegistro(kit.getFechaEjecucion());
			} catch (Exception e) {
				throw new ServiceException("Error al cargar plantillas.");
			}
		}
		kit.setListTemplates(plantillasGeneral);
		kit.setListTemplatesCliente(plantillasCliente);
	}

	public void cargarPlantillasContratoCliente(Kit kit) throws ServiceException{
		List<Plantilla> plantillas = 
			plantillaService.obtenerPlantillasPorKitActivas(kit.getCdKit(),kit.getFechaEjecucion());
		cargarPlantillasContratoCliente(kit,plantillas);
	}
	
	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla,Date fecHoraCreacion) throws ServiceException {
		List<ItemSection> secciones = seccionService.obtenerSeccionesPorPlantilla(cdPlantilla,fecHoraCreacion);
		if(secciones!=null && !secciones.isEmpty()){
			for(ItemSection seccion: secciones){
				List<ItemStamp> elementos = itemService.obtenerItemsPorSeccion(seccion.getCdSeccion(),fecHoraCreacion);
				seccion.setListItems(elementos);
			}
		}
		return secciones;
	}

	@Override
	public void cargarPlantillasContratoCliente(Kit kit, String nombrePlantillaFormato) throws ServiceException {
		Plantilla plantilla = plantillaService.obtenerPlantillaPorNombre(nombrePlantillaFormato);
		List<Plantilla> plantillas = new ArrayList<Plantilla>();
		plantillas.add(plantilla);
		cargarPlantillasContratoCliente(kit,plantillas);
	}
    
}