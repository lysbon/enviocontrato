//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="firmaContrato" type="{http://bbva.com/cuentas/firmarContratos}reprocesarContrato"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "firmaContrato"
})
@XmlRootElement(name = "reprocesar-contratos-request")
public class ReprocesarContratosRequest {

    @XmlElement(required = true)
    protected ReprocesarContrato firmaContrato;

    /**
     * Obtiene el valor de la propiedad firmaContrato.
     * 
     * @return
     *     possible object is
     *     {@link ReprocesarContrato }
     *     
     */
    public ReprocesarContrato getFirmaContrato() {
        return firmaContrato;
    }

    /**
     * Define el valor de la propiedad firmaContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link ReprocesarContrato }
     *     
     */
    public void setFirmaContrato(ReprocesarContrato value) {
        this.firmaContrato = value;
    }

}
