package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Trama;

@Component
public class TramaMapper implements RowMapper<Trama>{

	//TODO crear clases data
	public static final String CH_MOTI="CH_MOTI";
	public static final String CH_TRAMA="CH_TRAMA";
	public static final String FH_CREACION="FH_CREACION";
	public static final String CD_STEP="CD_STEP";
	public static final String CH_TIPO="CH_TIPO";
	public static final String CH_PROCEDENCIA="CH_PROCEDENCIA";
	public static final String CD_ENV="CD_ENV";
	public static final String CD_CLIENTE="CD_CLIENTE";
	public static final String CD_DOC="CD_DOC";
	public static final String DET_STEP="DET_STEP";
	
	/********04122017**********/
	public static final String ESTADO="ESTADO";
	public static final String NUM_REPROCESO="NUM_REPROCESO";
	public static final String TIME_REPROCESO="TIME_REPROCESO";
	public static final String ID_TRAMA="ID_TRAMA";
	
	/********04122017**********/
	final LobHandler lobHandler = new DefaultLobHandler();
	
	
	@Override
	public Trama mapRow(ResultSet rs, int rowNum) throws SQLException {
		Trama trama = new Trama();
		trama.setCdEnv(rs.getInt(CD_ENV));
		trama.setCdStep(rs.getInt(CD_STEP));
		trama.setChMoti(rs.getString(CH_MOTI));
		trama.setChProcedencia(rs.getString(CH_PROCEDENCIA));
		trama.setChTipo(rs.getString(CH_TIPO));
		trama.setChTrama(lobHandler.getClobAsString(rs, TramaMapper.CH_TRAMA));
		
		Timestamp timestamp=rs.getTimestamp(FH_CREACION);
		
		if (timestamp!=null){
			trama.setFechaCreacion(new java.sql.Date(timestamp.getTime()));
		}
		
		trama.setCdCliente(rs.getString(CD_CLIENTE));
		trama.setCdDoc(rs.getString(CD_DOC));
		trama.setDetStep(rs.getString(DET_STEP));
		
		/********04122017**********/
		trama.setTimeReproceso(rs.getTimestamp(TIME_REPROCESO));
		trama.setEstado(rs.getString(ESTADO));
		trama.setNumReproceso(rs.getInt(NUM_REPROCESO));
		trama.setIdTrama(rs.getLong(ID_TRAMA));
		
		/********04122017**********/
		
		return trama;
	}
}