package com.bbva.cuentas.aat.dao.impl;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.aat.dao.KitPlantillaLegacyDao;
import com.bbva.cuentas.aat.dao.mapper.KitPlantillaLegacyMapper;
import com.bbva.cuentas.bean.KitPlantillaLegacy;

@Component
public class KitPlantillaLegacyDaoImpl extends BaseDaoImpl<KitPlantillaLegacy, String> implements KitPlantillaLegacyDao{
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG001_KIT_PLANTILLA";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	public KitPlantillaLegacyDaoImpl(DataSource datasource, KitPlantillaLegacyMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	
}
