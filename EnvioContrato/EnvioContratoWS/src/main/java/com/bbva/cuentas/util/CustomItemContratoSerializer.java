package com.bbva.cuentas.util;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class CustomItemContratoSerializer extends StdSerializer<ItemContrato>{

	public CustomItemContratoSerializer() {
        this(null);
    }
 
    public CustomItemContratoSerializer(Class<ItemContrato> t) {
        super(t);
    }
    
	@Override
	public void serialize(ItemContrato value, JsonGenerator jgen, SerializerProvider provider)
			throws IOException {
		if(value!=null && 
				!StringUtils.isEmpty(value.getLabel()) &&
				!StringUtils.isEmpty(value.getValue())) {
			jgen.writeStartObject();
			jgen.writeStringField(value.getLabel(), value.getValue());
			jgen.writeEndObject();
		}
	}
	
}
