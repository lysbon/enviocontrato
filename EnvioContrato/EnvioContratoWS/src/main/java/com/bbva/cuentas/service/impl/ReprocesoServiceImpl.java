package com.bbva.cuentas.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.DetTramaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.ClienteReprocesoDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.HuellaDigitalService;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.ReprocesarService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

@Service
public class ReprocesoServiceImpl implements ReprocesarService{

	private static final Logger logger = org.apache.logging.log4j.LogManager
			.getLogger();

	@Autowired
	private TramaService tramaService;
	@Autowired
	private DetTramaService detTramaService;
	@Autowired
	private CoreService coreService;
	@Autowired
	protected KitCoreService kitService;
	@Autowired
	protected ErrorService backupService;
	@Autowired
	protected HuellaDigitalService huellaService;
	@Autowired
	protected Util util;
	
	public boolean tramaFallida(DetTrama detalle) {
		return StringUtils.isEmpty(detalle.getEstado()) || !detalle.getEstado().equals(Constants.ESTADO_PROCESO_EXITOSO);
	}
	
	public void process(List<Trama> listaTramas) throws ServiceException {
		try {
			if(listaTramas!=null) {
				logger.info("[num_tramas_reprocesar]: "+listaTramas.size());
				for(Trama trama : listaTramas) {
					RequestDTO peticion = parse(trama.getChTrama());
					List<DetTrama> detalleCliente = detTramaService.obtenerDetTramasReprocesar(trama.getIdTrama());
					Map<String,List<DetTrama>> clienteTramas = new HashMap<String,List<DetTrama>>();
					for(DetTrama detalle : detalleCliente) {
						if(tramaFallida(detalle)) {
							if(!StringUtils.isEmpty(detalle.getCdCliente())) {
								if(clienteTramas.containsKey(detalle.getCdCliente())) {
									clienteTramas.get(detalle.getCdCliente()).add(detalle);
								}else {
									List<DetTrama> pasosCliente = new ArrayList<DetTrama>();
									pasosCliente.add(detalle);
									clienteTramas.put(detalle.getCdCliente(),pasosCliente);
								}
							}else {
								List<Cliente> clientesTrama = peticion.getListaClientes();
								for(Cliente cliente : clientesTrama) {
									List<DetTrama> pasosCliente = new ArrayList<DetTrama>();
									DetTrama detTrama = new DetTrama();
									detTrama.setCdStep(ProcesoEnvioContrato.GENERICO.getPaso());
									clienteTramas.put(cliente.getCodigoCentral(),pasosCliente);
								}
							}
						}
					}
					reprocess(peticion,clienteTramas,trama.getTimeReproceso());
				}
			}
		}catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion. " + e.getMessage());
		}
	}
	
	public void reprocess(RequestDTO peticion,Map<String,List<DetTrama>> clientes,Date fechaProceso) throws ServiceException{
		if(clientes!=null && clientes.size()>0) {
			Map<String,ClienteReprocesoDTO> clientesReproceso = new HashMap<String,ClienteReprocesoDTO>();
			for (Map.Entry<String, List<DetTrama>> cliente : clientes.entrySet()){
				ClienteReprocesoDTO clienteDTO = new ClienteReprocesoDTO();
				List<DetTrama> pasos = cliente.getValue();
				clienteDTO.setCodCliente(cliente.getKey());
				clienteDTO.setProcesarFileUnico(false);
				clienteDTO.setProcesarMail(false);
				clienteDTO.setDetalleTrama(pasos);
				for(DetTrama paso : pasos) {
					if(paso.getCdStep().intValue() == ProcesoEnvioContrato.TRAMA_INICIAL.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.VALIDACION_TRAMA.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.OBTENER_HUELLA_DIGITAL.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_PDFS_GENERAL.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_PDFS_CLIENTE.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_CLIENTE.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_RUTAS.getPaso() ||
					   paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERICO.getPaso()) {
						clienteDTO.setProcesarFileUnico(true);
						clienteDTO.setProcesarMail(true);
					}else if(paso.getCdStep().intValue() == ProcesoEnvioContrato.ENVIO_EMAIL_CLIENTE.getPaso()){
						clienteDTO.setProcesarMail(true);
					}else if(paso.getCdStep().intValue() == ProcesoEnvioContrato.ENVIO_FILEUNICO_CLIENTE.getPaso()){
						clienteDTO.setProcesarFileUnico(true);
					}
				}
				clientesReproceso.put(clienteDTO.getCodCliente(),clienteDTO);
			}
			Kit kit = kitService.obtenerKitPorGrupo(peticion.getIdGrupo(),peticion.getIdContrato());
			kit.setFechaEjecucion(fechaProceso);
			process(peticion,kit,clientesReproceso);
		}
	}
	
	public RequestDTO parse(String trama) throws ServiceException{
		return backupService.getRequestJson(trama);
	}
	
	public void process(RequestDTO request, Kit kit,Map<String,ClienteReprocesoDTO> reproceso) throws ServiceException {
		List<ClienteDTO> plantillasCliente = null;
		Long idTrama = null;
		try {
			if (kit != null && request.getListaClientes() != null) {
				Map<String, Object> parametros = util.toMap(request);
				if (coreService.huellaActivo(kit))
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes()));
				plantillasCliente = coreService.processGenerarContratos(request, kit, parametros);
				for (int i = 0; i < plantillasCliente.size(); i++) {
					ClienteDTO cliente = plantillasCliente.get(i);
					ClienteReprocesoDTO reprocesoCliente = reproceso.get(cliente.getCliente().getCodigoCentral());
					Map<String, Object> parametrosCliente = util.toMap(request, cliente.getCliente());
					if(reprocesoCliente.isProcesarMail()) {
						coreService.processMail(request, kit, cliente, parametrosCliente);
					}
					if(reprocesoCliente.isProcesarFileUnico()) {
						coreService.processFileUnico(request, kit, cliente);
					}
					for(DetTrama tramaCliente : reprocesoCliente.getDetalleTrama()) {
						tramaCliente.setEstado(Constants.ESTADO_PROCESO_EXITOSO);
						tramaCliente.setFechaRegistro(new Date());
						idTrama = tramaCliente.getIdTrama();
						detTramaService.actualizarTrama(tramaCliente);
					}
				}
				Trama trama = tramaService.obtenerTramasPorId(idTrama);
				trama.setEstado(Constants.ESTADO_PROCESO_EXITOSO);
				if(trama.getNumReproceso()==null) {
					trama.setNumReproceso(1);
				}else {
					trama.setNumReproceso(trama.getNumReproceso()+1);
				}
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				trama.setTimeReproceso(timestamp);
				tramaService.actualizarTrama(trama);
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			coreService.depurarArchivos(plantillasCliente);
		}
	}
	
	
	

	@Override
	public void process() throws ServiceException {
		List<Trama> listaTramas = tramaService.obtenerTramasReprocesar(Constants.ESTADO_PROCESO_SELECCIONADO);
		process(listaTramas);
	}
	
	@Override
	public void process(Long from, Long to) throws ServiceException {
		List<Trama> listaTramas = new ArrayList<Trama>();
		for(Long i=from;0<to;i++) {
			Trama trama = tramaService.obtenerTramasPorId(i);
			if(Constants.ESTADO_PROCESO_ERROR.equals(trama.getEstado())) {
				trama.setEstado(Constants.ESTADO_PROCESO_SELECCIONADO);
				tramaService.actualizarTrama(trama);
			}
			listaTramas.add(trama);
		}
		process(listaTramas);
	}
	
}
