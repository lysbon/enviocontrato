package com.bbva.cuentas.service;

import java.util.List;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.gesdoc.basics.wsdl.PropiedadVO;
import com.bbva.gesdoc.basics.wsdl.RespuestaAltaDocumento;

public interface GestDocWSService {
	
	public RespuestaAltaDocumento altaDocumento(
			String nombreAdjunto,
			String rutaAdjunto,
			List<PropiedadVO> propiedades) throws ServiceException;
	
}
