package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.FlagDao;
import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.Flag;

@Service
public class FlagServiceImpl implements FlagService{

	private static final Logger logger = LoggerFactory.getLogger(FlagServiceImpl.class);
	
	@Autowired
	private FlagDao flagDao;

	@Override
	public Flag obtenerFlagPorCodigo(Long id) throws ServiceException {
		try {
			return flagDao.findById(id);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FLG_OBTENER_FLAG,"No se pudo obtener flag.");
		}
	}

	@Override
	public List<Flag> obtenerFlagsPorTipo(String tipo) throws ServiceException {
		try {
			return flagDao.obtenerFlagsPorTipo(tipo);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FLG_OBTENER_FLAG_POR_TIPO,"No se pudo obtener flags por tipo.");
		}
	}
	
	@Override
	public List<Flag> obtenerFlags() throws ServiceException {
		try {
			return flagDao.obtenerFlags();
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FLG_OBTENER_FLAG,"No se pudo obtener flags.");
		}
	}

	@Override
	public Long guardarFlag(Flag flag) throws ServiceException {
		try {
			return flagDao.insertAndReturnId(flag);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
	}

	@Override
	public Integer actualizarFlag(Flag flag) throws ServiceException {
		try {
			return flagDao.update(flag);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
	}

	@Override
	public Integer eliminarFlag(Long id) throws ServiceException {
		try {
			return flagDao.delete(id);
		} catch (DaoException e) {
			throw new ServiceException(e.getMessage());
		}
	}
		
}
