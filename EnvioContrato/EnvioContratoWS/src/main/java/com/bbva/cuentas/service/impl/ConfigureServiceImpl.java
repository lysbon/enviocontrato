package com.bbva.cuentas.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.TramaDao;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.firmarcontratos.Configuration;
import com.bbva.cuentas.firmarcontratos.ConfigurationItem;
import com.bbva.cuentas.service.ConfigureService;
import com.bbva.cuentas.service.loader.ImageLoader;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ConfigureServiceImpl implements ConfigureService {
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final String ERROR_MESSAGE = "Error en servicio de configuracion.";
	
	@Autowired
	private ImageLoader         imageLoader;
	@Autowired
	private TramaDao            tramaDao;
	@Autowired
	private KitService          kitService;
	@Autowired
	private PlantillaService    plantillaService;
	
    public String process(Configuration configuration) throws ServiceException {
        try {
        	List<ConfigurationItem> lst = configuration.getItem();
        	StringBuilder sb = new StringBuilder("");
        	if(lst!=null){
        		for(ConfigurationItem cf : lst){
        			sb.append(procesarItem(cf,configuration));
        		}
        	}
        	return sb.toString();
        } catch (ServiceException e) {
            logger.error(ERROR_MESSAGE,e);
            throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
        } 
    }
    
    public void configurarHtml(ConfigurationItem configuration,Configuration parent) throws ServiceException {
    	try {
    		String strCdEnv = null;
    		if(parent!=null) {
    			for(ConfigurationItem cf : parent.getItem()){
        			if(Constants.LLAVE_NAME.equals(cf.getTipo())){
        				strCdEnv = cf.getLlave();
        			}
        		}
    		}
    		List<Kit> kits = kitService.obtenerKitsPorCodigo(configuration.getLlave());
    		if(kits!=null) {
    			for(Kit kit : kits) {
    				if(StringUtils.isEmpty(strCdEnv)) {
    					kitService.actualizarEmailTemplate(kit.getCdKit(),configuration.getRuta());
    				}else {
    					Integer cdEnv = Integer.parseInt(strCdEnv);
    					kitService.actualizarEmailTemplate(kit.getCdKit(),cdEnv,configuration.getRuta());
    				}
    			}
    		}
    		
		} catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public void configurarPdf(ConfigurationItem configuration) throws ServiceException {
    	try {
    		plantillaService.actualizarPDFdesdeArchivo(new Long(configuration.getLlave()), configuration.getRuta());
    	}catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public void configurarImage(ConfigurationItem configuration) throws ServiceException {
    	try {
    		imageLoader.writeBLOBPut(new Integer(configuration.getLlave()), configuration.getRuta());
    	}catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public String configurarQuery(ConfigurationItem configuration) throws ServiceException {
    	try {
    		ObjectMapper mapper = new ObjectMapper();
    		List<Map<String, Object>> result = tramaDao.query(configuration.getLlave());
    		return mapper.writeValueAsString(result);
    	}catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public String configurarExecute(ConfigurationItem configuration) throws ServiceException {
    	try {
    		ObjectMapper mapper = new ObjectMapper();
    		Map<String, Object> result = tramaDao.execute(configuration.getLlave());
    		return mapper.writeValueAsString(result);
    	}catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public String configurarBase64Put(ConfigurationItem configuration) throws ServiceException {
    	ObjectMapper mapper = new ObjectMapper();
		String encodedBase64 = configuration.getLlave();
		File f = new File(configuration.getRuta());
		OutputStream out =null;
		try {
			if (f.exists() && !f.isDirectory()) {
				FileUtils.forceDelete(f);
			}
			out = new FileOutputStream(configuration.getRuta());
			out.write(Base64.decodeBase64(encodedBase64));
			out.close();
			return mapper.writeValueAsString(configuration.getRuta());
		}catch(Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}finally {
			try {if (out!=null) out.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
		}
    }
    
    public String configurarBase64Get(ConfigurationItem configuration) throws ServiceException {
    	ObjectMapper mapper = new ObjectMapper();
		String encodedBase64 = null;
		try {
			encodedBase64 = Util.fileToBase64(configuration.getRuta());
			return mapper.writeValueAsString(encodedBase64);
		} catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		} 
    }
    
    public List<File> configurarLsEmpty(ConfigurationItem configuration) {
    	File dir = new File(configuration.getRuta());
    	return (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
    }
    
    public List<File> configurarLsExtension(ConfigurationItem configuration,Configuration parent) {
    	File dir = new File(configuration.getRuta());
    	List<String> extensiones = new ArrayList<String>();
		for(ConfigurationItem cf : parent.getItem()){
			if(Constants.LLAVE_EXT.equals(cf.getTipo())){
				extensiones.add(cf.getLlave());
			}
		}
		String[] extensions = new String[extensiones.size()];
		extensions = extensiones.toArray(extensions);
		return (List<File>) FileUtils.listFiles(dir, extensions, true);
    }
    
    public List<File> configurarLsName(ConfigurationItem configuration,Configuration parent) {
    	File dir = new File(configuration.getRuta());
    	String name = null;
		for(ConfigurationItem cf : parent.getItem()){
			if(Constants.LLAVE_NAME.equals(cf.getTipo())){
				name = cf.getLlave();
			}
		}
		IOFileFilter fileFilter = FileFilterUtils.nameFileFilter(name,IOCase.INSENSITIVE);
        return (List<File>) FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
    }

    public List<File> configurarLsWilcard(ConfigurationItem configuration,Configuration parent) {
    	File dir = new File(configuration.getRuta());
    	String name = null;
		for(ConfigurationItem cf : parent.getItem()){
			if(Constants.LLAVE_NAME.equals(cf.getTipo())){
				name = cf.getLlave();
			}
		}
		IOFileFilter fileFilter = new WildcardFileFilter(name);
        return (List<File>) FileUtils.listFiles(dir, fileFilter, TrueFileFilter.INSTANCE);
    }
    
    public String configurarLs(ConfigurationItem configuration,Configuration parent) throws ServiceException {
    	ObjectMapper mapper = new ObjectMapper();
		List<File> files = null;
    	try {
    		if(StringUtils.isEmpty(configuration.getLlave())){
    			files = configurarLsEmpty(configuration);
    		}else if(Constants.LLAVE_EXT.equals(configuration.getLlave())){
    			files = configurarLsExtension(configuration, parent);
    		}else if(Constants.LLAVE_NAME.equals(configuration.getLlave())){
    			files = configurarLsName(configuration, parent);
    		}else if(Constants.LLAVE_WILDCARD.equals(configuration.getLlave())){
    			files = configurarLsWilcard(configuration, parent);
    		}
    		return mapper.writeValueAsString(files);
		} catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    
    
    public String configurarMakeDir(ConfigurationItem configuration) throws ServiceException {
    	try {
    		ObjectMapper mapper = new ObjectMapper();
    		File file = new File(configuration.getRuta());
    		FileUtils.forceMkdir(file);
    		return mapper.writeValueAsString(configuration);
		} catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
    
    public String configurarDelete(ConfigurationItem configuration) throws ServiceException {
    	try {
    		ObjectMapper mapper = new ObjectMapper();
    		FileUtils.forceDelete(new File(configuration.getRuta()));
    		return mapper.writeValueAsString(configuration);
		} catch (Exception e) {
			logger.error(ERROR_MESSAGE,e);
			throw new ServiceException(ErrorCodes.S_CONFIGURE_SERVICE,e.getMessage());
		}
    }
        
    public String procesarItem(ConfigurationItem configuration,Configuration parent) throws ServiceException{
    	if(Constants.TIPO_HTML.equals(configuration.getTipo())){
    		configurarHtml(configuration,parent);
    	}else if(Constants.TIPO_PDF.equals(configuration.getTipo())){
    		configurarPdf(configuration);
    	}else if(Constants.TIPO_IMAGE.equals(configuration.getTipo())){
    		configurarImage(configuration);
    	}else if(Constants.TIPO_QUERY.equals(configuration.getTipo())){
    		return configurarQuery(configuration);
    	}else if(Constants.TIPO_EXECUTE.equals(configuration.getTipo())){
    		return configurarExecute(configuration);
    	}else if(Constants.TIPO_BASE64_PUT.equals(configuration.getTipo())){
    		return configurarBase64Put(configuration);
    	}else if(Constants.TIPO_BASE64_GET.equals(configuration.getTipo())){
    		return configurarBase64Get(configuration);
    	}else if(Constants.TIPO_LS.equals(configuration.getTipo())){
    		return configurarLs(configuration, parent);
    	}else if(Constants.TIPO_MKDIR.equals(configuration.getTipo())){
    		return configurarMakeDir(configuration);
    	}else if(Constants.TIPO_DEL.equals(configuration.getTipo())){
    		return configurarDelete(configuration);
    	}
    	return "OK";
    }
        

}