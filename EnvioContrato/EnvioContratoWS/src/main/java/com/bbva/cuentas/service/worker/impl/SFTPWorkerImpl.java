package com.bbva.cuentas.service.worker.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ArchivoFileUnico;
import com.bbva.cuentas.service.worker.SFTPWorker;

@Component
public class SFTPWorkerImpl implements SFTPWorker{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	public void enviarArchivos(List<ArchivoFileUnico> archivos, String prefijo, String ext) throws Exception{
		throw new NotImplementedException("EnviarArchivos no implementado");
	}
	
	public void moverArchivos(List<ArchivoFileUnico> archivos, String rutaFileunico) throws Exception {
        try {
        	if(archivos!=null){
            	for(ArchivoFileUnico archivo : archivos){
            		File file = new File(archivo.getRuta());
	            	File dirDest = new File(rutaFileunico);
	            	FileUtils.moveFileToDirectory(file,dirDest,false);
            	}
            }
        } catch (Exception e) {
        	logger.error("Error al mover archivos.",e);
        	throw e;
        }
    }
	
	public void moverArchivo(String rutaOrigenFileunico, String rutaDestinoFileunico) throws Exception {
        try {
        	File file = new File(rutaOrigenFileunico);
        	File dirDest = new File(rutaDestinoFileunico);
        	FileUtils.moveFileToDirectory(file,dirDest,false);
        } catch (Exception e) {
        	logger.error("Error al mover archivos.",e);
        	throw e;
        }
    }
	
	public void copiarArchivos(List<ArchivoFileUnico> archivos, String rutaFileunico) throws Exception {
        try {
            if(archivos!=null){
            	for(ArchivoFileUnico archivo : archivos){
	            	File file = new File(archivo.getRuta());
	            	File dirDest = new File(rutaFileunico);
	            	FileUtils.copyFileToDirectory(file,dirDest,false);
            	}
            }
        } catch (Exception e) {
        	logger.error("Error al copiar archivos simple.",e);
        	throw e;
        }
    }
	
	public void copiarArchivos(List<ArchivoFileUnico> archivos, 
			String rutaFileunico, String prefijo, String ext) throws Exception {
        try {
            if(archivos!=null){
            	for(ArchivoFileUnico archivo : archivos){
            		File file = new File(archivo.getRuta());
	            	File fileDest = new File(FilenameUtils.concat(rutaFileunico, prefijo+archivo.getNombreBase()+ext));
	            	FileUtils.copyFile(file, fileDest);
            	}
            }
        } catch (Exception e) {
        	logger.error("Error al copiar archivos.",e);
        	throw e;
        }
    }
	
	
	public void copiarArchivo(String rutaOrigen, 
			String rutaFileunico) throws Exception {
        try {
            
            		File file = new File(rutaOrigen);
	            	File fileDest = new File(rutaFileunico);
	            	FileUtils.copyFile(file, fileDest);
            
        } catch (Exception e) {
        	logger.error("Error al copiar archivos.",e);
        	throw e;
        }
    }
	
	public void eliminarArchivos(List<ArchivoFileUnico> archivos) throws Exception {
        try {
        	if(archivos!=null){
            	for(ArchivoFileUnico archivo : archivos){
            		File file = new File(archivo.getRuta());
	            	FileUtils.deleteQuietly(file);
            	}
            }
        } catch (Exception e) {
        	logger.error("Error al eliminar archivos.",e);
        	throw e;
        }
    }
	
	public String generarRutaSFTP(String filename, String rutaSFTP,String rutaOrigen) {
		return filename.replace(rutaOrigen, rutaSFTP);
	}
	
}
