package com.bbva.cuentas.aat.dao;

import com.bbva.cuentas.bean.EnvKit;

public interface EnvKitDao extends BaseDao<EnvKit, Long>{
	
	public Integer delete(EnvKit envKit) throws DaoException;
	
	public Integer deletePorKit(Long cdKit) throws DaoException;
	
	public Integer actualizarPlantillaDesdeCadena(final Long cdKit, final Integer cdEnv, String cadena) throws DaoException;

	public Integer actualizarPlantillaDesdeArchivo(final Long cdKit, final Integer cdEnv, String archivo) throws DaoException;
	
	public String obtenerMailTemplate(Long cdKit, Integer cdEnv) throws DaoException;

}
