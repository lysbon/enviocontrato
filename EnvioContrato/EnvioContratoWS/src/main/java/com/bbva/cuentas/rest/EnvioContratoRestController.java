package com.bbva.cuentas.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.PivoteService;
import com.bbva.cuentas.service.ReprocesarService;
import com.bbva.cuentas.service.exception.ParametrosInvalidosException;
import com.bbva.cuentas.service.exception.PlantillaNoEncuentraException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;

@Controller
public class EnvioContratoRestController {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private PivoteService pivoteService;
	@Autowired
	private ReprocesarService reprocesoService;
	
	@RequestMapping(value = "/envioContrato", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> envioContrato(@RequestBody RequestDTO request) {
		logger.info("Procesando envio de contrato");
		try {
			pivoteService.process(request);
		} catch (Exception e) {
			Map<String,String> mapError = new HashMap<String,String>();	
			mapError.put(Result.ERROR_CODE, "500");
			mapError.put(Result.ERROR_MESSAGE,"Error al enviar contrato");
			return new ResponseEntity<Map<String,String>>(mapError,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/generarDocumentos", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Documentos> generarContratos(@RequestBody RequestDTO request) {
		Documentos documentos = null;
		try {
			documentos = pivoteService.processKitBase64(request);
		} catch (PlantillaNoEncuentraException e) {
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		} catch (ParametrosInvalidosException e) {
			return new ResponseEntity<Documentos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Documentos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(documentos == null){
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Documentos>(documentos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/generarDocumento", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Documentos> generarContrato(@RequestBody RequestDTO request) {
		Documentos documentos = null;
		try {
			documentos = pivoteService.processPlantillaBase64(request);
		} catch (PlantillaNoEncuentraException e) {
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		} catch (ParametrosInvalidosException e) {
			return new ResponseEntity<Documentos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Documentos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(documentos == null){
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Documentos>(documentos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reprocesar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> reprocesar() {
		logger.info("REPROCESANDO envio de contratos");
		try {
			reprocesoService.process();
		} catch (Exception e) {
			Map<String,String> mapError = new HashMap<String,String>();	
			mapError.put(Result.ERROR_CODE, "500");
			mapError.put(Result.ERROR_MESSAGE,"Error al reprocesar contratos");
			return new ResponseEntity<Map<String,String>>(mapError,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	@RequestMapping(value = "/reprocesar/{from}/{to}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> reprocesar(@PathVariable("from") Long from,@PathVariable("to") Long to) {
		logger.info("REPROCESANDO envio de contratos");
		try {
			reprocesoService.process(from,to);
		} catch (Exception e) {
			Map<String,String> mapError = new HashMap<String,String>();	
			mapError.put(Result.ERROR_CODE, "500");
			mapError.put(Result.ERROR_MESSAGE,"Error al reprocesar contratos");
			return new ResponseEntity<Map<String,String>>(mapError,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
}
