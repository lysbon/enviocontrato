package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.service.exception.ServiceException;

public interface FlagPlantillaService {
	
	public List<FlagPlantilla> obtenerFlagsPorPlantilla(Long id) throws ServiceException;
	
	public List<FlagPlantilla> obtenerFlagPlantillas() throws ServiceException;
	
	public FlagPlantilla obtenerFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException;
	
	public Integer guardarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException;
	
	public Integer actualizarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException;
	
	public Integer eliminarFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException;
	
}
