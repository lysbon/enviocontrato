package com.bbva.cuentas.service.impl;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.AsyncService;
import com.bbva.cuentas.service.CoreService;

@Service
public class AsyncServiceImpl implements AsyncService{
	
	@SuppressWarnings("unused")
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private CoreService coreService;
	
    @Async
    public void processAsync(RequestDTO request,Kit kit) throws ServiceException {
    	coreService.process(request, kit);
    }

}