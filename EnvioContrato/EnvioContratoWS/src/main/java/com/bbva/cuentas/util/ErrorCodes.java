package com.bbva.cuentas.util;

public class ErrorCodes {
	
	private ErrorCodes() {
	    throw new IllegalStateException("Utility class");
	}
	
	//Data errors
	public static final String D_INSERT_ERROR = "D-100";
    public static final String D_UPDATE_ERROR = "D-101";
    public static final String D_DELETE_ERROR = "D-102";
    public static final String D_PK_NOT_DEFINED = "D-103";    
    public static final String D_QUERY_ERROR = "D-104";
    public static final String D_SQL_ERROR = "D-199";
    
    //Service errors
    public static final String S_DB_ERROR = "S-100";
	public static final String S_OTHER_ERROR = "S-101";
	public static final String S_ERROR_OBTENER_MENSAJE = "S-102";
	public static final String S_ERROR_OBTENER_PARAMETRO = "S-103";
	public static final String S_ERROR_OBTENER_MESTRO = "S-104";
	public static final String S_ERROR_MAIL = "S-105";
	
	public static final String S_ERROR_KIT_OBTENER_KITS          = "S-140";
	public static final String S_ERROR_KIT_OBTENER_KIT           = "S-141";
	public static final String S_ERROR_KIT_GUARDAR_KIT           = "S-142";
	public static final String S_ERROR_KIT_ACTUALIZAR_KIT        = "S-143";
	public static final String S_ERROR_KIT_ELIMINAR_KIT          = "S-144";
	public static final String S_ERROR_KIT_OBTENER_MAIL_TEMPLATE = "S-145";
	public static final String S_ERROR_KIT_ACTUALIZAR_KIT_EMAIL  = "S-146";
	
	public static final String S_ERROR_KIT_PLANTILLA_OBTENER_KPS          = "S-130";
	public static final String S_ERROR_KIT_PLANTILLA_OBTENER_KP           = "S-131";
	public static final String S_ERROR_KIT_PLANTILLA_GUARDAR_KP           = "S-132";
	public static final String S_ERROR_KIT_PLANTILLA_ACTUALIZAR_KP        = "S-133";
	public static final String S_ERROR_KIT_PLANTILLA_ELIMINAR_KP          = "S-134";
	
	public static final String S_ERROR_ENV_KIT_GUARDAR_KP           = "S-160";
	public static final String S_ERROR_ENV_KIT_ACTUALIZAR_KP        = "S-161";
	public static final String S_ERROR_ENV_KIT_ELIMINAR_KP          = "S-162";
	
	public static final String S_ERROR_PLT_OBTENER_PLANTILLAS    = "S-150";
	public static final String S_ERROR_PLT_OBTENER_PLANTILLA     = "S-151";
	public static final String S_ERROR_PLT_OBTENER_PDF           = "S-152";
	public static final String S_ERROR_PLT_GUARDAR_PLANTILLA     = "S-153";
	public static final String S_ERROR_PLT_ACTUALIZAR_PLANTILLA  = "S-154";
	public static final String S_ERROR_PLT_ELIMINAR_PLANTILLA    = "S-155";
	public static final String S_ERROR_PLT_ACTUALIZAR_PDF        = "S-156";
	
	public static final String S_ERROR_SEC_OBTENER_POR_PLANTILLA = "S-160";
	public static final String S_ERROR_SEC_OBTENER_SECCIONES     = "S-161";
	public static final String S_ERROR_SEC_OBTENER_SECCION       = "S-162";
	public static final String S_ERROR_SEC_GUARDAR_SECCION       = "S-163";
	public static final String S_ERROR_SEC_ACTUALIZAR_SECCION    = "S-164";
	public static final String S_ERROR_SEC_ELIMINAR_SECCION      = "S-165";
	public static final String S_ERROR_SEC_GENERAR_SVG           = "S-166";
	public static final String S_ERROR_SEC_PROCESAR_SVG          = "S-167";
	public static final String S_ERROR_SEC_GENERAR_SCRIPT        = "S-168";
	public static final String S_ERROR_SEC_APLICAR_CAMBIOS       = "S-169";
	
	public static final String S_ERROR_ITM_OBTENER_POR_SECCION   = "S-170";
	public static final String S_ERROR_ITM_OBTENER_ITEMS         = "S-171";
	public static final String S_ERROR_ITM_OBTENER_ITEM          = "S-172";
	public static final String S_ERROR_ITM_GUARDAR_ITEM          = "S-173";
	public static final String S_ERROR_ITM_ACTUALIZAR_ITEM       = "S-174";
	public static final String S_ERROR_ITM_ELIMINAR_ITEM         = "S-175";
	
	public static final String S_ERROR_FPT_OBTENER_FLAG_PLANTILLAS    = "S-180";
	public static final String S_ERROR_FPT_OBTENER_FLAG_PLANTILLA     = "S-181";
	public static final String S_ERROR_FPT_OBTENER_FLAG_POR_PLANTILLA = "S-182";
	public static final String S_ERROR_FPT_GUARDAR_FLAG_PLANTILLA     = "S-183";
	public static final String S_ERROR_FPT_ACTUALIZAR_FLAG_PLANTILLA  = "S-184";
	public static final String S_ERROR_FPT_ELIMINAR_FLAG_PLANTILLA    = "S-185";

	public static final String S_ERROR_FLG_OBTENER_FLAGS            = "S-190";
	public static final String S_ERROR_FLG_OBTENER_FLAG             = "S-191";
	public static final String S_ERROR_FLG_OBTENER_FLAG_POR_TIPO    = "S-192";
	public static final String S_ERROR_FLG_GUARDAR_FLAG             = "S-193";
	public static final String S_ERROR_FLG_ACTUALIZAR_FLAG          = "S-194";
	public static final String S_ERROR_FLG_ELIMINAR_FLAG            = "S-195";
	
	public static final String S_ERROR_RES_OBTENER_RECURSOS            = "S-200";
	public static final String S_ERROR_RES_OBTENER_RECURSO             = "S-201";
	public static final String S_ERROR_RES_OBTENER_RECURSO_POR_TIPO    = "S-202";
	public static final String S_ERROR_RES_GUARDAR_RECURSO             = "S-203";
	public static final String S_ERROR_RES_ACTUALIZAR_RECURSO          = "S-204";
	public static final String S_ERROR_RES_ELIMINAR_RECURSO            = "S-205";
	
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENTS            = "S-210";
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENT             = "S-211";
	public static final String S_ERROR_ENV_OBTENER_ENVIROMENT_POR_TIPO    = "S-212";
	public static final String S_ERROR_ENV_GUARDAR_ENVIROMENT             = "S-213";
	public static final String S_ERROR_ENV_ACTUALIZAR_ENVIROMENT          = "S-214";
	public static final String S_ERROR_ENV_ELIMINAR_ENVIROMENT            = "S-215";
	
	public static final String S_ERROR_TRM_LIMPIAR_TRAMA            	= "S-220";
	public static final String S_ERROR_TRM_GUARDAR_TRAMA            	= "S-221";
	
	public static final String W_ERROR_WORKER_PDF            			= "W-100";
	
	
	public static final String S_ERROR_INIT            					= "S-230";
	public static final String S_ERROR_INIT_ENV        					= "S-231";
	
	public static final String S_ERROR_GENERAR_CONTRATOS				= "S-240";
	public static final String S_ERROR_GENERAR_CONTRATO					= "S-241";
	public static final String S_ERROR_REPROCESAR_CONTRATO				= "S-242";
	public static final String S_ERROR_OBTENER_KIT_GRUPO 				= "S-243";
	
	public static final String S_ERROR_MAIL_GENERAL 					= "S-250";
	public static final String S_ERROR_MAIL_KIT		 					= "S-251";
	
	public static final String S_ERROR_SIGNBOX_GENERAL 					= "S-260";
	public static final String S_ERROR_SIGNBOX_CLIENTE 					= "S-261";
	
	public static final String S_ERROR_FILEUNICO_GENERAL				= "S-270";
	public static final String S_ERROR_FILEUNICO_GENERAR_META			= "S-271";
	
	public static final String S_ERROR_PIVOT_GENERAL					= "S-280";
	
	//View Validation errors
	public static final String V_ERROR_INTERNO="V-100";
	
	
	public static final String S_ERROR_HUELLA 							= "S-290";
	
	public static final String S_CONFIGURE_SERVICE	 					= "S-301";
	
}
