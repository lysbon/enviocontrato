package com.bbva.cuentas.service.loader;

import java.io.IOException;
import java.sql.SQLException;

public interface HtmlTemplateLoader{

	public void writeCLOBPut(final String cdKit, String inputTextFileName) 
			throws IOException, SQLException;
	
}
