package com.bbva.cuentas.service;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.firmarcontratos.Configuration;

public interface ConfigureService {
	
    public String process(Configuration configuration) throws ServiceException;

}