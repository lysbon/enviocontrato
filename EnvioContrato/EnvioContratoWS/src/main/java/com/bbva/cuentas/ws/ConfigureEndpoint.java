package com.bbva.cuentas.ws;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bbva.cuentas.firmarcontratos.ConfigureRequest;
import com.bbva.cuentas.firmarcontratos.ConfigureResponse;
import com.bbva.cuentas.service.ConfigureService;
import com.bbva.cuentas.util.Constants;

@Endpoint
public class ConfigureEndpoint implements MarshallingConfigureService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	private ConfigureService confService;
	
	@Autowired
	public ConfigureEndpoint(ConfigureService confServiceParam) {
		confService = confServiceParam;
	}
	
	@PayloadRoot(namespace = NAMESPACE, localPart = CONFIGURE_REQUEST)
	@ResponsePayload
	public ConfigureResponse configure(@RequestPayload ConfigureRequest request) {
		ConfigureResponse response = new ConfigureResponse();
		try {			
			logger.info("[Ini] Servicio configuracion");
			String result = confService.process(request.getConfiguration());
			response.setMensajeResultado(result);
			response.setCodigoResultado(Constants.RESULT_OK);
			logger.info("[Fin] Servicio configuracion");			
		} catch (Exception e) {
			logger.error("Error al invocar configuracion. ",e.getMessage());
			response.setMensajeResultado(e.getMessage());
			response.setCodigoResultado(Constants.RESULT_KO);
		}		
		return response;
	}
	
}
