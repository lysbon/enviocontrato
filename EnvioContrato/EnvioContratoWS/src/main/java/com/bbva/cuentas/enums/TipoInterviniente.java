package com.bbva.cuentas.enums;

public enum TipoInterviniente {
	TITULAR("TIT"),
    CONYUGE("CNY"),
    AVALISTA("AVA"),
    PARTICIPE("PAR"),
    CONYUGEAVAL("CAV"),
    REPRESENTANTELEGAL("REP"),
    DIRECTOR("DIR"),
    ACCIONISTA("ACC");
	private String codigo;
	TipoInterviniente(String codigo) {
        this.codigo = codigo;
    }
    public String value() {
        return codigo;
    }
}