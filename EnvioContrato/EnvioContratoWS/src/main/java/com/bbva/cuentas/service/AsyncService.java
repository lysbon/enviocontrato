package com.bbva.cuentas.service;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.RequestDTO;

public interface AsyncService {
	
	public void processAsync(RequestDTO request,Kit kit) throws ServiceException;

}