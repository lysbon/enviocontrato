package com.bbva.cuentas.service.impl;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.dto.DocumentoCliente;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.FileUnicoService;
import com.bbva.cuentas.service.GenerarContratosService;
import com.bbva.cuentas.service.HuellaDigitalService;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.MailService;
import com.bbva.cuentas.service.SignContractService;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Util;

@Service
public class CoreServiceImpl implements CoreService {

	protected static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Autowired
	protected GenerarContratosService generarContratosService;
	@Autowired
	protected MailService mailService;
	@Autowired
	protected SignContractService signService;
	@Autowired
	protected FileUnicoService fileUnicoService;
	@Autowired
	protected ErrorService backupService;
	@Autowired
	protected Util util;
	@Autowired
	protected HuellaDigitalService huellaService;
	@Autowired
	protected KitCoreService kitService;

	@Autowired
	private SimpleDateFormat formatYYYYHHmm;

	@Value("${backup.tramas}")
	private boolean flagBackupTrama;

	public boolean huellaActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getGrupo().getStHuella())
				&& Constants.ST_ACTIVO.equals(kit.getStHuella());
	}

	public boolean signBoxActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getStSignBox());
	}

	public boolean fileunicoActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getStFileunico());
	}

	public boolean correoActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getGrupoKit().getStEnvCorreo());
	}

	public void process(RequestDTO request, Kit kit) throws ServiceException {

		List<ClienteDTO> plantillasCliente = null;

		try {
			backupTrama(request, kit);

			if (kit != null && request.getListaClientes() != null) {

				Map<String, Object> parametros = util.toMap(request);

				if (huellaActivo(kit))
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes()));

				plantillasCliente = processGenerarContratos(request, kit, parametros);

				for (int i = 0; i < plantillasCliente.size(); i++) {
					ClienteDTO cliente = plantillasCliente.get(i);
					Map<String, Object> parametrosCliente = util.toMap(request, cliente.getCliente());
					processMail(request, kit, cliente, parametrosCliente);
					processFileUnico(request, kit, cliente);
				}
			}
			logger.info("[fin]");
		} catch (ServiceException e) {
			logger.error("Error en general de proceso. ", e);
			guardarTramaNivelGeneral(request, kit, null, e);
		} catch (Exception e) {
			logger.error(e);
			guardarTramaNivelGeneral(request, kit, null, new ServiceException(e.getMessage()));
		} finally {
			depurarArchivos(plantillasCliente);
		}
	}

	protected void backupTrama(RequestDTO request, Kit kit) throws ServiceException {
		if (flagBackupTrama) {
			Timestamp timeActual = new Timestamp(System.currentTimeMillis());
			Trama trama = backupService.guardarTrama(request, null, null, kit.getGrupo().getCdEnv(),
					Constants.ESTADO_PROCESO_ERROR, 0, timeActual);
			kit.setTrama(trama);
		}
	}

	private void processSubContratos(RequestDTO request, List<ClienteDTO> plantillasCliente) throws ServiceException {
		if (request.getKits() != null && !request.getKits().isEmpty()) {
			for (RequestDTO subRequest : request.getKits()) {
				Map<String, Object> parametros = util.toMap(subRequest);
				subRequest.getListaClientes().addAll(request.getListaClientes());
				Kit subKit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), subRequest.getIdContrato());
				if(subKit!=null) {
					List<ClienteDTO> subPlantillasCliente = processGenerarContratos(subRequest,subKit, parametros);
					if (subPlantillasCliente != null && !subPlantillasCliente.isEmpty()) {
						consolidarKits(plantillasCliente, subPlantillasCliente);
					}
				}
			}
		}
	}

	public List<ClienteDTO> processGenerarContratos(RequestDTO request, Kit kit, Map<String, Object> parametros)
			throws ServiceException {
		try {
			kitService.cargarPlantillasContratoCliente(kit);
			List<ClienteDTO> plantillasCliente = procesarRutas(kit, request);
			ClienteDTO titular = Util.obtenerTitular(plantillasCliente);

			generarContratosService.generarContratos(kit.getListTemplates(), parametros, request.getContenido(),
					titular, plantillasCliente, request);

			if (signBoxActivo(kit)) {
				signService.signContracts(kit.getListTemplates(), request, titular);
			} else {
				signService.otherSignContracts(kit.getListTemplates(), request, titular);
			}

			for (int i = 0; i < plantillasCliente.size(); i++) {
				ClienteDTO cliente = plantillasCliente.get(i);
				try {
					Map<String, Object> parametrosCliente = util.toMap(parametros, cliente.getCliente());

					if (!kit.getListTemplatesCliente().isEmpty()) {
						generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametrosCliente,
								request.getContenido(), cliente, plantillasCliente, request);
						if (signBoxActivo(kit)) {
							signService.signContracts(kit.getListTemplatesCliente(), request, cliente);
						} else {
							signService.otherSignContracts(kit.getListTemplatesCliente(), request, cliente);
						}
					}

				} catch (ServiceException e) {
					logger.error("Error en servicio de firma digital. ", e);
					guardarTramaNivelCliente(request, kit, cliente, e);
				} catch (Exception e) {
					logger.error("Error generico ", e);
				}
			}

			processSubContratos(request, plantillasCliente);

			return plantillasCliente;

		} catch (Exception e) {
			logger.error("Error en servicio de firma digital. ", e);
			throw new GenerarContratoException("Error en generacion de plantillas.");
		}
	}

	private void consolidarKits(List<ClienteDTO> plantillasCliente, List<ClienteDTO> subPlantillasCliente) {
		for (ClienteDTO plantillaCliente : plantillasCliente) {
			for (ClienteDTO subPlantillaCliente : subPlantillasCliente) {
				if (plantillaCliente.getCliente().getCodigoCentral().equals(subPlantillaCliente.getCliente().getCodigoCentral())) {
					Map<Long,List<ConfigurationPath>> mapCfg = subPlantillaCliente.getListCfg();
					for (Map.Entry<Long, List<ConfigurationPath>> entry : mapCfg.entrySet()){
						plantillaCliente.addCfg(entry.getKey(), entry.getValue());
					}
				}
			}
		}
	}

	public void processMail(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros) {
		if (correoActivo(kit)) {
			try {
				mailService.sendMailProc(request, parametros, kit, cliente);
			} catch (ServiceException e) {
				logger.error("Error en servicio de envio de correos. ", e);
				guardarTramaNivelCliente(request, kit, cliente, e);
			}
		}
	}

	public void processFileUnico(RequestDTO request, Kit kit, ClienteDTO cliente) {
		if (fileunicoActivo(kit)) {
			try {
				fileUnicoService.procesarArchivosWithTemp(kit, request, cliente);
			} catch (ServiceException e) {
				logger.error("Error en servicio de envio fileunico. ", e);
				guardarTramaNivelCliente(request, kit, cliente, e);
			}
		}
	}

	protected void guardarTramaNivelCliente(RequestDTO request, Kit kit, ClienteDTO cliente,
			ServiceException serviceException) {
		Timestamp timeActual = new Timestamp(System.currentTimeMillis());
		try {
			String estadoProceso = Constants.ESTADO_PROCESO_EXITOSO;
			if (serviceException != null) {
				estadoProceso = Constants.ESTADO_PROCESO_ERROR;
			}
			Trama trama = null;
			if (kit.getTrama() != null) {
				trama = kit.getTrama();
			} else {
				trama = backupService.guardarTrama(request, cliente, serviceException, kit.getGrupo().getCdEnv(),
						estadoProceso, 0, timeActual);
				kit.setTrama(trama);
			}
			backupService.guardarDetTrama(cliente, trama, serviceException, estadoProceso);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	protected void guardarTramaNivelGeneral(RequestDTO request, Kit kit, ClienteDTO cliente,
			ServiceException serviceException) {
		Timestamp timeActual = new Timestamp(System.currentTimeMillis());
		try {
			String estadoProceso = Constants.ESTADO_PROCESO_EXITOSO;
			if (serviceException != null) {
				estadoProceso = Constants.ESTADO_PROCESO_ERROR;
			}
			Trama trama = backupService.guardarTrama(request, cliente, serviceException, kit.getGrupo().getCdEnv(),
					estadoProceso, 0, timeActual);
			kit.setTrama(trama);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void depurarArchivos(List<ClienteDTO> listaClientes) {
		if (listaClientes != null) {
			ClienteDTO clienteTitular = Util.obtenerTitular(listaClientes);
			depurarCliente(clienteTitular);
			for(ClienteDTO cliente: listaClientes) {
				depurarCliente(cliente);
			}
		}		
	}
	
	public void depurarCliente(ClienteDTO clienteDTO) {
		if (clienteDTO != null) {			
			Map<Long,List<ConfigurationPath>> archivos = clienteDTO.getListCfg();
			for (Map.Entry<Long, List<ConfigurationPath>> entry : archivos.entrySet())
			{
				List<ConfigurationPath> archivosPlantilla = entry.getValue();
				for(ConfigurationPath archivoPlantilla : archivosPlantilla) {
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaCSV()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaFirma()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaPDF()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaTempFileUnicoPDF()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaTempFileUnicoCSV()));
				}
			}
		}
	}

	@Override
	public Documentos processPreviewKit(RequestDTO request) throws ServiceException {
		Kit kit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), request.getIdContrato());
		kitService.cargarPlantillasContratoCliente(kit);
		return processPreview(request, kit);
	}

	@Override
	public Documentos processPreviewPlantilla(RequestDTO request) throws ServiceException {
		Kit kit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), Constants.COD_KIT_DEFAULT);
		kitService.cargarPlantillasContratoCliente(kit, request.getIdContrato());
		return processPreview(request, kit);
	}

	public Documentos processPreview(RequestDTO request, Kit kit) throws ServiceException {
		Documentos documentos = new Documentos();
		List<Documento> documentosGeneral = null;
		try {
			if (kit != null) {
				Map<String, Object> parametrosGeneral = util.toMap(request);
				List<ClienteDTO> listaClientes = procesarRutas(kit, request);
				ClienteDTO titular = Util.obtenerTitular(listaClientes);
				documentosGeneral = generarContratosService.generarDocumentosBase64(kit.getListTemplates(), request,
						titular, listaClientes, parametrosGeneral, request.getContenido());
				documentos.setDocumentos(documentosGeneral);
				if (listaClientes != null) {
					for (int i = 0; i < listaClientes.size(); i++) {
						DocumentoCliente cliente = new DocumentoCliente();
						List<Documento> documentosClientes = null;
						ClienteDTO clienteDTO = listaClientes.get(i);
						Map<String, Object> parametrosCliente = util.toMap(request,clienteDTO.getCliente());
						documentosClientes = generarContratosService.generarDocumentosBase64(
								kit.getListTemplatesCliente(), request, clienteDTO, listaClientes, parametrosCliente,
								request.getContenido());
						cliente.setDocumentos(documentosClientes);
						cliente.setCodCliente(clienteDTO.getCliente().getNroDocumento());
						documentos.getClientes().add(cliente);
					}
				}
			}
		} catch (ServiceException e) {
			logger.error("Error en servicio processPreviewKit. ", e);
			throw e;
		}
		return documentos;
	}

	public List<ClienteDTO> procesarRutas(Kit kit, RequestDTO request) {

		Map<Integer, String> rutasBase = util.generarRutas();

		String ss = formatYYYYHHmm.format(new java.util.Date());

		String nroContrato = request.getNumeroContrato();

		List<ClienteDTO> listaClientes = new ArrayList<ClienteDTO>();
		for (int i = 0; i < request.getListaClientes().size(); i++) {
			ClienteDTO clienteDTO = new ClienteDTO();
			clienteDTO.setCliente(request.getListaClientes().get(i));
			for (Plantilla plantilla : kit.getListTemplates()) {
				String pattern = nroContrato + plantilla.getNbTipoFileunico() + ss + plantilla.getCdPlantilla()
						+ kit.getCdCodigo();
				ConfigurationPath cfg = new ConfigurationPath();
				cfg.setBasename(pattern);
				String rutaOri = rutasBase.get(Constants.RUTA_IDX_ORIGINAL);
				cfg.setRutaSalidaPDF(FilenameUtils.concat(rutaOri, pattern + Constants.EXT_PDF));
				cfg.setRutaSalidaFirma(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_FIRMA), pattern + Constants.EXT_PDF));
				cfg.setRutaCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_CSV),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoPDF(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_PDF));

				cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
				cfg.setStFileunico(plantilla.getStFileunico());
				cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
				cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
				cfg.setNumeroContrato(request.getNumeroContrato());
				cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());

				clienteDTO.addCfg(plantilla.getCdPlantilla(), cfg);
			}
			for (Plantilla plantilla : kit.getListTemplatesCliente()) {
				String pattern = clienteDTO.getCliente().getCodigoCentral() + nroContrato
						+ plantilla.getNbTipoFileunico() + ss + plantilla.getCdPlantilla() + kit.getCdCodigo();
				ConfigurationPath cfg = new ConfigurationPath();
				cfg.setBasename(pattern);
				cfg.setRutaSalidaPDF(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_ORIGINAL), pattern + Constants.EXT_PDF));
				cfg.setRutaSalidaFirma(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_FIRMA), pattern + Constants.EXT_PDF));
				cfg.setRutaCSV(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_CSV), pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						pattern + Constants.EXT_CSV));
				cfg.setRutaSalidaTempFileUnicoPDF(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						pattern + Constants.EXT_PDF));

				cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
				cfg.setStFileunico(plantilla.getStFileunico());
				cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
				cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
				cfg.setNumeroContrato(request.getNumeroContrato());
				cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());
				
				clienteDTO.addCfg(plantilla.getCdPlantilla(), cfg);
			}
			listaClientes.add(clienteDTO);
		}
		return listaClientes;
	}

	public ClienteDTO procesarRutasSimple(String rutaGenerada, List<Plantilla> listTemplates,
			FirmaContrato firmaContrato) {
		String ss = formatYYYYHHmm.format(new java.util.Date());
		int indice = 0;
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setCliente(firmaContrato.getListaClientes().get(indice));
		Cliente cliente = clienteDTO.getCliente();
		for (Plantilla plantilla : listTemplates) {
			String patternCliente = cliente.getNroDocumento() + plantilla.getNbTipoFileunico() + ss
					+ plantilla.getCdPlantilla();
			ConfigurationPath cfg = new ConfigurationPath();
			cfg.setBasename(patternCliente);
			cfg.setRutaSalidaPDF(FilenameUtils.concat(rutaGenerada, patternCliente + Constants.EXT_PDF));
			cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
			cfg.setStFileunico(plantilla.getStFileunico());
			cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
			cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
			cfg.setNumeroContrato(firmaContrato.getNumeroContrato());
			cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());

			clienteDTO.addCfg(plantilla.getCdPlantilla(), cfg);
		}
		return clienteDTO;
	}

}