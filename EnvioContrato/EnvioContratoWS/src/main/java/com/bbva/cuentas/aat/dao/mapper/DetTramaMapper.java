package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.DetTrama;

@Component
public class DetTramaMapper implements RowMapper<DetTrama> {

	public static final String ID_DET_TRAMA = "ID_DET_TRAMA";
	public static final String ID_TRAMA = "ID_TRAMA";
	public static final String CD_CLIENTE = "CD_CLIENTE";
	public static final String CD_STEP = "CD_STEP";
	public static final String CD_DOC = "CD_DOC";
	public static final String DET_STEP = "DET_STEP";
	public static final String ESTADO = "ESTADO";
	public static final String NUM_REPROCESO = "NUM_REPROCESO";
	public static final String TIME_REPROCESO = "TIME_REPROCESO";

	@Override
	public DetTrama mapRow(ResultSet rs, int rowNum) throws SQLException {
		DetTrama detTrama = new DetTrama();
		detTrama.setIdDetTrama(rs.getLong(ID_DET_TRAMA));
		detTrama.setIdTrama(rs.getLong(ID_TRAMA));
		detTrama.setCdCliente(rs.getString(CD_CLIENTE));
		detTrama.setCdStep(rs.getInt(CD_STEP));
		detTrama.setCdDoc(rs.getString(CD_DOC));
		detTrama.setDetStep(rs.getString(DET_STEP));
		detTrama.setEstado(rs.getString(ESTADO));
		detTrama.setNumReproceso(rs.getInt(NUM_REPROCESO));
		detTrama.setTimeReproceso(rs.getTimestamp(TIME_REPROCESO));

		return detTrama;

	}
}
