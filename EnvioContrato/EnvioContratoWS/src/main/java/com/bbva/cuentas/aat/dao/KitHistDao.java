package com.bbva.cuentas.aat.dao;

import java.sql.Timestamp;
import com.bbva.cuentas.bean.Kit;

public interface KitHistDao extends BaseDao<Kit, Long>{
	
	public Kit obtenerKitPorCodigo(Integer cdEnv,String codigo,Timestamp fechaCreacion) throws DaoException;
	
}
