package com.bbva.cuentas.ws;

import org.apache.logging.log4j.Logger;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bbva.cuentas.firmarcontratos.ReenvioContratosRequest;
import com.bbva.cuentas.firmarcontratos.ReenvioContratosResponse;
import com.bbva.cuentas.util.Constants;

@Endpoint
public class ReenvioContratosEndpoint implements MarshallingReevioContratosService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	/*
	@Autowired
	private ReenvioService reenvioService;
	*/
	
	@PayloadRoot(namespace = NAMESPACE, localPart = REENVIO_CONTRATOS_REQUEST)
	@ResponsePayload
	public ReenvioContratosResponse reenviarContratos(@RequestPayload ReenvioContratosRequest request) {
		ReenvioContratosResponse response = new ReenvioContratosResponse();
		try {			
			logger.info("[Ini] Servicio firmarContratos");
			//reenvioService.process(request);
			logger.info("[Fin] Servicio firmarContratos");
			response.setCodigoResultado(Constants.RESULT_OK);			
		} catch (Exception e) {
			logger.error("Error al procesar reenvio de contratos. ",e);			
			response.setCodigoResultado(Constants.RESULT_KO);
		}		
		return response;
	}
	
}
