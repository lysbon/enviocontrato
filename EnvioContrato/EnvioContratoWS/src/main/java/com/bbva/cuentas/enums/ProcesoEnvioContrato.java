package com.bbva.cuentas.enums;

public enum ProcesoEnvioContrato {
	TRAMA_INICIAL(0,"TRAMA_INICIAL"),
	VALIDACION_TRAMA(1,"VALIDACION_TRAMA"),
	OBTENER_HUELLA_DIGITAL(2,"OBTENER_HUELLA_DIGITAL"),
	GENERACION_PDFS_GENERAL(3,"GENERACION_PDFS_GENERAL"),
    FIRMA_DIGITAL_PDF_GENERAL(4,"FIRMA_DIGITAL_PDF_GENERAL"),
	GENERACION_PDFS_CLIENTE(5,"GENERACION_PDFS_CLIENTE"),
    FIRMA_DIGITAL_PDF_CLIENTE(6,"FIRMA_DIGITAL_PDF_CLIENTE"),
    ENVIO_EMAIL_CLIENTE(7,"ENVIO_EMAIL_CLIENTE"),
    ENVIO_FILEUNICO_CLIENTE(8,"ENVIO_FILEUNICO_CLIENTE"),
	SIN_FIRMA_DIGITAL_PDF_GENERAL(9,"SIN_FIRMA_DIGITAL_PDF_GENERAL"),
	SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE(10,"SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE"),
	CARGA_PLANTILLAS(11,"CARGA_PLANTILLAS"),
	DEPURACION_PLANTILLAS(12,"DEPURACION_PLANTILLAS"),
	GENERACION_RUTAS(13,"GENERACION_RUTAS"),
	GENERICO(99,"GENERICO");

	private Integer paso;
	private String detPaso;
	
     ProcesoEnvioContrato(Integer paso, String detPaso) {
    	this.paso = paso;
    	this.detPaso = detPaso;
    }
    
     public Integer getPaso() {
 		return this.paso;
 	}


 	public String getDetPaso() {
 		return this.detPaso;
 	}
     
 	/*
    public Integer value() {
        return paso;
    }*/
}
