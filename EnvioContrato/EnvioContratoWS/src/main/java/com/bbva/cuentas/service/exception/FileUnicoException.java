package com.bbva.cuentas.service.exception;

import java.text.MessageFormat;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class FileUnicoException extends ServiceException{
	
	private static final long serialVersionUID = 1659667236074434395L;

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final String MENSAJE = "Error en el proceso fileunico. ";
	private static final String FORMATO_LOG = MENSAJE + " ''{0}''.";
		
	public FileUnicoException(String mensaje) {
		super(CODIGO_SERVICE_FILEUNICO, MENSAJE);
		setPaso(ProcesoEnvioContrato.ENVIO_FILEUNICO_CLIENTE);
		logger.info(MessageFormat.format(FORMATO_LOG, mensaje));
	}
	
}
