package com.bbva.cuentas.service.loader;

import java.sql.*;

import java.io.*;

public interface ImageLoader  {

	public void writeBLOBPut(final int cddItem, String inputTextFileName) 
            throws IOException, SQLException;
	
}
