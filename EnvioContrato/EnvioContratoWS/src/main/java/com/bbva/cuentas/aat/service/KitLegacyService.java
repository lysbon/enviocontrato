package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.KitLegacy;
import com.bbva.cuentas.bean.KitPlantillaLegacy;
import com.bbva.cuentas.service.exception.ServiceException;

public interface KitLegacyService {

	public List<KitLegacy> obtenerKits() throws ServiceException;
	
	public List<KitPlantillaLegacy> obtenerKitPlantillas() throws ServiceException;

	public KitLegacy obtenerKitPorId(String cdKit) throws ServiceException;
		
	public String obtenerMailTemplate(String cdKit) throws ServiceException;
		
}