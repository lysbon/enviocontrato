package com.bbva.cuentas.service.worker.impl;

import java.util.Collection;
import java.util.Map;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.service.worker.JasperWorker;
import com.bbva.cuentas.util.Constants;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JasperWorkerImpl implements JasperWorker{
		
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${ruta.formatoJasper}")
	private String rutaFormato;

	public void processStream(
			Plantilla plantilla, 
			ConfigurationPath cfg,
			byte[] is,
			Map<String,Object> parametros,
			Collection<Map<String,?>> contenido) throws ServiceException{
		try {
            InputStream inputStream=  new ByteArrayInputStream(is); 
			JasperReport jasperReport = null;
			if (Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())){
				jasperReport = (JasperReport)JRLoader.loadObject(inputStream);
			}else{
				jasperReport = JasperCompileManager.compileReport(inputStream);
			}
			exportJasperToPDF(jasperReport,cfg,parametros,contenido);	
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e.getMessage());
		} 
	}
	
	public void process(Plantilla plantilla, 
			ConfigurationPath cfg,
			Map<String,Object> parametros,
			Collection<Map<String,?>> contenido) throws ServiceException{
		try {
			JasperReport jasperReport = null;
			if (Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())){
				jasperReport = (JasperReport)JRLoader.loadObjectFromFile(plantilla.getPathTemplate());
			}else{
				jasperReport = JasperCompileManager.compileReport(plantilla.getPathTemplate());
			}
			exportJasperToPDF(jasperReport,cfg,parametros,contenido);
		} catch (JRException e) {
			logger.error(e);
			throw new ServiceException(e.getMessage());
		} 
	}
	
	public void exportJasperToPDF(JasperReport jasperReport,
			ConfigurationPath cfg,
			Map<String,Object> parametros,
			Collection<Map<String,?>> contenido)throws ServiceException{
		try {
			parametros.put("STYLE_DIR",rutaFormato);			
			parametros.put("SUBREPORT_DIR",rutaFormato);
			parametros.put("IMAGE_DIR",rutaFormato);
			if(contenido==null){
				contenido = new ArrayList<Map<String,?>>();
				contenido.add(new HashMap<String,Object>());
			}
			JRMapCollectionDataSource datasource = new JRMapCollectionDataSource(contenido);	
			JasperPrint print = JasperFillManager.fillReport(jasperReport, parametros, datasource);
			JasperExportManager.exportReportToPdfFile(print, cfg.getRutaSalidaPDF());

		} catch (JRException e) {
			
			logger.error(e);
			throw new ServiceException(e.getMessage());
		} 
		
	}

}
