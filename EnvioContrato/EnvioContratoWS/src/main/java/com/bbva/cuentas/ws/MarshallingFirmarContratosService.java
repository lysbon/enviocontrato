package com.bbva.cuentas.ws;

import com.bbva.cuentas.firmarcontratos.FirmarContratosRequest;
import com.bbva.cuentas.firmarcontratos.FirmarContratosResponse;

public interface MarshallingFirmarContratosService {

	public final static String NAMESPACE = "http://bbva.com/cuentas/firmarContratos";
    public final static String FIRMAR_CONTRATOS_REQUEST = "firmar-contratos-request";

    /**
     * Gets person list.
     */
    public FirmarContratosResponse firmarContratos(FirmarContratosRequest request);
    
}
