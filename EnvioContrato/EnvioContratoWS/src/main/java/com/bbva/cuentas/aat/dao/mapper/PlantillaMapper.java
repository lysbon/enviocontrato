package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Plantilla;

@Component
public class PlantillaMapper implements RowMapper<Plantilla>{

	//TODO crear clases data
	public static final String CD_PLANTILLA="CD_PLANTILLA";
	public static final String ST_ACTIVO="ST_ACTIVO";
	public static final String ST_CONDICION="ST_CONDICION";
	public static final String ST_FILEUNICO="ST_FILEUNICO";
	public static final String ST_ENV_CORREO="ST_ENV_CORREO";
	public static final String ST_TIPO_CONSTR="ST_TIPO_CONSTR";
	public static final String ST_SIGNED="ST_SIGNED";
	public static final String NB_NOMBRE_FORMATO="NB_NOMBRE_FORMATO";
	public static final String NB_TIPO_FILEUNICO="NB_TIPO_FILEUNICO";
	public static final String NB_NOMBRE_CORREO="NB_NOMBRE_CORREO";	
	public static final String CD_ENV="CD_ENV";
	public static final String CH_TIPO_PLANTILLA="CH_TIPO_PLANTILLA";
	public static final String NB_DESCRIPCION="NB_DESCRIPCION";
	
	public static final String LB_PDF_PLANTILLA="LB_PDF_PLANTILLA";
	
	@Override
	public Plantilla mapRow(ResultSet rs, int rowNum) throws SQLException {
		Plantilla plantilla = new Plantilla();
		plantilla.setCdPlantilla(rs.getLong(CD_PLANTILLA));
		plantilla.setStActivo(rs.getString(ST_ACTIVO));
		plantilla.setStCondicion(rs.getString(ST_CONDICION));
		plantilla.setStFileunico(rs.getString(ST_FILEUNICO));
		plantilla.setStEnvCorreo(rs.getString(ST_ENV_CORREO));
		plantilla.setStTipoConstr(rs.getString(ST_TIPO_CONSTR));
		plantilla.setStSigned(rs.getString(ST_SIGNED));
		plantilla.setNbNombreFormato(rs.getString(NB_NOMBRE_FORMATO));
		plantilla.setNbNombreCorreo(rs.getString(NB_NOMBRE_CORREO));
		plantilla.setNbTipoFileunico(rs.getString(NB_TIPO_FILEUNICO));
		plantilla.setChTipoPlantilla(rs.getString(CH_TIPO_PLANTILLA));
		plantilla.setNbDescripcion(rs.getString(NB_DESCRIPCION));
		return plantilla;
	}

}
