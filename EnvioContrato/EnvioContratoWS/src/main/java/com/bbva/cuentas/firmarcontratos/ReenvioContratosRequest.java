//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.07.08 a las 05:23:33 PM PET 
//


package com.bbva.cuentas.firmarcontratos;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="firmaContrato" type="{http://bbva.com/cuentas/firmarContratos}reenvioContrato" maxOccurs="unbounded"/>
 *         &lt;element name="trama" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="forzar" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaOrigen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaFirma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaCSV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rutaFileUnico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "firmaContrato",
    "trama",
    "forzar",
    "rutaOrigen",
    "rutaFirma",
    "rutaCSV",
    "rutaFileUnico"
})
@XmlRootElement(name = "reenvio-contratos-request")
public class ReenvioContratosRequest {

    @XmlElement(required = true)
    protected List<ReenvioContrato> firmaContrato;
    @XmlElement(required = true)
    protected String trama;
    @XmlElement(required = true)
    protected String forzar;
    @XmlElement(required = true)
    protected String rutaOrigen;
    @XmlElement(required = true)
    protected String rutaFirma;
    @XmlElement(required = true)
    protected String rutaCSV;
    @XmlElement(required = true)
    protected String rutaFileUnico;

    /**
     * Gets the value of the firmaContrato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the firmaContrato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFirmaContrato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReenvioContrato }
     * 
     * 
     */
    public List<ReenvioContrato> getFirmaContrato() {
        if (firmaContrato == null) {
            firmaContrato = new ArrayList<ReenvioContrato>();
        }
        return this.firmaContrato;
    }

    /**
     * Obtiene el valor de la propiedad trama.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrama() {
        return trama;
    }

    /**
     * Define el valor de la propiedad trama.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrama(String value) {
        this.trama = value;
    }

    /**
     * Obtiene el valor de la propiedad forzar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForzar() {
        return forzar;
    }

    /**
     * Define el valor de la propiedad forzar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForzar(String value) {
        this.forzar = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaOrigen() {
        return rutaOrigen;
    }

    /**
     * Define el valor de la propiedad rutaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaOrigen(String value) {
        this.rutaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaFirma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaFirma() {
        return rutaFirma;
    }

    /**
     * Define el valor de la propiedad rutaFirma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaFirma(String value) {
        this.rutaFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaCSV.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaCSV() {
        return rutaCSV;
    }

    /**
     * Define el valor de la propiedad rutaCSV.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaCSV(String value) {
        this.rutaCSV = value;
    }

    /**
     * Obtiene el valor de la propiedad rutaFileUnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutaFileUnico() {
        return rutaFileUnico;
    }

    /**
     * Define el valor de la propiedad rutaFileUnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutaFileUnico(String value) {
        this.rutaFileUnico = value;
    }

}
