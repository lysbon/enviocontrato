package com.bbva.cuentas.service.exception;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

public class ServiceException extends BaseException{
	
	private static final long serialVersionUID = -1033697598802715791L;
	
	private ProcesoEnvioContrato paso;

	public ServiceException(String errorCode,String message) {
		super(errorCode,message);
		setPaso(ProcesoEnvioContrato.GENERICO);
	}

	public ServiceException(String message) {
		super(CODIGO_ERROR_GENERICO,message);
		setPaso(ProcesoEnvioContrato.GENERICO);
	}

	public static final String CODIGO_PLANTILLA_NO_ENCUENTRA	= "0001";
	public static final String CODIGO_PARAMETROS_INVALIDOS 		= "0002";
	public static final String CODIGO_CLIENTES_REPETIDOS		= "0003";
	public static final String CODIGO_ERROR_GENERICO			= "0004";

	public static final String CODIGO_SERVICE_MAIL              = "0010";
	public static final String CODIGO_SERVICE_FILEUNICO         = "0011";
	public static final String CODIGO_SERVICE_GENERAR_CONTRATO  = "0012";
	public static final String CODIGO_SERVICE_SIGNBOX           = "0013";
	public static final String CODIGO_SERVICE_KIT_CARGA         = "0014";
	public static final String CODIGO_SERVICE_HUELLA_CARGA      = "0015";
	public static final String CODIGO_SERVICE_GENERA_RUTA       = "0016";
	public static final String CODIGO_SERVICE_DEPURACION        = "0017";
	
	public ProcesoEnvioContrato getPaso() {
		return paso;
	}

	public void setPaso(ProcesoEnvioContrato paso){
		this.paso = paso;
	}

}
