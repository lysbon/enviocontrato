package com.bbva.cuentas.service.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.service.exception.FileUnicoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.ArchivoFileUnico;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.FileUnicoMetadata;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.enums.EstadoConciliacion;
import com.bbva.cuentas.enums.EstadoValidacion;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.service.FileUnicoService;
import com.bbva.cuentas.service.GestDocWSService;
import com.bbva.cuentas.service.worker.SFTPWorker;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.CSVUtils;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.gesdoc.basics.wsdl.MetadatoDocumento;
import com.bbva.gesdoc.basics.wsdl.PropiedadVO;
import com.bbva.gesdoc.basics.wsdl.StringArray;

@Service
public class FileUnicoServiceImpl implements FileUnicoService {

	@Autowired
	private SFTPWorker worker;

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Value("${fileunico.sftp.ruta}")
	private String rutaFileunico;

	@Value("${ruta.tempFileUnico}")
	private String rutaFileunicoTemp;

	@Autowired
	private GestDocWSService gestDocWSService;
	
	public void procesarArchivos(Kit kit, RequestDTO firmaContrato,
			ClienteDTO cliente) throws ServiceException {
		try {
			if (Constants.GESTDOC_ONLINE.equals(firmaContrato.getGestDoc())) {
				procesarArchivosOnline(cliente, firmaContrato);
			} else {
				procesarArchivosBatch(cliente, kit, firmaContrato);
			}
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FILEUNICO_GENERAL,"No se pudo procesar fileunico.");
		}
	}

	public void procesarArchivosWithTemp(Kit kit, RequestDTO firmaContrato,
			ClienteDTO cliente) throws ServiceException {
		try {
			if (Constants.GESTDOC_ONLINE.equals(firmaContrato.getGestDoc())) {
				procesarArchivosOnline(cliente, firmaContrato);
			} else {
				String ruta = rutaFileunicoTemp;
				procesarArchivosTempFileUnico(cliente, kit, firmaContrato, ruta);
				procesarArchivosBatch(cliente, kit, firmaContrato);
			}
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new FileUnicoException(e.getMessage());
		}
	}

	public void procesarArchivos(Kit kit, RequestDTO firmaContrato,
			ClienteDTO cliente, boolean metadata, String rutaOpcional)
			throws ServiceException {
		try {
			if (Constants.GESTDOC_ONLINE.equals(firmaContrato.getGestDoc())) {
				procesarArchivosOnline(cliente, firmaContrato);
			} else {
				procesarArchivosBatch(cliente, kit, firmaContrato);
			}
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FILEUNICO_GENERAL,
					"No se pudo procesar fileunico.");
		}
	}

	public void procesarArchivosBatch(ClienteDTO cliente, Kit kit,
			RequestDTO firmaContrato)
			throws Exception {
		String ruta = rutaFileunico;
		procesarArchivosBatchFileUnico(cliente, kit, firmaContrato,ruta);
	}

	public void procesarArchivosTempFileUnico(ClienteDTO cliente, Kit kit,
			RequestDTO firmaContrato, String ruta) throws Exception {

		transfiereArchivos(cliente, kit.getListTemplates(), ruta,
				Constants.DESTINO_TEMPFILEUNICO, firmaContrato, kit);

		transfiereArchivos(cliente, kit.getListTemplatesCliente(), ruta,
				Constants.DESTINO_TEMPFILEUNICO, firmaContrato, kit);
	}

	public void procesarArchivosBatchFileUnico(ClienteDTO cliente, Kit kit,
			RequestDTO firmaContrato, String ruta)
			throws Exception {

		transfiereArchivos(cliente, kit.getListTemplates(), ruta,
				Constants.DESTINO_FILEUNICOBATCH, firmaContrato, kit);

		transfiereArchivos(cliente, kit.getListTemplatesCliente(), ruta,
				Constants.DESTINO_FILEUNICOBATCH, firmaContrato, kit);

	}
	
	public void procesarArchivosOnline(ClienteDTO cliente, RequestDTO firmaContrato)
			throws ServiceException {
		if(firmaContrato.getKits()!=null && !firmaContrato.getKits().isEmpty()) {
			for(RequestDTO contrato : firmaContrato.getKits()) {
				homologarFirmaContrato(firmaContrato,contrato);
				procesarArchivosOnlineContrato(cliente,contrato);
			}
		}else {
			procesarArchivosOnlineContrato(cliente,firmaContrato);
		}
	}
	
	private void homologarFirmaContrato(RequestDTO firmaContrato, RequestDTO contrato) {
		if(StringUtils.isEmpty(contrato.getOficinaGestora())) {
			contrato.setOficinaGestora(firmaContrato.getOficinaGestora());
		}
		if(StringUtils.isEmpty(contrato.getProcedencia())) {
			contrato.setOficinaGestora(firmaContrato.getProcedencia());
		}
	}

	public void procesarArchivosOnlineContrato(ClienteDTO cliente, RequestDTO firmaContrato)
			throws ServiceException {
		Map<Long, List<ConfigurationPath>> map = cliente.getListCfg();
		for (Map.Entry<Long, List<ConfigurationPath>> entry : map.entrySet()){
			List<ConfigurationPath> configuracionesPlantilla = cliente.getListCfg(entry.getKey());
			for(ConfigurationPath configuracionPlantilla : configuracionesPlantilla) {
				if (Constants.ST_ACTIVO.equals(configuracionPlantilla.getStFileunico()) &&
					(StringUtils.isEmpty(configuracionPlantilla.getNumeroContrato())||configuracionPlantilla.getNumeroContrato().equals(firmaContrato.getNumeroContrato()))) {
					String tituloDocumento = cliente.getCliente().getCodigoCentral() + configuracionPlantilla.getBasename() + Constants.EXT_PDF;
					List<PropiedadVO> metadata = obtenerMetadataLista(cliente, configuracionPlantilla,firmaContrato, tituloDocumento);
					
					String ruta = StringUtils.EMPTY;
					if(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
						ruta = configuracionPlantilla.getRutaSalidaFirma();
					}else if(ProcesoEnvioContrato.GENERACION_PDFS_GENERAL.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
						ruta = configuracionPlantilla.getRutaSalidaPDF();
					}
					if(!StringUtils.isEmpty(ruta)) {
						try {
							gestDocWSService.altaDocumento(ruta,ruta, metadata);
						} catch (Exception e) {
							logger.error(AppUtil.getDetailedException(e).toString());
						}
					}
				}
			}	
		}
	}

	public void procesarMetadata(ClienteDTO cliente, Kit kit,RequestDTO firmaContrato) throws ServiceException {
		for (Plantilla plantilla : kit.getListTemplates()) {
			List<ConfigurationPath> listCfg = cliente.getListCfg(plantilla.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				if (plantilla.isValida() && Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
					FileUnicoMetadata metadata = obtenerMetadata(cliente,
							plantilla, firmaContrato);
					guardarMetadata(cfg.getRutaCSV(), metadata);
				}
			}
		}
		for (Plantilla plantilla : kit.getListTemplatesCliente()) {
			List<ConfigurationPath> listCfg = cliente.getListCfg(plantilla.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				if (plantilla.isValida() && Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
					FileUnicoMetadata metadata = obtenerMetadata(cliente,
							plantilla, firmaContrato);
					guardarMetadata(cfg.getRutaCSV(), metadata);
				}
			}
		}
	}

	public FileUnicoMetadata obtenerMetadata(ClienteDTO cliente,
			Plantilla plantilla, RequestDTO firmaContrato) {
		FileUnicoMetadata metadata = new FileUnicoMetadata();
		metadata.setCodigoCentral(cliente.getCliente().getCodigoCentral());
		metadata.setDoi(cliente.getCliente().getNroDocumento());
		metadata.setTipoDoi(cliente.getCliente().getTipoDocumento());
		metadata.setOficinaGestora(firmaContrato.getOficinaGestora());
		metadata.setNumeroContrato(firmaContrato.getNumeroContrato());
		metadata.setProcedencia(firmaContrato.getProcedencia());
		metadata.setEstadoConciliacion(EstadoConciliacion.NOAPLICA.value());
		metadata.setEstadoValidacion(EstadoValidacion.CORRECTO.value());
		metadata.setDocumento(plantilla.getNbTipoFileunico());
		return metadata;
	}

	private PropiedadVO propiedad(MetadatoDocumento label, String value) {
		PropiedadVO prop = new PropiedadVO();
		prop.setPropiedad(label);
		prop.setValor(value);
		return prop;
	}

	private PropiedadVO propiedad(MetadatoDocumento label, StringArray value) {
		PropiedadVO prop = new PropiedadVO();
		prop.setPropiedad(label);
		prop.setValor(value);
		return prop;
	}

	private List<PropiedadVO> obtenerMetadataLista(ClienteDTO cliente, 
			ConfigurationPath cfg,
			RequestDTO firmaContrato,
			String tituloDocumento) {
		List<PropiedadVO> propiedades = new ArrayList<PropiedadVO>();
		propiedades.add(propiedad(MetadatoDocumento.CODIGO_CENTRAL, cliente
				.getCliente().getCodigoCentral()));
		propiedades.add(propiedad(MetadatoDocumento.DOI, cliente.getCliente()
				.getNroDocumento()));
		propiedades.add(propiedad(MetadatoDocumento.TIPO_DOI, cliente
				.getCliente().getTipoDocumento()));
		propiedades.add(propiedad(MetadatoDocumento.OFICINA,
				firmaContrato.getOficinaGestora()));
		propiedades.add(propiedad(MetadatoDocumento.CODIGO_CONTRATO,
				firmaContrato.getNumeroContrato()));
		propiedades.add(propiedad(MetadatoDocumento.PROCEDENCIA,
				firmaContrato.getProcedencia()));
		propiedades.add(propiedad(MetadatoDocumento.ESTADO_CONCILIACION,
				EstadoConciliacion.NOAPLICA.value()));
		StringArray rEstadoValidacion = new StringArray();
		rEstadoValidacion.getItem().add(EstadoValidacion.CORRECTO.value());
		propiedades.add(propiedad(MetadatoDocumento.ESTADO_VALIDACION,
				rEstadoValidacion));
		propiedades.add(propiedad(MetadatoDocumento.DOCUMENTO,
				cfg.getNbTipoFileunico()));
		propiedades.add(propiedad(MetadatoDocumento.TITULO_DOCUMENTO,
				tituloDocumento));

		return propiedades;
	}

	public void guardarMetadata(String ruta, FileUnicoMetadata metadata)
			throws ServiceException {
		FileWriter writer=null;
		try {
			writer = new FileWriter(ruta);
			List<String> list = new ArrayList<String>();
			list.add(metadata.getCodigoCentral());
			list.add(metadata.getDoi());
			list.add(metadata.getTipoDoi());
			list.add(metadata.getOficinaGestora());
			list.add(metadata.getNumeroContrato());
			list.add(metadata.getNumeroSolicitud());
			list.add(metadata.getNumeroOperacion());
			list.add(metadata.getProcedencia());
			list.add(metadata.getLocalizacionFisica());
			list.add(metadata.getEstadoConciliacion());
			list.add(metadata.getEstadoValidacion());
			list.add(metadata.getFechaBaseVigencia());
			list.add(metadata.getFechaFinalVigencia());
			list.add(metadata.getCodigoHash());
			list.add(metadata.getTipoInterviniente());
			list.add(metadata.getCodigoCentralInterviniente());
			list.add(metadata.getTipoDoiInterviniente());
			list.add(metadata.getDoiInterviniente());
			list.add(metadata.getContratosReferenciados());
			list.add(metadata.getDocumento());
			CSVUtils.writeLine(writer, list, '|');
			writer.flush();
		} catch (IOException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_FILEUNICO_GENERAR_META,
					"No se pudo generar metadata.");
		}finally {
			try {if (writer!=null) writer.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
			
		}
	}

	public List<ArchivoFileUnico> obtenerListaArchivosPDF(
			List<Plantilla> plantillas, Map<Long, ConfigurationPath> cfgs) {
		List<ArchivoFileUnico> pares = new ArrayList<ArchivoFileUnico>();
		for (Plantilla plantilla : plantillas) {
			if (plantilla.isValida()
					&& Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
				ArchivoFileUnico par = new ArchivoFileUnico();
				ConfigurationPath cfg = cfgs.get(plantilla.getCdPlantilla());
				if (Constants.ST_ACTIVO.equals(plantilla.getStSigned())) {
					par.setRuta(cfg.getRutaSalidaFirma());
				} else {
					par.setRuta(cfg.getRutaSalidaPDF());
				}
				par.setNombreBase(cfg.getBasename());
				pares.add(par);
			}
		}
		return pares;
	}

	public List<ArchivoFileUnico> obtenerListaArchivosCSV(
			List<Plantilla> plantillas, Map<Long, ConfigurationPath> cfgs) {
		List<ArchivoFileUnico> pares = new ArrayList<ArchivoFileUnico>();
		for (Plantilla plantilla : plantillas) {
			if (plantilla.isValida()
					&& Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
				ArchivoFileUnico par = new ArchivoFileUnico();
				ConfigurationPath cfg = cfgs.get(plantilla.getCdPlantilla());
				par.setRuta(cfg.getRutaCSV());
				pares.add(par);
			}
		}
		return pares;
	}

	public void transfiereArchivos(ClienteDTO cliente,List<Plantilla> plantillas,
			String rutaDestino, int tipoDestino, RequestDTO firmaContrato, Kit kit) throws Exception {
		Map<Long, List<ConfigurationPath>> cfgs = cliente.getListCfg();
		for (Plantilla plantilla : plantillas) {
			if (plantilla.isValida()
					&& Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
				List<ConfigurationPath> listCfg = cfgs.get(plantilla.getCdPlantilla());
				for(ConfigurationPath cfg : listCfg) {
					if(tipoDestino == Constants.DESTINO_TEMPFILEUNICO) {
						transfiereArchivosTemp(cliente,plantilla,cfg,firmaContrato,kit);
					}else if(tipoDestino == Constants.DESTINO_FILEUNICOBATCH) {
						transfiereArchivos(cfg,rutaDestino,firmaContrato);
					}
				}
			}
		}
	}

	public void transfiereArchivosTemp(ClienteDTO cliente,Plantilla plantilla,ConfigurationPath cfg,RequestDTO firmaContrato,Kit kit) throws Exception {
		String rutaOrigenPDF = null;
		String rutaOrigenCSV = null;
		rutaOrigenCSV = cfg.getRutaCSV();
		if (Constants.ST_ACTIVO.equals(plantilla.getStSigned())) {
			rutaOrigenPDF = cfg.getRutaSalidaFirma();
		} else {
			rutaOrigenPDF = cfg.getRutaSalidaPDF();
		}
		worker.copiarArchivo(rutaOrigenPDF,cfg.getRutaSalidaTempFileUnicoPDF());
		if (!(Constants.GESTDOC_ONLINE.equals(firmaContrato.getGestDoc()))) {
			procesarMetadata(cliente, kit, firmaContrato);
			worker.copiarArchivo(rutaOrigenCSV,
					cfg.getRutaSalidaTempFileUnicoCSV());
		}
	}
	
	public void transfiereArchivos(ConfigurationPath cfg,String rutaDestino,RequestDTO firmaContrato) throws Exception {
		String rutaOrigenPDF = null;
		String rutaOrigenCSV = null;
		rutaOrigenPDF = cfg.getRutaSalidaTempFileUnicoPDF();
		rutaOrigenCSV = cfg.getRutaSalidaTempFileUnicoCSV();
		worker.moverArchivo(rutaOrigenPDF, rutaDestino);
		if (!(Constants.GESTDOC_ONLINE.equals(firmaContrato.getGestDoc()))) {
			worker.moverArchivo(rutaOrigenCSV, rutaDestino);
		}
	}
	

}
