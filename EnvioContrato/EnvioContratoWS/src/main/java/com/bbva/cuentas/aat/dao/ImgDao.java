package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.ResourceItem;

public interface ImgDao extends BaseDao<ResourceItem, Long>{
	
	public byte[] obtenerImagen(Long cdItem) throws DaoException;

	public List<ResourceItem> obtenerImagenes() throws DaoException;
	
}
