package com.bbva.cuentas.service;

import java.util.List;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.SignContracts;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;


public interface SignContractService {
		
	public SignContracts signContracts(
			List<Plantilla> plantillas,FirmaContrato firmaContrato,ClienteDTO cliente) throws ServiceException;

	public SignContracts otherSignContracts(
			List<Plantilla> plantillas,FirmaContrato firmaContrato,ClienteDTO cliente) throws ServiceException;

}
