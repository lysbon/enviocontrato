package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.Trama;

public interface TramaDao extends BaseDao<Trama, Long>{

	public List<Trama> obtenerTrama(
			String cdEnv,
			String procedencia,
			String tipo) throws DaoException;
	
	public Integer limpiarTramas() throws DaoException;
	
	public List<Trama> obtenerTramasParaReproceso(String Estado) throws DaoException;
	
}