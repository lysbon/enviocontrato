package com.bbva.cuentas.aat.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.TramaDao;
import com.bbva.cuentas.aat.service.JdbcService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class JdbcServiceImpl implements JdbcService {
	
	private static final Logger logger = LoggerFactory.getLogger(JdbcServiceImpl.class);
	
	@Autowired
	private TramaDao tramaDao;

	@Override
	public Map<String, Object> execute(String query) throws ServiceException {
		try {
			return tramaDao.execute(query);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_DB_ERROR,"No se pudo ejecutar consulta.");
		}
	}

	@Override
	public List<Map<String, Object>> query(String query)
			throws ServiceException {
		try {
			return tramaDao.query(query);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_DB_ERROR,"No se pudo ejecutar consulta.");
		}
	}
	
}
