//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AltaDocumentoPropiedadesRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AltaDocumentoPropiedadesRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="auditoria" type="{http://ws.gesdoc.basics.continental.bbva.com/}AuditoriaVO" minOccurs="0"/&gt;
 *         &lt;element name="propiedades" type="{http://ws.gesdoc.basics.continental.bbva.com/}PropiedadVO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="contenidoAdjunto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="nombreContenidoAdjunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AltaDocumentoPropiedadesRequest", propOrder = {
    "auditoria",
    "propiedades",
    "contenidoAdjunto",
    "nombreContenidoAdjunto"
})
public class AltaDocumentoPropiedadesRequest {

    protected AuditoriaVO auditoria;
    protected List<PropiedadVO> propiedades;
    @XmlMimeType("application/octet-stream")
    protected DataHandler contenidoAdjunto;
    protected String nombreContenidoAdjunto;

    /**
     * Obtiene el valor de la propiedad auditoria.
     * 
     * @return
     *     possible object is
     *     {@link AuditoriaVO }
     *     
     */
    public AuditoriaVO getAuditoria() {
        return auditoria;
    }

    /**
     * Define el valor de la propiedad auditoria.
     * 
     * @param value
     *     allowed object is
     *     {@link AuditoriaVO }
     *     
     */
    public void setAuditoria(AuditoriaVO value) {
        this.auditoria = value;
    }

    /**
     * Gets the value of the propiedades property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propiedades property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropiedades().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PropiedadVO }
     * 
     * 
     */
    public List<PropiedadVO> getPropiedades() {
        if (propiedades == null) {
            propiedades = new ArrayList<PropiedadVO>();
        }
        return this.propiedades;
    }

    /**
     * Obtiene el valor de la propiedad contenidoAdjunto.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getContenidoAdjunto() {
        return contenidoAdjunto;
    }

    /**
     * Define el valor de la propiedad contenidoAdjunto.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setContenidoAdjunto(DataHandler value) {
        this.contenidoAdjunto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreContenidoAdjunto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreContenidoAdjunto() {
        return nombreContenidoAdjunto;
    }

    /**
     * Define el valor de la propiedad nombreContenidoAdjunto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreContenidoAdjunto(String value) {
        this.nombreContenidoAdjunto = value;
    }

}
