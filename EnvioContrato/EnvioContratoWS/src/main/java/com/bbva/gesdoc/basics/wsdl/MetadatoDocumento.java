//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para metadatoDocumento.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="metadatoDocumento"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CODIGO_CENTRAL"/&gt;
 *     &lt;enumeration value="TIPO_DOI"/&gt;
 *     &lt;enumeration value="DOI"/&gt;
 *     &lt;enumeration value="CODIGO_CENTRAL_PARTICIPANTE"/&gt;
 *     &lt;enumeration value="CODIGO_CONTRATO"/&gt;
 *     &lt;enumeration value="NUMERO_SOLICITUD"/&gt;
 *     &lt;enumeration value="DOCUMENTO"/&gt;
 *     &lt;enumeration value="LOCALIZACION_FISICA"/&gt;
 *     &lt;enumeration value="FECHA_CREACION"/&gt;
 *     &lt;enumeration value="FECHA_FIN_VIGENCIA"/&gt;
 *     &lt;enumeration value="FECHA_BASE_VIGENCIA"/&gt;
 *     &lt;enumeration value="ESTADO_CONCILIACION"/&gt;
 *     &lt;enumeration value="OFICINA"/&gt;
 *     &lt;enumeration value="PROCEDENCIA"/&gt;
 *     &lt;enumeration value="TITULO_DOCUMENTO"/&gt;
 *     &lt;enumeration value="DOI_PARTICIPANTE"/&gt;
 *     &lt;enumeration value="TIPO_PARTICIPANTE"/&gt;
 *     &lt;enumeration value="TIPO_DOI_PARTICIPANTE"/&gt;
 *     &lt;enumeration value="ESTADO_VALIDACION"/&gt;
 *     &lt;enumeration value="NUMERO_OPERACION"/&gt;
 *     &lt;enumeration value="MIME_TYPE"/&gt;
 *     &lt;enumeration value="CODIGO_HASH"/&gt;
 *     &lt;enumeration value="ID_SUBPRODUCTO"/&gt;
 *     &lt;enumeration value="ENLACES"/&gt;
 *     &lt;enumeration value="TIPO_OPERACION"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "metadatoDocumento")
@XmlEnum
public enum MetadatoDocumento {

    CODIGO_CENTRAL,
    TIPO_DOI,
    DOI,
    CODIGO_CENTRAL_PARTICIPANTE,
    CODIGO_CONTRATO,
    NUMERO_SOLICITUD,
    DOCUMENTO,
    LOCALIZACION_FISICA,
    FECHA_CREACION,
    FECHA_FIN_VIGENCIA,
    FECHA_BASE_VIGENCIA,
    ESTADO_CONCILIACION,
    OFICINA,
    PROCEDENCIA,
    TITULO_DOCUMENTO,
    DOI_PARTICIPANTE,
    TIPO_PARTICIPANTE,
    TIPO_DOI_PARTICIPANTE,
    ESTADO_VALIDACION,
    NUMERO_OPERACION,
    MIME_TYPE,
    CODIGO_HASH,
    ID_SUBPRODUCTO,
    ENLACES,
    TIPO_OPERACION;

    public String value() {
        return name();
    }

    public static MetadatoDocumento fromValue(String v) {
        return valueOf(v);
    }

}
