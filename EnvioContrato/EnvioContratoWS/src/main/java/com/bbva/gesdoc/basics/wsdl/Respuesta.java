//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Respuesta complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Respuesta"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="globalCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="globalDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Respuesta", propOrder = {
    "globalCode",
    "globalDescription"
})
@XmlSeeAlso({
    RespuestaAltaAccesoDirectoDocumento.class,
    RespuestaAltaDocumento.class,
    RespuestaAltaDocumentoPropiedades.class,
    RespuestaBorrarAccesoDirectoDocumento.class,
    RespuestaBorrarDocumento.class,
    RespuestaBuscarDocumentos.class,
    RespuestaBuscarEntidades.class,
    RespuestaDescargarDocumento.class,
    RespuestaModificarDocumento.class,
    RespuestaVersionarDocumento.class,
    RespuestaVersionarDocumentoPropiedades.class
})
public class Respuesta {

    protected Integer globalCode;
    protected String globalDescription;

    /**
     * Obtiene el valor de la propiedad globalCode.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGlobalCode() {
        return globalCode;
    }

    /**
     * Define el valor de la propiedad globalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGlobalCode(Integer value) {
        this.globalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad globalDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalDescription() {
        return globalDescription;
    }

    /**
     * Define el valor de la propiedad globalDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalDescription(String value) {
        this.globalDescription = value;
    }

}
