//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para VersionarDocumentoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="VersionarDocumentoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VersionarDocumentoResponse" type="{http://ws.gesdoc.basics.continental.bbva.com/}RespuestaVersionarDocumento" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VersionarDocumentoResponse", propOrder = {
    "versionarDocumentoResponse"
})
public class VersionarDocumentoResponse {

    @XmlElement(name = "VersionarDocumentoResponse")
    protected RespuestaVersionarDocumento versionarDocumentoResponse;

    /**
     * Obtiene el valor de la propiedad versionarDocumentoResponse.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaVersionarDocumento }
     *     
     */
    public RespuestaVersionarDocumento getVersionarDocumentoResponse() {
        return versionarDocumentoResponse;
    }

    /**
     * Define el valor de la propiedad versionarDocumentoResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaVersionarDocumento }
     *     
     */
    public void setVersionarDocumentoResponse(RespuestaVersionarDocumento value) {
        this.versionarDocumentoResponse = value;
    }

}
