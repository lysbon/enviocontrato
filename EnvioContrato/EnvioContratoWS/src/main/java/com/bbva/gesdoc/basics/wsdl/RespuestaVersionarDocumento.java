//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RespuestaVersionarDocumento complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RespuestaVersionarDocumento"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://ws.gesdoc.basics.continental.bbva.com/}Respuesta"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="documentoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contenidoAdjunto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="nombreFicheroAdjunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaVersionarDocumento", propOrder = {
    "documentoId",
    "contenidoAdjunto",
    "nombreFicheroAdjunto"
})
public class RespuestaVersionarDocumento
    extends Respuesta
{

    protected String documentoId;
    protected byte[] contenidoAdjunto;
    protected String nombreFicheroAdjunto;

    /**
     * Obtiene el valor de la propiedad documentoId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoId() {
        return documentoId;
    }

    /**
     * Define el valor de la propiedad documentoId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoId(String value) {
        this.documentoId = value;
    }

    /**
     * Obtiene el valor de la propiedad contenidoAdjunto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContenidoAdjunto() {
        return contenidoAdjunto;
    }

    /**
     * Define el valor de la propiedad contenidoAdjunto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContenidoAdjunto(byte[] value) {
        this.contenidoAdjunto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreFicheroAdjunto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreFicheroAdjunto() {
        return nombreFicheroAdjunto;
    }

    /**
     * Define el valor de la propiedad nombreFicheroAdjunto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreFicheroAdjunto(String value) {
        this.nombreFicheroAdjunto = value;
    }

}
