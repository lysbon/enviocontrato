//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RespuestaBorrarAccesoDirectoDocumento complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RespuestaBorrarAccesoDirectoDocumento"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://ws.gesdoc.basics.continental.bbva.com/}Respuesta"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="documentoId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="carpeta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="oficinaCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoCentral" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="doi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numeroSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaBorrarAccesoDirectoDocumento", propOrder = {
    "documentoId",
    "carpeta",
    "oficinaCliente",
    "codigoCentral",
    "doi",
    "codigoContrato",
    "numeroSolicitud"
})
public class RespuestaBorrarAccesoDirectoDocumento
    extends Respuesta
{

    protected String documentoId;
    protected String carpeta;
    protected String oficinaCliente;
    protected String codigoCentral;
    protected String doi;
    protected String codigoContrato;
    protected String numeroSolicitud;

    /**
     * Obtiene el valor de la propiedad documentoId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoId() {
        return documentoId;
    }

    /**
     * Define el valor de la propiedad documentoId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoId(String value) {
        this.documentoId = value;
    }

    /**
     * Obtiene el valor de la propiedad carpeta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarpeta() {
        return carpeta;
    }

    /**
     * Define el valor de la propiedad carpeta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarpeta(String value) {
        this.carpeta = value;
    }

    /**
     * Obtiene el valor de la propiedad oficinaCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficinaCliente() {
        return oficinaCliente;
    }

    /**
     * Define el valor de la propiedad oficinaCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficinaCliente(String value) {
        this.oficinaCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoCentral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCentral() {
        return codigoCentral;
    }

    /**
     * Define el valor de la propiedad codigoCentral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCentral(String value) {
        this.codigoCentral = value;
    }

    /**
     * Obtiene el valor de la propiedad doi.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDoi() {
        return doi;
    }

    /**
     * Define el valor de la propiedad doi.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDoi(String value) {
        this.doi = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoContrato() {
        return codigoContrato;
    }

    /**
     * Define el valor de la propiedad codigoContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoContrato(String value) {
        this.codigoContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    /**
     * Define el valor de la propiedad numeroSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroSolicitud(String value) {
        this.numeroSolicitud = value;
    }

}
