//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bbva.gesdoc.basics.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AltaAccesoDirectoDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaAccesoDirectoDocumentoRequest");
    private final static QName _AltaAccesoDirectoDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaAccesoDirectoDocumentoResponse");
    private final static QName _AltaDocumentoPropiedadesRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaDocumentoPropiedadesRequest");
    private final static QName _AltaDocumentoPropiedadesResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaDocumentoPropiedadesResponse");
    private final static QName _AltaDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaDocumentoRequest");
    private final static QName _AltaDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AltaDocumentoResponse");
    private final static QName _AuditoriaVO_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "AuditoriaVO");
    private final static QName _BorrarAccesoDirectoDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BorrarAccesoDirectoDocumentoRequest");
    private final static QName _BorrarAccesoDirectoDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BorrarAccesoDirectoDocumentoResponse");
    private final static QName _BorrarDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BorrarDocumentoRequest");
    private final static QName _BorrarDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BorrarDocumentoResponse");
    private final static QName _BuscarDocumentosEntidadRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarDocumentosEntidadRequest");
    private final static QName _BuscarDocumentosEntidadResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarDocumentosEntidadResponse");
    private final static QName _BuscarDocumentosRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarDocumentosRequest");
    private final static QName _BuscarDocumentosResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarDocumentosResponse");
    private final static QName _BuscarEntidadesClienteRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarEntidadesClienteRequest");
    private final static QName _BuscarEntidadesClienteResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "BuscarEntidadesClienteResponse");
    private final static QName _CriterioFiltro_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "CriterioFiltro");
    private final static QName _CriterioOrdenacion_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "CriterioOrdenacion");
    private final static QName _DescargarDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "DescargarDocumentoRequest");
    private final static QName _DescargarDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "DescargarDocumentoResponse");
    private final static QName _DocumentoVO_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "DocumentoVO");
    private final static QName _EntidadVO_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "EntidadVO");
    private final static QName _ModifcarVersionarDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModifcarVersionarDocumentoRequest");
    private final static QName _ModifcarVersionarDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModifcarVersionarDocumentoResponse");
    private final static QName _ModificarDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModificarDocumentoRequest");
    private final static QName _ModificarDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModificarDocumentoResponse");
    private final static QName _ModificarVersionarDocumentoPropiedadesRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModificarVersionarDocumentoPropiedadesRequest");
    private final static QName _ModificarVersionarDocumentoPropiedadesResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "ModificarVersionarDocumentoPropiedadesResponse");
    private final static QName _PropiedadVO_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "PropiedadVO");
    private final static QName _Respuesta_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "Respuesta");
    private final static QName _RespuestaAltaAccesoDirectoDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaAltaAccesoDirectoDocumento");
    private final static QName _RespuestaAltaDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaAltaDocumento");
    private final static QName _RespuestaAltaDocumentoPropiedades_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaAltaDocumentoPropiedades");
    private final static QName _RespuestaBorrarAccesoDirectoDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaBorrarAccesoDirectoDocumento");
    private final static QName _RespuestaBorrarDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaBorrarDocumento");
    private final static QName _RespuestaBuscarDocumentos_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaBuscarDocumentos");
    private final static QName _RespuestaBuscarEntidades_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaBuscarEntidades");
    private final static QName _RespuestaDescargarDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaDescargarDocumento");
    private final static QName _RespuestaModificarDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaModificarDocumento");
    private final static QName _RespuestaVersionarDocumento_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaVersionarDocumento");
    private final static QName _RespuestaVersionarDocumentoPropiedades_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "RespuestaVersionarDocumentoPropiedades");
    private final static QName _VersionarDocumentoRequest_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "VersionarDocumentoRequest");
    private final static QName _VersionarDocumentoResponse_QNAME = new QName("http://ws.gesdoc.basics.continental.bbva.com/", "VersionarDocumentoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bbva.gesdoc.basics.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RespuestaModificarDocumento }
     * 
     */
    public RespuestaModificarDocumento createRespuestaModificarDocumento() {
        return new RespuestaModificarDocumento();
    }

    /**
     * Create an instance of {@link RespuestaModificarDocumento.Propiedades }
     * 
     */
    public RespuestaModificarDocumento.Propiedades createRespuestaModificarDocumentoPropiedades() {
        return new RespuestaModificarDocumento.Propiedades();
    }

    /**
     * Create an instance of {@link DocumentoVO }
     * 
     */
    public DocumentoVO createDocumentoVO() {
        return new DocumentoVO();
    }

    /**
     * Create an instance of {@link DocumentoVO.Propiedades }
     * 
     */
    public DocumentoVO.Propiedades createDocumentoVOPropiedades() {
        return new DocumentoVO.Propiedades();
    }

    /**
     * Create an instance of {@link AltaAccesoDirectoDocumentoRequest }
     * 
     */
    public AltaAccesoDirectoDocumentoRequest createAltaAccesoDirectoDocumentoRequest() {
        return new AltaAccesoDirectoDocumentoRequest();
    }

    /**
     * Create an instance of {@link AltaAccesoDirectoDocumentoResponse }
     * 
     */
    public AltaAccesoDirectoDocumentoResponse createAltaAccesoDirectoDocumentoResponse() {
        return new AltaAccesoDirectoDocumentoResponse();
    }

    /**
     * Create an instance of {@link AltaDocumentoPropiedadesRequest }
     * 
     */
    public AltaDocumentoPropiedadesRequest createAltaDocumentoPropiedadesRequest() {
        return new AltaDocumentoPropiedadesRequest();
    }

    /**
     * Create an instance of {@link AltaDocumentoPropiedadesResponse }
     * 
     */
    public AltaDocumentoPropiedadesResponse createAltaDocumentoPropiedadesResponse() {
        return new AltaDocumentoPropiedadesResponse();
    }

    /**
     * Create an instance of {@link AltaDocumentoRequest }
     * 
     */
    public AltaDocumentoRequest createAltaDocumentoRequest() {
        return new AltaDocumentoRequest();
    }

    /**
     * Create an instance of {@link AltaDocumentoResponse }
     * 
     */
    public AltaDocumentoResponse createAltaDocumentoResponse() {
        return new AltaDocumentoResponse();
    }

    /**
     * Create an instance of {@link AuditoriaVO }
     * 
     */
    public AuditoriaVO createAuditoriaVO() {
        return new AuditoriaVO();
    }

    /**
     * Create an instance of {@link BorrarAccesoDirectoDocumentoRequest }
     * 
     */
    public BorrarAccesoDirectoDocumentoRequest createBorrarAccesoDirectoDocumentoRequest() {
        return new BorrarAccesoDirectoDocumentoRequest();
    }

    /**
     * Create an instance of {@link BorrarAccesoDirectoDocumentoResponse }
     * 
     */
    public BorrarAccesoDirectoDocumentoResponse createBorrarAccesoDirectoDocumentoResponse() {
        return new BorrarAccesoDirectoDocumentoResponse();
    }

    /**
     * Create an instance of {@link BorrarDocumentoRequest }
     * 
     */
    public BorrarDocumentoRequest createBorrarDocumentoRequest() {
        return new BorrarDocumentoRequest();
    }

    /**
     * Create an instance of {@link BorrarDocumentoResponse }
     * 
     */
    public BorrarDocumentoResponse createBorrarDocumentoResponse() {
        return new BorrarDocumentoResponse();
    }

    /**
     * Create an instance of {@link BuscarDocumentosEntidadRequest }
     * 
     */
    public BuscarDocumentosEntidadRequest createBuscarDocumentosEntidadRequest() {
        return new BuscarDocumentosEntidadRequest();
    }

    /**
     * Create an instance of {@link BuscarDocumentosEntidadResponse }
     * 
     */
    public BuscarDocumentosEntidadResponse createBuscarDocumentosEntidadResponse() {
        return new BuscarDocumentosEntidadResponse();
    }

    /**
     * Create an instance of {@link BuscarDocumentosRequest }
     * 
     */
    public BuscarDocumentosRequest createBuscarDocumentosRequest() {
        return new BuscarDocumentosRequest();
    }

    /**
     * Create an instance of {@link BuscarDocumentosResponse }
     * 
     */
    public BuscarDocumentosResponse createBuscarDocumentosResponse() {
        return new BuscarDocumentosResponse();
    }

    /**
     * Create an instance of {@link BuscarEntidadesClienteRequest }
     * 
     */
    public BuscarEntidadesClienteRequest createBuscarEntidadesClienteRequest() {
        return new BuscarEntidadesClienteRequest();
    }

    /**
     * Create an instance of {@link BuscarEntidadesClienteResponse }
     * 
     */
    public BuscarEntidadesClienteResponse createBuscarEntidadesClienteResponse() {
        return new BuscarEntidadesClienteResponse();
    }

    /**
     * Create an instance of {@link CriterioFiltro }
     * 
     */
    public CriterioFiltro createCriterioFiltro() {
        return new CriterioFiltro();
    }

    /**
     * Create an instance of {@link CriterioOrdenacion }
     * 
     */
    public CriterioOrdenacion createCriterioOrdenacion() {
        return new CriterioOrdenacion();
    }

    /**
     * Create an instance of {@link DescargarDocumentoRequest }
     * 
     */
    public DescargarDocumentoRequest createDescargarDocumentoRequest() {
        return new DescargarDocumentoRequest();
    }

    /**
     * Create an instance of {@link DescargarDocumentoResponse }
     * 
     */
    public DescargarDocumentoResponse createDescargarDocumentoResponse() {
        return new DescargarDocumentoResponse();
    }

    /**
     * Create an instance of {@link EntidadVO }
     * 
     */
    public EntidadVO createEntidadVO() {
        return new EntidadVO();
    }

    /**
     * Create an instance of {@link ModifcarVersionarDocumentoRequest }
     * 
     */
    public ModifcarVersionarDocumentoRequest createModifcarVersionarDocumentoRequest() {
        return new ModifcarVersionarDocumentoRequest();
    }

    /**
     * Create an instance of {@link ModifcarVersionarDocumentoResponse }
     * 
     */
    public ModifcarVersionarDocumentoResponse createModifcarVersionarDocumentoResponse() {
        return new ModifcarVersionarDocumentoResponse();
    }

    /**
     * Create an instance of {@link ModificarDocumentoRequest }
     * 
     */
    public ModificarDocumentoRequest createModificarDocumentoRequest() {
        return new ModificarDocumentoRequest();
    }

    /**
     * Create an instance of {@link ModificarDocumentoResponse }
     * 
     */
    public ModificarDocumentoResponse createModificarDocumentoResponse() {
        return new ModificarDocumentoResponse();
    }

    /**
     * Create an instance of {@link ModificarVersionarDocumentoPropiedadesRequest }
     * 
     */
    public ModificarVersionarDocumentoPropiedadesRequest createModificarVersionarDocumentoPropiedadesRequest() {
        return new ModificarVersionarDocumentoPropiedadesRequest();
    }

    /**
     * Create an instance of {@link ModificarVersionarDocumentoPropiedadesResponse }
     * 
     */
    public ModificarVersionarDocumentoPropiedadesResponse createModificarVersionarDocumentoPropiedadesResponse() {
        return new ModificarVersionarDocumentoPropiedadesResponse();
    }

    /**
     * Create an instance of {@link PropiedadVO }
     * 
     */
    public PropiedadVO createPropiedadVO() {
        return new PropiedadVO();
    }

    /**
     * Create an instance of {@link Respuesta }
     * 
     */
    public Respuesta createRespuesta() {
        return new Respuesta();
    }

    /**
     * Create an instance of {@link RespuestaAltaAccesoDirectoDocumento }
     * 
     */
    public RespuestaAltaAccesoDirectoDocumento createRespuestaAltaAccesoDirectoDocumento() {
        return new RespuestaAltaAccesoDirectoDocumento();
    }

    /**
     * Create an instance of {@link RespuestaAltaDocumento }
     * 
     */
    public RespuestaAltaDocumento createRespuestaAltaDocumento() {
        return new RespuestaAltaDocumento();
    }

    /**
     * Create an instance of {@link RespuestaAltaDocumentoPropiedades }
     * 
     */
    public RespuestaAltaDocumentoPropiedades createRespuestaAltaDocumentoPropiedades() {
        return new RespuestaAltaDocumentoPropiedades();
    }

    /**
     * Create an instance of {@link RespuestaBorrarAccesoDirectoDocumento }
     * 
     */
    public RespuestaBorrarAccesoDirectoDocumento createRespuestaBorrarAccesoDirectoDocumento() {
        return new RespuestaBorrarAccesoDirectoDocumento();
    }

    /**
     * Create an instance of {@link RespuestaBorrarDocumento }
     * 
     */
    public RespuestaBorrarDocumento createRespuestaBorrarDocumento() {
        return new RespuestaBorrarDocumento();
    }

    /**
     * Create an instance of {@link RespuestaBuscarDocumentos }
     * 
     */
    public RespuestaBuscarDocumentos createRespuestaBuscarDocumentos() {
        return new RespuestaBuscarDocumentos();
    }

    /**
     * Create an instance of {@link RespuestaBuscarEntidades }
     * 
     */
    public RespuestaBuscarEntidades createRespuestaBuscarEntidades() {
        return new RespuestaBuscarEntidades();
    }

    /**
     * Create an instance of {@link RespuestaDescargarDocumento }
     * 
     */
    public RespuestaDescargarDocumento createRespuestaDescargarDocumento() {
        return new RespuestaDescargarDocumento();
    }

    /**
     * Create an instance of {@link RespuestaVersionarDocumento }
     * 
     */
    public RespuestaVersionarDocumento createRespuestaVersionarDocumento() {
        return new RespuestaVersionarDocumento();
    }

    /**
     * Create an instance of {@link RespuestaVersionarDocumentoPropiedades }
     * 
     */
    public RespuestaVersionarDocumentoPropiedades createRespuestaVersionarDocumentoPropiedades() {
        return new RespuestaVersionarDocumentoPropiedades();
    }

    /**
     * Create an instance of {@link VersionarDocumentoRequest }
     * 
     */
    public VersionarDocumentoRequest createVersionarDocumentoRequest() {
        return new VersionarDocumentoRequest();
    }

    /**
     * Create an instance of {@link VersionarDocumentoResponse }
     * 
     */
    public VersionarDocumentoResponse createVersionarDocumentoResponse() {
        return new VersionarDocumentoResponse();
    }

    /**
     * Create an instance of {@link StringArray }
     * 
     */
    public StringArray createStringArray() {
        return new StringArray();
    }

    /**
     * Create an instance of {@link RespuestaModificarDocumento.Propiedades.Entry }
     * 
     */
    public RespuestaModificarDocumento.Propiedades.Entry createRespuestaModificarDocumentoPropiedadesEntry() {
        return new RespuestaModificarDocumento.Propiedades.Entry();
    }

    /**
     * Create an instance of {@link DocumentoVO.Propiedades.Entry }
     * 
     */
    public DocumentoVO.Propiedades.Entry createDocumentoVOPropiedadesEntry() {
        return new DocumentoVO.Propiedades.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaAccesoDirectoDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaAccesoDirectoDocumentoRequest")
    public JAXBElement<AltaAccesoDirectoDocumentoRequest> createAltaAccesoDirectoDocumentoRequest(AltaAccesoDirectoDocumentoRequest value) {
        return new JAXBElement<AltaAccesoDirectoDocumentoRequest>(_AltaAccesoDirectoDocumentoRequest_QNAME, AltaAccesoDirectoDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaAccesoDirectoDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaAccesoDirectoDocumentoResponse")
    public JAXBElement<AltaAccesoDirectoDocumentoResponse> createAltaAccesoDirectoDocumentoResponse(AltaAccesoDirectoDocumentoResponse value) {
        return new JAXBElement<AltaAccesoDirectoDocumentoResponse>(_AltaAccesoDirectoDocumentoResponse_QNAME, AltaAccesoDirectoDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaDocumentoPropiedadesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaDocumentoPropiedadesRequest")
    public JAXBElement<AltaDocumentoPropiedadesRequest> createAltaDocumentoPropiedadesRequest(AltaDocumentoPropiedadesRequest value) {
        return new JAXBElement<AltaDocumentoPropiedadesRequest>(_AltaDocumentoPropiedadesRequest_QNAME, AltaDocumentoPropiedadesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaDocumentoPropiedadesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaDocumentoPropiedadesResponse")
    public JAXBElement<AltaDocumentoPropiedadesResponse> createAltaDocumentoPropiedadesResponse(AltaDocumentoPropiedadesResponse value) {
        return new JAXBElement<AltaDocumentoPropiedadesResponse>(_AltaDocumentoPropiedadesResponse_QNAME, AltaDocumentoPropiedadesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaDocumentoRequest")
    public JAXBElement<AltaDocumentoRequest> createAltaDocumentoRequest(AltaDocumentoRequest value) {
        return new JAXBElement<AltaDocumentoRequest>(_AltaDocumentoRequest_QNAME, AltaDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AltaDocumentoResponse")
    public JAXBElement<AltaDocumentoResponse> createAltaDocumentoResponse(AltaDocumentoResponse value) {
        return new JAXBElement<AltaDocumentoResponse>(_AltaDocumentoResponse_QNAME, AltaDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuditoriaVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "AuditoriaVO")
    public JAXBElement<AuditoriaVO> createAuditoriaVO(AuditoriaVO value) {
        return new JAXBElement<AuditoriaVO>(_AuditoriaVO_QNAME, AuditoriaVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarAccesoDirectoDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BorrarAccesoDirectoDocumentoRequest")
    public JAXBElement<BorrarAccesoDirectoDocumentoRequest> createBorrarAccesoDirectoDocumentoRequest(BorrarAccesoDirectoDocumentoRequest value) {
        return new JAXBElement<BorrarAccesoDirectoDocumentoRequest>(_BorrarAccesoDirectoDocumentoRequest_QNAME, BorrarAccesoDirectoDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarAccesoDirectoDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BorrarAccesoDirectoDocumentoResponse")
    public JAXBElement<BorrarAccesoDirectoDocumentoResponse> createBorrarAccesoDirectoDocumentoResponse(BorrarAccesoDirectoDocumentoResponse value) {
        return new JAXBElement<BorrarAccesoDirectoDocumentoResponse>(_BorrarAccesoDirectoDocumentoResponse_QNAME, BorrarAccesoDirectoDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BorrarDocumentoRequest")
    public JAXBElement<BorrarDocumentoRequest> createBorrarDocumentoRequest(BorrarDocumentoRequest value) {
        return new JAXBElement<BorrarDocumentoRequest>(_BorrarDocumentoRequest_QNAME, BorrarDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BorrarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BorrarDocumentoResponse")
    public JAXBElement<BorrarDocumentoResponse> createBorrarDocumentoResponse(BorrarDocumentoResponse value) {
        return new JAXBElement<BorrarDocumentoResponse>(_BorrarDocumentoResponse_QNAME, BorrarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarDocumentosEntidadRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarDocumentosEntidadRequest")
    public JAXBElement<BuscarDocumentosEntidadRequest> createBuscarDocumentosEntidadRequest(BuscarDocumentosEntidadRequest value) {
        return new JAXBElement<BuscarDocumentosEntidadRequest>(_BuscarDocumentosEntidadRequest_QNAME, BuscarDocumentosEntidadRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarDocumentosEntidadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarDocumentosEntidadResponse")
    public JAXBElement<BuscarDocumentosEntidadResponse> createBuscarDocumentosEntidadResponse(BuscarDocumentosEntidadResponse value) {
        return new JAXBElement<BuscarDocumentosEntidadResponse>(_BuscarDocumentosEntidadResponse_QNAME, BuscarDocumentosEntidadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarDocumentosRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarDocumentosRequest")
    public JAXBElement<BuscarDocumentosRequest> createBuscarDocumentosRequest(BuscarDocumentosRequest value) {
        return new JAXBElement<BuscarDocumentosRequest>(_BuscarDocumentosRequest_QNAME, BuscarDocumentosRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarDocumentosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarDocumentosResponse")
    public JAXBElement<BuscarDocumentosResponse> createBuscarDocumentosResponse(BuscarDocumentosResponse value) {
        return new JAXBElement<BuscarDocumentosResponse>(_BuscarDocumentosResponse_QNAME, BuscarDocumentosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarEntidadesClienteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarEntidadesClienteRequest")
    public JAXBElement<BuscarEntidadesClienteRequest> createBuscarEntidadesClienteRequest(BuscarEntidadesClienteRequest value) {
        return new JAXBElement<BuscarEntidadesClienteRequest>(_BuscarEntidadesClienteRequest_QNAME, BuscarEntidadesClienteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarEntidadesClienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "BuscarEntidadesClienteResponse")
    public JAXBElement<BuscarEntidadesClienteResponse> createBuscarEntidadesClienteResponse(BuscarEntidadesClienteResponse value) {
        return new JAXBElement<BuscarEntidadesClienteResponse>(_BuscarEntidadesClienteResponse_QNAME, BuscarEntidadesClienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CriterioFiltro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "CriterioFiltro")
    public JAXBElement<CriterioFiltro> createCriterioFiltro(CriterioFiltro value) {
        return new JAXBElement<CriterioFiltro>(_CriterioFiltro_QNAME, CriterioFiltro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CriterioOrdenacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "CriterioOrdenacion")
    public JAXBElement<CriterioOrdenacion> createCriterioOrdenacion(CriterioOrdenacion value) {
        return new JAXBElement<CriterioOrdenacion>(_CriterioOrdenacion_QNAME, CriterioOrdenacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DescargarDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "DescargarDocumentoRequest")
    public JAXBElement<DescargarDocumentoRequest> createDescargarDocumentoRequest(DescargarDocumentoRequest value) {
        return new JAXBElement<DescargarDocumentoRequest>(_DescargarDocumentoRequest_QNAME, DescargarDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DescargarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "DescargarDocumentoResponse")
    public JAXBElement<DescargarDocumentoResponse> createDescargarDocumentoResponse(DescargarDocumentoResponse value) {
        return new JAXBElement<DescargarDocumentoResponse>(_DescargarDocumentoResponse_QNAME, DescargarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "DocumentoVO")
    public JAXBElement<DocumentoVO> createDocumentoVO(DocumentoVO value) {
        return new JAXBElement<DocumentoVO>(_DocumentoVO_QNAME, DocumentoVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntidadVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "EntidadVO")
    public JAXBElement<EntidadVO> createEntidadVO(EntidadVO value) {
        return new JAXBElement<EntidadVO>(_EntidadVO_QNAME, EntidadVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifcarVersionarDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModifcarVersionarDocumentoRequest")
    public JAXBElement<ModifcarVersionarDocumentoRequest> createModifcarVersionarDocumentoRequest(ModifcarVersionarDocumentoRequest value) {
        return new JAXBElement<ModifcarVersionarDocumentoRequest>(_ModifcarVersionarDocumentoRequest_QNAME, ModifcarVersionarDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifcarVersionarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModifcarVersionarDocumentoResponse")
    public JAXBElement<ModifcarVersionarDocumentoResponse> createModifcarVersionarDocumentoResponse(ModifcarVersionarDocumentoResponse value) {
        return new JAXBElement<ModifcarVersionarDocumentoResponse>(_ModifcarVersionarDocumentoResponse_QNAME, ModifcarVersionarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModificarDocumentoRequest")
    public JAXBElement<ModificarDocumentoRequest> createModificarDocumentoRequest(ModificarDocumentoRequest value) {
        return new JAXBElement<ModificarDocumentoRequest>(_ModificarDocumentoRequest_QNAME, ModificarDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModificarDocumentoResponse")
    public JAXBElement<ModificarDocumentoResponse> createModificarDocumentoResponse(ModificarDocumentoResponse value) {
        return new JAXBElement<ModificarDocumentoResponse>(_ModificarDocumentoResponse_QNAME, ModificarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarVersionarDocumentoPropiedadesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModificarVersionarDocumentoPropiedadesRequest")
    public JAXBElement<ModificarVersionarDocumentoPropiedadesRequest> createModificarVersionarDocumentoPropiedadesRequest(ModificarVersionarDocumentoPropiedadesRequest value) {
        return new JAXBElement<ModificarVersionarDocumentoPropiedadesRequest>(_ModificarVersionarDocumentoPropiedadesRequest_QNAME, ModificarVersionarDocumentoPropiedadesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarVersionarDocumentoPropiedadesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "ModificarVersionarDocumentoPropiedadesResponse")
    public JAXBElement<ModificarVersionarDocumentoPropiedadesResponse> createModificarVersionarDocumentoPropiedadesResponse(ModificarVersionarDocumentoPropiedadesResponse value) {
        return new JAXBElement<ModificarVersionarDocumentoPropiedadesResponse>(_ModificarVersionarDocumentoPropiedadesResponse_QNAME, ModificarVersionarDocumentoPropiedadesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PropiedadVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "PropiedadVO")
    public JAXBElement<PropiedadVO> createPropiedadVO(PropiedadVO value) {
        return new JAXBElement<PropiedadVO>(_PropiedadVO_QNAME, PropiedadVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Respuesta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "Respuesta")
    public JAXBElement<Respuesta> createRespuesta(Respuesta value) {
        return new JAXBElement<Respuesta>(_Respuesta_QNAME, Respuesta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaAltaAccesoDirectoDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaAltaAccesoDirectoDocumento")
    public JAXBElement<RespuestaAltaAccesoDirectoDocumento> createRespuestaAltaAccesoDirectoDocumento(RespuestaAltaAccesoDirectoDocumento value) {
        return new JAXBElement<RespuestaAltaAccesoDirectoDocumento>(_RespuestaAltaAccesoDirectoDocumento_QNAME, RespuestaAltaAccesoDirectoDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaAltaDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaAltaDocumento")
    public JAXBElement<RespuestaAltaDocumento> createRespuestaAltaDocumento(RespuestaAltaDocumento value) {
        return new JAXBElement<RespuestaAltaDocumento>(_RespuestaAltaDocumento_QNAME, RespuestaAltaDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaAltaDocumentoPropiedades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaAltaDocumentoPropiedades")
    public JAXBElement<RespuestaAltaDocumentoPropiedades> createRespuestaAltaDocumentoPropiedades(RespuestaAltaDocumentoPropiedades value) {
        return new JAXBElement<RespuestaAltaDocumentoPropiedades>(_RespuestaAltaDocumentoPropiedades_QNAME, RespuestaAltaDocumentoPropiedades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaBorrarAccesoDirectoDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaBorrarAccesoDirectoDocumento")
    public JAXBElement<RespuestaBorrarAccesoDirectoDocumento> createRespuestaBorrarAccesoDirectoDocumento(RespuestaBorrarAccesoDirectoDocumento value) {
        return new JAXBElement<RespuestaBorrarAccesoDirectoDocumento>(_RespuestaBorrarAccesoDirectoDocumento_QNAME, RespuestaBorrarAccesoDirectoDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaBorrarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaBorrarDocumento")
    public JAXBElement<RespuestaBorrarDocumento> createRespuestaBorrarDocumento(RespuestaBorrarDocumento value) {
        return new JAXBElement<RespuestaBorrarDocumento>(_RespuestaBorrarDocumento_QNAME, RespuestaBorrarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaBuscarDocumentos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaBuscarDocumentos")
    public JAXBElement<RespuestaBuscarDocumentos> createRespuestaBuscarDocumentos(RespuestaBuscarDocumentos value) {
        return new JAXBElement<RespuestaBuscarDocumentos>(_RespuestaBuscarDocumentos_QNAME, RespuestaBuscarDocumentos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaBuscarEntidades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaBuscarEntidades")
    public JAXBElement<RespuestaBuscarEntidades> createRespuestaBuscarEntidades(RespuestaBuscarEntidades value) {
        return new JAXBElement<RespuestaBuscarEntidades>(_RespuestaBuscarEntidades_QNAME, RespuestaBuscarEntidades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaDescargarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaDescargarDocumento")
    public JAXBElement<RespuestaDescargarDocumento> createRespuestaDescargarDocumento(RespuestaDescargarDocumento value) {
        return new JAXBElement<RespuestaDescargarDocumento>(_RespuestaDescargarDocumento_QNAME, RespuestaDescargarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaModificarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaModificarDocumento")
    public JAXBElement<RespuestaModificarDocumento> createRespuestaModificarDocumento(RespuestaModificarDocumento value) {
        return new JAXBElement<RespuestaModificarDocumento>(_RespuestaModificarDocumento_QNAME, RespuestaModificarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaVersionarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaVersionarDocumento")
    public JAXBElement<RespuestaVersionarDocumento> createRespuestaVersionarDocumento(RespuestaVersionarDocumento value) {
        return new JAXBElement<RespuestaVersionarDocumento>(_RespuestaVersionarDocumento_QNAME, RespuestaVersionarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaVersionarDocumentoPropiedades }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "RespuestaVersionarDocumentoPropiedades")
    public JAXBElement<RespuestaVersionarDocumentoPropiedades> createRespuestaVersionarDocumentoPropiedades(RespuestaVersionarDocumentoPropiedades value) {
        return new JAXBElement<RespuestaVersionarDocumentoPropiedades>(_RespuestaVersionarDocumentoPropiedades_QNAME, RespuestaVersionarDocumentoPropiedades.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionarDocumentoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "VersionarDocumentoRequest")
    public JAXBElement<VersionarDocumentoRequest> createVersionarDocumentoRequest(VersionarDocumentoRequest value) {
        return new JAXBElement<VersionarDocumentoRequest>(_VersionarDocumentoRequest_QNAME, VersionarDocumentoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.gesdoc.basics.continental.bbva.com/", name = "VersionarDocumentoResponse")
    public JAXBElement<VersionarDocumentoResponse> createVersionarDocumentoResponse(VersionarDocumentoResponse value) {
        return new JAXBElement<VersionarDocumentoResponse>(_VersionarDocumentoResponse_QNAME, VersionarDocumentoResponse.class, null, value);
    }

}
