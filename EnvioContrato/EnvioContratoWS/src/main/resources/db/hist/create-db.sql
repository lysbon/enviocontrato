--------------------------------------------------------
-- Archivo creado  - jueves-marzo-23-2017   
--------------------------------------------------------

  CREATE SCHEMA APDIGB AUTHORIZATION SA;

--------------------------------------------------------
--  DDL for Table TBG000_KIT
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG000_KIT" 
   ("CD_KIT" VARCHAR2(8) NOT NULL, 
	"NB_CORREO_ASUNTO" VARCHAR2(80), 
	"LB_CORREO_PLANTILLA" CLOB, 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE, 
	"ST_ENV_CORREO" CHAR(1), 
	"ST_FILEUNICO" CHAR(1), 
	"FL_VAL_HUELLA" CHAR(1), 
	"NB_CORREO_ENGINE" VARCHAR2(8)
   ) ;
--------------------------------------------------------
--  DDL for Table TBG001_KIT_PLANTILLA
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG001_KIT_PLANTILLA" 
   ("CD_KIT" VARCHAR2(8) NOT NULL, 
	"CD_PLANTILLA" NUMBER NOT NULL, 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TBG002_PLANTILLA
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG002_PLANTILLA" 
   ("CD_PLANTILLA" NUMBER NOT NULL, 
	"LB_PDF_PLANTILLA" BLOB, 
	"ST_ACTIVO" CHAR(1), 
	"ST_CONDICION" CHAR(1), 
	"ST_FILEUNICO" CHAR(1), 
	"ST_ENV_CORREO" CHAR(1), 
	"ST_TIPO_CONSTR" CHAR(1), 
	"ST_SIGNED" CHAR(1), 
	"NB_NOMBRE_FORMATO" VARCHAR2(40), 
	"NB_TIPO_FILEUNICO" VARCHAR2(10), 
	"NB_NOMBRE_CORREO" VARCHAR2(80), 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TBG003_SECCION
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG003_SECCION" 
   ("CD_SECCION" NUMBER NOT NULL, 
	"CD_PLANTILLA" NUMBER, 
	"NU_PAGINA" NUMBER(3,0), 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TBG004_ITEM
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG004_ITEM" 
   ("CD_ITEM" NUMBER NOT NULL, 
	"CD_SECCION" NUMBER, 
	"ST_ACTIVO" CHAR(1), 
	"NU_COOR_X" NUMBER(10,0), 
	"NU_COOR_Y" NUMBER(10,0), 
	"NB_NOMBRE_VAR" VARCHAR2(20), 
	"NB_VALOR_VAR" VARCHAR2(40), 
	"NB_FUENTE" VARCHAR2(20), 
	"NU_ALINEAR" CHAR(1), 
	"CH_TIPO" CHAR(1), 
	"NU_ANCHO" NUMBER, 
	"NU_SC_X" NUMBER, 
	"NU_SC_Y" NUMBER, 
	"NU_IDX_CLIENTE" NUMBER(12,0), 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE, 
	"NB_MASK" VARCHAR2(40)
   ) ;
--------------------------------------------------------
--  DDL for Table TBG005_FLAG
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG005_FLAG" 
   ("CD_FLAG" NUMBER NOT NULL, 
	"NB_CONDICION_NOMBRE" VARCHAR2(20), 
	"NB_CONDICION_VALOR" VARCHAR2(40), 
	"NU_ORDEN" NUMBER, 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE, 
	"CH_TIPO" CHAR(1)
   ) ;
--------------------------------------------------------
--  DDL for Table TBG006_FLAG_PLANTILLA
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" 
   ("CD_FLAG" NUMBER NOT NULL, 
	"CD_PLANTILLA" NUMBER NOT NULL, 
	"ST_VALOR" CHAR(1) NOT NULL, 
	"ST_ACTIVO" CHAR(1), 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TBG007_ITEM_IMG
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG007_ITEM_IMG" 
   ("CD_ITEM" NUMBER NOT NULL, 
	"LB_IMG" BLOB, 
	"CD_USU_CREA" VARCHAR2(20), 
	"CD_USU_MODI" VARCHAR2(20), 
	"FH_CREACION" DATE, 
	"FH_MODIFICACION" DATE
   ) ;
--------------------------------------------------------
--  DDL for Table TBG008_TRAMA
--------------------------------------------------------

  CREATE TABLE "APDIGB"."TBG008_TRAMA" 
   ("CH_MOTI" VARCHAR2(40), 
    "CH_TRAMA" VARCHAR2(4000), 
	"FH_CREACION" DATE
   ) ;

   
CREATE SEQUENCE APDIGB.tpl_seq
  MINVALUE 1
  MAXVALUE 9999999999999
  START WITH 1000
  INCREMENT BY 1
  CACHE 20;

CREATE SEQUENCE APDIGB.sec_seq
  MINVALUE 1
  MAXVALUE 9999999999999
  START WITH 1000
  INCREMENT BY 1
  CACHE 20;

CREATE SEQUENCE APDIGB.item_seq
  MINVALUE 1
  MAXVALUE 9999999999999
  START WITH 1000
  INCREMENT BY 1
  CACHE 20;
  
CREATE SEQUENCE APDIGB.res_seq
  MINVALUE 1
  MAXVALUE 9999999999999
  START WITH 1000
  INCREMENT BY 1
  CACHE 20;