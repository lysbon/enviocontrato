

/********* 2. Inserta Adicional de Firmas "APDIGB"."TBG002_PLANTILLA"  **********/
INSERT INTO "APDIGB"."TBG002_PLANTILLA" (
CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,
ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,
NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,
CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA) 
VALUES ((SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"),null,'A','V',
'A','A','P','A',
'BG0D1 - ADICIONAL FIRMAS','0002','ADICIONAL FIRMAS MANCOMUNADAS',
null,null,null,SYSDATE,'G');
COMMIT;
/********* 2. Inserta Adicional de Firmas "APDIGB"."TBG002_PLANTILLA"  **********/


/********* 3. Agregamos el indicador de mancomunada a la tabla TBG005_FLAG ******************/
INSERT INTO "APDIGB"."TBG005_FLAG" (
CD_FLAG,NB_CONDICION_NOMBRE,NB_CONDICION_VALOR,NU_ORDEN,
CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO) 
VALUES ((SELECT MAX(CD_FLAG)+1 FROM "APDIGB"."TBG005_FLAG"),'CFG0|P22','S','1',
null,null,null,null,'T');
COMMIT;
/********* 3. Agregamos el indicador de mancomunada a la tabla TBG005_FLAG ******************/

/********* 4. Actualiza APDIGB.TBG005_FLAG **********/
UPDATE APDIGB.TBG005_FLAG NB SET NB_CONDICION_NOMBRE = REPLACE(NB.NB_CONDICION_NOMBRE, 'CTR0', 'CFG0');
COMMIT;
/********* 4. Actualiza APDIGB.TBG005_FLAG **********/

/********* 5. Para actualizar la primera y segunda variable del asunto *****/
UPDATE APDIGB.TBG000_KIT SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CTR0G01>', '<CFG0P05>') 
WHERE  NB_CORREO_ASUNTO LIKE '%<CTR0G01>%'; 
   
UPDATE APDIGB.TBG000_KIT SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, ' ${CTR0|G01}', '<CFG0P05>') 
WHERE  NB_CORREO_ASUNTO LIKE '%${CTR0|G01}%';     
   
COMMIT;   
   
UPDATE APDIGB.TBG000_KIT SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CTR0P05>', '<CFG0P04>') 
WHERE  NB_CORREO_ASUNTO LIKE '%<CTR0P05>%';
   
UPDATE APDIGB.TBG000_KIT SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '${CTR0|P05}', '<CFG0P04>') 
WHERE  NB_CORREO_ASUNTO LIKE '%${CTR0|P05}%';
   
COMMIT;     
/********* 5. Para actualizar la primera y segunda variable del asunto *****/

/********* 6. Para actualizar el tenor del contenido del correo LB_CORREO_PLANTILLA *********/ 

UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CTR0G01>', '<CFG0P05>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%<CTR0G01>%'; 
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CTR0A01>', '<CFG0P23>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%<CTR0A01>%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CTR0A08>', '<CFG0P10>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%<CTR0A08>%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CTR0P01>', '<CFG0P01>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%<CTR0P01>%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CTR0P16>', '<CFG0P16>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%<CTR0P16>%';

UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCTR0P15', 'FCFG0P15') 
WHERE  LB_CORREO_PLANTILLA LIKE '%FCTR0P15%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCTR0P18', 'FCFG0P18') 
WHERE  LB_CORREO_PLANTILLA LIKE '%FCTR0P18%';
   
COMMIT;
    
    
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '${CTR0|G01}', '<CFG0P05>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%${CTR0|G01}%'; 
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '${CTR0|A01}', '<CFG0P23>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%${CTR0|A01}%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '${CTR0|A08}', '<CFG0P10>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%${CTR0|A08}%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '${CTR0|P01}', '<CFG0P01>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%${CTR0|P01}%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '${CTR0|P16}', '<CFG0P16>') 
WHERE  LB_CORREO_PLANTILLA LIKE '%${CTR0|P16}%';

UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCTR0P15', 'FCFG0P15') 
WHERE  LB_CORREO_PLANTILLA LIKE '%FCTR0P15%';
   
UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCTR0P18', 'FCFG0P18') 
WHERE  LB_CORREO_PLANTILLA LIKE '%FCTR0P18%';
   
COMMIT;
   
/********* 6. Para actualizar el tenor del contenido del correo LB_CORREO_PLANTILLA *********/ 

/************** 7. BG351 - CONTRATO CUENTA GANADORA ************* validado */  

INSERT INTO "APDIGB"."TBG004_ITEM" (
CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, 
NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '1', 'A', '46', '735',
null, 'CTR0|G02',null, 
'ArialBold10_5', '0', 'T',null,null,null);
COMMIT;

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '2', 'A', '370', '639',0,'CTR0|G02',null, 
'ArialBold9', '0', 'T',null,null,null);
COMMIT;

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '2', 'A', '420', '624',0, 'CTR0|G05',null, 
'ArialBold9', '0', 'T',null,null,null);
COMMIT;

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '2', 'A', '370', '763',1, 'huella', null, 
null,0, 'I',130,80,104);
COMMIT;

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '2', 'A', '369', '320',1, 'barcode', null, 
null,0, 'B',171,171,25);
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60',NU_COOR_Y='320',NU_ANCHO='171',NU_SC_X='171',NU_SC_Y='25' 
WHERE cd_seccion='2' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='89',NU_COOR_Y='763',NU_ANCHO='130',NU_SC_X='80',NU_SC_Y='104' 
WHERE cd_seccion='2' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

COMMIT;

/************** 7. BG351 - CONTRATO CUENTA GANADORA **************/  

/************** 8. BG461 - CONTRATO CUENTA SUELDO ************ validado **/ 


INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '7', 'A', '46', '720',null, 'CTR0|G02',null, 
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '8', 'A', '368', '613',null, 'CTR0|G02',null, 
'ArialBold9', '0', 'T', null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '8', 'A', '420', '602',null, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '8', 'A', '350', '736',1, 'huella',null,
'null', '0', 'I', 130,80,104);


INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '8', 'A', '369', '302',1, 'barcode', null, 
null, '0', 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60',NU_COOR_Y='302',NU_ANCHO='171',NU_SC_X='171',NU_SC_Y='25'   
WHERE cd_seccion='8' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='91',NU_COOR_Y='736',NU_ANCHO='130',NU_SC_X='80',NU_SC_Y='104'   
WHERE cd_seccion='8' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

COMMIT;

/************** 8. BG461 - CONTRATO CUENTA SUELDO **************/ 

/********** 9. Contrato CTA Senior ********** verificado*/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '14', 'A', '49', '733',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '15', 'A', '368', '645',null, 'CTR0|G02',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '15', 'A', '430', '630',null, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '15', 'A', '368', '760',1, 'huella',null,
'Arial9', '0', 'I', 130,80,100);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '15', 'A', '369', '384',1, 'barcode', null, 
null, '0', 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='93', NU_COOR_Y='760',NU_ANCHO='130',NU_SC_X='80',NU_SC_Y='100'   
WHERE cd_seccion='15' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60', NU_COOR_Y='384',NU_ANCHO='171',NU_SC_X='171',NU_SC_Y='25'   
WHERE cd_seccion='15' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 9. Contrato CTA Senior ***********/

/********** 10. Contrato CTA Independiente ********verificado***/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '20', 'A', '46', '736',0, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '21', 'A', '368', '420',0, 'CTR0|G02',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '21', 'A', '415', '408',0, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '21', 'A', '370', '534',1, 'huella',null,
'null', '0', 'I',130,80,101);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '21', 'A', '368', '105',1, 'barcode',null,
'null', '0', 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_Y='534',NU_ANCHO='130',NU_SC_X='80',NU_SC_Y='101'   
WHERE cd_seccion='21' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60',NU_COOR_Y='105',NU_ANCHO='171',NU_SC_X='171',NU_SC_Y='25' 
WHERE cd_seccion='21' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 10. Contrato CTA Independiente ***********/


/********** 11. BG971_Asociada_Prestamos **********verificado**/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '18', 'A', '49', '730',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T', null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '19', 'A', '370', '618',0, 'CTR0|G02',null, 
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '19', 'A', '417', '606',0, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '19', 'A', '370', '735',1, 'huella',null,
'', '', 'I', 130,80,104);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '19', 'A', '377', '308',1, 'barcode',null,
'Arial9', null, 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_Y='735',NU_ANCHO=130,NU_SC_X=80,NU_SC_Y=104    
WHERE cd_seccion='19' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60',NU_COOR_Y='308',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25   
WHERE cd_seccion='19' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 11. BG971_Asociada_Prestamos ***********/

/********** 12. BG511 - CONTRATO CONTIAHORRO EUROS ***********/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '16', 'A', '46', '735',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T', null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '17', 'A', '370', '336',0, 'CTR0|G02',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '17', 'A', '417', '324',0, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '17', 'A', '370', '452',1, 'huella',null,
null,0, 'I',130,80,104);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '17', 'A', '370', '92',1, 'barcode',null,
null, 0, 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_Y='452',NU_ANCHO=130,NU_SC_X=80,NU_SC_Y=104    
WHERE cd_seccion='17' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set  NU_COOR_X='60',NU_COOR_Y='92',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25   
WHERE cd_seccion='17' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 12. BG511 - CONTRATO CONTIAHORRO EUROS ***********/

/********** 13. BG451 - CONTRATO CUENTA REMESAS *********verificado**/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '12', 'A', '46', '745',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '13', 'A', '370', '618',0, 'CTR0|G02',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '13', 'A', '417', '601',0, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '13', 'A', '370', '739',1, 'huella',null,
null, 0, 'I', 130,80,104);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '13', 'A', '369', '300',1, 'barcode',null, 
null, 0, 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" SET NU_COOR_X = '60',NU_COOR_Y = '300',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25  
WHERE CD_SECCION=13 AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0';

UPDATE "APDIGB"."TBG004_ITEM" SET NU_COOR_X = '114',NU_COOR_Y = '618',NU_ALINEAR = '0' 
WHERE CD_SECCION=13 AND nb_nombre_var LIKE '%CTR0|G01%' ;

UPDATE "APDIGB"."TBG004_ITEM" SET NU_COOR_X = '369',NU_COOR_Y = '300',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25  
WHERE CD_SECCION=13 AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='1';

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_Y='739',NU_ANCHO=130,NU_SC_X=80,NU_SC_Y=104    
WHERE cd_seccion='13' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ;

COMMIT;

/********** 13. BG451 - CONTRATO CUENTA REMESAS ***********/

/********** 14.  BG151 - CONTRATO CUENTA  ***********/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '9', 'A', '49', '735',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '10', 'A', '370', '126',null, 'CTR0|G02',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '10', 'A', '417', '114',null, 'CTR0|G05',null,
'ArialBold9', '0', 'T', null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '10', 'A', '370', '243',1, 'huella',null,
'', '0', 'I',130,80,104);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '11', 'A', '369', '491',1, 'barcode',null,
null, '0', 'B',171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60',NU_COOR_Y='491',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25   
WHERE cd_seccion='11' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='88',NU_COOR_Y='243',NU_ANCHO=130,NU_SC_X=80,NU_SC_Y=104   
WHERE cd_seccion='10' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 14.  BG151 - CONTRATO CUENTA  ***********/

/********** 15.  BG111 - CONTRATO AHORRO   ***********/

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '22', 'A', '46', '745',null, 'CTR0|G02',null,
'ArialBold10_5', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '23', 'A', '370', '393',null, 'CTR0|G02',null,
'ArialBold9', '0', 'T', null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '23', 'A', '417', '377',null, 'CTR0|G05',null,
'ArialBold9', '0', 'T',null,null,null);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '23', 'A', '370', '509',1, 'huella',null,
null, '0', 'I', 130,80,104);

INSERT INTO "APDIGB"."TBG004_ITEM" (CD_ITEM, CD_SECCION, ST_ACTIVO, NU_COOR_X, NU_COOR_Y, NU_IDX_CLIENTE, NB_NOMBRE_VAR,NB_VALOR_VAR, 
NB_FUENTE, NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y) 
VALUES ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"), '23', 'A', '369', '90',1, 'barcode',null,
null, '0', 'B', 171,171,25);

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='60', NU_COOR_Y='90',NU_ANCHO=171,NU_SC_X=171,NU_SC_Y=25    
WHERE cd_seccion='23' AND nb_nombre_var LIKE '%barcode%' AND nu_idx_cliente='0'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='110',NU_ALINEAR='0', NU_COOR_Y='393'   
WHERE cd_seccion='23' AND nb_nombre_var LIKE '%CTR0|G01%'  ; 

UPDATE "APDIGB"."TBG004_ITEM" set NU_COOR_X='88',NU_COOR_Y='509',NU_ANCHO=130,NU_SC_X=80,NU_SC_Y=104   
WHERE cd_seccion='23' AND nb_nombre_var LIKE '%huella%' AND nu_idx_cliente='0'  ; 

COMMIT;

/********** 15.  BG111 - CONTRATO AHORRO   ***********/

/********** 16. Actualiza fecha larga en datos de los contratos ***********/
UPDATE "APDIGB"."TBG004_ITEM" SET nb_nombre_var='CTR0|G04'
WHERE cd_seccion='23' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" SET nb_nombre_var='CTR0|G04'
WHERE cd_seccion='2' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" SET nb_nombre_var='CTR0|G04'
WHERE cd_seccion='8' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" SET nb_nombre_var='CTR0|G04'
WHERE cd_seccion='10' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set nb_nombre_var='CTR0|G04'
WHERE cd_seccion='13' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set nb_nombre_var='CTR0|G04'
WHERE cd_seccion='17' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set nb_nombre_var='CTR0|G04'
WHERE cd_seccion='19' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set nb_nombre_var='CTR0|G04'
WHERE cd_seccion='21' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

UPDATE "APDIGB"."TBG004_ITEM" set nb_nombre_var='CTR0|G04'
WHERE cd_seccion='15' AND nb_nombre_var LIKE '%CTR0|A01%' ; 
COMMIT;

/********** 16. Actualiza fecha larga en datos de los contratos ***********/