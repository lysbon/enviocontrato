/********* 5. Agregamos la configuracion de la seccion al adicional de firmas ******************/
DECLARE
	v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
BEGIN
	SELECT CD_PLANTILLA 
	INTO   v_plantilla 
	FROM   "APDIGB"."TBG002_PLANTILLA" 
	WHERE  nb_nombre_formato LIKE '%BG0D1%' OR nb_nombre_correo LIKE '%BG0D1%';
  
	INSERT INTO "APDIGB"."TBG003_SECCION" (
		CD_SECCION,CD_PLANTILLA,NU_PAGINA,
		CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
	VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_plantilla,'1',
		null,null,null,null);
  
	COMMIT;
 
END;

/********* 5. Agregamos la configuracion de la seccion al adicional de firmas ******************/
