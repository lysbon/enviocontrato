/**** PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/
DECLARE

   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
   
    type typeCliente IS VARRAY(2) OF INTEGER; 
      
   idxCliente typeCliente;   
   numpagina_1 number(1);
  
   cont number;
   
   CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0C1%' or nb_nombre_correo like '%BG0C1%' order by CD_PLANTILLA ;  
     
BEGIN
   cont:=1; 
   idxCliente:= typeCliente(2, 6); 
   numpagina_1:=1;
  
   OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_cd_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;  
                  
                  select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_1; 
                  
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','56','765','nombreApellido',null,'ArialBold9','0','T',null,null,null,idxCliente(cont),null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','311','765','nombreApellido',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+1,null,null,null,null,null);   
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','56','460','nombreApellido',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+2,null,null,null,null,null);   
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','311','460','nombreApellido',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+3,null,null,null,null,null);
                  
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','104','745','huella',null,'null','0','I','130','80','104',idxCliente(cont),null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','360','745','huella',null,'null','0','I','130','80','104',idxCliente(cont)+1,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','104','438','huella',null,'null','0','I','130','80','104',idxCliente(cont)+2,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','360','438','huella',null,'null','0','I','130','80','104',idxCliente(cont)+3,null,null,null,null,null);
                  
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','130','145','CTR0|G04',null,'ArialBold9','0','T',null,null,null,'0',null,null,null,null,null);
				  
				  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','130','770','nroCta',null,'ArialBold9','0','T',null,null,null,'0',null,null,null,null,null);
                  
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','82','497','barcode',null,null,'0','B','171','171','10',idxCliente(cont),null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','341','497','barcode',null,null,'0','B','171','171','10',idxCliente(cont)+1,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','82','193','barcode',null,null,'0','B','171','171','10',idxCliente(cont)+2,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','341','193','barcode',null,null,'0','B','171','171','10',idxCliente(cont)+3,null,null,null,null,null);
                  
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','95','750','tipoDNI',null,'ArialBold9','0','T',null,null,null,idxCliente(cont),null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','350','750','tipoDNI',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+1,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','95','450','tipoDNI',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+2,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','350','450','tipoDNI',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+3,null,null,null,null,null);
               
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','110','750','dni',null,'ArialBold9','0','T',null,null,null,idxCliente(cont),null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','365','750','dni',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+1,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','110','450','dni',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+2,null,null,null,null,null);
                  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','365','450','dni',null,'ArialBold9','0','T',null,null,null,idxCliente(cont)+3,null,null,null,null,null);
                  
                  cont:=cont+1;
                  
                  COMMIT;
                            
         END LOOP;

  CLOSE C_PLANTILLA;
    
END;
/**** PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/