/* PLANTILLA BG531 - CUENTA CORRIENTE PERSONA NATURAL - GARANTIA DOLARES*/

DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(1);
   numpagina_2 number(1);
     
BEGIN
    numpagina_1:=1;
    numpagina_2:=4;
   
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG531%' or nb_nombre_correo like '%BG531%' ; 
   
    INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_1,
    null,null,null,null);
    
    
    INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_2,
    null,null,null,null);
    COMMIT;
   
END;
/* PLANTILLA BG531 - CUENTA CORRIENTE PERSONA NATURAL - GARANTIA DOLARES*/
