/**** PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/
DECLARE

   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
      
   numpagina_1 number(1);
   
   CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0C1%' or nb_nombre_correo like '%BG0C1%' order by CD_PLANTILLA ;  
     
BEGIN
  
  numpagina_1:=1;
    
  OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_cd_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;  
         
                INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
                CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
                VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_1,
                null,null,null,null);
                
                COMMIT;
      END LOOP;

  CLOSE C_PLANTILLA;
    
  
    
END;
/**** PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/