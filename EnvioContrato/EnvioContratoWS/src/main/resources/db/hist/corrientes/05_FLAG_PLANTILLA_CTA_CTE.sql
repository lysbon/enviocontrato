/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
DECLARE
    
    CURSOR C_FLAG IS select CD_FLAG  from "APDIGB"."TBG005_FLAG" where  nb_condicion_nombre like '%CFG0|P22%' or nb_condicion_nombre like '%CFG0|P24%' order by CD_FLAG;
    
    CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0C1%' or nb_nombre_correo like '%BG0C1%' order by CD_PLANTILLA ;   
  
    v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
    v_flag "APDIGB"."TBG005_FLAG".CD_FLAG%TYPE;
    
    
    TYPE t_plantilla IS TABLE OF NUMBER(4) INDEX BY BINARY_INTEGER;
   
    arrayPlantilla t_plantilla;
    
    cont NUMBER;
   
BEGIN
   
    cont:=1;
    OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;         
           
          arrayPlantilla(cont):=v_plantilla;       
          cont:=cont+1;
      END LOOP;

  CLOSE C_PLANTILLA;

  cont:=1;
  OPEN C_FLAG;
      LOOP
         FETCH C_FLAG  INTO v_flag;
         EXIT WHEN C_FLAG%NOTFOUND;         
           
                  INSERT INTO "APDIGB"."TBG006_FLAG_PLANTILLA" (CD_FLAG,CD_PLANTILLA,ST_VALOR,ST_ACTIVO,
                  CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
                  VALUES (v_flag,arrayPlantilla(cont),'A','A',
                  null,null,null,null);
                  
                  commit;
                  
                  cont:=cont+1;
         
      END LOOP;

  CLOSE C_FLAG;
   
END;

/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
