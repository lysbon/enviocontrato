--------------------------------------------------------
--  DDL for Index TBG001_KIT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG000_KIT_PK" ON "APDIGB"."TBG000_KIT" ("CD_KIT") 
  ;
--------------------------------------------------------
--  DDL for Index TBG001_KIT_PLT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG001_KIT_PLT_PK" ON "APDIGB"."TBG001_KIT_PLANTILLA" ("CD_KIT", "CD_PLANTILLA") 
  ;
--------------------------------------------------------
--  DDL for Index TBG002_PLANTILLA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG002_PLANTILLA_PK" ON "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA") 
  ;
--------------------------------------------------------
--  DDL for Index TBG003_SECCION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG003_SECCION_PK" ON "APDIGB"."TBG003_SECCION" ("CD_SECCION") 
  ;
--------------------------------------------------------
--  DDL for Index TBG004_ITEM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG004_ITEM_PK" ON "APDIGB"."TBG004_ITEM" ("CD_ITEM") 
  ;
--------------------------------------------------------
--  DDL for Index TBG005_FLAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG005_FLAG_PK" ON "APDIGB"."TBG005_FLAG" ("CD_FLAG") 
  ;
--------------------------------------------------------
--  DDL for Index TBG006_FLAG_PLANTILLA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG006_FLAG_PLANTILLA_PK" ON "APDIGB"."TBG006_FLAG_PLANTILLA" ("CD_FLAG", "CD_PLANTILLA", "ST_VALOR") 
  ;
--------------------------------------------------------
--  DDL for Index TBG007_ITEM_IMG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG007_ITEM_IMG_PK" ON "APDIGB"."TBG007_ITEM_IMG" ("CD_ITEM") 
  ;
--------------------------------------------------------
--  Constraints for Table TBG000_KIT
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG000_KIT" ADD CONSTRAINT "TBG000_KIT_PK" PRIMARY KEY ("CD_KIT");
--------------------------------------------------------
--  Constraints for Table TBG001_KIT_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_KIT_PLT_PK" PRIMARY KEY ("CD_KIT", "CD_PLANTILLA");
--------------------------------------------------------
--  Constraints for Table TBG002_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG002_PLANTILLA" ADD CONSTRAINT "TBG002_PLANTILLA_PK" PRIMARY KEY ("CD_PLANTILLA");
--------------------------------------------------------
--  Constraints for Table TBG003_SECCION
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG003_SECCION" ADD CONSTRAINT "TBG003_SECCION_PK" PRIMARY KEY ("CD_SECCION");
--------------------------------------------------------
--  Constraints for Table TBG004_ITEM
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG004_ITEM" ADD CONSTRAINT "TBG004_ITEM_PK" PRIMARY KEY ("CD_ITEM");
--------------------------------------------------------
--  Constraints for Table TBG005_FLAG
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG005_FLAG" ADD CONSTRAINT "TBG005_FLAG_PK" PRIMARY KEY ("CD_FLAG");
--------------------------------------------------------
--  Constraints for Table TBG006_FLAG_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TBG006_FLAG_PLANTILLA_PK" PRIMARY KEY ("CD_FLAG", "CD_PLANTILLA", "ST_VALOR");
--------------------------------------------------------
--  Constraints for Table TBG007_ITEM_IMG
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG007_ITEM_IMG" ADD CONSTRAINT "TBG007_ITEM_IMG_PK" PRIMARY KEY ("CD_ITEM");


--------------------------------------------------------
--  Ref Constraints for Table TBG001_KIT_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_PLT_KIT_PLT_FK1" FOREIGN KEY ("CD_KIT")
	  REFERENCES "APDIGB"."TBG000_KIT" ("CD_KIT");
  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_PLT_KIT_PLT_FK2" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");

--------------------------------------------------------
--  Ref Constraints for Table TBG003_SECCION
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG003_SECCION" ADD CONSTRAINT "TBG003_SEC_TBG002_PLT_FK" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");
--------------------------------------------------------
--  Ref Constraints for Table TBG004_ITEM
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG004_ITEM" ADD CONSTRAINT "TBG004_ITM_TBG003_SEC_FK" FOREIGN KEY ("CD_SECCION")
	  REFERENCES "APDIGB"."TBG003_SECCION" ("CD_SECCION");

--------------------------------------------------------
--  Ref Constraints for Table TBG006_FLAG_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TABLE_FLG_TBG002_PLT_FK" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");
  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TABLE_PLT_TBG005_FLG_FK" FOREIGN KEY ("CD_FLAG")
	  REFERENCES "APDIGB"."TBG005_FLAG" ("CD_FLAG");


