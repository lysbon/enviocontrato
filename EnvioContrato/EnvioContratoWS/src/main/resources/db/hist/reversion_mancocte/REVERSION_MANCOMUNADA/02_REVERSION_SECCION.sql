/********* 2. Revertimos la configuracion de la seccion al adicional de firmas ******************/
set serveroutput on
DECLARE

 
 
   v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  
   CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0D1%' or nb_nombre_correo like '%BG0D1%' order by CD_PLANTILLA ;   
 
     
BEGIN
      
      OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;         
          
             
              
              delete from "APDIGB"."TBG003_SECCION" where  CD_PLANTILLA=v_plantilla; 
              
              commit; 
			   
      END LOOP;
    CLOSE C_PLANTILLA;
    
  
   
  
   
    EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla para el BG0D1 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
    
END;
/********* 2. Revertimos la configuracion de la seccion al adicional de firmas ******************/