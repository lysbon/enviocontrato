/********* 1. Reversion de la configuracion de los items al ADICIONAL DE FIRMAS******************/
set serveroutput on
DECLARE

 
   v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
   v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(1);
   CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0D1%' or nb_nombre_correo like '%BG0D1%' order by CD_PLANTILLA ;   
  
     
BEGIN
  
  
  OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;         
          
              numpagina_1:=1;
              
              select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_plantilla  and nu_pagina=numpagina_1; 
	
              delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
              
              commit; 
			   
      END LOOP;
    CLOSE C_PLANTILLA;
   
  
  EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla para el BG0D1 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
    
END;
/********* 1. Reversion de la configuracion de los items al ADICIONAL DE FIRMAS******************/