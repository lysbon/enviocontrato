/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
DECLARE
    
  CURSOR C_TBG010_KIT IS select CD_KIT from "APDIGB"."TBG010_KIT" ;
  
  V_CD_ENV  "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  V_CD_KIT   "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
 
  
  cont NUMBER;
   
BEGIN
   
    cont:=0;
    
    OPEN C_TBG010_KIT;
      LOOP
         FETCH C_TBG010_KIT  INTO V_CD_KIT;
         EXIT WHEN C_TBG010_KIT%NOTFOUND;         
           
           select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT;
           
           IF cont=0 THEN 
            
                 INSERT INTO "APDIGB"."TBG012_ENV_KIT" (CD_ENV,CD_KIT) 
                 VALUES (1,V_CD_KIT);
                  
                 commit;
                  
           END IF;
              
    END LOOP;

  CLOSE C_TBG010_KIT;

   
END;

/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
