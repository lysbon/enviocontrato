
/********** 1. Contrato CTA Independiente ********verificado***/
delete from "APDIGB"."TBG004_ITEM" where cd_seccion='20' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='21' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='21' and nb_nombre_var like '%CTR0|G05%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='21' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='21' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;

/********** 1. Contrato CTA Independiente ***********/


/********** 2. BG971_Asociada_Prestamos **********verificado**/

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='18' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='19' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='19' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='19' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='19' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;

/********** 2. BG971_Asociada_Prestamos ***********/


/********** 3. BG511 - CONTRATO CONTIAHORRO EUROS ***********/
delete from "APDIGB"."TBG004_ITEM" where cd_seccion='16' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='17' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='17' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='17' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='17' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;

/********** 3. BG511 - CONTRATO CONTIAHORRO EUROS ***********/



/********** 4. BG451 - CONTRATO CUENTA REMESAS *********verificado**/
delete from "APDIGB"."TBG004_ITEM" where cd_seccion='12' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='13' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='13' and nb_nombre_var like '%CTR0|G05%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='13' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='13' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;

/********** 4. BG451 - CONTRATO CUENTA REMESAS ***********/

/********** 5.  BG151 - CONTRATO CUENTA  ***********/

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='9' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='10' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='10' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='10' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='11' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;
/********** 5.  BG151 - CONTRATO CUENTA  ***********/

/********** 6.  BG111 - CONTRATO AHORRO   ***********/

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='22' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='23' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='23' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='23' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='23' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;
/********** 6.  BG111 - CONTRATO AHORRO   ***********/

/************** 7. BG351 - CONTRATO CUENTA GANADORA ************* validado */  

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='1' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='2' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='2' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='2' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='2' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;
/************** 7. BG351 - CONTRATO CUENTA GANADORA **************/  

/************** 8. BG461 - CONTRATO CUENTA SUELDO ************ validado **/ 
delete from "APDIGB"."TBG004_ITEM" where cd_seccion='7' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='8' and nb_nombre_var like '%CTR0|G02%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='8' and nb_nombre_var like '%CTR0|G05%';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='8' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='8' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;
/************** 8. BG461 - CONTRATO CUENTA SUELDO **************/ 

/********** 9. Contrato CTA Senior ********** verificado*/

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='14' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='15' and nb_nombre_var like '%CTR0|G02%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='15' and nb_nombre_var like '%CTR0|G05%' ;

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='15' and nb_nombre_var like '%huella%' and nu_idx_cliente='1';

delete from "APDIGB"."TBG004_ITEM" where cd_seccion='15' and nb_nombre_var like '%barcode%' and nu_idx_cliente='1';

commit;
/********** 9. Contrato CTA Senior ***********/




/********* 13. Actualiza APDIGB.TBG005_FLAG **********/ 
UPDATE "APDIGB"."TBG005_FLAG" NB SET NB_CONDICION_NOMBRE = REPLACE(NB.NB_CONDICION_NOMBRE, 'CFG0', 'CTR0');
COMMIT;
/********* 13. Actualiza APDIGB.TBG005_FLAG **********/

/********* 14. Para actualizar la primera y segunda variable del asunto *****/ 
UPDATE "APDIGB"."TBG000_KIT" SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CFG0P05>', '<CTR0G01>') 
   WHERE  NB_CORREO_ASUNTO LIKE '%<CFG0P05>%'; 
   
UPDATE "APDIGB"."TBG000_KIT" SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CFG0P05>', '${CTR0|G01}') 
   WHERE  NB_CORREO_ASUNTO LIKE '%<CFG0P05>%';     
   
COMMIT;   
   
UPDATE "APDIGB"."TBG000_KIT" SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CFG0P04>', '<CTR0P05>') 
   WHERE  NB_CORREO_ASUNTO LIKE '%<CFG0P04>%';
   
UPDATE "APDIGB"."TBG000_KIT" SET NB_CORREO_ASUNTO=replace(NB_CORREO_ASUNTO, '<CFG0P04>', '${CTR0|P05}') 
   WHERE  NB_CORREO_ASUNTO LIKE '%<CFG0P04>%';
   
COMMIT;     
/********* 14. Para actualizar la primera y segunda variable del asunto *****/

/********* 15. Para actualizar el tenor del contenido del correo LB_CORREO_PLANTILLA *********/ 

   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P05>', '<CTR0G01>') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P05>%'; 
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P23>', '<CTR0A01>') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P23>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P10>', '<CTR0A08>') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P10>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P01>', '<CTR0P01>') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P01>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P16>', '<CTR0P16>') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P16>%';

   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCFG0P15', 'FCTR0P15') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%FCFG0P15%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCFG0P18', 'FCTR0P18') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%FCFG0P18%';
      
   COMMIT;
    
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P05>', '${CTR0|G01}') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P05>%'; 
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P23>', '${CTR0|A01}') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P23>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P10>', '${CTR0|A08}') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P10>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P01>', '${CTR0|P01}') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P01>%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, '<CFG0P16>', '${CTR0|P16}') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%<CFG0P16>%';

   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCFG0P15', 'FCTR0P15') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%FCFG0P15%';
   
   UPDATE APDIGB.TBG000_KIT SET LB_CORREO_PLANTILLA=replace(LB_CORREO_PLANTILLA, 'FCFG0P18', 'FCTR0P18') 
   WHERE  LB_CORREO_PLANTILLA LIKE '%FCFG0P18%';
   
   COMMIT;
   
/********* 15. Para actualizar el tenor del contenido del correo LB_CORREO_PLANTILLA *********/ 







