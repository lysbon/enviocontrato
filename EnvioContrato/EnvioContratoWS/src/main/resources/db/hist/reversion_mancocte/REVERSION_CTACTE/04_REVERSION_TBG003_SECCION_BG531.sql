/* PLANTILLA BG531 - Contrato garantia persona natural*/
set serveroutput on
DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
     
BEGIN
   
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG531%' or nb_nombre_correo like '%BG531%' ; 
   
   delete from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=v_cd_plantilla;

   COMMIT;
   
    EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla para el BG531 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!');   
   
END;

/* PLANTILLA BG481 - CONTR. CTA CTE (EUR)*/