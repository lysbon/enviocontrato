INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES 
(49,NULL,'A','F','A','A','P','A','BG561','0000','SOLICITUD DE AFILIACION A MULTIPLICA TU INTERES',NULL,NULL,NULL,TIMESTAMP '2018-02-14 12:23:13.000000','G','BG561 - AFILIACION')
,(50,NULL,'A','F','A','A','P','A','BG571','0000','DESAFILIACION A MULTIPLICA TU INTERES',NULL,NULL,NULL,TIMESTAMP '2018-02-14 12:23:13.000000','G','BG571 - DESAFILIACION')
,(57,NULL,'A','V','A','A','P','A','PE281','0498','PE281 - Aceptación de ley de protección de datos',NULL,NULL,NULL,TIMESTAMP '2018-02-20 13:14:10.000000','G','PE281 - LPDP')
,(53,NULL,'A','F','A','A','P','A','IC961','0500','IC961 - SOLICITUD Y CONDICIONADO PARTICULAR-SEGURO VIDA RENTA BBVA',NULL,NULL,NULL,TIMESTAMP '2018-02-16 13:16:20.000000','G','IC961 - SEGURO VIDA RENTA')
,(58,NULL,'A','V','A','A','P','A','PE101','0499','PE101 - Actualización de datos personales',NULL,NULL,NULL,TIMESTAMP '2018-02-20 13:14:10.000000','P','PE101 - LPDP')
,(54,NULL,'A','F','A','A','P','A','PE281','0498','PE281 - Aceptación de ley de protección de datos',NULL,NULL,NULL,TIMESTAMP '2018-02-20 13:14:10.000000','P','PE281 - LPDP')
,(55,NULL,'A','F','A','A','P','A','PE101','0499','PE101 - Actualización de datos personales',NULL,NULL,NULL,TIMESTAMP '2018-02-20 13:14:10.000000','P','PE101 - LPDP')
,(38,NULL,'A','F','A','A','P','A','BG011','0002','BG011 - CONTRATO CUENTA CORRIENTE SOLES',NULL,NULL,NULL,TIMESTAMP '2018-02-21 12:03:03.000000','G',NULL)
,(39,NULL,'A','F','A','A','P','A','BG481','0002','CONTRATO CTA CTE EUROS',NULL,NULL,NULL,TIMESTAMP '2018-01-29 17:27:19.000000','G',NULL)
,(40,NULL,'A','V','A','A','P','A','BG0C1','0002','BG0A3 - FIRMA INTERVINIENTES CTA CTE',NULL,NULL,NULL,TIMESTAMP '2018-02-21 15:50:46.000000','G',NULL)
;
INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES 
(41,NULL,'A','V','A','A','P','A','BG0C1','0002','BG0A3 - FIRMA INTERVINIENTES CTA CTE',NULL,NULL,NULL,TIMESTAMP '2017-09-28 11:52:44.000000','G',NULL)
,(42,NULL,'A','F','A','A','P','A','BG531','0002','BG531 - CUENTA CORRIENTE PERSONA NATURAL - GARANTIA',NULL,NULL,NULL,TIMESTAMP '2017-09-28 11:52:44.000000','G',NULL)
,(43,NULL,'A','V','A','A','P','A','BG0D1','0002','ADICIONAL FIRMAS MANCOMUNADAS',NULL,NULL,NULL,TIMESTAMP '2017-09-28 11:52:44.000000','G',NULL)
,(46,NULL,'A','F','A','A','J','A','VOUCHER_CANCEL_CTA_AHORRO','0486','VOUCHER CANCELACION CUENTA AHORRO',NULL,NULL,NULL,TIMESTAMP '2018-01-19 19:08:01.000000','G','VOUCHER_CANCEL_CTA_AHORRO')
,(37,NULL,'A','F','A','A','P','A','BG281','0002','CONTRATO CUENTA VIP SOLES',NULL,NULL,NULL,TIMESTAMP '2018-02-05 19:32:53.000000','G',NULL)
,(47,NULL,'A','F','A','A','P','A','VOUCHER_CANCEL_CTA_CTE','0486','VOUCHER CANCELACION CUENTA CORRIENTE',NULL,NULL,NULL,TIMESTAMP '2018-01-19 19:08:01.000000','G','VOUCHER_CANCEL_CTA_CTE')
,(48,NULL,'A','F','A','A','P','A','VOUCHER_CANCEL_CTS','0486','VOUCHER CANCELACION CTS',NULL,NULL,NULL,TIMESTAMP '2018-01-19 19:08:01.000000','G','VOUCHER_CANCEL_CTS')
,(56,NULL,'A','V','A','A','P','A','VOUCHER_CANCEL_CTA_CTE_ASO','0486','VOUCHER CANCELACION CUENTA CORRIENTE ASOCIADA',NULL,NULL,NULL,TIMESTAMP '2018-02-21 15:27:47.000000','G','VOUCHER_CANCEL_CTA_CTE_ASO')
;
INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES 
(26,NULL,'A','F','A','A','P','A','CT011','0002','CT011_CTS_Super_Depósito',NULL,NULL,NULL,TIMESTAMP '2017-05-19 18:59:54.000000','G','CT011 - CONTRATO CTS SUPER DEPÓSITO')
,(1,NULL,'A','F','A','A','P','A','BG351','0002','BG351 - CONTRATO CUENTA GANADORA',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:35:16.000000','G','BG351_Ganadora')
,(2,NULL,'A','F','A','A','P','A','CLAUG','0002','E3261 - CLAUSULAS GENERALES DE CONTRATACIÓN',NULL,NULL,NULL,TIMESTAMP '2018-02-06 10:25:46.000000','G','clau_general')
,(3,NULL,'A','V','A','A','P','A','BG871_NUEVO','0005','BG871 - APERTURA DE CUENTA PER. NATURAL',NULL,NULL,NULL,TIMESTAMP '2017-12-19 10:15:42.000000','P','BG871_APERTURA_CUENTA_NUEVO')
,(4,NULL,'A','V','A','A','P','A','BG871','0005','BG871 - APERTURA DE CUENTA PER. NATURAL',NULL,NULL,NULL,TIMESTAMP '2017-12-19 10:15:42.000000','P','BG871_APERTURA_CUENTA')
,(5,NULL,'A','V','A','A','P','A','E0600','0013','Declaración Jurada de Residencia Fiscal - Personas Naturales - KIT 01',NULL,NULL,NULL,TIMESTAMP '2018-01-22 15:08:46.000000','P','E0600_FATCA_FORM_AUTO_CERT_PN')
,(6,NULL,'I','V','A','A','P','A','E0601','0013','Declaración de Conocimiento y Autorización - Waiver - PE011',NULL,NULL,NULL,NULL,'P','E0601_FATCA_WAIVER')
,(7,NULL,'I','V','A','A','P','A','E0605','0013','Declaración Jurada Simple de Renuncia a la Nacionalidad Estadounidense - E0605',NULL,NULL,NULL,NULL,'P','E0605_FATCA_DJ_RNC_NAC_EEUU')
;
INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES 
(8,NULL,'A','F','A','A','P','A','BG461','0002','BG461 - CONTRATO CUENTA SUELDO',NULL,NULL,NULL,TIMESTAMP '2018-01-29 17:28:45.000000','G','BG461_Sueldo')
,(9,NULL,'A','F','A','A','N','A','DICTA','0233','RBA',NULL,NULL,NULL,NULL,'P','DICTAMEN_RBA_LINE')
,(10,NULL,'A','V','A','A','P','A','E3362','0233','E3362 - Declaración de Actividad Económica',NULL,NULL,NULL,NULL,'P','DAE_RBA')
,(11,NULL,'A','F','A','A','P','A','BG111','0002','BG111 - CONTRATO AHORRO VIVIENDA',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:37:15.000000','G','BG111_Ahorro_Vivienda')
,(12,NULL,'A','F','A','A','P','A','BG151','0002','BG151 - CONTRATO CUENTA FACIL',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:36:54.000000','G','BG151_Facil')
,(13,NULL,'A','F','A','A','P','A','BG451','0002','BG451 - CONTRATO CUENTA REMESAS',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:36:08.000000','G','BG451_Remesas')
,(14,NULL,'A','F','A','A','P','A','BG511','0002','BG511 - CONTRATO CONTIAHORRO EUROS',NULL,NULL,NULL,TIMESTAMP '2018-01-29 17:28:15.000000','G','BG511_Contiahorro_Euros')
,(15,NULL,'A','F','A','A','P','A','BG971','0002','BG971 - CONTRATO CUENTA ASOCIADA A PRESTAMOS',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:35:48.000000','G','BG971_Asociada_Prestamos')
,(16,NULL,'A','F','A','A','P','A','BGB81','0002','BGB81 - CONTRATO CUENTA INDEPENDENCIA',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:36:29.000000','G','BGB81_Independencia')
,(17,NULL,'A','F','A','A','P','A','BGC41','0002','BGC41 - CONTRATO CUENTA SENIOR',NULL,NULL,NULL,TIMESTAMP '2018-01-29 18:37:33.000000','G','BGC41_Senior')
;
INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES 
(18,NULL,'I','V','A','A','P','A','E2862','0004','E2862 - Formulario Familiares PEP y Vinculados',NULL,NULL,NULL,NULL,'G','E3362')
,(19,NULL,'I','V','A','A','P','A','Indicaciones_1','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_1')
,(20,NULL,'I','V','A','A','P','A','Indicaciones_2','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_2')
,(21,NULL,'I','V','A','A','P','A','Indicaciones_3','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_3')
,(22,NULL,'I','V','A','A','P','A','Indicaciones_4','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_4')
,(23,NULL,'I','V','A','A','P','A','Indicaciones_5','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_5')
,(24,NULL,'I','V','A','A','P','A','Indicaciones_6','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_6')
,(25,NULL,'I','V','A','A','P','A','Indicaciones_7','0000','Indicaciones',NULL,NULL,NULL,NULL,'P','Indicaciones_7')
,(59,NULL,'A','F','A','A','N','A','DNIBIO',        '0000','DNI BIOMETRICO',NULL,NULL,NULL,NULL,'G','DNI BIOMETRICO')
;