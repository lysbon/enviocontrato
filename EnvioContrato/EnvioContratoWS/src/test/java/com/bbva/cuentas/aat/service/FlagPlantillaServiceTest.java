package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class FlagPlantillaServiceTest {
	
	@Autowired
	private FlagPlantillaService flagPlantillaService;
		
	@Test
	public void obtenerItemPorCodigoTest() throws ServiceException{		
		List<FlagPlantilla> flagPlantillas = flagPlantillaService.obtenerFlagPlantillas();
		validarFlagPlantillas(flagPlantillas);
		FlagPlantilla flagPlantilla = flagPlantillaService.obtenerFlagPlantilla(
				flagPlantillas.get(0).getCdPlantilla(),
				flagPlantillas.get(0).getCdFlag());
		validarFlagPlantilla(flagPlantilla);
	}
	
	@Test
	public void obtenerFlagPlantillaPorPlantilla() throws ServiceException{
		List<FlagPlantilla> flagPlantillas = flagPlantillaService.obtenerFlagPlantillas();
		FlagPlantilla aux = flagPlantillas.get(0);
		List<FlagPlantilla> flagPlantillasTipo = flagPlantillaService.obtenerFlagsPorPlantilla(aux.getCdPlantilla());
		validarFlagPlantillas(flagPlantillasTipo);
	}
	
	@Test
	public void transaccionTest() throws ServiceException{
		
		List<FlagPlantilla> flagPlantillas = flagPlantillaService.obtenerFlagPlantillas();
		if(flagPlantillas!=null && flagPlantillas.size()>0){
			FlagPlantilla ref = flagPlantillas.get(0);
			Integer resultEliminar = flagPlantillaService.eliminarFlagPlantilla(ref.getCdPlantilla(),ref.getCdFlag());
			assertThat(resultEliminar!=null,is(true));
			assertThat(resultEliminar>0,is(true));
			FlagPlantilla flagPlantilla = flagPlantillaService.obtenerFlagPlantilla(ref.getCdPlantilla(),ref.getCdFlag());
			assertThat(flagPlantilla==null,is(true));
			
			Integer cdFlagPlantilla = flagPlantillaService.guardarFlagPlantilla(ref);
			assertThat(cdFlagPlantilla!=null,is(true));
			assertThat(cdFlagPlantilla>0,is(true));
			
			flagPlantilla = flagPlantillaService.obtenerFlagPlantilla(ref.getCdPlantilla(),ref.getCdFlag());
			assertThat(flagPlantilla!=null,is(true));
			assertThat(flagPlantilla.getCdFlag(),is(ref.getCdFlag()));
			assertThat(flagPlantilla.getCdPlantilla(),is(ref.getCdPlantilla()));
			assertThat(flagPlantilla.getStActivo(),is(ref.getStActivo()));
			assertThat(flagPlantilla.getStValor(),is(ref.getStValor()));
			
			FlagPlantilla flagPlantillaUpd = new FlagPlantilla();
			BeanUtils.copyProperties(flagPlantilla, flagPlantillaUpd);
			flagPlantillaUpd.setStValor(ParamConstants.FTP_ST_VALOR);
			Integer result = flagPlantillaService.actualizarFlagPlantilla(flagPlantillaUpd);
			assertThat(result!=null,is(true));
			assertThat(result>0,is(true));
			
			flagPlantilla = flagPlantillaService.obtenerFlagPlantilla(ref.getCdPlantilla(),ref.getCdFlag());
			assertThat(flagPlantilla!=null,is(true));
			assertThat(flagPlantilla.getCdFlag(),is(flagPlantillaUpd.getCdFlag()));
			assertThat(flagPlantilla.getCdPlantilla(),is(flagPlantillaUpd.getCdPlantilla()));
			assertThat(flagPlantilla.getStActivo(),is(flagPlantillaUpd.getStActivo()));
			assertThat(flagPlantilla.getStValor(),is(flagPlantillaUpd.getStValor()));
			
		}		
		
	}
	
	private void validarFlagPlantillas(List<FlagPlantilla> flagPlantillas){
		assertThat(flagPlantillas!=null,is(true));
		assertThat(flagPlantillas.size()>0,is(true));		
		for(FlagPlantilla flagPlantilla : flagPlantillas){
			validarFlagPlantilla(flagPlantilla);
		}
	}
	
	private void validarFlagPlantilla(FlagPlantilla flagPlantilla){
		assertThat(flagPlantilla!=null,is(true));
		assertThat(flagPlantilla.getCdFlag()!=null,is(true));
		assertThat(flagPlantilla.getCdPlantilla()!=null,is(true));
		assertThat(StringUtils.isEmpty(flagPlantilla.getStActivo()),is(false));
		assertThat(StringUtils.isEmpty(flagPlantilla.getStValor()),is(false));	
	}
	
}
