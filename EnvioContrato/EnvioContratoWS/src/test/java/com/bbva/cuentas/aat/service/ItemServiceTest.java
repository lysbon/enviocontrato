package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class ItemServiceTest {
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private SeccionService seccionService;
	
	@Value("${item.label.huella}")
	private String labelHuella;
	
	@Test
	public void obtenerItemPorCodigoTest() throws ServiceException{		
		List<ItemStamp> items = itemService.obtenerItems();
		validarItems(items);
		ItemStamp item = itemService.obtenerItemPorCodigo(items.get(0).getCdItem());
		validarItem(item);
	}
	
	@Test
	public void obtenerItemsPorSeccionTest() throws ServiceException{
		List<ItemStamp> items = itemService.obtenerItems();
		ItemStamp aux = items.get(0);
		List<ItemStamp> itemsSeccion = itemService.obtenerItemsPorSeccion(aux.getCdSeccion(),null);
		validarItems(itemsSeccion);
	}
	
	public ItemStamp nuevoItem() throws ServiceException{
		ItemStamp item = new ItemStamp();
		item.setChTipo(ParamConstants.ITM_CH_TIPO);
		item.setNbFuente(ParamConstants.ITM_NB_FUENTE);
		item.setNbMask(ParamConstants.ITM_NB_MASK);
		item.setNbNombreVar(ParamConstants.ITM_NB_NOMBRE_VAR);
		item.setNbValorVar(ParamConstants.ITM_NB_VALOR_VAR);
		item.setNuAlinear(ParamConstants.ITM_NU_ALINEAR);
		item.setNuAncho(ParamConstants.ITM_NU_ANCHO);
		item.setNuCoorX(ParamConstants.ITM_NU_COOR_X);
		item.setNuCoorY(ParamConstants.ITM_NU_COOR_Y);
		item.setNuIdxCliente(ParamConstants.ITM_NU_IDX_CLIENTE);
		item.setNuScX(ParamConstants.ITM_NU_SC_X);
		item.setNuScY(ParamConstants.ITM_NU_SC_Y);
		item.setStActivo(ParamConstants.ITM_ST_ACTIVO);
		
		List<ItemSection> secciones = seccionService.obtenerSecciones();
		if(secciones!=null && !secciones.isEmpty()){
			item.setCdSeccion(secciones.get(0).getCdSeccion());
		}else {
			item.setCdSeccion(ParamConstants.ITM_CD_SECCION);
		}
		
		return item;
	}
	
	@Test
	public void guardarEliminarItemTest() throws ServiceException{
		ItemStamp item = nuevoItem();		
		Long cdItem = itemService.guardarItem(item);
		assertThat(cdItem!=null,is(true));
		assertThat(cdItem>0,is(true));		
		Integer result = itemService.eliminarItem(cdItem);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void actualizarSeccionTest() throws ServiceException{
		List<ItemStamp> items = itemService.obtenerItems();
		if(items!=null && items.size()>0){
			ItemStamp item = items.get(0);
			item.setNbNombreVar(ParamConstants.ITM_NB_NOMBRE_VAR_2);
			item.setNbValorVar(ParamConstants.ITM_NB_VALOR_VAR_2);
			item.setNuCoorX(ParamConstants.ITM_NU_COOR_X_2);
			item.setNuCoorY(ParamConstants.ITM_NU_COOR_Y_2);
			Integer result = itemService.actualizarItem(item);
			assertThat(result!=null,is(true));
		}
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		ItemStamp item = nuevoItem();
		Long cdItem = itemService.guardarItem(item);
		assertThat(cdItem!=null,is(true));
		assertThat(cdItem>0,is(true));
		
		item = itemService.obtenerItemPorCodigo(cdItem);
		assertThat(item!=null,is(true));
		assertThat(item.getCdItem(),is(cdItem));
		assertThat(item.getCdSeccion(),is(item.getCdSeccion()));
		assertThat(item.getChTipo(),is(ParamConstants.ITM_CH_TIPO));
		assertThat(item.getNbFuente(),is(ParamConstants.ITM_NB_FUENTE));
		assertThat(item.getNbMask(),is(ParamConstants.ITM_NB_MASK));
		assertThat(item.getNbNombreVar(),is(ParamConstants.ITM_NB_NOMBRE_VAR));
		assertThat(item.getNbValorVar(),is(ParamConstants.ITM_NB_VALOR_VAR));
		assertThat(item.getNuAlinear(),is(ParamConstants.ITM_NU_ALINEAR));
		assertThat(item.getNuAncho(),is(ParamConstants.ITM_NU_ANCHO));
		assertThat(item.getNuCoorX(),is(ParamConstants.ITM_NU_COOR_X));
		assertThat(item.getNuCoorY(),is(ParamConstants.ITM_NU_COOR_Y));
		assertThat(item.getNuIdxCliente(),is(ParamConstants.ITM_NU_IDX_CLIENTE));
		assertThat(item.getNuScX(),is(ParamConstants.ITM_NU_SC_X));
		assertThat(item.getNuScY(),is(ParamConstants.ITM_NU_SC_Y));
		assertThat(item.getStActivo(),is(ParamConstants.ITM_ST_ACTIVO));
				
		ItemStamp itemUpd = new ItemStamp();
		BeanUtils.copyProperties(item, itemUpd);
		itemUpd.setNbNombreVar(ParamConstants.ITM_NB_NOMBRE_VAR_2);
		itemUpd.setNbValorVar(ParamConstants.ITM_NB_VALOR_VAR_2);
		itemUpd.setNuCoorX(ParamConstants.ITM_NU_COOR_X_2);
		itemUpd.setNuCoorY(ParamConstants.ITM_NU_COOR_Y_2);
		Integer result = itemService.actualizarItem(itemUpd);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		item = itemService.obtenerItemPorCodigo(cdItem);
		assertThat(item!=null,is(true));
		assertThat(item.getCdItem(),is(cdItem));
		assertThat(item.getCdSeccion(),is(item.getCdSeccion()));
		assertThat(item.getChTipo(),is(ParamConstants.ITM_CH_TIPO));
		assertThat(item.getNbFuente(),is(ParamConstants.ITM_NB_FUENTE));
		assertThat(item.getNbMask(),is(ParamConstants.ITM_NB_MASK));
		assertThat(item.getNbNombreVar(),is(ParamConstants.ITM_NB_NOMBRE_VAR_2));
		assertThat(item.getNbValorVar(),is(ParamConstants.ITM_NB_VALOR_VAR_2));
		assertThat(item.getNuAlinear(),is(ParamConstants.ITM_NU_ALINEAR));
		assertThat(item.getNuAncho(),is(ParamConstants.ITM_NU_ANCHO));
		assertThat(item.getNuCoorX(),is(ParamConstants.ITM_NU_COOR_X_2));
		assertThat(item.getNuCoorY(),is(ParamConstants.ITM_NU_COOR_Y_2));
		assertThat(item.getNuIdxCliente(),is(ParamConstants.ITM_NU_IDX_CLIENTE));
		assertThat(item.getNuScX(),is(ParamConstants.ITM_NU_SC_X));
		assertThat(item.getNuScY(),is(ParamConstants.ITM_NU_SC_Y));
		assertThat(item.getStActivo(),is(ParamConstants.ITM_ST_ACTIVO));
		
		Integer resultEliminar = itemService.eliminarItem(cdItem);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		item = itemService.obtenerItemPorCodigo(cdItem);
		assertThat(item==null,is(true));
	}
	
	private void validarItems(List<ItemStamp> items){
		assertThat(items!=null,is(true));
		assertThat(items.size()>0,is(true));		
		for(ItemStamp item : items){
			validarItem(item);
		}
	}
	
	private void validarItem(ItemStamp item){
		assertThat(item!=null,is(true));
		assertThat(item.getCdSeccion()!=null,is(true));
		assertThat(StringUtils.isEmpty(item.getChTipo()),is(false));
		if(Constants.TYPE_TEXT.equals(item.getChTipo())){
			assertThat(StringUtils.isEmpty(item.getNbNombreVar()),is(false));
			assertThat(StringUtils.isEmpty(item.getNbFuente()),is(false));
			assertThat(item.getNuAlinear()!=null,is(true));
			//assertThat(StringUtils.isEmpty(item.getNbMask()),is(false));
		}else if(Constants.TYPE_IMAGE.equals(item.getChTipo()) || Constants.TYPE_IMAGE_BASE64.equals(item.getChTipo())){
			assertThat(item.getNuScX()!=null,is(true));
			assertThat(item.getNuScY()!=null,is(true));
			assertThat(item.getNuAncho()!=null,is(true));
		}else if(Constants.TYPE_IMAGE_HUELLA.equals(item.getChTipo())){
			assertThat(item.getNuScX()!=null,is(true));
			assertThat(item.getNuScY()!=null,is(true));
			assertThat(item.getNuAncho()!=null,is(true));
			assertThat(item.getNuIdxCliente()!=null,is(true));
		}else if(Constants.TYPE_BAR_CODE.equals(item.getChTipo())){
			assertThat(item.getNuScX()!=null,is(true));
			assertThat(item.getNuScY()!=null,is(true));
			assertThat(item.getNuAncho()!=null,is(true));
			assertThat(item.getNuIdxCliente()!=null,is(true));
		}
		assertThat(item.getNuCoorX()!=null,is(true));
		assertThat(item.getNuCoorY()!=null,is(true));
		assertThat(StringUtils.isEmpty(item.getStActivo()),is(false));		
	}
}
