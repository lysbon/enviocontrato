package com.bbva.cuentas.mock.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Util;
import com.bbva.cuentas.dto.ClienteHuellaDetalle;
import com.bbva.cuentas.dto.ClienteHuellaDigital;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.HuellaDigitalService;

@Service
public class HuellaDigitalServiceImpl implements HuellaDigitalService{

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${flag.huella.digital.service}")
	private boolean flagHuellaDigitalService;
	@Value("${huella.digital.endpoint.get}")
	private String endpointGET;
	@Value("${huella.digital.endpoint.post}")
	private String endpointPOST;
	@Value("${service.init.ruta.recursos}")
	private String recursos;
	
	public ClienteHuellaDigital getClienteHuellaDigital(String tipoDocumento, String nroDocumento){
		RestTemplate restTemplate = new RestTemplate();
		ClienteHuellaDigital cliente = restTemplate.getForObject(endpointGET, ClienteHuellaDigital.class);
		logger.debug(cliente.toString());
        return cliente;
	}
	
	public String formatoIdTrx(String trx){
		return StringUtils.replace(trx, "#", "-");
	}
	
	public ClienteHuellaDigital postClienteHuellaDigital(String tipoDocumento, String nroDocumento, String trx){
		RestTemplate restTemplate = new RestTemplate();
		JSONObject cliente = new JSONObject();
		cliente.put("tipoDocumento",tipoDocumento);
		cliente.put("numeroDocumento",nroDocumento);
		cliente.put("idTransaccion",formatoIdTrx(trx));
		JSONObject request = new JSONObject();
		request.put("cliente", cliente);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(),headers);		
		ClienteHuellaDigital clienteHuella = restTemplate.postForObject(endpointPOST,entity,ClienteHuellaDigital.class);
        return clienteHuella;
	}
	
	public List<ClienteHuellaDigital> obtenerHuellasDigitalesRequest(List<Cliente> clientes) {
		List<ClienteHuellaDigital> listaHuellas = new ArrayList<ClienteHuellaDigital>();
    	for(Cliente cl : clientes){
    		ClienteHuellaDigital huella = postClienteHuellaDigital(
    				cl.getTipoDocumento(),
    				cl.getNroDocumento(),
    				cl.getIdTrxHuellaDigital());
    		listaHuellas.add(huella);
    	}
    	return listaHuellas;
	}
	
	public List<ClienteHuellaDigital> obtenerHuellasDigitalesTest() {
		List<ClienteHuellaDigital> listaHuellas = new ArrayList<ClienteHuellaDigital>();
		String minuciaPrueba = "Esta es una minucia de prueba. Esta es una minucia de prueba";
		try {
			ClienteHuellaDigital huella0 = new ClienteHuellaDigital();
			ClienteHuellaDetalle det0 = new ClienteHuellaDetalle();
			String nombre0 = "derecho0.jpg";
			det0.setIdDedo(Constants.ID_DEDO_DER);
			det0.setImgIndiceDerecho(Util.fileToBase64(recursos+File.separatorChar+"img"+File.separatorChar+nombre0));
			det0.setTemplateIndiceDerecho(minuciaPrueba);
			huella0.setCliente(det0);

			ClienteHuellaDigital huella1 = new ClienteHuellaDigital();
			ClienteHuellaDetalle det1 = new ClienteHuellaDetalle();
			String nombre1 = "izquierdo1.jpg";
			det1.setIdDedo(Constants.ID_DEDO_IZQ);
			det1.setImgIndiceIzquierdo(Util.fileToBase64(recursos+File.separatorChar+"img"+File.separatorChar+nombre1));
			det1.setTemplateIndiceIzquierdo(minuciaPrueba);
			huella1.setCliente(det1);

			ClienteHuellaDigital huella2 = new ClienteHuellaDigital();
			ClienteHuellaDetalle det2 = new ClienteHuellaDetalle();
			String nombre2 = "derecho2.jpg";
			det2.setIdDedo(Constants.VACIO);//PRUEBA DE DATA ERRADA
			det2.setImgIndiceDerecho(Util.fileToBase64(recursos+File.separatorChar+"img"+File.separatorChar+nombre2));
			det2.setTemplateIndiceDerecho(minuciaPrueba);
			huella2.setCliente(det2);

			listaHuellas.add(huella0);
			listaHuellas.add(huella1);
			listaHuellas.add(huella2);

		} catch (Exception e) {
			logger.error(e);
		}
		return listaHuellas;
	}
	
	public List<ClienteHuellaDigital> obtenerHuellasDigitales(List<Cliente> clientes){
		if(flagHuellaDigitalService){
			return obtenerHuellasDigitalesRequest(clientes);
		}else{
			return obtenerHuellasDigitalesTest();
		}
	}
	
	public ClienteHuellaDigital obtenerHuellasDigitales(Cliente cl) throws ServiceException {
		ClienteHuellaDigital huella = postClienteHuellaDigital(cl.getTipoDocumento(), cl.getNroDocumento(),
				cl.getIdTrxHuellaDigital());

		return huella;
	}
	
	@Override
	public Map<String,String> obtenerHuellasDigitalesBase64(List<Cliente> clientes) throws ServiceException {
		Map<String,String> huellasBase64 = new HashMap<String,String>();
		List<ClienteHuellaDigital> clienteHuellas = obtenerHuellasDigitales(clientes);

		int indiceCliente=0;
		for(ClienteHuellaDigital huellaCliente : clienteHuellas){
			
			if(Constants.ID_DEDO_IZQ.equals(huellaCliente.getCliente().getIdDedo()) &&
				!StringUtils.isEmpty(huellaCliente.getCliente().getImgIndiceIzquierdo())){
				huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceIzquierdo());
				huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceIzquierdo());
			}else if(Constants.ID_DEDO_DER.equals(huellaCliente.getCliente().getIdDedo()) &&
					!StringUtils.isEmpty(huellaCliente.getCliente().getImgIndiceDerecho())){
				huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceDerecho());
				huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceDerecho());
			}else{
				if(huellaCliente.getCliente().getImgIndiceDerecho()!=null){
					huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceDerecho());
					huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceDerecho());
				}else if(huellaCliente.getCliente().getImgIndiceIzquierdo()!=null){
					huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente, huellaCliente.getCliente().getImgIndiceIzquierdo());
					huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceIzquierdo());
				}
			}
			indiceCliente++;
		}
		return huellasBase64;
	}
	
}
