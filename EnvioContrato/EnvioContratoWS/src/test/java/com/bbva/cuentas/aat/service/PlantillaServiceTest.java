package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class PlantillaServiceTest {
	
	@Autowired
	private PlantillaService plantillaService;
	@Value("${service.init.ruta.recursos}")
	private String rutaFormatos;
	
	@Test
	public void obtenerPlantillasTest() throws ServiceException{		
		List<Plantilla> plantillas = plantillaService.obtenerPlantillas();
		validarListaPlantillas(plantillas);
	}
	
	@Test
	public void obtenerPlantillasTestPorEnv() throws ServiceException{		
		List<Plantilla> plantillas = plantillaService.obtenerPlantillas();
		validarListaPlantillas(plantillas);
	}
	
	@Test
	public void obtenerPlantillaPorCodigo() throws ServiceException{		
		Plantilla plantilla = plantillaService.obtenerPlantillaPorCodigo(ParamConstants.TPL_CD_PLANTILLA_CONSULTA);
		validarPlantilla(plantilla);
	}
	
	public Long guardarPlantillaTest() throws ServiceException{
		Plantilla plantilla = new Plantilla();
		plantilla.setStActivo(Constants.ST_ACTIVO);
		plantilla.setStCondicion(Constants.PLANTILLA_FIJA);
		plantilla.setStFileunico(Constants.ST_ACTIVO);
		plantilla.setStEnvCorreo(Constants.ST_ACTIVO);
		plantilla.setStTipoConstr(Constants.TIPO_CONSTR_PDF);
		plantilla.setStSigned(Constants.ST_ACTIVO);
		plantilla.setNbNombreFormato(ParamConstants.TPL_NOMBRE_FORMATO);
		plantilla.setNbTipoFileunico(ParamConstants.TPL_TIPO_FILEUNICO);
		plantilla.setNbNombreCorreo(ParamConstants.TPL_NOMBRE_CORREO);
		plantilla.setChTipoPlantilla(ParamConstants.TPL_TIPO_PLANTILLA);
		Long cdPlantilla = plantillaService.guardarPlantilla(plantilla);
		assertThat(cdPlantilla!=null,is(true));
		assertThat(cdPlantilla>0,is(true));
		return cdPlantilla;
	}
	
	@Test
	public void actualizarPlantillaTest() throws ServiceException{
		Plantilla plantilla = new Plantilla();
		plantilla.setCdPlantilla(ParamConstants.TPL_CD_PLANTILLA_TRX);
		plantilla.setStActivo(Constants.ST_ACTIVO);
		plantilla.setStCondicion(Constants.PLANTILLA_FIJA);
		plantilla.setStFileunico(Constants.ST_ACTIVO);
		plantilla.setStEnvCorreo(Constants.ST_ACTIVO);
		plantilla.setStTipoConstr(Constants.TIPO_CONSTR_PDF);
		plantilla.setStSigned(Constants.ST_ACTIVO);
		plantilla.setNbNombreFormato(ParamConstants.TPL_NOMBRE_FORMATO);
		plantilla.setNbTipoFileunico(ParamConstants.TPL_TIPO_FILEUNICO);
		plantilla.setNbNombreCorreo(ParamConstants.TPL_NOMBRE_CORREO);
		plantilla.setChTipoPlantilla(ParamConstants.TPL_TIPO_PLANTILLA);
		Integer result = plantillaService.actualizarPlantilla(plantilla);
		assertThat(result!=null,is(true));
	}
	
	//@Test
	public void eliminarPlantillaTest() throws ServiceException{
		Integer result = plantillaService.eliminarPlantilla(ParamConstants.TPL_CD_PLANTILLA_TRX);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		Long cdPlantilla = guardarPlantillaTest();
		Plantilla plantilla = plantillaService.obtenerPlantillaPorCodigo(cdPlantilla);
		assertThat(plantilla!=null,is(true));
		assertThat(plantilla.getCdPlantilla(),is(cdPlantilla));
		assertThat(plantilla.getStActivo(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStFileunico(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStEnvCorreo(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStSigned(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStCondicion(),is(Constants.PLANTILLA_FIJA));
		assertThat(plantilla.getStTipoConstr(),is(Constants.TIPO_CONSTR_PDF));
		assertThat(plantilla.getNbTipoFileunico(),is(ParamConstants.TPL_TIPO_FILEUNICO));
		assertThat(plantilla.getNbNombreFormato(),is(ParamConstants.TPL_NOMBRE_FORMATO));
		assertThat(plantilla.getNbNombreCorreo(),is(ParamConstants.TPL_NOMBRE_CORREO));
		assertThat(plantilla.getChTipoPlantilla(),is(ParamConstants.TPL_TIPO_PLANTILLA));
		
		Plantilla plantillaUpd = new Plantilla();
		BeanUtils.copyProperties(plantilla, plantillaUpd);
		plantillaUpd.setStActivo(Constants.ST_INACTIVO);
		plantillaUpd.setStFileunico(Constants.ST_INACTIVO);
		plantillaUpd.setNbNombreCorreo(ParamConstants.TPL_NOMBRE_CORREO_2);
		plantillaUpd.setNbNombreFormato(ParamConstants.TPL_NOMBRE_FORMATO_2);
		plantillaUpd.setNbTipoFileunico(ParamConstants.TPL_TIPO_FILEUNICO_2);
		Integer result = plantillaService.actualizarPlantilla(plantillaUpd);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		plantillaService.actualizarPDFdesdeArchivo(plantillaUpd.getCdPlantilla(), rutaFormatos+ParamConstants.TPL_PDF_RUTA1);
		byte[] pdf = plantillaService.obtenerPDF(plantillaUpd.getCdPlantilla(),null);

		validarPDF(pdf);
		
		plantilla = plantillaService.obtenerPlantillaPorCodigo(cdPlantilla);
		assertThat(plantilla!=null,is(true));
		assertThat(plantilla.getCdPlantilla(),is(cdPlantilla));
		assertThat(plantilla.getStActivo(),is(Constants.ST_INACTIVO));
		assertThat(plantilla.getStFileunico(),is(Constants.ST_INACTIVO));
		assertThat(plantilla.getStEnvCorreo(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStSigned(),is(Constants.ST_ACTIVO));
		assertThat(plantilla.getStCondicion(),is(Constants.PLANTILLA_FIJA));
		assertThat(plantilla.getStTipoConstr(),is(Constants.TIPO_CONSTR_PDF));
		assertThat(plantilla.getNbTipoFileunico(),is(ParamConstants.TPL_TIPO_FILEUNICO_2));
		assertThat(plantilla.getNbNombreFormato(),is(ParamConstants.TPL_NOMBRE_FORMATO_2));
		assertThat(plantilla.getNbNombreCorreo(),is(ParamConstants.TPL_NOMBRE_CORREO_2));
		assertThat(plantilla.getChTipoPlantilla(),is(ParamConstants.TPL_TIPO_PLANTILLA));
		
		Integer resultEliminar = plantillaService.eliminarPlantilla(cdPlantilla);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		plantilla = plantillaService.obtenerPlantillaPorCodigo(cdPlantilla);
		assertThat(plantilla==null,is(true));		
	}
	
	private void validarListaPlantillas(List<Plantilla> plantillas){
		assertThat(plantillas!=null,is(true));
		assertThat(plantillas.size()>0,is(true));		
		for(Plantilla plantilla : plantillas){
			validarPlantilla(plantilla);
		}
	}
	
	private void validarPlantilla(Plantilla plantilla){
		assertThat(plantilla!=null,is(true));
		assertThat(StringUtils.isEmpty(plantilla.getNbNombreCorreo()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStActivo()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStCondicion()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStEnvCorreo()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStSigned()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStTipoConstr()),is(false));
		assertThat(StringUtils.isEmpty(plantilla.getStFileunico()),is(false));
		if(Constants.ST_ACTIVO.equals(plantilla.getStFileunico())) {
			assertThat(StringUtils.isEmpty(plantilla.getNbTipoFileunico()),is(false));
		}
		assertThat(plantilla.getCdPlantilla()!=null,is(true));
	}
	
	private void validarPDF(byte[] pdf){
		assertThat(pdf!=null,is(true));
		assertThat(pdf.length>0,is(true));
	}
	
}
