package com.bbva.cuentas.datasource;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;

@Component
public class DataFiller {
	
	@Autowired
	KitService kitService;
	@Autowired
	PlantillaService plantillaService;
	@Autowired
	private SeccionService seccionService;
	
	@Value("${service.init.ruta.recursos}")
	private String rutaFormatos;
	
	public RequestDTO prepararDataContrato() throws ServiceException {
		Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.KIT_CD_ENV,ParamConstants.KIT_CD_CODIGO_DEFAULT);
		return prepararDataContrato(kit);
	}
	
	public RequestDTO prepararDataContrato(Kit kit) throws ServiceException {
		return prepararDataContrato(kit,"DO-RE-MI-FA-SOL-LA-SI");
	}
	
	public RequestDTO prepararDataContratoNombreVar(Kit kit) throws ServiceException {
		return prepararDataContrato(kit,StringUtils.EMPTY);
	}

	public RequestDTO prepararDataContrato(Kit kit,String value) throws ServiceException {
		List<Plantilla> plantillas = plantillaService.obtenerPlantillasPorKit(kit.getCdKit());
		return prepararDataContrato(kit.getCdCodigo(),plantillas,value);
	}
	
	public RequestDTO prepararDataContrato(Kit kit,List<Plantilla> plantillas) throws ServiceException {
		return prepararDataContrato(kit.getCdCodigo(),plantillas,StringUtils.EMPTY);
	}

	public RequestDTO prepararDataContrato(String idContrato,List<Plantilla> plantillas,String value) throws ServiceException {
		RequestDTO firmaContrato = new RequestDTO();
		Cliente cliente = new Cliente();
		cliente.setCodigoCentral("90016480");
		cliente.setTipoDocumento("L");
		cliente.setNroDocumento("43590095");
		cliente.setEmail("CARLOS.SOSA.NAVARRO@BBVA.COM");
		cliente.setIdTrxHuellaDigital("50220#138098");
		Cliente cliente1 = new Cliente();
		cliente1.setCodigoCentral("90016481");
		cliente1.setTipoDocumento("L");
		cliente1.setNroDocumento("43590096");
		cliente1.setEmail("CARLOS.SOSA.NAVARRO@BBVA.COM");
		cliente1.setIdTrxHuellaDigital("50220#138098");
		Cliente cliente2 = new Cliente();
		cliente2.setCodigoCentral("90016482");
		cliente2.setTipoDocumento("L");
		cliente2.setNroDocumento("43590097");
		cliente2.setEmail("CARLOS.SOSA.NAVARRO@BBVA.COM");
		cliente2.setIdTrxHuellaDigital("50220#138098");

		firmaContrato.getListaClientes().add(cliente);
		firmaContrato.getListaClientes().add(cliente1);
		firmaContrato.getListaClientes().add(cliente2);

		firmaContrato.setNumeroContrato("00110130230209049292");
		firmaContrato.setOficinaGestora("0130");
		firmaContrato.setProcedencia(ParamConstants.PROCEDENCIA_NACAR);
		firmaContrato.setIdContrato(idContrato);
		
		Map<String,String> parametros = new HashMap<String,String>();
		if(firmaContrato!=null){
			if(plantillas!= null){
				for(Plantilla ptl : plantillas){
					List<ItemSection> listaSections = seccionService.obtenerSeccionesItemPorPlantilla(ptl.getCdPlantilla(),null);
					if(listaSections!=null){
						for(ItemSection s : listaSections){
							List<ItemStamp> listaParams = s.getListItems();
							for(ItemStamp p : listaParams){
								if(Constants.TYPE_IMAGE_BASE64.equals(p.getChTipo())) {
									String imageBase64;
									try {
										imageBase64 = Util.fileToBase64(rutaFormatos+File.separatorChar+"img"+File.separatorChar+p.getNbNombreVar()+".jpg");
									} catch (Exception e) {
										throw new ServiceException(e.getMessage());
									}
									parametros.put(p.getNbNombreVar(),imageBase64);
								}else {
									if(!parametros.containsKey(p.getNbNombreVar())){
										if(StringUtils.isEmpty(p.getNbValorVar())){
											if(StringUtils.isEmpty(value)) {
												parametros.put(p.getNbNombreVar(),p.getNbNombreVar());
											}else {
												parametros.put(p.getNbNombreVar(),value);
											}
										}else{
											parametros.put(p.getNbNombreVar(),p.getNbValorVar());
										}
									}
								}	
							}
						}
					}	
				}
			}
		}
		ItemContrato ic = null;
		Iterator<Entry<String,String>> it = parametros.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String,String> pair = (Map.Entry<String,String>)it.next();
	        ic = new ItemContrato();
	        ic.setLabel(pair.getKey());
			ic.setValue(pair.getValue());
			firmaContrato.getListaItems().add(ic);
	    }
		return firmaContrato;
	}

	
	public RequestDTO prepararDataPlantilla(String nombrePlantilla) throws ServiceException {
		Plantilla plantilla = plantillaService.obtenerPlantillaPorNombre(nombrePlantilla);
		List<Plantilla> listaPlantillas = new ArrayList<Plantilla>();
		listaPlantillas.add(plantilla);
		return prepararDataContrato(plantilla.getNbNombreFormato(),listaPlantillas,StringUtils.EMPTY);
	}

}
