package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class FlagServiceTest {
	
	@Autowired
	private FlagService flagService;
		
	@Test
	public void obtenerItemPorCodigoTest() throws ServiceException{		
		List<Flag> flags = flagService.obtenerFlags();
		validarFlags(flags);
		Flag flag = flagService.obtenerFlagPorCodigo(flags.get(0).getCdFlag());
		validarFlag(flag);
	}
	
	@Test
	public void obtenerFlagPorTipo() throws ServiceException{
		List<Flag> flags = flagService.obtenerFlags();
		Flag aux = flags.get(0);
		List<Flag> flagsTipo = flagService.obtenerFlagsPorTipo(aux.getChTipo());
		validarFlagsPorTipo(flagsTipo,aux.getChTipo());
	}
	
	public Flag nuevoFlag(){
		Flag flag = new Flag();
		flag.setChTipo(ParamConstants.FLG_CH_TIPO);
		flag.setNbCondicionNombre(ParamConstants.FLG_NB_CONDICION_NOMBRE);
		flag.setNbCondicionValor(ParamConstants.FLG_NB_CONDICION_VALOR);
		flag.setNuOrden(ParamConstants.FLG_NU_ORDER);
		return flag;
	}
	
	@Test
	public void guardarEliminarFlagTest() throws ServiceException{
		Flag flag = nuevoFlag();		
		Long cdFlag = flagService.guardarFlag(flag);
		assertThat(cdFlag!=null,is(true));
		assertThat(cdFlag>0,is(true));		
		Integer result = flagService.eliminarFlag(cdFlag);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void actualizarFlagTest() throws ServiceException{
		List<Flag> flags = flagService.obtenerFlags();
		if(flags!=null && flags.size()>0){
			Flag flag = flags.get(0);
			Flag flagUpd = new Flag();
			BeanUtils.copyProperties(flag, flagUpd);
			flagUpd.setNbCondicionNombre(ParamConstants.FLG_NB_CONDICION_NOMBRE_2);
			flagUpd.setNbCondicionValor(ParamConstants.FLG_NB_CONDICION_VALOR_2);
			Integer result = flagService.actualizarFlag(flagUpd);
			assertThat(result!=null,is(true));
			
			Integer resultBack = flagService.actualizarFlag(flag);
			assertThat(resultBack!=null,is(true));
		}
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		Flag flag = nuevoFlag();
		Long cdFlag = flagService.guardarFlag(flag);
		assertThat(cdFlag!=null,is(true));
		assertThat(cdFlag>0,is(true));
		
		flag = flagService.obtenerFlagPorCodigo(cdFlag);
		assertThat(flag!=null,is(true));
		assertThat(flag.getCdFlag(),is(cdFlag));
		assertThat(flag.getChTipo(),is(ParamConstants.FLG_CH_TIPO));
		assertThat(flag.getNbCondicionNombre(),is(ParamConstants.FLG_NB_CONDICION_NOMBRE));
		assertThat(flag.getNbCondicionValor(),is(ParamConstants.FLG_NB_CONDICION_VALOR));
		assertThat(flag.getNuOrden(),is(ParamConstants.FLG_NU_ORDER));
		
		Flag flagUpd = new Flag();
		BeanUtils.copyProperties(flag, flagUpd);
		flagUpd.setNbCondicionNombre(ParamConstants.FLG_NB_CONDICION_NOMBRE_2);
		flagUpd.setNbCondicionValor(ParamConstants.FLG_NB_CONDICION_VALOR_2);
		Integer result = flagService.actualizarFlag(flagUpd);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		flag = flagService.obtenerFlagPorCodigo(cdFlag);
		assertThat(flag!=null,is(true));
		assertThat(flag.getCdFlag(),is(cdFlag));
		assertThat(flag.getChTipo(),is(ParamConstants.FLG_CH_TIPO));
		assertThat(flag.getNbCondicionNombre(),is(ParamConstants.FLG_NB_CONDICION_NOMBRE_2));
		assertThat(flag.getNbCondicionValor(),is(ParamConstants.FLG_NB_CONDICION_VALOR_2));
		assertThat(flag.getNuOrden(),is(ParamConstants.FLG_NU_ORDER));
		
		Integer resultEliminar = flagService.eliminarFlag(cdFlag);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		flag = flagService.obtenerFlagPorCodigo(cdFlag);
		assertThat(flag==null,is(true));
	}
	
	private void validarFlagsPorTipo(List<Flag> flagsTipo,String tipo) {
		validarFlags(flagsTipo);
		for(Flag flag : flagsTipo){
			assertThat(flag.getChTipo(),is(tipo));
		}
	}
	
	private void validarFlags(List<Flag> flags){
		assertThat(flags!=null,is(true));
		assertThat(flags.size()>0,is(true));
		for(Flag flag : flags){
			validarFlag(flag);
		}
	}
	
	private void validarFlag(Flag flag){
		assertThat(flag!=null,is(true));
		assertThat(flag.getCdFlag()!=null,is(true));
		assertThat(StringUtils.isEmpty(flag.getChTipo()),is(false));
		assertThat(StringUtils.isEmpty(flag.getNbCondicionNombre()),is(false));
		assertThat(StringUtils.isEmpty(flag.getNbCondicionValor()),is(false));
		assertThat(flag.getNuOrden()!=null,is(true));
	}
	
}
