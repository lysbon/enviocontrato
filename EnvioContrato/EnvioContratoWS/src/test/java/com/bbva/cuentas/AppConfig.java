package com.bbva.cuentas;

import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;

@Configuration
@ContextConfiguration(locations = {"classpath:WEB-INF/applicationContext.xml"})
public class AppConfig {

}
