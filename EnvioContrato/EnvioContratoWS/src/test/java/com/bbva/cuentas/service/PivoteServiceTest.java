package com.bbva.cuentas.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.datasource.DataFiller;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.util.Constants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class PivoteServiceTest {

	@Autowired
	private PivoteService service;
	@Autowired
	DataFiller filler;
	
	@Test
	public void dummy_Test() throws Exception{
		assertThat(StringUtils.EMPTY.isEmpty(), is(true));
	}

	@Test
	public void sincronoAsincrono() throws Exception{
		RequestDTO firmaContrato = filler.prepararDataContrato();
		firmaContrato.setAsync(Constants.ST_FALSE);
		
		Long startTime,endTime,timeSync,timeASync;
		
		Thread.sleep(1000);
		
		startTime= System.nanoTime();
		firmaContrato.setAsync(Constants.ST_FALSE);
		service.process(firmaContrato);
		endTime = System.nanoTime();
		timeSync = endTime - startTime;

		startTime = System.nanoTime();
		firmaContrato.setAsync(Constants.ST_TRUE);
		service.process(firmaContrato);
		endTime = System.nanoTime();
		timeASync = endTime - startTime;
		
		assertThat(timeASync<timeSync,is(true));
		
	}
		
}
