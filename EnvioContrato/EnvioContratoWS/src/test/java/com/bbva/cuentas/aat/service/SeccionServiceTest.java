package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class SeccionServiceTest {
	
	@Autowired
	private SeccionService seccionService;
		
	@Test
	public void obtenerSeccionPorCodigoTest() throws ServiceException{		
		List<ItemSection> secciones = seccionService.obtenerSecciones();
		validarSecciones(secciones);
		ItemSection seccion = seccionService.obtenerSeccionPorId(secciones.get(0).getCdSeccion());
		validarSeccion(seccion);
	}
	
	@Test
	public void obtenerSeccionesPorPlantillaTest() throws ServiceException{		
		List<ItemSection> secciones = seccionService.obtenerSeccionesPorPlantilla(ParamConstants.SEC_CD_PLANTILLA_CONSULTA,null);
		validarSecciones(secciones);
	}
	
	@Test
	public void guardarEliminarSeccionTest() throws ServiceException{
		ItemSection seccion = new ItemSection();
		seccion.setCdPlantilla(ParamConstants.SEC_CD_PLANTILLA_TRX);
		seccion.setNuPagina(ParamConstants.SEC_NUMPAG);
		Long cdseccion = seccionService.guardarSeccion(seccion);
		assertThat(cdseccion!=null,is(true));
		assertThat(cdseccion>0,is(true));
		
		Integer result = seccionService.eliminarSeccion(cdseccion);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void actualizarSeccionTest() throws ServiceException{
		List<ItemSection> secciones = seccionService.obtenerSecciones();
		if(secciones!=null && secciones.size()>0){
			ItemSection ref = secciones.get(0);
			ItemSection seccion = new ItemSection();
			seccion.setCdSeccion(ref.getCdSeccion());
			seccion.setCdPlantilla(ParamConstants.SEC_CD_PLANTILLA_TRX);
			seccion.setNuPagina(ParamConstants.SEC_NUMPAG);
			Integer result     = seccionService.actualizarSeccion(seccion);
			Integer resultBack = seccionService.actualizarSeccion(ref);
			assertThat(result!=null,is(true));
			assertThat(resultBack!=null,is(true));
		}
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		ItemSection seccion = new ItemSection();
		seccion.setCdPlantilla(ParamConstants.SEC_CD_PLANTILLA_TRX);
		seccion.setNuPagina(ParamConstants.SEC_NUMPAG);
		Long cdseccion = seccionService.guardarSeccion(seccion);
		assertThat(cdseccion!=null,is(true));
		assertThat(cdseccion>0,is(true));
		
		seccion = seccionService.obtenerSeccionPorId(cdseccion);
		assertThat(seccion!=null,is(true));
		assertThat(seccion.getCdSeccion(),is(cdseccion));
		assertThat(seccion.getCdPlantilla(),is(ParamConstants.SEC_CD_PLANTILLA_TRX));
		assertThat(seccion.getNuPagina(),is(ParamConstants.SEC_NUMPAG));
				
		ItemSection seccionUpd = new ItemSection();
		BeanUtils.copyProperties(seccion, seccionUpd);
		seccionUpd.setNuPagina(ParamConstants.SEC_NUMPAG_2);
		Integer result = seccionService.actualizarSeccion(seccionUpd);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		seccion = seccionService.obtenerSeccionPorId(cdseccion);
		assertThat(seccion!=null,is(true));
		assertThat(seccion.getCdSeccion(),is(cdseccion));
		assertThat(seccion.getCdPlantilla(),is(ParamConstants.SEC_CD_PLANTILLA_TRX));
		assertThat(seccion.getNuPagina(),is(ParamConstants.SEC_NUMPAG_2));
		
		Integer resultEliminar = seccionService.eliminarSeccion(cdseccion);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		seccion = seccionService.obtenerSeccionPorId(cdseccion);
		assertThat(seccion==null,is(true));

	}
	
	private void validarSecciones(List<ItemSection> seccions){
		assertThat(seccions!=null,is(true));
		assertThat(seccions.size()>0,is(true));		
		for(ItemSection seccion : seccions){
			validarSeccion(seccion);
		}
	}
	
	private void validarSeccion(ItemSection seccion){
		assertThat(seccion!=null,is(true));
		assertThat(seccion.getCdSeccion()!=0,is(true));
		assertThat(seccion.getNuPagina()!=null,is(true));
		assertThat(seccion.getCdPlantilla()!=null,is(true));
	}
	
}
