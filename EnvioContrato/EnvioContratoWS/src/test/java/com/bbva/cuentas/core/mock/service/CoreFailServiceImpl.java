package com.bbva.cuentas.core.mock.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.FileUnicoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.impl.CoreServiceImpl;

@Service
public class CoreFailServiceImpl extends CoreServiceImpl implements CoreService{

	public void process(RequestDTO request,Kit kit) throws ServiceException {
		List<ClienteDTO> listaClientes = null;
		try {
			backupTrama(request,kit);
			if (kit != null && request.getListaClientes() != null) {
				Map<String, Object> parametros = util.toMap(request);
				if(huellaActivo(kit))
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes()));
				
				listaClientes = processGenerarContratos(request,kit,parametros);
				
				for (int i = 0; i < listaClientes.size(); i++) {
					ClienteDTO cliente = listaClientes.get(i);
					
					System.out.println("Cliente "+i);
					System.out.println(cliente.getCliente().getNombreApellido());
					System.out.println(cliente.getCliente().getCodigoCentral());
					
					Map<String, Object> parametrosCliente = 
						util.toMap(request,cliente.getCliente());
					processMail(request,kit,cliente,parametrosCliente);
					processFileUnico(request,kit,cliente);
				}
			}
		} catch (ServiceException e) {
			guardarTramaNivelGeneral(request,kit,null,e);
		} catch (Exception e) {
			logger.error(e);
		} finally {
			depurarArchivos(listaClientes);
		}
	}
	
	public void processMail(RequestDTO request, Kit kit,ClienteDTO cliente,Map<String, Object> parametros) {
		if ("90016480".equals(cliente.getCliente().getCodigoCentral()) ||
			"90016482".equals(cliente.getCliente().getCodigoCentral())) {
			try {
				throw new EnvioMailException("Fail Intent");
			} catch (ServiceException e) {
				guardarTramaNivelCliente(request,kit,cliente,e);
			}
		}
	}
	
	public void processFileUnico(RequestDTO request, Kit kit,ClienteDTO cliente) {
		if ("90016481".equals(cliente.getCliente().getCodigoCentral()) ||
			"90016482".equals(cliente.getCliente().getCodigoCentral())) {
			try {
				throw new FileUnicoException("Fail Intent");
			} catch (ServiceException e) {
				guardarTramaNivelCliente(request,kit,cliente,e);
			}
		}
	}
	
}
