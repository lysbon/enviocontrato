package com.bbva.cuentas.util;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class ConfigureServiceTest {
	
	@Test
	public void test() throws Exception{
		
		Date date = new java.text.SimpleDateFormat("yyyy-MM-dd").parse("2017-10-11");
		String str = new java.text.SimpleDateFormat("dd/MM/yyyy").format(date);
		
		System.out.println(str);
		
	}
	
	
}
