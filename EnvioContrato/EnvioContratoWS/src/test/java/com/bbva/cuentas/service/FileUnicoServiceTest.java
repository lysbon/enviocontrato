package com.bbva.cuentas.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class FileUnicoServiceTest {

	@Autowired
	CoreService coreService;
	@Autowired
	KitCoreService kitCoreService;
	@Autowired
	GenerarContratosService generarContratosService;
	@Autowired
	KitService kitService;
	@Autowired
	HuellaDigitalService huellaService;
	@Autowired
	PlantillaService plantillaService;
	@Autowired
	private SeccionService seccionService;
	@Autowired
	private FileUnicoService fileUnicoService;
	@Autowired
	private Util util;
	
	@Value("${fileunico.sftp.ruta}")
	private String rutaFileunico;
	
	@Test
	public void dummy_Test() throws Exception{
		assertThat("".isEmpty(), is(true));
	}
	
	@Test
	public void fileunico_Test() throws Exception{
		Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.KIT_CD_ENV, ParamConstants.KIT_CD_CODIGO_DEFAULT);
		assertThat(kit!=null,is(true));
		RequestDTO firmaContrato = prepararDataContrato(kit);
		List<ClienteDTO> clientes = null;
		ClienteDTO cliente = null;
		try {
			Map<String,Object> parametrosGeneral = util.toMap(firmaContrato);
			parametrosGeneral.putAll(huellaService.obtenerHuellasDigitalesBase64(firmaContrato.getListaClientes()));
			kitCoreService.cargarPlantillasContratoCliente(kit);
			clientes = coreService.procesarRutas(kit, firmaContrato);
			cliente = Util.obtenerTitular(clientes);
			
			generarContratosService.generarContratos(
					kit.getListTemplates(), 
					parametrosGeneral, null,
					cliente,clientes,firmaContrato);
			
			Map<String,Object> parametrosCliente = util.toMap(firmaContrato,cliente.getCliente());
			
			generarContratosService.generarContratos(
					kit.getListTemplatesCliente(), 
					parametrosCliente, null,
					cliente,clientes,firmaContrato);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(Plantilla tpl : kit.getListTemplates()){
			if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && 
				Constants.ST_ACTIVO.equals(tpl.getStFileunico())&&
				tpl.isValida()){
				tpl.setStSigned(Constants.ST_INACTIVO);
			}
		}
		for(Plantilla tpl : kit.getListTemplatesCliente()){
			if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && 
				Constants.ST_ACTIVO.equals(tpl.getStFileunico())&&
				tpl.isValida()){
				
			}
		}
		
		fileUnicoService.procesarArchivosWithTemp(kit, firmaContrato, cliente);
		
		for(Plantilla tpl : kit.getListTemplates()){
			if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && 
				Constants.ST_ACTIVO.equals(tpl.getStFileunico())&&
				tpl.isValida()){
				List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
				for(ConfigurationPath cfg : listCfg) {
					String basename = FilenameUtils.getBaseName(cfg.getRutaSalidaPDF());
					String pathPDF = FilenameUtils.concat(rutaFileunico, cliente.getCliente().getCodigoCentral()+basename+Constants.EXT_PDF);
					String pathCSV = FilenameUtils.concat(rutaFileunico, cliente.getCliente().getCodigoCentral()+basename+Constants.EXT_CSV);
					File filePDF = new File(pathPDF);
					File fileCSV = new File(pathCSV);
					assertThat(filePDF.exists(), is(true));
					assertThat(fileCSV.exists(), is(true));
				}
			}
		}
		for(Plantilla tpl : kit.getListTemplatesCliente()){
			if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && 
				Constants.ST_ACTIVO.equals(tpl.getStFileunico())&&
				tpl.isValida()){
				List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
				for(ConfigurationPath cfg : listCfg) {
					String basename = FilenameUtils.getBaseName(cfg.getRutaSalidaPDF());
					String pathPDF = FilenameUtils.concat(rutaFileunico, basename+Constants.EXT_PDF);
					String pathCSV = FilenameUtils.concat(rutaFileunico, basename+Constants.EXT_CSV);
					File filePDF = new File(pathPDF);
					File fileCSV = new File(pathCSV);
					assertThat(filePDF.exists(), is(true));
					assertThat(fileCSV.exists(), is(true));
				}
			}
		}
		
	}
	
	private RequestDTO prepararDataContrato(Kit kit) throws Exception {
		RequestDTO firmaContrato = new RequestDTO();
		Cliente cliente = new Cliente();
		cliente.setCodigoCentral("90016480");
		cliente.setTipoDocumento("L");
		cliente.setNroDocumento("43590095");
		cliente.setEmail("CARLOS.SOSA.NAVARRO@BBVA.COM");
		cliente.setIdTrxHuellaDigital("50220#138098");
		firmaContrato.getListaClientes().add(0, cliente);
		firmaContrato.setNumeroContrato("00110130230209049292");
		firmaContrato.setOficinaGestora("0130");
		firmaContrato.setProcedencia(ParamConstants.PROCEDENCIA_NACAR);
		firmaContrato.setIdContrato(kit.getCdCodigo());
		
		Map<String,String> parametros = new HashMap<String,String>();
		if(firmaContrato!=null){
			List<Plantilla> plantillas = plantillaService.obtenerPlantillasPorKit(kit.getCdKit());
			if(plantillas!= null){
				for(Plantilla ptl : plantillas){
					List<ItemSection> listaSections = seccionService.obtenerSeccionesItemPorPlantilla(ptl.getCdPlantilla(),null);
					if(listaSections!=null){
						for(ItemSection s : listaSections){
							List<ItemStamp> listaParams = s.getListItems();
							for(ItemStamp p : listaParams){
								if(!parametros.containsKey(p.getNbNombreVar()))
									parametros.put(p.getNbNombreVar(),"LALALA");
							}
						}
					}	
				}
			}
		}
		ItemContrato ic = null;
		Iterator<Entry<String,String>> it = parametros.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String,String> pair = (Map.Entry<String,String>)it.next();
	        ic = new ItemContrato();
	        ic.setLabel(pair.getKey());
			ic.setValue(pair.getValue());
			firmaContrato.getListaItems().add(ic);
	    }
		return firmaContrato;
	}
	
}
