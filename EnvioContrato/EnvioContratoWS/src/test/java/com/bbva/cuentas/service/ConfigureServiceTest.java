package com.bbva.cuentas.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.firmarcontratos.Configuration;
import com.bbva.cuentas.firmarcontratos.ConfigurationItem;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class ConfigureServiceTest {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	ConfigureService configureService;
	@Autowired
	KitService kitService;
	@Autowired
	EnviromentService envService;
	
	@Value("${service.init.ruta.recursos}")
	private String rutaRecursos;
	@Value("${ruta.formatoPDF}")
	private String rutaFormatos;
	
	@Test
	public void testBase64() throws Exception{
		Configuration configuration = new Configuration();
		ConfigurationItem item = new ConfigurationItem();
		item.setRuta(FilenameUtils.concat(rutaRecursos,"pdf"+File.separatorChar+"dae"+File.separatorChar+"E3362.pdf"));
		item.setTipo(Constants.TIPO_BASE64_GET);
		logger.debug(item.getRuta());
		configuration.getItem().add(item);
		String result = configureService.process(configuration);
		assertThat(result!=null,is(true));
		
		item.setTipo(Constants.TIPO_BASE64_PUT);
		item.setLlave(result);
		item.setRuta(FilenameUtils.concat(rutaRecursos,"output.pdf"));
		
		result = configureService.process(configuration);
		assertThat(result!=null,is(true));
		File file = new File(item.getRuta());
		assertThat(file.exists(),is(true));
		
		item.setTipo(Constants.TIPO_DEL);
		result = configureService.process(configuration);
		File fileDel = new File(item.getRuta());
		assertThat(fileDel.exists(),is(false));
		
	}
	
	@Test
	public void testActualizarCorreo() throws Exception{
		
		Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.KIT_CD_ENV,ParamConstants.KIT_CD_CODIGO_DEFAULT);
		String template = kitService.obtenerMailTemplate(kit.getCdKit(),ParamConstants.KIT_CD_ENV);
		assertThat(StringUtils.isEmpty(template),is(false));
		Integer pos = StringUtils.indexOf(template,"Vivienda");
		assertThat(pos>0,is(false));
		System.out.println(pos);
		
		Configuration configuration = new Configuration();
		ConfigurationItem item = new ConfigurationItem();
		item.setRuta(FilenameUtils.concat(rutaRecursos,"strtpl"+File.separatorChar+"BG111.html"));
		item.setTipo(Constants.TIPO_HTML);
		item.setLlave(ParamConstants.KIT_CD_CODIGO_DEFAULT);
		logger.debug(item.getRuta());
		configuration.getItem().add(item);
		String result = configureService.process(configuration);
		assertThat(result!=null,is(true));
		System.out.println(result);
		
		template = kitService.obtenerMailTemplate(kit.getCdKit(),ParamConstants.KIT_CD_ENV);
		assertThat(StringUtils.isEmpty(template),is(false));
		pos = StringUtils.indexOf(template,"Vivienda");
		assertThat(pos>0,is(true));
		System.out.println(pos);
		
		Configuration configurationEnvKit = new Configuration();
		ConfigurationItem itemEnv = new ConfigurationItem();
		itemEnv.setTipo(Constants.LLAVE_NAME);
		itemEnv.setLlave(ParamConstants.KIT_CD_ENV+"");
		ConfigurationItem itemKit = new ConfigurationItem();
		itemKit.setRuta(FilenameUtils.concat(rutaRecursos,"strtpl"+File.separatorChar+"BG351.html"));
		itemKit.setTipo(Constants.TIPO_HTML);
		itemKit.setLlave(ParamConstants.KIT_CD_CODIGO_DEFAULT);
		configurationEnvKit.getItem().add(itemEnv);
		configurationEnvKit.getItem().add(itemKit);
		result = configureService.process(configurationEnvKit);
		assertThat(result!=null,is(true));
		System.out.println(result);
		
		List<Enviroment> envs = envService.obtenerEnvs();
		for(Enviroment env : envs) {
			template = kitService.obtenerMailTemplate(kit.getCdKit(),env.getCdEnv());
			assertThat(StringUtils.isEmpty(template),is(false));
			pos = StringUtils.indexOf(template,"Ganadora");
			System.out.println(env.getCdEnv()+"-"+pos);
			if(env.getCdEnv().intValue() == ParamConstants.KIT_CD_ENV.intValue()) {
				assertThat(pos>0,is(true));
			}else {
				assertThat(pos>0,is(false));
			}
		}
		
	}
	
}
