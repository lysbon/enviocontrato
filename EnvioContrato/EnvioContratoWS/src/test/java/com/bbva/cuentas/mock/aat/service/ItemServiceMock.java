package com.bbva.cuentas.mock.aat.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.service.impl.ItemServiceImpl;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class ItemServiceMock extends ItemServiceImpl{
	
	@Override
	public List<ItemStamp> obtenerItemsPorSeccion(Long cdSeccion,Date fecHoraCreacion)
			throws ServiceException {
		try {
			List<ItemStamp> tmp = new ArrayList<ItemStamp>();
			if(fecHoraCreacion!=null){
				Timestamp fecFiltro = new Timestamp(fecHoraCreacion.getTime());
				tmp = itemHistDao.obtenerItemsPorSeccionAscendente(cdSeccion,fecFiltro);
			}else{
				tmp = itemDao.obtenerItemsPorSeccionAscendente(cdSeccion);
			}
			List<String> fuentes = new ArrayList<String>();
			fuentes.add(Constants.FONT_COD_ARIAL+StringUtils.EMPTY      +Constants.FONT_COD_9);
			fuentes.add(Constants.FONT_COD_ARIAL+Constants.FONT_COD_BOLD+Constants.FONT_COD_9);
			fuentes.add(Constants.FONT_COD_ARIAL+StringUtils.EMPTY      +Constants.FONT_COD_10);
			fuentes.add(Constants.FONT_COD_ARIAL+Constants.FONT_COD_BOLD+Constants.FONT_COD_10);
			fuentes.add(Constants.FONT_COD_ARIAL+StringUtils.EMPTY      +Constants.FONT_COD_11);
			fuentes.add(Constants.FONT_COD_ARIAL+Constants.FONT_COD_BOLD+Constants.FONT_COD_11);
			
			fuentes.add(Constants.FONT_COD_CALIBRI+StringUtils.EMPTY      +Constants.FONT_COD_9);
			fuentes.add(Constants.FONT_COD_CALIBRI+Constants.FONT_COD_BOLD+Constants.FONT_COD_9);
			fuentes.add(Constants.FONT_COD_CALIBRI+StringUtils.EMPTY      +Constants.FONT_COD_10);
			fuentes.add(Constants.FONT_COD_CALIBRI+Constants.FONT_COD_BOLD+Constants.FONT_COD_10);
			fuentes.add(Constants.FONT_COD_CALIBRI+StringUtils.EMPTY      +Constants.FONT_COD_11);
			fuentes.add(Constants.FONT_COD_CALIBRI+Constants.FONT_COD_BOLD+Constants.FONT_COD_11);
			
			fuentes.add(Constants.FONT_COD_COURIER_NEW+StringUtils.EMPTY      +Constants.FONT_COD_9);
			fuentes.add(Constants.FONT_COD_COURIER_NEW+StringUtils.EMPTY      +Constants.FONT_COD_10);
			fuentes.add(Constants.FONT_COD_COURIER_NEW+StringUtils.EMPTY      +Constants.FONT_COD_11);
			
			int i = 0;
			for(ItemStamp st : tmp){
				if(i>=fuentes.size())
					i = 0;
				String fuente = fuentes.get(i);
				st.setNbFuente(fuente);
				st.setNbValorVar(fuente);
				i++;
			}
			return tmp;
		} catch (DaoException e) {
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener secciones de plantilla.");
		}
	}
	
}
