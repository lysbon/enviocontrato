package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class EnviromentServiceTest {
	
	@Autowired
	private EnviromentService envService;
		
	@Test
	public void obtenerItemPorCodigoTest() throws ServiceException{		
		List<Enviroment> envs = envService.obtenerEnvs();
		validarEnvs(envs);
		Enviroment env = envService.obtenerEnvPorCodigo(envs.get(0).getCdEnv());
		validarEnv(env);
	}
	
	@Test
	public void obtenerEnvPorTipo() throws ServiceException{
		List<Enviroment> envs = envService.obtenerEnvs();
		Enviroment aux = envs.get(0);
		List<Enviroment> envsTipo = envService.obtenerEnvPorTipo(aux.getChTipo());
		validarEnvs(envsTipo);
	}
	
	public Enviroment nuevoEnv(){
		Enviroment env = new Enviroment();
		env.setChTipo(ParamConstants.ENV_CH_TIPO);
		env.setNbDescripcion(ParamConstants.ENV_NB_DESCRIPCION);
		env.setNbNombre(ParamConstants.ENV_NB_NOMBRE);
		env.setStActivo(ParamConstants.ENV_ST_ACTIVO);
		return env;
	}
	
	@Test
	public void guardarEliminarEnvTest() throws ServiceException{
		Enviroment env = nuevoEnv();		
		Integer cdEnv = envService.guardarEnv(env);
		assertThat(cdEnv!=null,is(true));
		assertThat(cdEnv>0,is(true));		
		Integer result = envService.eliminarEnv(cdEnv);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void actualizarEnvTest() throws ServiceException{
		List<Enviroment> envs = envService.obtenerEnvs();
		if(envs!=null && envs.size()>0){
			Enviroment env = envs.get(0);
			env.setChTipo(ParamConstants.ENV_CH_TIPO_2);
			env.setNbNombre(ParamConstants.ENV_NB_NOMBRE_2);
			Integer result = envService.actualizarEnv(env);
			assertThat(result!=null,is(true));
		}
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		Enviroment env = nuevoEnv();
		Integer cdEnv = envService.guardarEnv(env);
		assertThat(cdEnv!=null,is(true));
		assertThat(cdEnv>0,is(true));
		
		env = envService.obtenerEnvPorCodigo(cdEnv);
		assertThat(env!=null,is(true));
		assertThat(env.getCdEnv(),is(cdEnv));
		assertThat(env.getChTipo(),is(ParamConstants.ENV_CH_TIPO));
		assertThat(env.getNbNombre(),is(ParamConstants.ENV_NB_NOMBRE));
		assertThat(env.getNbDescripcion(),is(ParamConstants.ENV_NB_DESCRIPCION));
		assertThat(env.getStActivo(),is(ParamConstants.ENV_ST_ACTIVO));
		
		Enviroment envUpd = new Enviroment();
		BeanUtils.copyProperties(env, envUpd);
		envUpd.setChTipo(ParamConstants.ENV_CH_TIPO_2);
		envUpd.setNbNombre(ParamConstants.ENV_NB_NOMBRE_2);
		Integer result = envService.actualizarEnv(envUpd);
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		env = envService.obtenerEnvPorCodigo(cdEnv);
		assertThat(env!=null,is(true));
		assertThat(env.getCdEnv(),is(cdEnv));
		assertThat(env.getChTipo(),is(ParamConstants.ENV_CH_TIPO_2));
		assertThat(env.getNbNombre(),is(ParamConstants.ENV_NB_NOMBRE_2));
		assertThat(env.getNbDescripcion(),is(ParamConstants.ENV_NB_DESCRIPCION));
		assertThat(env.getStActivo(),is(ParamConstants.ENV_ST_ACTIVO));
		
		Integer resultEliminar = envService.eliminarEnv(cdEnv);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		env = envService.obtenerEnvPorCodigo(cdEnv);
		assertThat(env==null,is(true));
	}
	
	private void validarEnvs(List<Enviroment> envs){
		assertThat(envs!=null,is(true));
		assertThat(envs.size()>0,is(true));		
		for(Enviroment env : envs){
			validarEnv(env);
		}
	}
	
	private void validarEnv(Enviroment env){
		assertThat(env!=null,is(true));
		assertThat(env.getCdEnv()!=null,is(true));
		assertThat(StringUtils.isEmpty(env.getChTipo()),is(false));
		assertThat(StringUtils.isEmpty(env.getNbNombre()),is(false));
		assertThat(StringUtils.isEmpty(env.getNbDescripcion()),is(false));
		assertThat(StringUtils.isEmpty(env.getStActivo()),is(false));	
	}
	
}
