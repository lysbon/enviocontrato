package com.bbva.cuentas.scripts;
//package com.bbva.cuentas.service;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.is;
//
//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.Reader;
//import java.io.StringReader;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.commons.lang3.StringEscapeUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.stringtemplate.v4.ST;
//
//import com.bbva.cuentas.aat.service.KitService;
//import com.bbva.cuentas.aat.service.PlantillaService;
//import com.bbva.cuentas.bean.Flag;
//import com.bbva.cuentas.bean.Kit;
//import com.bbva.cuentas.bean.Plantilla;
//import com.bbva.cuentas.util.Constants;
//import com.bbva.cuentas.util.Result;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("/app-config-nodb.xml")
//public class KitServiceTest {
//
//	private static final Logger logger = LoggerFactory.getLogger(KitServiceTest.class);
//	
//	@Autowired
//	private KitService kitService;
//	@Autowired
//	private PlantillaService plantillaService;
//	
//	
//	@SuppressWarnings("unchecked")
//	@Test
//	public void descargaPlantillas(){
//		Integer cdEnv = 1;
//		List<Kit> listaKits = null;
//		List<Kit> listaKitsFiltered = new ArrayList<Kit>();
//		//boolean bandera = true;
//		String[] patterns = {"SSS","SSN","SNS","SNN","NSS","NSN","NNS","NNN"};
//		
//		try {
//			Result result = kitService.obtenerKits(cdEnv);
//			listaKits = (List<Kit>)result.getEntities();
//			for(Kit kit : listaKits){
//				Result tmpPlantilla = plantillaService.obtenerPlantillasAsociadas(kit.getCdKit());
//				List<Plantilla> plantillas = (List<Plantilla>)tmpPlantilla.getEntities();
//				for(Plantilla plantilla : plantillas){
//					//long cdPlantillaLPD = 64l;//TEST
//					long cdPlantillaLPD = 47l;//PRD
//					if(plantilla.getCdPlantilla().longValue() == cdPlantillaLPD && plantilla.isAsociado()){
//						listaKitsFiltered.add(kit);
//						Result resCorreo = kitService.obtenerMailTemplate(kit.getCdKit());
//						kit.setLbCorreoPlantilla((String)resCorreo.getEntity());
//					}
//				}
//			}
//			for(Kit kit : listaKitsFiltered){
//				logger.info("kit("+kit.getCdCodigo()+")");
//				
//				String tplCorreo = kit.getLbCorreoPlantilla();
//				
//				//PRIMER PARRAFO
//				String primerParrafo1     = "Asimismo, en cumplimiento del art&iacute;culo 41&#x00b0 del Reglamento de Transparencia";
//				String primerParrafo2     = "Asimismo, en cumplimiento del art&iacute;culo 41&#x00b0; del Reglamento de Transparencia";
//				String nuevoPrimerParrafo = "Asimismo, en cumplimiento del art&iacute;culo 49&#x00b0; del Reglamento de Gesti&oacute;n de Conducta de Mercado del Sistema Financiero";
//				assertThat(StringUtils.contains(tplCorreo,primerParrafo1) || StringUtils.contains(tplCorreo,primerParrafo2),is(true));
//				
//				Integer repeticiones = cuantasVeces(tplCorreo, primerParrafo1)+cuantasVeces(tplCorreo, primerParrafo2);
//				assertThat(repeticiones==4,is(true));
//				tplCorreo = StringUtils.replace(tplCorreo, primerParrafo1, nuevoPrimerParrafo);
//				tplCorreo = StringUtils.replace(tplCorreo, primerParrafo2, nuevoPrimerParrafo);
//				
//				//SEGUNDO PARRAFO
//				String segundoParrafo1_3_1     = "30 d&iacute;as calendario."; //se repite con la de abajo
//				String segundoParrafo1_3_2     = "30 d&iacute;as calendario";
//				String nuevoSegundoParrafo1_3  = "30 d&iacute;as calendario.<if(FCFG0P33)> Adicionalmente adjuntamos la constancia de tu aceptaci&oacute;n para el tratamiento de tus datos personales.<endif>";
//				assertThat(StringUtils.contains(tplCorreo,segundoParrafo1_3_2),is(true));
//				
//				if(cuantasVeces(tplCorreo, segundoParrafo1_3_1)<2){
//					System.out.println("error2: "+kit.getCdCodigo());
//				}
//				
//				repeticiones = cuantasVeces(tplCorreo, segundoParrafo1_3_2);
//				assertThat(repeticiones==2,is(true));
//				tplCorreo = StringUtils.replace(tplCorreo, segundoParrafo1_3_1, segundoParrafo1_3_2);
//				tplCorreo = StringUtils.replace(tplCorreo, segundoParrafo1_3_2, nuevoSegundoParrafo1_3);
//				
//				String segundoParrafo2        = "para Personas Naturales.";
//				String nuevoSegundoParrafo2   = "para Personas Naturales.<if(FCFG0P33)> Adicionalmente adjuntamos la constancia de tu aceptaci&oacute;n para el tratamiento de tus datos personales.<endif>";
//				assertThat(StringUtils.contains(tplCorreo,segundoParrafo2),is(true));
//				repeticiones = cuantasVeces(tplCorreo, segundoParrafo2);
//				assertThat(repeticiones==1,is(true));
//				tplCorreo = StringUtils.replace(tplCorreo, segundoParrafo2, nuevoSegundoParrafo2);
//				
//				String segundoParrafo4        = "Cartilla de Informaci&oacute;n.";
//				String nuevoSegundoParrafo4   = "Cartilla de Informaci&oacute;n..<if(FCFG0P33)> Adicionalmente adjuntamos la constancia de tu aceptaci&oacute;n para el tratamiento de tus datos personales.<endif>";
//				assertThat(StringUtils.contains(tplCorreo,segundoParrafo4),is(true));
//				repeticiones = cuantasVeces(tplCorreo, segundoParrafo4);
//				assertThat(repeticiones==1,is(true));
//				tplCorreo = StringUtils.replace(tplCorreo, segundoParrafo4, nuevoSegundoParrafo4);
//				
//				
//				//TERCER PARRAFO
//				String tercerParrafo      = " penalidades, ingresa";
//				String nuevoTercerParrafo = " penalidades, <if(FCFG0P33)> y c&oacute;mo ejercer tus derechos sobre el tratamiento de datos <endif> ingresa";
//				assertThat(StringUtils.contains(tplCorreo,tercerParrafo),is(true));
//				repeticiones = cuantasVeces(tplCorreo, tercerParrafo);
//				assertThat(repeticiones==1,is(true));
//				tplCorreo = StringUtils.replace(tplCorreo, tercerParrafo, nuevoTercerParrafo);
//				
//				//writeFile(tplCorreo,"d:\\tpl\\"+kit.getCdCodigo()+".html");
//				
//				for(int i=0;i<patterns.length;i++){
//					Map<String,String> parametros = obtenerParametros(patterns[i]);
//					List<Flag> flags = obtenerFlagsCorreo();
//					/*
//					if(bandera){
//						System.out.println(buscarPosicion(tplCorreo,35,785));
//						System.out.println(buscarPosicion(tplCorreo,35,917));
//						System.out.println(buscarPosicion(tplCorreo,42,582));
//						System.out.println(buscarPosicion(tplCorreo,48,707));
//						System.out.println(buscarPosicion(tplCorreo,48,839));
//						System.out.println(buscarPosicion(tplCorreo,54,503));
//						System.out.println(buscarPosicion(tplCorreo,66,221));
//						bandera = false;
//					}*/
//					
//					String correoSalida = procesarPlantillaStrTpl(tplCorreo,parametros,flags,true);
//					
//					//presentacion funcional
//					correoSalida = StringUtils.replace(correoSalida, "cid:image1.jpg", "image1.jpg");
//					correoSalida = StringUtils.replace(correoSalida, "cid:image2.jpg", "image2.png");
//					
//					//writeFile(correoSalida,"d:\\tpl\\"+kit.getCdCodigo()+"_"+obtenerSufijo(patterns[i])+".html");
//				}
//				
//			}
//			
//		} catch (Exception e) {
//			logger.debug(e.getMessage());
//		}
//	}
//	
//	public String buscarPosicion(String texto,int line, int position) throws IOException{
//		Reader inputString = new StringReader(texto);
//	    BufferedReader br = new BufferedReader(inputString);
//	    String strLine;
//	    //Loop through and check if a header or footer line, if not
//	    //equate a substring to a temp variable and print it....
//	    int i = 1;
//	    while ((strLine = br.readLine()) != null)   {
//	    	if(i==(line)){
//	    		return strLine.substring(position,position+1);
//	    	}
//	    	i++;
//	    }
//	    //Close the input stream
//	    inputString.close();
//	    return "";
//	}
//	
//	public List<Flag> obtenerFlagsCorreo(){
//		List<Flag> flags = new ArrayList<Flag>();
//		Flag flag = new Flag();
//		flag.setNbCondicionNombre("CFG0|P15");
//		flag.setNbCondicionValor("1");
//		flags.add(flag);
//		flag = new Flag();
//		flag.setNbCondicionNombre("CFG0|P18");
//		flag.setNbCondicionValor("I");
//		flags.add(flag);
//		flag = new Flag();
//		flag.setNbCondicionNombre("CFG0|P33");
//		flag.setNbCondicionValor("S");
//		flags.add(flag);
//		return flags;
//	}
//	public String obtenerSufijo(String pattern){
//		switch (pattern) {
//		case "SSS" : return "LPD_FATCA1_DAE";
//		case "SSN" : return "____FATCA1_DAE";
//		case "SNS" : return "LPD FATCA1____";
//		case "SNN" : return "____FATCA1____";
//		case "NSS" : return "LPD________DAE";
//		case "NSN" : return "___________DAE";
//		case "NNS" : return "LPD___________";
//		case "NNN" : return "______________";
//		default:
//			return "";
//		}
//	}
//	public Map<String,String> obtenerParametros(String pattern){
//		Map<String,String> map = new HashMap<String, String>();
//		map.put("CFG0|P05", "VITOR FERREIRA GOMES");
//		map.put("CFG0|P23", "2017-05-26");
//		map.put("CFG0|P10", "SOLES");
//		map.put("CFG0|P01", "00110191410200324870");
//		map.put("CFG0|P16", "01119100020032487041");
//		map.put("CFG0|P15", (pattern.substring(0,1).equals("S")?"1":"0"));
//		map.put("CFG0|P18", (pattern.substring(1,2).equals("S")?"I":"III"));
//		map.put("CFG0|P33", (pattern.substring(2,3).equals("S")?"S":"N"));
//		return map;
//	}
//	
//	public String procesarPlantillaStrTpl(
//    		String plantilla,
//    		Map<String,String> parametrosCliente,
//    		List<Flag> listFlags,
//    		boolean escape){
//		ST st = new ST(plantilla);
//		if(listFlags!=null){
//			for(Flag flag:listFlags){
//				if(parametrosCliente.containsKey(flag.getNbCondicionNombre()) &&
//						parametrosCliente.get(flag.getNbCondicionNombre()).equals(flag.getNbCondicionValor())){
//					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),true);
//				}else{
//					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),false);
//				}
//			}
//		}
//		for (Map.Entry<String, String> entry : parametrosCliente.entrySet()){
//		    String value = entry.getValue();
//			if(escape){
//				value = StringEscapeUtils.escapeHtml4(value);
//			}
//			st.add(StringUtils.replace(entry.getKey(),"|",""),value);
//		}
//		return st.render();
//	}
//	
//	private void writeFile(String text, String filename) throws FileNotFoundException{
//		try (PrintWriter out = new PrintWriter(filename)) {
//		    out.println(text);
//		}
//	}
//	
//	private int cuantasVeces(String first,String second){
//		int ind = 0;
//		int cnt = 0;
//		while (true) {
//		    int pos = first.indexOf(second, ind);
//		    if (pos < 0) break;
//		    cnt++;
//		    ind = pos + 1; // Advance by second.length() to avoid self repetitions
//		}
//		return cnt;
//	}
//}
