package com.bbva.cuentas.scripts;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class KitServiceDescargarPlantillas {

	private static final Logger logger = LoggerFactory.getLogger(KitServiceDescargarPlantillas.class);
	
	@Autowired
	private KitService kitService;
	@Autowired
	private EnviromentService envService;
	
	@SuppressWarnings("unchecked")
	@Test
	public void descargaPlantillas() throws ServiceException{
		List<Kit> listaKits = null;
		List<Enviroment> listaEnvs = null;
		Map<String,Kit> kitsFiltered = new HashMap<String,Kit>();
		try {
			Result result = envService.obtenerEnvs();
			listaEnvs= (List<Enviroment>)result.getEntities();
			for(Enviroment env : listaEnvs) {
				result = kitService.obtenerKits(env.getCdEnv());
				listaKits = (List<Kit>)result.getEntities();
				for(Kit kit : listaKits){
					kitsFiltered.put(kit.getCdCodigo(),kit);
					Result resCorreo = kitService.obtenerMailTemplate(env.getCdEnv(),kit.getCdKit());
					kit.getGrupoKit().setLbCorreoPlantilla((String)resCorreo.getEntity());
				}
				for (Map.Entry<String, Kit> entry : kitsFiltered.entrySet()){
					Kit kit = entry.getValue();
					String tplCorreo = kit.getGrupoKit().getLbCorreoPlantilla();
					writeFile(tplCorreo,"d:\\tmp\\backupCalidad\\html\\"+kit.getCdCodigo()+".html");
				}
			}			
		} catch (Exception e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e.getMessage());
		}
	}
	
	private void writeFile(String text, String filename) throws FileNotFoundException{
		try {
			PrintWriter out = new PrintWriter(filename) ;		
		    out.println(text);
		}finally {
			
		}
	}
	
}
