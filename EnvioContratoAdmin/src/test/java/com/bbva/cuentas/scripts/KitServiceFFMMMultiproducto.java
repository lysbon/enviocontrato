package com.bbva.cuentas.scripts;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.stringtemplate.v4.ST;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-nodb.xml")
public class KitServiceFFMMMultiproducto {

	private static final Logger logger = LoggerFactory.getLogger(KitServiceFFMMMultiproducto.class);
	
	@Autowired
	private KitService kitService;
	@Autowired
	private EnviromentService envService;
	
	@SuppressWarnings("unchecked")
	@Test
	public void descargaPlantillas() throws ServiceException{
		Kit kit = null;
		List<Enviroment> listaEnvs = null;
		try {
			Result result = envService.obtenerEnvs();
			listaEnvs= (List<Enviroment>)result.getEntities();
			for(Enviroment env : listaEnvs) {
				if(env.getNbNombre().equals("BXI")) {
					result = kitService.obtenerKitPorCodigo(env.getCdEnv(),"FFMM");
					kit = (Kit)result.getEntity();
					Result resCorreo = kitService.obtenerMailTemplate(env.getCdEnv(),kit.getCdKit());
					kit.getGrupoKit().setLbCorreoPlantilla((String)resCorreo.getEntity());
					String tplCorreo = kit.getGrupoKit().getLbCorreoPlantilla();
					writeFile(tplCorreo,"d:\\tmp\\backupTest\\html\\"+kit.getCdCodigo()+".html");
					
					Map<String,String> parametros = obtenerParametros();
					String correoSalida = procesarPlantillaStrTpl(tplCorreo,parametros);
					correoSalida = StringUtils.replace(correoSalida, "cid:image1.jpg", "image1.jpg");
					correoSalida = StringUtils.replace(correoSalida, "cid:image2.jpg", "image2.jpg");					
					writeFile(correoSalida,"d:\\tmp\\backupTest\\html\\"+kit.getCdCodigo()+"_output.html");
				}
			}			
		} catch (Exception e) {
			logger.debug(e.getMessage());
			throw new ServiceException(e.getMessage());
		}
	}
	
	private void writeFile(String text, String filename) throws FileNotFoundException{
		try (PrintWriter out = new PrintWriter(filename)) {
		    out.println(text);
		}
	}
	
	public Map<String,String> obtenerParametros(){
		Map<String,String> map = new HashMap<String, String>();
		map.put("CFG0|P01", "VITOR FERREIRA GOMES");
		
		map.put("CFG0|P03", "0011");
		map.put("CFG0|P04", "FFMM BBVA SOLES 2");
		map.put("CFG0|P05", "S/400.00");
		map.put("CFG0|P06", "Cuenta Independencia ***5247");
		
		map.put("M800121CFG0|P03", "FFMM BBVA SOLES 2");
		map.put("M800121CFG0|P04", "0011-0199-3423543646");
		map.put("M800121CFG0|P05", "S/400.00");
		map.put("M800121CFG0|P06", "Cuenta Independencia ***5247");
		
		map.put("M800131CFG0|P03", "FFMM BBVA SOLES 4");
		map.put("M800131CFG0|P04", "0011-0199-3423543646");
		map.put("M800131CFG0|P05", "S/400.00");
		map.put("M800131CFG0|P06", "Cuenta Independencia ***5247");
		
		map.put("CFG0|P23", "2017-05-26");
		map.put("CFG0|P10", "SOLES");
			
		return map;
	}
	
	public String procesarPlantillaStrTpl(
    		String plantilla,
    		Map<String,String> parametrosCliente){
		ST st = new ST(plantilla);
		for (Map.Entry<String, String> entry : parametrosCliente.entrySet()){
		    String value = entry.getValue();
			st.add(StringUtils.replace(entry.getKey(),"|",""),value);
		}
		return st.render();
	}
	
}
