var plantillaSelected=null;
var seccionSelected=null;
var itemSelected=null;
var flagSelected = null;

$(document).ready(function() {
    
	$('.navTpl').addClass('active');
	
	var baseUrl = $('#baseUrl').val();
	
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		var target = $(e.target).attr("href");
		if(target == '#pdf'){
			cambiarTabBotones('.btn-pdf');
		}else if(target == '#home'){
			cambiarTabBotones('.btn-home');
		}else if(target == '#des'){
			cambiarTabBotones('.btn-des');
		}
	});
	
	actualizarListaPlantillas();
	bindListaPlantillas();
	bindTablaSecciones();
	bindTablaItems();
	bindTablaFlagsPlantilla();
	bindTablaFlags();
	//modal de carga secciones
	bindTablaItemsCarga();
	
	$("#selTipoCond").change(function() {
		if($(this).val()==$('#txtCondFijo').val()){
			$('#tabFlags').hide();
		}else{
			$('#tabFlags').show();
		}
	});
	
	$("#cbEnvCorreo").change(function() {
		if ($('#cbEnvCorreo').prop('checked')) {
			$('#txtNombreCorreo').prop("disabled", false);
		}else{
			$('#txtNombreCorreo').prop("disabled", true);
		}
	});
	
	$('#btnGuardarPlantilla').on( 'click', function () {
		guardarPlantilla();
	});
	
	$('#btnGuardarSeccion').on( 'click', function () {
		guardarSeccion();
	});
	
	$('#btnGuardarFlagPlantilla').on( 'click', function () {
		guardarFlagPlantilla();
	});
	
	$('#btnActPDF').on('click',function () {
		var data = new FormData();
		$.each($('#txtFileNamePDF')[0].files, function(i, file) {
			//file.name = file.name.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
			data.append('file', file);
		});
		data.append('name','prueba');
		$.ajax({
		    url: baseUrl+'/uploadFile',
		    data: data,
		    cache: false,
		    contentType: false,
		    processData: false,
		    type: 'POST',
		    success: function(dataFile){
		        $('#pdf .rptaSetupPDF').html('Se subi&oacute; el archivo correctamente. Procesando ...');
		        $('#pdf .rptaSetupPDF').removeClass('hidden');
		        $('#btnActPDF').text('Procesando...');
		        $('#btnActPDF').addClass('disabled');
		        var cdPlantilla = $('#txtPDFPlantilla').val();
		        $.ajax({
				    url: baseUrl+'/rest/pdf/'+cdPlantilla,
				    data: dataFile,
				    cache: false,
				    contentType: false,
				    processData: false,
				    type: 'POST',
				    success: function(data){
				    	$('#pdf .rptaSetupPDF').addClass('hidden');
				    	$('#btnActPDF').text('Actualizar PDF');
				    	$('#pdf object').remove();
				    	actualizarPDF(cdPlantilla);
				    }
		        });
		    }
		});
	});
	
	$('#btnSubir').on('click',function () {
		
		var data = new FormData();
		$.each($('#txtFileNameSVG')[0].files, function(i, file) {
		    data.append('file', file);
		});
		data.append('name','prueba');
		
		$('#btnSubir').text('Procesando...');
        $('#btnSubir').addClass('disabled');
        
		$.ajax({
		    url: baseUrl+'/uploadFile',
		    data: data,
		    cache: false,
		    contentType: false,
		    processData: false,
		    type: 'POST',
		    success: function(dataFile){
		        $('#modalPlantilla .rptaSetup').html('Se subi&oacute; el archivo correctamente. Procesando ...');
		        $('#modalPlantilla .rptaSetup').removeClass('hidden');
		        $('#modalPlantilla .fileSetup').addClass('hidden');
		        var cdSeccion = $('#txtSeccion').val();
		        $.ajax({
				    url: baseUrl+'/rest/svg/'+cdSeccion,
				    data: dataFile,
				    cache: false,
				    contentType: false,
				    processData: false,
				    type: 'POST',
				    success: function(data){
				    	$('#modalPlantilla .rptaSetup').addClass('hidden');
			    		$('#btnAplicar').removeClass('hidden');
			    		$('#btnScript').removeClass('hidden');
			    		$('#btnSubir').addClass('hidden');
			    		$('#scriptActualizar').val(data.scriptUpdate);
			    		$('#scriptAlta').val(data.scriptInsert);
			    		$('#modalPlantilla .selectSetup').removeClass('hidden');
			    		$('#modalPlantilla .panelScripts').removeClass('hidden');
			    		$('#modalPlantilla .btn-car2').removeClass('hidden');
			    		
			    		destruirTabla('#tblItemsCarga');
			    		
			    		$.each(data.items, function( index, value ){
			    			var detailClass = '';
			    			if(value.stSVGCarga == 'M'){
			    				detailClass = 'details-control';
			    			}
			    			var htmlRow=
			    				'<tr data-id="'+index+'" class="selectedItem">'+
			    				'<td><input id="cbItem'+index+'" type="checkbox" value="'+index+'">'+
			    				'</td>'+
			    				'<td>'+value.cdItem+'</td>'+
			    				'<td>'+value.nbNombreVar+'</td>'+
			    				'<td>'+value.desTipo+'</td>'+
			    				'<td>'+value.nuCoorX+'</td>'+
			    				'<td>'+value.nuCoorY+'</td>'+
			    				'<td class="'+detailClass+'">'+value.desEstadoSvg+'</td>'+
			    				'<td class="hidden">'+value.resultado+'</td>'+
			    				'<td class="hidden json">'+JSON.stringify(value)+'</td>'+
			    				'</tr>';
			    			$('#tblItemsCarga tbody').append(htmlRow);
			    			$('#cbItem'+index).attr('checked', true);
			    		});
			    		
			    		var table = dataTableDefecto('#tblItemsCarga');
			    		table.on('draw', function () {
			    	        $.each(detailRows, function (i,id) {
			    	            $('#'+id+' td.details-control').trigger('click');
			    	        });
			    	    });
				    },
		        	error: function(jqXHR, textStatus, errorThrown ){
		        		alert('Error al procesar archivo.')
		        		limpiarSubir();
		        	}
		        });
		    }
		});
    });
	
	$('#btnAplicar').on('click',function () {
		var listaCambios = obtenerListaCambios();
		
	});
	
	$('#btnScript').on('click',function () {
		var cdSeccion = $('#txtSeccion').val();
		var listaCambios = obtenerListaCambios();
		var data = {json:listaCambios};
		$.ajax({
		    url: baseUrl+'/rest/svg/'+cdSeccion+'/script',
		    type: "POST",
			contentType: "application/json",
			data: JSON.stringify(data),
		    success: function(data){
		    	alert('cool');
		    },
		    dataType: 'json'
		});
	});
	
	$('#confirm-delete').on('click', '.btn-ok', function(e) {
        var $modalDiv = $(e.delegateTarget);
        var id = $(this).data('recordId');
        var baseUrl = $('#baseUrl').val();
    	var url = baseUrl+'/rest/plantilla';
    	var data = {
			cdPlantilla : plantillaSelected
		};
    	$modalDiv.addClass('loading');
		$.ajax({
			type: 'DELETE',
			contentType: "application/json",
			url: url,
			data: JSON.stringify(data),
			success: function( data, textStatus, jqXHR ) {
				$modalDiv.modal('hide').removeClass('loading');
				actualizarListaPlantillas();
			},
			dataType: 'json'
		});
    });
	
    $('#confirm-delete').on('show.bs.modal', function(e) {
        var data = $(e.relatedTarget).data();
        $('.title', this).text(data.recordTitle);
        $('.btn-ok', this).data('recordId', plantillaSelected);
    });
	
} );

function limpiarSubir(){
	$('#btnSubir').text('Subir');
    $('#btnSubir').removeClass('disabled');
    $('#modalPlantilla .rptaSetup').html('');
    $('#modalPlantilla .rptaSetup').addClass('hidden');
    $('#modalPlantilla .selectSetup').addClass('hidden');
    $('#modalPlantilla .fileSetup').removeClass('hidden');
    $('#modalPlantilla .panelScripts').addClass('hidden');
    $('#txtFileNameSVG').val('');
    destruirTabla('#tblItemsCarga');
}

function actualizarListaPlantillas(){
	
	var baseUrl = $('#baseUrl').val();
	
	destruirTabla('#tblPlantillas');
	
	$.getJSON( baseUrl+'/rest/plantilla/all', {} )
	.done(function( data, textStatus, jqXHR ) {
		$.each(data, function( index, value ) {
			var htmlRow=
				'<tr>'+
				'<td><input type="hidden" value="'+value.cdPlantilla+'"/>'+value.cdPlantilla+'</td>'+
				'<td>'+value.nbNombreFormato+'</td>'+
				'<td class="plantillaDes">'+value.nbDescripcion+'</td>'+
				'<td>'+check(value.stActivo)+'</td>'+
				'<td>'+check(value.stEnvCorreo)+'</td>'+
				'<td>'+check(value.stFileunico)+'</td>'+
				'<td>'+check(value.stSigned)+'</td>'+
				'<td>'+
				'<span class="btnedit   glyphicon glyphicon-cog" aria-hidden="true"></span> '+
				'<span class="btnremove glyphicon glyphicon-minus" aria-hidden="true"></span>'+
				'</td>'+
				'</tr>';
			$('#tblPlantillas tbody').append(htmlRow);
		});
		
		$('#tblPlantillas').DataTable({
	    	"ordering"     : false,
	    	"lengthChange" : false,
	        "info"         : false,
	        "dom"          : '<"toolbar">frtip',
	        "language"     : {  "paginate": {
				             		"next": "Sig.",
				             		"previous": "Ant."
	        					},
	        					"search": "Buscar:"
	        				 }
	    });
		
		var butonera = 
			'<div class="btn-group" role="group" aria-label="...">'+
			'<button type="button" class="btn btn-sm btn-default"        id="btnNuevoPlantilla">'+
				'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo'+
			'</button> '+
			'<button type="button" disabled="disabled" class="btn btn-sm btn-default" id="btnEditarPlantilla">'+
				'<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>'+
			'</button> '+
			'<button class="btn btn-sm btn-default" data-record-id="" data-record-title="Plantilla" data-toggle="modal" data-target="#confirm-delete" id="btnEliminarPlantilla">'+
				'<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>'+
			'</button> '+
			'</div>';
		
		$("div.toolbar").html(butonera);
		
		$('#btnEditarPlantilla').on( 'click', function () {
			editarPlantilla();
		});
		$('#btnNuevoPlantilla').on( 'click', function () {
			nuevoPlantilla();
		});
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function bindListaPlantillas(){
	$('#tblPlantillas tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            limpiarBarra();
        }
        else {
        	$('#btnEditarPlantilla').prop("disabled", false);
        	$('#btnEliminarPlantilla').prop("disabled", false);
        	$('#tblPlantillas tbody tr').removeClass('selected');
            $(this).addClass('selected');
            plantillaSelected = $(this).find('input').val();
        }
    });
}

function obtenerListaCambios(){
	var tableItems = $('#tblItemsCarga').DataTable();
	var data = tableItems.rows('.selectedItem').data();
	var result = [];
	$(data).each(function(){
		result.push($(this)[8]);
	});
	return result;
}

function limpiarBarra(){
	$('#btnEditarPlantilla').prop("disabled", true);
	$('#btnEliminarPlantilla').prop("disabled", true);
}
function limpiarModal(){
	$('#accion').val('');
	$('#modalPlantilla h4.modal-title').html('Nuevo Plantilla');
	$('#txtCodigo').val('');
	$('#txtNombre').val('');	
	$('#cbEnvCorreo').attr('checked',false);
	$('#txtNombreCorreo').prop('disabled',true);
	$('#cbFileUnico').attr('checked', false);
	$('#cbSigned').attr('checked', false);
	$('#txtNombreCorreo').val('');	
	$('#selEstado').val('');
	$('#selTipoCond').val($('#txtCondFijo').val());
	$('#selTipoConstr').val('');
	$('#tabFlags').hide();
	$('#selTipoFileunico').val('');			
	$('#txtCodigo').attr('disabled', true);
	$('#txtDescripcion').val('');		
	$('#txtTipoFileunico').val('');	
	$('#txtDesTipoFileunico').val('');		
	$('#pdf object').remove();
	$('#txtFileNamePDF').val('');
	destruirTabla('#tblSecciones');
	destruirTabla('#tblItems');
	destruirTabla('#tblFlagsPlantilla');
	
	cambiarTab('#tabHome','#home','.btn-home');
	$('#panelSeccion').addClass('hidden');
	limpiarSubir();
};

function descargaSvg(cdSeccion){
	var baseUrl = $('#baseUrl').val();
	window.open(baseUrl+'/resource/svg/'+cdSeccion+'.svg');
}

function procesarSvg(cdSeccion){
	$('#txtSeccion').val(cdSeccion);
	limpiarSubir();
	cambiarTab('#tabCarga','#car','.btn-car');
}

function verItems(cdSeccion){
	actualizarItems(cdSeccion);
	cambiarTab('#tabItems','#par','.btn-par');
}

function cambiarTab(idTabActivar,idPanelActivar,classBotones){
	$('#modalPlantilla ul.nav-tabs li').removeClass('active').removeClass('in');
	$(idTabActivar).addClass('active').addClass('in');
	$('#modalPlantilla div.tab-content').children().removeClass('active').removeClass('in');
	$(idPanelActivar).addClass('active').addClass('in');
	cambiarTabBotones(classBotones);
}
function cambiarTabBotones(classBotones){
	$('#modalPlantilla .btn-primary').addClass('hidden');
	$('#modalPlantilla '+classBotones).removeClass('hidden');
}

function nuevoPlantilla(){
	limpiarModal();
	$("#modalPlantilla").modal();
}

function editarPlantilla(){
	limpiarModal();
	var baseUrl = $('#baseUrl').val();
	$('#accion').val('editar');
	$('#modalPlantilla h4.modal-title').html('Editar Plantilla');
	
	$.getJSON( baseUrl+'/rest/plantilla/'+plantillaSelected, {} )
	.done(function( data, textStatus, jqXHR ) {
		$('#plantilla-title').html("Editar Plantilla");
		
		$('#txtCdPlantilla').val(data.cdPlantilla);
		$('#txtCodigo').val(data.cdPlantilla);
		$('#txtNombre').val(data.nbNombreFormato);
		$('#txtDescripcion').val(data.nbDescripcion);
		if(data.stEnvCorreo=='A'){
			$('#cbEnvCorreo').prop('checked', true);
			$('#txtNombreCorreo').prop('disabled',false);
		}
		if(data.stFileunico=='A'){
			$('#cbFileUnico').prop('checked', true);
		}
		if(data.stSigned=='A'){
			$('#cbSigned').prop('checked', true);
		}
		$('#txtNombreCorreo').val(data.nbNombreCorreo);
		
		$('#selEstado').val(data.stActivo);
		$('#selTipoCond').val(data.stCondicion);
		$('#selTipoConstr').val(data.stTipoConstr);
		$('#selTipoPlantilla').val(data.chTipoPlantilla);
		if(data.stCondicion==$('#txtCondFijo').val()){
			$('#tabFlags').hide();
		}
		$('#txtTipoFileunico').val(data.nbTipoFileunico);
		actualizarDesFileunico();
				
		$('#txtCodigo').attr('disabled', true);
		
		$("#modalPlantilla").modal();
		
		actualizarFlagsPlantilla(plantillaSelected);
		actualizarSecciones(plantillaSelected);
		actualizarPDF(plantillaSelected);
	}).
	fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function actualizarPDF(cdPlantilla){
	var baseUrl = $('#baseUrl').val();
	$('#txtPDFPlantilla').val(cdPlantilla);
	var objectPDF = 
		'<object data="'+baseUrl+'/resource/pdf/'+cdPlantilla+'.pdf" type="application/pdf" width="570" height="300">'+
		'<a href="'+baseUrl+'/resource/pdf/'+cdPlantilla+'.pdf">Descargar PDF</a>'+
		'</object>';
	$('#pdf').append(objectPDF);
}

function actualizarDesFileunico(){
	var tipo = $('#txtTipoFileunico').val();
	if(tipo!=null && tipo!=''){
		var baseUrl = $('#baseUrl').val();
		$.getJSON( baseUrl+'/rest/fu/tipo/'+tipo, {} )
		.done(function( data, textStatus, jqXHR ) {    		
			$('#txtDesTipoFileunico').val(data.nombre);
		})
		.fail(function( jqXHR, textStatus, errorThrown ) {
			$('#txtDesTipoFileunico').val('');
			if ( console && console.log ) {
				console.log( "Algo ha fallado: " +  textStatus);
			}
		});
	}	
}

function validarFlagPlantilla(){
	if(flagSelected==null){
		alert('Debe seleccionar una bandera.');
		return false;
	}
	return true;
}

function guardarFlagPlantilla(){
	var cdEnv = $('#cdEnv').val();
	var baseUrl = $('#baseUrl').val();
	if(validarFlagPlantilla()){
		var url = baseUrl+'/rest/plantilla/flag';
		var cdPlantilla = plantillaSelected;
		var data = {
			cdFlag : flagSelected,
			cdPlantilla : cdPlantilla,
			stValor :  $('#txtValorGrupo').val(),
			stActivo : 'A'
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: url,
			data: JSON.stringify(data),
			success: function( data, textStatus, jqXHR ) {
				alert('Se guardo correctamente');
				$("#modalNuevoFlag").modal('hide');
			},
			dataType: 'json'
		});
	}
}

function actualizarTablaFlags(tipo){
	var baseUrl = $('#baseUrl').val();
	$.getJSON( baseUrl+'/rest/flags/'+tipo, {} )
	.done(function( data, textStatus, jqXHR ) {    		
		
		destruirTabla('#tblFlags');
		
		$.each(data, function( index, value ) {
			var htmlRow=
				'<tr>'+
				'<td><input type="hidden" value="'+value.cdFlag+'"/>'+value.cdFlag+'</td>'+
				'<td>'+value.nbCondicionNombre+'</td>'+
				'<td>'+value.desOperador+'</td>'+
				'<td>'+value.nbCondicionValor+'</td>'+
				'</tr>';
			$('#tblFlags tbody').append(htmlRow);
		});
		
		$('#tblFlags').DataTable({
	    	"ordering"     : false,
	    	"lengthChange" : false,
	        "info"         : false,
	        "pageLength"   : 4,
	        "language"     : {  "paginate": {
	        						"next": "Sig.",
	        						"previous": "Ant."
								},
								"search": "Buscar:",
								"infoEmpty": "No se encontraron flags",
			 				}
	    });
		
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function bindTablaFlags(){
	$('#tblFlags tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            flagSelected = null;
        } else {
        	$('#tblFlags tbody tr').removeClass('selected');
            $(this).addClass('selected');
            flagSelected = $(this).find('input').val();
        }
    });
}

function bindTablaItemsCarga(){
	$('#tblItemsCarga tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selectedItem') ) {
            $(this).removeClass('selectedItem');
        } else {
        	$(this).addClass('selectedItem');
        }
    });
    var detailRows = [];
    $('#tblItemsCarga tbody').on('click','tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var idx = $.inArray(tr.attr('id'),detailRows);		    	 
        if(row.child.isShown()) {
            tr.removeClass('details');
            row.child.hide();
            detailRows.splice(idx,1);
        }else{
            tr.addClass('details');
            row.child(format(row.data())).show();
            if (idx === -1) {
                detailRows.push(tr.attr('id'));
            }
        }
    });
}

function nuevoFlagPlantilla(){
	actualizarTablaFlags('T');//(T)emplate,(C)orreo
}

function actualizarSecciones(cdPlantilla){
	var baseUrl = $('#baseUrl').val();
	$.getJSON( baseUrl+'/rest/plantilla/'+cdPlantilla+'/secciones', {} )
	.done(function( data, textStatus, jqXHR ) {    		
		destruirTabla('#tblSecciones');
		$.each(data, function( index, value ) {
			var htmlRow=
				'<tr>'+
				'<td><a href="../seccion/'+value.cdSeccion+'">'+value.cdSeccion+'</a></td>'+
				'<td>'+value.nuPagina+'</td>'+
				'<td><span class="glyphicon glyphicon-th-large" aria-hidden="true"></span></td> '+
				'<td>'+
				'<a href="javascript:verItems('+value.cdSeccion+')"><span class="btnitems glyphicon glyphicon-list-alt" aria-hidden="true"></span></a> '+
				'<a href="javascript:descargaSvg('+value.cdSeccion+')"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></a> '+
				'<a href="javascript:procesarSvg('+value.cdSeccion+')"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span></a> '+
				'<a href="javascript:editarSeccion('+value.cdSeccion+')"><span class="btnedit glyphicon glyphicon-cog" aria-hidden="true"></span></a> '+
				'<a href="javascript:eliminarSeccion('+value.cdSeccion+')"><span class="btnremove glyphicon glyphicon-minus" aria-hidden="true"></span></a> '+
				'</td>'+
				'</tr>';
			$('#tblSecciones tbody').append(htmlRow);
		});
		$('#tblSecciones').DataTable({
	    	"ordering"     : false,
	    	"lengthChange" : false,
	        "info"         : false,
	        "pageLength"   : 4,
	        "dom"          : '<"toolbar toolbarsec">frtip',
	        "language"     : {  "paginate": {
	        						"next": "Sig.",
	        						"previous": "Ant."
								},
								"search": "Buscar:",
								"infoEmpty": "No se encontraron secciones",
			 				}
	    });
		var butonera = 
			'<div class="btn-group" role="group" aria-label="...">'+
			'<button type="button" class="btn btn-sm btn-default"        id="btnNuevoSeccion">'+
				'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo'+
			'</button> '+			
			'</div>';
		$("div.toolbarsec").html(butonera);
		$('#btnNuevoSeccion').on( 'click', function () {
			nuevaSeccion();
		});
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}
function bindTablaSecciones(){
	$('#tblSecciones tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        } else {
        	$('#tblSecciones tbody tr').removeClass('selected');
            $(this).addClass('selected');
            seccionSelected = $(this).find('input').val();
        }
    });
}

function nuevaSeccion(){
	$('#txtCdSeccion').val('');
	$('#txtNuPagina').val('');
	$('#txtCdSeccionPlantilla').val(plantillaSelected);
	$('#panelSeccion').removeClass('hidden');
}

function editarSeccion(cdSeccion){
	var baseUrl = $('#baseUrl').val();
	$('#txtCdSeccion').val(cdSeccion);
	$('#panelSeccion').removeClass('hidden');
	$.getJSON( baseUrl+'/rest/seccion/'+cdSeccion, {} )
	.done(function( data, textStatus, jqXHR ) {
		$('#txtNuPagina').val(data.nuPagina);
		$('#txtCdSeccionPlantilla').val(data.cdPlantilla);
	}).
	fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function actualizarFlagsPlantilla(cdPlantilla){
	var baseUrl = $('#baseUrl').val();
	$.getJSON( baseUrl+'/rest/plantilla/'+cdPlantilla+'/flags', {} )
	.done(function( data, textStatus, jqXHR ) {    		
		
		destruirTabla('#tblFlagsPlantilla');
		
		$.each(data, function( index, value ) {
			var htmlRow=
				'<tr>'+
				'<td><a href="../flag/'+value.cdFlag+'">'+value.cdFlag+'</a></td>'+
				'<td>'+value.flag.nbCondicionNombre+'</td>'+
				'<td>'+value.flag.nbCondicionValor+'</td>'+
				'<td>'+value.stValor+'</td>'+
				'<td>'+value.flag.desOperador+'</td>'+
				'<td>'+
				'<span class="btnedit glyphicon glyphicon-cog" aria-hidden="true"></span> '+
				'<span class="btnremove glyphicon glyphicon-minus" aria-hidden="true"></span> '+
				'</td>'+
				'</tr>';
			$('#tblFlagsPlantilla tbody').append(htmlRow);
		});
		
		$('#tblFlagsPlantilla').DataTable({
	    	"ordering"     : false,
	    	"lengthChange" : false,
	        "info"         : false,
	        "pageLength"   : 5,
	        "dom"          : '<"toolbar toolbarflag">frtip',
	        "language"     : {  "paginate": {
	        						"next": "Sig.",
	        						"previous": "Ant."
								},
								"search": "Buscar:",
								"infoEmpty": "No se encontraron flags",
			 				}
	    });
		var butonera = 
			'<div class="btn-group" role="group" aria-label="...">'+
			'<button type="button" class="btn btn-sm btn-default"        id="btnNuevoFlagPlantilla">'+
				'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo'+
			'</button>'+	
			'</div>';
		$("div.toolbarflag").html(butonera);
		
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}
function bindTablaFlagsPlantilla(){
	$('#tblFlagsPlantilla tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        } else {
        	$('#tblFlagsPlantilla tbody tr').removeClass('selected');
            $(this).addClass('selected');
            flagPlantillaSelected = $(this).find('input').val();
        }
    });
	$('#btnEditarFlagPlantilla').on( 'click', function () {
		editarFlagPlantilla();
	});
	$('#btnNuevoFlagPlantilla').on( 'click', function () {
		nuevoFlagPlantilla();
	});
}

function actualizarItems(cdSeccion){
	var baseUrl = $('#baseUrl').val();
	$.getJSON( baseUrl+'/rest/seccion/'+cdSeccion+'/items', {} )
	.done(function( data, textStatus, jqXHR ) {	
		
		destruirTabla('#tblItems');
		
		$.each(data, function( index, value ) {
			var htmlRow=
				'<tr>'+
				'<td><input type="hidden" value="'+value.cdItem+'"/>'+value.cdItem+'</td>'+
				'<td>'+value.nbNombreVar+'</td>'+
				'<td>'+check(value.stActivo)+'</td>'+
				'<td>'+checkTipo(value.chTipo,value.nbNombreVar)+'</td>'+
				'<td>'+value.nuCoorX+'</td>'+
				'<td>'+value.nuCoorY+'</td>'+
				'<td>'+
				'<span class="btnedit glyphicon glyphicon-cog" aria-hidden="true" id="btnEditarItem"></span> '+
				'<span class="btnremove glyphicon glyphicon-minus" aria-hidden="true" id="btnEliminarItem"></span> '+
				'</td>'+
				'</tr>';
			$('#tblItems tbody').append(htmlRow);
		});
		
		$('#tblItems').DataTable({
	    	"ordering"     : false,
	    	"lengthChange" : false,
	        "info"         : false,
	        "pageLength"   : 5,
	        "dom"          : '<"toolbar toolbaritems">frtip',
	        "language"     : {  "paginate": {
	        						"next": "Sig.",
	        						"previous": "Ant."
								},
								"search": "Buscar:",
								"infoEmpty": "No se encontraron items",
			 				}
	    });
		var butonera = 
			'<div class="btn-group" role="group" aria-label="...">'+
			'<button type="button" class="btn btn-sm btn-default"        id="btnNuevoItem">'+
				'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo'+
			'</button>'+	
			'</div>';
		$("div.toolbaritems").html(butonera);
		
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "Algo ha fallado: " +  textStatus);
		}
	});
}

function bindTablaItems(){
	$('#tblItems tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
        	$('#btnEditarItem').prop("disabled", false);
        	$('#btnEliminarItem').prop("disabled", false);
        	$('#tblItems tbody tr').removeClass('selected');
            $(this).addClass('selected');
            itemSelected = $(this).find('input').val();
        }
    });
}

function validarPlantilla(){
	/*
	if($('#txtCodigo').val().length > 8){
		alert('El campo código debe tener un tamaño de 8 carácteres.');
		return false;
	}*/
	return true;
}

function validarSeccion(){
	/*
	if($('#txtCodigo').val().length > 8){
		alert('El campo código debe tener un tamaño de 8 carácteres.');
		return false;
	}*/
	return true;
}

function guardarPlantilla(){
	var cdEnv = $('#cdEnv').val();
	var baseUrl = $('#baseUrl').val();
	if(validarPlantilla()){
		var url = baseUrl+'/rest/plantilla';
		if($('#accion').val()=='editar'){
			url = baseUrl+'/rest/plantilla/update';
		}
		var data = {
			cdPlantilla : $('#txtCdPlantilla').val(),
			nbNombreFormato : $('#txtNombre').val(),
			nbDescripcion :  $('#txtDescripcion').val(),
			stCondicion : $('#selTipoCond').val(),
			stFileunico : $('#cbFileUnico').is(':checked') ? $('#cbFileUnico').val() : '',
			stEnvCorreo : $('#cbEnvCorreo').is(':checked') ? $('#cbEnvCorreo').val() : '',
			stTipoConstr : $('#selTipoConstr').val(),
			stSigned : $('#cbSigned').is(':checked') ? $('#cbSigned').val() : '',
			stActivo : $('#selEstado').val(),
			nbTipoFileunico : $('#selTipoFileunico').val(),
			nbNombreCorreo : $('#txtNombreCorreo').val(),
			chTipoPlantilla : $('#selTipoPlantilla').val(),
			nbTipoFileunico : $('#txtTipoFileunico').val()
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: url,
			data: JSON.stringify(data),
			success: function( data, textStatus, jqXHR ) {
				alert('Se guardo correctamente');
				actualizarListaPlantillas();
				$("#modalPlantilla").modal('hide');
			},
			error: function(jqXHR, textStatus, errorThrown ){
        		alert('Error al guardar plantilla.');
        	},
			dataType: 'json'
		});
	}
}

function guardarSeccion(){
	var cdEnv = $('#cdEnv').val();
	var baseUrl = $('#baseUrl').val();
	if(validarSeccion()){
		var url = baseUrl+'/rest/seccion';
		if($('#accionSeccion').val()=='editar'){
			url = baseUrl+'/rest/seccion/update';
		}
		var data = {
			cdSeccion : $('#txtCdSeccion').val(),
			cdPlantilla : $('#txtCdSeccionPlantilla').val(),
			nuPagina : $('#txtNuPagina').val()
		};
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: url,
			data: JSON.stringify(data),
			success: function( data, textStatus, jqXHR ) {
				alert('Se guardo correctamente');
				actualizarSecciones(plantillaSelected);
				nuevaSeccion();
			},
			dataType: 'json'
		});
	}	
}