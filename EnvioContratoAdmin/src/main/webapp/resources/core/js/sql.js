$(document).ready(function() {
    $('#btnRunSQL').on( 'click', function () {
		ejecutarSQL();
	});	
} );

function ejecutarSQL(){
	limpiarResultado();
	var baseUrl = $('#baseUrl').val();
	
	var url = baseUrl+'/rest/query';
	if($('#selectTipo').val()==='E'){
		url = baseUrl+'/rest/execute';
	}
	var data = $('#sqlInput').val();
	$.ajax({
		type: "POST",
		contentType: "application/json",
		url: url,
		data: data,
		success: function( data, textStatus, jqXHR ) {
			var output = '';
			if(data){
				$.each(data, function( index, value ) {
					output += JSON.stringify(value) + '\n';
				});
			}
			$('#result').val(output);
		},
		dataType: 'json'
	});
}

function limpiarResultado(){
	$('#result').val('');
}