<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin</title>

<%@include file="base.jsp"%>

</head>

<body>	

	<%@include file="header.jsp"%>
	<div class="container">
		<!-- 
		<div class="py-5 text-center">
			<img class="d-block mx-auto mb-4" src="${imgSQL}" alt="" width="64" height="64">
		</div>
		 -->
		<hr class="mb-4">
		<div class="row">
			<div class="col-md-4 mb-4">
				<h4 class="d-flex justify-content-between align-items-center mb-3">
					<span class="text-muted">Plantillas</span>
				</h4>
				<ul class="list-group mb-3">
					<li class="list-group-item d-flex justify-content-between lh-condensed">
						<h6 class="my-0">Kits</h6>
					</li>
					<li class="list-group-item d-flex justify-content-between lh-condensed">
						<h6 class="my-0">Plantillas</h6>
					</li>
					<li class="list-group-item d-flex justify-content-between lh-condensed">
						<h6 class="my-0">Consultas</h6>
					</li>
					<li class="list-group-item d-flex justify-content-between lh-condensed">
						<h6 class="my-0">Updates</h6>
					</li>
				</ul>
			</div>
			
			<div class="col-md-8">
				<h4 class="d-flex justify-content-between align-items-center mb-3">
					<span class="text-muted">SQL</span>
				</h4>
				<form class="needs-validation" novalidate="">
					<div class="form-group">
						<select class="form-control" id="selectTipo" name="selectTipo">
							<option value="Q">Consulta</option>
							<option value="E">Actualizaci&oacute;n</option>
						</select>
					</div>
					<div class="form-group">
						<textarea class="form-control" id="sqlInput" rows="4"></textarea>
					</div>
					<a class="btn btn-primary btn-lg btn-block" id="btnRunSQL">Run</a>
				</form>
			</div>
		</div>
		<div class="row">
			<textarea class="form-control" id="result" rows="5"></textarea>
		</div>
	</div>

<script src="${mainJs}"></script>
<script src="${sqlJs}"></script>

</body>
</html>