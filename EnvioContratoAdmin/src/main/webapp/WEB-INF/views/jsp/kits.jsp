<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>EnvioContratos WebAdmin 3333</title>

<%@include file="base.jsp"%>

</head>

<body>

	<%@include file="header.jsp"%>

	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<%=request.getContextPath()%>">Inicio</a></li>
			<li><a href="">Kits</a></li>
		</ol>
	</div>

	<div class="container">

		<table id="tblKits" class="table table-striped" cellspacing="0"
			width="100%">
			<thead>
				<tr>
					<th>C�digo</th>
					<th>Nombre</th>
					<th>Correo</th>
					<th>FU</th>
					<th>Huella Dig</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		<hr>
		<footer>
			<p>&copy; BBVA Continental 2017</p>
		</footer>
	</div>

	<div id="modalKit" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document" style="width: 700px">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Nuevo Kit</h4>
				</div>
				<div class="modal-body">
					
					<div class="hidden alert alert-success">
						<strong>Success!</strong> Indicates a successful or positive action.
					</div>

					<ul class="nav nav-tabs">
						<li id="tabDatos" class="active"><a data-toggle="tab" href="#home">Datos</a></li>
						<li><a data-toggle="tab" href="#menu1">Plantillas</a></li>
						<li><a data-toggle="tab" href="#menu2">Correo</a></li>
					</ul>
					<div class="tab-content">
						<div id="home" class="tab-pane fade in active">
							<br/>
							<form class="form-horizontal">
								<div class="form-group">
									<label for="txtCodigo" class="col-sm-2 control-label">C�digo</label>
									<div class="col-sm-10">
										<input type="hidden" id="accion">
										<input type="hidden" id="txtCdKit">
										<input type="text" class="form-control" id="txtCodigo"
											placeholder="C�digo de Kit">
									</div>
								</div>
								<div class="form-group">
									<label for="txtNombre" class="col-sm-2 control-label">Nombre</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="txtNombre" maxlength="40"
											placeholder="Nombre de Kit">
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-3">
										<div class="checkbox">
											<label> <input type="checkbox" id="cbEnvCorreo"
												value="A"> Env�a Correo
											</label>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="checkbox">
											<label> <input type="checkbox" id="cbFileUnico"
												value="A"> Env�a FileUnico
											</label>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="checkbox">
											<label> <input type="checkbox" id="cbValHuella"
												value="A"> Validaci�n Huella Digital
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="selPlantillaCorreo" class="col-sm-2 control-label">
										Plantilla de correo
									</label>
									<div class="col-sm-10">
										<select class="form-control" id="selPlantillaCorreo">
											<option value="STRTPL">StringTemplate</option>
											<option value="LANG3">Commons-Lang3</option>
											<option value=""></option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="txtAsuntoCorreo" class="col-sm-2 control-label">Asunto</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="txtAsuntoCorreo"
											placeholder="Asunto de correo">
									</div>
								</div>
							</form>
						</div>
						<div id="menu1" class="tab-pane fade">
							<table id="tblPlantillas" class="table table-striped"
								cellspacing="0" width="100%">
								<thead>
									<tr>
										<th></th>
										<th>Cod.</th>
										<th>Nombre</th>
										<th>Activo</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div id="menu2" class="tab-pane fade">
							<br/>
							<textarea id="txtAreaCorreo" class="form-control" rows="15"></textarea>							
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary" id="btnGuardarKit">Guardar</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<script src="${kitsJs}"></script>

</body>
</html>