package com.bbva.cuentas.aat.comparator;

import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Constants;

@Component
public class PlantillaComparator implements Comparator<Plantilla>{

	@Override
	public int compare(Plantilla o1, Plantilla o2) {
		final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    	    
	    if (o1 == o2) return EQUAL;
	    
	    if(StringUtils.equals(o1.getStActivo(),Constants.ST_ACTIVO) && 
	    		(StringUtils.equals(o2.getStActivo(),Constants.ST_INACTIVO) || StringUtils.isEmpty(o2.getStActivo()))){
	    	return BEFORE;
	    }
	    if((StringUtils.equals(o1.getStActivo(),Constants.ST_INACTIVO) || StringUtils.isEmpty(o1.getStActivo())) && 
	    		StringUtils.equals(o2.getStActivo(),Constants.ST_ACTIVO)){
	    	return AFTER;
	    }
	    
	    if(o1.getNbNombreFormato()!=null && o2.getNbNombreFormato()!=null){
	    	int compare = o1.getNbNombreFormato().compareTo(o2.getNbNombreFormato());
	    	if(compare!=EQUAL){
	    		return compare;
	    	}
	    }else if(o1.getNbNombreFormato()!=null){
	    	return BEFORE;
	    }
	    
	    if (o1.getCdPlantilla() < o2.getCdPlantilla()) return BEFORE;
	    if (o1.getCdPlantilla() > o2.getCdPlantilla()) return AFTER;
	    
		return EQUAL;
	}
	
}
