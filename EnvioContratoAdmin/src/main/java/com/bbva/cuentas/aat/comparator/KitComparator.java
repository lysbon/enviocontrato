package com.bbva.cuentas.aat.comparator;

import java.util.Comparator;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Kit;

@Component
public class KitComparator implements Comparator<Kit>{

	@Override
	public int compare(Kit o1, Kit o2) {
		final int BEFORE = -1;
	    final int EQUAL = 0;
	    final int AFTER = 1;
	    	    
	    if (o1 == o2) return EQUAL;
	    
	    if(o1.getCdCodigo()!=null && o2.getCdCodigo()!=null){
	    	int compare = o1.getCdCodigo().compareTo(o2.getCdCodigo());
	    	if(compare!=EQUAL){
	    		return compare;
	    	}
	    }else if(o1.getCdCodigo()!=null){
	    	return BEFORE;
	    }
	    
	    if (o1.getCdKit() < o2.getCdKit()) return BEFORE;
	    if (o1.getCdKit() > o2.getCdKit()) return AFTER;
	    
		return EQUAL;
	}
	
}
