package com.bbva.cuentas.aat.worker;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Plantilla;

@Component
public interface PDFWorker {
	public List<String> generarSVGdesdePDF(Plantilla plantilla, byte[] resPDF);
	public List<String> generarSVGdesdePDF(Plantilla plantilla);
	public List<String> generarSVGdesdePDF(Plantilla entity, int numPage, byte[] resPDF);
}
