package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.util.Result;
import com.bbva.cuentas.aat.dto.ScriptDTO;
import com.bbva.cuentas.bean.ItemSection;

public interface SeccionService {
	
	public Result obtenerSecciones() throws ServiceException;
	
	public Result obtenerSeccionPorId(Long id) throws ServiceException;

	public Result procesarSVG(Long id, String filename) throws ServiceException;
	
	public Result generarSVG(Long id) throws ServiceException;
	
	public Result obtenerSeccionesPorPlantilla(Long cdPlantilla) throws ServiceException;
	
	public Result aplicarCambiosSVG(ScriptDTO dto) throws ServiceException;
	
	public Result guardarSeccion(ItemSection itemSection) throws ServiceException;
	
	public Result actualizarSeccion(ItemSection itemSection) throws ServiceException;
	
	public Result eliminarSeccion(Long id) throws ServiceException;
	
	public void generarSVGfromSeccion(Long id) throws ServiceException;
	
}
