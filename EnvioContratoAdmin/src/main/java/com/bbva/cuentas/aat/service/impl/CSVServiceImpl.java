package com.bbva.cuentas.aat.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.dto.TipoFileUnico;
import com.bbva.cuentas.aat.service.CSVService;
import com.bbva.cuentas.aat.service.ServiceException;

@Component
public class CSVServiceImpl implements CSVService,ResourceLoaderAware{
	
	private ResourceLoader resourceLoader;
	
	@Value("${fileunico.tipos.path}")
	private String ruta;
	
	private List<TipoFileUnico> listaTipos;
	
	@PostConstruct
	public void init(){
		Resource resource = resourceLoader.getResource(ruta);
		try {
			readCSV(resource.getFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readCSV(File file){
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        listaTipos = new ArrayList<TipoFileUnico>();
        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {
                String[] tipo = line.split(cvsSplitBy);
                if(tipo!=null && tipo.length>12){
                	TipoFileUnico tipoFileUnico = new TipoFileUnico();
                    tipoFileUnico.setIdTipo(tipo[0]);
                    tipoFileUnico.setNombre(tipo[1]);
                    tipoFileUnico.setComentario(tipo[12]);
                    listaTipos.add(tipoFileUnico);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
	}
	
	public List<TipoFileUnico> obtenerTipos() throws ServiceException{
		return listaTipos;
	}
	
	public TipoFileUnico obtenerTipoPorCodigo(String idTipo) throws ServiceException{
		for(TipoFileUnico tipo : listaTipos){
			if(tipo.getIdTipo().equals(idTipo)){
				return tipo;
			}
		}
		return null;
	}
	
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
}

