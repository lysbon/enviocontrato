package com.bbva.cuentas.aat.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScriptDTO {

	private List<ItemStampDTO> items;
	private String scriptUpdate;
	private String scriptInsert;
	
	public List<ItemStampDTO> getItems() {
		return items;
	}
	public void setItems(List<ItemStampDTO> items) {
		this.items = items;
	}
	public String getScriptUpdate() {
		return scriptUpdate;
	}
	public void setScriptUpdate(String scriptUpdate) {
		this.scriptUpdate = scriptUpdate;
	}
	public String getScriptInsert() {
		return scriptInsert;
	}
	public void setScriptInsert(String scriptInsert) {
		this.scriptInsert = scriptInsert;
	}
	
}
