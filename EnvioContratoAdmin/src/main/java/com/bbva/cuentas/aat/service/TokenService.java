package com.bbva.cuentas.aat.service;

import org.springframework.web.client.RestTemplate;

public interface TokenService {

	public RestTemplate getTemplate();

}
