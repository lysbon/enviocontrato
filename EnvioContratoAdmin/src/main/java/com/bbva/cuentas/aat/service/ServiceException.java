package com.bbva.cuentas.aat.service;

public class ServiceException extends Exception{
	private static final long serialVersionUID = 1L;
	        
    private String errorCode;
    private String message;
    
    public ServiceException(String errorCode) {
        this(errorCode, null);
    }
    
    public ServiceException(String errorCode,String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

    @Override
	public String getMessage() {
    	return message;
	}
}
