package com.bbva.cuentas.aat.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.comparator.PlantillaComparator;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.aat.worker.PDFWorker;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Result;

@Service
public class PlantillaServiceImpl implements PlantillaService{

	private static final Logger logger = LoggerFactory.getLogger(PlantillaServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	private PDFWorker pdfWorker;
	
	@Autowired
	private PlantillaComparator comparator;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	@Value("${fileupload.tmp.path}")
	private String tmpPath;
	
	@Override
	public Result obtenerPlantillas()	throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo plantillas");
			final String uri = urlServices+"/rest/plantilla/all";
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Plantilla>> kitResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Plantilla>>() {});
		    List<Plantilla> plantillas = kitResponse.getBody();
		    Collections.sort(plantillas,comparator);
		    res.setEntities(plantillas);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	public Result obtenerPlantillaPorCodigo(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo plantilla por id");
			final String uri = urlServices+"/rest/plantilla/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Plantilla> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Plantilla>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	public void generarSVGfromPlantilla(Long id) throws ServiceException {
		try {
			Result res = obtenerPlantillaPorCodigo(id);
		    byte[] resPDF = obtenerPDF(id);
		    pdfWorker.generarSVGdesdePDF((Plantilla)res.getEntity(),resPDF);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	}
	
	public void generarSVGPagefromPlantilla(Long id,int numPage) throws ServiceException {
		try {
			Result res = obtenerPlantillaPorCodigo(id);
		    byte[] resPDF = obtenerPDF(id);
		    pdfWorker.generarSVGdesdePDF((Plantilla)res.getEntity(),numPage,resPDF);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	}
	
	public byte[] obtenerPDF(Long id) throws ServiceException{
		byte[] pdf = null;
		try {
			logger.info("Obteniendo pdf plantilla");
			final String uri = urlServices+"/rest/plantilla/{id}/pdf";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<String> pdfResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, params);
		    pdf = Base64.decodeBase64(pdfResponse.getBody());
		    		    
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return pdf;
	}
	
	public Result obtenerPlantillasAsociadas(Long cdKit) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo plantillas");
			final String uri = urlServices+"/rest/kit/{id}/plantillas/asoc";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdKit);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Plantilla>> kitResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Plantilla>>() {}, params);
		    List<Plantilla> plantillas = kitResponse.getBody();
		    Collections.sort(plantillas,comparator);
		    res.setEntities(plantillas);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarPlantilla(Plantilla plantilla) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando plantilla");
			final String uri = urlServices+"/rest/plantilla";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    HttpEntity<Plantilla> request = new HttpEntity<Plantilla>(plantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class,params);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}
	
	@Override
	public Result actualizarPlantillaPDF(Long cdPlantilla, String filename) throws ServiceException {
		Result result = new Result();
		String encodedBase64 = null;
		 FileInputStream fileInputStreamReader=null;
		try {
			logger.info("Generando BASE64 de archivo PDF");
			Plantilla plantilla = new Plantilla();
		    plantilla.setCdPlantilla(cdPlantilla);
		    String rutaCompleta = FilenameUtils.concat(tmpPath, filename);
		    File pdfFile = new File(rutaCompleta);
		    fileInputStreamReader = new FileInputStream(pdfFile);
			byte[] bytes = new byte[(int) pdfFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));
			fileInputStreamReader.close();
			plantilla.setLbPdfPlantilla(encodedBase64);
		    
		    logger.info("Actualizando PDF de plantilla");
			final String uri = urlServices+"/rest/plantilla/pdf";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    HttpEntity<Plantilla> request = new HttpEntity<Plantilla>(plantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<String> res = restTemplate.postForEntity(uri, request, String.class,params);
			result.setEntity(res.getBody());
		} catch (FileNotFoundException e) {
			logger.error("Error al leer pdf original. "+e);
		} catch (IOException e) {
			logger.error("Error al leer pdf original. "+e);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}finally {
			try {if (fileInputStreamReader!=null) fileInputStreamReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}

		}
	    return result;
	}
	
	@Override
	public Result actualizarPlantillaPDFCadena(Long cdPlantilla, String absolutePath) throws ServiceException {
		Result result = new Result();
		String encodedBase64 = null;
		FileInputStream fileInputStreamReader=null;
		try {
			logger.info("Generando BASE64 de archivo PDF");
			Plantilla plantilla = new Plantilla();
		    plantilla.setCdPlantilla(cdPlantilla);
		    String rutaCompleta = absolutePath;
		    File pdfFile = new File(rutaCompleta);
		    fileInputStreamReader = new FileInputStream(pdfFile);
			byte[] bytes = new byte[(int) pdfFile.length()];
			int count=fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));
			fileInputStreamReader.close();
			plantilla.setLbPdfPlantilla(encodedBase64);
		    
		    logger.info("Actualizando PDF de plantilla");
			final String uri = urlServices+"/rest/plantilla/pdf";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    HttpEntity<Plantilla> request = new HttpEntity<Plantilla>(plantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<String> res = restTemplate.postForEntity(uri, request, String.class,params);
			result.setEntity(res.getBody());
		} catch (FileNotFoundException e) {
			logger.error("Error al leer pdf original. "+e);
		} catch (IOException e) {
			logger.error("Error al leer pdf original. "+e);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}finally {
			try {if (fileInputStreamReader!=null) fileInputStreamReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}

		}
	    return result;
	}

	@Override
	public Result actualizarPlantilla(Plantilla plantilla) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando plantilla");
			final String uri = urlServices+"/rest/plantilla/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Plantilla> request = new HttpEntity<Plantilla>(plantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarPlantilla(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar plantilla : " + id);
			final String uri = urlServices+"/rest/plantilla/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.POST, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
}
