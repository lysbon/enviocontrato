package com.bbva.cuentas.aat.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.dto.TipoFileUnico;

@Component
public interface CSVService{
	
	public List<TipoFileUnico> obtenerTipos() throws ServiceException;
	
	public TipoFileUnico obtenerTipoPorCodigo(String idTipo) throws ServiceException;
	
}

