package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.util.Result;

public interface ItemService {

	public Result obtenerItemPorCodigo(Long id) throws ServiceException;
	
	public Result obtenerItemsPorSeccion(Long id) throws ServiceException;
	
	public Result obtenerItems() throws ServiceException;
	
	public Result guardarItem(ItemStamp itemStamp) throws ServiceException;
	
	public Result actualizarItem(ItemStamp itemStamp) throws ServiceException;
	
	public Result eliminarItem(Long id) throws ServiceException;
	
}
