package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.dto.ItemStampDTO;
import com.bbva.cuentas.aat.dto.ScriptDTO;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Result;
import com.bbva.cuentas.aat.util.ErrorCodes;
import com.bbva.cuentas.aat.worker.PDFWorker;
import com.bbva.cuentas.aat.worker.SVGWorker;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Plantilla;

@Service
public class SeccionServiceImpl implements SeccionService{

	private static final Logger logger = LoggerFactory.getLogger(SeccionServiceImpl.class);
	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private ItemService itemService;

	@Autowired
	private PlantillaService plantillaService;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	@Value("${fileupload.tmp.path}")
	private String tmpPath;
	@Value("${fileupload.svg.path}")
	private String svgPath;
	@Value("${fileupload.suffix}")
	private String suffix;
	
	@Autowired
	private SVGWorker svgWorker;
	
	@Autowired
	private PDFWorker pdfWorker;
	
	public Result obtenerSeccionPorId(Long id) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo seccion por id");
			final String uri = urlServices+"/rest/seccion/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<ItemSection> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<ItemSection>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	private ItemStamp buscarItem(List<ItemStamp> items,String id){
		if(items==null) return null;
    	for(ItemStamp item : items){
    		if(item.getCdItem()!=null && StringUtils.equals(id,item.getCdItem().toString())){
				return item;
			}
		}
    	return null;
    }
	
	private ItemStampDTO buscarItemDTO(List<ItemStampDTO> items,Long id){
    	for(ItemStampDTO item : items){
    		if(id.toString().equals(item.getCdItem())){
				return item;
			}
		}
    	return null;
    }
	
	@SuppressWarnings("unchecked")
	public Result procesarSVG(Long cdSeccion, String filename) throws ServiceException{
		Result res = new Result();
		try {
			ScriptDTO salida = new ScriptDTO();
			String rutaCompleta = FilenameUtils.concat(tmpPath, filename);
			List<ItemStampDTO> listaSvg = svgWorker.procesarSVG(cdSeccion,rutaCompleta);
			Result result = itemService.obtenerItemsPorSeccion(cdSeccion);
			List<ItemStamp> items = (List<ItemStamp>) result.getEntities();
			if(listaSvg!=null){
				for(ItemStampDTO it : listaSvg){
					ItemStamp buscado = buscarItem(items,it.getCdItem());
					if(buscado!=null){
						boolean iguales = compararItem(it,buscado);
						if(iguales){
							it.setStSVGCarga(Constants.ST_IT_SIN);
						}else{
							it.setStSVGCarga(Constants.ST_IT_MOD);
						}
					}else{
						it.setStSVGCarga(Constants.ST_IT_CRE);
					}
				}
			}
			if(items!=null){
				for(ItemStamp it : items){
					ItemStampDTO buscado = buscarItemDTO(listaSvg,it.getCdItem());
					if(buscado==null){
						ItemStampDTO itemDel = new ItemStampDTO();
						itemDel.setCdItem(it.getCdItem().toString());
						itemDel.setCdSeccion(it.getCdSeccion());
						itemDel.setStSVGCarga(Constants.ST_IT_DEL);
						listaSvg.add(itemDel);
					}
				}
			}
			salida.setItems(listaSvg);
			salida.setScriptUpdate(generarScriptUpdate(cdSeccion,listaSvg));
			salida.setScriptInsert(generarScriptInsert(cdSeccion,listaSvg));
			res.setEntity(salida);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_PROCESAR_SVG,"No se pudo procesar svg para seccion.");
		}
		return res;
	}
	
	public String generarScriptUpdate(Long cdSeccion,List<ItemStampDTO> items) throws ServiceException{
		if(items==null) return StringUtils.EMPTY;
		StringBuilder sbUpdate = new StringBuilder();
		for(ItemStampDTO dto : items){
			sbUpdate.append(exportarScript(dto,false));
		}
		return sbUpdate.toString();
	}
	
	public String generarScriptInsert(Long cdSeccion,List<ItemStampDTO> items) throws ServiceException{
		if(items==null) return StringUtils.EMPTY;
		StringBuilder sbInsert = new StringBuilder("DELETE FROM APDIGB.TBG004_ITEM WHERE CD_SECCION="+cdSeccion+"\n");
		for(ItemStampDTO dto : items){
			sbInsert.append(exportarScript(dto,true));
		}
		return sbInsert.toString();
	}

	/*
	 * Return true if equals
	 *        false if not equals 
	 * */
	
	private String compararAttr(String llave,String valor1,String valor2){
		if(!StringUtils.equals(valor1,valor2)){
			return llave+"["+valor1+","+valor2+"]\n";
		}
		return StringUtils.EMPTY;
	}
	private String compararAttr(String llave,Integer valor1,Integer valor2){
		if(valor1==null && valor2==null){
			return StringUtils.EMPTY;
		}else{
			if(valor1==null){
				return compararAttr(llave,StringUtils.EMPTY,valor2.toString());
			}else if(valor2==null){
				return compararAttr(llave,valor1.toString(),StringUtils.EMPTY);
			}else{
				return compararAttr(llave,valor1.toString(),valor2.toString());
			}
		}
	}
	private String compararAttr(String llave,Long valor1,Long valor2){
		if(valor1==null && valor2==null){
			return StringUtils.EMPTY;
		}else{
			if(valor1==null){
				return compararAttr(llave,StringUtils.EMPTY,valor2.toString());
			}else if(valor2==null){
				return compararAttr(llave,valor1.toString(),StringUtils.EMPTY);
			}else{
				return compararAttr(llave,valor1.toString(),valor2.toString());
			}
		}
	}
	
	private String opcional(String value){
		return StringUtils.isEmpty(value)?StringUtils.EMPTY:value;
	}
	
	private boolean compararItem(ItemStampDTO it, ItemStamp buscado) {
		StringBuilder sb = new StringBuilder();
		sb.append(compararAttr("COD",  it.getCdItem(),      buscado.getCdItem().toString()));
		sb.append(compararAttr("SEC",  it.getCdSeccion(),   buscado.getCdSeccion()));
		sb.append(compararAttr("X",    it.getNuCoorX(),     buscado.getNuCoorX()));
		sb.append(compararAttr("Y",    it.getNuCoorY(),     buscado.getNuCoorY()));
		sb.append(compararAttr("PARAM",it.getNbNombreVar(), buscado.getNbNombreVar()));
		sb.append(compararAttr("VALUE",it.getNbValorVar(),  buscado.getNbValorVar()));
		sb.append(compararAttr("SX",   it.getNuScX(),       buscado.getNuScX()));
		sb.append(compararAttr("SY",   it.getNuScY(),       buscado.getNuScY()));
		sb.append(compararAttr("WIDTH",it.getNuAncho(),     buscado.getNuAncho()));
		sb.append(compararAttr("ALIGN",it.getNuAlinear(),   buscado.getNuAlinear()));
		sb.append(compararAttr("FONT", it.getNbFuente(),    buscado.getNbFuente()));
		sb.append(compararAttr("MASK", it.getNbMask(),      opcional(buscado.getNbMask())));
		sb.append(compararAttr("TIPO", it.getChTipo(),      buscado.getChTipo()));
		sb.append(compararAttr("IDCLI",it.getNuIdxCliente(),buscado.getNuIdxCliente()));
		sb.append(compararAttr("STS",  it.getStActivo(),    buscado.getStActivo()));
		
		it.setResultado(sb.toString());
		
		if(it.getResultado().length()==0){
			return true;
		}
		
		System.out.println("["+it.getCdItem()+","+it.getChTipo()+","+it.getNbNombreVar()+"]");
		System.out.println("[fromSVG,fromDB]");
		System.out.println(it.getResultado());
		return false;
	}

	@SuppressWarnings("unchecked")
	public Result generarSVG(Long id) throws ServiceException {
		Result res = new Result();
		try {
			Result result = itemService.obtenerItemsPorSeccion(id);
			List<ItemStamp> items = (List<ItemStamp>) result.getEntities();
			
			Result resultSeccion = obtenerSeccionPorId(id);
			ItemSection section = (ItemSection) resultSeccion.getEntity();
			Result resultPlantilla = plantillaService.obtenerPlantillaPorCodigo(section.getCdPlantilla());
			Plantilla plantilla = (Plantilla) resultPlantilla.getEntity();
			String fileOriginal = FilenameUtils.concat(svgPath, plantilla.getNbNombreFormato()+"_"+section.getNuPagina()+suffix);
			String fileSalida = FilenameUtils.concat(tmpPath, plantilla.getNbNombreFormato()+"_"+section.getCdSeccion()+suffix);
			svgWorker.generarSVG(fileOriginal,fileSalida,items);
			res.setEntity(fileSalida);
		} catch (ServiceException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_GENERAR_SVG,"No se pudo generar svg para seccion.");
		}
		return res;
	}

	@Override
	public Result obtenerSeccionesPorPlantilla(Long cdPlantilla) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo secciones");
			final String uri = urlServices+"/rest/plantilla/{id}/seccion/all";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdPlantilla);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<ItemSection>> kitResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ItemSection>>() {}, params);
		    res.setEntities(kitResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	public String exportarScript(ItemStampDTO is, boolean inserts) throws ServiceException {
		StringBuilder sb = new StringBuilder();
		try {
			Map<String,Object> campos = new HashMap<String, Object>();
			if(inserts) {
				campos.put("CD_ITEM", "[CDITEM]");
				campos.put("CD_SECCION", "[CDSECCION]");
			}else {
				campos.put("CD_ITEM", "APDIGB.item_seq.NEXTVAL");
				campos.put("CD_SECCION", is.getCdSeccion());
			}
			campos.put("ST_ACTIVO", (is.getStActivo()==null)?Constants.ST_ACTIVO:is.getStActivo());
			campos.put("NU_COOR_X", is.getNuCoorX());
			campos.put("NU_COOR_Y", is.getNuCoorY());
			campos.put("NB_NOMBRE_VAR", is.getNbNombreVar());
			campos.put("NB_VALOR_VAR", is.getNbValorVar());
			campos.put("NB_FUENTE", (StringUtils.isEmpty(is.getNbFuente()))?Constants.FONT_COD_DEFAULT:is.getNbFuente());
			campos.put("NU_ALINEAR", (is.getNuAlinear()==null)?Constants.ALIGN_LEFT:is.getNuAlinear());
			campos.put("CH_TIPO", (is.getChTipo()==null)?Constants.TYPE_TEXT:is.getChTipo());
			campos.put("NU_ANCHO", is.getNuAncho());
			campos.put("NU_SC_X", is.getNuScX());
			campos.put("NU_SC_Y", is.getNuScY());
			campos.put("NB_MASK", is.getNbMask());
			campos.put("NU_IDX_CLIENTE", is.getNuIdxCliente());
			
			if(Constants.ST_IT_DEL.equals(is.getStSVGCarga())) {
				if(!inserts) {
					sb.append("DELETE APDIGB.TBG004_ITEM ");
					sb.append("WHERE CD_ITEM = ");
				    sb.append(is.getCdItem());
				    sb.append(";"+System.getProperty("line.separator"));
				}
			}else if(inserts || Constants.ST_IT_CRE.equals(is.getStSVGCarga())){
				sb.append("INSERT INTO APDIGB.TBG004_ITEM (");
				for (Map.Entry<String, Object> entry : campos.entrySet()){
				    sb.append(entry.getKey()+",");
				}
				sb.append(") VALUES(");
				for (Map.Entry<String, Object> entry : campos.entrySet()){
					if(entry.getKey().equals("CD_ITEM"))
						sb.append(entry.getValue()+",");
					else if(entry.getValue() instanceof String){
						sb.append("'"+entry.getValue()+"',");
					}else{
						sb.append(entry.getValue()+",");
					}
				}
				sb.append(");");
				sb.append(System.getProperty("line.separator"));
			}else if(Constants.ST_IT_MOD.equals(is.getStSVGCarga())){
				sb.append("UPDATE APDIGB.TBG004_ITEM SET ");
				for (Map.Entry<String, Object> entry : campos.entrySet()){
				    sb.append(entry.getKey()+"=");
				    if(entry.getValue() instanceof String){
						sb.append("'"+entry.getValue()+"',");
					}else{
						sb.append(entry.getValue()+",");
					}
				}
				sb.append("WHERE CD_ITEM = ");
			    sb.append(is.getCdItem());
			    sb.append(";"+System.getProperty("line.separator"));
			}
			String result = sb.toString();
			result = result.replace(",WHERE"," WHERE");
			result = result.replace(",)",")");
			return result;
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_GENERAR_SCRIPT,"No se pudo generar script desde svg.");
		}
	}

	@Override
	public Result aplicarCambiosSVG(ScriptDTO dto) throws ServiceException {
		// TODO Auto-generated method stub
		try {
			return null;
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_APLICAR_CAMBIOS,"No se pudo aplicar cambios desde svg.");
		}
	}

	@Override
	public Result guardarSeccion(ItemSection itemSection) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando seccion");
			final String uri = urlServices+"/rest/seccion";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ItemSection> request = new HttpEntity<ItemSection>(itemSection, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarSeccion(ItemSection itemSection) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando plantilla");
			final String uri = urlServices+"/rest/seccion/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<ItemSection> request = new HttpEntity<ItemSection>(itemSection, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarSeccion(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar cdSeccion : " + id);
			final String uri = urlServices+"/rest/seccion/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerSecciones() throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo secciones");
			final String uri = urlServices+"/rest/seccion/all";
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<ItemSection>> seccionResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<ItemSection>>() {});
		    res.setEntities(seccionResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	@Override
	public void generarSVGfromSeccion(Long id) throws ServiceException {
		try {
			Result resSeccion = obtenerSeccionPorId(id);
			ItemSection is = (ItemSection)resSeccion.getEntity();
			Result resPlantilla = plantillaService.obtenerPlantillaPorCodigo(is.getCdPlantilla());
			Plantilla plantilla = (Plantilla)resPlantilla.getEntity();
		    byte[] resPDF = plantillaService.obtenerPDF(plantilla.getCdPlantilla());
		    pdfWorker.generarSVGdesdePDF(plantilla,is.getNuPagina(),resPDF);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_GENERAR_SVG_PAGE, "No se pudo generar svg de la seccion");
		}
	}
	
}
