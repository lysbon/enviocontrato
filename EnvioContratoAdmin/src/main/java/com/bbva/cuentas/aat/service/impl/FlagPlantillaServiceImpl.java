package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.util.Result;

@Service
public class FlagPlantillaServiceImpl implements FlagPlantillaService{

	private static final Logger logger = LoggerFactory.getLogger(FlagPlantillaServiceImpl.class);

	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	@Override
	public Result obtenerFlagsPorPlantilla(Long cdPlantilla)
			throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo flags por plantilla");
			final String uri = urlServices+"/rest/plantilla/{id}/flag/all";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdPlantilla);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<FlagPlantilla>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<FlagPlantilla>>() {}, params);
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo flag plantilla por id");
			final String uri = urlServices+"/rest/plantilla/{id}/flag/{cdFlag}";
			Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdPlantilla);
		    params.put("cdFlag", cdFlag);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Enviroment> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Enviroment>() {}, params);
		    res.setEntity(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarFlagPlantilla(FlagPlantilla flagPlantilla)
			throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando flag plantilla");
			final String uri = urlServices+"/rest/plantilla/flag";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<FlagPlantilla> request = new HttpEntity<FlagPlantilla>(flagPlantilla, headers);
		    Map<String, Object> params = new HashMap<String, Object>();
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class,params);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarFlagPlantilla(FlagPlantilla flagPlantilla)
			throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando flagplantilla");
			final String uri = urlServices+"/rest/plantilla/flag/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<FlagPlantilla> request = new HttpEntity<FlagPlantilla>(flagPlantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarFlagPlantilla(FlagPlantilla flagPlantilla) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando flagplantilla");
			final String uri = urlServices+"/rest/plantilla/flag/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<FlagPlantilla> request = new HttpEntity<FlagPlantilla>(flagPlantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	
}
