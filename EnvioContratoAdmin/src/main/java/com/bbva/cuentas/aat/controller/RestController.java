package com.bbva.cuentas.aat.controller;

import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bbva.cuentas.util.Result;
import com.bbva.cuentas.aat.dto.ScriptDTO;
import com.bbva.cuentas.aat.dto.TipoFileUnico;
import com.bbva.cuentas.aat.service.CSVService;
import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.SqlService;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;

@Controller
@RequestMapping("/rest")
public class RestController {

	private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	
	@Autowired
	private KitService kitService;
	@Autowired
	private PlantillaService plantillaService;
	@Autowired
	private SeccionService seccionService;
	@Autowired
	private FlagPlantillaService flagPlantillaService;
	@Autowired
	private FlagService flagService;
	@Autowired
	private ItemService itemService;
	@Autowired
	private CSVService fileUnicoService;
	@Autowired
	private EnviromentService envService;
	@Autowired
	private SqlService sqlService;
	
	/************************
	 * 
	 * Kits
	 * 
	 *************************/
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/kit/all/{cdEnv}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Kit>> kits(@PathVariable("cdEnv") Integer cdEnv) {
		List<Kit> kits = null;
		try {
			Result result = kitService.obtenerKits(cdEnv);
			kits = (List<Kit>)result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Kit>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Kit>>(kits, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Kit> kit(@PathVariable Long id) {
		Kit kit = null;
		try {
			Result result = kitService.obtenerKitPorId(id);
			kit = (Kit) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Kit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(kit==null){
			return new ResponseEntity<Kit>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Kit>(kit, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/correo/{cdEnv}/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> correo(@PathVariable("cdEnv") Integer cdEnv,
			@PathVariable("id") Long id) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			Result result = kitService.obtenerMailTemplate(cdEnv,id);
			jsonInString = mapper.writeValueAsString(result.getEntity());
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(jsonInString, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/kit/{id}/plantillas", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Plantilla>> plantillasPorKit(@PathVariable Long id) {
		List<Plantilla> listaPlantillas;
		try {
			Result result = plantillaService.obtenerPlantillasAsociadas(id);
			listaPlantillas = (List<Plantilla>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Plantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Plantilla>>(listaPlantillas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarKit(@RequestBody Kit kit) {
		Long res = null;
		try {
			Result result = kitService.guardarKit(kit);
			res = (Long) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarKit(@RequestBody Kit kit) {
		Integer res = null;
		try {
			Result result = kitService.actualizarKit(kit);
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/kit/add", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarGrupoKit(@RequestBody EnvKit envKit){
		try {
			Result result = envService.guardarEnvKit(envKit);
			return new ResponseEntity<Integer>((Integer) result.getEntity(), HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value = "/grupo/kit/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarGrupoKit(@RequestBody EnvKit envKit){
		try {
			Result result = envService.eliminarEnvKit(envKit);
			return new ResponseEntity<Integer>((Integer) result.getEntity(), HttpStatus.OK);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/************************
	 * 
	 * Plantillas
	 * 
	 *************************/
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/plantilla/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Plantilla>> plantillas() {
		List<Plantilla> plantillas = null;
		try {
			Result result = plantillaService.obtenerPlantillas();
			plantillas = (List<Plantilla>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Plantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Plantilla>>(plantillas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Plantilla> plantillas(@PathVariable("id") Long id) {
		Plantilla plantilla = null;
		try {
			Result result = plantillaService.obtenerPlantillaPorCodigo(id);
			plantilla = (Plantilla) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Plantilla>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(plantilla == null){
			return new ResponseEntity<Plantilla>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Plantilla>(plantilla, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/plantilla/{id}/secciones", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ItemSection>> secciones(@PathVariable("id") Long id) {
		List<ItemSection> secciones = null;
		try {
			Result result = seccionService.obtenerSeccionesPorPlantilla(id);
			secciones = (List<ItemSection>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<ItemSection>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(secciones == null){
			return new ResponseEntity<List<ItemSection>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<ItemSection>>(secciones, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/plantilla/{id}/flags", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<FlagPlantilla>> flags(@PathVariable Long id) {
		logger.info("Obteniendo flags por plantilla"); 
		List<FlagPlantilla> listaFlags;
		try {
			Result result = flagPlantillaService.obtenerFlagsPorPlantilla(id);
			listaFlags = (List<FlagPlantilla>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<FlagPlantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<FlagPlantilla>>(listaFlags, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarPlantilla(@RequestBody Plantilla plantilla) {
		Long res = null;
		try {
			Result result = plantillaService.guardarPlantilla(plantilla);
			res = (Long) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarPlantilla(@RequestBody Plantilla plantilla) {
		Integer res = null;
		try {
			Result result = plantillaService.actualizarPlantilla(plantilla);
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> eliminarFlagPlantilla(@RequestBody Plantilla plantilla) {
		Integer res = null;
		try {
			Result result = plantillaService.eliminarPlantilla(plantilla.getCdPlantilla());
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/flag", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarFlagPlantilla(@RequestBody FlagPlantilla flagPlantilla) {
		Integer res = null;
		try {
			Result result = flagPlantillaService.guardarFlagPlantilla(flagPlantilla);
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
		
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/flags/{tipo}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Flag>> flags(@PathVariable("tipo") String tipo) {
		logger.info("Obteniendo flags por tipo"); 
		List<Flag> listaFlags;
		try {
			Result result = flagService.obtenerFlagsPorTipo(tipo);
			listaFlags = (List<Flag>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Flag>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(listaFlags == null){
			return new ResponseEntity<List<Flag>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Flag>>(listaFlags, HttpStatus.OK);
	}
	
	/************************
	 * 
	 * Secciones
	 * 
	 *************************/
	@RequestMapping(value = "/seccion/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ItemSection> seccion(@PathVariable("id") Long id) {
		ItemSection seccion = null;
		try {
			Result result = seccionService.obtenerSeccionPorId(id);
			seccion = (ItemSection) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<ItemSection>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(seccion == null){
			return new ResponseEntity<ItemSection>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItemSection>(seccion, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarSeccion(@RequestBody ItemSection seccion) {
		Long res = null;
		try {
			Result result = seccionService.guardarSeccion(seccion);
			res = (Long) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarSeccion(@RequestBody ItemSection seccion) {
		Integer res = null;
		try {
			Result result = seccionService.actualizarSeccion(seccion);
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<Integer> eliminarSeccion(@RequestBody ItemSection seccion) {
		Integer res = null;
		try {
			Result result = seccionService.eliminarSeccion(seccion.getCdSeccion());
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/seccion/{id}/items", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ItemStamp>> items(@PathVariable("id") Long id) {
		logger.info("Obteniendo items por seccion"); 
		List<ItemStamp> listaItems;
		try {
			Result result = itemService.obtenerItemsPorSeccion(id);
			listaItems = (List<ItemStamp>) result.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<ItemStamp>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(listaItems == null){
			return new ResponseEntity<List<ItemStamp>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<ItemStamp>>(listaItems, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/svg/{cdSeccion}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<ScriptDTO> svg(
			@PathVariable("cdSeccion") Long cdSeccion,
			@RequestBody String filename) {
		ScriptDTO salida = new ScriptDTO();
		try {
			Result result = seccionService.procesarSVG(cdSeccion,filename);
			salida = (ScriptDTO) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<ScriptDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(salida == null){
			return new ResponseEntity<ScriptDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ScriptDTO>(salida, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/pdf/{cdPlantilla}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> pdf(
			@PathVariable("cdPlantilla") Long cdPlantilla,
			@RequestBody String filename) {
		String salida = new String();
		try {
			Result result = plantillaService.actualizarPlantillaPDF(cdPlantilla,filename);
			salida = (String) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(salida == null){
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(salida, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/svg/{cdSeccion}/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> svgUpdate(
			@PathVariable("cdSeccion") Long cdSeccion,
			@RequestBody ScriptDTO dto) {
		Integer res = null;
		try {
			Result result = seccionService.aplicarCambiosSVG(dto);
			res = (Integer) result.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/fu/tipos", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<TipoFileUnico>> consultarFileUnicoTipos() {
		List<TipoFileUnico> res = null;
		try {
			res = fileUnicoService.obtenerTipos();
		} catch (ServiceException e) {
			return new ResponseEntity<List<TipoFileUnico>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<TipoFileUnico>>(res, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/fu/tipo/{idTipo}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<TipoFileUnico> consultarFileUnicoTipo(
			@PathVariable("idTipo") String idTipo) {
		TipoFileUnico res = null;
		try {
			res = fileUnicoService.obtenerTipoPorCodigo(idTipo);
		} catch (ServiceException e) {
			return new ResponseEntity<TipoFileUnico>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<TipoFileUnico>(res, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<Map<String, Object>>> query(@RequestBody String query) {
		List<Map<String, Object>> res;
		try {
			Result tmp = sqlService.query(query);
			res = (List<Map<String, Object>>) tmp.getEntities();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Map<String, Object>>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Map<String, Object>>>(res, HttpStatus.OK);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/execute", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, Object>> execute(@RequestBody String query) {
		Map<String, Object> res;
		try {
			Result tmp = sqlService.execute(query);
			res = (Map<String, Object>) tmp.getEntity();
		} catch (ServiceException e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(res, HttpStatus.OK);
	}
	
} 