package com.bbva.cuentas.aat.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.comparator.KitComparator;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.util.Result;

@Service
public class KitServiceImpl implements KitService{

	private static final Logger logger = LoggerFactory.getLogger(KitServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Autowired
	KitComparator comparator;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	public Result obtenerKits(Integer cdEnv) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo kits");
			final String uri = urlServices+"/rest/kit/grupo/{cdEnv}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("cdEnv", cdEnv);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Kit>> kitResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Kit>>() {}, params);
		    List<Kit> kits = kitResponse.getBody();
		    Collections.sort(kits,comparator);
		    res.setEntities(kits);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	public Result obtenerMailTemplate(Integer cdEnv,Long cdKit) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo correo kit");
			final String uri = urlServices+"/rest/kit/correo/{cdEnv}/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("cdEnv", cdEnv);
		    params.put("id", cdKit);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<String> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<String>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result obtenerKitPorCodigo(Integer cdEnv, String codigo) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo kit por codigo");
			final String uri = urlServices+"/rest/kit/codigo/{cdEnv}/{codigo}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("cdEnv", cdEnv);
		    params.put("codigo", codigo);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Kit> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Kit>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	@Override
	public Result obtenerKitPorId(Long cdKit) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Obteniendo kit por id");
			final String uri = urlServices+"/rest/kit/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", cdKit);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Kit> itemResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Kit>() {}, params);
		    res.setEntity(itemResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarKit(Kit kit) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando kit");
			final String uri = urlServices+"/rest/kit";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Kit> request = new HttpEntity<Kit>(kit, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Long> res = restTemplate.postForEntity(uri, request, Long.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarKit(Kit kit) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando kit");
			final String uri = urlServices+"/rest/kit/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Kit> request = new HttpEntity<Kit>(kit, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarKit(Long id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar kit : " + id);
			final String uri = urlServices+"/rest/kit/delete/{id}";
		    Map<String, Object> params = new HashMap<String, Object>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("asociando kit plantilla");
			final String uri = urlServices+"/rest/kit/plantilla/add";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    HttpEntity<KitPlantilla> request = new HttpEntity<KitPlantilla>(kitPlantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class,params);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("eliminando kit plantilla");
			final String uri = urlServices+"/rest/kit/plantilla/delete";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    HttpEntity<KitPlantilla> request = new HttpEntity<KitPlantilla>(kitPlantilla, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class,params);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}
	
}
