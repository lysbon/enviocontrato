package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.ResourceItem;
import com.bbva.cuentas.util.Result;

public interface ResourceService {
	
	public byte[] obtenerImagen(Long id) throws ServiceException;
	
	public Result obtenerImagenes() throws ServiceException;

	public byte[] obtenerSvgSeccion(Long id) throws ServiceException;
	
	public Result guardarRecurso(ResourceItem recurso) throws ServiceException;
	
	public Result actualizarRecurso(ResourceItem recurso) throws ServiceException;
	
	public Result eliminarRecurso(Long id) throws ServiceException;
	
}
