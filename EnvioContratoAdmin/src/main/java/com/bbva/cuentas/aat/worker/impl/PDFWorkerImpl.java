package com.bbva.cuentas.aat.worker.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.util.SystemUnit;
import com.bbva.cuentas.aat.worker.PDFWorker;
import com.bbva.cuentas.aat.worker.SVGWorker;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Constants;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;

@Component
public class PDFWorkerImpl implements PDFWorker{
	
	@Value("${fileupload.pdf.path}")
	private String rutaFormatos;
	
	@Value("${inkscape.util.path}")
	private String inkscapeUtil;
	
	@Value("${fileupload.tmp.path}")
	private String rutaTMP;
	
	@Autowired
	SVGWorker svgWorker;
	
	@Override
	public List<String> generarSVGdesdePDF(Plantilla plantilla,int numPage,byte[] pdf) {
		try {
			List<String> pdfPorPaginas  = splitPDF(plantilla.getNbNombreFormato()+".pdf",numPage,pdf);
			List<String> svgConvertidos = executeInkscape(pdfPorPaginas);
			List<String> svgRefinados   = refinarSVG(svgConvertidos);
			return svgRefinados;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<String> generarSVGdesdePDF(Plantilla plantilla,byte[] pdf) {
		try {
			List<String> pdfPorPaginas  = splitPDF(plantilla.getNbNombreFormato()+".pdf",pdf);
			List<String> svgConvertidos = executeInkscape(pdfPorPaginas);
			List<String> svgRefinados   = refinarSVG(svgConvertidos);
			return svgRefinados;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<String> generarSVGdesdePDF(Plantilla plantilla) {
		try {
			String rutaPDF = rutaFormatos+File.separatorChar+"pdf";
			String filename = new String();
			if(Constants.ST_ACTIVO.equals(plantilla.getStActivo())){
				if(Constants.TIPO_FILEUNICO_CONTRATO.equals(plantilla.getNbTipoFileunico())){
					filename = rutaPDF+File.separatorChar+"contratos"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}else if(Constants.TIPO_FILEUNICO_SOLICITUD.equals(plantilla.getNbTipoFileunico())){
					filename = rutaPDF+File.separatorChar+"solicitud"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}else if(Constants.TIPO_FILEUNICO_FATCA.equals(plantilla.getNbTipoFileunico())){
					filename = rutaPDF+File.separatorChar+"fatca"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}else if(Constants.TIPO_FILEUNICO_PEP.equals(plantilla.getNbTipoFileunico())){
					filename = rutaPDF+File.separatorChar+"pep"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}else if(Constants.TIPO_FILEUNICO_DAE.equals(plantilla.getNbTipoFileunico())){
					filename = rutaPDF+File.separatorChar+"dae"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}else{
					filename = rutaPDF+File.separatorChar+"otros"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
				}
			}
			
			List<String> pdfPorPaginas  = splitPDF(filename);
			List<String> svgConvertidos = executeInkscape(pdfPorPaginas);
			List<String> svgRefinados   = refinarSVG(svgConvertidos);
			return svgRefinados;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<String> splitPDF(String filename,byte[] pdf) throws IOException, DocumentException{
		List<String> output = new ArrayList<String>();
		PdfReader reader = new PdfReader(pdf);
		int n = reader.getNumberOfPages();
		int i = 1;
		
		String directory = FilenameUtils.getFullPath(rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar);
		AppUtil.generarRuta(directory);

        while ( i < n ) {
        	String basename = FilenameUtils.getBaseName(filename);
            String outFile  = rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar+
            		basename+"_" + i + ".pdf";
            Document document = new Document(reader.getPageSizeWithRotation(1));
            PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
            document.open();
            PdfImportedPage page = writer.getImportedPage(reader,i);
            writer.addPage(page);
            document.close();
            writer.close();
            output.add(outFile);
        }
        return output;
	}
	
	public List<String> splitPDF(String filename,int numPage,byte[] pdf) throws IOException, DocumentException{
		List<String> output = new ArrayList<String>();
		PdfReader reader = new PdfReader(pdf);
		int n = reader.getNumberOfPages();
		if(numPage<=n){

			String directory = FilenameUtils.getFullPath(rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar);
			AppUtil.generarRuta(directory);

			String basename = FilenameUtils.getBaseName(filename);
            String outFile  = rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar+
            		basename+"_" + numPage + ".pdf";
            Document document = new Document(reader.getPageSizeWithRotation(1));
            PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
            document.open();
            PdfImportedPage page = writer.getImportedPage(reader, numPage);
            writer.addPage(page);
            document.close();
            writer.close();
            output.add(outFile);
		}
        return output;
	}
	
	public List<String> splitPDF(String filename) throws IOException, DocumentException{
		List<String> output = new ArrayList<String>();
		PdfReader reader = new PdfReader(filename);
		int n = reader.getNumberOfPages();
		int i = 1;       
		
		String directory = FilenameUtils.getFullPath(rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar);
		AppUtil.generarRuta(directory);

        while ( i < n ) {
        	String basename = FilenameUtils.getBaseName(filename);
            String outFile  = rutaTMP+File.separatorChar+
            		"results"+File.separatorChar+
            		"merge"+File.separatorChar+
            		basename+"_" + i + ".pdf";
            Document document = new Document(reader.getPageSizeWithRotation(1));
            PdfCopy writer = new PdfCopy(document, new FileOutputStream(outFile));
            document.open();
            PdfImportedPage page = writer.getImportedPage(reader, ++i);
            writer.addPage(page);
            document.close();
            writer.close();
            output.add(outFile);
        }
        return output;
	}
	
	public List<String> executeInkscape(List<String> filenames){
		List<String> output = new ArrayList<String>();
		for(String filename : filenames){
			String basename = FilenameUtils.getBaseName(filename);
			String outFile = rutaFormatos +File.separatorChar+"svg"+File.separatorChar+basename+".svg ";
			String command  = inkscapeUtil+" --without-gui --file "+filename+" "
					//+ "--export-text-to-path "
					+ "--export-plain-svg="+outFile;
			executeCommand(command);
			output.add(outFile);
		}
		return output;
	}
	
	private String executeCommand(String command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			System.out.println(command);
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(p.getInputStream()));
            String line = "";
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(output);
		return output.toString();
	}
	
	private List<String> refinarSVG(List<String> svgConvertidos) {
		for(String filename : svgConvertidos){
			svgWorker.toSystemUnit(filename,filename,SystemUnit.PX,SystemUnit.PT);
		}
		return svgConvertidos;
	}

}
