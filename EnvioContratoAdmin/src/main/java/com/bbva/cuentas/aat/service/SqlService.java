package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.util.Result;

public interface SqlService {

	public Result execute(String query) throws ServiceException;
	
	public Result query(String query) throws ServiceException;
	
}
