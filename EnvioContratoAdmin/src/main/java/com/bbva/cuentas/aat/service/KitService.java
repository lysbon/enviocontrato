package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.util.Result;

public interface KitService {

	public Result obtenerKits(Integer cdEnv) throws ServiceException;

	public Result obtenerKitPorId(Long cdKit) throws ServiceException;
	
	public Result obtenerKitPorCodigo(Integer cdEnv, String codigo) throws ServiceException;
	
	public Result obtenerMailTemplate(Integer cdEnv, Long cdKit) throws ServiceException;
	
	public Result guardarKit(Kit kit) throws ServiceException;
	
	public Result actualizarKit(Kit kit) throws ServiceException;
	
	public Result eliminarKit(Long id) throws ServiceException;
	
	public Result guardarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException;
	
	public Result eliminarKitPlantilla(KitPlantilla kitPlantilla) throws ServiceException;
	
}
