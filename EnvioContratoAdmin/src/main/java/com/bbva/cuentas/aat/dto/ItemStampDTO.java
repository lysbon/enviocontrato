package com.bbva.cuentas.aat.dto;

import com.bbva.cuentas.bean.Generic;
import com.bbva.cuentas.util.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemStampDTO extends Generic{
	
	private String   cdItem;
	private Long     cdSeccion;
	private Integer  nuCoorX;
	private Integer  nuCoorY;
	private String   nbNombreVar;
	private String   nbValorVar;
	private String   nbFuente;
	private Integer  nuAlinear;
	private String   chTipo;
	private Integer  nuAncho;
	private Integer  nuScX;
	private Integer  nuScY;
	private Integer  nuIdxCliente;
	private String   nbMask;
	private String   stActivo;
	
	private String   stSVGCarga;
	private String   resultado;
	
	public String getStSVGCarga() {
		return stSVGCarga;
	}
	public void setStSVGCarga(String stSVGCarga) {
		this.stSVGCarga = stSVGCarga;
	}
	
	public String getDesEstadoSvg(){
		if(Constants.ST_IT_CRE.equals(stSVGCarga)){
			return Constants.ST_DES_CRE;
		}else if(Constants.ST_IT_MOD.equals(stSVGCarga)){
			return Constants.ST_DES_MOD;
		}else if(Constants.ST_IT_DEL.equals(stSVGCarga)){
			return Constants.ST_DES_DEL;
		}else if(Constants.ST_IT_SIN.equals(stSVGCarga)){
			return Constants.ST_DES_SIN;
		}else{
			return Constants.TYPE_DES_UNDEFINED;
		}
	}
	
	public void setDesTipo(String desTipo){
		
	}
	
	public String getDesTipo(){
		if(Constants.TYPE_TEXT.equals(chTipo)){
			return Constants.TYPE_DES_TEXT;
		}else if(Constants.TYPE_IMAGE.equals(chTipo)){
			return Constants.TYPE_DES_IMAGE;
		}else if(Constants.TYPE_BAR_CODE.equals(chTipo)){
			return Constants.TYPE_DES_BAR_CODE;
		}else{
			return Constants.TYPE_DES_UNDEFINED;
		}
	}
	
	public String getNbMask() {
		return nbMask;
	}

	public void setNbMask(String nbMask) {
		this.nbMask = nbMask;
	}
	
	private byte[] img;
	
	public ItemStampDTO(){		
	}
	
	public ItemStampDTO(String _label,String _value,Integer _x,Integer _y){
		nbNombreVar = _label;
		nbValorVar = _value;
		nuCoorX = _x;
		nuCoorY = _y;
		nuAlinear = Constants.ALIGN_LEFT;
		chTipo = Constants.TYPE_TEXT;
	}
	public ItemStampDTO(String _label,Integer _x,Integer _y, String _type){
		nbNombreVar = _label;
		nuCoorX = _x;
		nuCoorY = _y;
		nuAlinear = Constants.ALIGN_LEFT;
		chTipo = _type;
	}

	public String getCdItem() {
		return cdItem;
	}

	public void setCdItem(String cdItem) {
		this.cdItem = cdItem;
	}

	public Long getCdSeccion() {
		return cdSeccion;
	}

	public void setCdSeccion(Long cdSeccion) {
		this.cdSeccion = cdSeccion;
	}

	public Integer getNuCoorX() {
		return nuCoorX;
	}

	public void setNuCoorX(Integer nuCoorX) {
		this.nuCoorX = nuCoorX;
	}

	public Integer getNuCoorY() {
		return nuCoorY;
	}

	public void setNuCoorY(Integer nuCoorY) {
		this.nuCoorY = nuCoorY;
	}

	public String getNbNombreVar() {
		return nbNombreVar;
	}

	public void setNbNombreVar(String nbNombreVar) {
		this.nbNombreVar = nbNombreVar;
	}

	public String getNbValorVar() {
		return nbValorVar;
	}

	public void setNbValorVar(String nbValorVar) {
		this.nbValorVar = nbValorVar;
	}

	public String getNbFuente() {
		return nbFuente;
	}

	public void setNbFuente(String nbFuente) {
		this.nbFuente = nbFuente;
	}

	public Integer getNuAlinear() {
		return nuAlinear;
	}

	public void setNuAlinear(Integer nuAlinear) {
		this.nuAlinear = nuAlinear;
	}
	public Integer getNuAncho() {
		return nuAncho;
	}

	public void setNuAncho(Integer nuAncho) {
		this.nuAncho = nuAncho;
	}

	public Integer getNuScX() {
		return nuScX;
	}

	public void setNuScX(Integer nuScX) {
		this.nuScX = nuScX;
	}

	public Integer getNuScY() {
		return nuScY;
	}

	public void setNuScY(Integer nuScY) {
		this.nuScY = nuScY;
	}

	public Integer getNuIdxCliente() {
		return nuIdxCliente;
	}

	public void setNuIdxCliente(Integer nuIdxCliente) {
		this.nuIdxCliente = nuIdxCliente;
	}
	public String getChTipo() {
		return chTipo;
	}

	public void setChTipo(String chTipo) {
		this.chTipo = chTipo;
	}
	

	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public String getStActivo() {
		return stActivo;
	}

	public void setStActivo(String stActivo) {
		this.stActivo = stActivo;
	}
	
	
	
	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cdItem == null) ? 0 : cdItem.hashCode());
		result = prime * result
				+ ((cdSeccion == null) ? 0 : cdSeccion.hashCode());
		result = prime * result + ((chTipo == null) ? 0 : chTipo.hashCode());
		result = prime * result
				+ ((nbNombreVar == null) ? 0 : nbNombreVar.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemStampDTO other = (ItemStampDTO) obj;
		if (cdItem == null) {
			if (other.cdItem != null)
				return false;
		} else if (!cdItem.equals(other.cdItem))
			return false;
		if (cdSeccion == null) {
			if (other.cdSeccion != null)
				return false;
		} else if (!cdSeccion.equals(other.cdSeccion))
			return false;
		if (chTipo == null) {
			if (other.chTipo != null)
				return false;
		} else if (!chTipo.equals(other.chTipo))
			return false;
		if (nbNombreVar == null) {
			if (other.nbNombreVar != null)
				return false;
		} else if (!nbNombreVar.equals(other.nbNombreVar))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemStamp [cdItem=" + cdItem + ", cdSeccion=" + cdSeccion + ", nuCoorX=" + nuCoorX + ", nuCoorY="
				+ nuCoorY + ", nbNombreVar=" + nbNombreVar + ", nbValorVar=" + nbValorVar + "]";
	}
	
	
	
}