package com.bbva.cuentas.aat.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.bbva.cuentas.aat.util.AppUtil;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {
	
	@Value("${fileupload.tmp.path}")
	private String rootPath;
	
	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody String uploadFileHandler(@RequestParam("name") String name,
			@RequestParam("file") MultipartFile file) {
		
		
		if (!file.isEmpty()) {
			BufferedOutputStream stream=null;
			try {
				byte[] bytes = file.getBytes(); 
				File dir = new File(rootPath); 
				if (!dir.exists())
					dir.mkdirs();
				// Create the file on server
				String filename = AppUtil.removeAccents(file.getOriginalFilename());
				File serverFile = new File(dir.getAbsolutePath()+File.separator+filename);
				stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				return serverFile.getName();
			} catch (Exception e) {
				return "You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage();
			}finally {
				try {if (stream!=null) stream.close();} catch (IOException e) {}

			}
		} else {
			return "You failed to upload " + file.getOriginalFilename()
					+ " because the file was empty.";
		}
	}

	/**
	 * Upload multiple file using Spring Controller
	 */
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST)
	public @ResponseBody
	String uploadMultipleFileHandler(@RequestParam("name") String[] names,
			@RequestParam("file") MultipartFile[] files) {

		if (files.length != names.length) return "Mandatory information missing";

		String message = "";
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			String name = names[i];
			BufferedOutputStream stream = null;
			try {
				byte[] bytes = file.getBytes();

				// Creating the directory to store file
				String rootPath = System.getProperty("catalina.home");
				File dir = new File(rootPath + File.separator + "tmpFiles");
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
				stream = new BufferedOutputStream(new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();

				//"System.out.println("Server File Location=" + serverFile.getAbsolutePath());"
				message = message + "You successfully uploaded file=" + name+ "";
				
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}finally {
				try {if (stream!=null) stream.close();} catch (IOException e) {}

			}
		}
		
		return message;
	}
}