package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.ServiceException;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.util.Result;

@Service
public class EnviromentServiceImpl implements EnviromentService{

	private static final Logger logger = LoggerFactory.getLogger(EnviromentServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	public Result obtenerEnvs() throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo los grupos");
			final String uri = urlServices+"/rest/grupo/all";
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Enviroment>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Enviroment>>() {}, params);
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;	
	}

	public Result obtenerEnvPorCodigo(Integer id) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo grupo por id");
			final String uri = urlServices+"/rest/grupo/{id}";
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Enviroment> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Enviroment>() {}, params);
		    res.setEntity(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
	public Result obtenerEnvPorTipo(String tipo) throws ServiceException{
		Result res = new Result();
		try {
			logger.info("Obteniendo los grupos por tipo");
			final String uri = urlServices+"/rest/grupo/{tipo}";
		    Map<String, String> params = new HashMap<String, String>();
		    params.put("tipo", tipo);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<List<Enviroment>> grupoResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Enviroment>>() {}, params);
		    res.setEntities(grupoResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarEnv(Enviroment env) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("registrando grupo");
			final String uri = urlServices+"/rest/grupo";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Enviroment> request = new HttpEntity<Enviroment>(env, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result actualizarEnv(Enviroment env) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("actualizando grupo");
			final String uri = urlServices+"/rest/grupo/update";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<Enviroment> request = new HttpEntity<Enviroment>(env, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarEnv(Integer id) throws ServiceException {
		Result res = new Result();
		try {
			logger.info("Eliminar grupo : " + id);
			final String uri = urlServices+"/rest/grupo/delete/{id}";
		    Map<String, Integer> params = new HashMap<String, Integer>();
		    params.put("id", id);
		    RestTemplate restTemplate = tokenService.getTemplate();
		    ResponseEntity<Integer> flujosResponse = restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<Integer>() {}, params);
		    res.setEntity(flujosResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result guardarEnvKit(EnvKit envKit) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("asociando kit grupo");
			final String uri = urlServices+"/rest/grupo/kit/add";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<EnvKit> request = new HttpEntity<EnvKit>(envKit, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
	    return result;
	}

	@Override
	public Result eliminarEnvKit(EnvKit envKit) throws ServiceException {
		Result result = new Result();
		try {
			logger.info("Eliminar grupo kit : " + envKit.getCdEnv()+","+envKit.getCdKit());
			final String uri = urlServices+"/rest/grupo/kit/delete";
			HttpHeaders headers = new HttpHeaders();
		    headers.setContentType(MediaType.APPLICATION_JSON);
		    HttpEntity<EnvKit> request = new HttpEntity<EnvKit>(envKit, headers);
		    RestTemplate restTemplate = tokenService.getTemplate();
			ResponseEntity<Integer> res = restTemplate.postForEntity(uri, request, Integer.class);
			result.setEntity(res.getBody());
		} catch (Exception e) {
			result.setErrorCode("ERROR");
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return result;
	}
	
}
