package com.bbva.cuentas.aat.util;

/**
 * Clase que representa una condicion usada en filtros de queries y updates.<BR><BR>
 * 
 * Usa enum Operador:  AND , OR
 *  
 * 
 * @author christian.chavez
 *
 */
public class SqlCondicion {
	
	private SqlOperador operador;
	private String nombre;
	private Object valor;
	
	public SqlCondicion(String nombre,Object valor){
		this(null, nombre, valor);
	}
	
	public SqlCondicion(SqlOperador operator,String nombre,Object valor){
		this.operador=operator;
		this.nombre=nombre;
		this.valor=valor;
	} 

	public SqlOperador getOperador() {
		return operador;
	}

	public void setOperador(SqlOperador operador) {
		this.operador = operador;
	} 

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Object getValor() {
		return valor;
	} 

	public void setValor(Object valor) {
		this.valor = valor;
	}
	
	
}
