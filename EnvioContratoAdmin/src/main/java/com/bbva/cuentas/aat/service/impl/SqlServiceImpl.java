package com.bbva.cuentas.aat.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.aat.service.SqlService;
import com.bbva.cuentas.aat.service.TokenService;
import com.bbva.cuentas.aat.util.AppUtil;
import com.bbva.cuentas.util.Result;

@Service
public class SqlServiceImpl implements SqlService{

	private static final Logger logger = LoggerFactory.getLogger(SqlServiceImpl.class);
	
	@Autowired
	TokenService tokenService;
	
	@Value("${servicios.ruta}")
	private String urlServices;
	
	@Override
	public Result execute(String query) {
		Result res = new Result();
		try {
			final String uri = urlServices+"/rest/sql/execute";
		    Map<String, Object> params = new HashMap<String, Object>();
		    RestTemplate restTemplate = tokenService.getTemplate();
		    HttpEntity<String> requestEntity = new HttpEntity<String>(query);
		    ResponseEntity<Map<String, Object>> executeResponse = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<Map<String, Object>>() {}, params);
		    res.setEntity(executeResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}

	@Override
	public Result query(String query) {
		Result res = new Result();
		try {
			final String uri = urlServices+"/rest/sql/query";
		    Map<String, Object> params = new HashMap<String, Object>();
		    RestTemplate restTemplate = tokenService.getTemplate();
		    HttpEntity<String> requestEntity = new HttpEntity<String>(query);
		    ResponseEntity<List<Map<String, Object>>> executeResponse = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Map<String, Object>>>() {}, params);
		    res.setEntities(executeResponse.getBody());
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		return res;
	}
	
}
