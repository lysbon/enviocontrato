package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.util.Result;

public interface EnviromentService {

	public Result obtenerEnvs() throws ServiceException;

	public Result obtenerEnvPorCodigo(Integer id) throws ServiceException;
	
	public Result obtenerEnvPorTipo(String tipo) throws ServiceException;
	
	public Result guardarEnv(Enviroment env) throws ServiceException;
	
	public Result actualizarEnv(Enviroment env) throws ServiceException;
	
	public Result eliminarEnv(Integer id) throws ServiceException;

	public Result guardarEnvKit(EnvKit envKit) throws ServiceException;

	public Result eliminarEnvKit(EnvKit envKit) throws ServiceException;
	
}
