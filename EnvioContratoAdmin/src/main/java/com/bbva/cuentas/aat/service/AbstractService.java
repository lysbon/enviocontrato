package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.util.Result;

public interface AbstractService<S,T> {

	public Result obtenerEntidades() throws ServiceException;

	public Result obtenerEntidadesPorGrupo(Integer cdEnv) throws ServiceException;
	
	public Result obtenerEntidadPorId(S id) throws ServiceException;
	
	public Result obtenerEntidadPorTipo(Integer cdEnv,String tipo) throws ServiceException;
	
	public Result obtenerEntidadPorEstado(Integer cdEnv,String estado) throws ServiceException;
	
	public Result guardarEntidad(T env) throws ServiceException;
	
	public Result actualizarEntidad(T env) throws ServiceException;
	
	public Result eliminarEntidad(T id) throws ServiceException;

	public byte[] obtenerRecursoPorId(S id) throws ServiceException;
	
}
