/**
 * FirmarContratosPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public interface FirmarContratosPortService extends javax.xml.rpc.Service {
    public java.lang.String getFirmarContratosPortSoap11Address();

    public com.bbva.cuentas.firmarContratos.FirmarContratosPort getFirmarContratosPortSoap11() throws javax.xml.rpc.ServiceException;

    public com.bbva.cuentas.firmarContratos.FirmarContratosPort getFirmarContratosPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
