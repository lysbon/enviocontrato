package com.bbva.cuentas;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.cuentas.firmarContratos.Cliente;
import com.bbva.cuentas.firmarContratos.FirmaContrato;
import com.bbva.cuentas.firmarContratos.FirmarContratosPort;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortService;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortServiceLocator;
import com.bbva.cuentas.firmarContratos.FirmarContratosRequest;
import com.bbva.cuentas.firmarContratos.FirmarContratosResponse;
import com.bbva.cuentas.firmarContratos.ItemContrato;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.Util;

public class Application {
	
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static void main(String[] args){
		String pathFile = null;
		if(args!=null && args.length>0){
			pathFile = args[0];
		}else{
			pathFile = "input.txt";
		}
		Application parser = new Application(pathFile);
		logger.debug("Procesar archivo de entrada");
	    boolean result = parser.processLine();
	    if(result){
	    	logger.debug("Invocar servicio apertura digital");
	    	parser.invocarServicio();
	    }
	}
	
	public Application(String aFileName){
		fFilePath = aFileName;
	}

	public void invocarServicio(){
		FirmarContratosPortService enviarContratoService = new FirmarContratosPortServiceLocator();
		((FirmarContratosPortServiceLocator)enviarContratoService).setFirmarContratosPortSoap11EndpointAddress(obtenerEndpoint());
		try {
			FirmarContratosPort port = enviarContratoService.getFirmarContratosPortSoap11();
			FirmarContratosResponse response = port.firmarContratos(obj);
			if(Constants.COD_EXITO_WS.equals(response.getCodigoResultado())){
				output14(Constants.COD_EXITO,"El proceso se realiz� correctamente.");
			}else{
				output14(Constants.COD_NO_EXITO_SERVICE,"Error al invocar servicio web de apertura digital. "+response.getMensajeResultado());
			}
		} catch (ServiceException e) {
			output14(Constants.COD_NO_EXITO_SERVICE,"Error al conectar con servicio web de apertura digital. "+e.getMessage());
		} catch (RemoteException e) {
			output14(Constants.COD_NO_EXITO_SERVICE,"Error al invocar servicio web de apertura digital. "+e.getMessage());
		}
	}
	
	private String obtenerEndpoint() {
		return Constants.URL_WEB_SERVICE;
	}

	public String obtenerRutaSalida() {
		try {
			File fileInput = new File(fFilePath);
			return fileInput.getAbsoluteFile().getParent()+"/output_contratodigital.txt";
		}catch(Exception e){
			return "output_contratodigital.txt";
		}
	}
	
	public void output14(String mensaje,String mensaje2) {

		FileOutputStream fop = null;
		File file;

		try {

			String rutaSalida = obtenerRutaSalida();
			
			logger.debug(rutaSalida);
						
			file = new File(rutaSalida);
			fop = new FileOutputStream(file);
			if (!file.exists() && !file.createNewFile()) {
				logger.debug("Error al crear archivo");
			}
			String output = mensaje;
			if(mensaje2!=null){
				output += mensaje2;
			}
			byte[] contentInBytes = output.getBytes();

			fop.write(contentInBytes);
			fop.flush();
			fop.close();

			logger.debug("Done");

		} catch (IOException e) {
			logger.error(e.getMessage());
		} finally {
			try {
				if (fop != null) {
					fop.close();
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	public final boolean processLine(){
		obj   = new FirmarContratosRequest();
		BufferedReader br = null;
		listaItems = new ArrayList<ItemContrato>();
		try {
			String line;
			int index = 0;
			br = new BufferedReader(new FileReader(fFilePath));
			while ((line = br.readLine()) != null) {
				processLineItem(line,index);
				index++;
			}
			br.close();
			
			ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
			listaItems.toArray(itemsContrato);
			
			imprimirParametria(itemsContrato);
			
			logger.debug("Validar items");
			validarListaItems(itemsContrato);
				
			List<Cliente> listClientes = new ArrayList<Cliente>();
			Cliente c1 = construirCliente1(itemsContrato);
			if(c1!=null) listClientes.add(c1);
			Cliente c2 = construirCliente2(itemsContrato);
			if(c2!=null) listClientes.add(c2);
			Cliente[] listaClientes = new Cliente[listClientes.size()];
			listaClientes = listClientes.toArray(listaClientes);
			
			FirmaContrato firmaContrato = new FirmaContrato();
			firmaContrato.setListaClientes(listaClientes);
			firmaContrato.setListaItems(itemsContrato);
			firmaContrato.setIdContrato(buscarNombreFormato(itemsContrato));
			firmaContrato.setNumeroContrato(buscarNumeroContrato(itemsContrato));
			firmaContrato.setOficinaGestora(buscarOficinaGestora(itemsContrato));
			firmaContrato.setProcedencia(buscarProcedencia(itemsContrato));
			obj.setFirmaContrato(firmaContrato);
						
		} catch (Exception e) {
			output14(Constants.COD_NO_EXITO,"Error al procesar archivo de entrada. "+e.getMessage());
			return false;
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				logger.debug(e.getMessage());
			}
		}
		return true;
	}
	
	private void imprimirParametria(ItemContrato[] itemsContrato) {
		for(int i=0;i<itemsContrato.length;i++){
			logger.debug("<fir:listaItems>");
			logger.debug("<fir:label>");
			logger.debug(itemsContrato[i].getLabel());
			logger.debug("</fir:label>");
			logger.debug("<fir:value>");
			logger.debug(itemsContrato[i].getValue());
			logger.debug("</fir:value>");
			logger.debug("</fir:listaItems>");
		}
	}

	private void validarListaItems(ItemContrato[] itemsContrato) {
		if(itemsContrato==null || itemsContrato.length==0){
			throw new IllegalArgumentException("La lista no contiene parametros");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_IDFORMATO)==null){
			throw new IllegalArgumentException("Debe enviar nombre de formato");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_OFIGESTOR)==null){
			throw new IllegalArgumentException("Debe enviar oficina gestora");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_NROCONTRATO)==null){
			throw new IllegalArgumentException("Debe enviar numero de contrato");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_TRX_HUELLA)==null){
			throw new IllegalArgumentException("Debe enviar numero de transaccion huella digital");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_CODCEN_TIT1)==null){
			throw new IllegalArgumentException("Debe enviar codigo central de titular");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_TIPDOC_TIT1)==null){
			throw new IllegalArgumentException("Debe enviar tipo de documento de titular");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_NRODOC_TIT1)==null){
			throw new IllegalArgumentException("Debe enviar numero de documento de titular");
		}
		if(buscarParametro(itemsContrato,Constants.COD_PARAM_EMAIL_TIT1)==null){
			throw new IllegalArgumentException("Debe enviar email de titular");
		}
	}
	
	private Cliente construirCliente1(ItemContrato[] itemsContrato) {
		return construirCliente(itemsContrato,
				Constants.COD_PARAM_CODCEN_TIT1,
				Constants.COD_PARAM_TIPDOC_TIT1,
				Constants.COD_PARAM_NRODOC_TIT1,
				Constants.COD_PARAM_EMAIL_TIT1,
				Constants.COD_PARAM_TRX_HUELLA);
	}
	private Cliente construirCliente2(ItemContrato[] itemsContrato) {
		return construirCliente(itemsContrato,
				Constants.COD_PARAM_CODCEN_TIT2,
				Constants.COD_PARAM_TIPDOC_TIT2,
				Constants.COD_PARAM_NRODOC_TIT2,
				Constants.COD_PARAM_EMAIL_TIT2,"");
	}
	
	private Cliente construirCliente(ItemContrato[] itemsContrato,
			String paramCodCen, String paramTipDoc, String paramNroDoc,String paramEmail, String paramTrxHuella) {
		Cliente cliente = new Cliente();
		for(int i=0;i<itemsContrato.length;i++){
			if(paramCodCen.equals(itemsContrato[i].getLabel())){
				cliente.setCodigoCentral(itemsContrato[i].getValue());
			}else if(paramTipDoc.equals(itemsContrato[i].getLabel())){
				cliente.setTipoDocumento(itemsContrato[i].getValue());
			}else if(paramNroDoc.equals(itemsContrato[i].getLabel())){
				cliente.setNroDocumento(itemsContrato[i].getValue());
			}else if(paramEmail.equals(itemsContrato[i].getLabel())){
				cliente.setEmail(itemsContrato[i].getValue());
			}else if(paramTrxHuella.equals(itemsContrato[i].getLabel())){
				cliente.setIdTrxHuellaDigital(itemsContrato[i].getValue());
			}
		}
		if(cliente.getCodigoCentral()==null || cliente.getCodigoCentral().length()==0){
			return null;
		}
		return cliente;
	}
	
	private String buscarNombreFormato(ItemContrato[] itemsContrato){
		return buscarParametro(itemsContrato,Constants.COD_PARAM_IDFORMATO);
	}
	private String buscarNumeroContrato(ItemContrato[] itemsContrato){
		return buscarParametro(itemsContrato,Constants.COD_PARAM_NROCONTRATO);
	}
	private String buscarOficinaGestora(ItemContrato[] itemsContrato){
		return buscarParametro(itemsContrato,Constants.COD_PARAM_OFIGESTOR);
	}
	private String buscarProcedencia(ItemContrato[] itemsContrato){
		return buscarParametro(itemsContrato,Constants.COD_PARAM_CODPROCEDEN);
	}
	
	private String buscarParametro(ItemContrato[] itemsContrato, String param) {		
		for(int i=0;i<itemsContrato.length;i++){
			if(param.equals(itemsContrato[i].getLabel())){
				return itemsContrato[i].getValue();
			}
		}
		return null;
	}

	protected void processLineItem(String aLine,int index){
		if(aLine!=null && aLine.length()>0){
			if(aLine.length()>(Constants.LABEL_LENGTH-1)){
				String label = aLine.substring(0,Constants.LABEL_LENGTH);
				String value = aLine.substring(Constants.LABEL_LENGTH);
				if(value!=null && !value.equals("\\")){
					value = StringUtils.replace(value,"\\","");
					listaItems.add(Util.newIC(label,value));
				}
			}else{
				throw new IllegalArgumentException("La linea debe tener longitud mayor de 5 caracteres. Linea "+(index+1));
			}
		}else{
			throw new IllegalArgumentException("La linea se encuentra vacia. Linea "+(index+1));
		}
	}
		
	private final String fFilePath;
	private FirmarContratosRequest obj;
	private List<ItemContrato> listaItems;
	
}