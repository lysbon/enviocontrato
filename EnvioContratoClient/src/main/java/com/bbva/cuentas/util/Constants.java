package com.bbva.cuentas.util;

public class Constants {

	public static final String COD_EXITO_WS = "1";
	
	public static final String COD_EXITO = "01";
	public static final String COD_NO_EXITO = "00";
	public static final String COD_NO_EXITO_SERVICE = "02";
	
	public static final String COD_PARAM_IDFORMATO   = "CTR0|P21";
	//public static final String COD_PARAM_IDFORMATO   = "CTR0|P00";
	public static final String COD_PARAM_NROCONTRATO = "CTR0|P01";
	public static final String COD_PARAM_OFIGESTOR   = "CTR0|P02";
	public static final String COD_PARAM_TIPDOC_TIT1 = "CTR0|P03";
	public static final String COD_PARAM_TIPDOC_TIT2 = "CTR0|P04";
	public static final String COD_PARAM_NRODOC_TIT1 = "CTR0|P05";
	public static final String COD_PARAM_NRODOC_TIT2 = "CTR0|P06";
	public static final String COD_PARAM_CODCEN_TIT1 = "CTR0|P07";
	public static final String COD_PARAM_CODCEN_TIT2 = "CTR0|P08";
	public static final String COD_PARAM_CODPROCEDEN = "CTR0|P09";
	public static final String COD_PARAM_CODDIVISA   = "CTR0|P10";
	public static final String COD_PARAM_EMAIL_TIT1  = "CTR0|P11";
	public static final String COD_PARAM_EMAIL_TIT2  = "CTR0|P12";
	public static final String COD_PARAM_TRX_HUELLA  = "CTR0|P17";
	
	public static final int LABEL_LENGTH = 8;
	
	//public static final String URL_WEB_SERVICE = "http://118.216.104.130:8090/ws";
	//public static final String URL_WEB_SERVICE = "http://118.216.104.130:8091/EnvioContratoWS/ws/envioContratoWS";
	//public static final String URL_WEB_SERVICE = "http://118.180.34.113:9080/envioContratoWS/ws";
	public static final String URL_WEB_SERVICE = "http://118.180.14.22:8080/envioContratoWS/ws";
	//public static final String URL_WEB_SERVICE = "http://118.180.13.221:8080/envioContratoWS/ws";

}
