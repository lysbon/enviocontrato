package com.bbva.cuentas;

public class Ejecucion {


	private String nombreOrigen;
	private String nombreDestino;
	private String codigo;
	private String script;
	
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public Ejecucion(String nombreOrigen, String nombreDestino) {
		super();
		this.nombreOrigen = nombreOrigen;
		this.nombreDestino = nombreDestino;
	}
	public Ejecucion(String codigo,String nombreOrigen, String nombreDestino) {
		super();
		this.nombreOrigen  = nombreOrigen;
		this.nombreDestino = nombreDestino;
		this.codigo        = codigo;
	}
	public Ejecucion(String script) {
		super();
		this.script = script;
	}
	public String getNombreOrigen() {
		return nombreOrigen;
	}
	public void setNombreOrigen(String nombreOrigen) {
		this.nombreOrigen = nombreOrigen;
	}
	public String getNombreDestino() {
		return nombreDestino;
	}
	public void setNombreDestino(String nombreDestino) {
		this.nombreDestino = nombreDestino;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	@Override
	public String toString() {
		return "Ejecucion ["
				+ (nombreOrigen != null ? "nombreOrigen=" + nombreOrigen + ", "
						: "")
				+ (nombreDestino != null ? "nombreDestino=" + nombreDestino
						+ ", " : "")
				+ (codigo != null ? "codigo=" + codigo + ", " : "")
				+ (script != null ? "script=" + script : "") + "]";
	}
	
}
