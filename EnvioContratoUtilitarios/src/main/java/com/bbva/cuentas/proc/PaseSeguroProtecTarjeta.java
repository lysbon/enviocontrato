package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseSeguroProtecTarjeta extends PaseGenerico{
	
	public static void run(){
		PaseSeguroProtecTarjeta proc = new PaseSeguroProtecTarjeta();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("IC991");
			if(contarKITS>0){
				proc.revertirKits("IC991");
				proc.revertirPlantillas("IC991");
			}
			proc.ejecutarScriptsSeguroKit();
			proc.ejecutarScriptsSeguroPlantilla();
			proc.ejecutarScriptsSeguroEnvKit();
			proc.ejecutarScriptsSeguroSeccion();
			proc.ejecutarScriptsSeguroItems();
			proc.ejecutarScriptsSeguroKitPlantilla();
			
			proc.actualizarSeguroCorreos();
			proc.actualizarSeguroPdf();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsSeguroEnvKit() throws IOException {
		List<String> kits = consultarScriptKit("IC991");
		String asunto = "BBVA - Certificado del Seguro de Protección de Tarjeta";
		String envCorreo = "A";
		String engine = "STRTPL";
		for(String kit : kits){
			ejecutarScriptsEnvKit(kit,"BXI",asunto,envCorreo,engine,false);
		}
	}
	
	public void ejecutarScriptsSeguroKit() throws Exception{
		asegurarSecuenciaKit();
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'400812',NULL,NULL,NULL,NULL,'A','A','KIT IC991 - SEGURO PROTECCION TARJETAS','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsSeguroPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWID FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWID");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();		
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','P','A','IC991','0520','IC991 - SOLICITUD Y CERTIFICADO SEGURO PROTECCION TARJETAS','G','IC991 SEGURO PROTECCION TARJETAS',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsSeguroSeccion() throws IOException{
		
		Integer codIC991 = this.consultarPlantillaPorFormato("IC991");
		
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_SECCION)+1 NEWID FROM APDIGB.TBG003_SECCION"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("NEWID");
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codIC991+",1)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codIC991+",2)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codIC991+",16)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsSeguroItems() throws IOException{
		
		List<Ejecucion> scriptsPage1 = new ArrayList<Ejecucion>();
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(498,758,null,null,'A','Calibri12','SOL0|A01','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(148,668,null,null,'A','Calibri12','SOL0|A02','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(337,668,null,null,'A','Calibri12','SOL0|A03','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(494,668,null,null,'A','Calibri12','SOL0|A04','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(149,640,null,null,'A','Calibri12','SOL0|A05','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(338,640,null,null,'A','Calibri12','SOL0|A06','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(493,640,null,null,'A','Calibri12','SOL0|A07','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(206,611,null,null,'A','Calibri12','SOL0|A09','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(344,611,null,null,'A','Calibri12','SOL0|A10','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(493,611,null,null,'A','Calibri12','SOL0|A11','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(107,611,null,null,'A','Calibri12','SOL0|A08','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(167,488,null,null,'A','Calibri12','SOL0|A12','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(432,488,null,null,'A','Calibri12','SOL0|A13','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(143,459,null,null,'A','Calibri12','SOL0|A14','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(296,459,null,null,'A','Calibri12','SOL0|A15','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(432,459,null,null,'A','Calibri12','SOL0|A16','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(201,430,null,null,'A','Calibri12','SOL0|A17','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(432,430,null,null,'A','Calibri12','SOL0|A18','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(61,403,null,null,'A','Calibri11','SOL0|A19','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(62,388,null,null,'A','Calibri11','SOL0|A21','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(314,388,null,null,'A','Calibri11','SOL0|A22','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(314,403,null,null,'A','Calibri11','SOL0|A20','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));

		List<Ejecucion> scriptsPage2 = new ArrayList<Ejecucion>();
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(169,760,null,null,'A','Calibri12','SOL0|A23','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(435,760,null,null,'A','Calibri12','SOL0|A24','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(168,732,null,null,'A','Calibri12','SOL0|A25','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(271,732,null,null,'A','Calibri12','SOL0|A26','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(435,732,null,null,'A','Calibri12','SOL0|A27','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(191,703,null,null,'A','Calibri12','SOL0|A28','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(435,703,null,null,'A','Calibri12','SOL0|A29','T','[CDSECCION]',null,null,null,null,[CDITEM],1)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(57,674,null,null,'A','Calibri11','SOL0|A30','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(57,660,null,null,'A','Calibri11','SOL0|A32','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(315,674,null,null,'A','Calibri11','SOL0|A31','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(314,660,null,null,'A','Calibri11','SOL0|A33','T','[CDSECCION]',null,null,null,null,[CDITEM],0)"));

		List<Ejecucion> scriptsPage16 = new ArrayList<Ejecucion>();
		scriptsPage16.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(144,329,null,null,'A','Calibri11','SOL0|A34','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		
		ejecutarScriptsItems("IC991","1",scriptsPage1);
		ejecutarScriptsItems("IC991","2",scriptsPage2);
		ejecutarScriptsItems("IC991","16",scriptsPage16);
		
	}
	
	public void ejecutarScriptsSeguroKitPlantilla(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='IC991'"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%IC991%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarSeguroCorreos(){
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\seguroProtecTarjeta\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"seguro.html",rutaDestino+"seguro20180905.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("400812",null,rutaDestino+"seguro20180905.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
	
	public void actualizarSeguroPdf() throws InterruptedException, IOException{
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\seguroProtecTarjeta\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("PDF");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("IC991")
		};
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
	
}
