package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseDepPlazo2 extends PaseGenerico{
	
	public static void run(){
		PaseDepPlazo2 proc = new PaseDepPlazo2();
		try {
			
			proc.actualizarJasper_DP();
			proc.descargarJaspers();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void actualizarJasper_DP() throws InterruptedException, JsonParseException, JsonMappingException, IOException{
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\depplazo\\jasper\\tmp\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("jrxml");
		util.setRutaDestino("/mnt/compartido/apdig/formatos/jasper/");
		Plantilla[] plantillas = { 
				util.plantilla("JP0002"),
				util.plantilla("JP0003"),
				util.plantilla("JP0004")
		};
		List<Ejecucion> listaPlantillas = util.carga(plantillas,StringUtils.EMPTY);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
	}
	
	public void descargarJaspers(){
		
		ConfigureApp.invocarBuscarArchivo("/mnt/compartido/apdig/formatos/jasper/",null);
		
		descargarJasper("JP0002.jrxml");
		descargarJasper("JP0003.jrxml");
		descargarJasper("JP0004.jrxml");
	}
	
	public static void descargarJasper(String nombreJasper){
		
		String file     = "/mnt/compartido/apdig/formatos/jasper/";
		String carpetaDestino = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\depplazo\\jasper\\tmp2\\";
		List<Ejecucion> archivos = new ArrayList<Ejecucion>();
		archivos.add(new Ejecucion(file+"/"+nombreJasper,carpetaDestino));
		ConfigureApp.obtenerArchivos(archivos,"",false);
	}
	
}
