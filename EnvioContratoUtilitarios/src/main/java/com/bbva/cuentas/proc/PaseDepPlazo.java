package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseDepPlazo extends PaseGenerico{
	
	public static void run(){
		PaseDepPlazo proc = new PaseDepPlazo();
		try {
			//SQL
//			Integer contarKITS = proc.consultarScriptCountKit("BG031");
//			if(contarKITS>0){
//				proc.revertirKits("BG031");
//				proc.revertirPlantillas("DEP.PLAZO");
//				proc.revertirPlantillas("CERTIFICADO");
//				proc.revertirPlantillas("BG031");
//				proc.revertirFlag("CFG0|P34","V");
//				proc.revertirFlag("CFG0|P34","M");
//			}
//			proc.ejecutarScriptsDP_KIT();
//			proc.ejecutarScriptsDP_PLANTILLA();
			proc.ejecutarScriptsDP_KIT_PLANTILLA2();
//			proc.ejecutarScriptsDP_FLAGS();
//			proc.ejecutarScriptsDP_ENVKIT();
//			proc.ejecutarScriptsDP_SECCION();
//			proc.ejecutarScriptsDP_ITEMS();
//			proc.ejecutarScriptsDP_KIT_PLANTILLA();
			//FORMATOS
//			proc.actualizarCorreosDP();
//			proc.actualizarPDF_DP();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsDP_ENVKIT() throws JsonParseException, JsonMappingException, IOException {
		List<String> kits = consultarScriptKit("BG031");
		String asunto = "¡Bienvenido! Con tu depósito a Plazo haz crecer ese dinero extra";
		String envCorreo = "A";
		String engine = "STRTPL";
		
		for(String kit : kits){
			ejecutarScriptsEnvKit(kit,"BXI",asunto,envCorreo,engine,true);
		}		
	}
	
	public void ejecutarScriptsDP_KIT() throws Exception{
		asegurarSecuenciaKit();
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'030021',NULL,NULL,NULL,NULL,'I','A','KIT BG031 - CUENTA A PLAZO P.NATURAL PEN','A','A')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'030022',NULL,NULL,NULL,NULL,'I','A','KIT BG031 - CUENTA A PLAZO P.NATURAL USD','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsDP_PLANTILLA() throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWID FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWID");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();		
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','P','A','BG031','0514','BG031 CONTRATO CUENTA A PLAZO','G','BG031 CONTRATO CUENTA A PLAZO SOLES',NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','V','A','A','J','A','JP0002','0490','CERTIFICADO AL VENCIMIENTO','G','JP0002 - DEP.PLAZO CERT. AL VENCIMENTO', NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','V','A','A','J','A','JP0003','0490','CERTIFICADO MENSUAL',       'G','JP0003 - DEP.PLAZO CERT. MENSUAL',       NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','J','A','JP0004','0490','CRONOGRAMA AL VENCIMIENTO', 'G','JP0004 - DEP.PLAZO CRON. AL VENCIMIENTO',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsDP_FLAGS() throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG005_FLAG(CD_FLAG,NB_CONDICION_NOMBRE,NB_CONDICION_VALOR,NU_ORDEN,CH_TIPO) VALUES(APDIGB.FLG_SEQ.NEXTVAL,'CFG0|P34','V',1,'T')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG005_FLAG(CD_FLAG,NB_CONDICION_NOMBRE,NB_CONDICION_VALOR,NU_ORDEN,CH_TIPO) VALUES(APDIGB.FLG_SEQ.NEXTVAL,'CFG0|P34','M',1,'T')"));
		ConfigureApp.ejecutarSQL(scripts);
		
		Integer codigoCertificadoVen = consultarPlantillaPorFormato("JP0002");
		Integer codigoCertificadoMen = consultarPlantillaPorFormato("JP0003");
		
		Integer codigoFlagVen = consultarFlagPorParametro("CFG0|P34","V");
		Integer codigoFlagMen = consultarFlagPorParametro("CFG0|P34","M");
		
		List<Ejecucion> scriptsFlagPlantilla = new ArrayList<Ejecucion>();
		scriptsFlagPlantilla.add(new Ejecucion("INSERT INTO APDIGB.TBG006_FLAG_PLANTILLA(CD_FLAG, CD_PLANTILLA, ST_VALOR, ST_ACTIVO) VALUES("+codigoFlagVen+","+codigoCertificadoVen+",'A','A')"));
		scriptsFlagPlantilla.add(new Ejecucion("INSERT INTO APDIGB.TBG006_FLAG_PLANTILLA(CD_FLAG, CD_PLANTILLA, ST_VALOR, ST_ACTIVO) VALUES("+codigoFlagMen+","+codigoCertificadoMen+",'A','A')"));
		ConfigureApp.ejecutarSQL(scriptsFlagPlantilla);
		
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsDP_SECCION() throws JsonParseException, JsonMappingException, IOException{
		
		Integer codBG031 = this.consultarPlantillaPorFormato("BG031");
		
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_SECCION)+1 NEWID FROM APDIGB.TBG003_SECCION"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("NEWID");
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codBG031+",1)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codBG031+",2)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codBG031+",4)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsDP_ITEMS() throws JsonParseException, JsonMappingException, IOException{
		
		List<Ejecucion> scriptsPage1 = new ArrayList<Ejecucion>();
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(470,782,null,null,'A','Calibri12','CTR0|A01','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(406,733,null,null,'A','Calibri12','CTR0|A02','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(442,733,null,null,'A','Calibri12','CTR0|A03','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(478,733,null,null,'A','Calibri12','CTR0|A04','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(541,733,null,null,'A','Calibri12','CTR0|A05','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(46, 682,null,50,'A','Calibri12','CTR0|A06','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(46, 665,null,null,'A','Calibri12','CTR0|A07','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(406,670,null,null,'A','Calibri12','CTR0|A08','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(442,670,null,null,'A','Calibri12','CTR0|A09','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(478,670,null,null,'A','Calibri12','CTR0|A10','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(541,670,null,null,'A','Calibri12','CTR0|A11','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(98, 648,null,null,'A','Calibri12','CTR0|A12','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(470,648,null,null,'A','Calibri12','CTR0|A13','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(45, 744,null,null,'A','Calibri12','CTR0|G01','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(45, 728,null,null,'A','Calibri12','CTR0|G02','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage1.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(173,713,null,null,'A','Calibri12','CTR0|G03','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));

		List<Ejecucion> scriptsPage2 = new ArrayList<Ejecucion>();
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(151,343,null,null,'A','Calibri12','CTR0|G03','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(166,463,null,null,'A','Calibri12','CTR0|G04','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage2.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,355,null,null,'A','Calibri12','CTR0|G01','T',[CDSECCION],null,null,null,null,[CDITEM],1)"));
		
		List<Ejecucion> scriptsPage4 = new ArrayList<Ejecucion>();
		scriptsPage4.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(154,567,null,null,'A','Calibri12','CTR0|G03','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage4.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(167,714,null,null,'A','Calibri12','CTR0|G04','T',[CDSECCION],null,null,null,null,[CDITEM],0)"));
		scriptsPage4.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(162,579,null,null,'A','Calibri12','CTR0|G01','T',[CDSECCION],null,null,null,null,[CDITEM],1)"));
		
		ejecutarScriptsItems("BG031","1",scriptsPage1);
		ejecutarScriptsItems("BG031","2",scriptsPage2);
		ejecutarScriptsItems("BG031","4",scriptsPage4);
		
	}
	
	public void ejecutarScriptsDP_KIT_PLANTILLA(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='BG031'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTA'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_R'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PE281' AND NB_DESCRIPCION ='PE281 - LPDP - CONTRATOS'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600_FATCA_FORM_AUTO_CERT_PN'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTAMEN_RBA_LINE'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_RBA'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='JP0002'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='JP0003'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='JP0004')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%BG031%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsDP_KIT_PLANTILLA2(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='BG871' AND NB_DESCRIPCION NOT LIKE '%NUEVO%'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='BG871_NUEVO' OR NB_DESCRIPCION LIKE '%NUEVO%'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='CLAUG')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%BG031%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarCorreosDP(){
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\depplazo\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"dp.html",rutaDestino+"dp20180716.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("030021",null,rutaDestino+"dp20180716.html"));
		kits.add(new Ejecucion("030022",null,rutaDestino+"dp20180716.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
	
	public void actualizarPDF_DP() throws InterruptedException, JsonParseException, JsonMappingException, IOException{
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\depplazo\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("BG031")
		};
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
	
	
}
