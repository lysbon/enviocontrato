package com.bbva.cuentas.proc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;

public class PaseAhorrador2 extends PaseGenerico{
	
	public static void run(){
		PaseAhorrador2 proc = new PaseAhorrador2();
		try {
			//SQL
			Integer codTplDES = proc.consultarPlantillaPorFormato("BGD01");
//			proc.ejecutarScriptsItems(codTplDES);
			proc.actualizarCorreos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void actualizarCorreos() {
		actualizarCorreos("AH03002A","TPL_AFILIACION_V2");
		actualizarCorreos("AH03002D","TPL_DESAFILIACION_V3");
	}
	
	private void actualizarCorreos(String codKitDes,String nombreArchivo) {
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi6\\ara\\jetform_digital\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+nombreArchivo+".html",rutaDestino+nombreArchivo+"20180816.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion(codKitDes,null,rutaDestino+nombreArchivo+"20180816.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}

	@SuppressWarnings("unchecked")
	private void ejecutarScriptsItems(Integer codDes) throws IOException {
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_ITEM)+1 NEWID FROM APDIGB.TBG004_ITEM"));
		String cdItem = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdItem, HashMap[].class);
		Integer cdItemInt = (Integer)dataList[0].get("NEWID");
		
		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codDes+" AND NU_PAGINA = 1"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("CD_SECCION");
		
		List<Ejecucion> revert = new ArrayList<Ejecucion>();
		revert.add(new Ejecucion("DELETE FROM APDIGB.TBG004_ITEM WHERE CD_SECCION ="+cdSeccionInt+" AND NB_NOMBRE_VAR LIKE 'AHN%'"));
		ConfigureApp.ejecutarSQL(revert);
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,643,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,643,null,null,'A','Arial11','AHN0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
    //	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(298,766,null,null,'A','ArialBold12','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",1)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(298,766,null,null,'A','ArialBold12','AHN0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",1)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,607,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,607,null,null,'A','Arial11','AHN0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//  scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,487,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,487,null,null,'A','Arial11','AHN0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,712,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,712,null,null,'A','Arial11','AHN0|A10','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,699,null,null,'A','Arial11','ARA0|A01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,699,null,null,'A','Arial11','AHN0|A11','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,208,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,208,null,null,'A','Arial11','AHN0|A09','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,687,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,687,null,null,'A','Arial11','AHN0|A09','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(81,400,null,null,'A','Arial11','ARA0|A16','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(81,400,null,null,'A','Arial11','AHN0|A02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(118,400,null,null,'A','Arial11','ARA0|A17','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(118,400,null,null,'A','Arial11','AHN0|A03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;		
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(201,400,null,null,'A','Arial11','ARA0|A18','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(201,400,null,null,'A','Arial11','AHN0|A04','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,220,null,null,'A','Arial11','ARA0|A21','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,220,null,null,'A','Arial11','AHN0|A07','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(172,220,null,null,'A','Arial11','ARA0|A22','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(172,220,null,null,'A','Arial11','AHN0|A08','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,243,null,null,'A','Arial11','ARA0|A19','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,243,null,null,'A','Arial11','AHN0|A05','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
	//	scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,231,null,null,'A','Arial11','ARA0|A20','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,231,null,null,'A','Arial11','AHN0|A06','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		
		ConfigureApp.ejecutarSQL(scripts);
		
		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT COUNT(1) CANT FROM APDIGB.TBG004_ITEM WHERE CD_SECCION ="+cdSeccionInt+" AND NB_NOMBRE_VAR LIKE 'AHN%'"));
		ConfigureApp.consultarSQL(query);
		
	}
	
}
