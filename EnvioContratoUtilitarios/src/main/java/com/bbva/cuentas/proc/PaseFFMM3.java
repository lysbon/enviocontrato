package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseFFMM3 extends PaseGenerico{
	
	public static void run(){
		PaseFFMM3 proc = new PaseFFMM3();
		try {
			//SQL
			Integer contarKITS = proc.consultarScriptCountKit("FM041");
			contarKITS += proc.consultarScriptCountKit("E5005");
			if(contarKITS>0){
				proc.revertirKits("FM041");
				proc.revertirKits("E5005");
				proc.revertirPlantilla("FM041");
				proc.revertirPlantilla("E5005");
			}
			proc.ejecutarScriptsFfmmKit();
			proc.ejecutarScriptsFfmmPlantilla();
			proc.ejecutarScriptsFfmmEnvKit();
			proc.ejecutarScriptsFfmmKitPlantillaBase();
			proc.ejecutarScriptsFfmmKitPlantillaProspecto();
			//FORMATOS
			proc.actualizarFfmmHtml();
			proc.actualizarFfmmPdf();
						
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ejecutarScriptsFfmmEnvKit() throws IOException {
		for(String kit : consultarScriptKit("FM041")){
			ejecutarScriptsEnvKit(kit,"BXI","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
		for(String kit : consultarScriptKit("E5005")){
			ejecutarScriptsEnvKit(kit,"BXI","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
			ejecutarScriptsEnvKit(kit,"BM","!Conoce más de tu Fondo Mutuo <CFG0P03>!","A","STRTPL",false);
		}
	}
		
	public void ejecutarScriptsFfmmKit(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'800352',NULL,NULL,NULL,NULL,'I','A','KIT FM011 FM041 SuperDolares4FMIV','A','A')"));
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_FILEUNICO,FL_VAL_HUELLA,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'800291',NULL,NULL,NULL,NULL,'I','A','KIT FM011 E5005 OportEspSol3FMIV','A','A')"));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsFfmmPlantilla() throws IOException{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWIDPL FROM APDIGB.TBG002_PLANTILLA"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWIDPL");
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','I','A','P','A','FM041','0000','Prospecto BBVA Super Dolares 4 FMIV','G','Prospecto SuperDol4FMIV',NULL,NULL,SYSDATE,NULL)"));
		cdPlantillaInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','I','A','P','A','E5005','0000','Prospecto BBVA Oportunidad Especial Soles 3 FMIV','G','Prospecto OportEspSol3FMIV',NULL,NULL,SYSDATE,NULL)"));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void ejecutarScriptsFfmmKitPlantillaBase(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,PL.CD_PLANTILLA "+
				"FROM   APDIGB.TBG010_KIT K,"+
				"	   ("+
				"		SELECT CD_PLANTILLA,NB_NOMBRE_FORMATO"+
				"		FROM   APDIGB.TBG002_PLANTILLA"+
				"		WHERE  CD_PLANTILLA IN ("+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='FM011'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTA'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_R'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='PE281' AND NB_DESCRIPCION ='PE281 - LPDP - CONTRATOS'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='E0600_FATCA_FORM_AUTO_CERT_PN'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DICTAMEN_RBA_LINE'),"+
				"		(SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO ='DAE_RBA')"+
				"		)"+
				"	   ) PL "+
				"WHERE K.NB_NOMBRE_KIT LIKE '%FM011 FM041%' OR"+
				"      K.NB_NOMBRE_KIT LIKE '%FM011 E5005%'"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsFfmmKitPlantillaProspecto(){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
				"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) "+
				"SELECT K.CD_KIT,P.CD_PLANTILLA         "+
				"FROM   APDIGB.TBG010_KIT K,            "+
				"       APDIGB.TBG002_PLANTILLA P       "+
				"WHERE  (K.NB_NOMBRE_KIT LIKE '%FM011 FM041%' OR "+
				"        K.NB_NOMBRE_KIT LIKE '%FM011 E5005%')"+
				"  AND  SUBSTR(K.NB_NOMBRE_KIT,11,5) = P.NB_NOMBRE_FORMATO"
				));
		ConfigureApp.ejecutarSQL(scripts);
	}
		
	public void actualizarFfmmHtml(){
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\ffmm\\html\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"ffmm.html",rutaDestino+"ffmm20180730.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion("800352",null,rutaDestino+"ffmm20180730.html"));
		kits.add(new Ejecucion("800291",null,rutaDestino+"ffmm20180730.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}
		
	public void actualizarFfmmPdf() throws InterruptedException, IOException{
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi9\\ffmm\\pdf\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = { 
				util.plantilla("FM041"),
				util.plantilla("E5005")
		};
		
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}
		
}
