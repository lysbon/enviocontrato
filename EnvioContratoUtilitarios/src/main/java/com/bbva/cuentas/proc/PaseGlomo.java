package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseGlomo extends PaseGenerico{
	
	public static void run(){
		PaseGlomo proc = new PaseGlomo();
		try {			
			
			String codigoKit="PVIEWKIT";
			String cd_kit ="";
			String cd_Env ="1";
									
			//UBICA KIT VISTA PREVIA, EL CUAL DEBE SER UNICO			
			List<String> listKits = proc.consultarScriptKit(codigoKit);			
			if (listKits.size()==0) {
				proc.ejecutarScriptsDP_KIT();           
				listKits = proc.consultarScriptKit(codigoKit);
			}		
			
			if (listKits.size()>1)  throw new Exception("Existe más de un kit VISTA PREVIA");
			cd_kit=listKits.get(0).toString();	
			
			//UBICA ENVIRONMENT			
			Integer existeEnv=proc.consultarScriptEnv(cd_Env);
							
			//LISTA PLANTILLAS
			List<Plantilla> listPlantillas = new ArrayList<Plantilla>();			
			listPlantillas.add(new Plantilla("JP0001","JP0001 - EECC ESTADO CUENTA", "JP0001 - EECC"));
			
			//Elimina KitxEnvironment y plantillas especificas.
			proc.revertirKITxEnv(cd_Env,cd_kit);
			proc.revertirPLANTILLAS(listPlantillas);
			
			//Crear Environment, KitxEnvironment, Plantilla
		    if (existeEnv==null) proc.ejecutarScriptsDP_Env(cd_Env);
			proc.ejecutarScriptsDP_KITxEnv(cd_Env,cd_kit);	
			proc.ejecutarScriptsDP_Plantilla(listPlantillas);
			
			//Sube los archivos a una carpeta del server.
			proc.actualizarPDF_DP();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void ejecutarScriptsDP_KIT() throws Exception{
		asegurarSecuenciaKit();
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG010_KIT(CD_KIT, CD_CODIGO, NB_CORREO_ASUNTO, LB_CORREO_PLANTILLA, CD_USU_CREA, CD_USU_MODI, FH_CREACION, FH_MODIFICACION, ST_ENV_CORREO, ST_FILEUNICO, FL_VAL_HUELLA, NB_CORREO_ENGINE, NB_NOMBRE_KIT, ST_SIGNBOX, ST_HUELLA) VALUES(APDIGB.KIT_SEQ.NEXTVAL, 'PVIEWKIT', 'Kit Vista Previa', NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', 'STRTPL', 'KIT VISTA PREVIA', 'A', 'N')"));		
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	
	
	protected void revertirKITxEnv(String cdEnv, String codKit){
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG012_ENV_KIT WHERE CD_KIT = "+codKit+" AND CD_ENV="+cdEnv));
		ConfigureApp.ejecutarSQL(scripts);
		
	}
	
	protected void revertirPLANTILLAS(List<Plantilla> plantillas) throws JsonParseException, JsonMappingException, IOException{
		
		for(Plantilla p : plantillas){
			Integer cd_plantilla = consultarPlantillaPorFormato(p.getNbNombreFormato());
			if (cd_plantilla!=null)	revertirPlantilla(cd_plantilla.toString());
		}
	}
	
	public void ejecutarScriptsDP_Env(String cdEnv) throws Exception{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		String sql="INSERT INTO APDIGB.TBG009_ENV(CD_ENV, NB_NOMBRE, NB_DESCRIPCION, NB_REMITENTE, NB_REMITENTE_CC, CH_TIPO, ST_ACTIVO, ST_DEFECTO, CD_USU_CREA, CD_USU_MODI, FH_CREACION, FH_MODIFICACION, NB_RUTAGESTDOC, ST_HUELLA) VALUES("+
				cdEnv+", 'GLOMO', 'Glomo', 'BBVA Continental <postmaster@bbvacontinental.pe>', 'apertura_digital@bbva.com', 'A', 'A', NULL, 'user', 'user', SYSDATE, SYSDATE, NULL, 'I')";
				
		scripts.add(new Ejecucion(sql));	
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void ejecutarScriptsDP_KITxEnv(String cdEnv,String codKit) throws Exception{

		List<Ejecucion> scripts = new ArrayList<Ejecucion>();				
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT) VALUES ("+cdEnv+","+codKit+")"));			
		
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsDP_Plantilla(List<Plantilla> plantillas) throws Exception{
		
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_PLANTILLA)+1 NEWID FROM APDIGB.TBG002_PLANTILLA"));
		String response = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(response, HashMap[].class);
		Integer cdPlantillaInt = (Integer)dataList[0].get("NEWID");
		
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		for(Plantilla p : plantillas){			
			
			String sql ="INSERT INTO APDIGB.TBG002_PLANTILLA ( CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CH_TIPO_PLANTILLA,NB_DESCRIPCION,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) VALUES ("+cdPlantillaInt.toString()+",NULL,'A','F','A','A','J','A','"+p.getNbNombreFormato()+"','0490','"+p.getNbNombreCorreo()+"','G','"+p.getNbDescripcion()+"',NULL,NULL,SYSDATE,NULL)";
			
			scripts.add(new Ejecucion(sql));	
			
			cdPlantillaInt++;
		}		
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	public void actualizarPDF_DP() throws InterruptedException, JsonParseException, JsonMappingException, IOException{
		String rutaOrigen  = "C:\\mntorig\\compartido\\apdig\\formatos\\jasper\\";
		String rutaDestino = "/mnt/compartido/apdig/formatos/jasper/";
		
		final File folder = new File(rutaOrigen);
		List<File> listfiles = Utilitarios.listFilesForFolder(folder);
		List<Ejecucion> listEjecuciones= new ArrayList<Ejecucion>();
		for(File onefile : listfiles){			
			listEjecuciones.add(new Ejecucion(rutaOrigen+onefile.getName(),rutaDestino+onefile.getName()));
		}
		//ConfigureApp.generarRuta("/mnt/compartido/apdig/formatos/jasper/");
		ConfigureApp.cargarArchivos(listEjecuciones,false);		
		Thread.sleep(10000l);
	}
	
	@SuppressWarnings("unchecked")
	protected Integer consultarScriptEnv(String cdEnv) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_ENV FROM APDIGB.TBG009_ENV WHERE CD_ENV="+cdEnv));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		
		return (Integer)dataList[0].get("CD_ENV");
		
	}
	
	@SuppressWarnings("unchecked")
	protected List<String> consultarScriptKit(String codigoKit) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO = "+codigoKit+""));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		List<String> listaKits = new ArrayList<String>();
		for(HashMap<String,Object> obj : dataList){
			listaKits.add(obj.get("CD_KIT").toString());
		}
		return listaKits;
	}
	
	
	
	

	
	
}
