package com.bbva.cuentas.proc;

import java.util.List;


public class PaseCorrientesCorrectivo extends PaseGenerico{
	
	public static void run(){
		PaseCorrientesCorrectivo proc = new PaseCorrientesCorrectivo();
		try {
			//SQL
			List<Integer> kits = proc.consultarScriptKitPorProducto("01");
			Integer cdPlantillaDae = proc.consultarPlantillaPorFormato("DAE_RBA");
			if(cdPlantillaDae==null || cdPlantillaDae<0) {
				cdPlantillaDae = proc.consultarPlantillaPorFormato("DAE_R");
			}
			for(Integer cdKit : kits) {
				try {
					proc.ejecutarScriptsKitPlantilla(cdKit,cdPlantillaDae);
				} catch (Exception e) {
					System.out.println("No se pudo para "+cdKit+". "+e.getMessage());
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
}
