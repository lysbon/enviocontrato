package com.bbva.cuentas.proc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.bbva.cuentas.ConfigureApp;
import com.bbva.cuentas.Ejecucion;
import com.bbva.cuentas.Utilitarios;
import com.bbva.cuentas.bean.Plantilla;

public class PaseAhorrador {
	
	public static void run(){
		PaseAhorrador proc = new PaseAhorrador();
		try {
			//SQL
			Integer codKitAFI = proc.consultarScriptCodKit("AH03002A");
			Integer codKitDES = proc.consultarScriptCodKit("AH03002D");
			Integer codTplAFI = proc.consultarScriptCodKit2("BGC91");
			Integer codTplDES = proc.consultarScriptCodKit2("BGD01");
			if(!(codKitAFI==null) || !(codKitDES==null)){
				proc.ejecutarRevert(codKitAFI,codTplAFI);
				proc.ejecutarRevert(codKitDES,codTplDES);
			}
			proc.ejecutarScriptsKit();
			codKitAFI = proc.consultarScriptCodKit("AH03002A");
			codKitDES = proc.consultarScriptCodKit("AH03002D");
			System.out.println(codKitAFI);
			System.out.println(codKitDES);
			proc.ejecutarScriptsEnvKit(codKitAFI);
			proc.ejecutarScriptsEnvKit(codKitDES);
			proc.ejecutarScriptsPlantilla();
			codTplAFI = proc.consultarScriptCodKit2("BGC91");
			codTplDES = proc.consultarScriptCodKit2("BGD01");
			System.out.println(codTplAFI);
			System.out.println(codTplDES);
			proc.ejecutarScriptsSeccion(codTplAFI,codTplDES);
			proc.ejecutarScriptsKitPlantilla(codKitAFI,codTplAFI);
			proc.ejecutarScriptsKitPlantilla(codKitDES,codTplDES);
			proc.ejecutarScriptsItems(codTplAFI,codTplDES);
//			//FORMATOS
			proc.actualizarCorreos("AH03002A","AH03002D");
			proc.actualizarPDF(codTplAFI,codTplDES);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void actualizarPDF(Integer codAfi,Integer codDes) throws InterruptedException, JsonParseException, JsonMappingException, IOException {
		final File folder = new File("C:\\Users\\P023915\\Desktop\\Proyectos\\pi6\\ara\\jetform_digital\\pdf20180716\\");
		List<File> pdfs = Utilitarios.listFilesForFolder(folder);
		Utilitarios util = new Utilitarios();
		util.setArchivosBase(pdfs);
		util.setExtension("pdf");
		util.setRutaDestino("/mnt/compartido/apdig/output/");
		Plantilla[] plantillas = {
				util.plantilla(new Long(codAfi),"BGC91"),
				util.plantilla(new Long(codDes),"BGD01")
		};
		List<Ejecucion> listaPlantillas = util.carga(plantillas);
		util.imprimir(listaPlantillas);
		ConfigureApp.cargarArchivos(listaPlantillas,false);
		Thread.sleep(10000l);
		ConfigureApp.actualizarArchivos("PDF",listaPlantillas);
	}

	private void actualizarCorreos(String codKitAfi,String codKitDes) {
		String rutaOrigen  = "C:\\Users\\P023915\\Desktop\\Proyectos\\pi6\\ara\\jetform_digital\\";
		String rutaDestino = "/mnt/compartido/apdig/output/";
		List<Ejecucion> plantillaHTML = new ArrayList<Ejecucion>();
		plantillaHTML.add(new Ejecucion(rutaOrigen+"TPL_AFILIACION.html",rutaDestino+"TPL_AFILIACION20180620.html"));
		plantillaHTML.add(new Ejecucion(rutaOrigen+"TPL_DESAFILIACION.html",rutaDestino+"TPL_DESAFILIACION20180620.html"));
		ConfigureApp.cargarArchivos(plantillaHTML,false);
		
		List<Ejecucion> kits = new ArrayList<Ejecucion>();
		kits.add(new Ejecucion(codKitAfi,null,rutaDestino+"TPL_AFILIACION20180620.html"));
		kits.add(new Ejecucion(codKitDes,null,rutaDestino+"TPL_DESAFILIACION20180620.html"));
		ConfigureApp.actualizarArchivos("HTML",kits);
	}

	@SuppressWarnings("unchecked")
	private void ejecutarScriptsItems(Integer codAfil,Integer codDes) throws JsonParseException, JsonMappingException, IOException {
		
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_ITEM)+1 NEWID FROM APDIGB.TBG004_ITEM"));
		String cdItem = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdItem, HashMap[].class);
		Integer cdItemInt = (Integer)dataList[0].get("NEWID");
		
		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codAfil+" AND NU_PAGINA = 1"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("CD_SECCION");
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		/*
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,709,null,null,'A','Arial11','ARA0|A01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(255,559,null,null,'A','Arial11','ARA0|A03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(346,559,null,null,'A','Arial11','ARA0|A04','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(143,443,null,null,'A','Arial11','ARA0|A05','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(143,419,null,null,'A','Arial11','ARA0|A06','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(143,395,null,null,'A','Arial11','ARA0|A07','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(143,371,null,null,'A','Arial11','ARA0|A08','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(299,443,null,null,'A','Arial11','ARA0|A09','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(299,419,null,null,'A','Arial11','ARA0|A10','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(415,412,null,null,'A','Arial11','ARA0|A13','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(431,295,null,null,'A','Arial11','ARA0|A15','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,721,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(299,394,null,null,'A','Arial11','ARA0|A11','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(299,371,null,null,'A','Arial11','ARA0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,697,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(297,767,null,null,'A','ArialBold12','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",1)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,685,null,null,'A','Arial11','ARA0|A02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,644,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(307,295,null,null,'A','Arial11','ARA0|A14','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		*/
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(255,559,null,null,'A','Arial11','ARA0|A03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(183,443,null,null,'A','Arial11','ARA0|A05','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(183,419,null,null,'A','Arial11','ARA0|A06','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(183,395,null,null,'A','Arial11','ARA0|A07','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(183,371,null,null,'A','Arial11','ARA0|A08','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(339,443,null,null,'A','Arial11','ARA0|A09','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(339,419,null,null,'A','Arial11','ARA0|A10','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(431,271,null,null,'A','Arial11','ARA0|A15','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,721,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(339,394,null,null,'A','Arial11','ARA0|A11','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(339,371,null,null,'A','Arial11','ARA0|A12','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,697,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(297,767,null,null,'A','ArialBold12','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,685,null,null,'A','Arial11','ARA0|A02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,644,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(307,271,null,null,'A','Arial11','ARA0|A14','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(186,709,null,null,'A','Arial11','ARA0|A01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(346,559,null,null,'A','Arial11','ARA0|A04','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;

		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codAfil+" AND NU_PAGINA = 2"));
		cdSeccion = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		cdSeccionInt = (Integer)dataList[0].get("CD_SECCION");
		
		/*
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(79,351,null,null,'A','Arial11','ARA0|A16','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,160,null,null,'A','Arial11','ARA0|A20','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,137,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(170,149,null,null,'A','Arial11','ARA0|A22','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(116,351,null,null,'A','Arial11','ARA0|A17','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,172,null,null,'A','Arial11','ARA0|A19','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,149,null,null,'A','Arial11','ARA0|A21','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(263,712,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(0,0,null,null,'A','Arial11','null','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(200,351,null,null,'A','Arial11','ARA0|A18','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(157,523,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		*/
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,160,null,null,'A','Arial11','ARA0|A20','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(170,149,null,null,'A','Arial11','ARA0|A22','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(116,351,null,null,'A','Arial11','ARA0|A17','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,172,null,null,'A','Arial11','ARA0|A19','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,149,null,null,'A','Arial11','ARA0|A21','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(263,664,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(200,351,null,null,'A','Arial11','ARA0|A18','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(179,475,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(79,351,null,null,'A','Arial11','ARA0|A16','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(156,137,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;

		query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codDes+" AND NU_PAGINA = 1"));
		cdSeccion = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		cdSeccionInt = (Integer)dataList[0].get("CD_SECCION");
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(298,766,null,null,'A','ArialBold12','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",1)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,220,null,null,'A','Arial11','ARA0|A21','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,712,null,null,'A','Arial11','ARA0|G02','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,687,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,643,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,607,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(51,487,null,null,'A','Arial11','ARA0|G01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(81,400,null,null,'A','Arial11','ARA0|A16','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(201,400,null,null,'A','Arial11','ARA0|A18','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(118,400,null,null,'A','Arial11','ARA0|A17','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,231,null,null,'A','Arial11','ARA0|A20','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,208,null,null,'A','Arial11','ARA0|G03','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(172,220,null,null,'A','Arial11','ARA0|A22','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(158,243,null,null,'A','Arial11','ARA0|A19','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		scripts.add(new Ejecucion("INSERT INTO APDIGB.TBG004_ITEM (NU_COOR_X,NU_COOR_Y,NU_IDX_CLIENTE,NU_ANCHO,ST_ACTIVO,NB_FUENTE,NB_NOMBRE_VAR,CH_TIPO,CD_SECCION,NU_SC_Y,NU_SC_X,NB_MASK,NB_VALOR_VAR,CD_ITEM,NU_ALINEAR) VALUES(137,699,null,null,'A','Arial11','ARA0|A01','T',"+cdSeccionInt+",null,null,'',null,"+cdItemInt+",0)"));cdItemInt++;
		
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsKitPlantilla(Integer codKitAFI, Integer codTplAFI) {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG011_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) VALUES ("+codKitAFI+","+codTplAFI+")"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	@SuppressWarnings("unchecked")
	private void ejecutarScriptsSeccion(Integer codTplAFI, Integer codTplDES) throws JsonParseException, JsonMappingException, IOException {
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_SECCION)+1 NEWID FROM APDIGB.TBG003_SECCION"));
		String cdSeccion = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdSeccion, HashMap[].class);
		Integer cdSeccionInt = (Integer)dataList[0].get("NEWID");
		
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codTplAFI+",1)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codTplAFI+",2)"));
		cdSeccionInt++;
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION,CD_PLANTILLA,NU_PAGINA) VALUES ("+cdSeccionInt.toString()+","+codTplDES+",1)"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsPlantilla() {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES (101,NULL,'A','F','A','A','P','A','BGC91','0000','SOLICITUD DE AFILIACION A MIS RECOMENDADOS',NULL,NULL,NULL,NULL,'P','BGC91 -  AFILIACION AHORRADOR RECOMIENDA')"));
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG002_PLANTILLA (CD_PLANTILLA,LB_PDF_PLANTILLA,ST_ACTIVO,ST_CONDICION,ST_FILEUNICO,ST_ENV_CORREO,ST_TIPO_CONSTR,ST_SIGNED,NB_NOMBRE_FORMATO,NB_TIPO_FILEUNICO,NB_NOMBRE_CORREO,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,CH_TIPO_PLANTILLA,NB_DESCRIPCION) VALUES (122,NULL,'A','F','A','A','P','A','BGD01','0000','DESAFILIACION A MIS RECOMENDADOS',NULL,NULL,NULL,NULL,'P','BGD01 - DESAFILIACION AHORRADOR RECOMIEN')"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	private void ejecutarScriptsEnvKit(Integer cdKit) {
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT) VALUES (1,'"+cdKit+"')"));
		scripts.add(new Ejecucion(
		"INSERT INTO APDIGB.TBG012_ENV_KIT(CD_ENV,CD_KIT) VALUES (2,'"+cdKit+"')"));
		ConfigureApp.ejecutarSQL(scripts);
	}

	public void ejecutarRevert(Integer codKit,Integer codTpl){
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		
//		scripts.add(new Ejecucion("DELETE FROM APDIGB.TBG010_KIT WHERE CD_KIT IN (1080,1081,1082,1083)"));
		
		if(!(codTpl==null)){
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG004_ITEM "+
					"WHERE  CD_SECCION IN (SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codTpl+")"));
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = "+codTpl));
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG011_KIT_PLANTILLA WHERE CD_PLANTILLA = "+codTpl));
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG002_PLANTILLA WHERE CD_PLANTILLA = "+codTpl));
		}
		if(!(codKit==null)){
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG012_ENV_KIT WHERE  CD_KIT = "+codKit));
			scripts.add(new Ejecucion(
					"DELETE FROM APDIGB.TBG010_KIT WHERE  CD_KIT = "+codKit));
		}
		
		ConfigureApp.ejecutarSQL(scripts);
	}
	
	@SuppressWarnings("unchecked")
	public void ejecutarScriptsKit() throws JsonParseException, JsonMappingException, IOException, Exception{
		List<Ejecucion> query = new ArrayList<Ejecucion>();
		query.add(new Ejecucion("SELECT MAX(CD_KIT)+1 NEWID FROM APDIGB.TBG010_KIT"));
		String cdItem = ConfigureApp.consultarSQL(query);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(cdItem, HashMap[].class);
		Integer cdItemIntMax = (Integer)dataList[0].get("NEWID");
		System.out.println("cdItemIntMax "+cdItemIntMax);
		query.add(new Ejecucion("SELECT APDIGB.KIT_SEQ.NEXTVAL NEWID FROM DUAL"));
		String cdItemSeq = ConfigureApp.consultarSQL(query);
		dataList = new ObjectMapper().readValue(cdItemSeq, HashMap[].class);
		Integer cdItemIntSeq = (Integer)dataList[0].get("NEWID");
		System.out.println("cdItemIntSeq "+cdItemIntSeq);
		
		if(cdItemIntSeq.intValue()>=cdItemIntMax.intValue()){
			List<Ejecucion> scripts = new ArrayList<Ejecucion>();
			scripts.add(new Ejecucion(
			"INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,NB_CORREO_ASUNTO,LB_CORREO_PLANTILLA,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_ENV_CORREO,ST_FILEUNICO,FL_VAL_HUELLA,NB_CORREO_ENGINE,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'AH03002A','Bienvenido a ¡Mis Recomendados!',NULL,NULL,NULL,NULL,NULL,'A','A','A','STRTPL','KIT BGC91 -  AFILIACION AHORRADOR RECOMI',NULL,NULL)"));
			scripts.add(new Ejecucion(
			"INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,NB_CORREO_ASUNTO,LB_CORREO_PLANTILLA,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_ENV_CORREO,ST_FILEUNICO,FL_VAL_HUELLA,NB_CORREO_ENGINE,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) VALUES (APDIGB.KIT_SEQ.NEXTVAL,'AH03002D','La desafiliación a Mis Recomendados fue satisfactoria',NULL,NULL,NULL,NULL,NULL,'A','A',NULL,'STRTPL','KIT BGD01 -  DESAFILIACION AHORRADOR REC',NULL,NULL)"));
			ConfigureApp.ejecutarSQL(scripts);
		}else{
			throw new Exception("Se cancela. Indices desincronizados.");
		}
	}
	
	@SuppressWarnings("unchecked")
	public Integer consultarScriptCodKit(String codigoKit) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_KIT FROM APDIGB.TBG010_KIT WHERE CD_CODIGO ='"+codigoKit+"'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return (Integer)dataList[0].get("CD_KIT");
	}
	
	@SuppressWarnings("unchecked")
	public Integer consultarScriptCodKit2(String codigoPlantilla) throws JsonParseException, JsonMappingException, IOException{
		List<Ejecucion> scripts = new ArrayList<Ejecucion>();
		scripts.add(new Ejecucion("SELECT CD_PLANTILLA FROM APDIGB.TBG002_PLANTILLA WHERE NB_NOMBRE_FORMATO = '"+codigoPlantilla+"'"));
		String result = ConfigureApp.consultarSQL(scripts);
		HashMap<String,Object>[] dataList = new ObjectMapper().readValue(result, HashMap[].class);
		if(dataList == null || dataList.length==0){
			return null;
		}
		return (Integer)dataList[0].get("CD_PLANTILLA");
	}
	
}
