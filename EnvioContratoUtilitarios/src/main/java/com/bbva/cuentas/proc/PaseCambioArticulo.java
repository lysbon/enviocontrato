package com.bbva.cuentas.proc;

import java.util.List;

import com.bbva.cuentas.Ejecucion;

public class PaseCambioArticulo extends PaseGenerico{

	public static void run(){
		PaseCambioArticulo proc = new PaseCambioArticulo();
		try {
			proc.actualizarCorreos();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void actualizarCorreos(){
		String rutaOrigen  = "D:\\tpl\\formato\\";
		String extension = "html";
		
		List<Ejecucion> cargaCarpeta = cargaCarpeta(rutaOrigen,extension);
		actualizarArchivos(cargaCarpeta, extension);
		
	}
	
}
