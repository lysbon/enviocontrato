package com.bbva.cuentas;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;

import com.bbva.cuentas.firmarContratos.ConfigurationItem;
import com.bbva.cuentas.firmarContratos.ConfigureRequest;
import com.bbva.cuentas.firmarContratos.ConfigureResponse;
import com.bbva.cuentas.firmarContratos.FirmarContratosPort;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortService;
import com.bbva.cuentas.firmarContratos.FirmarContratosPortServiceLocator;
import com.bbva.cuentas.firmarContratos.ReenvioContrato;
import com.bbva.cuentas.firmarContratos.ReenvioContratosRequest;
import com.bbva.cuentas.firmarContratos.ReenvioContratosResponse;

public class ConfigureApp {
	
	private static final String TB_PATRON = "PATRON";
	private static final String TB_SIMPLE = "SIMPLE";

	public static String encode(String ruta){
		String encodedBase64 = null;
		File originalFile = new File(ruta);
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
			byte[] bytes = new byte[(int) originalFile.length()];
			fileInputStreamReader.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));
			fileInputStreamReader.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return encodedBase64;
	}
	
	public static List<String> invocarBuscarArchivo(String rutaDestino,String nombreDestino) {
		List<ConfigurationItem> lstCI = new ArrayList<ConfigurationItem>();
		ConfigurationItem it0 = new ConfigurationItem("NAME",nombreDestino,"");
		ConfigurationItem it1 = new ConfigurationItem("LS"  ,"NAME",rutaDestino);
		if(!StringUtils.isEmpty(nombreDestino)){
			lstCI.add(it0);
			lstCI.add(it1);
		}else{
			lstCI.add(it1);
			it1.setLlave("");
		}
		ConfigurationItem[] itr= new ConfigurationItem[lstCI.size()];
		itr = lstCI.toArray(itr);
		ConfigureRequest request = new ConfigureRequest(itr);		
		String rpta = invocar(request,obtenerEndpoint());
		System.out.print(rpta);
		/*
		JSONArray  ja = new JSONArray(rpta);
		List<String> lst = new ArrayList<String>();
		for(int i=0; i<ja.length(); i++){
			System.out.println(ja.getString(i));
			lst.add(ja.getString(i));
		}
		return lst;
		*/
		return null;
	}
	
	private static String ejecutarSQL(String scriptSQL,String tipo) {
		List<ConfigurationItem> lstCI = new ArrayList<ConfigurationItem>();
		ConfigurationItem it = new ConfigurationItem(tipo,scriptSQL,"");
		lstCI.add(it);
		ConfigurationItem[] itr= new ConfigurationItem[lstCI.size()];
		itr = lstCI.toArray(itr);
		ConfigureRequest request = new ConfigureRequest(itr);		
		return invocar(request,obtenerEndpoint());
	}
	
	public static List<String> invocarBuscarArchivoPatron(String rutaDestino,String wildcard) {
		ConfigurationItem it0 = new ConfigurationItem("NAME",wildcard,"");
		ConfigurationItem it1 = new ConfigurationItem("LS"  ,"WILDCARD",rutaDestino);
		ConfigurationItem[] itr= new ConfigurationItem[2];
		itr[0] = it0;
		itr[1] = it1;
		ConfigureRequest request = new ConfigureRequest(itr);		
		String rpta = invocar(request,obtenerEndpoint());		
		JSONArray  ja = new JSONArray(rpta);
		List<String> lst = new ArrayList<String>();
		for(int i=0; i<ja.length(); i++){
			System.out.println(ja.getString(i));
			lst.add(ja.getString(i));
		}
		return lst;
	}
	
	public static void invocarSubirArchivo(String nombreSubida,String encoded){
		ConfigurationItem it = new ConfigurationItem("BASE64_PUT",encoded,nombreSubida);		
		ConfigurationItem[] itr= new ConfigurationItem[1];
		itr[0] = it;
		ConfigureRequest request = new ConfigureRequest(itr);
		invocar(request,obtenerEndpoint());
	}
	
	public static String invocarActualizarEntidad(String entidad,String codigo,String ruta){
		ConfigurationItem it = new ConfigurationItem(entidad,codigo,ruta);		
		ConfigurationItem[] itr= new ConfigurationItem[1];
		itr[0] = it;
		ConfigureRequest request = new ConfigureRequest(itr);
		return invocar(request,obtenerEndpoint());
	}
	
	public static void invocarBajarArchivo(String nombreSubida,String nombreBajada,String nodo_url){
		ConfigurationItem it = new ConfigurationItem("BASE64_GET","",nombreSubida);		
		ConfigurationItem[] itr= new ConfigurationItem[1];
		itr[0] = it;
		ConfigureRequest request = new ConfigureRequest(itr);
		String endpoint = obtenerEndpoint();
		if(nodo_url!=null){
			endpoint = nodo_url;
		}
		String strFile = invocar(request,endpoint);
		
		OutputStream out;
		try {
			out = new FileOutputStream(nombreBajada);
			out.write(Base64.decodeBase64(strFile));
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void invocarEjecutarReenvio(List<Ejecucion> p) {
		ReenvioContrato it0 = new ReenvioContrato("","","","","","","","","","","","","","");
		ReenvioContrato[] itr= new ReenvioContrato[1];
		itr[0] = it0;
		for(Ejecucion e : p){
			ReenvioContratosRequest request = new ReenvioContratosRequest(itr,e.getNombreOrigen(),"false");		
			String rpta = invocarReenvio(request);
			System.out.println(rpta);
		}
	}
	
	public static void cargarArchivos(List<Ejecucion> p,boolean validar){
		for(Ejecucion e : p){
			String encoded = ConfigureApp.encode(e.getNombreOrigen());
			System.out.println(encoded);
			String filename = FilenameUtils.getName(e.getNombreDestino());
			String rutaDest = FilenameUtils.getFullPath(e.getNombreDestino());
			ConfigureApp.invocarSubirArchivo(e.getNombreDestino(),encoded);
			if(validar){
				ConfigureApp.invocarBuscarArchivo(rutaDest,filename);
			}
		}
	}
	
	public static void actualizarArchivos(String tipoCarga,List<Ejecucion> p){
		for(Ejecucion e : p){
			String response = ConfigureApp.invocarActualizarEntidad(
					tipoCarga,
					e.getCodigo(),
					e.getNombreDestino());
			System.out.println(response);
		}
	}
	
	public static void ejecutarSQL(List<Ejecucion> p){
		for(Ejecucion e : p){
			System.out.println(e.getScript());
			ConfigureApp.ejecutarSQL(e.getScript(), "EXECUTE");
		}
	}

	public static String consultarSQL(List<Ejecucion> p){
		StringBuilder sb = new StringBuilder("");
		for(Ejecucion e : p){
			String response = ConfigureApp.ejecutarSQL(e.getScript(), "QUERY");
			sb.append(response);
			System.out.println(response);
		}
		return sb.toString();
	}
	
	public static void obtenerArchivos(List<Ejecucion> p, String sufijo, boolean log){
		for(Ejecucion e : p){
			String name = null;
			if(StringUtils.isEmpty(sufijo)){
				name = FilenameUtils.getName(e.getNombreOrigen());
			}else{
				name = FilenameUtils.getBaseName(e.getNombreOrigen())+
						"_"+sufijo+
						FilenameUtils.getExtension(e.getNombreOrigen());
			}
			System.out.println("Descargando "+e.getNombreOrigen()+"...");
			ConfigureApp.invocarBajarArchivo(
					e.getNombreOrigen(),
					e.getNombreDestino()+name,
					obtenerEndpoint());
		}
	}
	
	public static String invocar(ConfigureRequest request, String url){
		FirmarContratosPortService service = new FirmarContratosPortServiceLocator();
		((FirmarContratosPortServiceLocator)service).setFirmarContratosPortSoap11EndpointAddress(url);
		try {
			FirmarContratosPort port = service.getFirmarContratosPortSoap11();
			ConfigureResponse response = port.configure(request);
			if(COD_EXITO_WS.equals(response.getCodigoResultado())){
				System.out.println("Se ejecuto comando de configuracion.");				
			}else{
				System.out.println(response.getMensajeResultado());
			}
			return response.getMensajeResultado();
		} catch (ServiceException e) {
			System.out.println(e.getMessage());
		} catch (RemoteException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	public static String invocarReenvio(ReenvioContratosRequest request){
		FirmarContratosPortService service = new FirmarContratosPortServiceLocator();
		((FirmarContratosPortServiceLocator)service).setFirmarContratosPortSoap11EndpointAddress(obtenerEndpoint());
		try {
			FirmarContratosPort port = service.getFirmarContratosPortSoap11();
			ReenvioContratosResponse response = port.reenvioContratos(request);
			if(COD_EXITO_WS.equals(response.getCodigoResultado())){
				System.out.println("Se ejecuto comando de configuracion.");				
			}else{
				System.out.println(response.getMensajeResultado());
			}
			return response.getMensajeResultado();
		} catch (ServiceException e) {
			System.out.println(e.getMessage());
		} catch (RemoteException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	private static String obtenerEndpoint() {
		return URL_WEB_SERVICE;
	}
	
	public static void obtenerArchivo(String file,String carpetaDestino){
		Ejecucion[] plantillas = new Ejecucion[1];
		String ext = FilenameUtils.getExtension(file);
		String name = FilenameUtils.getBaseName(file);
		if(StringUtils.isEmpty(ext)){
			ext = "pdf";
		}
		plantillas[0] = new Ejecucion(file,carpetaDestino+name+"."+ext);
		for(int i=0;i<plantillas.length;i++){
			ConfigureApp.invocarBajarArchivo(
					plantillas[i].getNombreOrigen(),
					plantillas[i].getNombreDestino(),
					obtenerEndpoint());
		}
	}
	
	public static void obtenerDocumentos(String codcliente){
		String wildcard   = codcliente+"*";
		//String ruta     ="/mnt/compartido/apdig/output/";
		String ruta       = "/Apps/APDIG/output/";
		String rutaSalida = "C:\\Users\\p023915\\Downloads\\"+codcliente+File.separatorChar;
		obtenerArchivos(wildcard,ruta,rutaSalida,TB_PATRON,true);
	}
	
	public static void obtenerLogs(boolean flagBajar){
		String ruta       = "/pr/apdig/online/pe/web/log/";
		String rutaSalida = "C:\\Users\\p023915\\Downloads\\logs\\"+File.separatorChar;
		obtenerArchivos(null,ruta,rutaSalida,TB_SIMPLE,flagBajar);
	}
	
	public static void obtenerArchivos(String wildcard, String ruta, String rutaSalida, String tipoBusqueda, boolean flagBajar){
		List<String> archivos = null;
		if(TB_PATRON.equals(tipoBusqueda)){
			archivos = invocarBuscarArchivoPatron(ruta,wildcard);
		}else{
			archivos = invocarBuscarArchivo(ruta,null);
		}
		if(flagBajar){
			generarRuta(rutaSalida);
			for(int i=0;i<archivos.size();i++){
				ConfigureApp.invocarBajarArchivo(
						archivos.get(i),
						rutaSalida+FilenameUtils.getName(archivos.get(i)),
						URL_WEB_SERVICE);
			}
		}
	}
	
	public static void generarRuta(String directoryName){
		try {
			FileUtils.forceMkdir(new File(directoryName));
		} catch (IOException e) {
			System.out.println("Error al crear directorio. "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	//PRODUCCION
	private static String URL_WS_PRD      = "http://118.180.13.221:8080/envioContratoWS/ws";
	private static String URL_WS_PRD1     = "http://118.180.55.11:9080/envioContratoWS/ws";
	private static String URL_WS_PRD2     = "http://118.180.55.12:9080/envioContratoWS/ws";
	private static String URL_WS_CALIDAD  = "http://118.180.14.23:8080/envioContratoWS/ws";
	private static String URL_WS_CALIDAD1 = "http://118.180.34.115:8080/envioContratoWS/ws";
	private static String URL_WS_CALIDAD2 = "http://118.180.34.116:8080/envioContratoWS/ws";
	private static String URL_WS_TEST     = "http://118.180.14.22:8080/envioContratoWS/ws";
	private static String URL_WS_TEST1    = "http://118.180.34.112:8080/envioContratoWS/ws";
	private static String URL_WS_TEST2    = "http://118.180.34.113:8080/envioContratoWS/ws";
	
	
	//POR DEFECTO EN TEST
	private static String URL_WEB_SERVICE = "http://118.180.34.113:9080/envioContratoWS/ws";
	private static final Object COD_EXITO_WS = "1";
	
	
	private static String URL_WS_LOCAL     = "http://118.216.51.55:8090/EnvioContratoWS/ws";
	private static String URL_WS_LOCAL2    = "http://118.216.33.49:8090/envioContratoWS/ws";

	public static void setEnv(String env) {
		if(env.equals("TEST")){
			URL_WEB_SERVICE = URL_WS_TEST;
		}else if(env.equals("TEST1")){
			URL_WEB_SERVICE = URL_WS_TEST1;
		}else if(env.equals("TEST2")){
			URL_WEB_SERVICE = URL_WS_TEST2;
		}else if(env.equals("CALIDAD")){
			URL_WEB_SERVICE = URL_WS_CALIDAD;
		}else if(env.equals("CALIDAD1")){
			URL_WEB_SERVICE = URL_WS_CALIDAD1;
		}else if(env.equals("CALIDAD2")){
			URL_WEB_SERVICE = URL_WS_CALIDAD2;
		}else if(env.equals("PRD")){
			URL_WEB_SERVICE = URL_WS_PRD;
		}else if(env.equals("PRD1")){
			URL_WEB_SERVICE = URL_WS_PRD1;
		}else if(env.equals("PRD2")){
			URL_WEB_SERVICE = URL_WS_PRD2;
		}else if(env.equals("LOCAL")){
			URL_WEB_SERVICE = URL_WS_LOCAL;
		}else if(env.equals("LOCAL2")){
			URL_WEB_SERVICE = URL_WS_LOCAL2;
		}
	}
	
}
