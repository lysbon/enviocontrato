package com.bbva.cuentas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

public class ProcFUOutput {

	public static void main(String[] args) throws FileNotFoundException {
		//File file    = new File("D:\\ws\\ahorroaltoque\\sprint_fatca\\scripts\\export_mnt.txt");
		File file    = new File("D:\\ws\\ahorroaltoque\\sprint_fatca\\scripts\\20170209\\output_mnt.txt");
		//File file    = new File("D:\\ws\\ahorroaltoque\\sprint_fatca\\scripts\\20170209\\output_pdte.txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		try {
			String folder = "";
			String subfolder = "";
			boolean fsubfolder = false;
			boolean fu_bpo = false;
			Set<String> hs = null;
			Map<String,Long> hm = new HashMap<String,Long>();
			long docpersubfolder=0;
			for (String line = reader.readLine();  line != null/* && numrows<45000l*/;  line = reader.readLine()) {
				
				String txtHeader = line.substring(0,5);
				if("-----".equals(txtHeader)){
					String txtSubHeader = line.substring(0,10);
					if(!"----------".equals(txtSubHeader)){
						if(hs!=null && hs.size()>0){
							System.out.println(subfolder+hs.toString()+"("+docpersubfolder+")");
						}
						docpersubfolder=0;
						folder = StringUtils.replace(line,"-","");
						System.out.println(folder);
						fsubfolder = false;
						subfolder = "";
						hs = new HashSet<String>();
						if(StringUtils.containsIgnoreCase(folder,"fu_bpo")){
							fu_bpo = true;
						}
					}else{
						if(hs!=null && hs.size()>0){
							System.out.println(subfolder+hs.toString()+"("+docpersubfolder+")");
						}
						docpersubfolder = 0;
						subfolder = StringUtils.replace(line,"-","");
						fsubfolder = true;
						hs = new HashSet<String>();
					}
				}else if("total".equals(txtHeader)){
					
				}else{
					if(fu_bpo){
						StringTokenizer st = new StringTokenizer(line);
						String token = null;
						for(int i=0;st.hasMoreTokens();i++){
							token = st.nextToken();
							if(i==8){
								String key = token.substring(30,38);
								if(hm.containsKey(key)){
									hm.put(key,(hm.get(key)+1));
								}else{
									hm.put(key,1l);
								}																
							}
						}
						docpersubfolder++;
					}else if(!fsubfolder){
						StringTokenizer st = new StringTokenizer(line);
						String token = null;
						for(int i=0;st.hasMoreTokens();i++){
							token = st.nextToken();
							if(i==8){
								System.out.println(token);
							}
						}		
					}else{
						StringTokenizer st = new StringTokenizer(line);
						String token = null;
						for(int i=0;st.hasMoreTokens();i++){
							token = st.nextToken();
							if(i==8){
								hs.add(token.substring(30,38));
							}
						}
						docpersubfolder++;
					}
				}
			}
			
			if(hm!=null && hm.size()>0){			
				Iterator<Map.Entry<String,Long>> it = hm.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry<String,Long> pair = (Map.Entry<String,Long>)it.next();
			        System.out.println(pair.getKey() + " = " + pair.getValue());
			    }
			}
			
			reader.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
