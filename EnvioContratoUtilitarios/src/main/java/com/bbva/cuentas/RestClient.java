package com.bbva.cuentas;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class RestClient {

	private static final String endpointPOST = "http://118.180.202.48:8090/CuentaIdeal_Rest/prueba/respuesta/frente/1";
	private static final String ruta = "D:\\ws\\everest\\wsjava2\\CuentaIdeal_Prj\\CuentaIdeal\\src\\main\\webapp\\resources\\img\\icons\\";
	
	private static String convertirBase64(String ruta) {
		String decoded = 
			ConfigureApp.encode(ruta);
		return decoded;
	}
	
	public static void main(String args[]){
		cargarImagen("1", ruta+"EveryMonth.png");
		cargarImagen("2", ruta+"Regular.png");
		cargarImagen("4", ruta+"more.png");
		cargarImagen("5", ruta+"Regular.png");
		cargarImagen("6", ruta+"EveryMonth.png");
		cargarImagen("7", ruta+"more.png");
		cargarImagen("8", ruta+"less.png");
		cargarImagen("22",ruta+"more.png");
		cargarImagen("23",ruta+"medium.png");
		cargarImagen("24",ruta+"less.png");
		cargarImagen("25",ruta+"more.png");
		cargarImagen("26",ruta+"less.png");
	}
	
	public static void cargarImagen(String idRespuesta, String ruta){
		try {			
			String imagen = convertirBase64(ruta);
			RestTemplate restTemplate = new RestTemplate();
			JSONObject respuesta = new JSONObject();
			respuesta.put("idRespuesta",idRespuesta);
			respuesta.put("imgFrente",imagen);
						
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<String> entity = new HttpEntity<String>(respuesta.toString(),headers);
			
			String result = restTemplate.postForObject(endpointPOST,entity,String.class);
	        
			System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
