/**
 * Cliente.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class Cliente  implements java.io.Serializable {
    private java.lang.String codigoCentral;

    private java.lang.String tipoDocumento;

    private java.lang.String nroDocumento;

    private java.lang.String email;

    private java.lang.String tipo;

    private java.lang.String idTrxHuellaDigital;
    
    /******* 23052017 ********/
    private com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems;
    private java.lang.String nombreApellido;
   

	/******* 23052017 ********/
    
    public Cliente() {
    }

    public Cliente(
           java.lang.String codigoCentral,
           java.lang.String tipoDocumento,
           java.lang.String nroDocumento,
           java.lang.String email,
           java.lang.String tipo,
           java.lang.String idTrxHuellaDigital,
           /******* 23052017 ********/
           com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems,
           java.lang.String nombreApellido
           /******* 23052017 ********/) {
           this.codigoCentral = codigoCentral;
           this.tipoDocumento = tipoDocumento;
           this.nroDocumento = nroDocumento;
           this.email = email;
           this.tipo = tipo;
           this.idTrxHuellaDigital = idTrxHuellaDigital;
           /******* 23052017 ********/
           this.listaItems = listaItems;
           this.nombreApellido = nombreApellido;
           
           /******* 23052017 ********/
    }


    /**
     * Gets the codigoCentral value for this Cliente.
     * 
     * @return codigoCentral
     */
    public java.lang.String getCodigoCentral() {
        return codigoCentral;
    }


    /**
     * Sets the codigoCentral value for this Cliente.
     * 
     * @param codigoCentral
     */
    public void setCodigoCentral(java.lang.String codigoCentral) {
        this.codigoCentral = codigoCentral;
    }


    /**
     * Gets the tipoDocumento value for this Cliente.
     * 
     * @return tipoDocumento
     */
    public java.lang.String getTipoDocumento() {
        return tipoDocumento;
    }


    /**
     * Sets the tipoDocumento value for this Cliente.
     * 
     * @param tipoDocumento
     */
    public void setTipoDocumento(java.lang.String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }


    /**
     * Gets the nroDocumento value for this Cliente.
     * 
     * @return nroDocumento
     */
    public java.lang.String getNroDocumento() {
        return nroDocumento;
    }


    /**
     * Sets the nroDocumento value for this Cliente.
     * 
     * @param nroDocumento
     */
    public void setNroDocumento(java.lang.String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }


    /**
     * Gets the email value for this Cliente.
     * 
     * @return email
     */
    public java.lang.String getEmail() {
        return email;
    }


    /**
     * Sets the email value for this Cliente.
     * 
     * @param email
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }


    /**
     * Gets the tipo value for this Cliente.
     * 
     * @return tipo
     */
    public java.lang.String getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Cliente.
     * 
     * @param tipo
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the idTrxHuellaDigital value for this Cliente.
     * 
     * @return idTrxHuellaDigital
     */
    public java.lang.String getIdTrxHuellaDigital() {
        return idTrxHuellaDigital;
    }


    /**
     * Sets the idTrxHuellaDigital value for this Cliente.
     * 
     * @param idTrxHuellaDigital
     */
    public void setIdTrxHuellaDigital(java.lang.String idTrxHuellaDigital) {
        this.idTrxHuellaDigital = idTrxHuellaDigital;
    }
    
    
 /********** 23052017 **********/   
    
    /**
     * Gets the listaItems value for this FirmaContrato.
     * 
     * @return listaItems
     */
    public com.bbva.cuentas.firmarContratos.ItemContrato[] getListaItems() {
        return listaItems;
    }


    /**
     * Sets the listaItems value for this FirmaContrato.
     * 
     * @param listaItems
     */
    public void setListaItems(com.bbva.cuentas.firmarContratos.ItemContrato[] listaItems) {
        this.listaItems = listaItems;
    }

    public com.bbva.cuentas.firmarContratos.ItemContrato getListaItems(int i) {
        return this.listaItems[i];
    }

    public void setListaItems(int i, com.bbva.cuentas.firmarContratos.ItemContrato _value) {
        this.listaItems[i] = _value;
    }
    
    
    public java.lang.String getNombreApellido() {
		return nombreApellido;
	}

	public void setNombreApellido(java.lang.String nombreApellido) {
		this.nombreApellido = nombreApellido;
	}
    
/********** 23052017 **********/       
    

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Cliente)) return false;
        Cliente other = (Cliente) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoCentral==null && other.getCodigoCentral()==null) || 
             (this.codigoCentral!=null &&
              this.codigoCentral.equals(other.getCodigoCentral()))) &&
            ((this.tipoDocumento==null && other.getTipoDocumento()==null) || 
             (this.tipoDocumento!=null &&
              this.tipoDocumento.equals(other.getTipoDocumento()))) &&
            ((this.nroDocumento==null && other.getNroDocumento()==null) || 
             (this.nroDocumento!=null &&
              this.nroDocumento.equals(other.getNroDocumento()))) &&
            ((this.email==null && other.getEmail()==null) || 
             (this.email!=null &&
              this.email.equals(other.getEmail()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.idTrxHuellaDigital==null && other.getIdTrxHuellaDigital()==null) || 
             (this.idTrxHuellaDigital!=null &&
              this.idTrxHuellaDigital.equals(other.getIdTrxHuellaDigital()))) 
             /********** 23052017 **********/ 
              &&
              ((this.listaItems==null && other.getListaItems()==null) || 
                      (this.listaItems!=null &&
                       java.util.Arrays.equals(this.listaItems, other.getListaItems())))
              &&
              ((this.nombreApellido==null && other.getNombreApellido()==null) || 
                      (this.nombreApellido!=null &&
                       this.nombreApellido.equals(other.getNombreApellido()))) 
                       
            /********** 23052017 **********/           
              
              ;
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoCentral() != null) {
            _hashCode += getCodigoCentral().hashCode();
        }
        if (getTipoDocumento() != null) {
            _hashCode += getTipoDocumento().hashCode();
        }
        if (getNroDocumento() != null) {
            _hashCode += getNroDocumento().hashCode();
        }
        if (getEmail() != null) {
            _hashCode += getEmail().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getIdTrxHuellaDigital() != null) {
            _hashCode += getIdTrxHuellaDigital().hashCode();
        }
        
        /********* 23052017 *********/
        
        if (getListaItems() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getListaItems());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getListaItems(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        
        if (getNombreApellido() != null) {
            _hashCode += getNombreApellido().hashCode();
        }
        
        /********* 23052017 *********/ 
        
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Cliente.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "cliente"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoCentral");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "codigoCentral"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipoDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "tipoDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nroDocumento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "nroDocumento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("email");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "email"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idTrxHuellaDigital");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "idTrxHuellaDigital"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);

        /******** 23052017 ********/
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("listaItems");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "listaItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "itemContrato"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        
        
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombreApellido");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "nombreApellido"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        
        /******** 23052017 ********/
        
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
