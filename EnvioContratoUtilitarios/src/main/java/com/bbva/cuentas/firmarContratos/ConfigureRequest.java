/**
 * ConfigureRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public class ConfigureRequest  implements java.io.Serializable {
    private com.bbva.cuentas.firmarContratos.ConfigurationItem[] configuration;

    public ConfigureRequest() {
    }

    public ConfigureRequest(
           com.bbva.cuentas.firmarContratos.ConfigurationItem[] configuration) {
           this.configuration = configuration;
    }


    /**
     * Gets the configuration value for this ConfigureRequest.
     * 
     * @return configuration
     */
    public com.bbva.cuentas.firmarContratos.ConfigurationItem[] getConfiguration() {
        return configuration;
    }


    /**
     * Sets the configuration value for this ConfigureRequest.
     * 
     * @param configuration
     */
    public void setConfiguration(com.bbva.cuentas.firmarContratos.ConfigurationItem[] configuration) {
        this.configuration = configuration;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConfigureRequest)) return false;
        ConfigureRequest other = (ConfigureRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.configuration==null && other.getConfiguration()==null) || 
             (this.configuration!=null &&
              java.util.Arrays.equals(this.configuration, other.getConfiguration())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConfiguration() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConfiguration());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConfiguration(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConfigureRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", ">configure-request"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("configuration");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "configuration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "configurationItem"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://bbva.com/cuentas/firmarContratos", "item"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
