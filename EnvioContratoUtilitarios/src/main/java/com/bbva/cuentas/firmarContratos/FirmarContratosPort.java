/**
 * FirmarContratosPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.cuentas.firmarContratos;

public interface FirmarContratosPort extends java.rmi.Remote {
    public com.bbva.cuentas.firmarContratos.ReenvioContratosResponse reenvioContratos(com.bbva.cuentas.firmarContratos.ReenvioContratosRequest reenvioContratosRequest) throws java.rmi.RemoteException;
    public com.bbva.cuentas.firmarContratos.ConfigureResponse configure(com.bbva.cuentas.firmarContratos.ConfigureRequest configureRequest) throws java.rmi.RemoteException;
    public com.bbva.cuentas.firmarContratos.FirmarContratosResponse firmarContratos(com.bbva.cuentas.firmarContratos.FirmarContratosRequest firmarContratosRequest) throws java.rmi.RemoteException;
}
