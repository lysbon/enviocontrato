
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

EnvioContratoWS is a rest/soap web service to generate custom pdf, validate them and certify them as a legal document, to then be launch by email to all respective customers and be stored in local repositories.

* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Backlog ###
* personalizar remitente                                           OK   FALTA TESTUNIT
* generar contratos en base64                                      --   FALTA TESTUNIT
* configurar algun documento sin firmar                            --   FALTA TESTUNIT
* cortar el proceso en cualquier exception                         OK   FALTA TESTUNIT
* mejorar la captura de la huella digital                          --   FALTA TESTUNIT
* mejorar procesamiento de correos y carga de variables            --   FALTA TESTUNIT
* revisar las mascaras de parametros en correos                    --   FALTA TESTUNIT
* revisar funcion core en rest                                     --   FALTA TESTUNIT
* construir rest para demas operaciones                            OK   FALTA TESTUNIT
* construir clientes rest en admin                                 --   FALTA TESTUNIT
* personalizar plantilla de correo para casos x                    OK   FALTA TESTUNIT
* migracion de grupo                                               --   FALTA TESTUNIT
* homologar idiomas todo a ingles                                  --   FALTA TESTUNIT

### How do I get set up? ###

* Summary of set up

Just take contact with me and I'll be glad to help you out

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin none.carlos@gmail.com
* Other community or team contact

