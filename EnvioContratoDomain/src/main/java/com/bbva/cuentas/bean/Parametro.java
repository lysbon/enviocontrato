package com.bbva.cuentas.bean;

import java.sql.Date;
import java.sql.Timestamp;

public class Parametro extends Generic{

	private Long idParam ;
	private Long tipoParam ;
	private String nombreParam ;
	private String valorParam ;
	private String descParam ;
	private String estado ;
	private Date fhCreacion;
	private Date fhModificacion;
	private String usuCreacion ;
	private String usuModificacion ;
	private Timestamp fhTimestamp  ;
	
	public Long getIdParam() {
		return idParam;
	}
	public void setIdParam(Long idParam) {
		this.idParam = idParam;
	}
		
	public Long getTipoParam() {
		return tipoParam;
	}
	public void setTipoParam(Long tipoParam) {
		this.tipoParam = tipoParam;
	}
	public String getNombreParam() {
		return nombreParam;
	}
	public void setNombreParam(String nombreParam) {
		this.nombreParam = nombreParam;
	}
	public String getValorParam() {
		return valorParam;
	}
	public void setValorParam(String valorParam) {
		this.valorParam = valorParam;
	}
	public String getDescParam() {
		return descParam;
	}
	public void setDescParam(String descParam) {
		this.descParam = descParam;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public Date getFhModificacion() {
		return fhModificacion;
	}
	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}
	public String getUsuCreacion() {
		return usuCreacion;
	}
	public void setUsuCreacion(String usuCreacion) {
		this.usuCreacion = usuCreacion;
	}
	public String getUsuModificacion() {
		return usuModificacion;
	}
	public void setUsuModificacion(String usuModificacion) {
		this.usuModificacion = usuModificacion;
	}
	public Timestamp getFhTimestamp() {
		return fhTimestamp;
	}
	public void setFhTimestamp(Timestamp fhTimestamp) {
		this.fhTimestamp = fhTimestamp;
	}
	
}