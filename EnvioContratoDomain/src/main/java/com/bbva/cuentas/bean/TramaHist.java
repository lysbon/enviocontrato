package com.bbva.cuentas.bean;

import java.sql.Timestamp;
import java.util.Date;


public class TramaHist extends Generic {
	

	private Long idTrama ;
	private Integer env;
	private String procedencia;
	private Long kit; 
	private String contrato;
	private String titCodCli;
	private String estado;
	private String repro;
	private String masivo;
	private Timestamp fhProIni;
	private Timestamp fhProFin;
	private String  cdVersion;
	
	
	public Long getIdTrama() {
		return idTrama;
	}
	public void setIdTrama(Long idTrama) {
		this.idTrama = idTrama;
	}
	public Integer getEnv() {
		return env;
	}
	public void setEnv(Integer env) {
		this.env = env;
	}
	public String getProcedencia() {
		return procedencia;
	}
	public void setProcedencia(String procedencia) {
		this.procedencia = procedencia;
	}
	public Long getKit() {
		return kit;
	}
	public void setKit(Long kit) {
		this.kit = kit;
	}
	public String getContrato() {
		return contrato;
	}
	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	public String getTitCodCli() {
		return titCodCli;
	}
	public void setTitCodCli(String titCodCli) {
		this.titCodCli = titCodCli;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getRepro() {
		return repro;
	}
	public void setRepro(String repro) {
		this.repro = repro;
	}
	public String getMasivo() {
		return masivo;
	}
	public void setMasivo(String masivo) {
		this.masivo = masivo;
	}
	public Date getFhProIni() {
		return fhProIni;
	}
	public void setFhProIni(Timestamp fhProIni) {
		this.fhProIni = fhProIni;
	}
	public Date getFhProFin() {
		return fhProFin;
	}
	public void setFhProFin(Timestamp fhProFin) {
		this.fhProFin = fhProFin;
	}
	
	public String getCdVersion() {
		return cdVersion;
	}
	public void setCdVersion(String cdVersion) {
		this.cdVersion = cdVersion;
	}

}
