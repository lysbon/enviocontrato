package com.bbva.cuentas.bean;

public class KitPlantilla extends Generic{
	
	private Long cdKit;
	private Long cdPlantilla;
	
	public Long getCdKit() {
		return cdKit;
	}
	public void setCdKit(Long cdKit) {
		this.cdKit = cdKit;
	}
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	
}
