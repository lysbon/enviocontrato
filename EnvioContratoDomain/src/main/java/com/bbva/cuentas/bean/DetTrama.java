package com.bbva.cuentas.bean;

import java.sql.Timestamp;

import com.bbva.cuentas.util.Constants;

public class DetTrama extends Generic {
	
	private Long idDetTrama;
	private Long idTrama;
	private String cdCliente;
	private Integer cdStep;
	private String cdDoc;
	private String detStep;
	private String estado;
	private Integer numReproceso;
	private Timestamp timeReproceso ;
	
	public DetTrama() {
		super();
		this.numReproceso = 0;
		this.timeReproceso = new Timestamp(System.currentTimeMillis());
		this.estado=Constants.ESTADO_PROCESO_PENDIENTE;
		
	}
	
	public Long getIdTrama() {
		return idTrama;
	}
	public void setIdTrama(Long idTrama) {
		this.idTrama = idTrama;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public Integer getCdStep() {
		return cdStep;
	}
	public void setCdStep(Integer cdStep) {
		this.cdStep = cdStep;
	}
	public String getCdDoc() {
		return cdDoc;
	}
	public void setCdDoc(String cdDoc) {
		this.cdDoc = cdDoc;
	}
	public String getDetStep() {
		return detStep;
	}
	public void setDetStep(String detStep) {
		this.detStep = detStep;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getNumReproceso() {
		return numReproceso;
	}
	public void setNumReproceso(Integer numReproceso) {
		this.numReproceso = numReproceso;
	}
	public Timestamp getTimeReproceso() {
		return timeReproceso;
	}
	public void setTimeReproceso(Timestamp timeReproceso) {
		this.timeReproceso = timeReproceso;
	}	
	
	
	public Long getIdDetTrama() {
		return idDetTrama;
	}
	public void setIdDetTrama(Long idDetTrama) {
		this.idDetTrama = idDetTrama;
	}



}
