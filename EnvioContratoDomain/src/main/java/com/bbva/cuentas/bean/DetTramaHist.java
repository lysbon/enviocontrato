package com.bbva.cuentas.bean;

import java.sql.Timestamp;
import java.util.Date;


public class DetTramaHist extends Generic {
	
	private Long idTrama ;
	private String chParCodCli;
	private Timestamp fhProIni;
	private Timestamp fhProFin;
	private Integer nroReproceso;
	private Integer cdStep;
	private String detStep;
	private String estado;
	
	public Long getIdTrama() {
		return idTrama;
	}
	public void setIdTrama(Long idTrama) {
		this.idTrama = idTrama;
	}
	public String getChParCodCli() {
		return chParCodCli;
	}
	public void setChParCodCli(String chParCodCli) {
		this.chParCodCli = chParCodCli;
	}
	public Date getFhProIni() {
		return fhProIni;
	}
	public void setFhProIni(Timestamp fhProIni) {
		this.fhProIni = fhProIni;
	}
	public Date getFhProFin() {
		return fhProFin;
	}
	public void setFhProFin(Timestamp fhProFin) {
		this.fhProFin = fhProFin;
	}
	public Integer getNroReproceso() {
		return nroReproceso;
	}
	public void setNroReproceso(Integer nroReproceso) {
		this.nroReproceso = nroReproceso;
	}
	public Integer getCdStep() {
		return cdStep;
	}
	public void setCdStep(Integer cdStep) {
		this.cdStep = cdStep;
	}
	public String getDetStep() {
		return detStep;
	}
	public void setDetStep(String detStep) {
		this.detStep = detStep;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

}
