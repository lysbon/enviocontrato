package com.bbva.cuentas.bean;

public class ResourceItem extends Generic{
	
	private long cdItem;
	private byte[] file;
	
	public long getCdItem() {
		return cdItem;
	}
	public void setCdItem(long cdItem) {
		this.cdItem = cdItem;
	}
	public byte[] getFile() {
		return file;
	}
	public void setFile(byte[] file) {
		this.file = file;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (cdItem ^ (cdItem >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResourceItem other = (ResourceItem) obj;
		if (cdItem != other.cdItem)
			return false;
		return true;
	}
	
	
	
}
