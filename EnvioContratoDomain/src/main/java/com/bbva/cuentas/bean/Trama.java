package com.bbva.cuentas.bean;

import java.sql.Date;
import java.sql.Timestamp;

public class Trama extends Generic{
	
	private String chMoti;
	private String chTrama;
	private Date fechaCreacion;
	private Integer cdEnv;
	private Integer cdStep;
	private String chTipo;
	private String chProcedencia;
	private String cdCliente;
	private String cdDoc;
	private String detStep;
	/********04122017********/
	private String estado;
	private Integer numReproceso;
	private Long idTrama;
	private Timestamp timeReproceso ;
	//private String chTrama2;
	
	

	/********04122017********/
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getNumReproceso() {
		return numReproceso;
	}
	public void setNumReproceso(Integer numReproceso) {
		this.numReproceso = numReproceso;
	}
	public Timestamp getTimeReproceso() {
		return timeReproceso;
	}
	public void setTimeReproceso(Timestamp timeReproceso) {
		this.timeReproceso = timeReproceso;
	}
	
	public String getDetStep() {
		return detStep;
	}
	public void setDetStep(String detStep) {
		this.detStep = detStep;
	}
	public Integer getCdStep() {
		return cdStep;
	}
	public void setCdStep(Integer cdStep) {
		this.cdStep = cdStep;
	}
	public String getChTipo() {
		return chTipo;
	}
	public void setChTipo(String chTipo) {
		this.chTipo = chTipo;
	}
	public String getChMoti() {
		return chMoti;
	}
	public void setChMoti(String chMoti) {
		this.chMoti = chMoti;
	}
	public String getChTrama() {
		return chTrama;
	}
	public void setChTrama(String chTrama) {
		this.chTrama = chTrama;
	}
	public String getChProcedencia() {
		return chProcedencia;
	}
	public void setChProcedencia(String chProcedencia) {
		this.chProcedencia = chProcedencia;
	}
	public Integer getCdEnv() {
		return cdEnv;
	}
	public void setCdEnv(Integer cdEnv) {
		this.cdEnv = cdEnv;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getCdDoc() {
		return cdDoc;
	}
	public void setCdDoc(String cdDoc) {
		this.cdDoc = cdDoc;
	}
	
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	
	/*
	public String getChTrama2() {
		return chTrama2;
	}
	public void setChTrama2(String chTrama2) {
		this.chTrama2 = chTrama2;
	}
	*/
	
	public Long getIdTrama() {
		return idTrama;
	}
	public void setIdTrama(Long idTrama) {
		this.idTrama = idTrama;
	}
	
}
