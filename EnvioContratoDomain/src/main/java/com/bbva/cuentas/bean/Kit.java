package com.bbva.cuentas.bean;

import java.util.Date;
import java.util.List;

public class Kit extends Generic{
	
	private Long   cdKit;
	private String cdCodigo;
	private String stFileunico;
	private String flValHuella;
	private String nbNombreKit;
	private List<Plantilla> listTemplates;
	private List<Plantilla> listTemplatesCliente;
	private Enviroment grupo;
	private EnvKit grupoKit;
	private String stSignBox;
	private String stHuella;
	private Date fechaEjecucion;
	private Trama trama;
		
	public Kit(KitLegacy kitlegacy){
		this.cdKit = null;
		this.cdCodigo = kitlegacy.getCdKit();
		this.stFileunico = kitlegacy.getStFileunico();
		this.flValHuella = kitlegacy.getFlValHuella();
		this.nbNombreKit = kitlegacy.getNbNombreKit();
		this.listTemplates = kitlegacy.getListTemplates();
		this.stSignBox = kitlegacy.getStSignBox();
		this.grupo = new Enviroment();
		this.grupoKit = new EnvKit();
	}
	
	public Kit(Long string) {
		cdKit = string;
	}

	public Kit() {
	}
	
	public Long getCdKit() {
		return cdKit;
	}
	
	public void setCdKit(Long cdKit) {
		this.cdKit = cdKit;
	}

	public List<Plantilla> getListTemplates() {
		return listTemplates;
	}
	public void setListTemplates(List<Plantilla> listTemplates) {
		this.listTemplates = listTemplates;
	}
	public String getFlValHuella() {
		return flValHuella;
	}

	public void setFlValHuella(String flValHuella) {
		this.flValHuella = flValHuella;
	}

	public String getStFileunico() {
		return stFileunico;
	}

	public void setStFileunico(String stFileunico) {
		this.stFileunico = stFileunico;
	}

	public String getNbNombreKit() {
		return nbNombreKit;
	}

	public void setNbNombreKit(String nbNombreKit) {
		this.nbNombreKit = nbNombreKit;
	}

	public String getCdCodigo() {
		return cdCodigo;
	}

	public void setCdCodigo(String cdCodigo) {
		this.cdCodigo = cdCodigo;
	}

	public String getStSignBox() {
		return stSignBox;
	}

	public void setStSignBox(String stSignBox) {
		this.stSignBox = stSignBox;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cdKit == null) ? 0 : cdKit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kit other = (Kit) obj;
		if (cdKit == null) {
			if (other.cdKit != null)
				return false;
		} else if (!cdKit.equals(other.cdKit))
			return false;
		return true;
	}

	public Enviroment getGrupo() {
		if(grupo == null) grupo = new Enviroment();
		return grupo;
	}

	public void setGrupo(Enviroment grupo) {
		this.grupo = grupo;
	}

	public EnvKit getGrupoKit() {
		if(grupoKit == null) grupoKit = new EnvKit();
		return grupoKit;
	}

	public void setGrupoKit(EnvKit grupoKit) {
		this.grupoKit = grupoKit;
	}

	public List<Plantilla> getListTemplatesCliente() {
		return listTemplatesCliente;
	}

	public void setListTemplatesCliente(List<Plantilla> listTemplatesCliente) {
		this.listTemplatesCliente = listTemplatesCliente;
	}
	
	public String getStHuella() {
		return stHuella;
	}

	public void setStHuella(String stHuella) {
		this.stHuella = stHuella;
	}

	@Override
	public String toString() {
		return "Kit [cdKit=" + cdKit + ", cdCodigo=" + cdCodigo + ", nbNombreKit=" + nbNombreKit + "]";
	}

	public Trama getTrama() {
		return trama;
	}

	public void setTrama(Trama trama) {
		this.trama = trama;
	}
	
}
