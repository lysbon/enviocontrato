package com.bbva.cuentas.bean;

import com.bbva.cuentas.util.Constants;

public class ItemStamp extends Generic{
	
	private Long     cdItem;
	private Long     cdSeccion;
	private Integer  nuCoorX;
	private Integer  nuCoorY;
	private String   nbNombreVar;
	private String   nbValorVar;
	private String   nbFuente;
	private Integer  nuAlinear;
	private String   chTipo;
	private Integer  nuAncho;
	private Integer  nuScX;
	private Integer  nuScY;
	private Integer  nuIdxCliente;
	private String   nbMask;
	private String   stActivo;
		
	private byte[] img;
	
	public String getNbMask() {
		return nbMask;
	}

	public void setNbMask(String nbMask) {
		this.nbMask = nbMask;
	}
	
	public ItemStamp(){		
	}
	
	public ItemStamp(String _label,String _value,Integer _x,Integer _y){
		nbNombreVar = _label;
		nbValorVar = _value;
		nuCoorX = _x;
		nuCoorY = _y;
		nuAlinear = Constants.ALIGN_LEFT;
		chTipo = Constants.TYPE_TEXT;
	}
	public ItemStamp(String _label,Integer _x,Integer _y, String _type){
		nbNombreVar = _label;
		nuCoorX = _x;
		nuCoorY = _y;
		nuAlinear = Constants.ALIGN_LEFT;
		chTipo = _type;
	}

	public Long getCdItem() {
		return cdItem;
	}

	public void setCdItem(Long cdItem) {
		this.cdItem = cdItem;
	}

	public Long getCdSeccion() {
		return cdSeccion;
	}

	public void setCdSeccion(Long cdSeccion) {
		this.cdSeccion = cdSeccion;
	}

	public Integer getNuCoorX() {
		return nuCoorX;
	}

	public void setNuCoorX(Integer nuCoorX) {
		this.nuCoorX = nuCoorX;
	}

	public Integer getNuCoorY() {
		return nuCoorY;
	}

	public void setNuCoorY(Integer nuCoorY) {
		this.nuCoorY = nuCoorY;
	}

	public String getNbNombreVar() {
		return nbNombreVar;
	}

	public void setNbNombreVar(String nbNombreVar) {
		this.nbNombreVar = nbNombreVar;
	}

	public String getNbValorVar() {
		return nbValorVar;
	}

	public void setNbValorVar(String nbValorVar) {
		this.nbValorVar = nbValorVar;
	}

	public String getNbFuente() {
		return nbFuente;
	}

	public void setNbFuente(String nbFuente) {
		this.nbFuente = nbFuente;
	}

	public Integer getNuAlinear() {
		return nuAlinear;
	}

	public void setNuAlinear(Integer nuAlinear) {
		this.nuAlinear = nuAlinear;
	}
	public Integer getNuAncho() {
		return nuAncho;
	}

	public void setNuAncho(Integer nuAncho) {
		this.nuAncho = nuAncho;
	}

	public Integer getNuScX() {
		return nuScX;
	}

	public void setNuScX(Integer nuScX) {
		this.nuScX = nuScX;
	}

	public Integer getNuScY() {
		return nuScY;
	}

	public void setNuScY(Integer nuScY) {
		this.nuScY = nuScY;
	}

	public Integer getNuIdxCliente() {
		return nuIdxCliente;
	}

	public void setNuIdxCliente(Integer nuIdxCliente) {
		this.nuIdxCliente = nuIdxCliente;
	}
	public String getChTipo() {
		return chTipo;
	}

	public void setChTipo(String chTipo) {
		this.chTipo = chTipo;
	}
	
	public String getStActivo() {
		return stActivo;
	}

	public void setStActivo(String stActivo) {
		this.stActivo = stActivo;
	}
	
	public byte[] getImg() {
		return img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (cdItem ^ (cdItem >>> 32));
		result = prime * result + (int) (cdSeccion ^ (cdSeccion >>> 32));
		result = prime * result + ((nbNombreVar == null) ? 0 : nbNombreVar.hashCode());
		result = prime * result + ((chTipo == null) ? 0 : chTipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemStamp other = (ItemStamp) obj;
		if (cdItem != other.cdItem)
			return false;
		if (cdSeccion != other.cdSeccion)
			return false;
		if (nbNombreVar == null) {
			if (other.nbNombreVar != null)
				return false;
		} else if (!nbNombreVar.equals(other.nbNombreVar))
			return false;
		if (chTipo == null) {
			if (other.chTipo != null)
				return false;
		} else if (!chTipo.equals(other.chTipo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemStamp [cdItem=" + cdItem + ", cdSeccion=" + cdSeccion + ", nuCoorX=" + nuCoorX + ", nuCoorY="
				+ nuCoorY + ", nbNombreVar=" + nbNombreVar + ", nbValorVar=" + nbValorVar + "]";
	}
	
	
	
}