package com.bbva.cuentas.bean;

public class Adjunto {
	
	private String nombreCorreo;
	private String ruta;
	private Long   cdPlantilla;
	
	public Long getCdPlantilla() {
		return cdPlantilla;
	}
	public void setCdPlantilla(Long cdPlantilla) {
		this.cdPlantilla = cdPlantilla;
	}
	public String getNombreCorreo() {
		return nombreCorreo;
	}
	public void setNombreCorreo(String nombreCorreo) {
		this.nombreCorreo = nombreCorreo;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	
}
