package com.bbva.cuentas.bean;

public class Enviroment {
	
	private Integer cdEnv;
	private String  nbNombre;
	private String  nbDescripcion;
	private String  stActivo;
	private String  stDefecto;
	private String  chTipo;
	private String  nbRutagestdoc;
	private String  stHuella;
	
	public Integer getCdEnv() {
		return cdEnv;
	}
	public void setCdEnv(Integer cdEnv) {
		this.cdEnv = cdEnv;
	}
	public String getNbNombre() {
		return nbNombre;
	}
	public void setNbNombre(String nbNombre) {
		this.nbNombre = nbNombre;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public String getStActivo() {
		return stActivo;
	}
	public void setStActivo(String stActivo) {
		this.stActivo = stActivo;
	}
	public String getChTipo() {
		return chTipo;
	}
	public void setChTipo(String chTipo) {
		this.chTipo = chTipo;
	}
	public String getStDefecto() {
		return stDefecto;
	}
	public void setStDefecto(String stDefecto) {
		this.stDefecto = stDefecto;
	}
	public String getNbRutagestdoc() {
		return nbRutagestdoc;
	}
	public void setNbRutagestdoc(String nbRutagestdoc) {
		this.nbRutagestdoc = nbRutagestdoc;
	}
	public String getStHuella() {
		return stHuella;
	}
	public void setStHuella(String stHuella) {
		this.stHuella = stHuella;
	}
	
}
