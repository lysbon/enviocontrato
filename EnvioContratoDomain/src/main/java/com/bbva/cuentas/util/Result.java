package com.bbva.cuentas.util;

import java.util.List;

public class Result {
	public static final String ERROR_CODE = "error-code";
	public static final String ERROR_MESSAGE = "error-message";	
	
	private Integer statusCode;
	private String errorCode;
	private String errorMessage;
	private Object entity;
	private List<?> entities;
		
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Object getEntity() {
		return entity;
	}
	public void setEntity(Object entity) {
		this.entity = entity;
	}
	public List<?> getEntities() {
		return entities;
	}
	public void setEntities(List<?> entities) {
		this.entities = entities;
	}
}
