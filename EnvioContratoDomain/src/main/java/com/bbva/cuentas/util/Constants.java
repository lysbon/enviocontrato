package com.bbva.cuentas.util;

public class Constants {
	
	private Constants() {
	    throw new IllegalStateException("Utility class");
	}
	
	public static final String RESULT_OK = "1";
	public static final String RESULT_KO = "0";
	public static final int NRESULT_OK = 1;
	public static final int NRESULT_KO = 0;
	
	public static final String ID_DEDO_DER = "2";
	public static final String ID_DEDO_IZQ = "7";
	
	public static final String TYPE_TEXT     		= "T";	
	public static final String TYPE_IMAGE    		= "I";
	public static final String TYPE_IMAGE_BASE64    = "J";
	public static final String TYPE_IMAGE_HUELLA    = "H";
	public static final String TYPE_BAR_CODE 		= "B";
	
	public static final String TYPE_DES_TEXT      		= "Texto";
	public static final String TYPE_DES_IMAGE     		= "Imagen";
	public static final String TYPE_DES_IMAGE_BASE64 	= "Imagen Base64";
	public static final String TYPE_DES_IMAGE_HUELLA 	= "Imagen Huella";
	public static final String TYPE_DES_BAR_CODE  		= "Código de Barras";
	public static final String TYPE_DES_UNDEFINED 		= "Indefinido";
	
	public static final String STS_CONCIL_NOAPLICA = "NA";
	public static final String STS_VALID_OK = "OK";
	
	public static final int IDX_NRO_DOC = 8;
		
	public static final String PAQUETE_SERVICIO   = "PBG_ENV_DIGITAL_CONF";
	public static final String PAQUETE_ADMIN      = "PBG_ENV_DIGITAL_ADMIN_CONF";
	public static final String PAQUETE_ADMIN_TRX  = "PBG_ENV_DIGITAL_ADMIN_TRX";
	public static final String PLANTILLA_FIJA     = "F";
	public static final String PLANTILLA_VARIABLE = "V";
	
	public static final String FONT_COD_CALIBRI          = "Calibri";
	public static final String FONT_COD_ARIAL            = "Arial";
	public static final String FONT_COD_COURIER_NEW      = "CourierNew";
	public static final String FONT_COD_BOLD             = "Bold";
	public static final String FONT_COD_8                = "8";
	public static final String FONT_COD_9                = "9";
	public static final String FONT_COD_10               = "10";
	public static final String FONT_COD_11               = "11";
	public static final String FONT_COD_12               = "12";
	public static final String FONT_COD_DEFAULT          = "Calibri11";
	
	public static final String FONT_ITEXT_CALIBRI          = "Calibri";
	public static final String FONT_ITEXT_ARIAL            = "Arial";
	public static final String FONT_ITEXT_COURIER_NEW      = "Courier New";
	
	public static final String ST_ACTIVO           = "A";
	public static final String ST_INACTIVO         = "I";
	
	public static final String TIPO_CONSTR_PDF     = "P";
	public static final String TIPO_CONSTR_JASPER  = "J";
	public static final String TIPO_CONSTR_JASPER_C= "C";
	public static final String TIPO_CONSTR_NUEVO   = "N";
	public static final String TIPO_CONSTR_JSON    = "S";
	public static final String TIPO_CONSTR_JSON_C  = "K";
	
	public static final int MIN_SIZE_PDF = 100;
	public static final int MIN_SIZE_JASPER = 10;
		
	public static final String ST_ABSOLUTO        = "A";
	public static final String ST_COMPLEJO        = "C";
	public static final Integer EQUAL = 1;
	public static final Integer NOEQUAL = 2;
	
	public static final String TIPO_HTML  = "HTML";
	public static final String TIPO_PDF   = "PDF";
	public static final String TIPO_IMAGE = "IMAGE";
	public static final String TIPO_QUERY = "QUERY";
	public static final String TIPO_EXECUTE = "EXECUTE";
	public static final String TIPO_LS = "LS";
	public static final String LLAVE_EXT = "EXT";
	public static final String TIPO_DEL = "DEL";
	public static final String TIPO_MKDIR = "MKDIR";
	public static final String LLAVE_NAME = "NAME";
	public static final String TIPO_BASE64_PUT = "BASE64_PUT";
	public static final String TIPO_BASE64_GET = "BASE64_GET";
	public static final String LLAVE_WILDCARD = "WILDCARD";
	
	public static final String ST_TRUE  = "true";
	public static final String ST_FALSE = "false";
	public static final String RESULT_OK_MSG = "Envio de contrato exitoso";
	public static final String ENGINE_STR_TPL = "STRTPL";
	public static final String ENGINE_LANG3 =   "LANG3";
	public static final String FLAG_TIPO_EMAIL = "E";
	public static final String FLAG_TIPO_PLANTILLA = "T";
	public static final String PREFIX_FLAG = "F";
		
	public static final String PREFIX_HUELLA  = "HUELLA_";
	public static final String PREFIX_MINUCIA = "MINUCIA_";
	
	public static final Integer ALIGN_LEFT = 0;
	public static final Integer ALIGN_CENTER = 1;
	public static final Integer ALIGN_RIGHT = 2;
	public static final String ALIGN_DES_LEFT = "Izquierda";
	public static final String ALIGN_DES_CENTER = "Centro";
	public static final String ALIGN_DES_RIGHT = "Derecha";
	
	public static final String TXT_FIJO = "Fijo";
	public static final String TXT_VARIABLE = "Variable";
	public static final String TXT_ACTIVO   = "Activo";
	public static final String TXT_INACTIVO = "Inactivo";	
	public static final String TXT_TIPO_CONST_PDF = "PDF";
	public static final String TXT_TIPO_CONST_NUEVO = "Nuevo";
	public static final String TXT_TIPO_CONST_JASPER = "JRXML";
	public static final String TXT_TIPO_CONST_JASPERC = "JASPER";
	
	public static final String ST_IT_MOD = "M";
	public static final String ST_IT_CRE = "C";
	public static final String ST_IT_DEL = "D";
	public static final String ST_IT_SIN = "S";
	public static final String ST_DES_CRE = "Crear";
	public static final String ST_DES_MOD = "Modificar";
	public static final String ST_DES_DEL = "Eliminar";
	public static final String ST_DES_SIN = "Sin Cambios";
	
	public static final String SCHEMA = "APDIGB";
	public static final String SCHEMA_POINT = ".";
	public static final String VACIO = "";
	
	public static final String TIPO_FILEUNICO_CONTRATO  = "0002";
	public static final String TIPO_FILEUNICO_SOLICITUD = "0005";
	public static final String TIPO_FILEUNICO_FATCA     = "0013";
	public static final String TIPO_FILEUNICO_PEP       = "0004";
	public static final String TIPO_FILEUNICO_DAE       = "0233";
	
	public static final String TIPO_TRAMA_ERROR   = "E";
	public static final String TIPO_TRAMA_GENERAL = "G";
	
	public static final String TIPO_PLANTILLA_GENERICA = "G";
	public static final String TIPO_PLANTILLA_PERSONAL = "P";
	public static final String TXT_TIPO_GENERICA = "General";
	public static final String TXT_TIPO_PERSONAL = "Personal";
	
	public static final String EXT_CSV = ".csv";
	public static final String EXT_PDF = ".pdf";
	public static final int CLIENTE_INDICE_PREVIEW = 0;
	
	public static final int RUTA_IDX_ORIGINAL = 0;
	public static final int RUTA_IDX_FIRMA = 1;
	public static final int RUTA_IDX_CSV = 2;
	public static final int RUTA_IDX_TEMPFILEUNICO = 3;
	
	public static final String GESTDOC_ONLINE = "online";
	public static final String GESTDOC_BATCH  = "batch";
	
	/******** MTI 04122017 *********/
	public static final int LABEL_LENGTH = 8;
	public static final String CONSTANTE_REPROCESO = "I";
	
	public static final String LABEL_CABECERA_INICIO ="<CAB>";
	public static final String LABEL_CABECERA_FIN = "</CAB>";
	
	public static final String LABEL_GENERAL_INICIO = "<GEN>";
	public static final String LABEL_GENERAL_FIN = "</GEN>";
	
	public static final String LABEL_PARTICIPE_INICIO = "<PAR>";
	public static final String LABEL_PARTICIPE_FIN = "</PAR>";
	
	
	/****** Datos Configuracion Cabecera *******/
	public static final String COD_PARAM_NROCONTRATO = "CFG0|P01";
	public static final String COD_PARAM_OFIGESTOR = "CFG0|P02";
	public static final String COD_PARAM_CODPROCEDEN = "CFG0|P09";
	public static final String COD_PARAM_CODDIVISA = "CFG0|P10";
	public static final String COD_PARAM_IDFORMATO = "CFG0|P21";
	public static final String COD_PARAM_NROCONTRATO_METADATA = "CFG0|P27";
	public static final String COD_PARAM_GESTDOC_FILEUNICO = "CFG0|P28";
	public static final String COD_PARAM_GRUPO = "CFG0|P29";
	/****** Datos Configuracion Cabecera *******/

	/****** Datos Configuracion Cliente *******/
	public static final String COD_PARAM_CODCEN_TIT1 = "CFG0|P07";
	public static final String COD_PARAM_TIPDOC_TIT1 = "CFG0|P03";
	public static final String COD_PARAM_NRODOC_TIT1 = "CFG0|P04";
	public static final String COD_PARAM_EMAIL_TIT1 = "CFG0|P11";
	public static final String COD_PARAM_TRX_HUELLA = "CFG0|P17";
	public static final String COD_PARAM_NOMBREAPELIDO_TIT1 = "CFG0|P05";
		
		
	public static final String TEXTO_NUMCONTRATO = "numeroContrato";
	public static final String TEXTO_OFICINAGESTORA = "oficinaGestora";
	public static final String TEXTO_CODIGO_PROCEDENCIA = "procedencia";
	public static final String TEXTO_ID_KIT = "idContrato";
	public static final String TEXTO_FLAG_BATCH_ONLINE = "gestDoc";
	public static final String TEXTO_ID_GRUPO = "idGrupo";
		
	/************* Actualizacion 24052017 **************/

	public static final String TIPO_PROCESO_FIRMA_DIGITAL = "F"; // Proceso de firma digital
	public static final String TIPO_REPROCESO_FIRMA_DIGITAL = "R"; // Reproceso

	public static final String ESTADO_PROCESO_PENDIENTE = "P";
	//public static final String ESTADO_PROCESO_EXITOSO = "R";
	public static final String ESTADO_PROCESO_ERROR = "E";
	public static final String ESTADO_PROCESO_SELECCIONADO = "S";
	public static final String ESTADO_PROCESO_REPROCESADO = "R";
	public static final String ESTADO_PROCESO_EXITO = "A";	

	public static final String ESTADO_SIGBOX_EXITOSO = "0"; // Resultado Exitoso
	public static final String ESTADO_SIGBOX_ERROR = "3"; // Resultado Exitoso

	public static final String TEXTO_SEPARACION_TRAMA = "<!$!>"; // Resultado Exitoso
	public static final String TEXTO_EXTRACION_SEPARACION_TRAMA = "<![$]!>"; // Resultado Exitoso
		
	/******** MTI 04122017 *********/

	public static final int DESTINO_TEMPFILEUNICO = 1;
	public static final int DESTINO_FILEUNICOBATCH = 2;
	public static final String COD_KIT_DEFAULT = "PVIEWKIT";
	
	public static final String MAX_INDEX = "__MAXINDEX__";
	
	public static final String FORMATO_BBVA = "BBVAFormato";
	public static final String NUMERO_SOL = "numeroSolicitud";
	
	
		
	
}