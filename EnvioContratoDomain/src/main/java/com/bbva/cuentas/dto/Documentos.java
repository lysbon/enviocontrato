package com.bbva.cuentas.dto;

import java.util.ArrayList;
import java.util.List;

public class Documentos {
	private List<Documento> documentos;
	private List<DocumentoCliente> clientes;
	public List<Documento> getDocumentos() {
		return documentos;
	}
	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}
	public List<DocumentoCliente> getClientes() {
		if(clientes==null)
			clientes = new ArrayList<DocumentoCliente>();
		return clientes;
	}
	public void setClientes(List<DocumentoCliente> clientes) {
		this.clientes = clientes;
	}
	
	
}
