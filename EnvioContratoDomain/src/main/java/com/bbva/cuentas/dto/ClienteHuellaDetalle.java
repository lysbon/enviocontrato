package com.bbva.cuentas.dto;

public class ClienteHuellaDetalle {
	private String tipoDocumento;
	private String numeroDocumento;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String nombres;
	private String templateIndiceDerecho;
	private String templateIndiceIzquierdo;
	private String imgIndiceDerecho;
	private String imgIndiceIzquierdo;
	private String fechaHora;
	private String requestId;
	private String idDedo;
	private String descripcionDedo;
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getTemplateIndiceDerecho() {
		return templateIndiceDerecho;
	}
	public void setTemplateIndiceDerecho(String templateIndiceDerecho) {
		this.templateIndiceDerecho = templateIndiceDerecho;
	}
	public String getTemplateIndiceIzquierdo() {
		return templateIndiceIzquierdo;
	}
	public void setTemplateIndiceIzquierdo(String templateIndiceIzquierdo) {
		this.templateIndiceIzquierdo = templateIndiceIzquierdo;
	}
	public String getImgIndiceDerecho() {
		return imgIndiceDerecho;
	}
	public void setImgIndiceDerecho(String imgIndiceDerecho) {
		this.imgIndiceDerecho = imgIndiceDerecho;
	}
	public String getImgIndiceIzquierdo() {
		return imgIndiceIzquierdo;
	}
	public void setImgIndiceIzquierdo(String imgIndiceIzquierdo) {
		this.imgIndiceIzquierdo = imgIndiceIzquierdo;
	}
	public String getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public String getIdDedo() {
		return idDedo;
	}
	public void setIdDedo(String idDedo) {
		this.idDedo = idDedo;
	}
	public String getDescripcionDedo() {
		return descripcionDedo;
	}
	public void setDescripcionDedo(String descripcionDedo) {
		this.descripcionDedo = descripcionDedo;
	}
	
	@Override
	public String toString() {
		return "ClienteHuellaDigital [tipoDocumento=" + tipoDocumento + ", numeroDocumento=" + numeroDocumento
				+ ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno + ", nombres="
				+ nombres + ", templateIndiceDerecho=" + templateIndiceDerecho + ", templateIndiceIzquierdo="
				+ templateIndiceIzquierdo + ", imgIndiceDerecho=" + imgIndiceDerecho + ", imgIndiceIzquierdo="
				+ imgIndiceIzquierdo + ", fechaHora=" + fechaHora + ", requestId=" + requestId + ", idDedo=" + idDedo
				+ ", descripcionDedo=" + descripcionDedo + "]";
	}
	
}
