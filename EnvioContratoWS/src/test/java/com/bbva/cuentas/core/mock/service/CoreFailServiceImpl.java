package com.bbva.cuentas.core.mock.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.FileUnicoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.impl.CoreServiceImpl;

@Service
public class CoreFailServiceImpl extends CoreServiceImpl implements CoreService{

	public void process(RequestDTO request,Kit kit) throws ServiceException {
		List<ClienteDTO> listaClientes = null;
		try {
	
			if (kit != null && request.getListaClientes() != null) {
				Map<String, Object> parametros = util.toMap(request);
				if(huellaActivo(kit))
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes(),null,null,null,null));
				
	
				
				for (int i = 0; i < listaClientes.size(); i++) {
					ClienteDTO cliente = listaClientes.get(i);
					
					
					
					Map<String, Object> parametrosCliente = 
						util.toMap(request,cliente.getCliente());
					processMail(request,kit,cliente,parametrosCliente);
					//processFileUnico(request,kit,cliente);
				}
			}
		} catch (ServiceException e) {

		} catch (Exception e) {
			logger.error(e);
		} finally {
			depurarArchivos(listaClientes);
		}
	}
	
	public void processMail(RequestDTO request, Kit kit,ClienteDTO cliente,Map<String, Object> parametros) {
		if ("90016480".equals(cliente.getCliente().getCodigoCentral()) ||
			"90016482".equals(cliente.getCliente().getCodigoCentral())) {
			try {
				throw new EnvioMailException("Fail Intent");
			} catch (ServiceException e) {
				
			}
		}
	}
	
	public void processFileUnico(ClienteDTO cliente) {
		if ("90016481".equals(cliente.getCliente().getCodigoCentral()) ||
			"90016482".equals(cliente.getCliente().getCodigoCentral())) {
			try {
				throw new FileUnicoException("Fail Intent");
			} catch (ServiceException e) {
			
			}
		}
	}
	
}
