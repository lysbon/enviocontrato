package com.bbva.cuentas.aat.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class KitServiceTest {
	
	@Autowired
	private KitService kitService;
	
	@Value("${service.init.ruta.recursos}")
	private String rutaFormatos;
	
	@Test
	public void obtenerkitsTest() throws ServiceException{		
		List<Kit> kits = kitService.obtenerKits();
		validarListaKits(kits);
	}
	
	@Test
	public void obtenerkitsTestPorEnv() throws ServiceException{		
		List<Kit> kits = kitService.obtenerKits(ParamConstants.TPL_CD_ENV);
		validarListaKits(kits);
	}
	
	@Test
	public void obtenerKitPorCodigo() throws ServiceException{
		Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.TPL_CD_ENV,ParamConstants.KIT_CD_CODIGO_DEFAULT);
		validarKitCompleto(kit);
	}
	
	@Test
	public void obtenerKitPorId() throws ServiceException{		
		List<Kit> kits = kitService.obtenerKits(ParamConstants.TPL_CD_ENV);
		Kit kit = kitService.obtenerKitPorId(kits.get(0).getCdKit());
		validarKit(kit);
	}
	
	public Long guardarkitTest() throws ServiceException{
		Kit kit = new Kit();
		kit.setCdCodigo(ParamConstants.KIT_CD_CODIGO_DEFAULT);
		kit.getGrupoKit().setNbCorreoAsunto(ParamConstants.KIT_CORREO_ASUNTO);
		kit.getGrupoKit().setStEnvCorreo(Constants.ST_ACTIVO);
		kit.setStFileunico(Constants.ST_ACTIVO);
		kit.setFlValHuella(Constants.ST_ACTIVO);
		kit.getGrupoKit().setNbCorreoEngine(Constants.ENGINE_STR_TPL);
		kit.setNbNombreKit(ParamConstants.KIT_NOMBRE_KIT);		
		Long cdkit = kitService.guardarKit(kit);
		kit.getGrupoKit().setCdEnv(ParamConstants.KIT_CD_ENV);
		kit.getGrupoKit().setCdKit(cdkit);
		kitService.guardarEnvKit(kit.getGrupoKit());
		assertThat(cdkit!=null,is(true));
		assertThat(cdkit>0,is(true));
		return cdkit;
	}
	
	@Test
	public void actualizarkitTest() throws ServiceException{
		Kit kit = new Kit();
		kit.setCdKit(ParamConstants.KIT_CD_KIT_TRX);
		kit.setCdCodigo(ParamConstants.KIT_CD_CODIGO_DEFAULT);
		kit.getGrupoKit().setNbCorreoAsunto(ParamConstants.KIT_CORREO_ASUNTO);
		kit.getGrupoKit().setStEnvCorreo(Constants.ST_ACTIVO);
		kit.setStFileunico(Constants.ST_ACTIVO);
		kit.setFlValHuella(Constants.ST_ACTIVO);
		kit.getGrupoKit().setNbCorreoEngine(Constants.ENGINE_STR_TPL);
		kit.setNbNombreKit(ParamConstants.KIT_NOMBRE_KIT);
		Integer result = kitService.actualizarKit(kit);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void eliminarKitTest() throws ServiceException{
		Integer result = kitService.eliminarKit(ParamConstants.KIT_CD_KIT_TRX);
		assertThat(result!=null,is(true));
	}
	
	@Test
	public void transaccionTest() throws ServiceException{		
		Long cdKit = guardarkitTest();
		//Kit kit = kitService.obtenerKitPorId(cdKit);
		Kit kit = kitService.obtenerKitPorId(ParamConstants.KIT_CD_ENV, cdKit);
		assertThat(kit!=null,is(true));
		assertThat(kit.getCdKit(),is(cdKit));
		assertThat(kit.getCdCodigo(),is(ParamConstants.KIT_CD_CODIGO_DEFAULT));
		//assertThat(kit.getGrupoKit().getNbCorreoAsunto(),is(ParamConstants.KIT_CORREO_ASUNTO));
		//assertThat(kit.getGrupoKit().getStEnvCorreo(),is(Constants.ST_ACTIVO));
		assertThat(kit.getStFileunico(),is(Constants.ST_ACTIVO));
		assertThat(kit.getFlValHuella(),is(Constants.ST_ACTIVO));
		//assertThat(kit.getGrupoKit().getNbCorreoEngine(),is(Constants.ENGINE_STR_TPL));
		assertThat(kit.getNbNombreKit(),is(ParamConstants.KIT_NOMBRE_KIT));
		
		Kit kitUpd = new Kit();
		BeanUtils.copyProperties(kit, kitUpd);
		kitUpd.setStFileunico(Constants.ST_INACTIVO);
		kitUpd.getGrupoKit().setNbCorreoEngine(Constants.ENGINE_LANG3);
		kitUpd.setNbNombreKit(ParamConstants.KIT_NOMBRE_KIT_2);
		kitUpd.getGrupoKit().setNbCorreoAsunto(ParamConstants.KIT_CORREO_ASUNTO_2);
		Integer result = kitService.actualizarKit(kitUpd);
		kitUpd.getGrupoKit().setCdEnv(ParamConstants.KIT_CD_ENV);
		kitUpd.getGrupoKit().setCdKit(kit.getCdKit());
		kitService.actualizarEnvKit(kitUpd.getGrupoKit());
		assertThat(result!=null,is(true));
		assertThat(result>0,is(true));
		
		//kitService.actualizarEmailTemplate(kitUpd.getCdKit(), rutaFormatos+ParamConstants.TPL_HTML_RUTA1);
		//String html = kitService.obtenerMailTemplate(kitUpd.getCdKit(),ParamConstants.KIT_CD_ENV);
		//validarEmail(html);
		
		kit = kitService.obtenerKitPorId(ParamConstants.KIT_CD_ENV, cdKit);
		assertThat(kit!=null,is(true));
		assertThat(kit.getCdKit(),is(cdKit));
		assertThat(kit.getCdCodigo(),is(ParamConstants.KIT_CD_CODIGO_DEFAULT));
		//assertThat(kit.getGrupoKit().getNbCorreoAsunto(),is(ParamConstants.KIT_CORREO_ASUNTO_2));
		//assertThat(kit.getGrupoKit().getStEnvCorreo(),is(Constants.ST_ACTIVO));
		assertThat(kit.getStFileunico(),is(Constants.ST_INACTIVO));
		assertThat(kit.getFlValHuella(),is(Constants.ST_ACTIVO));
		//assertThat(kit.getGrupoKit().getNbCorreoEngine(),is(Constants.ENGINE_LANG3));
		assertThat(kit.getNbNombreKit(),is(ParamConstants.KIT_NOMBRE_KIT_2));
		
		Integer resultEliminar = kitService.eliminarKit(cdKit);
		assertThat(resultEliminar!=null,is(true));
		assertThat(resultEliminar>0,is(true));
		kit = kitService.obtenerKitPorId(cdKit);
		assertThat(kit==null,is(true));
	}
	
	private void validarEmail(String html) {
		assertThat(html!=null,is(true));
		assertThat(html.length()>0,is(true));
	}

	private void validarListaKits(List<Kit> kits){
		assertThat(kits!=null,is(true));
		assertThat(kits.size()>0,is(true));		
		for(Kit kit : kits){
			validarKit(kit);
		}
	}
	
	private void validarKit(Kit kit){
		assertThat(kit!=null,is(true));
		assertThat(StringUtils.isEmpty(kit.getCdCodigo()),is(false));
		assertThat(kit.getFlValHuella()==null,is(false));
		assertThat(StringUtils.isEmpty(kit.getNbNombreKit()),is(false));
		assertThat(StringUtils.isEmpty(kit.getStFileunico()),is(false));
		assertThat(kit.getCdKit()!=null,is(true));
	}

	private void validarKitCompleto(Kit kit){
		assertThat(kit!=null,is(true));
		assertThat(StringUtils.isEmpty(kit.getCdCodigo()),is(false));
		assertThat(kit.getFlValHuella()==null,is(false));
		//assertThat(StringUtils.isEmpty(kit.getLbCorreoPlantilla()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbCorreoAsunto()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getNbCorreoEngine()),is(false));
		assertThat(StringUtils.isEmpty(kit.getNbNombreKit()),is(false));
		assertThat(StringUtils.isEmpty(kit.getGrupoKit().getStEnvCorreo()),is(false));
		assertThat(StringUtils.isEmpty(kit.getStFileunico()),is(false));
		assertThat(kit.getCdKit()!=null,is(true));
	}
	
}
