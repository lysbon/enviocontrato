package com.bbva.cuentas.mock.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.bean.Adjunto;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.MailService;
import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.impl.MailServiceImpl;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

@Service
public class MailServiceMockImpl extends MailServiceImpl implements MailService{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${ruta.formatoEmail}")
	private String rutaFormatos;
	
	@Autowired
	private FlagService flagService;
	
	
	public void sendMailProc(
			Cliente cliente,
			Map<String,Object> parametrosCliente,
			String asunto,
			String plantillaCorreo,
			String motor,
			String remitente,
			String remitenteCC,
			List<Adjunto> adjuntos) throws EnvioMailException{
    	logger.info("[ini]");
		String outputCorreo = null;
		String outputAsunto = null;
		try {
			if(Constants.ENGINE_STR_TPL.equals(motor)){
				List<Flag> listFlags = flagService.obtenerFlagsPorTipo(Constants.FLAG_TIPO_EMAIL);
				outputCorreo   = procesarPlantillaStrTpl(plantillaCorreo, parametrosCliente, listFlags, true);
				outputAsunto   = procesarPlantillaStrTpl(asunto         , parametrosCliente, listFlags, false);
			}else{
				outputCorreo = procesarPlantillaLangs3(plantillaCorreo, parametrosCliente, true);
				outputAsunto = procesarPlantillaLangs3(asunto         , parametrosCliente, false);
			}				
			writeMail(outputAsunto,outputCorreo,parametrosCliente);
			
		} catch (Exception e){
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new EnvioMailException("No se pudo generar correo.");
		}
	}

	private void writeMail(String asunto,String correo,Map<String, Object> parametrosCliente) {
		String rutaOutput = FilenameUtils.concat(rutaFormatos, "mailOutput");
		Util.generarRuta(rutaOutput);
		
		BufferedWriter bw = null;
		FileWriter fw = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(asunto);
			sb.append("\n");
			sb.append("\n");
			sb.append(correo);
			for(Map.Entry<String,Object> entry : parametrosCliente.entrySet()) {
				sb.append(String.format("|%30s|", entry.getKey())+"-"+entry.getValue()+"\n");
			}
			
			fw = new FileWriter(rutaOutput);
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
