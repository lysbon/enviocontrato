package com.bbva.cuentas.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.datasource.DataFiller;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = "/app-config.xml")
//@ContextConfiguration("resources/test_applicationContext.xml")
//@ContextConfiguration(locations = "classpath:app-config.xml")
@ContextConfiguration("/app-config-h2.xml")
public class GenerarContratosTest {

	@Autowired
	CoreService coreService;
	@Autowired
	KitCoreService kitCoreService;
	@Autowired
	GenerarContratosService generarContratosService;
	@Autowired
	KitService kitService;
	@Autowired
	HuellaDigitalService huellaService;
	@Autowired
	PlantillaService plantillaService;
	@Autowired
	private Util util;
	
	@Autowired
	DataFiller filler;
	
	@Value("${ruta.original}")
	private String rutaOriginal;
	@Value("${service.init.ruta.recursos}")
	private String rutaFormatos;

	public void generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ClienteDTO cliente,
    		List<ClienteDTO> clientes,FirmaContrato firmaContrato) throws ServiceException {
    	try {
    		if(plantillas!=null){
        		for(Plantilla template : plantillas){
        			template.setValida(true);
        			List<ConfigurationPath> listCfg = cliente.getListCfg().get(template.getCdPlantilla());
        			for(ConfigurationPath cfg : listCfg) {
        				generarContratosService.generarContrato(null,null,null,null, 0);
        			}
        		    
            	}
        	}
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
    }

	private List<Plantilla> todasPlantillasActivas() throws ServiceException{
		System.out.println("entro xx");
		List<Plantilla> plantillasActivas = new ArrayList<Plantilla>();
		System.out.println("entro x1");
		List<Plantilla> plantillas = plantillaService.obtenerPlantillas();
		System.out.println("entro x2");
		for(Plantilla plantilla : plantillas){
			if(Constants.ST_ACTIVO.equals(plantilla.getStActivo())){
				System.out.println("entro x4");
				plantillasActivas.add(plantilla);
			}
		}
		System.out.println("entro x5");
		return plantillasActivas;
	}

	@Test
	public void test_generar_contrato_plantillas() throws Exception{
		
		List<Plantilla> plantillas = todasPlantillasActivas();
		 assertEquals("hello world","hello world");
		/*Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.KIT_CD_ENV,ParamConstants.KIT_CD_CODIGO_DEFAULT);
		assertThat(kit!=null,is(true));
		
		kitCoreService.cargarPlantillasContratoCliente(kit,plantillas);
		RequestDTO firmaContrato = filler.prepararDataContrato(kit,plantillas);

		Map<String,Object> parametros = util.toMap(firmaContrato);
		parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(firmaContrato.getListaClientes(),null,null,null,null));
		List<ClienteDTO> clientes = coreService.procesarRutas(kit, firmaContrato);
		ClienteDTO cliente = Util.obtenerTitular(clientes);
		
		//generarContratosService.generarContratos(kit.getListTemplates(), parametros, null,cliente,clientes,firmaContrato);
		//generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametros, null,cliente,clientes,firmaContrato);
		this.generarContratos(kit.getListTemplates(), parametros, null,cliente,clientes,firmaContrato);
		this.generarContratos(kit.getListTemplatesCliente(), parametros, null,cliente,clientes,firmaContrato);

		for(Plantilla tpl : kit.getListTemplates()){
			List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				assertThat(StringUtils.isEmpty(cfg.getRutaSalidaPDF()),is(false));
				if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && tpl.isValida()){
					File file = new File(cfg.getRutaSalidaPDF());
					assertThat(file.isFile(),is(true));
				}
			}
		}*/
	}


	@Test
	public void test_generar_contrato_kits() throws Exception{
		List<Kit> kits = kitService.obtenerKits();
		for(Kit kit : kits){
			test_generar_contrato_kit(kit.getCdKit());
		}
	}
	
	public void test_generar_contrato_kit(Long id) throws Exception{
		
		Kit kit = kitService.obtenerKitPorId(id);
		assertThat(kit!=null,is(true));
		RequestDTO firmaContrato = filler.prepararDataContrato(kit);
		
		Map<String,Object> parametrosGeneral = util.toMap(firmaContrato);
		parametrosGeneral.putAll(huellaService.obtenerHuellasDigitalesBase64(firmaContrato.getListaClientes(),null,null,null,null));
		kitCoreService.cargarPlantillasContratoCliente(kit);
		List<ClienteDTO> clientes = coreService.procesarRutas(kit, firmaContrato);
		ClienteDTO cliente = Util.obtenerTitular(clientes);
		Map<String,Object> parametrosCliente = util.toMap(firmaContrato,cliente.getCliente());
		
		generarContratosService.generarContratos(kit.getListTemplates(), parametrosGeneral, null,cliente,0);
		generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametrosCliente, null,cliente,0);
		
		for(Plantilla tpl : kit.getListTemplates()){
			List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				assertThat(StringUtils.isEmpty(cfg.getRutaSalidaPDF()),is(false));
				if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && tpl.isValida()){
					File file = new File(cfg.getRutaSalidaPDF());
					assertThat(file.isFile(),is(true));
				}
			}
		}
	}
	
	@Test
	public void test_generar_contrato_fuentes() throws Exception{
		Kit kit = kitCoreService.obtenerKitPorGrupo(null, "070000C");
		kitCoreService.cargarPlantillasContratoCliente(kit);
		RequestDTO firmaContrato = filler.prepararDataContrato(kit);
		Map<String,Object> parametrosGeneral = util.toMap(firmaContrato);
		List<ClienteDTO> clientes = coreService.procesarRutas(kit, firmaContrato);
		ClienteDTO cliente = Util.obtenerTitular(clientes);
		Map<String,Object> parametrosCliente = util.toMap(firmaContrato,cliente.getCliente());
		try {
			generarContratosService.generarContratos(kit.getListTemplates(), parametrosGeneral, null,cliente,0);
			generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametrosCliente, null, cliente,0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(Plantilla tpl : kit.getListTemplates()){
			List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				assertThat(StringUtils.isEmpty(cfg.getRutaSalidaPDF()),is(false));
				if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && tpl.isValida()){
					File file = new File(cfg.getRutaSalidaPDF());
					assertThat(file.isFile(),is(true));
				}
			}
		}
	}
	
	@Test
	public void test_generar_contrato_blanco() throws Exception{
		Kit kit = kitCoreService.obtenerKitPorGrupo(null, "020000C");
		kitCoreService.cargarPlantillasContratoCliente(kit);
		RequestDTO firmaContrato = filler.prepararDataContrato(kit);
		Map<String,Object> parametrosGeneral = util.toMap(firmaContrato);
		List<ClienteDTO> clientes = coreService.procesarRutas(kit, firmaContrato);
		ClienteDTO cliente = Util.obtenerTitular(clientes);
		Map<String,Object> parametrosCliente = util.toMap(firmaContrato,cliente.getCliente());
		try {
			for(Plantilla plt : kit.getListTemplates()){
				plt.setStTipoConstr(Constants.TIPO_CONSTR_NUEVO);
			}
			generarContratosService.generarContratos(kit.getListTemplates(), parametrosGeneral, null, cliente,0);
			generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametrosCliente, null, cliente,0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(Plantilla tpl : kit.getListTemplates()){
			List<ConfigurationPath> listCfg = cliente.getListCfg().get(tpl.getCdPlantilla());
			for(ConfigurationPath cfg : listCfg) {
				assertThat(StringUtils.isEmpty(cfg.getRutaSalidaPDF()),is(false));
				if(Constants.ST_ACTIVO.equals(tpl.getStActivo()) && tpl.isValida()){
					File file = new File(cfg.getRutaSalidaPDF());
					assertThat(file.isFile(),is(true));
				}
			}
		}
	}
	
		
	//@Test
	public void evaluarPlantillaTest() throws DaoException, ServiceException{
		Map<String,Object> parametros = new HashMap<String, Object>();
		//plantillas fijas
		evaluarPlantilla("BG351", parametros, true);
		evaluarPlantilla("CLAUG", parametros, true);
		evaluarPlantilla("DICTA", parametros, true);
		evaluarPlantilla("BG461", parametros, true);
		evaluarPlantilla("BG111", parametros, true);
		evaluarPlantilla("BG151", parametros, true);
		evaluarPlantilla("BG451", parametros, true);
		evaluarPlantilla("BG511", parametros, true);
		evaluarPlantilla("BG971", parametros, true);
		evaluarPlantilla("BGB81", parametros, true);
		evaluarPlantilla("BGC41", parametros, true);
		evaluarPlantilla("BGB81", parametros, true);
		evaluarPlantilla("CT011", parametros, true);
		
		/*
		parametros.put("CTR0|P19","N");
		evaluarPlantilla("BG871",parametros, true);
		parametros.put("CTR0|P19","S");
		evaluarPlantilla("BG871_NUEVO",parametros, true);
		*/
		/*
		parametros.put("CTR0|P15","1");
		evaluarPlantilla("E0600",parametros, true);
		parametros.put("CTR0|P15","2");
		evaluarPlantilla("E0600",parametros, true);
		parametros.put("CTR0|P15","3");
		evaluarPlantilla("E0600",parametros, true);
		parametros.put("CTR0|P15","4");
		evaluarPlantilla("E0600",parametros,false);
		*/
		parametros.put("CFG0|P18","I");
		evaluarPlantilla("E3362",parametros,true);
		parametros.put("CFG0|P18","II");
		evaluarPlantilla("E3362",parametros,false);
		
		parametros.put("CFG0|P13","S");
		evaluarPlantilla("E2862",parametros,true);
		parametros.put("CFG0|P13","N");
		evaluarPlantilla("E2862",parametros,false);
		/*
		13  15      18
		PEP	FATCA	RBA	FAM 	PDF
		NO	NO		SI			1
		SI	NO		SI			2
		NO	SI		SI			3
		SI	SI		SI			4
		SI	NO		NO			5
		NO	SI		NO			6
		SI	SI		NO			7
		-------------------------
		NO	NO		NO			0
		*/
		/*
		evaluarPlantillaGrupal("Indicaciones_1",false,0,true);
		evaluarPlantillaGrupal("Indicaciones_2",true, 0,true);
		evaluarPlantillaGrupal("Indicaciones_3",false,1,true);
		evaluarPlantillaGrupal("Indicaciones_4",true, 2,true);
		evaluarPlantillaGrupal("Indicaciones_5",true, 0,false);
		evaluarPlantillaGrupal("Indicaciones_6",false,1,false);
		evaluarPlantillaGrupal("Indicaciones_7",true, 3,false);
		
		evaluarPlantillaGrupal("Indicaciones_1",true,0,true,	false);
		evaluarPlantillaGrupal("Indicaciones_1",false,1,true,	false);
		evaluarPlantillaGrupal("Indicaciones_1",false,0,false,	false);
		*/
	}
	
	public void evaluarPlantilla(String nombre,Map<String,Object> parametros,boolean aserto) throws ServiceException, DaoException{
		Plantilla plantilla = plantillaService.obtenerPlantillaPorNombre(nombre);
		boolean resultado = generarContratosService.evaluarPlantilla(plantilla, parametros);
		assertThat(resultado,is(aserto));
	}
	
	public void evaluarPlantillaGrupal(String nombre,
			boolean st13,int st15,boolean st18) throws ServiceException, DaoException{
		evaluarPlantillaGrupal(nombre, st13, st15, st18, true);
	}
	
	public void evaluarPlantillaGrupal(String nombre,
			boolean st13,int st15,boolean st18, boolean asserto) throws ServiceException, DaoException{
		Map<String,Object> parametros = new HashMap<String, Object>();
		if(st13){
			parametros.put("CTR0|P13", "S");
		}else{
			parametros.put("CTR0|P13", "N");
		}
		if(st18){
			parametros.put("CTR0|P18", "I");
		}else{
			parametros.put("CTR0|P18", "II");
		}
		switch (st15) {
			case 0:parametros.put("CTR0|P15", "0");break;
			case 1:parametros.put("CTR0|P15", "1");break;
			case 2:parametros.put("CTR0|P15", "2");break;
			case 3:parametros.put("CTR0|P15", "3");break;
		}
		Plantilla plantilla = plantillaService.obtenerPlantillaPorNombre(nombre);
		boolean resultado = generarContratosService.evaluarPlantilla(plantilla, parametros);
		assertThat(resultado,is(asserto));
	}
	
	@Test
	public void test_generar_contrato_imagen_base64() throws Exception{
		Kit kit = kitCoreService.obtenerKitPorGrupo(null, "DNIBIO");
		kitCoreService.cargarPlantillasContratoCliente(kit);
		RequestDTO firmaContrato = filler.prepararDataContrato(kit);
		Map<String,Object> parametrosGeneral = util.toMap(firmaContrato);
		List<ClienteDTO> clientes = coreService.procesarRutas(kit, firmaContrato);
		ClienteDTO cliente = Util.obtenerTitular(clientes);
		generarContratosService.generarContratos(kit.getListTemplates(), 
				parametrosGeneral, null, cliente,0);
	}
			
}
