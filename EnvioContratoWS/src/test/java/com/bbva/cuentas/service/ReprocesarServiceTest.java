package com.bbva.cuentas.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.datasource.DataFiller;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.Constants;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration("/app-config-h2-fail.xml")
public class ReprocesarServiceTest {

	@Autowired
	DataFiller filler;
	@Autowired
	PivoteService pivot;
	
	@Test
	public void testReprocesar() throws ServiceException{
		//generarTramasReproceso
		RequestDTO firmaContrato = filler.prepararDataContrato();
		firmaContrato.setAsync(Constants.ST_FALSE);
		pivot.process(firmaContrato);
		
		String stop = "STOP";
		System.out.println(stop);
		
	}
	
}
