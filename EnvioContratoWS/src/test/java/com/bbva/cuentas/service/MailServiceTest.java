package com.bbva.cuentas.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class MailServiceTest {

	@Autowired
	private MailService mailService;
	@Autowired
	private KitService  kitService;
	@Autowired
	private EnviromentService envService;
	@Autowired
	private Util util;
	
	private RequestDTO prepararData() {
		RequestDTO firmaContrato = new RequestDTO();
		Cliente cliente = new Cliente();
		cliente.setCodigoCentral("90016480");
		cliente.setTipoDocumento("L");
		cliente.setNroDocumento("43590095");
		cliente.setEmail("CARLOS.SOSA.NAVARRO@BBVA.COM");
		cliente.setIdTrxHuellaDigital("50220#138098");
		firmaContrato.getListaClientes().add(0, cliente);
		firmaContrato.setNumeroContrato("00110130230209049292");
		firmaContrato.setOficinaGestora("0130");
		firmaContrato.setProcedencia(ParamConstants.PROCEDENCIA_NACAR);
		
		Map<String,String> parametros = new HashMap<String,String>();
		parametros.put("CTR0G01","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0P05","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0A01","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0A08","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0P01","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0P16","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0P15","LALALALALALALALALALALALALALALALALALALALALALA");
		parametros.put("CTR0P18","LALALALALALALALALALALALALALALALALALALALALALA");
		
		ItemContrato ic = null;
		Iterator<Entry<String,String>> it = parametros.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String,String> pair = (Map.Entry<String,String>)it.next();
	        ic = new ItemContrato();
	        ic.setLabel(pair.getKey());
			ic.setValue(pair.getValue());
			firmaContrato.getListaItems().add(ic);
	    }
		return firmaContrato;
	}
	
	@SuppressWarnings("unused")
	@Test
	public void sendMailProc_Test() throws ServiceException{
		RequestDTO firmaContrato = prepararData();
		List<Enviroment> envs = envService.obtenerEnvs();
		int indiceCliente = 0;
		for(Enviroment env : envs){
			List<Kit> kits = kitService.obtenerKits(env.getCdEnv());
			for(Kit kit : kits){
				String plantilla = kitService.obtenerMailTemplate(kit.getCdKit(),env.getCdEnv());
				if(!StringUtils.isEmpty(plantilla)){
					Map<String,Object> parametrosCliente = util.toMap(firmaContrato);
					kit.setGrupo(env);
					ClienteDTO cliente = new ClienteDTO();
					cliente.setCliente(firmaContrato.getListaClientes().get(0));
					mailService.sendMailProc(firmaContrato, parametrosCliente, kit, cliente);
					System.out.println(mailService.getClass());
					break;
				}
			}
		}
	}
	
}
