package com.bbva.cuentas.service;

import java.sql.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.datasource.DataFiller;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.ParamConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class ErrorServiceTest {

	@Autowired
	private ErrorService errorService;
	@Autowired
	private KitService kitService;
	@Autowired
	private EnviromentService envService;
	@Autowired
	private DataFiller filler;
	
	@Test
	public void test_guardarTrama() throws Exception{
		Kit kit = kitService.obtenerKitPorCodigo(ParamConstants.KIT_CD_ENV, ParamConstants.KIT_CD_CODIGO_DEFAULT);
		Enviroment grupo = envService.obtenerEnvPorCodigo(ParamConstants.KIT_CD_ENV);
		kit.setGrupo(grupo);
		FirmaContrato firmaContrato = filler.prepararDataContrato(kit);
		try {
			throw new ServiceException("Cualquier cosa");
		} catch (ServiceException e) {
			Timestamp timeActual = new Timestamp(System.currentTimeMillis());

			//errorService.guardarTrama(firmaContrato, e, kit.getGrupo().getCdEnv(),"",0,timeActual,null);
		}
		firmaContrato = filler.prepararDataContrato(kit);
		ClienteDTO cliente = new ClienteDTO();
		cliente.setCliente(firmaContrato.getListaClientes().get(0));
		try {
			try {
				throw new Exception("12345678901234567890");
			} catch (Exception e) {
				throw new ServiceException(ErrorCodes.S_ERROR_TRM_GUARDAR_TRAMA, e.getMessage());
			}
		} catch (ServiceException e) {
			Timestamp timeActual = new Timestamp(System.currentTimeMillis());
			//errorService.guardarTrama(firmaContrato, cliente, e, kit.getGrupo().getCdEnv(),"",0,timeActual,null);

		}
		
		int validarOperacion=10;
		validarOperacion = validarOperacion + 10;
	}
	
}
