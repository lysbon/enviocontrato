package com.bbva.cuentas.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.datasource.DataFiller;
import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.dto.DocumentoCliente;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.util.ParamConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/app-config-h2.xml")
public class CoreServiceTest {
	
	@Autowired
	CoreService coreService;	
	@Autowired
	GenerarContratosService generarContratosService;	
	@Autowired
	KitService kitService;	
	@Autowired
	PlantillaService plantillaService;
	@Autowired
	DataFiller filler;
	
	@Value("${ruta.original}")
	private String ruta;
	
	@Test
	public void testPreviewKit() throws Exception{
		Kit kit = kitService.obtenerKitPorId(1031l);
		RequestDTO request = filler.prepararDataContrato(kit);
		Documentos documentos = coreService.processPreviewKit(request);
		assertThat(documentos.getDocumentos()!=null,is(true));
		for(Documento documento : documentos.getDocumentos()){
			OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,documento.getDescripcion()+".pdf"));
			out.write(Base64.decodeBase64(documento.getDocumento()));
			out.close();
		}
		assertThat(documentos.getClientes()!=null,is(true));
		for(DocumentoCliente cliente: documentos.getClientes()){
			assertThat(cliente.getDocumentos()!=null,is(true));
			for(Documento documento : cliente.getDocumentos()){
				OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				out.write(Base64.decodeBase64(documento.getDocumento()));
				out.close();
				
				File file = new File(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				assertThat(file.exists(), is(true));
			}
		}
	}
	
	@Test
	public void testPreviewPlantillaPDF() throws Exception{
		RequestDTO request = filler.prepararDataPlantilla(ParamConstants.TPL_NOMBRE_FORMATO_GANADORA);
		Documentos documentos = coreService.processPreviewPlantilla(request);
		assertThat(documentos.getDocumentos()!=null,is(true));
		for(Documento documento : documentos.getDocumentos()){
			OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,documento.getDescripcion()+".pdf"));
			out.write(Base64.decodeBase64(documento.getDocumento()));
			out.close();
		}
		assertThat(documentos.getClientes()!=null,is(true));
		for(DocumentoCliente cliente: documentos.getClientes()){
			assertThat(cliente.getDocumentos()!=null,is(true));
			for(Documento documento : cliente.getDocumentos()){
				OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				out.write(Base64.decodeBase64(documento.getDocumento()));
				out.close();
				
				File file = new File(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				assertThat(file.exists(), is(true));
			}
		}
	}
	
	@Test
	public void testPreviewPlantillaJasper() throws Exception{
		RequestDTO request = filler.prepararDataPlantilla(ParamConstants.TPL_NOMBRE_FORMATO_EECC);
		Documentos documentos = coreService.processPreviewPlantilla(request);
		assertThat(documentos.getDocumentos()!=null,is(true));
		for(Documento documento : documentos.getDocumentos()){
			OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,documento.getDescripcion()+".pdf"));
			out.write(Base64.decodeBase64(documento.getDocumento()));
			out.close();
		}
		assertThat(documentos.getClientes()!=null,is(true));
		for(DocumentoCliente cliente: documentos.getClientes()){
			assertThat(cliente.getDocumentos()!=null,is(true));
			for(Documento documento : cliente.getDocumentos()){
				OutputStream out = new FileOutputStream(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				out.write(Base64.decodeBase64(documento.getDocumento()));
				out.close();
				
				File file = new File(FilenameUtils.concat(ruta,cliente.getCodCliente()+"_"+documento.getDescripcion()+".pdf"));
				assertThat(file.exists(), is(true));
			}
		}
	}
	
}
