package com.bbva.cuentas.aat.dao.impl;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.PlantillaHistDao;
import com.bbva.cuentas.aat.dao.mapper.PlantillaMapper;
import com.bbva.cuentas.bean.Plantilla;

@Component
public class PlantillaHistDaoImpl extends BaseDaoImpl<Plantilla, Long> implements PlantillaHistDao{
	
	private static final Logger logger = LoggerFactory.getLogger(PlantillaHistDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG002_PLANTILLA_HIST";
	static final String TABLE_PK  = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	PlantillaMapper plantillaMapper;
	
	@Autowired
	public PlantillaHistDaoImpl(DataSource datasource, PlantillaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
		
	
	@Override
	public List<Plantilla> obtenerPlantillasPorKitEstado(Long cdKit, String estado,Timestamp fecHoraCreacion) throws DaoException {
		List<Plantilla> listaPlantilla = new ArrayList<Plantilla>();
		try{
			String sql = 
					"SELECT P.CD_PLANTILLA,P.ST_ACTIVO,P.ST_CONDICION,P.ST_FILEUNICO,P.ST_ENV_CORREO,"+
					"P.ST_TIPO_CONSTR,P.ST_SIGNED,P.NB_NOMBRE_FORMATO,P.NB_NOMBRE_CORREO,P.NB_TIPO_FILEUNICO, "+
					"P.CH_TIPO_PLANTILLA,P.NB_DESCRIPCION "+
					"FROM   "+KitPlantillaDaoImpl.TABLE_NAME_HIST+" KP "+
					"INNER  JOIN "+TABLE_NAME+" P "+
					"ON     KP.CD_PLANTILLA = P.CD_PLANTILLA "+
					"WHERE  KP.CD_KIT = ? "+
					"AND    CAST(KP.FH_INICIO AS TIMESTAMP)<=? "+
					"AND    ?<=CAST(NVL(KP.FH_FIN,SYSDATE) AS TIMESTAMP) "+
					"AND    P.ST_ACTIVO = ? "+
					"AND    CAST(P.FH_INICIO AS TIMESTAMP)<=? "+
					"AND    ?<=CAST(NVL(P.FH_FIN,SYSDATE) AS TIMESTAMP) "+
					"ORDER  BY P.CD_PLANTILLA";
			Object[] args = new Object[6];
			args[0] = cdKit;
			args[1] = fecHoraCreacion;
			args[2] = fecHoraCreacion;
			args[3] = estado;
			args[4] = fecHoraCreacion;
			args[5] = fecHoraCreacion;
			listaPlantilla = getJdbcTemplate().query(sql,args,plantillaMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return listaPlantilla;
	}	
	
	public byte[] obtenerPDFTemplate(Long cdPlantilla,Timestamp fecHoraCreacion) {
		final LobHandler lobHandler = new DefaultLobHandler();
		List<byte[]> lista = getJdbcTemplate().query(
				"SELECT  LB_PDF_PLANTILLA "
				+" FROM  "+TABLE_NAME
				+" WHERE  CD_PLANTILLA = ? "
				+" AND    CAST(FH_INICIO AS TIMESTAMP)<=? "
				+" AND    ?<=CAST(NVL(FH_FIN,SYSDATE) AS TIMESTAMP) "
			    +" ORDER  BY CD_PLANTILLA DESC",
				new Object[] { cdPlantilla,fecHoraCreacion,fecHoraCreacion },
				new RowMapper<byte[]>(){
					@Override
					public byte[] mapRow(ResultSet rs, int rownumber) throws SQLException {
						byte[] blob = lobHandler.getBlobAsBytes(rs, "LB_PDF_PLANTILLA");
						return blob;
					}
				}
        );
		if(lista!=null && lista.size()>0)
			return lista.get(0);
		return null;
	}

}
