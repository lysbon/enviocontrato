package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.FlagPlantillaDao;
import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.FlagPlantilla;

@Service
public class FlagPlantillaServiceImpl implements FlagPlantillaService{

	private static final Logger logger = LoggerFactory.getLogger(FlagPlantillaServiceImpl.class);
	
	@Autowired
	private FlagPlantillaDao flagPlantillaDao;

	@Override
	public List<FlagPlantilla> obtenerFlagsPorPlantilla(Long id)
			throws GenerarContratoException {
		try {
			return flagPlantillaDao.obtenerFlagsPorPlantilla(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new GenerarContratoException("No se pudo obtener flags por plantilla.");
		}
	}

	@Override
	public List<FlagPlantilla> obtenerFlagPlantillas() throws ServiceException {
		try {
			return flagPlantillaDao.getAllEntities();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FPT_OBTENER_FLAG_PLANTILLAS,"No se pudo obtener flags.");
		}
	}
	
	@Override
	public FlagPlantilla obtenerFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException {
		try {
			return flagPlantillaDao.obtenerFlagPlantilla(cdPlantilla,cdFlag);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FPT_OBTENER_FLAG_PLANTILLAS,"No se pudo obtener flags.");
		}
	}

	@Override
	public Integer guardarFlagPlantilla(FlagPlantilla flagPlantilla)
			throws ServiceException {
		try {
			return flagPlantillaDao.insert(flagPlantilla);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FPT_GUARDAR_FLAG_PLANTILLA,"No se pudo guardar flag plantilla.");
		}
	}

	@Override
	public Integer actualizarFlagPlantilla(FlagPlantilla flagPlantilla)
			throws ServiceException {
		try {
			return flagPlantillaDao.update(flagPlantilla);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FPT_ACTUALIZAR_FLAG_PLANTILLA,"No se pudo actualizar flag plantilla.");
		}
	}

	@Override
	public Integer eliminarFlagPlantilla(Long cdPlantilla,Long cdFlag) throws ServiceException {
		try {
			return flagPlantillaDao.delete(cdPlantilla,cdFlag);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_FPT_ELIMINAR_FLAG_PLANTILLA,"No se pudo eliminar flag plantilla.");
		}
	}

	
}
