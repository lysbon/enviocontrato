package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ParametroDao;
import com.bbva.cuentas.aat.service.ParametroService;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;


@Service
public class ParametroServiceImpl implements ParametroService {
	
private static final Logger logger = LoggerFactory.getLogger(ParametroServiceImpl.class);
	
	@Autowired
	private ParametroDao parametroDao;
	
	public List<Parametro> obtenerParametroReprocesar(String Id_Param) throws ServiceException {
		try {
			return parametroDao.obtenerParametroParaReproceso(Id_Param);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener Crom para ejecución de reproceso automático.");
		}
	}   

	@Override
	public Parametro obtenerValorById(String idParam) throws ServiceException {
		try {
			
			return parametroDao.obtenerValorById(idParam);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,
					"No se pudo obtener Crom para ejecución de reproceso automático.");
		}
	}
	
	@Override
	public Parametro obtenerValorByName(String name) throws ServiceException {
		try {
			
			return parametroDao.obtenerValorByName(name);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,
					"No se pudo obtener Crom para ejecución de reproceso automático.");
		}
	}

}
