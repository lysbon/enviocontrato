package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.KitDao;
import com.bbva.cuentas.aat.dao.mapper.KitEnvMapper;
import com.bbva.cuentas.aat.dao.mapper.KitMapper;
import com.bbva.cuentas.bean.Kit;

@Component
public class KitDaoImpl extends BaseDaoImpl<Kit, Long> implements KitDao{
	
	private static final Logger logger = LoggerFactory.getLogger(KitDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG010_KIT";
	static final String TABLE_PK = "CD_KIT";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"kit_seq";
	
	@Autowired
	KitMapper kitMapper;
	@Autowired
	KitEnvMapper kitEnvMapper;
	
	@Autowired
	public KitDaoImpl(DataSource datasource, KitMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	
	@Override
	public Long insertAndReturnId(Kit kit) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(KitMapper.CD_CODIGO, kit.getCdCodigo());
		args.put(KitMapper.ST_FILEUNICO, kit.getStFileunico());
		args.put(KitMapper.FL_VAL_HUELLA, kit.getFlValHuella());
		args.put(KitMapper.NB_NOMBRE_KIT, kit.getNbNombreKit());
		return super.insertAndReturnId(args);
	}

	@Override
	public List<Kit> obtenerKitPorEnv(Integer cdEnv) throws DaoException {
		List<Kit> listaKits = new ArrayList<Kit>();
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+EnvKitDaoImpl.TABLE_NAME+" EP "+
					"ON     P.CD_KIT = EP.CD_KIT "+
					"WHERE  EP.CD_ENV = ? "+
					"ORDER  BY P.CD_KIT";
			Integer[] args = new Integer[1];
			args[0] = cdEnv;
			listaKits = getJdbcTemplate().query(sql,args,kitEnvMapper);			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return listaKits;
	}

	@Override
	public int update(Kit kit) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(KitMapper.CD_CODIGO, kit.getCdCodigo());
		args.put(KitMapper.NB_NOMBRE_KIT, kit.getNbNombreKit());
		args.put(KitMapper.ST_FILEUNICO, kit.getStFileunico());
		args.put(KitMapper.FL_VAL_HUELLA, kit.getFlValHuella());
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(KitMapper.CD_KIT, kit.getCdKit());
		return super.update(args, condiciones);
	}

	@Override
	public Kit obtenerKitPorCodigo(Integer cdEnv, String codigo)
			throws DaoException {
		List<Kit> listaKits = null;
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+EnvKitDaoImpl.TABLE_NAME+" EP "+
					"ON     P.CD_KIT = EP.CD_KIT "+
					"WHERE  EP.CD_ENV = ? "+
					"AND    P.CD_CODIGO = ? "+
					"ORDER  BY P.CD_KIT";
			Object[] args = new Object[2];
			args[0] = cdEnv;
			args[1] = codigo;
			listaKits = getJdbcTemplate().query(sql,args,kitEnvMapper);
			if(listaKits!=null && !listaKits.isEmpty()){
				return listaKits.get(0);
			}
			return null;
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
	}
	
	@Override
	public Kit obtenerKitPorIdEnv(Integer cdEnv, Long cdKit)
			throws DaoException {
		List<Kit> listaKits = null;
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+EnvKitDaoImpl.TABLE_NAME+" EP "+
					"ON     P.CD_KIT = EP.CD_KIT "+
					"WHERE  EP.CD_ENV = ? "+
					"AND    P.CD_KIT = ? "+
					"ORDER  BY P.CD_KIT";
			Object[] args = new Object[2];
			args[0] = cdEnv;
			args[1] = cdKit;
			listaKits = getJdbcTemplate().query(sql,args,kitEnvMapper);
			if(listaKits!=null && !listaKits.isEmpty()){
				return listaKits.get(0);
			}
			return null;
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
	}
	
	@Override
	public List<Kit> obtenerKitsPorCodigo(String codigo)
			throws DaoException {
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"WHERE  P.CD_CODIGO = ? "+
					"ORDER  BY P.CD_KIT";
			Object[] args = new Object[1];
			args[0] = codigo;
			return getJdbcTemplate().query(sql,args,kitMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
	}

	@Override
	public Kit obtenerKitPorId(Long cdKit) throws DaoException {
		return super.findById(cdKit);
	}
	
}
