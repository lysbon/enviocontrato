package com.bbva.cuentas.aat.dao;

import java.util.List;
import com.bbva.cuentas.bean.Plantilla;

public interface PlantillaDao extends BaseDao<Plantilla, Long>{
	
	public byte[] obtenerPDFTemplate(Long cdPlantilla) throws DaoException;
	
	public List<Plantilla> obtenerPlantillasPorKit(Long cdKit) throws DaoException;
	
	public Integer actualizarPDF(final Long cdPlantilla, String filename) throws DaoException;
	
	public Integer actualizarPDF(final Long cdPlantilla, byte[] bytes) throws DaoException;
	
	public Plantilla obtenerPlantillasPorNombre(String nombre) throws DaoException;

	public List<Plantilla> obtenerPlantillasPorKitEstado(Long cdKit, String stActivo) throws DaoException;

	public List<Plantilla> obtenerPlantillas() throws DaoException;
	
}
