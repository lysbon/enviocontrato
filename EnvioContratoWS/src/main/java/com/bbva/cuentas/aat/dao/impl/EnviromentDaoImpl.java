package com.bbva.cuentas.aat.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.EnviromentDao;
import com.bbva.cuentas.aat.dao.mapper.EnviromentMapper;
import com.bbva.cuentas.bean.Enviroment;

@Component
public class EnviromentDaoImpl extends BaseDaoImpl<Enviroment, Integer> implements EnviromentDao{
	
	private static final Logger logger = LoggerFactory.getLogger(EnviromentDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG009_ENV";
	static final String TABLE_PK = "CD_ENV";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"env_seq";
	
	@Autowired
	public EnviromentDaoImpl(DataSource datasource, EnviromentMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<Enviroment> obtenerEnvPorTipo(String tipo) throws DaoException{
		List<Enviroment> envs=null;		
		try{
			SqlCondicion[] condiciones = new SqlCondicion[1];
			condiciones[0] = new SqlCondicion(EnviromentMapper.CH_TIPO,tipo);
			envs = this.getEntities(condiciones);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return envs;
	}
	
	@Override
	public Integer insertAndReturnId(Enviroment env) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnviromentMapper.NB_NOMBRE, env.getNbNombre());
		args.put(EnviromentMapper.NB_DESCRIPCION, env.getNbDescripcion());
		args.put(EnviromentMapper.ST_ACTIVO, env.getStActivo());
		args.put(EnviromentMapper.CH_TIPO, env.getChTipo());
		args.put(EnviromentMapper.ST_DEFECTO, env.getStDefecto());
		args.put(EnviromentMapper.NB_RUTAGESTDOC, env.getNbRutagestdoc());
		return super.insertAndReturnId(args);
	}
	
	@Override
	public int update(Enviroment env) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnviromentMapper.NB_NOMBRE, env.getNbNombre());
		args.put(EnviromentMapper.NB_DESCRIPCION, env.getNbDescripcion());
		args.put(EnviromentMapper.ST_ACTIVO, env.getStActivo());
		args.put(EnviromentMapper.CH_TIPO, env.getChTipo());
		args.put(EnviromentMapper.ST_DEFECTO, env.getStDefecto());
		args.put(EnviromentMapper.NB_RUTAGESTDOC, env.getNbRutagestdoc());
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(EnviromentMapper.CD_ENV, env.getCdEnv());
		return super.update(args, condiciones);
	}

	@Override
	public Enviroment obtenerEnvPorDefecto() throws DaoException {
		try{
			SqlCondicion[] condiciones = new SqlCondicion[1];
			condiciones[0] = new SqlCondicion(EnviromentMapper.ST_DEFECTO,Constants.ST_ACTIVO);
			List<Enviroment> envs = this.getEntities(condiciones);
			if(envs!=null && !envs.isEmpty()){
				return envs.get(0);
			}else{
				envs = super.getAllEntities();
				return envs.get(0);
			}
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
	}
	
}
