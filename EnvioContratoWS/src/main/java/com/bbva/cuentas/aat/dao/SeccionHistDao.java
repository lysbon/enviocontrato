package com.bbva.cuentas.aat.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ItemSection;

@Component
public interface SeccionHistDao extends BaseDao<ItemSection, Long>{

	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla,Timestamp fecHoraCreacion) throws DaoException; 
	
}