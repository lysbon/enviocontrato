package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ItemDao;
import com.bbva.cuentas.aat.dao.mapper.ItemMapper;
import com.bbva.cuentas.bean.ItemStamp;

@Component
public class ItemDaoImpl extends BaseDaoImpl<ItemStamp, Long> implements ItemDao{
	
	private static final Logger logger = LoggerFactory.getLogger(ItemDaoImpl.class);
	
	public static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG004_ITEM";
	public static final String TABLE_PK = "CD_ITEM";
	public static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"item_seq";
	
	@Autowired
	ItemMapper itemMapper;
	
	@Autowired
	public ItemDaoImpl(DataSource datasource, ItemMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<ItemStamp> obtenerItemsPorSeccionEstado(Long cdSeccion,String estado) throws DaoException {
		List<ItemStamp> items=null;		
		try{
			SqlCondicion[] condiciones = new SqlCondicion[2];
			condiciones[0] = new SqlCondicion(ItemMapper.CD_SECCION,cdSeccion);
			condiciones[1] = new SqlCondicion(SqlOperador.AND,ItemMapper.ST_ACTIVO,estado);
			items = this.getEntities(condiciones);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}

	@Override
	public List<ItemStamp> obtenerItemsPorSeccion(Long cdSeccion)
			throws DaoException {
		List<ItemStamp> items=null;		
		try{
			SqlCondicion[] condiciones = new SqlCondicion[1];
			condiciones[0] = new SqlCondicion(ItemMapper.CD_SECCION,cdSeccion);
			items = this.getEntities(condiciones);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<ItemStamp> obtenerItemsPorSeccionAscendente(Long cdSeccion)
			throws DaoException {
		List<ItemStamp> items= new ArrayList<ItemStamp>() ;		
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CD_SECCION = ? "+
					"ORDER  BY NU_IDX_CLIENTE";
			Long[] args = new Long[1];
			args[0] = cdSeccion;
			items = getJdbcTemplate().query(sql,args,itemMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}

	@Override
	public Long insertAndReturnId(ItemStamp item) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(ItemMapper.CD_SECCION, item.getCdSeccion());
		args.put(ItemMapper.CH_TIPO, item.getChTipo());
		args.put(ItemMapper.NB_FUENTE, item.getNbFuente());
		args.put(ItemMapper.NB_MASK, item.getNbMask());
		args.put(ItemMapper.NB_NOMBRE_VAR, item.getNbNombreVar());
		args.put(ItemMapper.NB_VALOR_VAR, item.getNbValorVar());
		args.put(ItemMapper.NU_ALINEAR, item.getNuAlinear());
		args.put(ItemMapper.NU_ANCHO, item.getNuAncho());
		args.put(ItemMapper.NU_COOR_X, item.getNuCoorX());
		args.put(ItemMapper.NU_COOR_Y, item.getNuCoorY());
		args.put(ItemMapper.NU_IDX_CLIENTE, item.getNuIdxCliente());
		args.put(ItemMapper.NU_SC_X, item.getNuScX());
		args.put(ItemMapper.NU_SC_Y, item.getNuScY());
		args.put(ItemMapper.ST_ACTIVO, item.getStActivo());
		return super.insertAndReturnId(args);
	}

	@Override
	public int update(ItemStamp item) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(ItemMapper.CD_SECCION, item.getCdSeccion());
		args.put(ItemMapper.CH_TIPO, item.getChTipo());
		args.put(ItemMapper.NB_FUENTE, item.getNbFuente());
		args.put(ItemMapper.NB_MASK, item.getNbMask());
		args.put(ItemMapper.NB_NOMBRE_VAR, item.getNbNombreVar());
		args.put(ItemMapper.NB_VALOR_VAR, item.getNbValorVar());
		args.put(ItemMapper.NU_ALINEAR, item.getNuAlinear());
		args.put(ItemMapper.NU_ANCHO, item.getNuAncho());
		args.put(ItemMapper.NU_COOR_X, item.getNuCoorX());
		args.put(ItemMapper.NU_COOR_Y, item.getNuCoorY());
		args.put(ItemMapper.NU_IDX_CLIENTE, item.getNuIdxCliente());
		args.put(ItemMapper.NU_SC_X, item.getNuScX());
		args.put(ItemMapper.NU_SC_Y, item.getNuScY());
		args.put(ItemMapper.ST_ACTIVO, item.getStActivo());

		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(ItemMapper.CD_ITEM, item.getCdItem());
		return super.update(args, condiciones);
	}
	
}
