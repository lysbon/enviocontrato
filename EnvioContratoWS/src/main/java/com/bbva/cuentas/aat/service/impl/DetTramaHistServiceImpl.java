package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;

import com.bbva.cuentas.aat.dao.DetTramaHistDao;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.DetTramaHistService;


import com.bbva.cuentas.bean.DetTramaHist;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class DetTramaHistServiceImpl implements DetTramaHistService {

	
	
private static final Logger logger = LoggerFactory.getLogger(DetTramaHistServiceImpl.class);
	
	@Autowired
	private DetTramaHistDao detTramaHistDao;

	
	public List<DetTramaHist> obtenerDetTramasHistPorIdTrama(String idTrama) throws ServiceException {
		try {
			return detTramaHistDao.obtenerDetTramasHistPorIdTrama(idTrama);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					ErrorCodes.DESC_ERROR_ITM_OBTENER_POR_SECCION);
		}
	}   
	
	public List<DetTramaHist> obtenerDetTramasPorParCodCli(String chParCodCli) throws ServiceException {
		try {
			return detTramaHistDao.obtenerDetTramasPorParCodCli(chParCodCli);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					ErrorCodes.DESC_ERROR_ITM_OBTENER_POR_SECCION);
		}
	} 
	
	public List<DetTramaHist> obtenerDetTramasPorEstado(String estado) throws ServiceException {
		try {
			return detTramaHistDao.obtenerDetTramasPorEstado(estado);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	} 
	
	
	
	public int insert(DetTramaHist detTramaHist) {
		try {
			return detTramaHistDao.insert(detTramaHist);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return 0;
	}
	
	@Override
	public int updateDetTramaHist(DetTramaHist detTramaHist) throws ServiceException {
		try {
			return detTramaHistDao.updateDetTramaHist(detTramaHist);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");
		}
	}
	
	public DetTramaHist obtenerDetTramasHistPorPK(Long idTrama,String chParCodCli) throws ServiceException {
		try {
			return detTramaHistDao.obtenerDetTramasHistPorPK(idTrama,chParCodCli);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	}   
	



}
