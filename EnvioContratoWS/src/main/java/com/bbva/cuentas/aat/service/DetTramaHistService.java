package com.bbva.cuentas.aat.service;

import java.util.List;


import com.bbva.cuentas.bean.DetTramaHist;

import com.bbva.cuentas.service.exception.ServiceException;

public interface DetTramaHistService {
	
	public List<DetTramaHist> obtenerDetTramasHistPorIdTrama(String idTrama) throws ServiceException;

	public List<DetTramaHist> obtenerDetTramasPorParCodCli(String chParCodCli) throws ServiceException;

	public List<DetTramaHist> obtenerDetTramasPorEstado(String estado) throws ServiceException;

	public int insert(DetTramaHist detTramaHist) throws ServiceException;

	public int updateDetTramaHist(DetTramaHist detTramaHist) throws ServiceException;
	
	public DetTramaHist obtenerDetTramasHistPorPK(Long idTrama,String chParCodCli) throws ServiceException;

}
