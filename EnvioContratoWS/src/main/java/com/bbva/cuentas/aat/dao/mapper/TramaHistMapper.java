package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;


import com.bbva.cuentas.bean.TramaHist;

@Component
public class TramaHistMapper  implements RowMapper<TramaHist>{
	
	
	public static final String ID_TRAMA="ID_TRAMA";
	public static final String CD_ENV="CD_ENV";
	public static final String CH_PROCEDENCIA="CH_PROCEDENCIA";
	public static final String CD_KIT="CD_KIT";
	public static final String CD_CONTRATO="CD_CONTRATO";
	public static final String CH_TIT_CODCLI="CH_TIT_CODCLI";
	public static final String ST_ESTADO="ST_ESTADO";
	public static final String ST_REPRO="ST_REPRO";
	public static final String ST_MASIVO="ST_MASIVO";
	public static final String FH_PRO_INI="FH_PRO_INI";
	public static final String FH_PRO_FIN="FH_PRO_FIN";
	
	

	final LobHandler lobHandler = new DefaultLobHandler();

	@Override
	public TramaHist mapRow(ResultSet rs, int rowNum) throws SQLException {
		TramaHist tramaHist = new TramaHist();

		tramaHist.setIdTrama(rs.getLong(ID_TRAMA));
		tramaHist.setEnv(rs.getInt(CD_ENV));
		tramaHist.setProcedencia(rs.getString(CH_PROCEDENCIA));
		tramaHist.setKit(rs.getLong(CD_KIT));
		tramaHist.setContrato(rs.getString(CD_CONTRATO));
		tramaHist.setTitCodCli(rs.getString(CH_TIT_CODCLI));
		tramaHist.setEstado(rs.getString(ST_ESTADO));
		tramaHist.setRepro(rs.getString(ST_REPRO));
		tramaHist.setMasivo(rs.getString(ST_MASIVO));

		tramaHist.setFhProIni(rs.getTimestamp(FH_PRO_INI));
		tramaHist.setFhProFin(rs.getTimestamp(FH_PRO_FIN));
		
	

		return tramaHist;
	}
	
	

}
