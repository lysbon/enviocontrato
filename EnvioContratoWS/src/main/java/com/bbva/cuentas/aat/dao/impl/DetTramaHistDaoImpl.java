package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.DetTramaHistDao;
import com.bbva.cuentas.aat.dao.mapper.DetTramaHistMapper;


import com.bbva.cuentas.bean.DetTramaHist;

import com.bbva.cuentas.util.Constants;


@Component
public class DetTramaHistDaoImpl extends BaseDaoImpl<DetTramaHist, Long> implements DetTramaHistDao {
	

	private static final Logger logger = LoggerFactory.getLogger(DetTramaHistDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG015_DET_TRAMA_HIST";
	static final String TABLE_PK =  Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	DetTramaHistMapper detTramaHistMapper;
	
	@Autowired
	public DetTramaHistDaoImpl(DataSource datasource, DetTramaHistMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	
	
	
	@Override
	public List<DetTramaHist> obtenerDetTramasHistPorIdTrama(String idtrama)
			throws DaoException {
		List<DetTramaHist> items= new ArrayList<DetTramaHist>() ;		
		try{
			
			String sql = 
					"SELECT  * "+
					"FROM "+TABLE_NAME+ " "+
					"WHERE  ID_TRAMA = ? "+
					"ORDER  BY ID_TRAMA";
			
			String[] args = new String[1];
			args[0] = idtrama;
			items = getJdbcTemplate().query(sql,args,detTramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<DetTramaHist> obtenerDetTramasPorParCodCli(String chParCodCli)
			throws DaoException {
		List<DetTramaHist> items= new ArrayList<DetTramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CH_PAR_CODCLI = ? "+
					"ORDER  BY CH_PAR_CODCLI";
			
			String [] args = new String[1];
			args[0] = chParCodCli;
			items = getJdbcTemplate().query(sql,args,detTramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<DetTramaHist> obtenerDetTramasPorEstado(String estado)
			throws DaoException {
		List<DetTramaHist> items= new ArrayList<DetTramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  ST_ESTADO = ? "+
					"ORDER  BY ST_ESTADO";
			
			String[] args = new String[1];
			args[0] = estado;
			items = getJdbcTemplate().query(sql,args,detTramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
		
	@Override
	public int insert(DetTramaHist detTramaHist) throws DaoException {
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(DetTramaHistMapper.ID_TRAMA, detTramaHist.getIdTrama());
		args.put(DetTramaHistMapper.CH_PAR_CODCLI, detTramaHist.getChParCodCli());
		args.put(DetTramaHistMapper.FH_REPRO_INI, detTramaHist.getFhProIni());
		args.put(DetTramaHistMapper.FH_REPRO_FIN, detTramaHist.getFhProFin());
		args.put(DetTramaHistMapper.NU_NRO_REPRO, detTramaHist.getNroReproceso());
		args.put(DetTramaHistMapper.CD_STEP, detTramaHist.getCdStep());
		args.put(DetTramaHistMapper.CH_DET_STEP, detTramaHist.getDetStep());
		args.put(DetTramaHistMapper.ST_ESTADO, detTramaHist.getEstado());
		
		
		return super.insert(args);
	}
	
	@Override
	public int updateDetTramaHist(DetTramaHist detTramaHist) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(DetTramaHistMapper.ID_TRAMA, detTramaHist.getIdTrama());
		args.put(DetTramaHistMapper.CH_PAR_CODCLI, detTramaHist.getChParCodCli());
		args.put(DetTramaHistMapper.FH_REPRO_INI, detTramaHist.getFhProIni());
		args.put(DetTramaHistMapper.FH_REPRO_FIN, detTramaHist.getFhProFin());
		args.put(DetTramaHistMapper.NU_NRO_REPRO, detTramaHist.getNroReproceso());
		args.put(DetTramaHistMapper.CD_STEP, detTramaHist.getCdStep());
		args.put(DetTramaHistMapper.CH_DET_STEP, detTramaHist.getDetStep());
		args.put(DetTramaHistMapper.ST_ESTADO, detTramaHist.getEstado());
		
				
		SqlCondicion[] condiciones = new SqlCondicion[2];
		condiciones[0] = new SqlCondicion(DetTramaHistMapper.ID_TRAMA , detTramaHist.getIdTrama());
		condiciones[1] = new SqlCondicion(SqlOperador.AND,DetTramaHistMapper.CH_PAR_CODCLI , detTramaHist.getChParCodCli());
		return super.update(args, condiciones);
	}
	
	
	@Override
	public DetTramaHist obtenerDetTramasHistPorPK(Long idTrama,String chParCodCli)
			throws DaoException {
		List<DetTramaHist> items= new ArrayList<DetTramaHist>() ;		
		try{
			
			String sql = 
					" SELECT * FROM "+TABLE_NAME +
					" WHERE  ID_TRAMA = ? "+" AND "+" CH_PAR_CODCLI = ?   " +
					" ORDER  BY ID_TRAMA";
			
			String[] args = new String[2];
			
			args[0] = String.valueOf(idTrama);
			args[1] = chParCodCli;
			items = getJdbcTemplate().query(sql,args,detTramaHistMapper);

			if(items!=null && !items.isEmpty()){
				return items.get(0);
			}
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return null;
	}
	
	
	
	

}
