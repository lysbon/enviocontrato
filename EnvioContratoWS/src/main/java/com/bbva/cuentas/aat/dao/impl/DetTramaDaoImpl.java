package com.bbva.cuentas.aat.dao.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.DetTramaDao;
import com.bbva.cuentas.aat.dao.mapper.DetTramaMapper;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.util.Constants;

@Component
public class DetTramaDaoImpl extends BaseDaoImpl<DetTrama, Long> implements DetTramaDao{
	
	private static final Logger logger = LoggerFactory.getLogger(DetTramaDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG013_DET_TRAMA";
	static final String TABLE_PK = "ID_DET_TRAMA";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"DETTRAMA_SEQ";
	
	@Autowired
	DetTramaMapper detTramaMapper;
	
	@Autowired
	public DetTramaDaoImpl(DataSource datasource, DetTramaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public int insert(DetTrama detTrama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(DetTramaMapper.ID_DET_TRAMA,detTrama.getIdDetTrama());
		args.put(DetTramaMapper.ID_TRAMA, detTrama.getIdTrama());
		args.put(DetTramaMapper.CD_CLIENTE, detTrama.getCdCliente());
		args.put(DetTramaMapper.CD_STEP,  detTrama.getCdStep()  );
		args.put(DetTramaMapper.CD_DOC,  detTrama.getCdDoc()  );
		args.put(DetTramaMapper.DET_STEP,detTrama.getDetStep() );
		args.put(DetTramaMapper.ESTADO, detTrama.getEstado()  );
		args.put(DetTramaMapper.NUM_REPROCESO, detTrama.getNumReproceso()   );
		args.put(DetTramaMapper.TIME_REPROCESO,  detTrama.getTimeReproceso());
		args.put(DetTramaMapper.DET_STEP, detTrama.getDetStep());		
		return super.insert(args);
	}
	
	
	@Override
	public Long insertAndReturnId(DetTrama detTrama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		//"args.put(DetTramaMapper.ID_DET_TRAMA,detTrama.getIdDetTrama());"
		args.put(DetTramaMapper.ID_TRAMA, detTrama.getIdTrama());
		args.put(DetTramaMapper.CD_CLIENTE, detTrama.getCdCliente());
		args.put(DetTramaMapper.CD_STEP,  detTrama.getCdStep()  );
		args.put(DetTramaMapper.CD_DOC,  detTrama.getCdDoc()  );
		args.put(DetTramaMapper.DET_STEP,detTrama.getDetStep() );
		args.put(DetTramaMapper.ESTADO, detTrama.getEstado()  );
		args.put(DetTramaMapper.NUM_REPROCESO, detTrama.getNumReproceso()   );
		args.put(DetTramaMapper.TIME_REPROCESO,  detTrama.getTimeReproceso());
		args.put(DetTramaMapper.DET_STEP, detTrama.getDetStep());		
		return super.insertAndReturnId(args);
	}
	
	@Override
	public int update(DetTrama detTrama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(DetTramaMapper.ID_DET_TRAMA,detTrama.getIdDetTrama());
		args.put(DetTramaMapper.ID_TRAMA, detTrama.getIdTrama());
		args.put(DetTramaMapper.CD_CLIENTE, detTrama.getCdCliente());
		args.put(DetTramaMapper.CD_STEP,  detTrama.getCdStep()  );
		args.put(DetTramaMapper.CD_DOC,  detTrama.getCdDoc()  );
		args.put(DetTramaMapper.DET_STEP,detTrama.getDetStep() );
		args.put(DetTramaMapper.ESTADO, detTrama.getEstado()  );
		args.put(DetTramaMapper.NUM_REPROCESO, detTrama.getNumReproceso()   );
		args.put(DetTramaMapper.TIME_REPROCESO,  detTrama.getTimeReproceso());
		args.put(DetTramaMapper.DET_STEP, detTrama.getDetStep());
		
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(DetTramaMapper.ID_DET_TRAMA , detTrama.getIdDetTrama());
		return super.update(args, condiciones);
	}
	
	@Override
	public int updateCliente(DetTrama detTrama) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(DetTramaMapper.ID_DET_TRAMA,detTrama.getIdDetTrama());
		args.put(DetTramaMapper.ID_TRAMA, detTrama.getIdTrama());
		args.put(DetTramaMapper.CD_CLIENTE, detTrama.getCdCliente());
		args.put(DetTramaMapper.CD_STEP,  detTrama.getCdStep()  );
		args.put(DetTramaMapper.CD_DOC,  detTrama.getCdDoc()  );
		args.put(DetTramaMapper.DET_STEP,detTrama.getDetStep() );
		args.put(DetTramaMapper.ESTADO, detTrama.getEstado()  );
		args.put(DetTramaMapper.NUM_REPROCESO, detTrama.getNumReproceso()   );
		args.put(DetTramaMapper.TIME_REPROCESO,  detTrama.getTimeReproceso());
		args.put(DetTramaMapper.DET_STEP, detTrama.getDetStep());
		SqlCondicion[] condiciones = new SqlCondicion[2];
		condiciones[0] = new SqlCondicion(DetTramaMapper.ID_TRAMA , detTrama.getIdDetTrama());
		condiciones[1] = new SqlCondicion(DetTramaMapper.CD_CLIENTE , detTrama.getCdCliente());
		return super.update(args, condiciones);
	}
	
	@Override
	public List<DetTrama> obtenerDetTramasParaReproceso(String estado)
			throws DaoException {
		List<DetTrama> items= new ArrayList<DetTrama>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  ESTADO = ? "+
					"ORDER  BY TIME_REPROCESO";
			
			String[] args = new String[1];
			args[0] = estado;
			items = getJdbcTemplate().query(sql,args,detTramaMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	
	@Override
	public List<DetTrama> obtenerDetTramasPorTramaReproceso(String estado,Long idTrama)
			throws DaoException {
		List<DetTrama> items= new ArrayList<DetTrama>() ;		
		try{
			
			String sql = 
					" SELECT * FROM "+TABLE_NAME +
					" WHERE  ESTADO = ? "+" AND "+" ID_TRAMA = ?   " +
					" ORDER  BY TIME_REPROCESO";
			
			String[] args = new String[2];
			args[0] = estado;
			args[1] = String.valueOf(idTrama);
			items = getJdbcTemplate().query(sql,args,detTramaMapper);
			//"items = getJdbcTemplate().query(sql,detTramaMapper);"
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<DetTrama> obtenerDetTramasPorTramaReproceso(Long idTrama)
			throws DaoException {
		List<DetTrama> items= new ArrayList<DetTrama>() ;		
		try{
			
			String sql = 
					" SELECT * FROM "+TABLE_NAME +
					" WHERE   ID_TRAMA = ?   " +
					" ORDER  BY TIME_REPROCESO";
			
			String[] args = new String[1];
			args[0] = String.valueOf(idTrama);
			items = getJdbcTemplate().query(sql,args,detTramaMapper);
			//"items = getJdbcTemplate().query(sql,detTramaMapper);"
			
			
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
}
