package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.DetTramaHist;

@Component
public class DetTramaHistMapper implements RowMapper<DetTramaHist>  {
	
	public static final String ID_TRAMA = "ID_TRAMA";
	public static final String CH_PAR_CODCLI = "CH_PAR_CODCLI";
	public static final String FH_REPRO_INI = "FH_REPRO_INI";
	public static final String FH_REPRO_FIN = "FH_REPRO_FIN";
	public static final String NU_NRO_REPRO = "NU_NRO_REPRO";
	public static final String CD_STEP = "CD_STEP";
	public static final String CH_DET_STEP = "CH_DET_STEP";
	public static final String ST_ESTADO = "ST_ESTADO";

	final LobHandler lobHandler = new DefaultLobHandler();
	
	@Override
	public DetTramaHist mapRow(ResultSet rs, int rowNum) throws SQLException {
		DetTramaHist detTramaHist = new DetTramaHist();
		detTramaHist.setIdTrama(rs.getLong(ID_TRAMA));
		detTramaHist.setChParCodCli(rs.getString(CH_PAR_CODCLI));
		
		
		detTramaHist.setFhProIni(rs.getTimestamp(FH_REPRO_INI));
		detTramaHist.setFhProFin(rs.getTimestamp(FH_REPRO_FIN));
		
	
		detTramaHist.setNroReproceso(rs.getInt(NU_NRO_REPRO));
		detTramaHist.setCdStep(rs.getInt(CD_STEP));
		detTramaHist.setDetStep(rs.getString(CH_DET_STEP));
		detTramaHist.setEstado(rs.getString(ST_ESTADO));
		return detTramaHist;

	}

}
