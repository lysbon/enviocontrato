package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.KitLegacy;

@Component
public class KitLegacyMapper implements RowMapper<KitLegacy>{

	public static final String CD_KIT="CD_KIT";
	public static final String NB_NOMBRE_KIT="NB_NOMBRE_KIT";
	public static final String ST_ENV_CORREO="ST_ENV_CORREO";
	public static final String ST_FILEUNICO="ST_FILEUNICO";
	public static final String FL_VAL_HUELLA="FL_VAL_HUELLA";
	public static final String NB_CORREO_ENGINE="NB_CORREO_ENGINE";
	public static final String NB_CORREO_ASUNTO="NB_CORREO_ASUNTO";
	
	public static final String LB_CORREO_PLANTILLA="LB_CORREO_PLANTILLA";
	
	@Override
	public KitLegacy mapRow(ResultSet rs, int rowNum) throws SQLException {
		KitLegacy kit = new KitLegacy();
		kit.setCdKit(rs.getString(CD_KIT));
		kit.setStEnvCorreo(rs.getString(ST_ENV_CORREO));
		kit.setStFileunico(rs.getString(ST_FILEUNICO));
		kit.setFlValHuella(rs.getString(FL_VAL_HUELLA));
		kit.setNbCorreoEngine(rs.getString(NB_CORREO_ENGINE));
		kit.setNbCorreoAsunto(rs.getString(NB_CORREO_ASUNTO));
		kit.setNbNombreKit(rs.getString(NB_NOMBRE_KIT));
		return kit;
	}

}
