package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.DetTramaDao;
import com.bbva.cuentas.aat.dao.TramaDao;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class TramaServiceImpl implements TramaService {
	
private static final Logger logger = LoggerFactory.getLogger(TramaServiceImpl.class);
	
	@Autowired
	private TramaDao tramaDao;
	@Autowired
	private DetTramaDao detTramaDao;
	
	@Override
	public Integer actualizarTrama(Trama trama) throws ServiceException {
		try {
			return tramaDao.update(trama);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");
		}
	}


	public Long insertaTramaSeq(Trama trama) {
		try {
			return tramaDao.insertAndReturnId(trama);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return null;
	}
	
	public int insertaTrama(Trama trama) {
		try {
			return tramaDao.insert(trama);
			
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return 0;
	}
	
	
	 	
    public void updateTrama(Trama trama) {
		try {
			tramaDao.update(trama);
		} catch (Exception e) {
	        logger.debug(AppUtil.getDetailedException(e).toString());
	    }
	}
    
    public void updateTramaporEstado(String estadoActualizar,String estadoSeleccion,String fechaDesde,String fechaHasta,String intentos) {
		try {
			
			if(fechaHasta!=null)
			{	
				tramaDao.updateTramaPorEstadoFechaHasta(estadoActualizar, estadoSeleccion,fechaDesde,fechaHasta, intentos);
			}
			else
			{
				tramaDao.updateTramaPorEstadoFecha(estadoActualizar, estadoSeleccion,fechaDesde, intentos);
			}
			 
			

		} catch (Exception e) {
	        logger.debug(AppUtil.getDetailedException(e).toString());
	    }
	}
	 	
	public void updateDetTrama(DetTrama detTrama) {
	    try {
    		detTramaDao.update(detTrama);
    	} catch (Exception e) {
    		logger.debug(AppUtil.getDetailedException(e).toString());
    	}
    }	
	
	public Long insertaDetTrama(DetTrama detTrama) {
		Long pk = 0l;
		try {
			pk = detTramaDao.insertAndReturnId(detTrama);

		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			return 0l;
		}
		return pk;
	}

	public List<Trama> obtenerTramasReprocesar(String estado) throws ServiceException {
		try {
			return tramaDao.obtenerTramasParaReproceso(estado);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	}   
	
	
	public List<Trama> obtenerTramasReprocesar(String estado, String intentos, String cantidad) throws ServiceException {
		try {
			return tramaDao.obtenerTramasParaReproceso(estado, intentos, cantidad);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	}   
	
	public Integer limpiarTramas() throws ServiceException {
		try {
			return tramaDao.limpiarTramas();
		} catch (DaoException dae) {
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_TRM_LIMPIAR_TRAMA,"No se pudo limpiar tramas.");
		}
	}

	public List<DetTrama> obtenerDetTramasReprocesar(String estado,Long idTrama) throws ServiceException {
		try {
			return detTramaDao.obtenerDetTramasPorTramaReproceso(estado,idTrama);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener tramas de reproceso.");
		}
	}

	@Override
	public Trama obtenerTramasPorId(Long id) throws ServiceException {
		try {
			return tramaDao.findById(id);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener trama por id.");
		}
	}
	
	@Override
	public int contarTramasPorEstado(String estado,String intentos) throws ServiceException {
		try {
			return tramaDao.contarTramasPorEstado(estado, intentos);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener trama por id.");
		}
	}
	
	


}
