package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.SeccionHistDao;
import com.bbva.cuentas.aat.dao.mapper.SeccionMapper;
import com.bbva.cuentas.bean.ItemSection;

@Component
public class SeccionHistDaoImpl extends BaseDaoImpl<ItemSection, Long> implements SeccionHistDao{
	
	private static final Logger logger = LoggerFactory.getLogger(SeccionHistDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG003_SECCION_HIST";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	SeccionMapper seccionMapper;
	
	@Autowired
	public SeccionHistDaoImpl(DataSource datasource, SeccionMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}	
	
	@Override
	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla,Timestamp fecHoraCreacion)	throws DaoException {
		List<ItemSection> sections=new ArrayList<ItemSection>();
		try{
			String sql = 
					"SELECT CD_SECCION,CD_PLANTILLA,NU_PAGINA "+
					"FROM   "+TABLE_NAME+" S "+
					"WHERE  S.CD_PLANTILLA = ? "+
					"AND    CAST(S.FH_INICIO AS TIMESTAMP)<=? "+
					"AND    ?<=CAST(NVL(S.FH_FIN,SYSDATE) AS TIMESTAMP) ";
					Object[] args = new Object[3];
					args[0] = cdPlantilla;
					args[1] = fecHoraCreacion;
					args[2] = fecHoraCreacion;
					sections = getJdbcTemplate().query(sql,args,seccionMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return sections;
	}
	
}
