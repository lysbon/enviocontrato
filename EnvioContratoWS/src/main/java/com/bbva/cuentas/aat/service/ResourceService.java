package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.ResourceItem;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;

public interface ResourceService {
	
	public byte[] obtenerImagen(Long id) throws GenerarContratoException;
	
	public String obtenerImagenBase64(Long id) throws ServiceException;
	
	public List<ResourceItem> obtenerImagenes() throws ServiceException;
	
	public Long guardarRecurso(ResourceItem recurso) throws ServiceException;
	
	public Integer actualizarRecurso(ResourceItem recurso) throws ServiceException;
	
	public Integer eliminarRecurso(Long id) throws ServiceException;
	
}
