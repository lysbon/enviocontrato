package com.bbva.cuentas.aat.service;

import java.util.Date;
import java.util.List;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.service.exception.ServiceException;

public interface ItemService {

	public ItemStamp obtenerItemPorCodigo(Long id) throws ServiceException;
	
	public List<ItemStamp> obtenerItemsPorSeccion(Long id,Date fecHoraCreacion) throws ServiceException;
	
	public List<ItemStamp> obtenerItems() throws ServiceException;
	
	public Long guardarItem(ItemStamp itemStamp) throws ServiceException;
	
	public Integer actualizarItem(ItemStamp itemStamp) throws ServiceException;
	
	public Integer eliminarItem(Long id) throws ServiceException;
	
}
