package com.bbva.cuentas.aat.dao;

import java.util.List;


import com.bbva.cuentas.bean.DetTramaHist;


public interface DetTramaHistDao extends BaseDao<DetTramaHist, Long> {

	public List<DetTramaHist> obtenerDetTramasHistPorIdTrama(String idTrama) throws DaoException;

	public List<DetTramaHist> obtenerDetTramasPorParCodCli(String chParCodCli) throws DaoException;

	public List<DetTramaHist> obtenerDetTramasPorEstado(String estado) throws DaoException;

	
	public int insert(DetTramaHist detTramaHist) throws DaoException;

	public int updateDetTramaHist(DetTramaHist detTramaHist) throws DaoException;
	
	public DetTramaHist obtenerDetTramasHistPorPK(Long idTrama,String chParCodCli)  throws DaoException; 

}
