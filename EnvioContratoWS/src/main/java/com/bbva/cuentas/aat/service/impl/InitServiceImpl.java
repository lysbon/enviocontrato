package com.bbva.cuentas.aat.service.impl;

import java.io.File;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.InitService;
import com.bbva.cuentas.aat.service.KitLegacyService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitLegacy;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.bean.KitPlantillaLegacy;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.util.Constants;

@Service
public class InitServiceImpl implements InitService{

	private static final Logger logger = LoggerFactory.getLogger(InitServiceImpl.class);
	
	@Autowired
	private PlantillaService plantillaService;
	@Autowired
	private KitService kitService;
	@Autowired
	private KitLegacyService kitLegacyService;
	@Autowired
	private EnviromentService envService;
	
	@Value("${service.init.ruta.recursos}")
	private String rutaFormatos;
	@Value("${service.initEnv.envDefault}")
	private Integer cdEnvDefault;
	@Value("${spring.profiles.active}")
	private String profile;
	

	@PostConstruct
	public void onCreate(){
		if(StringUtils.equals(profile,"devh2")) {
			try {
				init();
			} catch (ServiceException e) {
				logger.error(e.getMessage());
			}
		}
	}
	
	@Override
	public void init() throws ServiceException{
		try {
			List<Plantilla> plantillas = plantillaService.obtenerPlantillas();			
			cargarPlantillas(plantillas);
		} catch (ServiceException e) {
			logger.error(e.getMessage(),e);
		}
		cargarPlantillasHtml();
	}
	
	public void cargarPlantillas(List<Plantilla> plantillas) throws ServiceException {
		for(Plantilla plantilla : plantillas){
			if(Constants.ST_ACTIVO.equals(plantilla.getStActivo())){
				byte[] pdf = plantillaService.obtenerPDF(plantilla.getCdPlantilla(),null);
				if(pdf!=null && pdf.length>0){
					return;
				}
			}
		}
		for(Plantilla plantilla : plantillas){
			if(Constants.ST_ACTIVO.equals(plantilla.getStActivo()) &&
			   !Constants.TIPO_CONSTR_NUEVO.equals(plantilla.getStTipoConstr())){
				cargaPlantillaPDF(plantilla);
			}
		}
	}
	public void cargaPlantillaPDF(Plantilla plantilla) {
		String rutaPDF = rutaFormatos+File.separatorChar+"pdf";
		String filename = null;
		if(Constants.TIPO_FILEUNICO_CONTRATO.equals(plantilla.getNbTipoFileunico())){
			filename = rutaPDF+File.separatorChar+"contratos"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}else if(Constants.TIPO_FILEUNICO_SOLICITUD.equals(plantilla.getNbTipoFileunico())){
			filename = rutaPDF+File.separatorChar+"solicitud"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}else if(Constants.TIPO_FILEUNICO_FATCA.equals(plantilla.getNbTipoFileunico())){
			filename = rutaPDF+File.separatorChar+"fatca"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}else if(Constants.TIPO_FILEUNICO_PEP.equals(plantilla.getNbTipoFileunico())){
			filename = rutaPDF+File.separatorChar+"pep"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}else if(Constants.TIPO_FILEUNICO_DAE.equals(plantilla.getNbTipoFileunico())){
			filename = rutaPDF+File.separatorChar+"dae"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}else{
			filename = rutaPDF+File.separatorChar+"otros"+File.separatorChar+plantilla.getNbNombreFormato()+".pdf";
		}
		try {
			File f = new File(filename);
			if(f.exists() && !f.isDirectory()) { 
				plantillaService.actualizarPDFdesdeArchivo(plantilla.getCdPlantilla(), filename);
			}else{
				logger.error("No se encontro el archivo PDF solicitado.");
				logger.error(filename);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	public void cargarPlantillasHtml() throws ServiceException{
		String rutaEmail = rutaFormatos+File.separatorChar+"strtpl";
		try {
			List<Kit> kits = kitService.obtenerKits();
			for(Kit kit : kits){
				String basename = kit.getNbNombreKit().substring(4,9);				
				String filename = rutaEmail+File.separatorChar+basename+".html";
				cargaPlantillaHtml(kit,filename);
			}
		} catch (ServiceException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_INIT,"No se pudo procesar carga de recursos.");
		}
	}
	public void cargaPlantillaHtml(Kit kit,String filename) {
		try {
			File f = new File(filename);
			if(f.exists() && !f.isDirectory()) {
				kitService.actualizarEmailTemplate(kit.getCdKit(), filename);
			}else{
				logger.error("No se encontro el archivo HTML solicitado.");
				logger.error(filename);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	@Override
	public void initEnv() throws ServiceException{
		try {
			List<Kit> kitsn = kitService.obtenerKits();
			if(kitsn!=null && !kitsn.isEmpty()){
				return;
			}
			List<KitLegacy> kits = kitLegacyService.obtenerKits();			
			for(KitLegacy kitlegacy : kits){
				Kit kit = new Kit(kitlegacy);
				String templateKit = kitLegacyService.obtenerMailTemplate(kit.getCdCodigo());
				kit.getGrupoKit().setLbCorreoPlantilla(templateKit);
				Long cdKit = kitService.guardarKit(kit);
				EnvKit envKit = new EnvKit();
				envKit.setCdKit(cdKit);
				envKit.setCdEnv(cdEnvDefault);
				envService.guardarEnvKit(envKit);
			}
			List<KitPlantillaLegacy> kitPlantillas = kitLegacyService.obtenerKitPlantillas();
			for(KitPlantillaLegacy kitPlantilla : kitPlantillas){
				Kit kit = kitService.obtenerKitPorCodigo(cdEnvDefault,kitPlantilla.getCdKit());
				KitPlantilla kp = new KitPlantilla();
				kp.setCdKit(kit.getCdKit());
				kp.setCdPlantilla(kitPlantilla.getCdPlantilla());
				kitService.guardarKitPlantilla(kp);
			}
		} catch (ServiceException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_INIT_ENV,"No se pudo procesar carga de enviroments.");
		}
	}
	
}
