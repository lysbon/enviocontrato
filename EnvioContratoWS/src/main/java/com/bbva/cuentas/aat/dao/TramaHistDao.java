package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;

public interface TramaHistDao extends BaseDao<TramaHist, Long> {
	
	public TramaHist obtenerTramaHistPoridTrama(
			Long idTrama) throws DaoException;
	
	public List<TramaHist> obtenerTramaHistPorEnv(
			Integer env) throws DaoException;
	
	public List<TramaHist> obtenerTramaHistPorKit(
			Long kit) throws DaoException;
	
	public List<TramaHist> obtenerTramaHistPorContrato(
			String contrato) throws DaoException;
	
	public List<TramaHist> obtenerTramaHistPorTitCodCli(
			String titCodCli) throws DaoException;
	
	public Long insertAndReturnId(TramaHist tramaHist) throws DaoException;
	
	public int updateTramaHist(TramaHist tramaHist) throws DaoException;
	


}
