package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.TramaHistDao;
import com.bbva.cuentas.aat.dao.mapper.PlantillaMapper;
import com.bbva.cuentas.aat.dao.mapper.TramaHistMapper;
import com.bbva.cuentas.aat.dao.mapper.TramaMapper;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.util.Constants;

@Component
public class TramaHistDaoImpl extends BaseDaoImpl<TramaHist, Long> implements TramaHistDao{
	
	//@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(TramaHistDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG014_TRAMA_HIST";
	
	//static final String TABLE_PK = Constants.VACIO;
	//static final String TABLE_SEQ = Constants.VACIO;
	static final String TABLE_PK = "ID_TRAMA";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"TRAZA_SEQ";
	
	@Autowired
	TramaHistMapper tramaHistMapper;
	
	@Autowired
	public TramaHistDaoImpl(DataSource datasource, TramaHistMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
	
	
	@Override
	public TramaHist obtenerTramaHistPoridTrama(Long id_trama)
			throws DaoException {
		List<TramaHist> items= new ArrayList<TramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  ID_TRAMA = ? "+
					"ORDER  BY ID_TRAMA";
			
			Long[] args = new Long[1];
			args[0] = id_trama;
			items = getJdbcTemplate().query(sql,args,tramaHistMapper);
			
			if(items!=null && !items.isEmpty()){
				return items.get(0);
			}
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return null;
	}
	
	@Override
	public List<TramaHist> obtenerTramaHistPorEnv(Integer env)
			throws DaoException {
		List<TramaHist> items= new ArrayList<TramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CD_ENV = ? "+
					"ORDER  BY CD_ENV";
			
			Integer[] args = new Integer[1];
			args[0] = env;
			items = getJdbcTemplate().query(sql,args,tramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<TramaHist> obtenerTramaHistPorKit(Long kit)
			throws DaoException {
		List<TramaHist> items= new ArrayList<TramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CD_KIT = ? "+
					"ORDER  BY CD_KIT";
			
			Long[] args = new Long[1];
			args[0] = kit;
			items = getJdbcTemplate().query(sql,args,tramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public List<TramaHist> obtenerTramaHistPorContrato(String contrato)
			throws DaoException {
		List<TramaHist> items= new ArrayList<TramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CD_CONTRATO = ? "+
					"ORDER  BY CD_CONTRATO";
			
			String[] args = new String[1];
			args[0] = contrato;
			items = getJdbcTemplate().query(sql,args,tramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	
	@Override
	public List<TramaHist> obtenerTramaHistPorTitCodCli(String titCodCli)
			throws DaoException {
		List<TramaHist> items= new ArrayList<TramaHist>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  CH_TIT_CODCLI = ? "+
					"ORDER  BY CD_CONTRATO";
			
			String[] args = new String[1];
			args[0] = titCodCli;
			items = getJdbcTemplate().query(sql,args,tramaHistMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public Long insertAndReturnId(TramaHist tramaHist) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		//args.put(TramaHistMapper.ID_TRAMA, tramaHist.getIdTrama());
		args.put(TramaHistMapper.CD_ENV, tramaHist.getEnv());
		args.put(TramaHistMapper.CH_PROCEDENCIA, tramaHist.getProcedencia());
		args.put(TramaHistMapper.CD_KIT, tramaHist.getKit());
		args.put(TramaHistMapper.CD_CONTRATO, tramaHist.getContrato());
		args.put(TramaHistMapper.CH_TIT_CODCLI, tramaHist.getTitCodCli());
		args.put(TramaHistMapper.ST_ESTADO, tramaHist.getEstado());
		args.put(TramaHistMapper.ST_REPRO, tramaHist.getRepro());
		args.put(TramaHistMapper.ST_MASIVO, tramaHist.getMasivo());
		args.put(TramaHistMapper.FH_PRO_INI, new Date());
		args.put(TramaHistMapper.FH_PRO_FIN, tramaHist.getFhProFin());
		//return super.insertAndReturnId(args);
		return	super.insertAndReturnId(args);
	}
	
	@Override
	public int updateTramaHist(TramaHist tramaHist) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(TramaHistMapper.ID_TRAMA, tramaHist.getIdTrama());
		args.put(TramaHistMapper.CD_ENV, tramaHist.getEnv());
		args.put(TramaHistMapper.CH_PROCEDENCIA, tramaHist.getProcedencia());
		args.put(TramaHistMapper.CD_KIT, tramaHist.getKit());
		args.put(TramaHistMapper.CD_CONTRATO, tramaHist.getContrato());
		args.put(TramaHistMapper.CH_TIT_CODCLI, tramaHist.getTitCodCli());
		args.put(TramaHistMapper.ST_ESTADO, tramaHist.getEstado());
		args.put(TramaHistMapper.ST_REPRO, tramaHist.getRepro());
		args.put(TramaHistMapper.ST_MASIVO, tramaHist.getMasivo());
		args.put(TramaHistMapper.FH_PRO_INI, tramaHist.getFhProIni());
		args.put(TramaHistMapper.FH_PRO_FIN, tramaHist.getFhProFin());
		
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(TramaHistMapper.ID_TRAMA, tramaHist.getIdTrama());
		return super.update(args, condiciones);
	}
	

}
