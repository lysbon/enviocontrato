package com.bbva.cuentas.aat.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.EnvKitDao;
import com.bbva.cuentas.aat.dao.mapper.EnvKitMapper;
import com.bbva.cuentas.bean.EnvKit;

@Component
public class EnvKitDaoImpl extends BaseDaoImpl<EnvKit, Long> implements EnvKitDao{
	
	private static final Logger logger = LoggerFactory.getLogger(EnvKitDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG012_ENV_KIT";
	static final String TABLE_NAME_HIST = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG012_ENV_KIT_HIST";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	public EnvKitDaoImpl(DataSource datasource, EnvKitMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public Integer delete(EnvKit envKit) throws DaoException {
		Integer result = null;
		try {
			String sql = "DELETE FROM " + TABLE_NAME + 
					     " WHERE  "+KitDaoImpl.TABLE_PK+"  = ? "+
			             "   AND "+EnviromentDaoImpl.TABLE_PK + " = ?";
			Object[] args = new Object[2];
			args[0] = envKit.getCdKit();
			args[1] = envKit.getCdEnv();
			result = getJdbcTemplate().update(sql, args);			
		} catch (DataAccessException dae) {
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return result;
	}
	
	@Override
	public Integer deletePorKit(Long cdKit) throws DaoException {
		Integer result = null;
		try {
			String sql = "DELETE FROM " + TABLE_NAME + 
					     " WHERE "+KitDaoImpl.TABLE_PK+"  = ?  ";
			Object[] args = new Object[1];
			args[0] = cdKit;
			result = getJdbcTemplate().update(sql, args);			
		} catch (DataAccessException dae) {
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return result;
	}
	
	@Override
	public int insert(EnvKit envKit) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnvKitMapper.CD_KIT, envKit.getCdKit());
		args.put(EnvKitMapper.CD_ENV, envKit.getCdEnv());
		args.put(EnvKitMapper.NB_REMITENTE, envKit.getNbRemitente());
		args.put(EnvKitMapper.NB_REMITENTE_CC, envKit.getNbRemitenteCC());
		args.put(EnvKitMapper.NB_CORREO_ASUNTO, envKit.getNbCorreoAsunto());
		args.put(EnvKitMapper.ST_ENV_CORREO, envKit.getStEnvCorreo());
		args.put(EnvKitMapper.NB_CORREO_ENGINE, envKit.getNbCorreoEngine());
		return super.insert(args);
	}

	@Override
	public int update(EnvKit envKit) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(EnvKitMapper.NB_REMITENTE, envKit.getNbRemitente());
		args.put(EnvKitMapper.NB_REMITENTE_CC, envKit.getNbRemitenteCC());
		args.put(EnvKitMapper.ST_ENV_CORREO, envKit.getStEnvCorreo());
		args.put(EnvKitMapper.NB_CORREO_ENGINE, envKit.getNbCorreoEngine());
		args.put(EnvKitMapper.NB_CORREO_ASUNTO, envKit.getNbCorreoAsunto());
		SqlCondicion[] condiciones = new SqlCondicion[2];
		condiciones[0] = new SqlCondicion(EnvKitMapper.CD_KIT, envKit.getCdKit());
		condiciones[1] = new SqlCondicion(SqlOperador.AND,EnvKitMapper.CD_ENV, envKit.getCdEnv());
		return super.update(args, condiciones);
	}

	public String obtenerMailTemplate(Long cdKit, final Integer cdEnv) throws DaoException{
		final LobHandler lobHandler = new DefaultLobHandler();
		List<String> lista = getJdbcTemplate().query(
				"SELECT  "+ EnvKitMapper.LB_CORREO_PLANTILLA
				+" FROM  "+ TABLE_NAME
				+" WHERE "+ KitDaoImpl.TABLE_PK+"  = ?  "
				+"   AND "+ EnviromentDaoImpl.TABLE_PK+" = ?", new Object[]{cdKit,cdEnv},
				new RowMapper<String>(){
					@Override
					public String mapRow(ResultSet rs, int rownumber) throws SQLException {
						return lobHandler.getClobAsString(rs, EnvKitMapper.LB_CORREO_PLANTILLA);
					}
				}
        );
		if(lista!=null && !lista.isEmpty())
			return lista.get(0);
		return null;
	}

	public Integer actualizarPlantillaDesdeCadena(final Long cdKit, final Integer cdEnv,final String template) 
            throws DaoException {
		String sqlText = null;
		int result;
		try {        	
			sqlText = "UPDATE "+TABLE_NAME+" SET "+EnvKitMapper.LB_CORREO_PLANTILLA+" =  ?"+
			          "WHERE  "+KitDaoImpl.TABLE_PK+" = ?"+
			          "  AND  "+EnviromentDaoImpl.TABLE_PK + " = ?";
        	InputStream is = new ByteArrayInputStream(template.getBytes("UTF-8"));
        	final LobHandler lobHandler = new DefaultLobHandler();
        	final InputStreamReader clobReader = new InputStreamReader(is);
        	result = getJdbcTemplate().execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	    	lobCreator.setClobAsCharacterStream(ps, 1, clobReader, (int)template.getBytes().length);
					ps.setLong(2, cdKit);
					ps.setInt(3, cdEnv);
        	      }
        	  }
        	);
			clobReader.close();
			return result;
        } catch (IOException e) {
        	logger.error("Caught I/O Exception: (Write CLOB value - Put Method).",e);
            throw new DaoException(ErrorCodes.D_UPDATE_ERROR,"Error al actualizar plantilla de correo");
        }
    }
	
	public Integer actualizarPlantillaDesdeArchivo(final Long cdKit, final Integer cdEnv, String filename) 
            throws DaoException {
		String sqlText = null;
		int result;
		InputStream clobIs=null;
		InputStreamReader clobReader=null;
		try {        	
        	sqlText = "UPDATE "+TABLE_NAME+" SET "+EnvKitMapper.LB_CORREO_PLANTILLA+" = ? "+
			          "WHERE  "+KitDaoImpl.TABLE_PK+" = ? "+
			          "  AND  "+EnviromentDaoImpl.TABLE_PK + " = ?";
        	final File clobIn = new File(filename);
        	clobIs = new FileInputStream(clobIn);
        	final LobHandler lobHandler = new DefaultLobHandler();
        	clobReader = new InputStreamReader(clobIs);
        	final InputStreamReader tmpClobReader=clobReader;
        	
        	result = getJdbcTemplate().execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	        lobCreator.setClobAsCharacterStream(ps, 1, tmpClobReader, (int)clobIn.length());
					ps.setLong(2, cdKit);
					ps.setInt(3, cdEnv);
        	      }
        	  }
        	);
        	clobReader.close();
			return result;
        } catch (IOException e) {
        	logger.error("Caught I/O Exception: (Write CLOB value - Put Method).",e);
            throw new DaoException(ErrorCodes.D_UPDATE_ERROR,"Error al actualizar plantilla de correo");
        }finally {
        	try {if (clobIs!=null) clobIs.close();} catch (IOException e)  {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        	try {if (clobReader!=null) clobReader.close();} catch (IOException e)  {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        }
    }
	
}
