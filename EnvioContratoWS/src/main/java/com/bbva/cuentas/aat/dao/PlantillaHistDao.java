package com.bbva.cuentas.aat.dao;

import java.util.List;
import java.sql.Timestamp;

import com.bbva.cuentas.bean.Plantilla;

public interface PlantillaHistDao extends BaseDao<Plantilla, Long>{
	
	public List<Plantilla> obtenerPlantillasPorKitEstado(Long cdKit, String stActivo,Timestamp fecHoraCreacion) throws DaoException;
	
	public byte[] obtenerPDFTemplate(Long cdPlantilla,Timestamp fecHoraCreacion) throws DaoException;
}
