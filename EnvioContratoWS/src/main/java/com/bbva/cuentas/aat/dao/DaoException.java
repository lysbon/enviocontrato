package com.bbva.cuentas.aat.dao;

public class DaoException extends Exception {
   
	private static final long serialVersionUID = 1L;
		
    private final String errorCode;    
    private final String message;
    
    public DaoException(String errorCode) {
        this(errorCode, null);
    }
    
    public DaoException(String errorCode,String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

    @Override
	public String getMessage() {
    		//Retorna mensaje customizado
    		return message;
	}
	
}