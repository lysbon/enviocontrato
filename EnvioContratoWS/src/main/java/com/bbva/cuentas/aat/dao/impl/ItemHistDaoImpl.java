package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ItemHistDao;
import com.bbva.cuentas.aat.dao.mapper.ItemMapper;
import com.bbva.cuentas.bean.ItemStamp;

@Component
public class ItemHistDaoImpl extends BaseDaoImpl<ItemStamp, Long> implements ItemHistDao{
	
	private static final Logger logger = LoggerFactory.getLogger(ItemHistDaoImpl.class);
	
	public static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG004_ITEM_HIST";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	ItemMapper itemMapper;
	
	@Autowired
	public ItemHistDaoImpl(DataSource datasource, ItemMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<ItemStamp> obtenerItemsPorSeccionAscendente(Long cdSeccion,Timestamp fecHoraCreacion)
			throws DaoException {
		List<ItemStamp> items= new ArrayList<ItemStamp>() ;		
		try{
			
			String sql = 
					"SELECT I.* "+
					"FROM   "+TABLE_NAME+ " I "+
					"WHERE  CD_SECCION = ? "+
					"AND    CAST(I.FH_INICIO AS TIMESTAMP)<=? "+
					"AND    ?<=CAST(NVL(I.FH_FIN,SYSDATE) AS TIMESTAMP) "+
					"ORDER  BY NU_IDX_CLIENTE";
			
			Object[] args = new Object[3];
			args[0] = cdSeccion;
			args[1] = fecHoraCreacion;
			args[2] = fecHoraCreacion;
			items = getJdbcTemplate().query(sql,args,itemMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
}
