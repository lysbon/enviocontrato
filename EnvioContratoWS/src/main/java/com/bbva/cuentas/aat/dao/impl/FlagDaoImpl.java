package com.bbva.cuentas.aat.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.FlagDao;
import com.bbva.cuentas.aat.dao.mapper.FlagMapper;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.util.Constants;

@Component
public class FlagDaoImpl extends BaseDaoImpl<Flag, Long> implements FlagDao{
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(FlagDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG005_FLAG";
	static final String TABLE_PK = "CD_FLAG";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"flg_seq";
	
	@Autowired
	public FlagDaoImpl(DataSource datasource, FlagMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<Flag> obtenerFlagsPorTipo(String tipo) throws DaoException {
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(FlagMapper.CH_TIPO, tipo);
		return super.getEntities(condiciones);
	}
	
	@Override
	public List<Flag> obtenerFlags() throws DaoException {
		return super.getAllEntities();
	}
	
	@Override
	public Long insertAndReturnId(Flag flag) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(FlagMapper.CH_TIPO, flag.getChTipo());
		args.put(FlagMapper.NB_CONDICION_NOMBRE, flag.getNbCondicionNombre());
		args.put(FlagMapper.NB_CONDICION_VALOR, flag.getNbCondicionValor());
		args.put(FlagMapper.NU_ORDEN, flag.getNuOrden());
		return super.insertAndReturnId(args);
	}
	
	@Override
	public int update(Flag flag) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(FlagMapper.CH_TIPO, flag.getChTipo());
		args.put(FlagMapper.NB_CONDICION_NOMBRE, flag.getNbCondicionNombre());
		args.put(FlagMapper.NB_CONDICION_VALOR, flag.getNbCondicionValor());
		args.put(FlagMapper.NU_ORDEN, flag.getNuOrden());
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(FlagMapper.CD_FLAG, flag.getCdFlag());
		return super.update(args, condiciones);
	}
	
}