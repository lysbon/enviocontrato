package com.bbva.cuentas.aat.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ImgDao;
import com.bbva.cuentas.aat.dao.mapper.ResourceItemMapper;
import com.bbva.cuentas.bean.ResourceItem;
import com.bbva.cuentas.util.Constants;

@Component
public class ImgDaoImpl extends BaseDaoImpl<ResourceItem, Long> implements ImgDao{
	
	@SuppressWarnings("unused")
	private static final Logger logger = LoggerFactory.getLogger(ImgDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG007_ITEM_IMG";
	static final String TABLE_PK = "CD_ITEM";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"res_seq";
	
	@Autowired
	public ImgDaoImpl(DataSource datasource, ResourceItemMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	
	public byte[] obtenerImagen(Long cdItem) throws DaoException{
		final LobHandler lobHandler = new DefaultLobHandler();
		List<byte[]> lista = getJdbcTemplate().query(
				"SELECT  " + ResourceItemMapper.LB_IMG +
				"  FROM  "+TABLE_NAME +
				" WHERE  "+ResourceItemMapper.CD_ITEM+"+ = ?", new Object[] { cdItem },
				new RowMapper<byte[]>(){
					@Override
					public byte[] mapRow(ResultSet rs, int rownumber) throws SQLException {
						return lobHandler.getBlobAsBytes(rs, ResourceItemMapper.LB_IMG);
					}
				}
        );
		if(lista!=null && !lista.isEmpty())
			return lista.get(0);
		return new byte[0];
	}
	
	@Override
	public List<ResourceItem> obtenerImagenes() throws DaoException {
		return super.getAllEntities();
	}
	
}
