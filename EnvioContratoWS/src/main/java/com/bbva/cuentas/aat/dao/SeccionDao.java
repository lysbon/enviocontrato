package com.bbva.cuentas.aat.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ItemSection;

@Component
public interface SeccionDao extends BaseDao<ItemSection, Long>{

	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla) throws DaoException; 
	
}