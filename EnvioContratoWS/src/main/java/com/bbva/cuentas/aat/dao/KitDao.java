package com.bbva.cuentas.aat.dao;

import java.util.List;
import com.bbva.cuentas.bean.Kit;

public interface KitDao extends BaseDao<Kit, Long>{
	
	public List<Kit> obtenerKitPorEnv(Integer cdEnv) throws DaoException;

	public Kit obtenerKitPorCodigo(Integer cdEnv,String codigo) throws DaoException;
	
	public Kit obtenerKitPorIdEnv(Integer cdEnv,Long cdKit) throws DaoException;
	
	public Kit obtenerKitPorId(Long cdKit) throws DaoException;

	public List<Kit> obtenerKitsPorCodigo(String codigo) throws DaoException;
	
}
