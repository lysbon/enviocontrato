package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ParametroDao;
import com.bbva.cuentas.aat.dao.mapper.ParametroMapper;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.util.Constants;

@Component
public class ParametroDaoImpl extends BaseDaoImpl<Parametro, Long> implements ParametroDao{
	
	
	private static final Logger logger = LoggerFactory.getLogger(ParametroDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG016_PARAMETRO";
	static final String TABLE_PK = "ID_PARAM";
	static final String TABLE_SEQ = Constants.VACIO;
	
	
	@Autowired
	ParametroMapper parametroMapper;
	
	@Autowired
	public ParametroDaoImpl(DataSource datasource, ParametroMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<Parametro> obtenerParametroParaReproceso(String Id_Param)
			throws DaoException {
		List<Parametro> items= new ArrayList<Parametro>() ;		
		try{
			
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+ " "+
					"WHERE  ID_PARAM = ? ";
			
			String[] args = new String[1];
			args[0] = Id_Param;
			items = getJdbcTemplate().query(sql,args,parametroMapper);
			
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return items;
	}
	
	@Override
	public Parametro obtenerValorById(String idParam)
	      	throws DaoException {
			List<Parametro> listaItems=null ;
			try {
				
				String sql = 
							"SELECT * "+
							"FROM "+TABLE_NAME+ " "+
							"WHERE ID_PARAM = ? ";				
				String[] args = new String[1];
				args[0] = idParam;
				listaItems = getJdbcTemplate().query(sql,args,parametroMapper);
				if(listaItems!=null && !listaItems.isEmpty()){
					return listaItems.get(0);
				} 
				return null;
				
			}catch (DataAccessException dae){
				logger.debug(AppUtil.getDetailedException(dae).toString());
				throw new DaoException(ErrorCodes.D_QUERY_ERROR);
			
			}
	}
	
	
	
	@Override
	public Parametro obtenerValorByName(String name)
	      	throws DaoException {
			List<Parametro> listaItems=null ;
			try {
				
				String sql = 
							"SELECT * "+
							"FROM "+TABLE_NAME+ " "+
							"WHERE CH_NOMBRE_PARAM LIKE '?' ";				
				String[] args = new String[1];
				args[0] = name;
				listaItems = getJdbcTemplate().query(sql,args,parametroMapper);
				if(listaItems!=null && !listaItems.isEmpty()){
					return listaItems.get(0);
				} 
				return null;
				
			}catch (DataAccessException dae){
				logger.debug(AppUtil.getDetailedException(dae).toString());
				throw new DaoException(ErrorCodes.D_QUERY_ERROR);
			
			}
	}
}

