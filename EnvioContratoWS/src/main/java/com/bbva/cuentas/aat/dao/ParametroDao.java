package com.bbva.cuentas.aat.dao;

import java.util.List;
import com.bbva.cuentas.bean.Parametro;



public interface ParametroDao extends BaseDao<Parametro, Long> {
	
	public List<Parametro> obtenerParametroParaReproceso(String idParam) throws DaoException;
	public Parametro obtenerValorById(String idParam) throws DaoException;
	public Parametro obtenerValorByName(String name) throws DaoException ;
}
