package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.service.exception.ServiceException;

public interface ParametroService {
	
	List<Parametro> obtenerParametroReprocesar(String ID_Param) throws ServiceException;
	public Parametro obtenerValorById(String idParam) throws ServiceException;
	public Parametro obtenerValorByName(String name) throws ServiceException;

}
