package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ItemStamp;

@Component
public class ItemMapper implements RowMapper<ItemStamp>{

	
	public static final String CD_ITEM="CD_ITEM";
	public static final String CD_SECCION="CD_SECCION";
	public static final String ST_ACTIVO="ST_ACTIVO";
	public static final String NU_COOR_X="NU_COOR_X";
	public static final String NU_COOR_Y="NU_COOR_Y";
	public static final String NB_NOMBRE_VAR="NB_NOMBRE_VAR";
	public static final String NB_VALOR_VAR="NB_VALOR_VAR";
	public static final String NB_FUENTE="NB_FUENTE";
	public static final String NU_ALINEAR="NU_ALINEAR";
	public static final String CH_TIPO="CH_TIPO";
	public static final String NU_ANCHO="NU_ANCHO";
	public static final String NU_SC_X="NU_SC_X";
	public static final String NU_SC_Y="NU_SC_Y";
	public static final String NB_MASK="NB_MASK";
	public static final String NU_IDX_CLIENTE="NU_IDX_CLIENTE";
	public static final String CD_ENV="CD_ENV";
	
	@Override
	public ItemStamp mapRow(ResultSet rs, int rowNum) throws SQLException {
		ItemStamp item = new ItemStamp();
		item.setCdItem(rs.getLong(CD_ITEM));
		item.setCdSeccion(rs.getLong(CD_SECCION));
		item.setStActivo(rs.getString(ST_ACTIVO));
		item.setNuCoorX(rs.getInt(NU_COOR_X));
		item.setNuCoorY(rs.getInt(NU_COOR_Y));
		item.setNbNombreVar(rs.getString(NB_NOMBRE_VAR));
		item.setNbValorVar(rs.getString(NB_VALOR_VAR));
		item.setNbFuente(rs.getString(NB_FUENTE));
		item.setNuAlinear(rs.getInt(NU_ALINEAR));
		item.setChTipo(rs.getString(CH_TIPO));
		item.setNuAncho(rs.getInt(NU_ANCHO));
		if(rs.wasNull()){
			item.setNuAncho(null);
		}
		item.setNuScX(rs.getInt(NU_SC_X));
		if(rs.wasNull()){
			item.setNuScX(null);
		}
		item.setNuScY(rs.getInt(NU_SC_Y));
		if(rs.wasNull()){
			item.setNuScY(null);
		}
		item.setNbMask(rs.getString(NB_MASK));
		item.setNuIdxCliente(rs.getInt(NU_IDX_CLIENTE));
		if(rs.wasNull()){
			item.setNuIdxCliente(null);
		}
		return item;
	}

}
