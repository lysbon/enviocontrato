package com.bbva.cuentas.aat.service.impl;

import java.util.List;
import java.sql.Timestamp;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.EnvKitDao;
import com.bbva.cuentas.aat.dao.KitDao;
import com.bbva.cuentas.aat.dao.KitHistDao;
import com.bbva.cuentas.aat.dao.KitPlantillaDao;
import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.bean.Plantilla;

@Service
public class KitServiceImpl implements KitService{

	private static final Logger logger = LoggerFactory.getLogger(KitServiceImpl.class);
	
	@Autowired
	private KitDao kitDao;
	@Autowired
	private KitHistDao kitHistDao;
	@Autowired
	private EnvKitDao envKitDao;
	@Autowired
	private KitPlantillaDao kitPlantillaDao;
	@Autowired
	private EnviromentService envService;

	public List<Kit> obtenerKits() throws ServiceException{
		try {
			return kitDao.getAllEntities();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KITS,"No se pudo obtener kits.");
		}
	}
	
	public List<Kit> obtenerKits(Integer cdEnv) throws ServiceException{
		try {
			return kitDao.obtenerKitPorEnv(cdEnv);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KITS,"No se pudo obtener kits.");
		}
	}

	public String obtenerMailTemplate(Long cdKit, Integer cdEnv) throws ServiceException{
		try {
			return envKitDao.obtenerMailTemplate(cdKit,cdEnv);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_MAIL_TEMPLATE,"No se pudo obtener plantilla de correo.");
		}
	}

	@Override
	public Integer actualizarEmailTemplate(Long cdKit, String filename) throws ServiceException {
		try {
			List<Enviroment> envs = envService.obtenerEnvs();
			for(Enviroment env : envs){
				envKitDao.actualizarPlantillaDesdeArchivo(cdKit,env.getCdEnv(),filename);
			}
			return 1;
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_ACTUALIZAR_KIT,"No se pudo actualizar plantilla de correo.");
		}
	}
	
	@Override
	public Integer actualizarEmailTemplate(Long cdKit, Integer cdEnv, String filename) throws ServiceException {
		try {
			return envKitDao.actualizarPlantillaDesdeArchivo(cdKit,cdEnv,filename);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_ACTUALIZAR_KIT,"No se pudo actualizar plantilla de correo.");
		}
	}

	@Override
	public List<Kit> obtenerKitsPorCodigo(String codigo) throws ServiceException{
		try {
			return kitDao.obtenerKitsPorCodigo(codigo);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KITS,"No se pudo obtener kits.");
		}
	}
	
	@Override
	public Kit obtenerKitPorCodigo(Integer cdEnv, String codigo) throws ServiceException {
		try {
			return kitDao.obtenerKitPorCodigo(cdEnv,codigo);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KIT,"No se pudo obtener kit por codigo.");
		}
	}
	
	@Override
	public Kit obtenerKitPorId(Integer cdEnv, Long cdKit) throws ServiceException {
		try {
			return kitDao.obtenerKitPorIdEnv(cdEnv,cdKit);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KIT,"No se pudo obtener kit.");
		}
	}
	
	@Override
	public Kit obtenerKitHistPorCodigo(Integer cdEnv, String codigo,Timestamp fechaCreacion) throws ServiceException {
		try {
			return kitHistDao.obtenerKitPorCodigo(cdEnv,codigo,fechaCreacion);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KIT,"No se pudo obtener kit por codigo.");
		}
	}
	
	@Override
	public Kit obtenerKitPorId(Long cdKit) throws ServiceException {
		try {
			return kitDao.findById(cdKit);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_OBTENER_KIT,"No se pudo obtener kit.");
		}
	}

	@Override
	public Long guardarKit(Kit kit) throws ServiceException {
		try {
			Long cdKit = kitDao.insertAndReturnId(kit);
			if(kit.getListTemplates()!=null && !kit.getListTemplates().isEmpty()){
				for(Plantilla plantilla : kit.getListTemplates()){
					KitPlantilla kp = new KitPlantilla();
					kp.setCdKit(cdKit);
					kp.setCdPlantilla(plantilla.getCdPlantilla());
					kitPlantillaDao.insert(kp);
				}
			}
			if(!StringUtils.isEmpty(kit.getGrupoKit().getLbCorreoPlantilla())){
				kit.getGrupoKit().setCdKit(cdKit);
				envKitDao.insert(kit.getGrupoKit());
				List<Enviroment> envs = envService.obtenerEnvs();
				for(Enviroment env : envs){
					envKitDao.actualizarPlantillaDesdeCadena(cdKit,env.getCdEnv(),kit.getGrupoKit().getLbCorreoPlantilla());
				}
			}
			return cdKit;
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_GUARDAR_KIT,"No se pudo guardar kit.");
		}
	}

	@Override
	public Integer actualizarKit(Kit kit) throws ServiceException {
		try {
			Integer result = kitDao.update(kit);
			if(kit.getListTemplates()!=null && !kit.getListTemplates().isEmpty()){
				kitPlantillaDao.deletePorKit(kit.getCdKit());
				for(Plantilla plantilla : kit.getListTemplates()){
					KitPlantilla kp = new KitPlantilla();
					kp.setCdKit(kit.getCdKit());
					kp.setCdPlantilla(plantilla.getCdPlantilla());
					kitPlantillaDao.insert(kp);
				}
			}
			if(kit.getGrupoKit()!=null && !StringUtils.isEmpty(kit.getGrupoKit().getLbCorreoPlantilla())){
				if(kit.getGrupoKit().getCdEnv()!=null) {
					envKitDao.actualizarPlantillaDesdeCadena(kit.getCdKit(),kit.getGrupoKit().getCdEnv(),kit.getGrupoKit().getLbCorreoPlantilla());
				}else {
					List<Enviroment> envs = envService.obtenerEnvs();
					for(Enviroment env : envs){
						envKitDao.actualizarPlantillaDesdeCadena(kit.getCdKit(),env.getCdEnv(),kit.getGrupoKit().getLbCorreoPlantilla());
					}
				}
			}
			return result;
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_ACTUALIZAR_KIT,"No se pudo actualizar kit.");
		}
	}

	@Override
	public Integer eliminarKit(Long id) throws ServiceException {
		try {
			return kitDao.delete(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_ELIMINAR_KIT,"No se pudo eliminar kit.");
		}
	}

	@Override
	public Integer guardarKitPlantilla(KitPlantilla kitPlantilla)
			throws ServiceException {
		try {
			return kitPlantillaDao.insert(kitPlantilla);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_PLANTILLA_GUARDAR_KP,"No se pudo guardar kit plantilla.");
		}
	}

	@Override
	public Integer eliminarKitPlantilla(KitPlantilla kitPlantilla)
			throws ServiceException {
		try {
			return kitPlantillaDao.delete(kitPlantilla);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_PLANTILLA_ELIMINAR_KP,"No se pudo eliminar kit plantilla.");
		}
	}
	
	@Override
	public Integer guardarEnvKit(EnvKit envKit) throws ServiceException {
		try {
			return envKitDao.insert(envKit);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_KIT_GUARDAR_KP,"No se pudo guardar envkit.");
		}
	}

	@Override
	public Integer eliminarEnvKit(EnvKit envKit) throws ServiceException {
		try {
			return envKitDao.delete(envKit);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_KIT_ELIMINAR_KP,"No se pudo eliminar envkit.");
		}
	}
	
	@Override
	public Integer actualizarEnvKit(EnvKit envKit) throws ServiceException {
		try {
			return envKitDao.update(envKit);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_KIT_ACTUALIZAR_KP,"No se pudo actualizar envkit.");
		}
	}
	
}
