package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Parametro;

@Component
public class ParametroMapper implements RowMapper<Parametro> {

	
			public static final String ID_PARAM="ID_PARAM";
		    public static final String NU_TIPO_PARAM="NU_TIPO_PARAM";
			public static final String CH_NOMBRE_PARAM="CH_NOMBRE_PARAM";
			public static final String CH_VALOR_PARAM="CH_VALOR_PARAM";
			public static final String CH_DESC_PARAM="CH_DESC_PARAM";
			public static final String ST_ESTADO="ST_ESTADO";
			public static final String FH_FEC_CREACION="FH_FEC_CREACION";
			public static final String FH_FEC_MODIF="FH_FEC_MODIF";
			public static final String CH_USU_CREACION="CH_USU_CREACION";
			public static final String CH_USU_MODIFI="CH_USU_MODIFI";
			public static final String FH_TIMESTAMP="FH_TIMESTAMP";
			
			
			
			final LobHandler lobHandler = new DefaultLobHandler();
			
			
			@Override
	public Parametro mapRow(ResultSet rs, int rowNum) throws SQLException {
		Parametro parametro = new Parametro();
		parametro.setTipoParam(rs.getLong(NU_TIPO_PARAM));
		parametro.setNombreParam(rs.getString(CH_NOMBRE_PARAM));
		parametro.setValorParam(rs.getString(CH_VALOR_PARAM));
		parametro.setDescParam(rs.getString(CH_DESC_PARAM));
		parametro.setEstado(rs.getString(ST_ESTADO));
		parametro.setFhCreacion(rs.getDate(FH_FEC_CREACION));
		parametro.setFhModificacion(rs.getDate(FH_FEC_MODIF));
		parametro.setUsuCreacion(rs.getString(CH_USU_CREACION));
		parametro.setUsuModificacion(rs.getString(CH_USU_MODIFI));
		parametro.setFhTimestamp(rs.getTimestamp(FH_TIMESTAMP));

		return parametro;
	}

}
