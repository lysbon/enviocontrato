package com.bbva.cuentas.aat.service.impl;

import java.util.Date;
import java.util.List;
import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.ItemDao;
import com.bbva.cuentas.aat.dao.ItemHistDao;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.ItemStamp;

@Service
public class ItemServiceImpl implements ItemService{

	private static final Logger logger = LoggerFactory.getLogger(ItemServiceImpl.class);
	
	@Autowired
	protected ItemDao itemDao;
	
	@Autowired
	protected ItemHistDao itemHistDao;
	
	public ItemStamp obtenerItemPorCodigo(Long id) throws ServiceException {
		try {
			return itemDao.findById(id);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_ITEM,"No se pudo obtener item.");
		}
	}

	@Override
	public List<ItemStamp> obtenerItemsPorSeccion(Long cdSeccion,Date fecHoraCreacion)
			throws ServiceException {
		try {
			
			if(fecHoraCreacion!=null){
				Timestamp fecFiltro = new Timestamp(fecHoraCreacion.getTime());
				return itemHistDao.obtenerItemsPorSeccionAscendente(cdSeccion,fecFiltro);
			}else{
				return itemDao.obtenerItemsPorSeccionAscendente(cdSeccion);
			}
			
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,"No se pudo obtener secciones de plantilla.");
		}
	}

	@Override
	public Long guardarItem(ItemStamp itemStamp) throws ServiceException {
		try {
			return itemDao.insertAndReturnId(itemStamp);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_GUARDAR_ITEM,"No se pudo guardar item.");
		}
	}

	@Override
	public Integer actualizarItem(ItemStamp itemStamp) throws ServiceException {
		try {
			return itemDao.update(itemStamp);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");
		}
	}

	@Override
	public Integer eliminarItem(Long id) throws ServiceException {
		try {
			return itemDao.delete(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ELIMINAR_ITEM,"No se pudo eliminar item.");
		}
	}

	@Override
	public List<ItemStamp> obtenerItems() throws ServiceException {
		try {
			return itemDao.getAllEntities();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_OBTENER_ITEMS,"No se pudo obtener items.");
		}
	}
	
}
