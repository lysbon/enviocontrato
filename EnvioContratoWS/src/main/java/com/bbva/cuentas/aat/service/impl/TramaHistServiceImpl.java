package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.DetTramaDao;
import com.bbva.cuentas.aat.dao.TramaDao;
import com.bbva.cuentas.aat.dao.TramaHistDao;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.TramaHistService;

import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;

@Service
public class TramaHistServiceImpl  implements TramaHistService{
	
	
private static final Logger logger = LoggerFactory.getLogger(TramaHistServiceImpl.class);
	
	@Autowired
	private TramaHistDao tramaHistDao;

	
	public TramaHist obtenerTramaHistPoridTrama(Long idTrama) throws ServiceException {
		try {
			return tramaHistDao.obtenerTramaHistPoridTrama(idTrama);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	}   
	
	public List<TramaHist> obtenerTramaHistPorEnv(Integer env) throws ServiceException {
		try {
			return tramaHistDao.obtenerTramaHistPorEnv(env);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	} 
	
	public List<TramaHist> obtenerTramaHistPorKit(Long kit) throws ServiceException {
		try {
			return tramaHistDao.obtenerTramaHistPorKit(kit);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	} 
	
	public List<TramaHist> obtenerTramaHistPorContrato(String contrato) throws ServiceException {
		try {
			return tramaHistDao.obtenerTramaHistPorContrato(contrato);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	} 
	
	public List<TramaHist> obtenerTramaHistPorTitCodCli(String titCodCli) throws ServiceException {
		try {
			return tramaHistDao.obtenerTramaHistPorTitCodCli(titCodCli);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(
					ErrorCodes.S_ERROR_ITM_OBTENER_POR_SECCION,
					"No se pudo obtener trama a reprocesar.");
		}
	} 
	
	@Override
	public int updateTramaHist(TramaHist tramaHist) throws ServiceException {
		try {
			return tramaHistDao.updateTramaHist(tramaHist);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");
		}
	}

	@Override
	public Long insertAndReturnId(TramaHist tramaHist) {
		try {
			return tramaHistDao.insertAndReturnId(tramaHist);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return null;
	}
	
	
	
	
	

}
