package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.Trama;

public interface TramaDao extends BaseDao<Trama, Long>{

	public List<Trama> obtenerTrama(
			String cdEnv,
			String procedencia,
			String tipo) throws DaoException;
	
	public Integer limpiarTramas() throws DaoException;
	
	public List<Trama> obtenerTramasParaReproceso(String estado) throws DaoException;
	

	public List<Trama> obtenerTramasParaReproceso(String estado, String intentos, String cantidad) throws DaoException;
	
	
	public int contarTramasPorEstado(String estado) throws DaoException ;
	
	public int updatePorEstado(String estadoActualizar,String estadoSeleccion) throws DaoException;
	
	
	
	public int contarTramasPorEstado(String estado,String intentos) throws DaoException;
	
	public Integer updateTramaPorEstadoFecha(String estadoActualizar,String estadoSeleccion,String fechaDesde,String intentos) throws DaoException;
	
	public Integer updateTramaPorEstadoFechaHasta(String estadoActualizar,String estadoSeleccion,String fechaDesde,String fechaHasta,String intentos) throws DaoException;

}