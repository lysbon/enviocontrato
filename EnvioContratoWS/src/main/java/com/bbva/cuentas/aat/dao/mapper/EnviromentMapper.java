package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.Enviroment;

@Component
public class EnviromentMapper implements RowMapper<Enviroment>{

	public static final String CD_ENV="CD_ENV";
	public static final String NB_NOMBRE="NB_NOMBRE";
	public static final String NB_DESCRIPCION="NB_DESCRIPCION";
	public static final String ST_ACTIVO="ST_ACTIVO";
	public static final String CH_TIPO="CH_TIPO";
	public static final String ST_DEFECTO="ST_DEFECTO";
	public static final String NB_RUTAGESTDOC="NB_RUTAGESTDOC";
	
	public static final String ST_HUELLA="ST_HUELLA";
	
	@Override
	public Enviroment mapRow(ResultSet rs, int rowNum) throws SQLException {
		Enviroment env = new Enviroment();
		env.setCdEnv(rs.getInt(CD_ENV));
		env.setChTipo(rs.getString(CH_TIPO));
		env.setNbNombre(rs.getString(NB_NOMBRE));
		env.setNbDescripcion(rs.getString(NB_DESCRIPCION));
		env.setStActivo(rs.getString(ST_ACTIVO));
		env.setStDefecto(rs.getString(ST_DEFECTO));
		env.setNbRutagestdoc(rs.getString(NB_RUTAGESTDOC));
		env.setStHuella(rs.getString(ST_HUELLA));
		
		return env;
	}

}
