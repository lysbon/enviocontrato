package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.KitPlantilla;

@Component
public class KitPlantillaMapper implements RowMapper<KitPlantilla>{

	public static final String CD_KIT="CD_KIT";
	public static final String CD_PLANTILLA="CD_PLANTILLA";
	public static final String CD_ENV="CD_ENV";
	
	@Override
	public KitPlantilla mapRow(ResultSet rs, int rowNum) throws SQLException {
		KitPlantilla kitPlantilla = new KitPlantilla();
		kitPlantilla.setCdKit(rs.getLong(CD_KIT));
		kitPlantilla.setCdPlantilla(rs.getLong(CD_PLANTILLA));
		return kitPlantilla;
	}

}
