package com.bbva.cuentas.aat.dao;

import com.bbva.cuentas.bean.KitLegacy;

public interface KitLegacyDao extends BaseDao<KitLegacy, String>{
	
	public String obtenerMailTemplate(String cdKit);
		
}
