package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.FlagPlantilla;

@Component
public class FlagPlantillaMapper implements RowMapper<FlagPlantilla>{

	public static final String CD_FLAG="CD_FLAG";
	public static final String CD_PLANTILLA="CD_PLANTILLA";
	public static final String ST_VALOR="ST_VALOR";
	public static final String ST_ACTIVO="ST_ACTIVO";
	public static final String CD_ENV="CD_ENV";
	
	@Autowired
	FlagMapper flagMapper;
	
	@Override
	public FlagPlantilla mapRow(ResultSet rs, int rowNum) throws SQLException {
		FlagPlantilla flagPlantilla = new FlagPlantilla();
		flagPlantilla.setCdFlag(rs.getLong(CD_FLAG));
		flagPlantilla.setCdPlantilla(rs.getLong(CD_PLANTILLA));
		flagPlantilla.setStValor(rs.getString(ST_VALOR));
		flagPlantilla.setStActivo(rs.getString(ST_ACTIVO));
		flagPlantilla.setFlag(flagMapper.mapRow(rs, rowNum));
		return flagPlantilla;
	}

}
