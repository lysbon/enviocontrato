package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.service.exception.ServiceException;

public interface EnviromentService {

	public List<Enviroment> obtenerEnvs() throws ServiceException;

	public Enviroment obtenerEnvPorCodigo(Integer id) throws ServiceException;
	
	public Enviroment obtenerEnvPorDefecto() throws ServiceException;
	
	public List<Enviroment> obtenerEnvPorTipo(String tipo) throws ServiceException;
	
	public Integer guardarEnv(Enviroment env) throws ServiceException;
	
	public Integer actualizarEnv(Enviroment env) throws ServiceException;
	
	public Integer eliminarEnv(Integer id) throws ServiceException;
	
	public Integer guardarEnvKit(EnvKit envKit) throws ServiceException;
	
	public Integer eliminarEnvKit(EnvKit envKit) throws ServiceException;

	public Enviroment obtenerGrupoEnv(String idGrupo) throws ServiceException;
		
}
