package com.bbva.cuentas.aat.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.SqlOperador;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.FlagPlantillaDao;
import com.bbva.cuentas.aat.dao.mapper.FlagPlantillaMapper;
import com.bbva.cuentas.bean.FlagPlantilla;

@Component
public class FlagPlantillaDaoImpl extends BaseDaoImpl<FlagPlantilla, Long> implements FlagPlantillaDao{
	
	private static final Logger logger = LoggerFactory.getLogger(FlagPlantillaDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG006_FLAG_PLANTILLA";
	static final String TABLE_PK = Constants.VACIO;
	static final String TABLE_SEQ = Constants.VACIO;
	
	@Autowired
	FlagPlantillaMapper flagPlantillaMapper;
	
	@Autowired
	public FlagPlantillaDaoImpl(DataSource datasource, FlagPlantillaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}

	@Override
	public List<FlagPlantilla> obtenerFlagsPorPlantilla(Long id)
			throws DaoException {
		List<FlagPlantilla> flagsPlantilla = new ArrayList<FlagPlantilla>();
		try{
			String sql = 
					"SELECT *  "+
					"FROM  "+TABLE_NAME+" FP "+
					"INNER JOIN "+FlagDaoImpl.TABLE_NAME+" F "+
					"ON    FP.CD_FLAG = F.CD_FLAG "+
					"WHERE  FP.CD_PLANTILLA = ? ";
			Long[] args = new Long[1];
			args[0] = id;
			flagsPlantilla = getJdbcTemplate().query(sql,args,flagPlantillaMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return flagsPlantilla;
	}
	
	@Override
	public FlagPlantilla obtenerFlagPlantilla(Long cdPlantilla, Long cdFlag) throws DaoException {
		List<FlagPlantilla> flagsPlantilla = null;
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" FP "+
					"INNER  JOIN "+FlagDaoImpl.TABLE_NAME+" F "+
					"ON     FP.CD_FLAG = F.CD_FLAG "+
					"WHERE  FP.CD_PLANTILLA = ? "+
					" AND   FP.CD_FLAG = ? ";
			Long[] args = new Long[2];
			args[0] = cdPlantilla;
			args[1] = cdFlag;
			flagsPlantilla = getJdbcTemplate().query(sql,args,flagPlantillaMapper);
			if(flagsPlantilla!=null && !flagsPlantilla.isEmpty())
				return flagsPlantilla.get(0);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return null;
	}

	@Override
	public List<FlagPlantilla> getAllEntities() {
		List<FlagPlantilla> flagsPlantilla = new ArrayList<FlagPlantilla>();
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" FP "+
					"INNER  JOIN "+FlagDaoImpl.TABLE_NAME+" F "+
					"ON     FP.CD_FLAG = F.CD_FLAG ";
			flagsPlantilla = getJdbcTemplate().query(sql,flagPlantillaMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
		}
		return flagsPlantilla;
	}

	@Override
	public Integer delete(Long cdPlantilla, Long cdFlag) throws DaoException {
		SqlCondicion[] condiciones = new SqlCondicion[2];
		condiciones[0] = new SqlCondicion(FlagPlantillaMapper.CD_FLAG, cdFlag);
		condiciones[1] = new SqlCondicion(SqlOperador.AND,FlagPlantillaMapper.CD_PLANTILLA, cdPlantilla);
		return super.delete(condiciones);
	}
	
	@Override
	public int insert(FlagPlantilla flagPlantilla) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(FlagPlantillaMapper.CD_FLAG, flagPlantilla.getCdFlag());
		args.put(FlagPlantillaMapper.CD_PLANTILLA, flagPlantilla.getCdPlantilla());
		args.put(FlagPlantillaMapper.ST_ACTIVO, flagPlantilla.getStActivo());
		args.put(FlagPlantillaMapper.ST_VALOR, flagPlantilla.getStValor());
		return super.insert(args);
	}

	@Override
	public int update(FlagPlantilla flagPlantilla) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(FlagPlantillaMapper.CD_PLANTILLA, flagPlantilla.getCdPlantilla());
		args.put(FlagPlantillaMapper.ST_ACTIVO, flagPlantilla.getStActivo());
		args.put(FlagPlantillaMapper.ST_VALOR, flagPlantilla.getStValor());
		SqlCondicion[] condiciones = new SqlCondicion[2];
		condiciones[0] = new SqlCondicion(FlagPlantillaMapper.CD_FLAG, flagPlantilla.getCdFlag());
		condiciones[1] = new SqlCondicion(SqlOperador.AND,FlagPlantillaMapper.CD_PLANTILLA, flagPlantilla.getCdPlantilla());
		return super.update(args, condiciones);
	}
	
}