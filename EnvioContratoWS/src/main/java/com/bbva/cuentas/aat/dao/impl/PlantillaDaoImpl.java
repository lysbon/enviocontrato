package com.bbva.cuentas.aat.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.SqlCondicion;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.PlantillaDao;
import com.bbva.cuentas.aat.dao.mapper.PlantillaMapper;
import com.bbva.cuentas.bean.Plantilla;

@Component
public class PlantillaDaoImpl extends BaseDaoImpl<Plantilla, Long> implements PlantillaDao{
	
	private static final Logger logger = LoggerFactory.getLogger(PlantillaDaoImpl.class);
	
	static final String TABLE_NAME = Constants.SCHEMA+Constants.SCHEMA_POINT+"TBG002_PLANTILLA";
	static final String TABLE_PK = "CD_PLANTILLA";
	static final String TABLE_SEQ = Constants.SCHEMA+Constants.SCHEMA_POINT+"tpl_seq";
	
	@Autowired
	PlantillaMapper plantillaMapper;
	
	@Autowired
	public PlantillaDaoImpl(DataSource datasource, PlantillaMapper mapper) {
		super(datasource, TABLE_NAME, TABLE_PK, TABLE_SEQ, mapper);
	}
		
	public byte[] obtenerPDFTemplate(Long cdPlantilla) {
		final LobHandler lobHandler = new DefaultLobHandler();
		List<byte[]> lista = getJdbcTemplate().query(
				"SELECT  "+PlantillaMapper.LB_PDF_PLANTILLA+" "
				+" FROM  "+TABLE_NAME
				+" WHERE "+TABLE_PK+" = ?", new Object[] { cdPlantilla },
				new RowMapper<byte[]>(){
					@Override
					public byte[] mapRow(ResultSet rs, int rownumber) throws SQLException {
						byte[] blob = lobHandler.getBlobAsBytes(rs, "LB_PDF_PLANTILLA");
						return blob;
					}
				}
        );
		if(lista!=null && lista.size()>0)
			return lista.get(0);
		return null;
	}

	@Override
	public List<Plantilla> obtenerPlantillas() throws DaoException {
		return super.getAllEntities();
	}
	
	@Override
	public List<Plantilla> obtenerPlantillasPorKit(Long cdKit) throws DaoException {
		List<Plantilla> listaPlantilla = new ArrayList<Plantilla>();
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+KitPlantillaDaoImpl.TABLE_NAME+" KP "+
					"ON     P.CD_PLANTILLA = KP.CD_PLANTILLA "+
					"WHERE  KP.CD_KIT = ? "+
					"ORDER  BY P.CD_PLANTILLA";
			Long[] args = new Long[1];
			args[0] = cdKit;
			listaPlantilla = getJdbcTemplate().query(sql,args,plantillaMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return listaPlantilla;
	}
	
	@Override
	public List<Plantilla> obtenerPlantillasPorKitEstado(Long cdKit, String estado) throws DaoException {
		List<Plantilla> listaPlantilla = new ArrayList<Plantilla>();
		try{
			String sql = 
					"SELECT * "+
					"FROM   "+TABLE_NAME+" P "+
					"INNER  JOIN "+KitPlantillaDaoImpl.TABLE_NAME+" KP "+
					"ON     P.CD_PLANTILLA = KP.CD_PLANTILLA "+
					"WHERE  KP.CD_KIT = ? "+
					"AND    P.ST_ACTIVO = ? "+
					"ORDER  BY P.CD_PLANTILLA";
			Object[] args = new Object[2];
			args[0] = cdKit;
			args[1] = estado;
			listaPlantilla = getJdbcTemplate().query(sql,args,plantillaMapper);
		}catch (DataAccessException dae){
			logger.debug(AppUtil.getDetailedException(dae).toString());
			throw new DaoException(ErrorCodes.D_QUERY_ERROR);
		}
		return listaPlantilla;
	}
	
	@Override
	public Integer actualizarPDF(final Long cdPlantilla, final byte[] pdf) 
            throws DaoException {
		String sqlText = null;
		int result;
		try {        	
        	sqlText = "UPDATE "+TABLE_NAME+" SET "+PlantillaMapper.LB_PDF_PLANTILLA+" = ? WHERE "+TABLE_PK+" = ? ";
        	final InputStream blobIs = new ByteArrayInputStream(pdf);
        	final LobHandler lobHandler = new DefaultLobHandler();
        	result = getJdbcTemplate().execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	    	  lobCreator.setBlobAsBinaryStream(ps, 1, blobIs, (int)pdf.length);
        	        ps.setLong(2, cdPlantilla);
        	      }
        	  }
        	);
        	blobIs.close();
			return result;
        } catch (IOException e) {
        	logger.error("Caught I/O Exception: (Write BLOB value - Put Method).",e);
            throw new DaoException(ErrorCodes.D_UPDATE_ERROR,"Error al actualizar pdf");
        }
    }
	
	public Integer actualizarPDF(final Long cdPlantilla, String filename) 
            throws DaoException {
		
		String sqlText = null;
		int result;
		InputStream blobIs=null;
		try {        	
        	sqlText = "UPDATE "+TABLE_NAME+" SET LB_PDF_PLANTILLA = ?, FH_MODIFICACION = SYSDATE WHERE CD_PLANTILLA = ? ";
        	final File blobIn = new File(filename);
        	blobIs = new FileInputStream(blobIn);
        	final InputStream tmpBlobIs=blobIs;
        	final LobHandler lobHandler = new DefaultLobHandler();
        	result = getJdbcTemplate().execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	        lobCreator.setBlobAsBinaryStream(ps, 1, tmpBlobIs, (int)blobIn.length());
        	        ps.setLong(2, cdPlantilla);
        	      }
        	  }
        	);
        	blobIs.close();
        	return result;
        } catch (IOException e) {
        	logger.error("Caught I/O Exception: (Write BLOB value - Put Method).",e);
            throw new DaoException(ErrorCodes.D_UPDATE_ERROR,"Error al actualizar pdf");
        }finally {
        	try {if (blobIs!=null) blobIs.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar inputStream",e);}
        }
    }

	@Override
	public Long insertAndReturnId(Plantilla plantilla) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(PlantillaMapper.ST_ACTIVO, plantilla.getStActivo());
		args.put(PlantillaMapper.ST_CONDICION, plantilla.getStCondicion());
		args.put(PlantillaMapper.ST_FILEUNICO, plantilla.getStFileunico());
		args.put(PlantillaMapper.ST_ENV_CORREO, plantilla.getStEnvCorreo());
		args.put(PlantillaMapper.ST_TIPO_CONSTR, plantilla.getStTipoConstr());
		args.put(PlantillaMapper.ST_SIGNED, plantilla.getStSigned());
		args.put(PlantillaMapper.NB_NOMBRE_FORMATO, plantilla.getNbNombreFormato());
		args.put(PlantillaMapper.NB_TIPO_FILEUNICO, plantilla.getNbTipoFileunico());
		args.put(PlantillaMapper.NB_NOMBRE_CORREO, plantilla.getNbNombreCorreo());
		args.put(PlantillaMapper.CH_TIPO_PLANTILLA, plantilla.getChTipoPlantilla());
		args.put(PlantillaMapper.NB_DESCRIPCION, plantilla.getNbDescripcion());
		return super.insertAndReturnId(args);
	}

	@Override
	public int update(Plantilla plantilla) throws DaoException {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put(PlantillaMapper.ST_ACTIVO, plantilla.getStActivo());
		args.put(PlantillaMapper.ST_CONDICION, plantilla.getStCondicion());
		args.put(PlantillaMapper.ST_FILEUNICO, plantilla.getStFileunico());
		args.put(PlantillaMapper.ST_ENV_CORREO, plantilla.getStEnvCorreo());
		args.put(PlantillaMapper.ST_TIPO_CONSTR, plantilla.getStTipoConstr());
		args.put(PlantillaMapper.ST_SIGNED, plantilla.getStSigned());
		args.put(PlantillaMapper.NB_NOMBRE_FORMATO, plantilla.getNbNombreFormato());
		args.put(PlantillaMapper.NB_TIPO_FILEUNICO, plantilla.getNbTipoFileunico());
		args.put(PlantillaMapper.NB_NOMBRE_CORREO, plantilla.getNbNombreCorreo());
		args.put(PlantillaMapper.CH_TIPO_PLANTILLA, plantilla.getChTipoPlantilla());
		args.put(PlantillaMapper.NB_DESCRIPCION, plantilla.getNbDescripcion());
		
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(PlantillaMapper.CD_PLANTILLA, plantilla.getCdPlantilla());
		return super.update(args, condiciones);
	}

	@Override
	public Plantilla obtenerPlantillasPorNombre(String nombre)
			throws DaoException {
		SqlCondicion[] condiciones = new SqlCondicion[1];
		condiciones[0] = new SqlCondicion(PlantillaMapper.NB_NOMBRE_FORMATO, nombre);
		List<Plantilla> plantillas =  super.getEntities(condiciones);
		if(plantillas!=null && plantillas.size()>0){
			return plantillas.get(0);
		}
		return null;
	}
	
}
