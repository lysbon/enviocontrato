package com.bbva.cuentas.aat.service;

import java.util.Date;
import java.util.List;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;

public interface PlantillaService {

	public List<Plantilla> obtenerPlantillas() throws ServiceException;

	public Plantilla obtenerPlantillaPorCodigo(Long id) throws ServiceException;
		
	public String obtenerPDFBase64(Long id) throws ServiceException;
	
	public byte[] obtenerPDF(Long id,Date fecHoraCreacion) throws GenerarContratoException;
		
	public List<Plantilla> obtenerPlantillasAsociadas(Long cdKit) throws ServiceException;
	
	public void actualizarPDFdesdeArchivo(Long id, String filename) throws ServiceException;
	
	public void actualizarPDFdesdeCadena(Long id, String cadena) throws ServiceException;
	
	public Long guardarPlantilla(Plantilla plantilla) throws ServiceException;
	
	public Integer actualizarPlantilla(Plantilla plantilla) throws ServiceException;
	
	public Integer eliminarPlantilla(Long id) throws ServiceException;

	public List<Plantilla> obtenerPlantillasPorKit(Long cdKit) throws ServiceException;
	
	public List<Plantilla> obtenerPlantillasPorKitActivas(Long cdKit,Date fecHoraCreacion) throws ServiceException;

	public Plantilla obtenerPlantillaPorNombre(String nombre) throws ServiceException;
	public Plantilla buscarPlantilla(List<Plantilla> lista, long codigo) throws ServiceException;
}
