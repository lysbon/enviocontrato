package com.bbva.cuentas.aat.service;

import java.util.List;



import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.service.exception.ServiceException;

public interface DetTramaService {
	
	public List<DetTrama> obtenerDetTramasReprocesar(String estado) throws ServiceException;
	public List<DetTrama> obtenerDetTramasReprocesar(Long idTrama) throws ServiceException;
	public List<DetTrama> obtenerDetTramasReprocesar(String estado,Long idTrama) throws ServiceException;
	public Integer actualizarTrama(DetTrama detTrama) throws ServiceException; 

}
