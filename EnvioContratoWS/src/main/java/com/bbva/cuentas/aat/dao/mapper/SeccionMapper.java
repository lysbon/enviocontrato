package com.bbva.cuentas.aat.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.bean.ItemSection;

@Component
public class SeccionMapper implements RowMapper<ItemSection>{

	//TODO crear clases data
	public static final String CD_SECCION="CD_SECCION";
	public static final String CD_PLANTILLA="CD_PLANTILLA";
	public static final String NU_PAGINA="NU_PAGINA";
	public static final String CD_ENV="CD_ENV";
	
	@Override
	public ItemSection mapRow(ResultSet rs, int rowNum) throws SQLException {
		ItemSection seccion = new ItemSection();
		seccion.setCdSeccion(rs.getLong(CD_SECCION));
		seccion.setCdPlantilla(rs.getLong(CD_PLANTILLA));
		seccion.setNuPagina(rs.getInt(NU_PAGINA));
		return seccion;
	}

}
