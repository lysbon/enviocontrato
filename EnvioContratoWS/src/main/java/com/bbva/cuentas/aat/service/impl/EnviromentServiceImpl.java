package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.EnvKitDao;
import com.bbva.cuentas.aat.dao.EnviromentDao;
import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;

@Service
public class EnviromentServiceImpl implements EnviromentService{

	private static final Logger logger = LoggerFactory.getLogger(EnviromentServiceImpl.class);
	
	private static final String ERROR_OBTENER_ENV = "No se pudo obtener enviroment.";
	@Autowired
	private EnviromentDao enviromentDao;
	@Autowired
	private EnvKitDao envKitDao;
	
	public List<Enviroment> obtenerEnvs() throws ServiceException{
		try {
			return enviromentDao.getAllEntities();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_OBTENER_ENVIROMENTS,"No se pudo obtener enviroments.");
		}
	}

	public Enviroment obtenerEnvPorCodigo(Integer id) throws ServiceException{
		try {
			return enviromentDao.findById(id);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_OBTENER_ENVIROMENT,ERROR_OBTENER_ENV);
		}
	}
	
	public List<Enviroment> obtenerEnvPorTipo(String tipo) throws ServiceException{
		try {
			return enviromentDao.obtenerEnvPorTipo(tipo);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_OBTENER_ENVIROMENT,ERROR_OBTENER_ENV);
		}
	}

	@Override
	public Integer guardarEnv(Enviroment env) throws ServiceException {
		try {
			return enviromentDao.insertAndReturnId(env);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_GUARDAR_ENVIROMENT,"No se pudo guardar enviroment.");
		}
	}

	@Override
	public Integer actualizarEnv(Enviroment env) throws ServiceException {
		try {
			return enviromentDao.update(env);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_ACTUALIZAR_ENVIROMENT,"No se pudo actualizar enviroment.");
		}
	}

	@Override
	public Integer eliminarEnv(Integer id) throws ServiceException {
		try {
			return enviromentDao.delete(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_ELIMINAR_ENVIROMENT,"No se pudo eliminar enviroment.");
		}
	}

	@Override
	public Enviroment obtenerEnvPorDefecto() throws ServiceException {
		try {
			return enviromentDao.obtenerEnvPorDefecto();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_ENV_OBTENER_ENVIROMENT,ERROR_OBTENER_ENV);
		}
	}

	@Override
	public Integer guardarEnvKit(EnvKit envKit) throws ServiceException {
		try {
			return envKitDao.insert(envKit);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_PLANTILLA_GUARDAR_KP,"No se pudo guardar kit env.");
		}
	}

	@Override
	public Integer eliminarEnvKit(EnvKit envKit) throws ServiceException {
		try {
			return envKitDao.delete(envKit);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_KIT_PLANTILLA_ELIMINAR_KP,"No se pudo eliminar kit env.");
		}
	}

	@Override
	public Enviroment obtenerGrupoEnv(String idGrupo) throws ServiceException{
		Enviroment env = null;
		if(StringUtils.isEmpty(idGrupo)){
			env = this.obtenerEnvPorDefecto();
		}else{
			Integer cdEnv = new Integer(idGrupo);
			env = this.obtenerEnvPorCodigo(cdEnv);
		}
		return env;
	}
	
}
