package com.bbva.cuentas.aat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.KitLegacyDao;
import com.bbva.cuentas.aat.dao.KitPlantillaLegacyDao;
import com.bbva.cuentas.aat.service.KitLegacyService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.KitLegacy;
import com.bbva.cuentas.bean.KitPlantillaLegacy;

@Service
public class KitLegacyServiceImpl implements KitLegacyService{

	@Autowired
	private KitLegacyDao kitDao;
	@Autowired
	private KitPlantillaLegacyDao kitPlantillaDao;
	
	public List<KitLegacy> obtenerKits() throws ServiceException{
		return kitDao.getAllEntities();
	}

	public KitLegacy obtenerKitPorId(String kit) throws ServiceException{
		return kitDao.findById(kit);
	}
	
	public String obtenerMailTemplate(String kit) throws ServiceException{
		return kitDao.obtenerMailTemplate(kit);
	}

	@Override
	public List<KitPlantillaLegacy> obtenerKitPlantillas() throws ServiceException {
		return kitPlantillaDao.getAllEntities();
	}
	
}
