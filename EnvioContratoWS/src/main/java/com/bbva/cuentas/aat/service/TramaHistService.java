package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.DetTramaHist;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.service.exception.ServiceException;

public interface TramaHistService {

	TramaHist obtenerTramaHistPoridTrama(Long idTrama) throws ServiceException;

	List<TramaHist> obtenerTramaHistPorEnv(Integer env) throws ServiceException;

	List<TramaHist> obtenerTramaHistPorKit(Long kit) throws ServiceException;

	List<TramaHist> obtenerTramaHistPorContrato(String contrato) throws ServiceException;

	List<TramaHist> obtenerTramaHistPorTitCodCli(String titCodCli) throws ServiceException;

	Long insertAndReturnId(TramaHist tramaHist) throws ServiceException;

	int updateTramaHist(TramaHist tramaHist) throws ServiceException;

}
