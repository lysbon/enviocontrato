package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.service.exception.ServiceException;

public interface TramaService {
	

	List<Trama> obtenerTramasReprocesar(String Estado, String Intentos, String Cantidad) throws ServiceException;
	
	List<Trama> obtenerTramasReprocesar(String Estado) throws ServiceException;
	
	Integer actualizarTrama(Trama trama) throws ServiceException; 
	
	Long insertaTramaSeq(Trama trama);
	
	int insertaTrama(Trama trama);

	void updateTrama(Trama trama);
	
	//void updateTramaporEstado(String estadoActualizar,String estadoSeleccion,String fechaDesde);

	void updateDetTrama(DetTrama detTrama);

	Long insertaDetTrama(DetTrama detTrama);

	Integer limpiarTramas() throws ServiceException;

	List<DetTrama> obtenerDetTramasReprocesar(String estado,Long idTrama) throws ServiceException;

	Trama obtenerTramasPorId(Long i) throws ServiceException;
	
	int contarTramasPorEstado(String estado,String intentos) throws ServiceException;
	
	void updateTramaporEstado(String estadoActualizar,String estadoSeleccion,String fechaDesde,String fechaHasta,String intentos);

}
