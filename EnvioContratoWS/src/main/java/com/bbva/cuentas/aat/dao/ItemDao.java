package com.bbva.cuentas.aat.dao;

import java.util.List;

import com.bbva.cuentas.bean.ItemStamp;

public interface ItemDao extends BaseDao<ItemStamp, Long>{
	
	public List<ItemStamp> obtenerItemsPorSeccionEstado(Long cdSeccion,String estado) throws DaoException;

	public List<ItemStamp> obtenerItemsPorSeccion(Long cdSeccion) throws DaoException;
	
	public List<ItemStamp> obtenerItemsPorSeccionAscendente(Long cdSeccion) throws DaoException;
	
}
