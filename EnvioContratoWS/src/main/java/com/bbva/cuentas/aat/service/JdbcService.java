package com.bbva.cuentas.aat.service;

import java.util.List;

import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;

public interface JdbcService {
	
	public Map<String, Object> execute(String query) throws ServiceException;
	
	public List<Map<String, Object>> query(String query) throws ServiceException;
	
}
