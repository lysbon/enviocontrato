package com.bbva.cuentas.aat.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.SeccionDao;
import com.bbva.cuentas.aat.dao.SeccionHistDao;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;

@Service
public class SeccionServiceImpl implements SeccionService{

	private static final Logger logger = LoggerFactory.getLogger(SeccionServiceImpl.class);
	
	@Autowired
	private SeccionDao seccionDao;
	@Autowired
	private SeccionHistDao seccionHistDao;
	@Autowired
	private ItemService itemService;
	
	public ItemSection obtenerSeccionPorId(Long id) throws ServiceException{
		try {
			return seccionDao.findById(id);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_OBTENER_SECCION,"No se pudo obtener seccion.");
		}
	}

	@Override
	public List<ItemSection> obtenerSeccionesPorPlantilla(Long cdPlantilla,Date fecHoraCreacion) throws ServiceException {
		try {
			if (fecHoraCreacion!=null){
				Timestamp fecFiltro = new Timestamp(fecHoraCreacion.getTime());
				return seccionHistDao.obtenerSeccionesPorPlantilla(cdPlantilla,fecFiltro);
			}else{
				return seccionDao.obtenerSeccionesPorPlantilla(cdPlantilla);
			}
			
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_OBTENER_POR_PLANTILLA,"No se pudo obtener secciones de plantilla.");
		}
	}
	
	@Override
	public List<ItemSection> obtenerSeccionesItemPorPlantilla(Long cdPlantilla,Date fecHoraCreacion) throws ServiceException {
		try {
			List<ItemSection> secciones = obtenerSeccionesPorPlantilla(cdPlantilla,fecHoraCreacion);
			if(secciones!=null && secciones.size()>0){
				for(ItemSection seccion: secciones){
					List<ItemStamp> elementos = itemService.obtenerItemsPorSeccion(seccion.getCdSeccion(),fecHoraCreacion);
					seccion.setListItems(elementos);
				}
				return secciones;
			}else{
				return null;
			}
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_OBTENER_POR_PLANTILLA,"No se pudo obtener secciones de plantilla.");
		}
	}

	@Override
	public Long guardarSeccion(ItemSection itemSection) throws ServiceException {
		try {
			return seccionDao.insertAndReturnId(itemSection);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_GUARDAR_SECCION,"No se pudo guardar seccion.");
		}
	}

	@Override
	public Integer actualizarSeccion(ItemSection itemSection)
			throws ServiceException {
		try {
			return seccionDao.update(itemSection);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_ACTUALIZAR_SECCION,"No se pudo actualizar seccion.");
		}
	}

	@Override
	public Integer eliminarSeccion(Long id) throws ServiceException {
		try {
			return seccionDao.delete(id);
		} catch (DaoException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_ELIMINAR_SECCION,"No se pudo eliminar seccion.");
		}
	}

	@Override
	public List<ItemSection> obtenerSecciones() throws ServiceException {
		try {
			return seccionDao.getAllEntities();
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_SEC_OBTENER_SECCIONES,"No se pudo obtener secciones.");
		}
	}	
	
}
