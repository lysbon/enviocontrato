package com.bbva.cuentas.aat.service;

import com.bbva.cuentas.service.exception.ServiceException;

public interface InitService {

	public void init() throws ServiceException;
	
	public void initEnv() throws ServiceException;
	
}
