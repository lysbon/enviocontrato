package com.bbva.cuentas.aat.dao;

import com.bbva.cuentas.bean.KitPlantillaLegacy;

public interface KitPlantillaLegacyDao extends BaseDao<KitPlantillaLegacy, String>{
	
}
