package com.bbva.cuentas.aat.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.dao.PlantillaDao;
import com.bbva.cuentas.aat.dao.PlantillaHistDao;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.Plantilla;

@Service
public class PlantillaServiceImpl implements PlantillaService{

	private static final Logger logger = LoggerFactory.getLogger(PlantillaServiceImpl.class);
	
	@Autowired
	private PlantillaDao plantillaDao;
	@Autowired
	private PlantillaHistDao plantillaHistDao;
	@Override
	public List<Plantilla> obtenerPlantillas()	throws ServiceException {
		try {
			System.out.println("entro xx_1");
			return plantillaDao.obtenerPlantillas();
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLAS,"No se pudo obtener plantillas.");
		}
	}

	public Plantilla obtenerPlantillaPorCodigo(Long id) throws ServiceException {
		try {
			return plantillaDao.findById(id);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLA,"No se pudo obtener plantilla.");
		}
	}
	
	public byte[] obtenerPDF(Long id,Date fecHoraCreacion) throws GenerarContratoException{
		try {
			if (fecHoraCreacion != null){
				Timestamp fechaFiltro = new Timestamp(fecHoraCreacion.getTime());
				return plantillaHistDao.obtenerPDFTemplate(id,fechaFiltro);
			}else{
				return plantillaDao.obtenerPDFTemplate(id);
			}
			
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new GenerarContratoException("No se pudo obtener el PDF solicitado.");
		}
	}
	
	public String obtenerPDFBase64(Long id) throws ServiceException{
		try {
			byte[] pdf = plantillaDao.obtenerPDFTemplate(id);
			if(pdf!=null){
				String encodedBase64 = new String(Base64.encodeBase64(pdf));
				return encodedBase64;
			}
			return StringUtils.EMPTY;
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PDF,"No se pudo obtener el PDF solicitado.");
		}
	}
	
	public Plantilla buscarPlantilla(List<Plantilla> lista, long codigo){
		if(lista==null) return null;
		for(Plantilla p : lista){
			if(p.getCdPlantilla() == codigo){
				return p;
			}
		}
		return null;
	}
	
	public List<Plantilla> obtenerPlantillasAsociadas(Long cdKit) throws ServiceException{
		List<Plantilla> listaPlantillas = new ArrayList<Plantilla>();
		try {
			listaPlantillas     = plantillaDao.getAllEntities();
			List<Plantilla> listaPlantillasAsoc = plantillaDao.obtenerPlantillasPorKit(cdKit);
			if(listaPlantillas!=null){
				for(Plantilla p : listaPlantillas){
					Plantilla asoc = buscarPlantilla(listaPlantillasAsoc,p.getCdPlantilla());
					if(asoc!=null){
						p.setAsociado(true);
					}else{
						p.setAsociado(false);
					}
				}
			}
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLAS,"No se pudo obtener las plantillas solicitadas.");
		}
		return listaPlantillas;		
	}

	@Override
	public void actualizarPDFdesdeArchivo(Long id, String filename) throws ServiceException {
		try {
			plantillaDao.actualizarPDF(id, filename);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_ACTUALIZAR_PDF,"No se pudo actualizar PDF.");
		}
	}
	
	@Override
	public void actualizarPDFdesdeCadena(Long id, String cadena)
			throws ServiceException {
		try {
			byte[] decodeBase64 = Base64.decodeBase64(cadena);
			plantillaDao.actualizarPDF(id, decodeBase64);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_ACTUALIZAR_PDF,"No se pudo actualizar PDF.");
		}
	}

	@Override
	public Long guardarPlantilla(Plantilla plantilla) throws ServiceException {
		try {
			Long id = plantillaDao.insertAndReturnId(plantilla);
			if(!StringUtils.isEmpty(plantilla.getLbPdfPlantilla())){
				actualizarPDFdesdeCadena(id, plantilla.getLbPdfPlantilla());
			}
			return id;
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_GUARDAR_PLANTILLA,"No se pudo guardar plantilla.");
		}
	}

	@Override
	public Integer actualizarPlantilla(Plantilla plantilla)
			throws ServiceException {
		try {
			int result = plantillaDao.update(plantilla);
			if(!StringUtils.isEmpty(plantilla.getLbPdfPlantilla())){
				actualizarPDFdesdeCadena(plantilla.getCdPlantilla(), plantilla.getLbPdfPlantilla());
			}
			return result;
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_ACTUALIZAR_PLANTILLA,"No se pudo actualizar plantilla.");
		}
	}

	@Override
	public Integer eliminarPlantilla(Long id) throws ServiceException {
		try {
			return plantillaDao.delete(id);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_ELIMINAR_PLANTILLA,"No se pudo eliminar plantilla.");
		}
	}

	@Override
	public List<Plantilla> obtenerPlantillasPorKit(Long cdKit) throws ServiceException {
		try {
			return plantillaDao.obtenerPlantillasPorKit(cdKit);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLA,"No se pudo obtener plantilla.");
		}
	}

	@Override
	public Plantilla obtenerPlantillaPorNombre(String nombre)
			throws ServiceException {
		try {
			return plantillaDao.obtenerPlantillasPorNombre(nombre);
		} catch (DaoException e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLAS,"No se pudo obtener plantillas.");
		}
	}

	@Override
	public List<Plantilla> obtenerPlantillasPorKitActivas(Long cdKit,Date fecHoraCreacion) throws ServiceException {
		try {
			if (fecHoraCreacion!=null){
				Timestamp fecFiltro = new Timestamp(fecHoraCreacion.getTime());
				return plantillaHistDao.obtenerPlantillasPorKitEstado(cdKit,Constants.ST_ACTIVO,fecFiltro);
			}else{

				return plantillaDao.obtenerPlantillasPorKitEstado(cdKit,Constants.ST_ACTIVO);
			}
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PLT_OBTENER_PLANTILLA,"No se pudo obtener plantilla.");
		}
	}
	
}
