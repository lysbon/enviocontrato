package com.bbva.cuentas.aat.service;

import java.util.List;

import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.service.exception.ServiceException;

public interface FlagService {
	
	public Flag obtenerFlagPorCodigo(Long id) throws ServiceException;

	public List<Flag> obtenerFlagsPorTipo(String tipo) throws ServiceException;
	
	public List<Flag> obtenerFlags() throws ServiceException;
	
	public Long guardarFlag(Flag flag) throws ServiceException;
	
	public Integer actualizarFlag(Flag flag) throws ServiceException;
	
	public Integer eliminarFlag(Long id) throws ServiceException;
	
}
