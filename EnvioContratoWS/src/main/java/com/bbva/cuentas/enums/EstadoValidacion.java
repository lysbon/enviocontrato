package com.bbva.cuentas.enums;

public enum EstadoValidacion {
	CORRECTO("OK"),
    FALTAFIRMAHUELLATITULAR("OB0001"),
    FALTAHUELLATITULAR("OB0002"),
    FALTAFIRMAFUNCIONARIO("OB0003"),
    FALTAFIRMATITULARFUNCIONARIO("OB0004"),
    FALTASELLOFUNCIONARIO("OB0005"),
    DOCUMENTONORECIBIDO("OB0006");
	private String codigo;
	EstadoValidacion(String codigo) {
        this.codigo = codigo;
    }
    public String value() {
        return codigo;
    }
}
