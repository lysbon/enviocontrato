package com.bbva.cuentas.enums;

public enum EstadoConciliacion {
	NOAPLICA("NA"),
    CONCILIADO("CNC");
	private String codigo;
	EstadoConciliacion(String codigo) {
        this.codigo = codigo;
    }
    public String value() {
        return codigo;
    }
}
