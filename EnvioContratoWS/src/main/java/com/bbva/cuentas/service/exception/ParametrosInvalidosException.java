package com.bbva.cuentas.service.exception;

import java.text.MessageFormat;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class ParametrosInvalidosException extends ServiceException{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final long serialVersionUID = 2834674696845011801L;
	private static final String MENSAJE = "Los parametros no son correctos. Revisar peticion.";
	private static final String FORMATO_LOG = MENSAJE + " ''{0}''.";
		
	public ParametrosInvalidosException(String mensaje) {
		super(CODIGO_PARAMETROS_INVALIDOS, MENSAJE);
		setPaso(ProcesoEnvioContrato.VALIDACION_TRAMA);
		logger.info(MessageFormat.format(FORMATO_LOG, mensaje));
	}
	
}
