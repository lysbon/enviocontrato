package com.bbva.cuentas.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bbva.cuentas.aat.service.DetTramaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Trama;

import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.AsyncService;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.ReprocesarService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

@Service
public class ReprocesarServiceImpl implements ReprocesarService {

	private static final Logger logger = org.apache.logging.log4j.LogManager
			.getLogger();

	@Autowired
	private AsyncService asyncService;
	@Autowired
	private CoreService coreService;
	@Autowired
	private KitCoreService kitCoreService;
	@Autowired
	private TramaService tramaService;
	@Autowired
	private DetTramaService detTramaService;

	@Value("${char.original}")
	private String charOriginal;
	@Value("${char.nuevo}")
	private String charNuevo;

//	public static List<ItemContrato> listaItems;

	
	public void process(String pTrama) throws ServiceException {
		List<Trama> listaTramas = null;
		try {
			listaTramas = tramaService.obtenerTramasReprocesar(Constants.ESTADO_PROCESO_SELECCIONADO);
			if(listaTramas!=null) {
				logger.info("[Num_Tramas_Reprocesar]: "+listaTramas.size());
				for(Trama trama : listaTramas) {
					String cadenatrama = trama.getChTrama();
					
				}
			}
		}catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion. " + e.getMessage());
		}
	}
	
//	public void process() throws ServiceException {
//		try {
//			
//			Kit kit=new Kit();
//			int estado = 0; // inicio =1 y fin=2
//			int estadoFormato = 1; // CAB=1 o PAR=2
//			
//			int index = 0;
//			Trama trama = null;
//
//			List<Cliente> listClientes = new ArrayList<Cliente>();
//			List<Cliente> listClientesProcesar = new ArrayList<Cliente>();
//
//			listaTramas = tramaService.obtenerTramasReprocesar(Constants.ESTADO_PROCESO_SELECCIONADO);
//			
//			logger.info("[Num_Tramas_Reprocesar]: "+listaTramas.size());
//			
//			for (int i = 0; i < listaTramas.size(); i++) {
//				
//				FirmaContrato firmaContrato = new FirmaContrato();
//				estadoFormato = 1; // CAB=1 o PAR=2
//				estado = 0;
//				index=0;
//				trama = null;
//				listClientes = new ArrayList<Cliente>();
//				listClientesProcesar = new ArrayList<Cliente>();
//				
//				String[] arryTrama = (listaTramas.get(i).getChTrama())
//						.split(Constants.TEXTO_EXTRACION_SEPARACION_TRAMA);
//
//				for (int j = 0; j < arryTrama.length; j++) {
//					String line = arryTrama[j].trim();
//
//					switch (estadoFormato) {
//					case 1:
//
//						if (line.equals(Constants.LABEL_CABECERA_INICIO)) {
//							estadoFormato = 1;
//							estado = 1;
//
//							listaItems = new ArrayList<ItemContrato>();
//							firmaContrato = new FirmaContrato();
//
//						} else {
//							if (line.equals(Constants.LABEL_CABECERA_FIN)) {
//								estado = 2;
//								estadoFormato = 2;
//
//								ItemContrato[] itemsContrato = new ItemContrato[listaItems
//										.size()];
//								listaItems.toArray(itemsContrato);
//
//								firmaContrato.setIdContrato(buscarParametro(
//										itemsContrato,
//										Constants.COD_PARAM_IDFORMATO));
//
//								firmaContrato
//										.setNumeroContrato(buscarParametro(
//												itemsContrato,
//												Constants.COD_PARAM_NROCONTRATO));
//
//								firmaContrato
//										.setOficinaGestora(buscarParametro(
//												itemsContrato,
//												Constants.COD_PARAM_OFIGESTOR));
//
//								firmaContrato.setProcedencia(buscarParametro(
//										itemsContrato,
//										Constants.COD_PARAM_CODPROCEDEN));
//
//								firmaContrato.setGestDoc(buscarParametro(
//										itemsContrato,
//										Constants.COD_PARAM_GESTDOC_FILEUNICO));
//
//								firmaContrato.setIdGrupo(buscarParametro(
//										itemsContrato,
//										Constants.COD_PARAM_GRUPO));
//								// imprimirParametria(itemsContrato);
//							} else {
//								if (estado == 1) {
//
//									processLineItem(line, index);
//									index++;
//
//								}
//							}
//						}
//						break;
//
//					case 2:
//
//						if (line.equals(Constants.LABEL_GENERAL_INICIO)) {
//							estadoFormato = 2;
//							estado = 1;
//
//							listaItems = new ArrayList<ItemContrato>();
//
//						} else {
//							if (line.equals(Constants.LABEL_GENERAL_FIN)) {
//								estado = 2;
//
//								estadoFormato = 3;
//
//								ItemContrato[] itemsContrato = new ItemContrato[listaItems
//										.size()];
//
//								listaItems.toArray(itemsContrato);
//
//								for (int t = 0; t < itemsContrato.length; t++) {
//									firmaContrato.getListaItems().add(
//											itemsContrato[t]);
//								}
//								// imprimirParametria(itemsContrato);
//							} else {
//								if (estado == 1) {
//
//									processLineItem(line, index);
//									index++;
//
//								}
//							}
//						}
//						break;
//
//					case 3:
//
//						if (line.equals(Constants.LABEL_PARTICIPE_INICIO)) {
//							estadoFormato = 3;
//							estado = 1;
//							listaItems = new ArrayList<ItemContrato>();
//						} else {
//							if (line.equals(Constants.LABEL_PARTICIPE_FIN)) {
//								estado = 2;
//
//								ItemContrato[] itemsContrato = new ItemContrato[listaItems
//										.size()];
//								listaItems.toArray(itemsContrato);
//
//								Cliente cliente = construirCliente1(itemsContrato);
//								for (int t = 0; t < itemsContrato.length; t++) {
//									cliente.getListaItems().add(
//											itemsContrato[t]);
//
//								}
//								listClientes.add(cliente);
//
//								// imprimirParametria(itemsContrato);
//							} else {
//								if (estado == 1) {
//
//									processLineItem(line, index);
//									index++;
//
//								}
//							}
//						}
//						break;
//					}
//
//				}
//
//				List<DetTrama> listDetTrama = detTramaService
//						.obtenerDetTramasReprocesar(
//								Constants.ESTADO_PROCESO_ERROR, listaTramas
//										.get(i).getIdTrama());
//
//				for (int t = 0; t < listClientes.size(); t++) {
//					firmaContrato.getListaClientes().add(listClientes.get(t));
//
//					for (int j = 0; j < listDetTrama.size(); j++) {
//						if (listClientes.get(t).getCodigoCentral()
//								.equals(listDetTrama.get(j).getCdCliente())) {
//
//							listClientesProcesar.add(listClientes.get(t));
//							break;
//						}
//					}
//				}
//
//				if (listClientesProcesar.size() == 0) {
//					listClientesProcesar.addAll(listClientes);
//				}
//
//				firmaContrato.getListaClientes().clear();
//				firmaContrato.getListaClientes().addAll(listClientesProcesar);
//				
//				logger.info("[Cod_Contrato]: "+firmaContrato.getIdContrato()+"- [Total clientes]: "+firmaContrato.getListaClientes().size());
//				
//				Date fecHoraCreacion= listaTramas.get(i).getFechaCreacion();
//				
//				logger.info("[Cod_Contrato]: "+firmaContrato.getIdContrato()+"- [Fecha_Hora_Trama]: "+fecHoraCreacion);
//				
//				kit = kitCoreService.obtenerKitHistPorGrupo(
//					  firmaContrato.getIdGrupo(),
//					  firmaContrato.getIdContrato(),
//					  fecHoraCreacion);
//			    
//				/*
//				kit = generarContratosService.obtenerKitPorGrupo(
//						  firmaContrato.getIdGrupo(),
//						  firmaContrato.getIdContrato());
//				*/
//				firmaContrato = validateFormatKit(firmaContrato, kit);
//
//				trama = listaTramas.get(i);
//				trama.setNumReproceso(trama.getNumReproceso() + 1);
//
//				kit.setFechaEjecucion(fecHoraCreacion);
//
//				if (Constants.ST_FALSE.equals(firmaContrato.getAsync())) {
//					
//					coreService.process((RequestDTO)firmaContrato, 
//										kit/*,
//										Constants.TIPO_REPROCESO_FIRMA_DIGITAL, 
//										trama,
//										listDetTrama*/);
//					
//				} else {
//					
//                	asyncService.processAsync((RequestDTO)firmaContrato, 
//                							  kit/*,
//                							  Constants.TIPO_REPROCESO_FIRMA_DIGITAL, 
//                							  trama,
//                							  listDetTrama*/);
//							
//				}
//			}
//
//		} catch (Exception e) {
//			logger.error(AppUtil.getDetailedException(e).toString());
//			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
//					"No se pudo procesar peticion. " + e.getMessage());
//		}
//	}

//	public static void processLineItem(String aLine, int index)
//			throws Exception {
//		if (aLine != null && aLine.length() > 0) {
//			if (aLine.length() > (Constants.LABEL_LENGTH - 1)) {
//				String label = aLine.substring(0, Constants.LABEL_LENGTH);
//				String value = aLine.substring(Constants.LABEL_LENGTH);
//				if (value != null && !value.equals("\\")) {
//					value = StringUtils.replace(value, "\\", "");
//					listaItems.add(Util.newIC(label, value));
//				}
//			} else {
//				System.out.println("la salida es xxx " + aLine);
//				throw new Exception(
//						"La linea debe tener longitud mayor de 5 caracteres. Linea "
//								+ (index + 1));
//			}
//		} else {
//			throw new Exception("La linea se encuentra vacia. Linea "
//					+ (index + 1));
//		}
//	}

	public FirmaContrato validateFormatKit(FirmaContrato firmaContrato, Kit kit)
			throws Exception {
		if (kit == null) {
			throw new Exception("No se encuentra kit con codigo "
					+ firmaContrato.getIdContrato());
		}
		if (Constants.ST_ACTIVO.equals(kit.getGrupo().getStHuella())) {
			if (Constants.ST_ACTIVO.equals(kit.getStHuella())) {
				if (Constants.ST_ACTIVO.equals(kit.getFlValHuella())) {
					for (Cliente cl : firmaContrato.getListaClientes()) {
						if (StringUtils.isEmpty(cl.getIdTrxHuellaDigital())) {
							throw new Exception(
									"Debe ingresar IdTrxHuellaDigital para cliente "
											+ cl.getCodigoCentral());
						}
					}
					firmaContrato = processIdTrxHuellaDigital(firmaContrato,
							charOriginal, charNuevo);
				}
			}
		}

		return firmaContrato;
	}

	public FirmaContrato processIdTrxHuellaDigital(
			FirmaContrato firmaContrato/* List<Cliente> clientes */,
			String charOriginal, String charNuevo) throws ServiceException {
		try {

			for (int i = 0; i < firmaContrato.getListaClientes().size(); i++) {
				firmaContrato
						.getListaClientes()
						.get(i)
						.setIdTrxHuellaDigital(
								Util.reemplazaCampos(firmaContrato
										.getListaClientes().get(i)
										.getIdTrxHuellaDigital(), charOriginal,
										charNuevo));

			}
			return firmaContrato;

		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion.");
		}
	}

	private static String buscarParametro(ItemContrato[] itemsContrato,
			String param) {
		for (int i = 0; i < itemsContrato.length; i++) {
			if (param.equals(itemsContrato[i].getLabel())) {
				return itemsContrato[i].getValue();
			}
		}
		return null;
	}

	private static Cliente construirCliente1(ItemContrato[] itemsContrato) {
		return construirCliente(itemsContrato, Constants.COD_PARAM_CODCEN_TIT1,
				Constants.COD_PARAM_TIPDOC_TIT1,
				Constants.COD_PARAM_NRODOC_TIT1,
				Constants.COD_PARAM_EMAIL_TIT1, Constants.COD_PARAM_TRX_HUELLA,
				Constants.COD_PARAM_NOMBREAPELIDO_TIT1);
	}

	private static Cliente construirCliente(ItemContrato[] itemsContrato,
			String paramCodCen, String paramTipDoc, String paramNroDoc,
			String paramEmail, String paramTrxHuella, String paramNombreApellido) {
		Cliente cliente = new Cliente();
		for (int i = 0; i < itemsContrato.length; i++) {
			if (paramCodCen.equals(itemsContrato[i].getLabel())) {
				cliente.setCodigoCentral(itemsContrato[i].getValue());
			} else if (paramTipDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setTipoDocumento(itemsContrato[i].getValue());
			} else if (paramNroDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setNroDocumento(itemsContrato[i].getValue());
			} else if (paramEmail.equals(itemsContrato[i].getLabel())) {
				cliente.setEmail(itemsContrato[i].getValue());
			} else if (paramTrxHuella.equals(itemsContrato[i].getLabel())) {
				cliente.setIdTrxHuellaDigital(itemsContrato[i].getValue());
			} else if (paramNombreApellido.equals(itemsContrato[i].getLabel())) {
				cliente.setNombreApellido(itemsContrato[i].getValue());
			}
		}
		if (cliente.getCodigoCentral() == null
				|| cliente.getCodigoCentral().length() == 0) {
			return null;
		}
		return cliente;
	}

	@Override
	public void process(Long from, Long to,TramaHist tramaHist) throws ServiceException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void process() throws ServiceException {

		// TODO Auto-generated method stub
		
	}

}
