package com.bbva.cuentas.service.worker;

import java.util.List;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.ItemSection;

public interface PDFWorker {
	
	public void process(Plantilla template, List<ItemSection> listaSections, ConfigurationPath cfg) throws ServiceException;

	public void processNuevo(Plantilla template, List<ItemSection> listaSections, ConfigurationPath cfg) throws ServiceException;
	
	public void reprocess(Plantilla template, List<ItemSection> listaSections, ConfigurationPath cfg) throws ServiceException;
	
	public void processStream(Plantilla template, List<ItemSection> listaSections,byte[] is, ConfigurationPath cfg) throws ServiceException;

}
