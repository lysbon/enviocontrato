package com.bbva.cuentas.service.exception;

import org.apache.commons.lang3.StringUtils;

public class BaseRuntimeException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
    private final String errorCode;
        
    public BaseRuntimeException(String errorCode) {
        this(errorCode,StringUtils.EMPTY);
    }
    
    public BaseRuntimeException(String errorCode,String message) {
        super(message);
    	this.errorCode = errorCode;
    }
    
    public BaseRuntimeException(String errorCode, Throwable cause) {
		super(cause);
		this.errorCode = errorCode;
	}
    
    public BaseRuntimeException(String errorCode,String message, Throwable cause) {
        super(message,cause);
    	this.errorCode = errorCode;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

}
