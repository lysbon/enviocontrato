package com.bbva.cuentas.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bbva.cuentas.service.exception.HuellaDigitalException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.ClienteHuellaDigital;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.HuellaDigitalService;

@Service
public class HuellaDigitalServiceImpl implements HuellaDigitalService{
	
	@Autowired
	CoreService coreService;

	@SuppressWarnings("unused")
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
		
	@Value("${huella.digital.endpoint.get}")
	private String endpointGET;
	@Value("${huella.digital.endpoint.post}")
	private String endpointPOST;
	
	public String formatoIdTrx(String trx){
		return StringUtils.replace(trx, "#", "-");
	}
	
	public ClienteHuellaDigital postClienteHuellaDigital(String tipoDocumento, String nroDocumento, String trx) throws HuellaDigitalException {
		RestTemplate restTemplate = new RestTemplate();
		JSONObject cliente = new JSONObject();
		JSONObject request = new JSONObject();
		cliente.put("tipoDocumento",tipoDocumento);
		cliente.put("numeroDocumento",nroDocumento);
		cliente.put("idTransaccion",formatoIdTrx(trx));
		request.put("cliente", cliente);
	
		try {
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(),headers);		
		return restTemplate.postForObject(endpointPOST,entity,ClienteHuellaDigital.class);
		
		} catch (Exception e) {
			throw new HuellaDigitalException(ErrorCodes.DESC_ERROR_HUELLA);
		}
	}
	
	public List<ClienteHuellaDigital> obtenerHuellasDigitales(List<Cliente> clientes) throws HuellaDigitalException {
		List<ClienteHuellaDigital> listaHuellas = new ArrayList<ClienteHuellaDigital>();
		
		    	for(Cliente cl : clientes){
		    		ClienteHuellaDigital huella = postClienteHuellaDigital(
		    				cl.getTipoDocumento(),
		    				cl.getNroDocumento(),
		    				cl.getIdTrxHuellaDigital());
		    			
		    		        listaHuellas.add(huella);
		    	}
		    	return listaHuellas;
    	
	     
	}
	
	public ClienteHuellaDigital obtenerHuellasDigitales(Cliente cl) throws ServiceException {
		return postClienteHuellaDigital(cl.getTipoDocumento(), cl.getNroDocumento(),cl.getIdTrxHuellaDigital());
	}

	@Override
	public Map<String,String> obtenerHuellasDigitalesBase64(List<Cliente> clientes,RequestDTO request,Kit kit, ClienteDTO titular,TramaHist  tramaHist) throws ServiceException {
						Map<String,String> huellasBase64 = new HashMap<String,String>();
				try {		
						List<ClienteHuellaDigital> clienteHuellas = obtenerHuellasDigitales(clientes);
						
						if (clienteHuellas!=null)
						{
										int indiceCliente=0;
										for(ClienteHuellaDigital huellaCliente : clienteHuellas){
											if(Constants.ID_DEDO_IZQ.equals(huellaCliente.getCliente().getIdDedo()) &&
												!StringUtils.isEmpty(huellaCliente.getCliente().getImgIndiceIzquierdo())){
												huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceIzquierdo());
												huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceIzquierdo());
											}else if(Constants.ID_DEDO_DER.equals(huellaCliente.getCliente().getIdDedo()) &&
													!StringUtils.isEmpty(huellaCliente.getCliente().getImgIndiceDerecho())){
												huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceDerecho());
												huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceDerecho());
											}else{
												if(huellaCliente.getCliente().getImgIndiceDerecho()!=null){
													huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente,  huellaCliente.getCliente().getImgIndiceDerecho());
													huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceDerecho());
												}else if(huellaCliente.getCliente().getImgIndiceIzquierdo()!=null){
													huellasBase64.put(Constants.PREFIX_HUELLA+indiceCliente, huellaCliente.getCliente().getImgIndiceIzquierdo());
													huellasBase64.put(Constants.PREFIX_MINUCIA+indiceCliente, huellaCliente.getCliente().getTemplateIndiceIzquierdo());
												}
											}
											indiceCliente++;
										}
						}		
						
				} catch (HuellaDigitalException e) {
					logger.error(ErrorCodes.DESC_ERROR_HUELLA, e);
					coreService.guardarTramaNivelGeneral(request, kit, titular, e, tramaHist);
					throw new ServiceException(ErrorCodes.S_ERROR_ITM_ACTUALIZAR_ITEM,"No se pudo actualizar item.");

				}
						
		return huellasBase64;
	}
	
}
