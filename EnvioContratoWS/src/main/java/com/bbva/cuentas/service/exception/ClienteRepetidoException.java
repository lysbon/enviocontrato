package com.bbva.cuentas.service.exception;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class ClienteRepetidoException extends ServiceException{
	
	private static final long serialVersionUID = 1659667236074434395L;

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final String MENSAJE = "El cliente(s) se encuentra repetido.";
	private static final String FORMATO_LOG = MENSAJE;
	
	public ClienteRepetidoException() {
		super(CODIGO_CLIENTES_REPETIDOS, MENSAJE);
		setPaso(ProcesoEnvioContrato.VALIDACION_TRAMA);
		logger.info(FORMATO_LOG);
	}
	
}
