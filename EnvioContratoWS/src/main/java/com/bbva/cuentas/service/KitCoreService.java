package com.bbva.cuentas.service;

import java.util.Date;
import java.util.List;

import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.service.exception.ServiceException;

public interface KitCoreService {

	public void cargarPlantillasContratoCliente(Kit kit) throws ServiceException;

	public void cargarPlantillasContratoCliente(Kit kit,List<Plantilla> plantillas) throws ServiceException;
	
	public Kit obtenerKitPorGrupo(String idGrupo,String idContrato) throws ServiceException;

    public Kit obtenerKitHistPorGrupo(String idGrupo,String idContrato,Date fechaCreacion) throws ServiceException;

	public void cargarPlantillasContratoCliente(Kit kit, String nombrePlantillaFormato) throws ServiceException;

}
