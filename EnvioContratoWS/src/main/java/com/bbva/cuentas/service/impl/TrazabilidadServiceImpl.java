package com.bbva.cuentas.service.impl;

import java.util.Date;
//import java.sql.Date;
import java.sql.Timestamp;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bbva.cuentas.aat.service.DetTramaHistService;
import com.bbva.cuentas.aat.service.TramaHistService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.DetTramaHist;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.TrazabilidadService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.CustomItemContratoDeserializer;
import com.bbva.cuentas.util.CustomItemContratoSerializer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Service
public class TrazabilidadServiceImpl implements TrazabilidadService {
	
    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private TramaHistService tramaHistService;
	
	@Autowired
	private DetTramaHistService detTramaHistService;
	
	public TramaHist guardaTramaHist(
			RequestDTO request, 
			Kit kit,
			String estado,
			Timestamp fechaProceso
			
			) {
		TramaHist tramaHist = new TramaHist();
		try {

			
			
			tramaHist.setEnv(kit.getGrupo().getCdEnv());
			tramaHist.setProcedencia(request.getProcedencia());
			tramaHist.setKit(kit.getCdKit());
			tramaHist.setContrato(request.getNumeroContrato());
			tramaHist.setEstado(estado);
			tramaHist.setRepro(Constants.RESULT_KO);
			tramaHist.setMasivo(Constants.RESULT_KO);
			if (request.getListaClientes().size()>1)
			{
			  tramaHist.setMasivo(Constants.RESULT_OK);	
			}
			tramaHist.setFhProIni(fechaProceso);
			tramaHist.setFhProFin(null);
			tramaHist.setTitCodCli(request.getListaClientes().get(0).getCodigoCentral());
			Long id = tramaHistService.insertAndReturnId(tramaHist);
			tramaHist.setIdTrama(id);

		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return tramaHist;
	}
	
	
	 public void updateTramaHist(TramaHist tramaHist,String estado,Timestamp fechaFinProceso) 
	 {

		try {

			tramaHist.setEstado(estado);

			tramaHist.setFhProFin(fechaFinProceso);

			tramaHistService.updateTramaHist(tramaHist);

		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
	}
	 
	public DetTramaHist guardaDetTramaHist(

			DetTrama detTrama, Timestamp fechaIniProceso,Timestamp fechaFinProceso,ServiceException error

	) {
		DetTramaHist detTramaHist = new DetTramaHist();
		try {

			detTramaHist.setIdTrama(detTrama.getIdTrama());
			detTramaHist.setChParCodCli(detTrama.getCdCliente());
			detTramaHist.setFhProIni(fechaIniProceso);
			detTramaHist.setFhProFin(fechaFinProceso);
			detTramaHist.setNroReproceso(detTrama.getNumReproceso());
			detTramaHist.setCdStep(detTrama.getCdStep());
			detTramaHist.setDetStep(detTrama.getDetStep());
			detTramaHist.setEstado(detTrama.getEstado());
			detTramaHistService.insert(detTramaHist);

		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return detTramaHist;
	}
	 
	 
	 public void updateDetTramaHist(DetTramaHist detTramaHist,String estado,Timestamp fechaFinProceso) 
	{

		try {

			detTramaHist.setEstado(estado);
			detTramaHist.setNroReproceso(0);
			detTramaHist.setCdStep(0);
			detTramaHist.setDetStep(null);
			detTramaHist.setFhProFin(fechaFinProceso);

		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
	}

}
