package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.ClienteHuellaDigital;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;

public interface HuellaDigitalService {
	
	public List<ClienteHuellaDigital> obtenerHuellasDigitales(List<Cliente> clientes) throws ServiceException;
	public ClienteHuellaDigital obtenerHuellasDigitales(Cliente cliente) throws ServiceException;
	public Map<String,String> obtenerHuellasDigitalesBase64(List<Cliente> clientes,RequestDTO request,Kit kit, ClienteDTO titular,TramaHist  tramaHist) throws ServiceException;
	
}
