package com.bbva.cuentas.service;


import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.service.exception.ServiceException;

public interface ReprocesarService {
	
	
    void process() throws ServiceException;
    void process(Long from,Long to,TramaHist tramaHist) throws ServiceException;


}