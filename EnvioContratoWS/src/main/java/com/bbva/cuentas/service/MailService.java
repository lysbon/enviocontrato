package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Adjunto;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public interface MailService {
	
	public void sendMailProc(
			FirmaContrato firmaContrato,
			Map<String,Object> parametrosCliente,
			Kit kit,
			ClienteDTO cliente) throws EnvioMailException;
	
	public void sendMailProc(
			Cliente cliente,
			Map<String,Object> parametrosCliente,
			Kit kit,
			List<Adjunto> adjuntos) throws EnvioMailException;
	
	public void sendMailProc(
			Cliente cliente,
			Map<String,Object> parametrosCliente, 
			Kit kit, 
			List<Adjunto> adjuntos,
			String plantillaCorreo,
			Integer idGrupoPlantilla) throws EnvioMailException;
	
	public void sendMailProc(
			Cliente cliente,
			Map<String,Object> parametrosCliente,
			String asunto,
			String plantillaCorreo,
			String motor,
			String remitente,
			String remitenteCC,
			List<Adjunto> adjuntos) throws EnvioMailException;
	
}
