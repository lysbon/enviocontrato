package com.bbva.cuentas.service.impl;

import java.util.ArrayList;


import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.sql.Timestamp;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;



import com.bbva.cuentas.aat.service.DetTramaService;
import com.bbva.cuentas.aat.service.ParametroService;
import com.bbva.cuentas.aat.service.TramaHistService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.DetTrama;

import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;

import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.HuellaDigitalService;
import com.bbva.cuentas.service.KitCoreService;

import com.bbva.cuentas.service.ReprocesoService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;


import com.bbva.cuentas.config.thread.AsyncConfiguration;


@Service("ReprocesoServiceImpl")
public class ReprocesoServiceImpl implements ReprocesoService{

	private static final Logger logger = org.apache.logging.log4j.LogManager
			.getLogger();

	@Autowired
	private TramaService tramaService;
	@Autowired
	private DetTramaService detTramaService;
	@Autowired
	private CoreService coreService;
	@Autowired
	protected KitCoreService kitService;
	@Autowired
	protected ErrorService backupService;
	@Autowired
	protected HuellaDigitalService huellaService;
	@Autowired
	protected Util util;
	@Autowired
	private TramaHistService tramaHistService;

	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private ErrorService errorService;
	
	@Autowired
	private AsyncConfiguration asyncConfiguration;

	
	public boolean tramaFallida(DetTrama detalle) {
		return StringUtils.isEmpty(detalle.getEstado()) || !detalle.getEstado().equals(Constants.ESTADO_PROCESO_REPROCESADO);
	}
	
	public void process() throws ServiceException {

		List<Trama> listaTramas = null;
		Parametro reproIntentos = null;
		Parametro reproCantidad = null;
		Parametro reproFechaDesde = null;
		Parametro reproFechaHasta = null;
		int numIteraciones = 0;
		int cantidadTramas = 0;
		int cont = 0;

		try {
			
			reproFechaDesde = parametroService.obtenerValorById(ParamConstants.PRM_FECHA_DESDE);
			reproIntentos = parametroService.obtenerValorById(ParamConstants.PRM_ID_MAXINTENTOS);
			reproCantidad = parametroService.obtenerValorById(ParamConstants.PRM_ID_CANTIDAD);
			reproFechaHasta = parametroService.obtenerValorById(ParamConstants.PRM_FECHA_HASTA);
			
			tramaService.updateTramaporEstado(Constants.ESTADO_PROCESO_SELECCIONADO, Constants.ESTADO_PROCESO_ERROR,reproFechaDesde.getValorParam(),reproFechaHasta.getValorParam(),reproIntentos.getValorParam());
			cantidadTramas = tramaService.contarTramasPorEstado(Constants.ESTADO_PROCESO_SELECCIONADO,
					reproIntentos.getValorParam());

			numIteraciones = Util.obtenerIteraciones(cantidadTramas, Integer.parseInt(reproCantidad.getValorParam()));

			while (cont < numIteraciones) {
				cont++;

				listaTramas = tramaService.obtenerTramasReprocesar(Constants.ESTADO_PROCESO_SELECCIONADO,
						reproIntentos.getValorParam(), reproCantidad.getValorParam());
				logger.info("[num_tramas_reprocesar]: " + listaTramas.size());
				iteraTrama(listaTramas);

				Thread.sleep(10000);
			}

		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion. " + e.getMessage());
		}

	}
	
	
	
	
	public void iteraTrama(List<Trama> listaTramas) throws ServiceException {

		for (Trama trama : listaTramas) {
			trama.setEstado(Constants.ESTADO_PROCESO_PENDIENTE);
			tramaService.actualizarTrama(trama);
			RequestDTO peticion = parse(trama.getChTrama());
			List<DetTrama> detalleCliente = detTramaService.obtenerDetTramasReprocesar(Constants.ESTADO_PROCESO_ERROR,
					trama.getIdTrama());

			reprocess(peticion, detalleCliente, trama);
		}

	}		
	
	public void iteraDetTrama(List<DetTrama> detalleCliente, Map<String, List<DetTrama>> clienteTramas,
			RequestDTO peticion) {

		for (DetTrama detalle : detalleCliente) {
			if (tramaFallida(detalle)) {
				if (!StringUtils.isEmpty(detalle.getCdCliente())) {
					if (clienteTramas.containsKey(detalle.getCdCliente())) {
						clienteTramas.get(detalle.getCdCliente()).add(detalle);
					} else {
						List<DetTrama> pasosCliente = new ArrayList<DetTrama>();
						pasosCliente.add(detalle);
						clienteTramas.put(detalle.getCdCliente(), pasosCliente);
					}
				} else {

					iteraClientesTrama(peticion.getListaClientes(), clienteTramas);

				}
			}
		}
	}
	
	public void iteraClientesTrama(List<Cliente> clientesTrama, Map<String, List<DetTrama>> clienteTramas) {
				for (Cliente cliente : clientesTrama) {
				List<DetTrama> pasosCliente = new ArrayList<DetTrama>();
				DetTrama detTrama = new DetTrama();
				detTrama.setCdStep(ProcesoEnvioContrato.GENERICO.getPaso());
				pasosCliente.add(detTrama);
				clienteTramas.put(cliente.getCodigoCentral(), pasosCliente);
			   }
	}	
	
	@Async("asyncExecutor")
	public void reprocess(RequestDTO peticion,List<DetTrama> detTrama,Trama trama) throws ServiceException{
		if(detTrama!=null ) {
			
			Kit kit = kitService.obtenerKitPorGrupo(peticion.getIdGrupo(),peticion.getIdContrato());
			kit.setTrama(trama);
			processexecute(peticion,kit,detTrama,trama);
		}
	}
	
	public RequestDTO parse(String trama) throws ServiceException{
		return backupService.getRequestJson(trama);
	}
	
	public List<ClienteDTO> seleccionClientesReproceso(List<DetTrama> detTrama,List<ClienteDTO> plantillasCliente)
	{

		List<ClienteDTO> plantillasClienteResul = new ArrayList<ClienteDTO>();

		if (detTrama.isEmpty()) {
			plantillasClienteResul.addAll(plantillasCliente);
		} else {
			for (DetTrama oDetTrama : detTrama) {

				for (ClienteDTO detPlantillaCliente : plantillasCliente) {

					if (detPlantillaCliente.getCliente().getCodigoCentral().equals(oDetTrama.getCdCliente())) {
						detPlantillaCliente = procesosFileEmailDetTrama(detPlantillaCliente, oDetTrama);
						detPlantillaCliente.setDetTrama(oDetTrama);
						plantillasClienteResul.add(detPlantillaCliente);
					}
				}
			}
		}

		return plantillasClienteResul;

	}

   public ClienteDTO procesosFileEmailDetTrama(ClienteDTO detPlantillaCliente,DetTrama paso)
	{

		if (paso.getCdStep().intValue() == ProcesoEnvioContrato.TRAMA_INICIAL.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.VALIDACION_TRAMA.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.OBTENER_HUELLA_DIGITAL.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_PDFS_GENERAL.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_PDFS_CLIENTE.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_CLIENTE.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERACION_RUTAS.getPaso()
				|| paso.getCdStep().intValue() == ProcesoEnvioContrato.GENERICO.getPaso()) {
			detPlantillaCliente.setProcesarFileUnico(true);
			detPlantillaCliente.setProcesarMail(true);
		} else if (paso.getCdStep().intValue() == ProcesoEnvioContrato.ENVIO_EMAIL_CLIENTE.getPaso()) {
			detPlantillaCliente.setProcesarMail(true);
		} else if (paso.getCdStep().intValue() == ProcesoEnvioContrato.ENVIO_FILEUNICO_CLIENTE.getPaso()) {
			detPlantillaCliente.setProcesarFileUnico(true);
		}
		return detPlantillaCliente;

	}
	
	
	public void processexecute(RequestDTO request, Kit kit, List<DetTrama> detTrama, Trama trama)
			throws ServiceException {

		List<ClienteDTO> plantillasCliente = null;

		TramaHist tramaHist = new TramaHist();

		ClienteDTO titular = null;
		String estadoProceso = Constants.ESTADO_PROCESO_REPROCESADO;
		try {

			//tramaHist = tramaHistService.obtenerTramaHistPoridTrama(trama.getIdTrama());
			tramaHist = tramaHistService.obtenerTramaHistPoridTrama(kit.getTrama().getIdTrama());

			if (kit != null && request.getListaClientes() != null) {
				Map<String, Object> parametros = util.toMap(request);

				if (coreService.huellaActivo(kit)) {
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes(), request,
							kit, titular, tramaHist));
				}

				coreService.procesoGeneralContratos(request, kit, parametros, tramaHist,detTrama);

			} else {
				estadoProceso = Constants.ESTADO_PROCESO_ERROR;
			}

		} catch (Exception e) {
			logger.error(e);
			estadoProceso=Constants.ESTADO_PROCESO_ERROR;
			

		} finally {
			kit.getTrama().setEstado(estadoProceso);
			//trama.setEstado(estadoProceso);
			
			actualizaTrazabilidad(kit.getTrama(), tramaHist);
			//coreService.depurarArchivos(plantillasCliente);
			
		}
	}

	@Override
	public void processfrom(Long from, Long to,TramaHist tramaHist) throws ServiceException {
		for(Long i=from;0<to;i++) {
			Trama trama = tramaService.obtenerTramasPorId(i);
			if(Constants.ESTADO_PROCESO_ERROR.equals(trama.getEstado())) {
				trama.setEstado(Constants.ESTADO_PROCESO_SELECCIONADO);
				tramaService.actualizarTrama(trama);
			}
		}
		process();

	}
	
	@Override
	public void actualizaTrazabilidad(Trama trama,TramaHist tramaHist) throws ServiceException {
		 
		

		if (trama.getNumReproceso() == null) {
			trama.setNumReproceso(1);
		} else {
			trama.setNumReproceso(trama.getNumReproceso() + 1);
		}

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		trama.setTimeReproceso(timestamp);
		tramaService.actualizarTrama(trama);
		
		if (tramaHist!=null)
		{	
			tramaHist.setEstado(trama.getEstado());
			tramaHist.setRepro(Constants.RESULT_OK);
			tramaHistService.updateTramaHist(tramaHist);
		}

	}
	
}
