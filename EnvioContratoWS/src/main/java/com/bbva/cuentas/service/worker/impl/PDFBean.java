package com.bbva.cuentas.service.worker.impl;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.ErrorCodes;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFBean {

	private PdfReader  reader;
	private PdfStamper stamper;
	private Document   document;
	private PdfWriter  writer;
	
	public PDFBean(){
		reader   = null;
		stamper  = null;
		document = null;
		writer   = null;
	}
	
	public PdfReader getReader() {
		return reader;
	}
	public void setReader(PdfReader reader) {
		this.reader = reader;
	}
	public PdfStamper getStamper() {
		return stamper;
	}
	public void setStamper(PdfStamper stamper) {
		this.stamper = stamper;
	}
	public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}
	public PdfWriter getWriter() {
		return writer;
	}
	public void setWriter(PdfWriter writer) {
		this.writer = writer;
	}
	
	public void finish() throws DocumentException, IOException{
		if(document!=null && document.isOpen()){
			document.close();
		}else{
			stamper.close();
			reader.close();
		}
	}
	
	public void prepare(byte[] is, String rutaFormatoOut) throws Exception{
		reader = new PdfReader(is);
		stamper = new PdfStamper(reader, new FileOutputStream(rutaFormatoOut));
		Map<String, String> info = reader.getInfo();
	    stamper.setMoreInfo(info);
	}
	
	public void prepare(String rutaFormatoOut) throws FileNotFoundException, DocumentException{
		document = new Document();
		writer = PdfWriter.getInstance(document, new FileOutputStream(rutaFormatoOut));
		document.open();
	}
	
	public void prepare(String rutaFormato,String rutaFormatoOut) throws Exception{		
		reader = new PdfReader(rutaFormato);
		stamper = new PdfStamper(reader, new FileOutputStream(rutaFormatoOut));
		Map<String, String> info = reader.getInfo();
	    stamper.setMoreInfo(info);
	}
	
	public ByteArrayOutputStream prepareOnMemory(String rutaFormato) throws Exception{
		reader = new PdfReader(rutaFormato);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		stamper = new PdfStamper(reader, out);
		Map<String, String> info = reader.getInfo();
	    stamper.setMoreInfo(info);
	    return out;
	}
	
	public PdfContentByte crearContenidoAbsoluto(int nuPagina) throws ServiceException{
		if(document!=null && document.isOpen()){
			return writer.getDirectContent();
		}else{
			if(reader!=null){
				if(nuPagina>reader.getNumberOfPages()){
					throw new ServiceException(ErrorCodes.W_ERROR_WORKER_PDF,"La seccion se encuentra configurada fuera del tamaño de paginas("+nuPagina+")");
				}	
			}
			return stamper.getOverContent(nuPagina);
		}
	}
	
}
