package com.bbva.cuentas.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.core.io.Resource;

import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.FileUnicoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Adjunto;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.service.MailService;
import com.bbva.cuentas.service.ResourceAwareService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

import org.stringtemplate.v4.*;

@Service
public class MailServiceImpl implements MailService{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${bbva.mail.host}")
	private String host;
	@Value("${bbva.mail.smtp.auth}")
	private String auth;
	@Value("${bbva.mail.debug}")
	private String debug;
	@Value("${bbva.mail.from.user}")
	private String user;
	@Value("${bbva.mail.from.pass}")
	private String pass;
	
	@Value("${flag.mail.template.db}")
	private boolean flagTemplateDB;
	
	@Autowired
	private KitService kitService;
	@Autowired
	private ResourceAwareService resourceService;
	@Autowired
	private FlagService flagService;
	
	private Properties maskProperties;
	
	@PostConstruct
	public void init(){
		try {
			logger.debug("MailService.init() - Cargando mascaras");
			Resource resource = resourceService.getResource("classpath:mask.properties");
			maskProperties = new Properties();
			maskProperties.load(resource.getInputStream());
		} catch (IOException e) {
			logger.error("Error al obtener archivo desde classpath.",e);
		}
	}
	
	public void sendMail(
			String toAddress, 
			String asunto,
			String outputTemplate, 
			String remitente,
			List<Adjunto> adjuntos) throws Exception {
        Properties props = System.getProperties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", auth);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", debug);
        Session session = Session.getInstance(props, null);
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(remitente));
        message.setRecipients(Message.RecipientType.TO, toAddress);
        
        message.setSubject(asunto,"UTF-8");
        
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(outputTemplate, "text/html; charset=utf-8");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        
        if(adjuntos!=null){
        	for(Adjunto adjunto : adjuntos){
            	addAttachment(multipart,adjunto.getRuta(),adjunto.getNombreCorreo()+".pdf");
            }
        }
        
        try {
			addInlineAttachment(multipart, "image1.jpg");
			addInlineAttachment(multipart, "image2.jpg");
        } catch (Exception e) {
        	logger.error("Error en MailService.addInlineAttachment.",e);
        	throw e;
		}
        
        message.setContent(multipart);
        
        try {
        	Transport tr = session.getTransport("smtp");
            tr.connect(host, user, pass);
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
        } catch (SendFailedException sfe) {
        	logger.error("Error en MailService.sendMail.",sfe);
        	throw sfe;
        }
    }

	protected void addInlineAttachment(Multipart multipart, String name) throws MessagingException{
        DataSource source = new FileDataSource(getClassPathFile(name));
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setHeader("Content-ID", "<"+name+">");
        messageBodyPart.setHeader("Content-Type", "image/jpeg");
        messageBodyPart.setHeader("Content-Transfer-Encoding", "base64");
        messageBodyPart.setDisposition(Part.INLINE);
        multipart.addBodyPart(messageBodyPart);
    }
    
	private File getClassPathFile(String name) {
		try {
			Resource resource = resourceService.getResource("classpath:"+name);
			logger.debug("Cargando archivo "+resource.getFilename());
			return resource.getFile();
		} catch (IOException e) {
			logger.error("Error al obtener archivo desde classpath.",e);
		}
		return null;
	}
    
    protected void addAttachment(Multipart multipart, String filename, String shortName) throws MessagingException{
    	final MimeBodyPart attachmentPart = new MimeBodyPart();
    	try {
    		File f = new File(filename);
    		if(f.exists() && !f.isDirectory()) { 
    			final DataSource source = new FileDataSource(filename);
        	    attachmentPart.setDataHandler(new DataHandler(source));
        	    attachmentPart.setFileName(MimeUtility.encodeText(shortName, "UTF-8", null));
        	    attachmentPart.setDisposition(Part.ATTACHMENT);
        	    attachmentPart.setHeader("Content-Transfer-Encoding", "base64");
        	    multipart.addBodyPart(attachmentPart);
    		}else{
    			logger.error("Archivo adjunto no existe. "+filename);
    		}
    	} catch (final Exception e) {
    	    logger.error("Exception: " + e.getMessage());
    	}
    }
    
    public String leerPlantilla(String nombrePlantilla){
		BufferedReader reader = null;
		String result = null;
		try {
			InputStream in = this.getClass().getClassLoader()
                    .getResourceAsStream(nombrePlantilla);
			reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder out = new StringBuilder();
		    String line;
		    while ((line = reader.readLine()) != null) {
		        out.append(line);
		    }
		    result = out.toString();
		} catch (IOException e) {
			logger.error("Error al leer plantilla de correo. "+e);
		} finally {
			try {
				if(reader!=null) reader.close();
			} catch (IOException ex) {
				logger.error("Error al cerrar plantilla de correo. "+ex);
			}
		}
		return result;
	}
    
    public ItemContrato buscarItem(List<ItemContrato> items,String nombre){
    	for(ItemContrato item : items){
			if(nombre.equals(item.getLabel())){
				return item; 
			}
		}
    	return null;
    }
    
    public String procesarPlantillaLangs3(
    		String plantilla,
    		Map<String,Object> parametrosCliente,
    		boolean escape){
    	Map<String, String> valuesMap = new HashMap<String, String>();
    	for (Map.Entry<String, Object> entry : parametrosCliente.entrySet()){
			String value = entry.getValue().toString();
			if(maskProperties.containsKey(entry.getKey())){
				try {
					value = Util.formatString(value,maskProperties.getProperty(entry.getKey()));
				} catch (ParseException e) {
					logger.error(e);
				}
			}
			if(escape){
				value = StringEscapeUtils.escapeHtml4(value);
			}
			valuesMap.put(entry.getKey(),value);
		}
		StrSubstitutor sub = new StrSubstitutor(valuesMap);
		return sub.replace(plantilla);
    }
    
    public String procesarPlantillaStrTpl(
    		String plantilla,
    		Map<String,Object> parametrosCliente,
    		List<Flag> listFlags,
    		boolean escape){
		ST st = new ST(plantilla);
		if(listFlags!=null){
			for(Flag flag:listFlags){
				if(parametrosCliente.containsKey(flag.getNbCondicionNombre()) &&
						parametrosCliente.get(flag.getNbCondicionNombre()).equals(flag.getNbCondicionValor())){
					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),true);
				}else{
					st.add(Constants.PREFIX_FLAG+StringUtils.replace(flag.getNbCondicionNombre(),"|",""),false);
				}
			}
		}
		
		for (Map.Entry<String, Object> entry : parametrosCliente.entrySet()){
			String value = StringUtils.EMPTY;
			if(entry.getValue()!=null) {
				value = entry.getValue().toString();
			} 
			if(maskProperties.containsKey(entry.getKey())){
				try {
					value = Util.formatString(entry.getValue().toString(),maskProperties.getProperty(entry.getKey()));
				} catch (ParseException e) {
					logger.error(e);
				}
			}
			if(escape){
				value = StringEscapeUtils.escapeHtml4(value);
			}
			st.add(StringUtils.replace(entry.getKey(),"|",""),value);
		}
		return st.render();
	}

    public void sendMailProc(
			Cliente cliente,
			Map<String,Object> parametrosCliente,
			String asunto,
			String plantillaCorreo,
			String motor,
			String remitente,
			String remitenteCC,
			List<Adjunto> adjuntos) throws EnvioMailException{
    	logger.info("[ini]");
		String outputCorreo = null;
		String outputAsunto = null;
		try {
			if(Constants.ENGINE_STR_TPL.equals(motor)){
				List<Flag> listFlags = flagService.obtenerFlagsPorTipo(Constants.FLAG_TIPO_EMAIL);
				outputCorreo   = procesarPlantillaStrTpl(plantillaCorreo, parametrosCliente, listFlags, true);
				outputAsunto   = procesarPlantillaStrTpl(asunto         , parametrosCliente, listFlags, false);
			}else{
				outputCorreo = procesarPlantillaLangs3(plantillaCorreo, parametrosCliente, true);
				outputAsunto = procesarPlantillaLangs3(asunto         , parametrosCliente, false);
			}	
			
			sendMail(cliente.getEmail(),outputAsunto,outputCorreo,remitente,adjuntos);
			if(!StringUtils.isEmpty(remitenteCC)){
				sendMail(remitenteCC,outputAsunto,outputCorreo,remitente,adjuntos);
			}
		} catch (MessagingException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new EnvioMailException(e.getMessage());
		} catch (Exception e){
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new EnvioMailException(e.getMessage());
		}
	}
    
    public void sendMailProc(
    		Cliente cliente,
    		Map<String,Object> parametrosCliente,
			Kit kit,
			List<Adjunto> adjuntos) throws EnvioMailException{
    	try {
    		String plantillaCorreo = null;
    		if(flagTemplateDB){
    			plantillaCorreo = kitService.obtenerMailTemplate(kit.getCdKit(),kit.getGrupo().getCdEnv());
    		}else{
    			plantillaCorreo = leerPlantilla(kit.getCdCodigo());
    		}
    		sendMailProc(cliente,
    				parametrosCliente,
        			kit.getGrupoKit().getNbCorreoAsunto(),
        			plantillaCorreo,
        			kit.getGrupoKit().getNbCorreoEngine(),
        			kit.getGrupoKit().getNbRemitente(),
        			kit.getGrupoKit().getNbRemitenteCC(),
        			adjuntos);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new EnvioMailException(e.getMessage());
		}
    }
    
    public void sendMailProc(
    		Cliente cliente,
    		Map<String,Object> parametrosCliente,
			Kit kit,
			List<Adjunto> adjuntos,
			String codigoPlantilla,
			Integer idGrupoPlantilla) throws EnvioMailException{
    	String plantillaCorreo = null;
    	Kit kitPlantilla = null;
    	try {
    		kitPlantilla = kitService.obtenerKitPorCodigo(idGrupoPlantilla, codigoPlantilla);
    		if(flagTemplateDB){
    			plantillaCorreo = kitService.obtenerMailTemplate(kitPlantilla.getCdKit(),kitPlantilla.getGrupo().getCdEnv());
    		}else{
    			plantillaCorreo = leerPlantilla(kitPlantilla.getCdCodigo());
    		}
    		sendMailProc(cliente,
    				parametrosCliente,
        			kitPlantilla.getGrupoKit().getNbCorreoAsunto(),
        			plantillaCorreo,
        			kitPlantilla.getGrupoKit().getNbCorreoEngine(),
        			kitPlantilla.getGrupoKit().getNbRemitente(),
        			kitPlantilla.getGrupoKit().getNbRemitenteCC(),
        			adjuntos);
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new EnvioMailException(e.getMessage());
		}
    }

    public List<Adjunto> obtenerAdjuntos(Map<Long, List<ConfigurationPath>> cfg){
    	List<Adjunto> adjuntos = new ArrayList<Adjunto>();
    	Map<Long, List<ConfigurationPath>> map = cfg;
		for (Map.Entry<Long, List<ConfigurationPath>> entry : map.entrySet()){
			List<ConfigurationPath> configuracionesPlantilla = cfg.get(entry.getKey());
			for(ConfigurationPath configuracionPlantilla : configuracionesPlantilla) {
				if(Constants.ST_ACTIVO.equals(configuracionPlantilla.getStEnvCorreo())){
					Adjunto adjunto = new Adjunto();
	    			adjunto.setNombreCorreo(configuracionPlantilla.getNbNombreCorreo());
	    			if(configuracionesPlantilla.size()>1) {
	    				adjunto.setNombreCorreo(configuracionPlantilla.getNumeroContrato()+"_"+configuracionPlantilla.getNbNombreCorreo());
	    			}
	    			if(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
	    				adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
	    			}else if(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_CLIENTE.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
	    				adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
	    			}else if(ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
	    				adjunto.setRuta(configuracionPlantilla.getRutaSalidaPDF());
	    			}else if(ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE.getPaso().intValue() == 
	    					configuracionPlantilla.getStep().intValue()) {
	    				adjunto.setRuta(configuracionPlantilla.getRutaSalidaPDF());
	    			}
	    			if(!StringUtils.isEmpty(adjunto.getRuta())) {
	    				adjuntos.add(adjunto);
	    			}
				}
			}
		}
		return adjuntos;
    }
    
    
	public List<Adjunto> obtenerAdjuntos(Kit kit,  /*Map<Long,ConfigurationPath> cfg,*/
			Map<Long, List<ConfigurationPath>> cfg,List<ConfigurationPath> listCfgEmail) {


		List<Adjunto> adjuntos = new ArrayList<Adjunto>();
		

		for(ConfigurationPath configuracionPlantilla : listCfgEmail){	

		
			if(Constants.ST_ACTIVO.equals(configuracionPlantilla.getStEnvCorreo())){
				Adjunto adjunto = new Adjunto();
				adjunto.setCdPlantilla(configuracionPlantilla.getCdPlantilla());
				adjunto.setNombreCorreo(configuracionPlantilla.getNbNombreCorreo());
				//if(configuracionesPlantilla.size()>1) {
				//	adjunto.setNombreCorreo(configuracionPlantilla.getNumeroContrato()+"_"+configuracionPlantilla.getNbNombreCorreo());
				//}
				if(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()) {
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
				}else if(ProcesoEnvioContrato.GENERACION_PDFS_GENERAL.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()){
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaPDF());
				}else if(ProcesoEnvioContrato.GENERACION_PDFS_CLIENTE.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()){
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaPDF());
				}else if(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_CLIENTE.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()){
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
				}else if(ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()){
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
				}else if(ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL.getPaso().intValue() == 
						configuracionPlantilla.getStep().intValue()){
					adjunto.setRuta(configuracionPlantilla.getRutaSalidaFirma());
				}
				if(!StringUtils.isEmpty(adjunto.getRuta())) {
					adjuntos.add(adjunto);
				}
			}
		}	
		
		return adjuntos;


	}
	
	
	public boolean validaAdjunto(List<Adjunto> adjuntos,ConfigurationPath configuracionPlantilla) {
    	
		  boolean resp=false;
		  
		   		if(Constants.ST_ACTIVO.equals(configuracionPlantilla.getStEnvCorreo())){
		   			
		   			for(Adjunto adjunto : adjuntos){
		   				 
		   				if (adjunto.getCdPlantilla().equals(configuracionPlantilla.getCdPlantilla()))
		   				{
		   					if (configuracionPlantilla.getChTipoPlantilla().equals(Constants.TIPO_PLANTILLA_PERSONAL))
		   					{
		   						resp=true;
		   						break;
		   					}
		   					
		   				}
		   				
		   			}
					
				}
		
		  return resp;
	}
	
    
    public void sendMailProc(
			FirmaContrato firmaContrato,
			Map<String,Object> parametrosCliente,
			Kit kit,
			ClienteDTO cliente) throws EnvioMailException{
    	try {
    		if(StringUtils.isEmpty(firmaContrato.getPlantillaCorreo())){
    			sendMailProc(cliente.getCliente(),parametrosCliente,kit,obtenerAdjuntos(kit,cliente.getListCfg(),cliente.getListCfgEmail()));
    		}else{
    			sendMailProc(cliente.getCliente(),parametrosCliente,kit,obtenerAdjuntos(kit,cliente.getListCfg(),cliente.getListCfgEmail()),
    					firmaContrato.getPlantillaCorreo(),firmaContrato.getGrupoCorreo());
    		}
    		
             }catch (Exception e) {
    			logger.error(AppUtil.getDetailedException(e).toString());
    			throw new EnvioMailException(e.getMessage());
    		}
    	
    }
       
}
