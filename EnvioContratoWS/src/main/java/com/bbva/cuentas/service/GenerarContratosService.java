package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public interface GenerarContratosService {

	public void generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ClienteDTO cliente) throws ServiceException;
	
	public boolean generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ClienteDTO cliente,
    		int paso) throws ServiceException ;
    
    

    
    public Plantilla generarContrato(
    		
    		Plantilla plantilla,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ConfigurationPath cfg, int paso) throws ServiceException;

    public List<Documento> generarDocumentosBase64(
			List<Plantilla> plantillas,
			FirmaContrato firmaContrato,
			ClienteDTO cliente,
			List<ClienteDTO> clientes,
			Map<String,Object> parametros,
			List<Map<String,?>> contenido) throws ServiceException;
	
	public boolean evaluarPlantilla(Plantilla template,Map<String,Object> parametros) throws DaoException, ServiceException;
	

}