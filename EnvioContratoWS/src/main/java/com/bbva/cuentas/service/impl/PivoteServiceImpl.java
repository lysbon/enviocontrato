package com.bbva.cuentas.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bbva.cuentas.service.exception.ClienteRepetidoException;
import com.bbva.cuentas.service.exception.ParametrosInvalidosException;
import com.bbva.cuentas.service.exception.PlantillaNoEncuentraException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.AsyncService;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.PivoteService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

@Service
public class PivoteServiceImpl implements PivoteService{
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private AsyncService asyncService;
	@Autowired
	private CoreService coreService;
	@Autowired
	private KitCoreService kitService;
	
	@Value("${char.original}")
	private String charOriginal;
	@Value("${char.nuevo}")
	private String charNuevo;
	
	public void validateRequest(FirmaContrato firmaContrato) throws ServiceException{
		if(firmaContrato==null){
			throw new ParametrosInvalidosException("La peticion se encuentra vacia");
		}
		if(StringUtils.isEmpty(firmaContrato.getIdContrato())){
			throw new ParametrosInvalidosException("El IdContrato se encuentra vacio");
		}
		if(firmaContrato.getListaClientes()==null ||firmaContrato.getListaClientes().isEmpty()){
			throw new ParametrosInvalidosException("La lista de clientes se encuentra vacia");
		}
		if(!validateClientRepeated(firmaContrato.getListaClientes())){
			throw new ClienteRepetidoException();
		}
	}
	
	public boolean validateClientRepeated(List<Cliente> clientes){
		Map<String,String> record = new HashMap<String, String>();
		for(Cliente cliente:clientes){
			if(record.containsKey(cliente.getCodigoCentral())){
				return false;
			}
			record.put(cliente.getCodigoCentral(), cliente.getCodigoCentral());
		}
		return true;
	}
	
	public void validateKit(FirmaContrato firmaContrato, Kit kit) throws ServiceException {
		if (kit == null) {
			throw new PlantillaNoEncuentraException(firmaContrato.getIdContrato());
		}
		if (Constants.ST_ACTIVO.equals(kit.getFlValHuella())){
			for (Cliente cl : firmaContrato.getListaClientes()) {
				if (StringUtils.isEmpty(cl.getIdTrxHuellaDigital())) {
					throw new ParametrosInvalidosException("Debe ingresar IdTrxHuellaDigital para cliente " + cl.getCodigoCentral());
				}
			}
		}
	}
	
	public FirmaContrato validateFormatKit(FirmaContrato firmaContrato, Kit kit) throws ServiceException {
		if (kit == null) {
			throw new PlantillaNoEncuentraException(firmaContrato.getIdContrato());
		}
		if (Constants.ST_ACTIVO.equals(kit.getGrupo().getStHuella()) &&
			Constants.ST_ACTIVO.equals(kit.getStHuella()) &&
			Constants.ST_ACTIVO.equals(kit.getFlValHuella())) {
			for (Cliente cl : firmaContrato.getListaClientes()) {
				if (StringUtils.isEmpty(cl.getIdTrxHuellaDigital())) {
					throw new ParametrosInvalidosException(
								"Debe ingresar IdTrxHuellaDigital para cliente " + cl.getCodigoCentral());
				}
				}
			firmaContrato = processIdTrxHuellaDigital(firmaContrato, charOriginal, charNuevo);
		}
		return firmaContrato;
	}
	
	public void process(RequestDTO request) throws ServiceException {
		try {
			
			validateRequest(request);

			Kit kit = kitService.obtenerKitPorGrupo(
					request.getIdGrupo(), request.getIdContrato());

			validateFormatKit(request, kit);

			if (Constants.ST_FALSE.equals(request.getAsync())) {
				coreService.process(request, kit);
			} else {
				asyncService.processAsync(request, kit);
			}

		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion. " + e.getMessage());
		}
	}
	
	public Documentos processKitBase64(RequestDTO request) throws ServiceException {
		validateRequest(request);
		return coreService.processPreviewKit(request);
	}
	
	public Documentos processPlantillaBase64(RequestDTO request) throws ServiceException {
		validateRequest(request);
		return coreService.processPreviewPlantilla(request);
	}
	
	public FirmaContrato processIdTrxHuellaDigital(FirmaContrato firmaContrato, String charOriginal, String charNuevo) throws ServiceException {
		try {
			for (int i = 0; i < firmaContrato.getListaClientes().size(); i++) {
				firmaContrato.getListaClientes().get(i).setIdTrxHuellaDigital(Util.reemplazaCampos(
						firmaContrato.getListaClientes().get(i).getIdTrxHuellaDigital(), charOriginal, charNuevo));
			}
			return firmaContrato;
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL, "No se pudo procesar peticion.");
		}
	}


}