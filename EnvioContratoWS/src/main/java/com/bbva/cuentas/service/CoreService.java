package com.bbva.cuentas.service;


import java.util.List;
import java.util.Map;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.exception.SignBoxException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;


public interface CoreService {
	
	void process(RequestDTO request,Kit kit) throws ServiceException;

	Documentos processPreviewKit(RequestDTO request) throws ServiceException;

	Documentos processPreviewPlantilla(RequestDTO request) throws ServiceException;

	List<ClienteDTO> procesarRutas(Kit kit, RequestDTO request);
	
	ClienteDTO procesarRutasSimple(String rutaGenerada,List<Plantilla> listTemplates, FirmaContrato firmaContrato);
	
	
	void processMail(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros,TramaHist tramaHist,int numReproceso,DetTrama detTrama);
	
	void processFileUnico(RequestDTO request, Kit kit, ClienteDTO cliente,TramaHist tramaHist,DetTrama detTrama);
	
	void depurarArchivos(List<ClienteDTO> listaClientes);
	
	boolean huellaActivo(Kit kit);
	
	boolean processMailResp(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros,TramaHist tramaHist,DetTrama detTrama);

	boolean processFileUnicoResp(RequestDTO request, Kit kit, ClienteDTO cliente,TramaHist tramaHist,DetTrama detTrama);
	
	public List<ClienteDTO> processGenerarContratosGenerales(RequestDTO request, Kit kit, Map<String, Object> parametros,List<ClienteDTO> plantillasCliente,ClienteDTO titular,TramaHist tramaHist) throws ServiceException;
	
	
	public boolean procesaCorreoFileClientes(List<ClienteDTO> plantillasCliente, RequestDTO request, Kit kit,Map<String, Object> parametros,TramaHist tramaHist  ) throws ServiceException ;
 
	public void guardarTramaNivelGeneral(RequestDTO request, Kit kit, ClienteDTO cliente,
			ServiceException serviceException,TramaHist tramaHist) 	throws ServiceException ;
	
	public List<ClienteDTO> procesoGeneralContratos( RequestDTO request, Kit kit,
			Map<String, Object> parametros, TramaHist tramaHist,List<DetTrama> detTrama) throws ServiceException ;


	
}