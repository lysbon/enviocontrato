package com.bbva.cuentas.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.dao.DaoException;
import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.ResourceService;
import com.bbva.cuentas.service.exception.ServiceException;

import com.bbva.cuentas.service.exception.GenerarContratoException;

import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.service.GenerarContratosService;
import com.bbva.cuentas.service.worker.PDFWorker;
import com.bbva.cuentas.service.worker.JasperWorker;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

@Service
public class GenerarContratosServiceImpl implements GenerarContratosService{
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private FlagPlantillaService flagPlantillaService; 
	@Autowired
	private ResourceService imgService;
	@Autowired
	private PDFWorker worker;
	@Autowired
	private JasperWorker jasperWorker;
	@Autowired
	private PlantillaService plantillaService;
	
	@Value("${flag.pdf.database}")
	private boolean flagPDFDatabase;
	@Value("${flag.jasper.database}")
	private boolean flagJasperDatabase;
	
	public void generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ClienteDTO cliente) throws ServiceException {
    	logger.info("[ini]");
    	try {
    		if(plantillas!=null){
        		boolean evalua = false;
        		for(Plantilla template : plantillas){
        			evalua = evaluarPlantilla(template,parametros);
        			template.setValida(evalua);
        			if(evalua){
        				ConfigurationPath cfg = cliente.getCfg().get(template.getCdPlantilla());
        		
        					logger.info("pdf["+template.getCdPlantilla()+","+cfg.getBasename()+","+evalua+"]");
                            generarContrato(template, parametros, contenido,cfg,ProcesoEnvioContrato.TRAMA_INICIAL.getPaso());
        				
        			}
            	}
        	}
    		
    	}  catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new GenerarContratoException(e.getMessage());
		}
    }
	
	public boolean generarContratos(
    		List<Plantilla> plantillas,
    		Map<String,Object> parametros,
    		List<Map<String,?>> contenido,
    		ClienteDTO cliente,
    		int paso) throws ServiceException{
    	logger.info("[ini]");
    	boolean resultado = true;
    	
    		
    		if(plantillas!=null  ){
        		boolean evalua = false;
        		for(Plantilla template : plantillas){
        			evalua = evaluarPlantilla(template,parametros);
        			template.setValida(evalua);
        			if(evalua){
        				ConfigurationPath cfg = cliente.getCfg().get(template.getCdPlantilla());
        			
        					logger.info("pdf["+template.getCdPlantilla()+","+cfg.getBasename()+","+evalua+"]");
                            generarContrato(template, parametros, contenido,cfg,paso);
        				
        			}
            	}
        	}
    		return resultado;
    	
    }
	
	public boolean evaluarPlantillaAbsoluto(List<FlagPlantilla> flags,Map<String,Object> parametros){
		Map<String, List<FlagPlantilla>> resultsMap = new HashMap<String, List<FlagPlantilla>>();
		for (FlagPlantilla o : flags) {
			if(!resultsMap.containsKey(o.getStValor())){
				resultsMap.put(o.getStValor(), new ArrayList<FlagPlantilla>());
			}
			resultsMap.get(o.getStValor()).add(o);
		}
		if(resultsMap.containsKey(Constants.ST_ABSOLUTO)){
			List<FlagPlantilla> flagsAbsolutos = resultsMap.get(Constants.ST_ABSOLUTO);
			for(FlagPlantilla flag : flagsAbsolutos){
				String parametro = Util.buscarCampo(parametros, flag.getFlag().getNbCondicionNombre());
				if(Constants.EQUAL.equals(  flag.getFlag().getNuOrden()) &&  StringUtils.equals(flag.getFlag().getNbCondicionValor(), parametro) ||
				   Constants.NOEQUAL.equals(flag.getFlag().getNuOrden()) && !StringUtils.equals(flag.getFlag().getNbCondicionValor(), parametro)){
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean evaluarPlantillaGrupo(List<FlagPlantilla> flags,Map<String,Object> parametros) {
		Map<String, List<FlagPlantilla>> resultsMap = new HashMap<String, List<FlagPlantilla>>();
		for (FlagPlantilla o : flags) {
			if(!resultsMap.containsKey(o.getStValor())){
				resultsMap.put(o.getStValor(), new ArrayList<FlagPlantilla>());
			}
			resultsMap.get(o.getStValor()).add(o);
		}
		resultsMap.remove(Constants.ST_ABSOLUTO);
		Iterator<Map.Entry<String,List<FlagPlantilla>>> it = resultsMap.entrySet().iterator();
		while (it.hasNext()) {
			boolean result = evaluarPlantillaGrupoItem(it.next(),parametros);
			if(result) return result;
		}
		return false;
	}
	
	public boolean evaluarPlantillaGrupoItem(Map.Entry<String,List<FlagPlantilla>> flagsEntry,Map<String,Object> parametros) {
		List<FlagPlantilla> flagsGrupo = flagsEntry.getValue();
		boolean todosOK = true;
		for(FlagPlantilla flag : flagsGrupo){
			String parametro = Util.buscarCampo(parametros, flag.getFlag().getNbCondicionNombre());
			if(Constants.EQUAL.equals(flag.getFlag().getNuOrden())){
				if(!flag.getFlag().getNbCondicionValor().equals(parametro)){
					todosOK = false;
				}
			}else if(Constants.NOEQUAL.equals(flag.getFlag().getNuOrden()) &&
					flag.getFlag().getNbCondicionValor().equals(parametro)){
				todosOK = false;
			}	
		}
		return todosOK;
	}
        
    public boolean evaluarPlantilla(Plantilla plantilla,Map<String,Object> parametros) throws GenerarContratoException {
    	if(StringUtils.isEmpty(plantilla.getStCondicion())||Constants.PLANTILLA_FIJA.equals(plantilla.getStCondicion())){
    		return true;
		}else{
			List<FlagPlantilla> flags = flagPlantillaService.obtenerFlagsPorPlantilla(plantilla.getCdPlantilla());
			boolean resultAbsoluto = evaluarPlantillaAbsoluto(flags,parametros);
			if(resultAbsoluto) return resultAbsoluto;
			boolean resultGrupo = evaluarPlantillaGrupo(flags,parametros);
			if(resultGrupo) return resultGrupo;
		}
    	return false;
    }
    
    private byte[] obtenerStreamPlantilla(Plantilla plantilla) throws GenerarContratoException {
    	byte[] is = plantillaService.obtenerPDF(plantilla.getCdPlantilla(),null);
		if(is==null || is.length<Constants.MIN_SIZE_PDF){
			throw new GenerarContratoException("La plantilla "+plantilla.getCdPlantilla()+" no se encuentra correctamente guardada en la BD");
		}
		return is;
    }
    
 
    
    public void procesaJasper(

    		Plantilla plantilla,
    		Map<String,Object> parametros, 
    		List<Map<String,?>> contenido,
    		ConfigurationPath cfg) throws ServiceException{
    	
    	if(flagJasperDatabase){
			byte[] is = obtenerStreamPlantilla(plantilla); 
			if(Constants.TIPO_CONSTR_JASPER.equals(plantilla.getStTipoConstr()) 
        			|| Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())){
        		jasperWorker.processStream(plantilla,cfg,is,parametros,contenido);
			}else{
				jasperWorker.processStreamJson(plantilla,cfg,is,parametros,contenido);
			}
    	}else{
    		if(Constants.TIPO_CONSTR_JASPER.equals(plantilla.getStTipoConstr()) 
        			|| Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())){
    			jasperWorker.process(plantilla,cfg,parametros,contenido);
			}else{
				jasperWorker.processJson(plantilla,cfg,parametros,contenido);
			}
    	}
    	
    }
    
    
    
    public Plantilla generarContrato(

    		Plantilla plantilla,
    		Map<String,Object> parametros, 
    		List<Map<String,?>> contenido,
    		ConfigurationPath cfg,int paso) throws ServiceException{
    	

			List<ItemSection> listaSections = plantilla.getSecciones();
			fillReport(listaSections,parametros);
			
			
    		
    		if(Constants.TIPO_CONSTR_PDF.equals(plantilla.getStTipoConstr())){
        		if(flagPDFDatabase){
        			byte[] is = obtenerStreamPlantilla(plantilla);
            		worker.processStream(plantilla,listaSections,is,cfg);
            	}else{
            		worker.process(plantilla,listaSections,cfg);
            	}
        	}else if(Constants.TIPO_CONSTR_JASPER.equals(plantilla.getStTipoConstr()) 
        			|| Constants.TIPO_CONSTR_JASPER_C.equals(plantilla.getStTipoConstr())
        			|| Constants.TIPO_CONSTR_JSON.equals(plantilla.getStTipoConstr())
        			|| Constants.TIPO_CONSTR_JSON_C.equals(plantilla.getStTipoConstr())
        			){
        		
        		       procesaJasper(plantilla,parametros,contenido,cfg);

        	}else if(Constants.TIPO_CONSTR_NUEVO.equals(plantilla.getStTipoConstr())){
        		worker.processNuevo(plantilla,listaSections,cfg);
        	}else{
                Util.debug(plantilla,"no se encuentra correctamente configurada (TIPO_CONSTR).");
        	}
    		
    		cfg.setStep(paso);
    		
    		return plantilla;
    	
    }
    
    public void fillItemText(ItemStamp item, Map<String,Object> parametros) {
    	String strvalue = Util.buscarCampo(parametros, item.getNbNombreVar());
		if(item.getNuAncho()!=null){
			strvalue = StringUtils.left(strvalue,item.getNuAncho());
		}
		if(StringUtils.isEmpty(item.getNbMask())){
			item.setNbValorVar(strvalue);
		}else{
			try {
				item.setNbValorVar(Util.formatString(strvalue,item.getNbMask()));
			} catch (ParseException e) {
				item.setNbValorVar(strvalue);
			}
		}
	}
	    
    public void fillItemImageHuella(ItemStamp item, Map<String,Object> parametros) {
    	if(parametros.containsKey(Constants.PREFIX_HUELLA+item.getNuIdxCliente())) {
			item.setNbValorVar((String)parametros.get(Constants.PREFIX_HUELLA+item.getNuIdxCliente()));
		}
    }
    
    public void fillItemImage(ItemStamp item) throws ServiceException {
    	byte[] img = imgService.obtenerImagen(new Long(item.getNbValorVar()));
		item.setImg(img);
    }
    
    public void fillItemImageBase64(ItemStamp item, Map<String,Object> parametros) {
    	String strvalue = Util.buscarCampo(parametros, item.getNbNombreVar());
    	item.setNbValorVar(strvalue);
    }
    
    public void fillItemBarCode(ItemStamp item, Map<String,Object> parametros) {
    	if(parametros.containsKey(Constants.PREFIX_MINUCIA+item.getNuIdxCliente())) {
			item.setNbValorVar((String)parametros.get(Constants.PREFIX_MINUCIA+item.getNuIdxCliente()));
		}
    }
    
    public void fillItem(ItemStamp item, Map<String,Object> parametros) throws ServiceException{
    	if(Constants.TYPE_TEXT.equals(item.getChTipo())){
    		fillItemText(item,parametros);
		}else if(Constants.TYPE_IMAGE_HUELLA.equals(item.getChTipo())){
			fillItemImageHuella(item,parametros);	
		}else if(Constants.TYPE_IMAGE_BASE64.equals(item.getChTipo())){
			fillItemImageBase64(item,parametros);
		}else if(Constants.TYPE_BAR_CODE.equals(item.getChTipo())) {
			fillItemBarCode(item, parametros);	
		}else if(Constants.TYPE_IMAGE.equals(item.getChTipo())) {
			fillItemImage(item);
		}
    }
    
	public void fillReport(
			List<ItemSection> listaSections, 
			Map<String,Object> parametros) throws ServiceException{
		if(listaSections!=null){
			for(ItemSection is : listaSections){
    			List<ItemStamp> items = is.getListItems();
    			if(items!=null){
    				for(ItemStamp item : items){
    					fillItem(item,parametros);
    				}
    			}
    		}
    	}
    }
    
    private Documento generarTramaDocumentos(Plantilla plantilla,ClienteDTO cliente) throws IOException{
		Documento documento = new Documento();
		FileInputStream fileInputStreamReader=null;			
		if(Constants.ST_ACTIVO.equals(plantilla.getStActivo())){
			ConfigurationPath cfg = cliente.getCfg().get(plantilla.getCdPlantilla());
			
				try {
					File originalFile = new File(cfg.getRutaSalidaPDF());				
					fileInputStreamReader = new FileInputStream(originalFile);
					byte[] bytes = new byte[(int) originalFile.length()];
					int count=fileInputStreamReader.read(bytes);
					if(count>0) {
						documento.setEstado(plantilla.getStActivo());
						documento.setDescripcion(plantilla.getNbNombreFormato());
						documento.setNombreDescripcion(plantilla.getNbDescripcion());
						documento.setDocumento(new String(Base64.encodeBase64(bytes)));
					}
				} finally {
					try {if (fileInputStreamReader!=null) fileInputStreamReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
				}
			
		}
		return documento;
	}
	
	@Override
	public List<Documento> generarDocumentosBase64(
			List<Plantilla> plantillas,
			FirmaContrato firmaContrato,
			ClienteDTO cliente,
			List<ClienteDTO> clientes,
			Map<String,Object> parametros,
			List<Map<String,?>> contenido) throws ServiceException {
		List<Documento> documentos = new ArrayList<Documento>();
		try {
			if(plantillas!=null){ 
				for(Plantilla template : plantillas){
					ConfigurationPath cfg = cliente.getCfg().get(template.getCdPlantilla());
					
						Plantilla plantilla = generarContrato(
	        					template,
	        					parametros,contenido,cfg,ProcesoEnvioContrato.TRAMA_INICIAL.getPaso());
	        			Documento documento = generarTramaDocumentos(plantilla,cliente);
	        			documentos.add(documento);
				
            	}
			}
            return documentos;
		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_GENERAR_CONTRATOS,"No se pudo generar contratos.");
		}
	}
	
	
}