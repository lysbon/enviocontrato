package com.bbva.cuentas.service;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.service.exception.ServiceException;

public interface ReprocesoService {
	
	public boolean tramaFallida(DetTrama detalle);
	public void process() throws ServiceException;
	public void processfrom(Long from,Long to,TramaHist tramaHist) throws ServiceException;
	public  void iteraTrama(List<Trama> listaTramas) throws ServiceException;
    public void iteraDetTrama(List<DetTrama> detalleCliente,Map<String, List<DetTrama>> clienteTramas,RequestDTO peticion);
    public void iteraClientesTrama(List<Cliente> clientesTrama, Map<String, List<DetTrama>> clienteTramas);
    public void reprocess(RequestDTO peticion,List<DetTrama> detTrama,Trama trama) throws ServiceException;
    public RequestDTO parse(String trama) throws ServiceException;
    public List<ClienteDTO> seleccionClientesReproceso(List<DetTrama> detTrama,List<ClienteDTO> plantillasCliente);
    public ClienteDTO procesosFileEmailDetTrama(ClienteDTO detPlantillaCliente,DetTrama paso);
    public void processexecute(RequestDTO request, Kit kit, List<DetTrama> detTrama, Trama trama) throws ServiceException;
    public void actualizaTrazabilidad(Trama trama,TramaHist tramaHist) throws ServiceException;
    	
    
    	

}
