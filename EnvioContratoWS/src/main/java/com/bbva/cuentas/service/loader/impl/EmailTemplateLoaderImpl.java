package com.bbva.cuentas.service.loader.impl;

import java.sql.*;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.service.loader.EmailTemplateLoader;

import java.io.*;

import oracle.sql.*;
import oracle.jdbc.*;

@Component
public class EmailTemplateLoaderImpl implements EmailTemplateLoader {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	private static final String ERROR_STREAM = "Ocurrio un error al intentar cerrar stream";
	
	@Value("${datasource.schema}")
	private String schema;
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public EmailTemplateLoaderImpl(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }
    
    public void writeCLOBPut(String cdKit, String inputTextFileName) 
            throws IOException, SQLException {
        try {
        	prepararCampo(cdKit);
            actualizarPlantilla(cdKit,inputTextFileName);
        } catch (Exception e) {
            logger.error("Error al guardar email template. ",e);
        }
    }
    
    private void prepararCampo(String cdKit) {
    	String sqlText = "UPDATE ?.TBG000_KIT SET LB_CORREO_PLANTILLA = EMPTY_CLOB() WHERE  CD_KIT = ? ";
        jdbcTemplate.update(sqlText,schema,cdKit);
    }
    
    private void actualizarPlantilla(String cdKit,final String inputTextFile) {
    	String sqlText = "SELECT LB_CORREO_PLANTILLA FROM ?.TBG000_KIT WHERE CD_KIT = ? FOR UPDATE";
    	jdbcTemplate.query(sqlText, new Object[] { schema, cdKit },
				new RowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rownumber) throws SQLException {
				actualizarCampo(rs,inputTextFile);
				return StringUtils.EMPTY;
			}
		});
    }
    
    private void actualizarCampo(ResultSet rs,final String inputTextFile) throws SQLException{
    	FileInputStream     inputFileInputStream    = null;
        InputStreamReader   inputInputStreamReader  = null;
        BufferedReader      inputBufferedReader     = null;
        int                 chunkSize;
        char[]              textBuffer;
        long                position;
        int                 charsRead               = 0;
		try {
        	inputFileInputStream = new FileInputStream(inputTextFile);
            inputInputStreamReader = new InputStreamReader(inputFileInputStream);
            inputBufferedReader = new BufferedReader(inputInputStreamReader);					
			CLOB xmlDocument = ((OracleResultSet) rs).getCLOB("LB_CORREO_PLANTILLA");
			chunkSize = xmlDocument.getChunkSize();
            textBuffer = new char[chunkSize];
            position = 1;
            while ((charsRead = inputBufferedReader.read(textBuffer)) != -1) {
                xmlDocument.putChars(position, textBuffer, charsRead);
                position        += charsRead;
            }
        } catch (IOException e) {
            logger.error(e);
        }finally {
        	try {if (inputBufferedReader!=null) inputBufferedReader.close();} catch (IOException e)  {logger.error(ERROR_STREAM,e);}
        	try {if (inputInputStreamReader!=null) inputInputStreamReader.close();} catch (IOException e)  {logger.error(ERROR_STREAM,e);}
        	try {if (inputFileInputStream!=null) inputFileInputStream.close();} catch (IOException e)  {logger.error(ERROR_STREAM,e);}			            
        }
    }
	
}
