package com.bbva.cuentas.service.exception;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

public class StringException extends BaseException {
	
	
		
		private static final long serialVersionUID = -1033697598802715791L;
		
		private ProcesoEnvioContrato paso;

		public StringException(String errorCode,String message) {
			super(errorCode,message);
			setPaso(ProcesoEnvioContrato.GENERICO);
		}

		public StringException(String message) {
			super(CODIGO_ERROR_GENERICO,message);
			setPaso(ProcesoEnvioContrato.GENERICO);
		}

		
		public static final String CODIGO_ERROR_GENERICO			= "0004";

		
		
		public ProcesoEnvioContrato getPaso() {
			return paso;
		}

		public void setPaso(ProcesoEnvioContrato paso){
			this.paso = paso;
		}

	


}
