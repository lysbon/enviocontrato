package com.bbva.cuentas.service.impl;

import java.io.File;

import java.util.Date;
import java.util.HashMap;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.service.exception.EnvioMailException;
import com.bbva.cuentas.service.exception.FileUnicoException;
import com.bbva.cuentas.service.exception.GenerarContratoException;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.exception.SignBoxException;
import com.bbva.cuentas.aat.service.DetTramaHistService;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.DetTramaHist;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.dto.ClienteDTO;

import com.bbva.cuentas.dto.Documento;
import com.bbva.cuentas.dto.DocumentoCliente;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.dto.SignContracts;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.service.CoreService;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.FileUnicoService;
import com.bbva.cuentas.service.GenerarContratosService;
import com.bbva.cuentas.service.HuellaDigitalService;
import com.bbva.cuentas.service.KitCoreService;
import com.bbva.cuentas.service.MailService;
import com.bbva.cuentas.service.ReprocesoService;
import com.bbva.cuentas.service.SignContractService;
import com.bbva.cuentas.service.TrazabilidadService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;



@Service
public class CoreServiceImpl implements CoreService {

	protected static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Autowired
	protected GenerarContratosService generarContratosService;
	@Autowired
	protected MailService mailService;
	@Autowired
	protected SignContractService signService;
	@Autowired
	protected FileUnicoService fileUnicoService;
	@Autowired
	protected ErrorService backupService;
	
	@Autowired
	private TramaService tramaService;
	@Autowired
	protected TrazabilidadService trazabilidadService;
	@Autowired
	private DetTramaHistService detTramaHistService;
	
		
	@Autowired
	protected Util util;
	@Autowired
	protected HuellaDigitalService huellaService;
	@Autowired
	protected KitCoreService kitService;
	@Autowired
	protected ReprocesoService reprocesoService;

	 
	

	SimpleDateFormat formatYYYYHHmm = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
	

	@Value("${backup.tramas}")
	private boolean flagBackupTrama;
	
	@Value("${backup.trazabilidad}")
	private boolean flagBackupTrazabilidad;

	public boolean huellaActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getGrupo().getStHuella())
				&& Constants.ST_ACTIVO.equals(kit.getStHuella());
	}

	public boolean signBoxActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getStSignBox());
	}

	public boolean fileunicoActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getStFileunico());
	}

	public boolean correoActivo(Kit kit) {
		return Constants.ST_ACTIVO.equals(kit.getGrupoKit().getStEnvCorreo());
	}

	public void process(RequestDTO request, Kit kit) throws ServiceException {

		Timestamp fechaIniProceso = new Timestamp(System.currentTimeMillis());
		Timestamp fechaFinProceso = new Timestamp(System.currentTimeMillis());

		String estadoProceso = Constants.ESTADO_PROCESO_EXITO;
		TramaHist tramaHist = null;
		Trama trama = new Trama();
		ClienteDTO titular = null;
		try {

			tramaHist = iniciaTrazabilidad(request, kit, estadoProceso, fechaIniProceso);

			if (kit != null && request.getListaClientes() != null) {

				kit.setTrama(trama);
				kit.getTrama().setNumReproceso(0);
				kit.getTrama().setEstado(estadoProceso);

				Map<String, Object> parametros = util.toMap(request);

				if (huellaActivo(kit)) {
					parametros.putAll(huellaService.obtenerHuellasDigitalesBase64(request.getListaClientes(), request,
							kit, titular, tramaHist));
				}

				procesoGeneralContratos(request, kit, parametros, tramaHist,null);

			} else {
				estadoProceso = Constants.ESTADO_PROCESO_ERROR;
			}

		} catch (Exception e) {
			estadoProceso = Constants.ESTADO_PROCESO_ERROR;

		} finally {
			fechaFinProceso = new Timestamp(System.currentTimeMillis());
			actualizaTrazabilidad(tramaHist, estadoProceso, fechaFinProceso);

		}
	}

	
	public boolean validaUsoFileUnicoResp(RequestDTO request, Kit kit, ClienteDTO cliente, TramaHist tramaHist) {
		boolean resp = true;

		if (cliente.isProcesarFileUnico()) {

			resp = processFileUnicoResp(request, kit, cliente, tramaHist, cliente.getDetTrama());

		}

		return resp;
	}
	
	
	public List<ClienteDTO> procesoGeneralContratos(RequestDTO request, Kit kit,
			Map<String, Object> parametros, TramaHist tramaHist,List<DetTrama> detTrama) throws ServiceException {

		kitService.cargarPlantillasContratoCliente(kit);
		List<ClienteDTO> plantillasCliente = procesarRutas(kit, request);
		ClienteDTO cliente = Util.obtenerTitular(plantillasCliente);
		
		if (detTrama!=null)
		{	
			plantillasCliente=reprocesoService.seleccionClientesReproceso(detTrama,plantillasCliente);
		}
		
		boolean resp = true;

		try {
			processGenerarContratosGenerales(request, kit, parametros, plantillasCliente, cliente, tramaHist);

			if (!plantillasCliente.isEmpty()) {
				resp = procesaCorreoFileClientes(plantillasCliente, request, kit, parametros, tramaHist);
			}

			if (!resp) {

				throw new ServiceException("error en la firma o en el Contrato");
			}

		} finally {
			depurarArchivos(plantillasCliente);
		}

		return plantillasCliente;

	}
	
	public void procesaEnvioCorreoFileClientes(List<ClienteDTO> plantillasCliente, RequestDTO request, Kit kit,
			TramaHist tramaHist) throws ServiceException {
		boolean resp = true;
		boolean resGenerico = true;
		ClienteDTO cliente = null;

		for (int i = 0; i < plantillasCliente.size(); i++) {

			cliente = plantillasCliente.get(i);
			Map<String, Object> parametrosCliente = util.toMap(request, cliente.getCliente());
			if (cliente.isProcesarMail())
				resp = processMailResp(request, kit, cliente, parametrosCliente, tramaHist, cliente.getDetTrama());

			if (resp)
				resp = validaUsoFileUnicoResp(request, kit, cliente, tramaHist);

			if (!resp) {
				resGenerico = resp;
			}

		}

		if (!resGenerico) {
			throw new ServiceException("error en la firma o en el Contrato");
		}

	}
			
	
	public boolean procesaCorreoFileClientes(List<ClienteDTO> plantillasCliente, RequestDTO request, Kit kit,
			Map<String, Object> parametros, TramaHist tramaHist) throws ServiceException {

		boolean resGenerico = true;
		boolean resp = true;
		ClienteDTO cliente = null;
		try {

			for (int i = 0; i < plantillasCliente.size(); i++) {

				cliente = plantillasCliente.get(i);
				
				

				resp = processGenerarContratosPorCliente(request, kit, parametros, tramaHist, cliente.getDetTrama(),
						 cliente);

				if (resp) {
					resp = processSubContratosCliente(request, plantillasCliente, tramaHist, cliente,kit);
				}

				if (resp) {
					Map<String, Object> parametrosCliente = util.toMap(request, cliente.getCliente());
					if (cliente.isProcesarMail())
						resp = processMailResp(request, kit, cliente, parametrosCliente, tramaHist,
								cliente.getDetTrama());

					if (resp)
						validaUsoFileUnicoResp(request, kit, cliente, tramaHist);
				}

				if (!resp) {
					resGenerico = resp;
				}
			}

		} catch (Exception e) {
			return resGenerico;
		}
		return resGenerico;

	}

	protected TramaHist iniciaTrazabilidad(RequestDTO request, Kit kit,String estado,Timestamp fechaProceso) throws ServiceException {
		
		    return trazabilidadService.guardaTramaHist(request, kit, estado,fechaProceso);
			
	}
	
	protected DetTramaHist iniciaDetTrazabilidad(DetTrama detTrama,Timestamp fechaIniProceso,Timestamp fechaFinProceso, ServiceException serviceException) throws ServiceException 
	{
		return trazabilidadService.guardaDetTramaHist(detTrama,fechaIniProceso,fechaFinProceso,serviceException);
		
	}
	
	protected void actualizaTrazabilidad(TramaHist tramaHist,String estado,Timestamp fechaFinProceso)  {
		    
			trazabilidadService.updateTramaHist(tramaHist,estado,fechaFinProceso);
	}
	
	protected void actualizaDetTrazabilidad(TramaHist tramaHist,String estado,Timestamp fechaFinProceso) {
			trazabilidadService.updateTramaHist(tramaHist,estado,fechaFinProceso);
	}

	
	public void actualizaPasoDocGenerales(ClienteDTO titular,List<ClienteDTO> plantillasCliente)  {
		
		for (int i = 0; i < plantillasCliente.size(); i++) {

			ClienteDTO	cliente = plantillasCliente.get(i);
		
				Map<Long, ConfigurationPath> mapCfg = cliente.getCfg();
		
				for (Map.Entry<Long, ConfigurationPath> entry : mapCfg.entrySet()) {
					
		          
					if (entry.getValue().getChTipoPlantilla().equals(Constants.TIPO_PLANTILLA_GENERICA))
					{
					   ConfigurationPath cfgTitular = titular.getCfg().get(entry.getValue().getCdPlantilla());
					   entry.getValue().setStep(cfgTitular.getStep());
					}  
				
				}
		}
	}

	public void processSubContratosGenerales(RequestDTO request, List<ClienteDTO> plantillasCliente,
			TramaHist tramaHist, ClienteDTO titular) throws ServiceException {
		if (request.getKits() != null && !request.getKits().isEmpty()) {
			for (RequestDTO subRequest : request.getKits()) {
				Map<String, Object> parametros = util.toMap(subRequest);
				subRequest.getListaClientes().addAll(request.getListaClientes());
				Kit subKit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), subRequest.getIdContrato());
				if (subKit != null) {

					List<ClienteDTO> subPlantillasCliente = procesoGeneralContratos(request, subKit, parametros,
							tramaHist,null);

					if (subPlantillasCliente != null && !subPlantillasCliente.isEmpty()) {
						consolidarKits(subPlantillasCliente,null,null);
					}
				}
			}
		}
	}
	
	
	public boolean processSubContratosCliente(RequestDTO request, List<ClienteDTO> plantillasCliente,
			TramaHist tramaHist, ClienteDTO cliente,Kit kit) throws ServiceException {
		boolean resp = true;
		ClienteDTO clienteSubKit=null;
		List<ClienteDTO> subPlantillasCliente = null;
		try {
			if (request.getKits() != null && !request.getKits().isEmpty()) {
				for (RequestDTO subRequest : request.getKits()) {
					Map<String, Object> parametros = util.toMap(subRequest);
					subRequest.getListaClientes().addAll(request.getListaClientes());
					Kit subKit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), subRequest.getIdContrato());
					if (subKit != null) {

						kitService.cargarPlantillasContratoCliente(subKit);
						subPlantillasCliente = procesarRutas(subKit, subRequest);
						clienteSubKit = Util.obtenerTitular(subPlantillasCliente);
						
						consolidarPlantillasKits(kit,subKit);

						processGenerarContratosGenerales(request, subKit, parametros, subPlantillasCliente, clienteSubKit,
								tramaHist);

						resp = processGenerarContratosPorCliente(request, subKit, parametros, tramaHist,
								clienteSubKit.getDetTrama(),  clienteSubKit);

						if (subPlantillasCliente != null && !subPlantillasCliente.isEmpty()) {
							 consolidarKits(subPlantillasCliente,subRequest, cliente);
						}
					}
				}
			}
			
			
			consolidarKitsEnListEmail(request,cliente);

		} catch (SignBoxException e) {

			logger.error(ErrorCodes.DESC_ERROR_FIRMA_DIG, e);

			return false;

		}

		return resp;
	}

	
	
	
	public List<ClienteDTO> processGenerarContratosGenerales(RequestDTO request, Kit kit, Map<String, Object> parametros,List<ClienteDTO> plantillasCliente,ClienteDTO titular,TramaHist tramaHist)
			throws ServiceException {

		boolean resp = true;
		ServiceException exception = new ServiceException("");

		try {

			if (!kit.getListTemplates().isEmpty()) {
				generarContratosService.generarContratos(kit.getListTemplates(), parametros, request.getContenido(),
						titular, ProcesoEnvioContrato.GENERACION_PDFS_GENERAL.getPaso());

				if (signBoxActivo(kit)) {

					if (!(signService.signContracts(kit.getListTemplates(), request, titular).getEstado()
							.equals(Constants.ESTADO_SIGBOX_EXITOSO))) {

						throw new SignBoxException(ErrorCodes.DESC_ERROR_FIRMA_DIG);

					}

				} else {

					if (!(signService
							.otherSignContracts(kit.getListTemplates(), request, titular,
									ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL.getPaso())
							.getEstado().equals(Constants.ESTADO_SIGBOX_EXITOSO))) {

						throw new SignBoxException(ErrorCodes.DESC_ERROR_FIRMA_DIG);

					}

				}
				
				actualizaPasoDocGenerales(titular,plantillasCliente);

			}

			

		} catch (SignBoxException e) {

			logger.error(ErrorCodes.DESC_ERROR_FIRMA_DIG, e);
			exception = e;
			resp= false;
			throw exception;	
			

		} catch (GenerarContratoException e) {
			exception = e;
			resp= false;
			logger.error(ErrorCodes.DESC_ERROR_GEN_CONTRATO, e);
			throw exception;
			

		} catch (ServiceException e) {
			exception = e;
			resp= false;
			logger.error(ErrorCodes.DESC_ERROR_GENFIR_CONTRATO, e);
			throw exception;
			
			

		} finally {
			if (!resp) {
				guardarTramaNivelGeneral(request, kit, titular, exception, tramaHist);
				
			}

		}
		return plantillasCliente;
	}
	
	
	
	
	
	public boolean procesaContratosCliente(RequestDTO request, Kit kit, Map<String, Object> parametros, ClienteDTO cliente) throws ServiceException 
	{
		boolean resp = true;
		Map<String, Object> parametrosCliente = null;
		
			parametrosCliente = util.toMap(parametros, cliente.getCliente());

			resp = generarContratosService.generarContratos(kit.getListTemplatesCliente(), parametrosCliente,
					request.getContenido(), cliente, ProcesoEnvioContrato.GENERACION_PDFS_CLIENTE.getPaso());

		 

		return resp;
	}
	
	public boolean procesaFirmaContrato(RequestDTO request, Kit kit,ClienteDTO cliente) throws SignBoxException, ServiceException
	{
		boolean resp = true;

		if (signBoxActivo(kit)) {

			if (!(signService.signContracts(kit.getListTemplatesCliente(), request, cliente).getEstado()
					.equals(Constants.ESTADO_SIGBOX_EXITOSO))) {

				throw new SignBoxException(ErrorCodes.DESC_ERROR_FIRMA_DIG);

			}

		} else {

			if (!(signService
					.otherSignContracts(kit.getListTemplatesCliente(), request, cliente,
							ProcesoEnvioContrato.SIN_FIRMA_DIGITAL_PDF_GENERAL_CLIENTE.getPaso())
					.getEstado().equals(Constants.ESTADO_SIGBOX_EXITOSO))) {

				throw new SignBoxException(ErrorCodes.DESC_ERROR_FIRMA_DIG);

			}

		}
		return resp;
	}
	
	public boolean processGenerarContratosPorCliente(RequestDTO request, Kit kit, Map<String, Object> parametros,TramaHist tramaHist, DetTrama detTrama,ClienteDTO cliente)
			throws ServiceException {
		boolean resp = true;
		ServiceException exception = new ServiceException("");
		try {

			resp = procesaContratosCliente(request, kit, parametros, cliente);

			if (resp) {

				resp = procesaFirmaContrato(request, kit, cliente);
			}

		} catch (SignBoxException e) {

			logger.error(ErrorCodes.DESC_ERROR_FIRMA_DIG, e);
			e.setPaso(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_CLIENTE);
			exception = e;
			resp= false;

		} catch (GenerarContratoException e) {

			logger.error(ErrorCodes.DESC_ERROR_GEN_CONTRATO_CLIE, e);
			e.setPaso(ProcesoEnvioContrato.GENERACION_PDFS_CLIENTE);
			exception = e;
			resp= false;

		} catch (ServiceException e) {

			logger.error(ErrorCodes.DESC_ERROR_ENV_EMAIL, e);
			exception = e;
			resp= false;

		} finally {
			if (!resp) {
				guardarTramaNivelCliente(request, kit, cliente, exception, tramaHist, detTrama);
			}

		}
		return resp;
	}
	

	private ClienteDTO consolidarKits(List<ClienteDTO> subPlantillasCliente,RequestDTO subRequest,ClienteDTO cliente) {

		
			for (ClienteDTO subPlantillaCliente : subPlantillasCliente) {

				
				if (cliente.getCliente().getCodigoCentral()
						.equals(subPlantillaCliente.getCliente().getCodigoCentral())) {

					Map<Long, ConfigurationPath> mapCfg = subPlantillaCliente.getCfg();

					for (Map.Entry<Long, ConfigurationPath> entry : mapCfg.entrySet()) {
						
					
						cliente.getListCfgEmail().add(entry.getValue());
						cliente.getListCfgFU().add(entry.getValue());
						

					}

				}
			}
		
		
		consolidarKitsEnListKits(subRequest, cliente);
		

		return cliente;
	}
	
	
	private ClienteDTO consolidarKitsEnListEmail(RequestDTO request,ClienteDTO cliente ) {

		

			Map<Long, ConfigurationPath> mapCfg = cliente.getCfg();

			for (Map.Entry<Long, ConfigurationPath> entry : mapCfg.entrySet()) {

				
				cliente.getListCfgEmail().add(entry.getValue());
				
				if (!(request.getKits() != null && !request.getKits().isEmpty())) {
					
					cliente.getListCfgFU().add(entry.getValue());
				}

			}

		

		return cliente;
	}
	
	
	private ClienteDTO consolidarKitsEnListKits(RequestDTO subRequest,ClienteDTO cliente) {

		String nroContrato = null;
		String pattern = null;
		Map<Integer, String> rutasBase = util.generarRutas();

		nroContrato = subRequest.getNumeroContrato();

		

			Map<Long, ConfigurationPath> mapCfg = cliente.getCfg();

			for (Map.Entry<Long, ConfigurationPath> entry : mapCfg.entrySet()) {

				ConfigurationPath cfg = new ConfigurationPath();
				pattern = cliente.getCliente().getCodigoCentral() + nroContrato + entry.getValue().getNbTipoFileunico()
						+ entry.getValue().getStTime() + entry.getValue().getCdPlantilla()
						+ entry.getValue().getCdCodigo();

				cfg.setBasename(pattern);

				cfg.setRutaSalidaPDF(entry.getValue().getRutaSalidaPDF());
				cfg.setRutaSalidaFirma(entry.getValue().getRutaSalidaFirma());

				cfg.setRutaCSV(entry.getValue().getRutaCSV());

				cfg.setRutaSalidaTempFileUnicoCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						cliente.getCliente().getCodigoCentral() + pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoPDF(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						cliente.getCliente().getCodigoCentral() + pattern + Constants.EXT_PDF));

				cfg.setStEnvCorreo(entry.getValue().getStEnvCorreo());
				cfg.setStFileunico(entry.getValue().getStFileunico());
				cfg.setNbNombreCorreo(entry.getValue().getNbNombreCorreo());
				cfg.setNbTipoFileunico(entry.getValue().getNbTipoFileunico());
				cfg.setNumeroContrato(entry.getValue().getNumeroContrato());
				cfg.setStep(entry.getValue().getStep());

				cfg.setStTime(entry.getValue().getStTime());
				cfg.setCdCodigo(entry.getValue().getCdCodigo());
				cfg.setCdPlantilla(entry.getValue().getCdPlantilla());

				
				cliente.getListCfgFU().add(cfg);

			}

		

		return cliente;
	}
	
	private void consolidarPlantillasKits(Kit kit, Kit subKit) {

		for (Plantilla plantillasubkit : subKit.getListTemplates()) {

			kit.getListTemplates().add(plantillasubkit);

		}

		for (Plantilla plantillasubkit : subKit.getListTemplatesCliente()) {

			kit.getListTemplatesCliente().add(plantillasubkit);

		}

		
	}

	public void processMail(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros,TramaHist tramaHist,int numReproceso,DetTrama detTrama) {
		if (correoActivo(kit)) {
			try {
				mailService.sendMailProc(request, parametros, kit, cliente);
			} catch (ServiceException e) {
				logger.error("Error en servicio de envio de correos. ", e);
				kit.getTrama().setNumReproceso(numReproceso);
				guardarTramaNivelCliente(request, kit, cliente, e,tramaHist,detTrama);
			}
		}
	}
	
	public boolean processMailResp(RequestDTO request, Kit kit, ClienteDTO cliente, Map<String, Object> parametros,
			TramaHist tramaHist, DetTrama detTrama)  {
		try {
			if (correoActivo(kit)) {
				mailService.sendMailProc(request, parametros, kit, cliente);
			}
		} catch (EnvioMailException e) {
			logger.error("Error en servicio de envio de correos. ", e);

			guardarTramaNivelCliente(request, kit, cliente, e, tramaHist, detTrama);
			return false;
		}
		return true;
	}

	public void processFileUnico(RequestDTO request, Kit kit, ClienteDTO cliente,TramaHist tramaHist,DetTrama detTrama) {
		
			try {
				    if (fileunicoActivo(kit)) {
				    fileUnicoService.procesarArchivosWithTemp(kit, request, cliente);
				}
			} catch (ServiceException e) {
				logger.error("Error en servicio de envio fileunico. ", e);
	
				guardarTramaNivelCliente(request, kit, cliente, e,tramaHist,detTrama);
			}
		
	}
	
	public boolean processFileUnicoResp(RequestDTO request, Kit kit, ClienteDTO cliente, TramaHist tramaHist,
			 DetTrama detTrama) {

		try {
			if (fileunicoActivo(kit)) {
				fileUnicoService.procesarArchivosWithTemp(kit, request, cliente);
			}
		} catch (FileUnicoException e) {
			logger.error("Error en servicio de envio fileunico. ", e);
			
			guardarTramaNivelCliente(request, kit, cliente, e, tramaHist, detTrama);
			return false;
		}

		return true;
	}

	protected void guardarTramaNivelCliente(RequestDTO request, Kit kit, ClienteDTO cliente,
			ServiceException serviceException, TramaHist tramaHist, DetTrama detTrama) {
		Timestamp timeActual = new Timestamp(System.currentTimeMillis());
		Timestamp fechaIniProceso = new Timestamp(System.currentTimeMillis());

		Timestamp fechaFinProceso = new Timestamp(System.currentTimeMillis());

		Trama trama = kit.getTrama();
		DetTramaHist detTramaHist = new DetTramaHist();
		try {

			if (kit.getTrama() != null && (kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_SELECCIONADO) || 
					kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_PENDIENTE)) ) {

				if (detTrama != null) {
					detTrama.setEstado(Constants.ESTADO_PROCESO_ERROR);
					detTrama.setNumReproceso(trama.getNumReproceso() + 1);
					detTrama.setCdStep(serviceException.getPaso().getPaso());
					detTrama.setDetStep(serviceException.getPaso().getDetPaso());
					
					
					detTrama = backupService.updateDetTramaObj(detTrama);
					
					

					detTramaHist = detTramaHistService.obtenerDetTramasHistPorPK(detTrama.getIdTrama(),
							detTrama.getCdCliente());
					
					
					if (detTramaHist!=null)
					{	
						detTramaHist.setFhProFin(timeActual);
						detTramaHist.setNroReproceso(detTrama.getNumReproceso());
						detTramaHist.setEstado(Constants.ESTADO_PROCESO_ERROR);
						detTramaHist.setCdStep(detTrama.getCdStep());
						detTramaHist.setDetStep(detTrama.getDetStep());
						detTramaHistService.updateDetTramaHist(detTramaHist);
				    }
					
					trama.setCdStep(serviceException.getPaso().getPaso());
					trama.setDetStep(serviceException.getPaso().getDetPaso());
					trama.setChMoti(backupService.getMensaje(serviceException));
					
					
				} else {
					detTrama = backupService.guardarDetTramaObj(cliente, trama, serviceException,
							Constants.ESTADO_PROCESO_ERROR);
					trazabilidadService.guardaDetTramaHist(detTrama, fechaIniProceso, fechaFinProceso,
							serviceException);
				}

			} else {
				if (kit.getTrama() != null && kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_ERROR)) {

					detTrama = backupService.guardarDetTramaObj(cliente, trama, serviceException,
							kit.getTrama().getEstado());
					trazabilidadService.guardaDetTramaHist(detTrama, fechaIniProceso, fechaFinProceso,
							serviceException);

				} else {

					if (cliente != null)
						trama.setCdCliente(cliente.getCliente().getCodigoCentral());
					trama.setIdTrama(tramaHist.getIdTrama());
					trama.setCdEnv(kit.getGrupo().getCdEnv());
					trama.setEstado(Constants.ESTADO_PROCESO_ERROR);
					trama.setTimeReproceso(timeActual);

					trama = backupService.guardarTramaGeneral(request, serviceException, trama);

					detTrama = backupService.guardarDetTramaObj(cliente, trama, serviceException,
							Constants.ESTADO_PROCESO_ERROR);
					trazabilidadService.guardaDetTramaHist(detTrama, fechaIniProceso, fechaFinProceso,
							serviceException);

					//kit.setTrama(trama);

				}

			}

			kit.setTrama(trama);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void guardarTramaNivelGeneral(RequestDTO request, Kit kit, ClienteDTO cliente,
			ServiceException serviceException, TramaHist tramaHist) {

		Timestamp timeActual = new Timestamp(System.currentTimeMillis());
		

		try {

			if (kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_SELECCIONADO)
					|| kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_PENDIENTE)) {

				kit.getTrama().setEstado(Constants.ESTADO_PROCESO_ERROR);
				kit.getTrama().setTimeReproceso(timeActual);
				tramaService.actualizarTrama(kit.getTrama());

			} else if (kit.getTrama().getEstado().equals(Constants.ESTADO_PROCESO_EXITO)) {

				if (cliente != null)
					kit.getTrama().setCdCliente(cliente.getCliente().getCodigoCentral());
				kit.getTrama().setIdTrama(tramaHist.getIdTrama());
				kit.getTrama().setCdEnv(kit.getGrupo().getCdEnv());
				kit.getTrama().setEstado(Constants.ESTADO_PROCESO_ERROR);
				kit.getTrama().setNumReproceso(kit.getTrama().getNumReproceso());
				kit.getTrama().setTimeReproceso(timeActual);
				kit.setTrama(backupService.guardarTramaGeneral(request, serviceException, kit.getTrama()));

			}

			

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	
	

	
	public void depurarArchivos(List<ClienteDTO> listaClientes) {
		if (listaClientes != null) {
			
			for(ClienteDTO cliente: listaClientes) {
				depurarCliente(cliente);
			}
		}		
	}
	
	public void depurarCliente(ClienteDTO clienteDTO) {
		if (clienteDTO != null) {
			

			
				List<ConfigurationPath> archivosPlantilla = clienteDTO.getListCfgFU();
				for (ConfigurationPath archivoPlantilla : archivosPlantilla) {
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaCSV()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaFirma()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaPDF()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaTempFileUnicoPDF()));
					FileUtils.deleteQuietly(new File(archivoPlantilla.getRutaSalidaTempFileUnicoCSV()));
				}
			
		}
	}

	

	@Override
	public Documentos processPreviewKit(RequestDTO request) throws ServiceException {
		Kit kit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), request.getIdContrato());
		kitService.cargarPlantillasContratoCliente(kit);
		return processPreview(request, kit);
	}

	@Override
	public Documentos processPreviewPlantilla(RequestDTO request) throws ServiceException {
		Kit kit = kitService.obtenerKitPorGrupo(request.getIdGrupo(), Constants.COD_KIT_DEFAULT);
		kitService.cargarPlantillasContratoCliente(kit, request.getIdContrato());
		return processPreview(request, kit);
	}

	public Documentos processPreview(RequestDTO request, Kit kit) throws ServiceException {
		Documentos documentos = new Documentos();
		List<Documento> documentosGeneral = null;
		try {
			if (kit != null) {
				Map<String, Object> parametrosGeneral = util.toMap(request);
				List<ClienteDTO> listaClientes = procesarRutas(kit, request);
				ClienteDTO titular = Util.obtenerTitular(listaClientes);
				documentosGeneral = generarContratosService.generarDocumentosBase64(kit.getListTemplates(), request,
						titular, listaClientes, parametrosGeneral, request.getContenido());
				documentos.setDocumentos(documentosGeneral);
				if (listaClientes != null) {
					for (int i = 0; i < listaClientes.size(); i++) {
						DocumentoCliente cliente = new DocumentoCliente();
						List<Documento> documentosClientes = null;
						ClienteDTO clienteDTO = listaClientes.get(i);
						Map<String, Object> parametrosCliente = util.toMap(request,clienteDTO.getCliente());
						documentosClientes = generarContratosService.generarDocumentosBase64(
								kit.getListTemplatesCliente(), request, clienteDTO, listaClientes, parametrosCliente,
								request.getContenido());
						cliente.setDocumentos(documentosClientes);
						cliente.setCodCliente(clienteDTO.getCliente().getNroDocumento());
						documentos.getClientes().add(cliente);
					}
				}
			}
		} catch (ServiceException e) {
			logger.error("Error en servicio processPreviewKit. ", e);
			throw e;
		}
		return documentos;
	}

	public List<ClienteDTO> procesarRutas(Kit kit, RequestDTO request) {

		Map<Integer, String> rutasBase = util.generarRutas();

		String ss = formatYYYYHHmm.format(new java.util.Date());

		String nroContrato = request.getNumeroContrato();

		List<ClienteDTO> listaClientes = new ArrayList<ClienteDTO>();
		for (int i = 0; i < request.getListaClientes().size(); i++) {
			ClienteDTO clienteDTO = new ClienteDTO();
			clienteDTO.setCliente(request.getListaClientes().get(i));
			for (Plantilla plantilla : kit.getListTemplates()) {
				String pattern = nroContrato + plantilla.getNbTipoFileunico() + ss + plantilla.getCdPlantilla()
						+ kit.getCdCodigo();
				ConfigurationPath cfg = new ConfigurationPath();
				cfg.setBasename(pattern);
				String rutaOri = rutasBase.get(Constants.RUTA_IDX_ORIGINAL);
				cfg.setRutaSalidaPDF(FilenameUtils.concat(rutaOri, pattern + Constants.EXT_PDF));
				cfg.setRutaSalidaFirma(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_FIRMA), pattern + Constants.EXT_PDF));
				cfg.setRutaCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_CSV),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoPDF(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						clienteDTO.getCliente().getCodigoCentral() + pattern + Constants.EXT_PDF));

				cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
				cfg.setStFileunico(plantilla.getStFileunico());
				cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
				cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
				cfg.setNumeroContrato(request.getNumeroContrato());
				
				cfg.setCdCodigo(kit.getCdCodigo());
				cfg.setCdPlantilla(plantilla.getCdPlantilla());
				cfg.setStTime(ss);
				cfg.setChTipoPlantilla(plantilla.getChTipoPlantilla());

				
				cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());
				
				clienteDTO.getCfg().put(plantilla.getCdPlantilla(), cfg);

			}
			for (Plantilla plantilla : kit.getListTemplatesCliente()) {
				String pattern = clienteDTO.getCliente().getCodigoCentral() + nroContrato
						+ plantilla.getNbTipoFileunico() + ss + plantilla.getCdPlantilla() + kit.getCdCodigo();
				ConfigurationPath cfg = new ConfigurationPath();
				cfg.setBasename(pattern);
				cfg.setRutaSalidaPDF(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_ORIGINAL), pattern + Constants.EXT_PDF));
				cfg.setRutaSalidaFirma(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_FIRMA), pattern + Constants.EXT_PDF));
				cfg.setRutaCSV(
						FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_CSV), pattern + Constants.EXT_CSV));

				cfg.setRutaSalidaTempFileUnicoCSV(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						pattern + Constants.EXT_CSV));
				cfg.setRutaSalidaTempFileUnicoPDF(FilenameUtils.concat(rutasBase.get(Constants.RUTA_IDX_TEMPFILEUNICO),
						pattern + Constants.EXT_PDF));

				cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
				cfg.setStFileunico(plantilla.getStFileunico());
				cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
				cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
				cfg.setNumeroContrato(request.getNumeroContrato());
				
				cfg.setCdCodigo(kit.getCdCodigo());
				cfg.setCdPlantilla(plantilla.getCdPlantilla());
				cfg.setStTime(ss);
				cfg.setChTipoPlantilla(plantilla.getChTipoPlantilla());
				

				cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());
				
				clienteDTO.getCfg().put(plantilla.getCdPlantilla(), cfg);

			}
			listaClientes.add(clienteDTO);
		}
		return listaClientes;
	}

	public ClienteDTO procesarRutasSimple(String rutaGenerada, List<Plantilla> listTemplates,
			FirmaContrato firmaContrato) {
		String ss = formatYYYYHHmm.format(new java.util.Date());
		int indice = 0;
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setCliente(firmaContrato.getListaClientes().get(indice));
		Cliente cliente = clienteDTO.getCliente();
		for (Plantilla plantilla : listTemplates) {
			String patternCliente = cliente.getNroDocumento() + plantilla.getNbTipoFileunico() + ss
					+ plantilla.getCdPlantilla();
			ConfigurationPath cfg = new ConfigurationPath();
			cfg.setBasename(patternCliente);
			cfg.setRutaSalidaPDF(FilenameUtils.concat(rutaGenerada, patternCliente + Constants.EXT_PDF));
			cfg.setStEnvCorreo(plantilla.getStEnvCorreo());
			cfg.setStFileunico(plantilla.getStFileunico());
			cfg.setNbNombreCorreo(plantilla.getNbNombreCorreo());
			cfg.setNbTipoFileunico(plantilla.getNbTipoFileunico());
			cfg.setNumeroContrato(firmaContrato.getNumeroContrato());
			cfg.setStep(ProcesoEnvioContrato.CARGA_PLANTILLAS.getPaso());

			clienteDTO.addCfg(plantilla.getCdPlantilla(), cfg);
		}
		return clienteDTO;
	}


}