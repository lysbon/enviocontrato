package com.bbva.cuentas.service;

import org.springframework.core.io.Resource;

public interface ResourceAwareService{

	public Resource getResource(String location);
	
}
