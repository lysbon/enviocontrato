package com.bbva.cuentas.service;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;

public interface PivoteService {
	
	public void process(RequestDTO request) throws ServiceException;
	
	public Documentos processKitBase64(RequestDTO request) throws ServiceException;
	
	public Documentos processPlantillaBase64(RequestDTO request) throws ServiceException;
	

}