package com.bbva.cuentas.service.exception;

import java.text.MessageFormat;

import com.bbva.cuentas.enums.ProcesoEnvioContrato;

import org.apache.logging.log4j.Logger;

public class SignBoxException extends ServiceException{
	
	private static final long serialVersionUID = 1659667236074434395L;

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	private static final String MENSAJE = "Error en la firma digital de plantillas con signbox. ";
	private static final String FORMATO_LOG = MENSAJE + " ''{0}''.";
		
	public SignBoxException(String mensaje) {
		super(CODIGO_SERVICE_SIGNBOX, MENSAJE);
		setPaso(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL);
		logger.info(MessageFormat.format(FORMATO_LOG, mensaje));
	}

	public SignBoxException(String mensaje, ProcesoEnvioContrato paso) {
		super(CODIGO_SERVICE_SIGNBOX, MENSAJE);
		setPaso(paso);
		logger.info(MessageFormat.format(FORMATO_LOG, mensaje));
	}
	
}
