package com.bbva.cuentas.service.loader.impl;

import java.sql.*;

import javax.sql.DataSource;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.AbstractLobCreatingPreparedStatementCallback;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.service.loader.HtmlTemplateLoader;

import java.io.*;

@Component
public class HtmlTemplateLoaderImpl implements HtmlTemplateLoader {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${datasource.schema}")
	private String schema;
	private final JdbcTemplate jdbcTemplate;
	
	@Autowired
    public HtmlTemplateLoaderImpl(DataSource datasource) {
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }
    
	public void writeCLOBPut(final String cdKit, String inputTextFileName) 
            throws IOException, SQLException {
		
		String sqlText = null;
		InputStream clobIs= null;
		InputStreamReader clobReader=null;
		try {        	
        	sqlText = "UPDATE "+schema+".TBG010_KIT SET LB_CORREO_PLANTILLA = ? WHERE CD_CODIGO = ? ";
        	final File clobIn = new File(inputTextFileName);
        	final LobHandler lobHandler = new DefaultLobHandler();
        	clobIs = new FileInputStream(clobIn);
        	clobReader = new InputStreamReader(clobIs);
        	
        	final InputStreamReader tempClobReader= clobReader;
        	
			jdbcTemplate.execute(sqlText,
        	  new AbstractLobCreatingPreparedStatementCallback(lobHandler) {
        	      protected void setValues(PreparedStatement ps, LobCreator lobCreator) 
        	          throws SQLException {
        	        lobCreator.setClobAsCharacterStream(ps, 1, tempClobReader, (int)clobIn.length());
        	        ps.setString(2, cdKit);
        	      }
        	  }
        	);
			//"clobReader.close();"
        	
        } catch (IOException e) {
            logger.error(e);
            throw e;
        }finally {
        	try {if (clobIs!=null) clobIs.close();} catch (IOException e)  {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        	try {if (clobReader!=null) clobReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
        }
    }
	
}
