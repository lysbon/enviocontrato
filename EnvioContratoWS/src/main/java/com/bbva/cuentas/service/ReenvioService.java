package com.bbva.cuentas.service;

import org.springframework.scheduling.annotation.Async;

import com.bbva.cuentas.firmarcontratos.ReenvioContratosRequest;

public interface ReenvioService {
	
    @Async
    public void process(ReenvioContratosRequest request);    

}