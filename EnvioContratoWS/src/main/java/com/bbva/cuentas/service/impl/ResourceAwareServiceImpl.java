package com.bbva.cuentas.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.bbva.cuentas.service.ResourceAwareService;

@Service
public class ResourceAwareServiceImpl implements ResourceLoaderAware,ResourceAwareService{
	
	private ResourceLoader resourceLoader;

	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

	public Resource getResource(String location){
		return resourceLoader.getResource(location);
	}
}
