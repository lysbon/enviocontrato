package com.bbva.cuentas.service.loader;

import java.io.IOException;
import java.sql.SQLException;

public interface EmailTemplateLoader  {

    public void writeCLOBPut(String cdKit, String inputTextFileName) 
    		throws IOException, SQLException;
	
}
