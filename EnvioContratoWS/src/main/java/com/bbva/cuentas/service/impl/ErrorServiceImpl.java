package com.bbva.cuentas.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.exception.StringException;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Trama;

import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.CustomItemContratoDeserializer;
import com.bbva.cuentas.util.CustomItemContratoSerializer;
import com.bbva.cuentas.util.Util;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Service
public class ErrorServiceImpl implements ErrorService {
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private TramaService tramaService;
	
    
    
	public Trama guardarTramaGeneral(FirmaContrato firmaContrato,
			
			ServiceException error,
			
			Trama trama) {
		
		try {
			
			trama.setCdDoc(firmaContrato.getNumeroContrato());
			
			trama.setChTipo(Constants.TIPO_TRAMA_ERROR);
			trama.setChProcedencia(firmaContrato.getProcedencia());
			trama.setCdStep(error.getPaso().getPaso());
			trama.setChMoti(getMensaje(error));
			trama.setDetStep(error.getPaso().getDetPaso());
			
			String tramaTotal = getTramaJson(firmaContrato);
			trama.setChTrama(tramaTotal);
			tramaService.insertaTrama(trama);
			
			return trama;
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return trama;
	}
	
	public void guardarDetTrama(ClienteDTO cliente,Trama trama,ServiceException error,String estadoProceso) throws ServiceException{
		DetTrama detTrama = new DetTrama();
		detTrama.setIdTrama(trama.getIdTrama());
		detTrama.setCdCliente(cliente.getCliente().getCodigoCentral());
		detTrama.setCdStep(error.getPaso().getPaso());
		detTrama.setDetStep(error.getPaso().getDetPaso());
		detTrama.setEstado(estadoProceso);
		detTrama.setCdDoc(trama.getCdDoc());
		detTrama.setIdTrama(trama.getIdTrama());
		detTrama.setTimeReproceso(trama.getTimeReproceso());
		detTrama.setNumReproceso(trama.getNumReproceso());
		tramaService.insertaDetTrama(detTrama);
	 }
	
	public DetTrama guardarDetTramaObj(ClienteDTO cliente,Trama trama,ServiceException error,String estadoProceso) throws ServiceException{
		DetTrama detTrama = new DetTrama();
		detTrama.setIdTrama(trama.getIdTrama());
		detTrama.setCdCliente(cliente.getCliente().getCodigoCentral());
		detTrama.setCdStep(error.getPaso().getPaso());
		detTrama.setDetStep(error.getPaso().getDetPaso());
		detTrama.setEstado(estadoProceso);
		detTrama.setCdDoc(trama.getCdDoc());
		detTrama.setIdTrama(trama.getIdTrama());
		detTrama.setTimeReproceso(trama.getTimeReproceso());
		detTrama.setNumReproceso(trama.getNumReproceso());
		Long pk=tramaService.insertaDetTrama(detTrama);
		 detTrama.setIdDetTrama(pk);
		 return detTrama;
	 }
	
	
	public DetTrama updateDetTramaObj(DetTrama detTrama) throws ServiceException{
		
		tramaService.updateDetTrama(detTrama)	 ;
		return detTrama;
	 }
	
    
    public void updateTramaGeneral(
    		FirmaContrato firmaContrato,
    		ServiceException error,
    		Integer cdEnv,String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso) {
    	updateTramaGeneral(firmaContrato,null,error,cdEnv,estado,numReproceso,timeReproceso);
    }
    
    public void updateTramaGeneral(
    		FirmaContrato firmaContrato,
    		ClienteDTO cliente,
    		ServiceException error,
    		Integer cdEnv,
    		String estado,
    		Integer numReproceso,
    		Timestamp timeReproceso) {
		Trama trama = new Trama();
		try {
			if (cliente != null)
				trama.setCdCliente(cliente.getCliente().getCodigoCentral());
			trama.setCdDoc(firmaContrato.getNumeroContrato());
			trama.setCdEnv(cdEnv);
			trama.setChTipo(Constants.TIPO_TRAMA_ERROR);
			trama.setChProcedencia(firmaContrato.getProcedencia());
			trama.setCdStep(error.getPaso().getPaso());
			trama.setChMoti(getMensaje(error));
			trama.setDetStep(error.getPaso().getDetPaso());
			trama.setEstado(estado);
			trama.setNumReproceso(numReproceso);
			trama.setTimeReproceso(timeReproceso);
			String tramaTotal = getTrama(firmaContrato);
			trama.setChTrama(tramaTotal);
			tramaService.updateTrama(trama);
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
	}
    
    public String getMensaje(Throwable error){
    	String code = Constants.VACIO;
    	if(error == null) return code;
    	if(error instanceof ServiceException){
    		code = ((ServiceException)error).getErrorCode();
    	}
    	String detalle = AppUtil.getDetailedException(error).toString();
    	if(detalle.length()>40)
    		return (code+"-"+detalle).substring(0,40);
    	return (code+"-"+detalle);
    }
    
    public RequestDTO getRequestJson(String trama) throws ServiceException{
    	trama = StringUtils.replace(trama, "\"_c\"", "\"numeroContrato\""    );
    	trama = StringUtils.replace(trama, "\"_d\"", "\"oficinaGestora\""    );
    	trama = StringUtils.replace(trama, "\"_e\"", "\"procedencia\""       );
    	trama = StringUtils.replace(trama, "\"_f\"", "\"plantillaCorreo\""   );
    	trama = StringUtils.replace(trama, "\"_g\"", "\"grupoCorreo\""       );
    	trama = StringUtils.replace(trama, "\"_h\"", "\"idContrato\""        );
    	trama = StringUtils.replace(trama, "\"_i\"", "\"idGrupo\""           );
    	trama = StringUtils.replace(trama, "\"_j\"", "\"async\""             );
    	trama = StringUtils.replace(trama, "\"_k\"", "\"gestDoc\""           );
    	trama = StringUtils.replace(trama, "\"_l\"", "\"codigoCentral\""     );
    	trama = StringUtils.replace(trama, "\"_m\"", "\"tipoDocumento\""     );
    	trama = StringUtils.replace(trama, "\"_n\"", "\"nroDocumento\""      );
    	trama = StringUtils.replace(trama, "\"_o\"", "\"email\""             );
    	trama = StringUtils.replace(trama, "\"_p\"", "\"tipo\""              );
    	trama = StringUtils.replace(trama, "\"_q\"", "\"idTrxHuellaDigital\"");
    	trama = StringUtils.replace(trama, "\"_r\"", "\"nombreApellido\""    );
    	trama = StringUtils.replace(trama, "\"_s\"", "\"listaItems\""        );
    	trama = StringUtils.replace(trama, "\"_t\"", "\"listaClientes\""     );
    	
    	ObjectMapper mapper = new ObjectMapper();
    	SimpleModule module = new SimpleModule();
    	module.addDeserializer(ItemContrato.class, new CustomItemContratoDeserializer());
    	mapper.registerModule(module);
    	try {
    		return mapper.readValue(trama,RequestDTO.class);
    		
		} catch (Exception e) {
			try {
				return getTramaOld(trama);
			} catch (Exception e2) {
				throw new ServiceException("Error en parseo de trama");
			}
		}
    }
    
    private RequestDTO getTramaOld(String trama) throws Exception{
    	RequestDTO firmaContrato = new RequestDTO();
		Integer estadoFormato = 1; // CAB=1 o PAR=2
		Integer estado = 0;
		Integer index=0;
		List<ItemContrato> listaItems = new ArrayList<ItemContrato>();
		
		String[] arryTrama = (trama).split(Constants.TEXTO_EXTRACION_SEPARACION_TRAMA);
		for (int j = 0; j < arryTrama.length; j++) {
			String line = arryTrama[j].trim();
			switch (estadoFormato) {
			case 1:
				if (line.equals(Constants.LABEL_CABECERA_INICIO)) {
					estadoFormato = 1;
					estado = 1;
					listaItems = new ArrayList<ItemContrato>();
					firmaContrato = new RequestDTO();
				} else {
					if (line.equals(Constants.LABEL_CABECERA_FIN)) {
						estado = 2;
						estadoFormato = 2;
						ItemContrato[] itemsContrato = new ItemContrato[listaItems
								.size()];
						listaItems.toArray(itemsContrato);
						firmaContrato.setIdContrato(buscarParametro(
								itemsContrato,
								Constants.COD_PARAM_IDFORMATO));
						firmaContrato
								.setNumeroContrato(buscarParametro(
										itemsContrato,
										Constants.COD_PARAM_NROCONTRATO));
						firmaContrato
								.setOficinaGestora(buscarParametro(
										itemsContrato,
										Constants.COD_PARAM_OFIGESTOR));
						firmaContrato.setProcedencia(buscarParametro(
								itemsContrato,
								Constants.COD_PARAM_CODPROCEDEN));
						firmaContrato.setGestDoc(buscarParametro(
								itemsContrato,
								Constants.COD_PARAM_GESTDOC_FILEUNICO));
						firmaContrato.setIdGrupo(buscarParametro(
								itemsContrato,
								Constants.COD_PARAM_GRUPO));
					} else {
						if (estado == 1) {
							processLineItem(line, index,listaItems);
							index++;
						}
					}
				}
				break;
			case 2:
				if (line.equals(Constants.LABEL_GENERAL_INICIO)) {
					estadoFormato = 2;
					estado = 1;
					listaItems = new ArrayList<ItemContrato>();
				} else {
					if (line.equals(Constants.LABEL_GENERAL_FIN)) {
						estado = 2;
						estadoFormato = 3;
						ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
						listaItems.toArray(itemsContrato);
						for (int t = 0; t < itemsContrato.length; t++) {
							firmaContrato.getListaItems().add(itemsContrato[t]);
						}
					} else {
						if (estado == 1) {
							processLineItem(line, index,listaItems);
							index++;
						}
					}
				}
				break;
			case 3:
				if (line.equals(Constants.LABEL_PARTICIPE_INICIO)) {
					estadoFormato = 3;
					estado = 1;
					listaItems = new ArrayList<ItemContrato>();
				} else {
					if (line.equals(Constants.LABEL_PARTICIPE_FIN)) {
						estado = 2;
						ItemContrato[] itemsContrato = new ItemContrato[listaItems.size()];
						listaItems.toArray(itemsContrato);
						Cliente cliente = construirCliente1(itemsContrato);
						for (int t = 0; t < itemsContrato.length; t++) {
							if(cliente!=null) {
								cliente.getListaItems().add(itemsContrato[t]);
							}
						}
						firmaContrato.getListaClientes().add(cliente);
					} else {
						if (estado == 1) {
							processLineItem(line, index,listaItems);
							index++;
						}
					}
				}
				break;
			}
		}
		return firmaContrato;
    }
    
    private static Cliente construirCliente1(ItemContrato[] itemsContrato) {
		return construirCliente(itemsContrato, Constants.COD_PARAM_CODCEN_TIT1,
				Constants.COD_PARAM_TIPDOC_TIT1,
				Constants.COD_PARAM_NRODOC_TIT1,
				Constants.COD_PARAM_EMAIL_TIT1, Constants.COD_PARAM_TRX_HUELLA,
				Constants.COD_PARAM_NOMBREAPELIDO_TIT1);
	}

	private static Cliente construirCliente(ItemContrato[] itemsContrato,
			String paramCodCen, String paramTipDoc, String paramNroDoc,
			String paramEmail, String paramTrxHuella, String paramNombreApellido) {
		Cliente cliente = new Cliente();
		for (int i = 0; i < itemsContrato.length; i++) {
			if (paramCodCen.equals(itemsContrato[i].getLabel())) {
				cliente.setCodigoCentral(itemsContrato[i].getValue());
			} else if (paramTipDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setTipoDocumento(itemsContrato[i].getValue());
			} else if (paramNroDoc.equals(itemsContrato[i].getLabel())) {
				cliente.setNroDocumento(itemsContrato[i].getValue());
			} else if (paramEmail.equals(itemsContrato[i].getLabel())) {
				cliente.setEmail(itemsContrato[i].getValue());
			} else if (paramTrxHuella.equals(itemsContrato[i].getLabel())) {
				cliente.setIdTrxHuellaDigital(itemsContrato[i].getValue());
			} else if (paramNombreApellido.equals(itemsContrato[i].getLabel())) {
				cliente.setNombreApellido(itemsContrato[i].getValue());
			}
		}
		if (cliente.getCodigoCentral() == null
				|| cliente.getCodigoCentral().length() == 0) {
			return null;
		}
		return cliente;
	}
    
    private static String buscarParametro(ItemContrato[] itemsContrato,
			String param) {
		for (int i = 0; i < itemsContrato.length; i++) {
			if (param.equals(itemsContrato[i].getLabel())) {
				return itemsContrato[i].getValue();
			}
		}
		return null;
	}
    
    public static void processLineItem(String aLine, int index, List<ItemContrato> listaItems) throws StringException  {
    	
    	
    	if (aLine != null && aLine.length() > 0) {
    		if (aLine.length() > (Constants.LABEL_LENGTH - 1)) {
				String label = aLine.substring(0, Constants.LABEL_LENGTH);
				String value = aLine.substring(Constants.LABEL_LENGTH);
				if (value != null && !value.equals("\\")) {
					value = StringUtils.replace(value, "\\", "");
					listaItems.add(Util.newIC(label, value));
				}
			} else {
				
				throw new StringException(
						"La linea debe tener longitud mayor de 5 caracteres. Linea "
								+ (index + 1));
			}
		} else {
			
			throw new StringException("La linea se encuentra vacia. Linea "
					+ (index + 1));
		}
    
    }
    
    private String getTramaJson(FirmaContrato firmaContrato) {
    	ObjectMapper mapper = new ObjectMapper();
    	try {
    		SimpleModule module = new SimpleModule("ItemContratoSerializer", new Version(1, 0, 0, null, null, null));
    		module.addSerializer(ItemContrato.class, new CustomItemContratoSerializer());
    		mapper.registerModule(module);
    		String jsonResult = mapper.writeValueAsString(firmaContrato);
    		jsonResult = StringUtils.replace(jsonResult, "\"numeroContrato\""    , "\"_c\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"oficinaGestora\""    , "\"_d\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"procedencia\""       , "\"_e\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"plantillaCorreo\""   , "\"_f\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"grupoCorreo\""       , "\"_g\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"idContrato\""        , "\"_h\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"idGrupo\""           , "\"_i\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"async\""             , "\"_j\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"gestDoc\""           , "\"_k\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"codigoCentral\""     , "\"_l\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"tipoDocumento\""     , "\"_m\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"nroDocumento\""      , "\"_n\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"email\""             , "\"_o\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"tipo\""              , "\"_p\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"idTrxHuellaDigital\"", "\"_q\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"nombreApellido\""    , "\"_r\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"listaItems\""        , "\"_s\"");
    		jsonResult = StringUtils.replace(jsonResult, "\"listaClientes\""     , "\"_t\"");
			return jsonResult;
		} catch (JsonProcessingException e) {
			return getTrama(firmaContrato);
		}
    }
    
	private String getTrama(FirmaContrato firmaContrato) {

		String gestDoc=firmaContrato.getGestDoc();
		String idGrupo=firmaContrato.getIdGrupo();
		
		if (firmaContrato.getGestDoc()==null)
		{
			gestDoc="";
		}
		
		if (firmaContrato.getIdGrupo()==null)
		{
			idGrupo="";
		}
		
		String cadenaCab = Constants.LABEL_CABECERA_INICIO + Constants.TEXTO_SEPARACION_TRAMA
				+ Constants.COD_PARAM_NROCONTRATO
				+ firmaContrato.getNumeroContrato() + Constants.TEXTO_SEPARACION_TRAMA
				+ Constants.COD_PARAM_OFIGESTOR
				+ firmaContrato.getOficinaGestora() + Constants.TEXTO_SEPARACION_TRAMA
				+ Constants.COD_PARAM_CODPROCEDEN
				+ firmaContrato.getProcedencia() + Constants.TEXTO_SEPARACION_TRAMA
				+ Constants.COD_PARAM_IDFORMATO + firmaContrato.getIdContrato()
				+ Constants.TEXTO_SEPARACION_TRAMA + Constants.COD_PARAM_GESTDOC_FILEUNICO
				+ gestDoc + Constants.TEXTO_SEPARACION_TRAMA + Constants.COD_PARAM_GRUPO
				+ idGrupo + Constants.TEXTO_SEPARACION_TRAMA
				+ Constants.LABEL_CABECERA_FIN;

		StringBuilder sb = new StringBuilder();
		sb.append(cadenaCab + Constants.TEXTO_SEPARACION_TRAMA);

		if (firmaContrato.getListaItems() != null) {
			sb.append(Constants.LABEL_GENERAL_INICIO + Constants.TEXTO_SEPARACION_TRAMA);
			for (ItemContrato cl : firmaContrato.getListaItems()) {
				sb.append(cl.getLabel() + cl.getValue() + Constants.TEXTO_SEPARACION_TRAMA);
			}
			sb.append(Constants.LABEL_GENERAL_FIN );
		}

		for (int i = 0; i < firmaContrato.getListaClientes().size(); i++) {
			if (firmaContrato.getListaClientes().get(i).getListaItems() != null) {
				sb.append(Constants.TEXTO_SEPARACION_TRAMA+Constants.LABEL_PARTICIPE_INICIO + Constants.TEXTO_SEPARACION_TRAMA);
				for (ItemContrato cl : firmaContrato.getListaClientes().get(i)
						.getListaItems()) {
					sb.append(cl.getLabel() + cl.getValue() + Constants.TEXTO_SEPARACION_TRAMA);
				}
				sb.append(Constants.LABEL_PARTICIPE_FIN);
			}
		}

		return sb.toString();
	}
	
	public void procesaError(Trama trama,String indicadorProceso) {
		try {
			if (indicadorProceso.equals(Constants.TIPO_PROCESO_FIRMA_DIGITAL  )){
				tramaService.insertaTrama(trama) ;
			}else{
				tramaService.updateTrama(trama) ;
			}
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
	}
	 	
	public Trama generaTrama(
			FirmaContrato firmaContrato,
			String cliente,
			ServiceException error,
			Integer cdEnv,
			String estado,
			Integer numReproceso,
			Timestamp timeReproceso
			) {
		Trama trama = new Trama();
		try {

			trama.setCdCliente(cliente);
			trama.setCdDoc(firmaContrato.getNumeroContrato());
			trama.setCdEnv(cdEnv);
			trama.setChTipo(Constants.TIPO_TRAMA_ERROR);
			trama.setChProcedencia(firmaContrato.getProcedencia());
			trama.setCdStep(error.getPaso().getPaso());
			trama.setChMoti(getMensaje(error));
			trama.setDetStep(error.getPaso().getDetPaso());

			trama.setEstado(estado);
			trama.setNumReproceso(numReproceso);
			trama.setTimeReproceso(timeReproceso);

			String tramaTotal = getTrama(firmaContrato);
			trama.setChTrama(tramaTotal);
			
		} catch (Exception e) {
			logger.debug(AppUtil.getDetailedException(e).toString());
		}
		return trama;
	}
	
	
	@Async("asyncExecutor")
	 public void processPrueba(int i) throws ServiceException, InterruptedException{
 
		System.out.println("entroa al hilo: " + i);
		
		Thread.sleep(1000);
		System.out.println("salio de hilo: " + i);
		 
	}
	
	
	

}