package com.bbva.cuentas.service.impl;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;



import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.service.exception.SignBoxException;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.SignContracts;
import com.bbva.cuentas.enums.ProcesoEnvioContrato;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.service.SignContractService;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

@Service
public class SignContractServiceImpl implements SignContractService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Value("${signcontract.endpoint}")
	private String endpoint;
	@Value("${flag.signBox.service}")
	private boolean flagService;
	@Value("${flag.signBox.limpiartemporal}")
	private boolean flagEliminarTemporal;
	
	@Autowired
	private Util util;
	
	SimpleDateFormat formatYYYYHHmm = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");

	private String generarTramaDocumentos(List<Plantilla> plantillas,ClienteDTO cliente) throws IOException {
		
		JSONObject jo = new JSONObject();
		String encodedBase64 = null;
		int contTrama = 0;
		FileInputStream fileInputStreamReader=null;
		try {
			for (Plantilla template : plantillas) {
				if (template.isValida()	&& Constants.ST_ACTIVO.equals(template.getStSigned())) {
					ConfigurationPath cfg = cliente.getCfg().get(template.getCdPlantilla());
					
						File originalFile = new File(cfg.getRutaSalidaPDF());
						fileInputStreamReader = new FileInputStream(originalFile);
						byte[] bytes = new byte[(int) originalFile.length()];
						int count=fileInputStreamReader.read(bytes);
						if(count>0) {
							contTrama = contTrama + 1;
							encodedBase64 = new String(Base64.encodeBase64(bytes));
							jo.put(Constants.FORMATO_BBVA + template.getCdPlantilla(), encodedBase64);
						}
						fileInputStreamReader.close();
					
				}
			}
			if (contTrama == 0) return null;
			return jo.toString();
		
		}finally {
			try {if (fileInputStreamReader!=null) fileInputStreamReader.close();} catch (IOException e) {logger.error(ErrorCodes.DESC_ERROR_CERRAR_STREAM,e);}
		}
	}

	public SignContracts signContracts(List<Plantilla> plantillas,
			FirmaContrato firmaContrato, ClienteDTO cliente)
			throws ServiceException {
		
		SignContracts result = new SignContracts();
		result.setEstado(Constants.ESTADO_SIGBOX_EXITOSO);
		String ss = formatYYYYHHmm.format(new java.util.Date());
		OutputStream out =null;
		String numeroContrato=null;
		if (flagService) {
			try {
				if (generarTramaDocumentos(plantillas, cliente) != null) {

					RestTemplate restTemplate = new RestTemplate();
					JSONObject mainObj = new JSONObject();
					mainObj.put("codigoArea", "ARQ");
					mainObj.put("contrato",
							generarTramaDocumentos(plantillas, cliente));
					mainObj.put("tipoDocumentoCliente", cliente.getCliente()
							.getTipoDocumento());
					mainObj.put("numeroDocumentoCliente", cliente.getCliente()
							.getNroDocumento());

					numeroContrato=cliente.getCliente().getCodigoCentral() + ss;
					if (!StringUtils.isEmpty(firmaContrato.getNumeroContrato())) {
						numeroContrato=firmaContrato.getNumeroContrato();
						
					} 
					
					mainObj.put(Constants.NUMERO_SOL, numeroContrato);

					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_JSON);
					HttpEntity<String> entity = new HttpEntity<String>(
							mainObj.toString(), headers);

					result = restTemplate.postForObject(endpoint, entity,
							SignContracts.class);
					
					
					if (result.getEstado().equals(
						Constants.ESTADO_SIGBOX_EXITOSO)) {
						procesaArchivos( plantillas, cliente, result);
				     }
				}

			} catch (Exception e) {
				logger.error(AppUtil.getDetailedException(e).toString());
				result.setEstado(Constants.ESTADO_SIGBOX_ERROR);
				return result;
			} finally {
				try {if (out!=null) out.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
			}
		}
		return result;
	}
	
	
	public void procesaArchivos(List<Plantilla> plantillas,
			 ClienteDTO cliente,SignContracts result)
			 {
		
		OutputStream out =null;
		try {
			
		
			
			for (Plantilla template : plantillas) {
				if (template.isValida()	&& Constants.ST_ACTIVO.equals(template.getStSigned())) {
					
					ConfigurationPath cfg = cliente.getCfg().get(
							template.getCdPlantilla());
						
						out = new FileOutputStream(cfg.getRutaSalidaFirma());
													
						out.write(Base64.decodeBase64(new JSONObject(
								result.getDocumento())
								.getString("BBVAFormato"
										+ template.getCdPlantilla()
										+ ".pdf")));
						out.close();
						cfg.setStep(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso());
						//cliente.getCfg().get(template.getCdPlantilla()).setStep(ProcesoEnvioContrato.FIRMA_DIGITAL_PDF_GENERAL.getPaso());
						
						if (flagEliminarTemporal) {
							FileUtils.deleteQuietly(new File(cfg.getRutaSalidaPDF()));
						}
				}
			}
		
		
		} catch (FileNotFoundException e) {
		
			
			logger.error(AppUtil.getDetailedException(e).toString());
		} catch (JSONException e) {
		
			logger.error(AppUtil.getDetailedException(e).toString());
		} catch (IOException e) {
			
			logger.error(AppUtil.getDetailedException(e).toString());
		} finally {
			try {if (out!=null) out.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
		}
		
	}
	
	

	public SignContracts otherSignContracts(List<Plantilla> plantillas,
			FirmaContrato firmaContrato, ClienteDTO cliente)
			throws ServiceException {
		logger.info("[ini]");
		SignContracts result = new SignContracts();
		result.setEstado(Constants.ESTADO_SIGBOX_EXITOSO);
		if (flagService) {
			String rutasBase = util.getRutas().get(Constants.RUTA_IDX_FIRMA);
			try {
				for (Plantilla template : plantillas) {
					procesaOtrosArchivos( template, cliente, rutasBase,ProcesoEnvioContrato.TRAMA_INICIAL.getPaso() );
				}
			} catch (Exception e) {
				logger.error(AppUtil.getDetailedException(e).toString());
				throw new SignBoxException(e.getMessage());
		    }
			return result;
		} else {
			for (Plantilla template : plantillas) {
				template.setStSigned(Constants.ST_INACTIVO);
			}
			return result;
		}
	}
	
	public SignContracts otherSignContracts(List<Plantilla> plantillas,
			FirmaContrato firmaContrato, ClienteDTO cliente,int paso)
			throws ServiceException {
		
		SignContracts result = new SignContracts();
		result.setEstado(Constants.ESTADO_SIGBOX_EXITOSO);
		if (flagService) {
			String rutasBase = util.getRutas().get(Constants.RUTA_IDX_FIRMA);
			try {
				for (Plantilla template : plantillas) {
					procesaOtrosArchivos( template, cliente, rutasBase, paso);
				}
			} catch (Exception e) {
				
				logger.error(AppUtil.getDetailedException(e).toString());
				throw new SignBoxException(e.getMessage());

			}
			return result;
		} else {
			for (Plantilla template : plantillas) {
				template.setStSigned(Constants.ST_INACTIVO);
			}
			return result;
		}
	}
	
	
	public void procesaOtrosArchivos(Plantilla template,ClienteDTO cliente,String rutasBase,int paso)
	{
		try {
		if (template.isValida()	&& Constants.ST_ACTIVO.equals(template.getStSigned())) {
			
			ConfigurationPath cfg = cliente.getCfg().get(template.getCdPlantilla());
			
		
				File file = new File(cfg.getRutaSalidaPDF());
				File dirDest = new File(rutasBase);
				
					FileUtils.moveFileToDirectory(file, dirDest, false);
				
				if (flagEliminarTemporal) {
					FileUtils.deleteQuietly(new File(cfg.getRutaSalidaPDF()));
				}
				cfg.setStep(paso);
						
		}
		
		} catch (IOException e) {
			
			logger.error(AppUtil.getDetailedException(e).toString());
		}
		
	}

}
