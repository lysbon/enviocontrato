package com.bbva.cuentas.service;


import java.sql.Timestamp;

import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.TramaHist;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.DetTramaHist;

import com.bbva.cuentas.dto.RequestDTO;


public interface TrazabilidadService {
	
	public TramaHist guardaTramaHist(
			RequestDTO request, 
			Kit kit,
    		String estado,
    		Timestamp fechaProceso
    		)throws ServiceException;
	
	 public void updateTramaHist(TramaHist tramaHist,String estado,Timestamp fechaFinProceso); 
	 
	 public DetTramaHist guardaDetTramaHist(
			 DetTrama detTrama, Timestamp fechaIniProceso,Timestamp fechaFinProceso,ServiceException error
	    		)throws ServiceException;
	 
	 public void updateDetTramaHist(DetTramaHist detTramaHist,String estado,Timestamp fechaFinProceso); 

}
