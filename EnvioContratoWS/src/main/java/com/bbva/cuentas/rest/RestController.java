package com.bbva.cuentas.rest;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bbva.cuentas.aat.service.EnviromentService;
import com.bbva.cuentas.aat.service.FlagPlantillaService;
import com.bbva.cuentas.aat.service.FlagService;
import com.bbva.cuentas.aat.service.ItemService;
import com.bbva.cuentas.aat.service.JdbcService;
import com.bbva.cuentas.aat.service.KitService;
import com.bbva.cuentas.aat.service.PlantillaService;
import com.bbva.cuentas.aat.service.ResourceService;
import com.bbva.cuentas.aat.service.SeccionService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.bean.EnvKit;
import com.bbva.cuentas.bean.Enviroment;
import com.bbva.cuentas.bean.Flag;
import com.bbva.cuentas.bean.FlagPlantilla;
import com.bbva.cuentas.bean.ItemSection;
import com.bbva.cuentas.bean.ItemStamp;
import com.bbva.cuentas.bean.Kit;
import com.bbva.cuentas.bean.KitPlantilla;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.bean.ResourceItem;

@Controller
public class RestController {

	private static final Logger logger = LoggerFactory.getLogger(RestController.class);
	
	@Autowired
	private KitService kitService;
	@Autowired
	private PlantillaService plantillaService;
	@Autowired
	private SeccionService seccionService;
	@Autowired
	private FlagPlantillaService flagPlantillaService;
	@Autowired
	private FlagService flagService;
	@Autowired
	private ItemService itemService;
	@Autowired
	private EnviromentService envService;
	@Autowired
	private ResourceService resourceService;
	@Autowired
	private JdbcService jdbcService;
	
	/************************
	 * 
	 * Kits
	 * 
	 *************************/
	
	@RequestMapping(value = "/kit/grupo/{cdEnv}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Kit>> kits(@PathVariable("cdEnv") Integer cdEnv) {
		logger.info("Obteniendo kits por grupo"); 
		List<Kit> kits = null;
		try {
			kits = kitService.obtenerKits(cdEnv);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Kit>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(kits == null){
			return new ResponseEntity<List<Kit>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Kit>>(kits, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/grupo", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarEnvKit(@RequestBody EnvKit envKit) {
		Integer result = null;
		try {
			result = kitService.guardarEnvKit(envKit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/grupo/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarEnvKit(@RequestBody EnvKit envKit) {
		Integer result = null;
		try {
			result = kitService.actualizarEnvKit(envKit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/grupo/eliminar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarEnvKit(@RequestBody EnvKit envKit) {
		Integer result = null;
		try {
			result = kitService.eliminarEnvKit(envKit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Kit> kit(@PathVariable Long id) {
		logger.info("Obteniendo kit por id"); 
		Kit kit = null;
		try {
			kit = kitService.obtenerKitPorId(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Kit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(kit == null){
			return new ResponseEntity<Kit>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Kit>(kit, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/codigo/{cdEnv}/{codigo}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Kit> kitCodigo(@PathVariable("cdEnv") Integer cdEnv,@PathVariable("codigo") String codigo) {
		logger.info("Obteniendo kit por codigo"); 
		Kit kit = null;
		try {
			kit = kitService.obtenerKitPorCodigo(cdEnv, codigo);
		} catch (ServiceException e) {
			return new ResponseEntity<Kit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(kit == null){
			return new ResponseEntity<Kit>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Kit>(kit, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/id/{cdEnv}/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Kit> kitIdGrupo(@PathVariable("cdEnv") Integer cdEnv,@PathVariable("id") Long id) {
		logger.info("Obteniendo kit por codigo"); 
		Kit kit = null;
		try {
			kit = kitService.obtenerKitPorId(cdEnv, id);
		} catch (ServiceException e) {
			return new ResponseEntity<Kit>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(kit == null){
			return new ResponseEntity<Kit>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Kit>(kit, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/correo/{cdEnv}/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> correo(@PathVariable("id") Long id,@PathVariable("cdEnv") Integer cdEnv) {
		logger.info("Obteniendo correo por id");
		String template = null;
		try {
			template = kitService.obtenerMailTemplate(id,cdEnv);
		} catch (ServiceException e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(template == null){
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(template, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/{id}/plantillas/asoc", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Plantilla>> plantillasAsocidas(@PathVariable Long id) {
		List<Plantilla> listaPlantillas;
		try {
			listaPlantillas = plantillaService.obtenerPlantillasAsociadas(id);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Plantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(listaPlantillas == null){
			return new ResponseEntity<List<Plantilla>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Plantilla>>(listaPlantillas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/{id}/plantillas", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Plantilla>> plantillasPorKit(@PathVariable Long id) {
		List<Plantilla> listaPlantillas;
		try {
			listaPlantillas = plantillaService.obtenerPlantillasPorKit(id);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Plantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Plantilla>>(listaPlantillas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarKit(@RequestBody Kit kit) {
		Long result = null;
		try {
			result = kitService.guardarKit(kit);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarKit(@RequestBody Kit kit) {
		Integer result = null;
		try {
			result = kitService.actualizarKit(kit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/{id}/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarKit(@PathVariable("id") Long id) {
		Integer result = null;
		try {
			result = kitService.eliminarKit(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/plantilla/add", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarKitPlantilla(@RequestBody KitPlantilla kitPlantilla){
		Integer result = null;
		try {
			result = kitService.guardarKitPlantilla(kitPlantilla);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/kit/plantilla/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarKitPlantilla(@RequestBody KitPlantilla kitPlantilla){
		Integer result = null;
		try {
			result = kitService.eliminarKitPlantilla(kitPlantilla);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/************************
	 * 
	 * Plantillas
	 * 
	 *************************/
	
	@RequestMapping(value = "/plantilla/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Plantilla>> plantillas() {
		List<Plantilla> plantillas = null;
		try {
			plantillas = plantillaService.obtenerPlantillas();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Plantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Plantilla>>(plantillas, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Plantilla> plantillas(@PathVariable("id") Long id) {
		Plantilla plantilla = null;
		try {
			plantilla = plantillaService.obtenerPlantillaPorCodigo(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Plantilla>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(plantilla == null){
			return new ResponseEntity<Plantilla>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Plantilla>(plantilla, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/{id}/pdf", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> plantillaPdf(@PathVariable Long id) {
		String template = null;
		try {
			template = plantillaService.obtenerPDFBase64(id);
		} catch (ServiceException e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(template == null){
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(template, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/pdf", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> actualizarPDFPlantilla(@RequestBody Plantilla plantilla) {
		Long result = null;
		try {
			plantillaService.actualizarPDFdesdeCadena(plantilla.getCdPlantilla(), plantilla.getLbPdfPlantilla());
			result = plantilla.getCdPlantilla();
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarPlantilla(@RequestBody Plantilla plantilla) {
		Long result = null;
		try {
			result = plantillaService.guardarPlantilla(plantilla);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarPlantilla(@RequestBody Plantilla plantilla) {
		Integer result = null;
		try {
			result = plantillaService.actualizarPlantilla(plantilla);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/delete/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarPlantilla(@PathVariable("id") Long id) {
		Integer result = null;
		try {
			result = plantillaService.eliminarPlantilla(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/***************
	 * SECCIONES
	 * **********/

	@RequestMapping(value = "/plantilla/{id}/seccion/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ItemSection>> secciones(@PathVariable("id") Long id) {
		logger.info("Obteniendo secciones por plantilla"); 
		List<ItemSection> secciones;
		try {
			secciones = seccionService.obtenerSeccionesPorPlantilla(id,null);
		} catch (ServiceException e) {
			return new ResponseEntity<List<ItemSection>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(secciones == null){
			return new ResponseEntity<List<ItemSection>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<ItemSection>>(secciones, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ItemSection> seccion(@PathVariable("id") Long id) {
		logger.info("Obteniendo seccion por id"); 
		ItemSection seccion;
		try {
			seccion = seccionService.obtenerSeccionPorId(id);
		} catch (ServiceException e) {
			return new ResponseEntity<ItemSection>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(seccion == null){
			return new ResponseEntity<ItemSection>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItemSection>(seccion, HttpStatus.OK);
	}

	@RequestMapping(value = "/seccion", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarSeccion(@RequestBody ItemSection seccion) {
		Long result = null;
		try {
			result = seccionService.guardarSeccion(seccion);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarSeccion(@RequestBody ItemSection seccion) {
		Integer result = null;
		try {
			result = seccionService.actualizarSeccion(seccion);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/seccion/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarSeccion(@PathVariable Long id) {
		Integer result = null;
		try {
			result = seccionService.eliminarSeccion(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/**********************
	 * Items
	 ***********************
	 */
	@RequestMapping(value = "/seccion/{id}/items", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ItemStamp>> items(
			@PathVariable("id") Long id) {
		logger.info("Obteniendo items por seccion"); 
		List<ItemStamp> listaItems;
		try {
			listaItems = itemService.obtenerItemsPorSeccion(id,null);
		} catch (ServiceException e) {
			return new ResponseEntity<List<ItemStamp>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(listaItems == null){
			return new ResponseEntity<List<ItemStamp>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<ItemStamp>>(listaItems, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<ItemStamp> item(@PathVariable("id") Long id) {
		logger.info("Obteniendo item por id"); 
		ItemStamp item;
		try {
			item = itemService.obtenerItemPorCodigo(id);
		} catch (ServiceException e) {
			return new ResponseEntity<ItemStamp>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(item == null){
			return new ResponseEntity<ItemStamp>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<ItemStamp>(item, HttpStatus.OK);
	}

	@RequestMapping(value = "/item", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarItem(@RequestBody ItemStamp item) {
		Long result = null;
		try {
			result = itemService.guardarItem(item);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/item/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarItem(@RequestBody ItemStamp item) {
		Integer result = null;
		try {
			result = itemService.actualizarItem(item);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/item/delete/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Integer> eliminarItem(@PathVariable Long id) {
		Integer result = null;
		try {
			result = itemService.eliminarItem(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/************************
	 * enviroments
	 */
	@RequestMapping(value = "/grupo/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Enviroment>> grupos() {
		logger.info("Obteniendo grupos"); 
		List<Enviroment> grupos;
		try {
			grupos = envService.obtenerEnvs();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Enviroment>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(grupos == null){
			return new ResponseEntity<List<Enviroment>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Enviroment>>(grupos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/tipo/{tipo}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Enviroment>> gruposPorTipo(@PathVariable("tipo") String tipo) {
		logger.info("Obteniendo grupos por tipo"); 
		List<Enviroment> grupos;
		try {
			grupos = envService.obtenerEnvPorTipo(tipo);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Enviroment>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(grupos == null){
			return new ResponseEntity<List<Enviroment>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Enviroment>>(grupos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Enviroment> env(@PathVariable("id") Integer id) {
		logger.info("Obteniendo item por id"); 
		Enviroment env;
		try {
			env = envService.obtenerEnvPorCodigo(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Enviroment>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(env == null){
			return new ResponseEntity<Enviroment>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Enviroment>(env, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/defecto", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Enviroment> envdefecto() {
		logger.info("Obteniendo grupo por id"); 
		Enviroment env;
		try {
			env = envService.obtenerEnvPorDefecto();
		} catch (ServiceException e) {
			return new ResponseEntity<Enviroment>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(env == null){
			return new ResponseEntity<Enviroment>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Enviroment>(env, HttpStatus.OK);
	}

	@RequestMapping(value = "/grupo", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarGrupo(@RequestBody Enviroment env) {
		Integer result = null;
		try {
			result = envService.guardarEnv(env);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarGrupo(@RequestBody Enviroment env) {
		Integer result = null;
		try {
			result = envService.guardarEnv(env);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/delete/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarGrupo(@PathVariable Integer id) {
		Integer result = null;
		try {
			result = envService.eliminarEnv(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/grupo/kit/add", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarGrupoKit(@RequestBody EnvKit envKit){
		Integer result = null;
		try {
			result = envService.guardarEnvKit(envKit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "/grupo/kit/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarGrupoKit(@RequestBody EnvKit envKit){
		Integer result = null;
		try {
			result = envService.eliminarEnvKit(envKit);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/**********************
	 * Recursos
	 ***********************
	 */
	@RequestMapping(value = "/recurso/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<ResourceItem>> recursos() {
		logger.info("Obteniendo recursos"); 
		List<ResourceItem> recursos;
		try {
			recursos = resourceService.obtenerImagenes();
		} catch (ServiceException e) {
			return new ResponseEntity<List<ResourceItem>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(recursos == null){
			return new ResponseEntity<List<ResourceItem>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<ResourceItem>>(recursos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/recurso/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<String> recurso(@PathVariable("id") Long id) {
		logger.info("Obteniendo recurso por id"); 
		String item;
		try {
			item = resourceService.obtenerImagenBase64(id);
		} catch (ServiceException e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(item == null){
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>(item, HttpStatus.OK);
	}

	@RequestMapping(value = "/recurso", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarRecurso(@RequestBody ResourceItem item) {
		Long result = null;
		try {
			result = resourceService.guardarRecurso(item);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/recurso/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarRecurso(@RequestBody ResourceItem item) {
		Integer result = null;
		try {
			result = resourceService.actualizarRecurso(item);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/recurso/delete/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarRecurso(@PathVariable Long id) {
		Integer result = null;
		try {
			result = resourceService.eliminarRecurso(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/**********************
	 * Flag
	 ***********************
	 */
	@RequestMapping(value = "/flag/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Flag>> flags() {
		logger.info("Obteniendo flags"); 
		List<Flag> flags;
		try {
			flags = flagService.obtenerFlags();
		} catch (ServiceException e) {
			return new ResponseEntity<List<Flag>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(flags == null){
			return new ResponseEntity<List<Flag>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Flag>>(flags, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/flag/all/{tipo}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<Flag>> flags(
			@PathVariable("tipo") String tipo) {
		logger.info("Obteniendo flags por tipo"); 
		List<Flag> listaFlags;
		try {
			listaFlags = flagService.obtenerFlagsPorTipo(tipo);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Flag>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(listaFlags == null){
			return new ResponseEntity<List<Flag>>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<Flag>>(listaFlags, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/flag/{id}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Flag> flag(@PathVariable("id") Long id) {
		logger.info("Obteniendo flag por id"); 
		Flag flag;
		try {
			flag = flagService.obtenerFlagPorCodigo(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Flag>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(flag == null){
			return new ResponseEntity<Flag>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Flag>(flag, HttpStatus.OK);
	}

	@RequestMapping(value = "/flag", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Long> guardarFlag(@RequestBody Flag flag) {
		Long result = null;
		try {
			result = flagService.guardarFlag(flag);
		} catch (ServiceException e) {
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Long>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/flag/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarFlag(@RequestBody Flag flag) {
		Integer result = null;
		try {
			result = flagService.actualizarFlag(flag);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/flag/delete/{id}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarFlag(@PathVariable Long id) {
		Integer result = null;
		try {
			result = flagService.eliminarFlag(id);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	/**********************
	 * FlagPlantilla
	 */
	
	@RequestMapping(value = "/plantilla/{id}/flag/all", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<List<FlagPlantilla>> flags(@PathVariable Long id) {
		logger.info("Obteniendo flags por plantilla"); 
		List<FlagPlantilla> listaFlags;
		try {
			listaFlags = flagPlantillaService.obtenerFlagsPorPlantilla(id);
		} catch (ServiceException e) {
			return new ResponseEntity<List<FlagPlantilla>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<FlagPlantilla>>(listaFlags, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/{id}/flag/{cdFlag}", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<FlagPlantilla> flagPlantilla(
			@PathVariable("id") Long cdPlantilla,@PathVariable("cdFlag") Long cdFlag) {
		logger.info("Obteniendo flagPlantilla por id"); 
		FlagPlantilla flag;
		try {
			flag = flagPlantillaService.obtenerFlagPlantilla(cdPlantilla, cdFlag);
		} catch (ServiceException e) {
			return new ResponseEntity<FlagPlantilla>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(flag == null){
			return new ResponseEntity<FlagPlantilla>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<FlagPlantilla>(flag, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/flag", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> guardarFlagPlantilla(@RequestBody FlagPlantilla flag) {
		Integer result = null;
		try {
			result = flagPlantillaService.guardarFlagPlantilla(flag);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/flag/update", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> actualizarFlagPlantilla(@RequestBody FlagPlantilla flag) {
		Integer result = null;
		try {
			result = flagPlantillaService.actualizarFlagPlantilla(flag);
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/plantilla/flag/delete", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Integer> eliminarFlagPlantilla(
			@RequestBody FlagPlantilla flag) {
		Integer result = null;
		try {
			result = flagPlantillaService.eliminarFlagPlantilla(flag.getCdPlantilla(), flag.getCdFlag());
		} catch (ServiceException e) {
			return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sql/query", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<List<Map<String, Object>>> query(@RequestBody String query) {
		logger.debug("query");
		List<Map<String, Object>> result = null;
		try {
			result = jdbcService.query(query);
		} catch (ServiceException e) {
			return new ResponseEntity<List<Map<String, Object>>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<Map<String, Object>>>(result, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sql/execute", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Map<String, Object>> execute(@RequestBody String sql) {
		Map<String, Object> result = null;
		try {
			result = jdbcService.execute(sql);
		} catch (ServiceException e) {
			return new ResponseEntity<Map<String, Object>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}
	
}