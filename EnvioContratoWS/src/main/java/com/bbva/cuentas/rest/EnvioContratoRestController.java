package com.bbva.cuentas.rest;



import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import com.bbva.cuentas.automatic.QuartzScheduler;
import com.bbva.cuentas.cargaDatos.ReprocesoAutomaticoService;
import com.bbva.cuentas.config.thread.AsyncConfiguration;
import com.bbva.cuentas.dto.Documentos;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.PivoteService;

import com.bbva.cuentas.service.ReprocesoService;
import com.bbva.cuentas.service.exception.ParametrosInvalidosException;
import com.bbva.cuentas.service.exception.PlantillaNoEncuentraException;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;


@Controller
public class EnvioContratoRestController {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Autowired
	private PivoteService pivoteService;
	@Autowired
	private ReprocesoService reprocesoService;
	@Autowired
	private ReprocesoAutomaticoService reprocesoAutomaticoService;
	@Autowired
	private QuartzScheduler quartzScheduler;
	
	@Autowired
	private AsyncConfiguration asyncConfiguration;
	
	@RequestMapping(value = "/envioContrato", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> envioContrato(@RequestBody RequestDTO request) {
		logger.info("Procesando envio de contrato");
		try {
			//System.out.println(request.getFechaProc());
			pivoteService.process(request);
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorCodes.S_ERROR_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/generarDocumentos", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Documentos> generarContratos(@RequestBody RequestDTO request) {
		Documentos documentos = null;
		try {
			documentos = pivoteService.processKitBase64(request);
		} catch (PlantillaNoEncuentraException e) {
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		} catch (ParametrosInvalidosException e) {
			return new ResponseEntity<Documentos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Documentos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(documentos == null){
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Documentos>(documentos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/generarDocumento", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<Documentos> generarContrato(@RequestBody RequestDTO request) {
		Documentos documentos = null;
		try {
			documentos = pivoteService.processPlantillaBase64(request);
		} catch (PlantillaNoEncuentraException e) {
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		} catch (ParametrosInvalidosException e) {
			return new ResponseEntity<Documentos>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<Documentos>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(documentos == null){
			return new ResponseEntity<Documentos>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Documentos>(documentos, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reprocesar", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> reprocesar() {
		logger.info("REPROCESANDO envio de contratos ");
		try {
			reprocesoAutomaticoService.process();
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorCodes.S_ERROR_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reprocesar/{from}/{to}", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> reprocesar(@PathVariable("from") Long from,@PathVariable("to") Long to) {
		logger.info("REPROCESANDO envio de contratos");
		try {
			reprocesoService.processfrom(from,to,null);
		} catch (Exception e) {
			return new ResponseEntity<String>(ErrorCodes.S_ERROR_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/restarSchedule", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> reprocesarAuto() {
		logger.info("Reprocesando Automaticamente Envio de contratos");
		try {
			quartzScheduler.reiniciarSchedule();
		} catch (Exception e) {
			
			return new ResponseEntity<String>(ErrorCodes.S_ERROR_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/restarThread", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<String> restarThread() {
		logger.info("Reprocesando Automaticamente Envio de contratos");
		try {
			asyncConfiguration.restarTasks();
		} catch (Exception e) {
			
			return new ResponseEntity<String>(ErrorCodes.S_ERROR_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
		return new ResponseEntity<String>(Constants.RESULT_OK, HttpStatus.OK);
	}

	
}
