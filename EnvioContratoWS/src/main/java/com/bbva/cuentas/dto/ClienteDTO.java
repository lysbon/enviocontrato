package com.bbva.cuentas.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bbva.cuentas.bean.Adjunto;
import com.bbva.cuentas.bean.ConfigurationPath;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.firmarcontratos.Cliente;

public class ClienteDTO {

	
	private Cliente cliente;
	private Map<Long,ConfigurationPath> cfgFileUnico;
	private Map<Long,ConfigurationPath> cfg;
	private Map<Long,List<ConfigurationPath>> listCfg;
	
	private Map<Long,List<ConfigurationPath>> listCfgFileUnico;
	
	private List<ConfigurationPath> listCfgEmail;
	private List<ConfigurationPath> listCfgFU;
	
	
	

	private boolean procesarFileUnico;
	private boolean procesarMail;
	
	private DetTrama detTrama;
	
	
     public ClienteDTO() {
		
		this.procesarFileUnico=true;
		this.procesarMail=true;
	}
	
	public DetTrama getDetTrama() {
		return detTrama;
	}
	public void setDetTrama(DetTrama detTrama) {
		this.detTrama = detTrama;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Map<Long, List<ConfigurationPath>> getListCfg() {
		if(listCfg==null)
			listCfg = new HashMap<Long, List<ConfigurationPath>>();
		return listCfg;
	}
	public void setListCfg(Map<Long,List<ConfigurationPath>> listCfg) {
		this.listCfg = listCfg;
	}
	
	
	public Map<Long, List<ConfigurationPath>> getListCfgFileUnico() {
		if(listCfgFileUnico==null)
			listCfgFileUnico = new HashMap<Long, List<ConfigurationPath>>();
		return listCfgFileUnico;
	}
	public void setListCfgFileUnico(Map<Long,List<ConfigurationPath>> listCfgFileUnico) {
		this.listCfgFileUnico = listCfgFileUnico;
	}
	
	
	
	
	public Map<Long, ConfigurationPath> getCfg() {
		if(cfg==null)
			cfg = new HashMap<Long, ConfigurationPath>();
		return cfg;
	}
	public void setCfg(Map<Long, ConfigurationPath> cfg) {
		this.cfg = cfg;
	}
	
	public Map<Long, ConfigurationPath> getCfgFileUnico() {
		if(cfgFileUnico==null)
			cfgFileUnico = new HashMap<Long, ConfigurationPath>();
		return cfgFileUnico;
	}
	public void setCfgFileUnico(Map<Long, ConfigurationPath> cfgFileUnico) {
		this.cfgFileUnico = cfgFileUnico;
	}
	
	public void addCfg(Long cdPlantilla,ConfigurationPath cfg) {
		if(getListCfg().containsKey(cdPlantilla)) {
			getListCfg().get(cdPlantilla).add(cfg);
		}else{
			getListCfg().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			getListCfg().get(cdPlantilla).add(cfg);
		}
	}
	
	public void addCfg(Long cdPlantilla,List<ConfigurationPath> listCfg) {
		if(!getListCfg().containsKey(cdPlantilla)) {
			
		
			getListCfg().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			
		}
		for(ConfigurationPath vcfg : listCfg) {
			getListCfg().get(cdPlantilla).add(vcfg);
		}
	}
	
	
	
	public void addCfgFileUnico(Long cdPlantilla,ConfigurationPath cfgFileUnico) {
		if(getListCfgFileUnico().containsKey(cdPlantilla)) {
			getListCfgFileUnico().get(cdPlantilla).add(cfgFileUnico);
		}else{
			getListCfgFileUnico().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			getListCfgFileUnico().get(cdPlantilla).add(cfgFileUnico);
		}
	}
	
	public void addCfgFileUnico(Long cdPlantilla,List<ConfigurationPath> listCfgFileUnico) {
		if(!getListCfgFileUnico().containsKey(cdPlantilla)) {
			
		
			getListCfgFileUnico().put(cdPlantilla, new ArrayList<ConfigurationPath>());
			
		}
		for(ConfigurationPath vcfg : listCfgFileUnico) {
			getListCfgFileUnico().get(cdPlantilla).add(vcfg);
		}
	}
	
	public List<ConfigurationPath> getListCfg(Long cdPlantilla) {
		if(listCfg==null)
			listCfg = new HashMap<Long, List<ConfigurationPath>>();
		return listCfg.get(cdPlantilla);
	}
	
	
	public List<ConfigurationPath> getListCfgFileUnico(Long cdPlantilla) {
		if(listCfgFileUnico==null)
			listCfgFileUnico = new HashMap<Long, List<ConfigurationPath>>();
		return listCfgFileUnico.get(cdPlantilla);
	}
	
	
	public boolean isProcesarFileUnico() {
		return procesarFileUnico;
	}
	public void setProcesarFileUnico(boolean procesarFileUnico) {
		this.procesarFileUnico = procesarFileUnico;
	}
	public boolean isProcesarMail() {
		return procesarMail;
	}
	public void setProcesarMail(boolean procesarMail) {
		this.procesarMail = procesarMail;
	}
	
	
	
	
	public List<ConfigurationPath> getListCfgFU() {
		if(listCfgFU==null)
			listCfgFU = new ArrayList<ConfigurationPath>();
		return listCfgFU;
	}

	public void setListCfgFU(List<ConfigurationPath> listCfgFU) {
		this.listCfgFU = listCfgFU;
	}
	
	
	public List<ConfigurationPath> getListCfgEmail() {
		
		if(listCfgEmail==null)
			listCfgEmail = new ArrayList<ConfigurationPath>();
		return listCfgEmail;
	}

	public void setListCfgEmail(List<ConfigurationPath> listCfgEmail) {
		this.listCfgEmail = listCfgEmail;
	}
	
}
