package com.bbva.cuentas.dto;

import java.util.List;
import java.util.Map;

import com.bbva.cuentas.firmarcontratos.FirmaContrato;

public class RequestDTO extends FirmaContrato{
	
	
	private List<Map<String,?>> contenido;
	private List<RequestDTO>    kits;

	public List<Map<String, ?>> getContenido() {
		return contenido;
	}

	public void setContenido(List<Map<String, ?>> contenido) {
		this.contenido = contenido;
	}

	public List<RequestDTO> getKits() {
		return kits;
	}

	public void setKits(List<RequestDTO> kits) {
		this.kits = kits;
	}
	
}
