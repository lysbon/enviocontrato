package com.bbva.cuentas.config.thread;

import java.util.concurrent.Executor;

import javax.annotation.Resource;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.ParametroService;
import com.bbva.cuentas.aat.service.impl.ParametroServiceImpl;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.cargaDatos.ReprocesoAutomaticoImpl;
import com.bbva.cuentas.listener.WebServletContextListener;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.ParamConstants;

@Configuration
@EnableAsync
@Service
public class AsyncConfiguration {
	
	
	
	//@Value("${backup.trazabilidad}")
	//@Value("#{new boolean('${backup.trazabilidad}')}")
	@Value("${threadpool.corepoolsize}")
	private int corePoolSize;
	 
	 
	 @Value("${threadpool.maxpoolsize}")
	 private int maxPoolSize;
	 
	 @Value("${threadpool.queuecapacity}")
	 private int maxCapacity;
	 
	@Autowired
	@Qualifier("asyncExecutor")
	private ThreadPoolTaskExecutor executorAsync;
	
	
	@Autowired
	ParametroService parametroService;
	
	private Parametro corePoolSizeParam = null;
	private Parametro reproIntentosParam = null;
	
	/*
	public void init() throws ServiceException {

		corePoolSizeParam = parametroService.obtenerValorById(ParamConstants.PRM_COREPOOLSIZE);
		reproIntentosParam = parametroService.obtenerValorById(ParamConstants.PRM_MAXPOOLSIZE);

		System.out.println(" corePoolSizeParaminit: " + corePoolSizeParam);
		System.out.println(" reproIntentosParaminit: " + reproIntentosParam);

		// AnnotationConfigApplicationContext context =
		// new AnnotationConfigApplicationContext(
		// AsyncConfiguration.class);

	}
	*/
	
	@Bean(name = "asyncExecutor")
	public ThreadPoolTaskExecutor asyncExecutor() throws ServiceException {

		executorAsync = new ThreadPoolTaskExecutor();
		executorAsync.setCorePoolSize(corePoolSize);
		executorAsync.setMaxPoolSize(maxPoolSize);

		executorAsync.setThreadNamePrefix("AsynchThread-");

		executorAsync.setWaitForTasksToCompleteOnShutdown(true);
		return executorAsync;
	}
	
	
	
	public void configura() throws ServiceException {

		corePoolSizeParam = parametroService.obtenerValorById(ParamConstants.PRM_COREPOOLSIZE);
		reproIntentosParam = parametroService.obtenerValorById(ParamConstants.PRM_MAXPOOLSIZE);

		executorAsync.setCorePoolSize(Integer.parseInt(corePoolSizeParam.getValorParam()));
		executorAsync.setMaxPoolSize(Integer.parseInt(corePoolSizeParam.getValorParam()));

		executorAsync.setThreadNamePrefix("AsynchThread2-");
		executorAsync.initialize();
		executorAsync.setWaitForTasksToCompleteOnShutdown(true);

	}
		
	
	public ThreadPoolTaskExecutor getExecutor() {
		return executorAsync;
	}

	public void setExecutor(ThreadPoolTaskExecutor executor) {
		this.executorAsync = executor;
	}
	
	
	
    public void restarTasks() throws ServiceException{
		executorAsync.shutdown(); 
        this.configura();
        
    }

}
