package com.bbva.cuentas.cargaDatos;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.bbva.cuentas.aat.service.DetTramaService;
import com.bbva.cuentas.aat.service.ParametroService;
import com.bbva.cuentas.aat.service.TramaService;
import com.bbva.cuentas.bean.DetTrama;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.bean.Trama;
import com.bbva.cuentas.config.thread.AsyncConfiguration;
import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.service.ErrorService;
import com.bbva.cuentas.service.ReprocesoService;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.Constants;
import com.bbva.cuentas.util.ErrorCodes;
import com.bbva.cuentas.util.ParamConstants;
import com.bbva.cuentas.util.Util;

@Service("reprocesoAutomaticoImpl")
public class ReprocesoAutomaticoImpl implements ReprocesoAutomaticoService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Autowired
	private ReprocesoService reprocesoService;

	@Autowired
	private ParametroService parametroService;

	@Autowired
	private TramaService tramaService;

	@Autowired
	private ErrorService errorService;
	
	@Autowired
	private DetTramaService detTramaService;

	@Autowired
	private AsyncConfiguration asyncConfiguration;

	public void process() throws ServiceException  {

		ThreadPoolTaskExecutor taskExecutor = asyncConfiguration.getExecutor();

		
		taskExecutor.initialize();

		List<Trama> listaTramas = null;
		Parametro reproIntentos = null;
		Parametro reproCantidad = null;
		Parametro reproFechaDesde = null;
		Parametro reproFechaHasta = null;
		int numIteraciones = 0;
		int cantidadTramas = 0;
		int cont = 0;

		try {

			reproFechaDesde = parametroService.obtenerValorById(ParamConstants.PRM_FECHA_DESDE);
			reproIntentos = parametroService.obtenerValorById(ParamConstants.PRM_ID_MAXINTENTOS);
			reproCantidad = parametroService.obtenerValorById(ParamConstants.PRM_ID_CANTIDAD);
			reproFechaHasta = parametroService.obtenerValorById(ParamConstants.PRM_FECHA_HASTA);

			tramaService.updateTramaporEstado(Constants.ESTADO_PROCESO_SELECCIONADO, Constants.ESTADO_PROCESO_ERROR,
					reproFechaDesde.getValorParam(), reproFechaHasta.getValorParam(),reproIntentos.getValorParam());
			cantidadTramas = tramaService.contarTramasPorEstado(Constants.ESTADO_PROCESO_SELECCIONADO,
					reproIntentos.getValorParam());

			numIteraciones = Util.obtenerIteraciones(cantidadTramas, Integer.parseInt(reproCantidad.getValorParam()));

			while (cont < numIteraciones) {
				cont++;

				listaTramas = tramaService.obtenerTramasReprocesar(Constants.ESTADO_PROCESO_SELECCIONADO,
						reproIntentos.getValorParam(), reproCantidad.getValorParam());
				logger.info("[num_tramas_reprocesar]: " + listaTramas.size());

				for (Trama trama : listaTramas) {
					trama.setEstado(Constants.ESTADO_PROCESO_PENDIENTE);
					tramaService.actualizarTrama(trama);
					RequestDTO peticion = reprocesoService.parse(trama.getChTrama());
					List<DetTrama> detalleCliente = detTramaService
							.obtenerDetTramasReprocesar(Constants.ESTADO_PROCESO_ERROR, trama.getIdTrama());

					reprocesoService.reprocess(peticion, detalleCliente, trama);

				}

			}

		} catch (Exception e) {
			logger.error(AppUtil.getDetailedException(e).toString());
			throw new ServiceException(ErrorCodes.S_ERROR_PIVOT_GENERAL,
					"No se pudo procesar peticion. " + e.getMessage());
		}

	}

}
