package com.bbva.cuentas.cargaDatos;

import com.bbva.cuentas.service.exception.ServiceException;

public interface ReprocesoAutomaticoService {
	public void process() throws ServiceException;
}
