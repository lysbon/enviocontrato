package com.bbva.cuentas.ws;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bbva.cuentas.dto.RequestDTO;
import com.bbva.cuentas.firmarcontratos.FirmarContratosRequest;
import com.bbva.cuentas.firmarcontratos.FirmarContratosResponse;
import com.bbva.cuentas.service.PivoteService;
import com.bbva.cuentas.util.Constants;

@Endpoint
public class FirmarContratosEndpoint implements MarshallingFirmarContratosService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	@Autowired
	private PivoteService pivotService;
	
	@PayloadRoot(namespace = NAMESPACE, localPart = FIRMAR_CONTRATOS_REQUEST)
	@ResponsePayload
	public FirmarContratosResponse firmarContratos(@RequestPayload FirmarContratosRequest request) {
		FirmarContratosResponse response = new FirmarContratosResponse();
		try {
			RequestDTO requestDTO = new RequestDTO();
			BeanUtils.copyProperties(requestDTO, request.getFirmaContrato());
			requestDTO.getListaItems().addAll(request.getFirmaContrato().getListaItems());
			requestDTO.getListaClientes().addAll(request.getFirmaContrato().getListaClientes());
			logger.info("[Ini] Servicio firmarContratos");
			pivotService.process(requestDTO);
			response.setCodigoResultado(Constants.RESULT_OK);
			response.setMensajeResultado(Constants.RESULT_OK_MSG);
			logger.info("[Fin] Servicio firmarContratos");
		} catch (Exception e) {
			logger.error("Error en firmarContratos",e);
			response.setCodigoResultado(Constants.RESULT_KO);
			response.setMensajeResultado(e.getMessage());
		}
		return response;
	}
	
}
