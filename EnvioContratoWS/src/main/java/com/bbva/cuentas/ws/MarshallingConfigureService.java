package com.bbva.cuentas.ws;

import com.bbva.cuentas.firmarcontratos.ConfigureRequest;
import com.bbva.cuentas.firmarcontratos.ConfigureResponse;

public interface MarshallingConfigureService {

	public final static String NAMESPACE = "http://bbva.com/cuentas/firmarContratos";
    public final static String CONFIGURE_REQUEST = "configure-request";

    /**
     * Gets person list.
     */
    public ConfigureResponse configure(ConfigureRequest request);
    
}
