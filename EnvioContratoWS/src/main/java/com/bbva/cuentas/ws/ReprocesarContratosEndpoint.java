package com.bbva.cuentas.ws;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bbva.cuentas.firmarcontratos.ReprocesarContratosRequest;
import com.bbva.cuentas.firmarcontratos.ReprocesarContratosResponse;
import com.bbva.cuentas.service.ReprocesarService;
import com.bbva.cuentas.util.Constants;

@Endpoint
public class ReprocesarContratosEndpoint implements MarshallingReprocesarContratosService {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	/*******04122017*********/
	@Autowired
	private ReprocesarService reprocesarService;
	/*******04122017*********/
	
	@PayloadRoot(namespace = NAMESPACE, localPart = REPROCESAR_CONTRATOS_REQUEST)
	@ResponsePayload
	public ReprocesarContratosResponse reprocesarContratos(@RequestPayload ReprocesarContratosRequest request) {
		ReprocesarContratosResponse response = new ReprocesarContratosResponse();
		try {			
			logger.info("[Inicio reprocesamiento de contratos v2.1]");
			/*******04122017*********/
			reprocesarService.process();
			/*******04122017*********/
			logger.info("[Fin reprocesamiento de contratos v2.1]");
			response.setCodigoResultado(Constants.RESULT_OK);
		} catch (Exception e) {
			logger.error("Error al reprocesar de contratos. ",e);			
			response.setCodigoResultado(Constants.RESULT_KO);
		}
		return response;
	}
	
}
