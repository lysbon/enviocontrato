package com.bbva.cuentas.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bbva.cuentas.enums.TipoInterviniente;
import com.bbva.cuentas.firmarcontratos.Cliente;
import com.bbva.cuentas.firmarcontratos.FirmaContrato;
import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.bbva.cuentas.bean.Plantilla;
import com.bbva.cuentas.dto.ClienteDTO;
import com.bbva.cuentas.dto.RequestDTO;

@Component
public class Util {
	
	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();
	
	@Value("${ruta.original}")
	private String rutaOriginal;
	@Value("${ruta.firma}")
	private String rutaFirma;
	@Value("${ruta.csv}")
	private String rutaCSV;
	@Value("${ruta.tempFileUnico}")
	private String rutaTempFileUnico;
	
	private Map<Integer,String> mapRutas;
	private String strFechaActual;

	@Value("${item.label.nomape}")
	private String labelNomApe;
	@Value("${item.label.tipdoc}")
	private String labelTipDoc;
	@Value("${item.label.nrodoc}")
	private String labelNroDoc;
	@Value("${item.label.nroCta}")
	private String labelNroCta;
		
	@Value("${item.label.ENTIDAD}")	
	private String labelNroCtaEntidad;
	@Value("${item.label.CENTRO_OFICINA}")
	private String labelNroCtaCentroOficina;
	@Value("${item.label.CUENTA}")
	private String labelNroCtaCuenta;
	@Value("${item.label.DIGITS}")
	private String labelNroCtaDigits;
	@Value("${item.label.NROCTAEDIT}")
	private String labelNroCtaEditado;
	
	public static void debug(Plantilla template,String mensajePlantilla){
		logger.debug(
			"Plantilla "+
			template.getCdPlantilla()+" - "+
			mensajePlantilla);
	}

	public static String getPath2(String name) {
		String contextPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		return new File(contextPath).getPath() + File.separatorChar + name;
	}
	
	public static String obtenerCodigoPlantilla(String name) {
		return name.substring(44,name.indexOf('.'));
	}

	public static String obtenerCodigoFileUnico(String name) {
		return name.substring(26,30);
	}
		
	public static String formatString(String string, String mask) throws java.text.ParseException {
		javax.swing.text.MaskFormatter mf = new javax.swing.text.MaskFormatter(mask);
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(string);
	}

	public Map<String, Object> toMap(RequestDTO request, Cliente cliente) {
		Map<String, Object> mapParametros = new HashMap<String, Object>();
		Map<String, Object> mapGeneral = toMap(request);
		Map<String, Object> mapCliente = toMap(cliente);
		mapParametros.putAll(mapGeneral);
		mapParametros.putAll(mapCliente);
		return mapParametros;
	}
	
	public Map<String, Object> toMap(Map<String, Object> mapGeneral, Cliente cliente) {
		Map<String, Object> mapParametros = new HashMap<String, Object>();
		Map<String, Object> mapCliente = toMap(cliente);
		mapParametros.putAll(mapGeneral);
		mapParametros.putAll(mapCliente);
		return mapParametros;
	}
	
	public Map<String, Object> toMap(List<ItemContrato> items) {
		Map<String,Object> mapParametros = new HashMap<String, Object>();
		for(ItemContrato ic : items){
			mapParametros.put(ic.getLabel(), ic.getValue());
		}
		return mapParametros;
	}
	
	public Map<String, Object> toMap(List<ItemContrato> items, String prefijo) {
		Map<String,Object> mapParametros = new HashMap<String, Object>();
		for(ItemContrato ic : items){
			mapParametros.put(prefijo+ic.getLabel(), ic.getValue());
		}
		return mapParametros;
	}
	
	public Map<String, Object> toMap(Cliente cliente) {
		Map<String, Object> parametros = toMap(cliente.getListaItems());
		parametros.put(labelNomApe, cliente.getNombreApellido());
		parametros.put(labelTipDoc, cliente.getTipoDocumento());
		parametros.put(labelNroDoc, cliente.getNroDocumento());
		return parametros;
	}	
	
	public Map<String, Object> toMap(RequestDTO firmaContrato) {
		Map<String, Object> parametros = toMap(firmaContrato.getListaItems());
		String nroContrato = obtenerContrato(parametros);
		if(!StringUtils.isEmpty(nroContrato))
			parametros.put(labelNroCta,nroContrato);
		if(firmaContrato.getKits()!=null && !firmaContrato.getKits().isEmpty()) {
			for(int i =0;i<firmaContrato.getKits().size();i++) {
				RequestDTO request = firmaContrato.getKits().get(i);
				String prefijo = String.format("L%02d",i);
				Map<String, Object> subparametros = toMap(request.getListaItems(),prefijo);
				subparametros.put("prefijo",prefijo);
				parametros.putAll(subparametros);
			}
		}
		return parametros;
	}
	
	public String obtenerContrato(Map<String, Object> parametros) {
		String str1 = Util.buscarCampo(parametros, labelNroCtaEntidad);
		String str2 = Util.buscarCampo(parametros, labelNroCtaCentroOficina);
		String str3 = Util.buscarCampo(parametros, labelNroCtaCuenta);
		if (!StringUtils.isEmpty(str3)) {
			return str1 + str2 + str3;
		}
		return null;
	}

	public String obtenerContratoEditado(Map<String,Object> parametros){
		String str0 = Util.buscarCampo(parametros,labelNroCtaEditado);
		if(!StringUtils.isEmpty(str0)){
			return str0;
		}
    	String str1 = Util.buscarCampo(parametros,labelNroCtaEntidad);
    	String str2 = Util.buscarCampo(parametros,labelNroCtaCentroOficina);
    	String str3 = Util.buscarCampo(parametros,labelNroCtaCuenta);
    	String str4 = Util.buscarCampo(parametros,labelNroCtaDigits);
    	return str1+"-"+str2+"-"+str4+"-"+str3;
    }

	public static ClienteDTO obtenerTitular(List<ClienteDTO> listaClientes) {
		for(ClienteDTO cliente : listaClientes){
			if(TipoInterviniente.TITULAR.value().equals(cliente.getCliente().getTipo())){
				return cliente;
			}
		}
		return listaClientes.get(0);
	}
	
	public static int obtenerIndiceTitular(FirmaContrato firmaContrato) {
		for(int i=0 ; i<firmaContrato.getListaClientes().size();i++){
			Cliente cliente = firmaContrato.getListaClientes().get(i);
			if(TipoInterviniente.TITULAR.value().equals(cliente.getTipo())){
				return i;
			}
		}
		return 0;
	}
	
	public static String buscarCampo(Map<String,Object> parametros, String label){
    	if(parametros!=null && parametros.containsKey(label))
    		return parametros.get(label).toString();
    	return "";
    }
	
	public Map<Integer,String> getRutas(){
		return mapRutas;
	}
	
	public Map<Integer,String> generarRutas(){
		logger.debug("INI Service: Ejecutando metodo generarRuta");		
		Date fechaActual = new Date();
		SimpleDateFormat formatYYYYMMDD = new SimpleDateFormat("yyyyMMdd", new Locale("es", "ES"));
		if(mapRutas!= null && !mapRutas.isEmpty() &&
		   StringUtils.equals(this.strFechaActual,formatYYYYMMDD.format(fechaActual))){
			return mapRutas;
		}else {
			String folderName = formatYYYYMMDD.format(fechaActual)+File.separatorChar;
			this.strFechaActual = formatYYYYMMDD.format(fechaActual);
			if(mapRutas==null) mapRutas = new HashMap<Integer,String>();
			mapRutas.clear();
			mapRutas.put(Constants.RUTA_IDX_ORIGINAL,rutaOriginal+folderName);
			mapRutas.put(Constants.RUTA_IDX_FIRMA,rutaFirma+folderName);
			mapRutas.put(Constants.RUTA_IDX_CSV,rutaCSV+folderName);
			mapRutas.put(Constants.RUTA_IDX_TEMPFILEUNICO,rutaTempFileUnico+folderName);
			for(Map.Entry<Integer,String> entry : mapRutas.entrySet()){
				try {
					FileUtils.forceMkdir(new File(entry.getValue()));
				} catch (IOException e) {
					logger.debug("Error al crear directorio. "+e.getMessage());
				}
			}
		}
		logger.debug("FIN Service: Ejecutando metodo generarRuta");
		return mapRutas;
	}
	
	public static String generarRuta(String ruta){
		logger.debug("INI Service: Ejecutando metodo generarRuta");		
		Date entrega = new Date();
		SimpleDateFormat formatYYYYMMDD = new SimpleDateFormat("yyyyMMdd", new Locale("es", "ES"));
		String folderName = formatYYYYMMDD.format(entrega)+File.separatorChar;
		String directoryName = ruta + folderName;
		try {
			FileUtils.forceMkdir(new File(directoryName));
		} catch (IOException e) {
			logger.debug("Error al crear directorio. "+e.getMessage());
		}
		logger.debug("FIN Service: Ejecutando metodo generarRuta");
		return directoryName;
	}
	
	public static String reemplazaCampos(String ruta,String oldChart,String newChart ){
		return ruta.replace(oldChart,newChart);
	}
	
	public static String obtieneStringPosicional(String cadena,int posicionInicial,int posicionFinal){
		return cadena.substring(posicionInicial,posicionFinal);
	}
	
	public static ItemContrato newIC(String label,String value){
		ItemContrato ic = new ItemContrato();
		ic.setLabel(label);
		ic.setValue(value);
		return ic;
	}

	public static void base64ToFile(String file,String encodedBase64) throws Exception{
		File f = new File(file);
		OutputStream out =null;
		try {
			if (f.exists() && !f.isDirectory()) {
				FileUtils.forceDelete(f);
			}
			out = new FileOutputStream(f);
			out.write(Base64.decodeBase64(encodedBase64));
			out.close();
		}catch(Exception e) {
			logger.error(e);
		}finally {
			try {if (out!=null) out.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
		}
	}
	
	public static String fileToBase64(String file) throws Exception{
		String encodedBase64 = null;
		File originalFile = new File(file);
		FileInputStream fileInputStreamReader=null;
		try {
			fileInputStreamReader = new FileInputStream(originalFile);
			byte[] bytes = new byte[(int) originalFile.length()];	
			int read = fileInputStreamReader.read(bytes);
			if(read>0) {
				encodedBase64 = new String(Base64.encodeBase64(bytes));
			}
			fileInputStreamReader.close();
		} catch (Exception e) {
			logger.error(e);
		} finally {
			try {if (fileInputStreamReader!=null) fileInputStreamReader.close();} catch (IOException e) {logger.error("Ocurrio un error al intentar cerrar stream",e);}
		}
		if(!StringUtils.isEmpty(encodedBase64)){
			return encodedBase64;
		}else {
			throw new Exception("Error en lectura de archivo "+file);
		}
	}
	

	public static int obtenerIteraciones(int numRegistro,int numRegParSeleccionar) {
		int ite=1;
		int res=0;
		if (numRegistro>numRegParSeleccionar)
		{
			ite = (int) numRegistro/numRegParSeleccionar; /* retorna 0.2*/
			res=(int) numRegistro%numRegParSeleccionar;
			
			if (res >0)
				ite=ite+1;
		}
		
		return ite;
	}

}
