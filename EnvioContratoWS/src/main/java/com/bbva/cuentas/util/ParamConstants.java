package com.bbva.cuentas.util;

import java.io.File;

public class ParamConstants {

	public static final String CD_KIT_PLANTILLA_DEFAULT1 = "020009";
	public static final String PROCEDENCIA_NACAR = "NA";

	public static final Long   KIT_CD_KIT_TRX = 9999l;
	public static final String KIT_CD_CODIGO_DEFAULT = "020009";
	public static final String KIT_CORREO_ASUNTO = "NOMBRENOMBRE";
	public static final String KIT_NOMBRE_KIT = "NOMBREKIT";
	public static final Integer KIT_CD_ENV = 1;
	public static final String KIT_CORREO_ASUNTO_2 = "NOMBRENOMBRE2";
	public static final String KIT_NOMBRE_KIT_2 = "NOMBREKIT2";
	public static final String TPL_HTML_RUTA1 = File.separatorChar+"strtpl"+File.separatorChar+"BG111.html";
	
	public static final Long   TPL_CD_PLANTILLA_CONSULTA = 1l;
	public static final Long   TPL_CD_PLANTILLA_TRX = 9999l;
	public static final String TPL_NOMBRE_FORMATO_GANADORA = "BG351";
	public static final String TPL_NOMBRE_FORMATO_EECC = "JP0001";
	public static final String TPL_TIPO_FILEUNICO = "1";	
	public static final String TPL_NOMBRE_FORMATO = "PRUEBA123";
	public static final String TPL_NOMBRE_CORREO = "Prueba 123";
	public static final String TPL_TIPO_PLANTILLA = "G";
	public static final String TPL_TIPO_FILEUNICO_2 = "2";	
	public static final String TPL_NOMBRE_FORMATO_2 = "PRUEBA1234";
	public static final String TPL_NOMBRE_CORREO_2 = "Prueba 1234";	
	public static final String TPL_PDF_RUTA1 = File.separatorChar+"pdf"+File.separatorChar+"contratos"+File.separatorChar+"BG111.pdf";
	public static final Integer TPL_CD_ENV = 1;
	
	public static final Long SEC_CD_PLANTILLA_CONSULTA = 1l;
	public static final Long SEC_CD_PLANTILLA_TRX = 1l;
	public static final Long SEC_CD_SECTION_DEL = 1l;	
	public static final Integer SEC_NUMPAG = 10;
	public static final Integer SEC_CD_ENV = 1;
	public static final Integer SEC_NUMPAG_2 = 11;
	public static final Integer SEC_CD_ENV_2 = 2;
	
	public static final Long    ITM_CD_ITEM=1l;
	public static final Long    ITM_CD_SECCION=1l;
	public static final String  ITM_ST_ACTIVO="A";
	public static final Integer ITM_NU_COOR_X=500;
	public static final Integer ITM_NU_COOR_Y=100;
	public static final Integer ITM_NU_COOR_X_2=800;
	public static final Integer ITM_NU_COOR_Y_2=200;
	public static final String  ITM_NB_NOMBRE_VAR="NB_NOMBRE_VAR";
	public static final String  ITM_NB_VALOR_VAR="NB_VALOR_VAR";
	public static final String  ITM_NB_NOMBRE_VAR_2="NB_NOMBRE_VAR_2";
	public static final String  ITM_NB_VALOR_VAR_2="NB_VALOR_VAR_2";
	public static final String  ITM_NB_FUENTE="NB_FUENTE";
	public static final Integer ITM_NU_ALINEAR=1;
	public static final String  ITM_CH_TIPO="T";
	public static final Integer ITM_NU_ANCHO=200;
	public static final Integer ITM_NU_SC_X=1;
	public static final Integer ITM_NU_SC_Y=1;
	public static final String  ITM_NB_MASK="NB_MASK";
	public static final Integer ITM_NU_IDX_CLIENTE=0;
	public static final Integer ITM_CD_ENV=1;
	
	public static final String ENV_CH_TIPO = "G";
	public static final String ENV_CH_TIPO_2 = "T";
	public static final String ENV_NB_DESCRIPCION = "ENV_NB_DESCRIPCION";
	public static final String ENV_NB_NOMBRE = "ENV_NB_NOMBRE";
	public static final String ENV_NB_NOMBRE_2 = "ENV_NB_NOMBRE2";
	public static final String ENV_ST_ACTIVO = "A";
	
	public static final String  FLG_CH_TIPO = "A";
	public static final String  FLG_NB_CONDICION_NOMBRE = "A1";
	public static final String  FLG_NB_CONDICION_VALOR = "A2";
	public static final String  FLG_NB_CONDICION_NOMBRE_2 = "A3";
	public static final String  FLG_NB_CONDICION_VALOR_2 = "A4";
	public static final Integer FLG_NU_ORDER = 1;
	public static final Integer FLG_CD_ENV = 1;
	
	public static final String FTP_ST_VALOR = "Z";
	

	public static final Integer PRM_TIPO_QUARTZ = 1;
	public static final String PRM_ID_CROM = "1";
	public static final String PRM_ID_MAXINTENTOS = "2";
	public static final String PRM_ID_FYHCORTE = "3";
	public static final String PRM_ID_CANTIDAD = "4";
	public static final String PRM_FECHA_DESDE = "5";
	public static final String PRM_COREPOOLSIZE = "6";
	public static final String PRM_MAXPOOLSIZE = "7";
	public static final String PRM_CAPACITY = "8";
	public static final String PRM_FECHA_HASTA = "9";

	public static String test() {
	      return "test";
	}

}
