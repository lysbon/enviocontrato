package com.bbva.cuentas.util;

import java.io.IOException;
import java.util.Iterator;


import com.bbva.cuentas.firmarcontratos.ItemContrato;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class CustomItemContratoDeserializer extends StdDeserializer<ItemContrato>{

	public CustomItemContratoDeserializer() {
        this(null);
    }
 
    public CustomItemContratoDeserializer(Class<ItemContrato> t) {
        super(t);
    }
    
	@Override
	public ItemContrato deserialize(JsonParser jp, DeserializationContext ctxt)
      throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        ItemContrato ic = new ItemContrato();
        Iterator<String> itFields = node.fieldNames();
        if(itFields.hasNext()) {
        	String field = itFields.next();
        	JsonNode contenido = node.get(field);
        	if(contenido.isValueNode()) {
        		ic.setLabel(field);
        		ic.setValue(contenido.asText());
        	}
        }
        return ic;
    }
	
}
