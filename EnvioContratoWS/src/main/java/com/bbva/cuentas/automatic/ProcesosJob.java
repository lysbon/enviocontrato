package com.bbva.cuentas.automatic;




import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;



import com.bbva.cuentas.service.impl.ReprocesoServiceImpl;
import com.bbva.cuentas.cargaDatos.ReprocesoAutomaticoImpl;
import com.bbva.cuentas.listener.WebServletContextListener;



public class ProcesosJob extends QuartzJobBean {

	ReprocesoAutomaticoImpl reprocesoAutomaticoImpl;

	private static final Logger logger = LoggerFactory.getLogger(ProcesosJob.class);

	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

		try {

			reprocesoAutomaticoImpl = (ReprocesoAutomaticoImpl) WebServletContextListener
					.getBean("reprocesoAutomaticoImpl");

			reprocesoAutomaticoImpl.process();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			logger.error(e.getMessage());

		}
	}

}
