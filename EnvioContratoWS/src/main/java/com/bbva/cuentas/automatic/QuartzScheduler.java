/**
 * COPYRIGHT (C) 2015 Andrew Liu. All Rights Reserved.
 * <p>
 * SpringDemo com.geekspearls.quartz.example2.QuartzScheduler
 *
 * @author Andrew Liu
 * @since 2015 24/07/2015 9:21 PM
 */
package com.bbva.cuentas.automatic;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.apache.logging.log4j.Logger;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bbva.cuentas.aat.service.ParametroService;
import com.bbva.cuentas.bean.Parametro;
import com.bbva.cuentas.config.thread.AsyncConfiguration;
import com.bbva.cuentas.service.exception.ServiceException;
import com.bbva.cuentas.util.AppUtil;
import com.bbva.cuentas.util.ParamConstants;


/**
 * @author Andrew
 */
public class QuartzScheduler {

	private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

	@Autowired
	ParametroService parametroService;

	private Scheduler scheduler;

	public void init() throws ServiceException {

		iniciaSchedule();
		
		//AnnotationConfigApplicationContext context =
         //       new AnnotationConfigApplicationContext(
          //     		 AsyncConfiguration.class);

	}

	public void reiniciarSchedule() throws ServiceException {
		try {
			scheduler.clear();
			iniciaSchedule();
		} catch (SchedulerException e) {
			logger.error(AppUtil.getDetailedException(e).toString());
		}

	}

	private void iniciaSchedule() throws ServiceException {

		Parametro parametro = parametroService.obtenerValorById(ParamConstants.PRM_ID_CROM);

		scheduleReproAuto("reprocesoAutomatico", parametro.getValorParam());

	}

	private void scheduleReproAuto(String procesos, String cron) {

		JobKey jobKey = new JobKey(procesos, procesos);

		JobDetail job = newJob(ProcesosJob.class).withIdentity(jobKey).build();

		TriggerKey triggerKey = TriggerKey.triggerKey(procesos, procesos);
		Trigger trigger = newTrigger().withIdentity(triggerKey).withSchedule(cronSchedule(cron)).forJob(job).build();

		try {
			scheduler.deleteJob(jobKey);
			scheduler.scheduleJob(job, trigger);
		} catch (SchedulerException e) {
			logger.error(AppUtil.getDetailedException(e).toString());

		}

	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	private enum Procesos {
		INFRACCIONES
	}

}
