//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BorrarDocumentoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BorrarDocumentoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BorrarDocumentoResponse" type="{http://ws.gesdoc.basics.continental.bbva.com/}RespuestaBorrarDocumento" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BorrarDocumentoResponse", propOrder = {
    "borrarDocumentoResponse"
})
public class BorrarDocumentoResponse {

    @XmlElement(name = "BorrarDocumentoResponse")
    protected RespuestaBorrarDocumento borrarDocumentoResponse;

    /**
     * Obtiene el valor de la propiedad borrarDocumentoResponse.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaBorrarDocumento }
     *     
     */
    public RespuestaBorrarDocumento getBorrarDocumentoResponse() {
        return borrarDocumentoResponse;
    }

    /**
     * Define el valor de la propiedad borrarDocumentoResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaBorrarDocumento }
     *     
     */
    public void setBorrarDocumentoResponse(RespuestaBorrarDocumento value) {
        this.borrarDocumentoResponse = value;
    }

}
