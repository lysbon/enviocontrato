//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BuscarDocumentosRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BuscarDocumentosRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="criteriosFiltro" type="{http://ws.gesdoc.basics.continental.bbva.com/}CriterioFiltro" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="propiedades" type="{http://ws.gesdoc.basics.continental.bbva.com/}metadatoDocumento" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maximoResultados" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="ordenacion" type="{http://ws.gesdoc.basics.continental.bbva.com/}CriterioOrdenacion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BuscarDocumentosRequest", propOrder = {
    "criteriosFiltro",
    "propiedades",
    "maximoResultados",
    "ordenacion"
})
public class BuscarDocumentosRequest {

    protected List<CriterioFiltro> criteriosFiltro;
    @XmlSchemaType(name = "string")
    protected List<MetadatoDocumento> propiedades;
    protected Integer maximoResultados;
    protected List<CriterioOrdenacion> ordenacion;

    /**
     * Gets the value of the criteriosFiltro property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the criteriosFiltro property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriosFiltro().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriterioFiltro }
     * 
     * 
     */
    public List<CriterioFiltro> getCriteriosFiltro() {
        if (criteriosFiltro == null) {
            criteriosFiltro = new ArrayList<CriterioFiltro>();
        }
        return this.criteriosFiltro;
    }

    /**
     * Gets the value of the propiedades property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propiedades property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropiedades().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadatoDocumento }
     * 
     * 
     */
    public List<MetadatoDocumento> getPropiedades() {
        if (propiedades == null) {
            propiedades = new ArrayList<MetadatoDocumento>();
        }
        return this.propiedades;
    }

    /**
     * Obtiene el valor de la propiedad maximoResultados.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMaximoResultados() {
        return maximoResultados;
    }

    /**
     * Define el valor de la propiedad maximoResultados.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMaximoResultados(Integer value) {
        this.maximoResultados = value;
    }

    /**
     * Gets the value of the ordenacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ordenacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrdenacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriterioOrdenacion }
     * 
     * 
     */
    public List<CriterioOrdenacion> getOrdenacion() {
        if (ordenacion == null) {
            ordenacion = new ArrayList<CriterioOrdenacion>();
        }
        return this.ordenacion;
    }

}
