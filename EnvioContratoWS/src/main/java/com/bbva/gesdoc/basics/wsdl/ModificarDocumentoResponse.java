//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ModificarDocumentoResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ModificarDocumentoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ModificarDocumentoResponse" type="{http://ws.gesdoc.basics.continental.bbva.com/}RespuestaModificarDocumento" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ModificarDocumentoResponse", propOrder = {
    "modificarDocumentoResponse"
})
public class ModificarDocumentoResponse {

    @XmlElement(name = "ModificarDocumentoResponse")
    protected RespuestaModificarDocumento modificarDocumentoResponse;

    /**
     * Obtiene el valor de la propiedad modificarDocumentoResponse.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaModificarDocumento }
     *     
     */
    public RespuestaModificarDocumento getModificarDocumentoResponse() {
        return modificarDocumentoResponse;
    }

    /**
     * Define el valor de la propiedad modificarDocumentoResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaModificarDocumento }
     *     
     */
    public void setModificarDocumentoResponse(RespuestaModificarDocumento value) {
        this.modificarDocumentoResponse = value;
    }

}
