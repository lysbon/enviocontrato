//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2018.03.16 a las 06:44:35 PM COT 
//


package com.bbva.gesdoc.basics.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CriterioFiltro complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CriterioFiltro"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="campo" type="{http://ws.gesdoc.basics.continental.bbva.com/}metadatoDocumento" minOccurs="0"/&gt;
 *         &lt;element name="operador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="operadorLogico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentesisDerecho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentesisIzquierdo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CriterioFiltro", propOrder = {
    "campo",
    "operador",
    "operadorLogico",
    "parentesisDerecho",
    "parentesisIzquierdo",
    "valor"
})
public class CriterioFiltro {

    @XmlSchemaType(name = "string")
    protected MetadatoDocumento campo;
    protected String operador;
    protected String operadorLogico;
    protected String parentesisDerecho;
    protected String parentesisIzquierdo;
    protected Object valor;

    /**
     * Obtiene el valor de la propiedad campo.
     * 
     * @return
     *     possible object is
     *     {@link MetadatoDocumento }
     *     
     */
    public MetadatoDocumento getCampo() {
        return campo;
    }

    /**
     * Define el valor de la propiedad campo.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadatoDocumento }
     *     
     */
    public void setCampo(MetadatoDocumento value) {
        this.campo = value;
    }

    /**
     * Obtiene el valor de la propiedad operador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperador() {
        return operador;
    }

    /**
     * Define el valor de la propiedad operador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperador(String value) {
        this.operador = value;
    }

    /**
     * Obtiene el valor de la propiedad operadorLogico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperadorLogico() {
        return operadorLogico;
    }

    /**
     * Define el valor de la propiedad operadorLogico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperadorLogico(String value) {
        this.operadorLogico = value;
    }

    /**
     * Obtiene el valor de la propiedad parentesisDerecho.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesisDerecho() {
        return parentesisDerecho;
    }

    /**
     * Define el valor de la propiedad parentesisDerecho.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesisDerecho(String value) {
        this.parentesisDerecho = value;
    }

    /**
     * Obtiene el valor de la propiedad parentesisIzquierdo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentesisIzquierdo() {
        return parentesisIzquierdo;
    }

    /**
     * Define el valor de la propiedad parentesisIzquierdo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentesisIzquierdo(String value) {
        this.parentesisIzquierdo = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValor(Object value) {
        this.valor = value;
    }

}
