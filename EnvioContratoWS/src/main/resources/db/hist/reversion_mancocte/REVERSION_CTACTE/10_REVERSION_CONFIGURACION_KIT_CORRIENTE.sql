/***********************************************/
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010001';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010002';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010008';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010091';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010092';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010041';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010042';
   
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010031';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010032';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010034';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010093';
   delete from "APDIGB"."TBG000_KIT" where CD_KIT='010094';
   
   commit;	

  

/********* reversion TBG002_PLANTILLA  *********/
DELETE FROM "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG281%' or nb_nombre_correo like '%BG281%' ;
DELETE FROM "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG011%' or nb_nombre_correo like '%BG011%' ;
DELETE FROM "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG481%' or nb_nombre_correo like '%BG481%' ;
DELETE FROM "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0A3%' or nb_nombre_correo like '%BG0A3%' ;
DELETE FROM "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG531%' or nb_nombre_correo like '%BG531%' ;

COMMIT;
/********* reversion TBG002_PLANTILLA  *********/



/********* Agregamos el indicador de mancomunada a la tabla TBG005_FLAG ******************/
DELETE FROM "APDIGB"."TBG005_FLAG" WHERE NB_CONDICION_NOMBRE like '%CFG0|P24%' or NB_CONDICION_NOMBRE like '%CTR0|P24%' ;

COMMIT;
/********* Agregamos el indicador de mancomunada a la tabla TBG005_FLAG ******************/