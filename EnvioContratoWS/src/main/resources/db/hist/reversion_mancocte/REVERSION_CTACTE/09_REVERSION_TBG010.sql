/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
DECLARE
    
    CURSOR C_TBG010_KIT IS select CD_KIT  from "APDIGB"."TBG010_KIT" ;
    
    V_CD_KIT     "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
    V_CD_PLANTILLA   "APDIGB"."TBG011_KIT_PLANTILLA".CD_PLANTILLA%TYPE;
     
    cont NUMBER;
   
BEGIN
   
    cont:=0;
    OPEN C_TBG010_KIT;
      LOOP
         FETCH C_TBG010_KIT  INTO V_CD_KIT;
         EXIT WHEN C_TBG010_KIT%NOTFOUND;         
           
           select COUNT(*) into cont from "APDIGB"."TBG011_KIT_PLANTILLA" WHERE CD_KIT=V_CD_KIT ;
            
           IF cont=0 THEN 
            
                 DELETE FROM "APDIGB"."TBG010_KIT" WHERE CD_KIT=V_CD_KIT  ; 
                 commit;
                  
           END IF;
        
    END LOOP;

  CLOSE C_TBG010_KIT;
   
END;


/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
