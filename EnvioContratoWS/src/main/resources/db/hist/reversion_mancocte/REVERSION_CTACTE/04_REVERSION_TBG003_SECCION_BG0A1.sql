/**** 4 PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/
set serveroutput on
DECLARE
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  
   CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0A3%' or nb_nombre_correo like '%BG0A3%' order by CD_PLANTILLA ;  
     
BEGIN
  
  OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_cd_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;  
           
		         delete from "APDIGB"."TBG003_SECCION" where cd_plantilla=v_cd_plantilla;  

				 COMMIT;
      END LOOP;

  CLOSE C_PLANTILLA;
    
 EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla para el BG0A3 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!');  
    
END;
/**** 4 PLANTILLA BG0A1 - FIRMA DE HUELLAS DACTILARES DEL CLIENTE *****/