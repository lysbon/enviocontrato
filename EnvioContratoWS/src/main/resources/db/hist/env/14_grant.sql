GRANT SELECT, UPDATE, INSERT, DELETE ON APDIGB.TBG009_ENV TO APP_APDIGB;
GRANT SELECT, UPDATE, INSERT, DELETE ON APDIGB.TBG010_KIT TO APP_APDIGB;
GRANT SELECT, UPDATE, INSERT, DELETE ON APDIGB.TBG011_KIT_PLANTILLA TO APP_APDIGB;
GRANT SELECT, UPDATE, INSERT, DELETE ON APDIGB.TBG012_ENV_KIT TO APP_APDIGB;
GRANT SELECT, UPDATE, INSERT, DELETE ON "APDIGB"."oauth_access_token" TO APP_APDIGB;
GRANT SELECT ON APDIGB.env_seq TO APP_APDIGB;
GRANT SELECT ON APDIGB.flg_seq TO APP_APDIGB;
GRANT SELECT ON APDIGB.kit_seq TO APP_APDIGB;