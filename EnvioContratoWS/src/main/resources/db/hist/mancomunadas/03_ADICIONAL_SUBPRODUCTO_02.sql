/********* 3. Cargar el adicional de la plantilla en los productos 02 **********/
DECLARE
  CURSOR c_kit is
    SELECT cd_kit 
    FROM   "APDIGB"."TBG000_KIT" 
    WHERE  cd_kit LIKE '02_%' 
      AND  cd_kit != '020092' 
      AND  cd_kit != '020093';
   
  v_kit "APDIGB"."TBG000_KIT".cd_kit%TYPE;
  v_plantillaAdicionalFirmas "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
BEGIN
  SELECT CD_PLANTILLA 
  INTO   v_plantillaAdicionalFirmas 
  FROM   "APDIGB"."TBG002_PLANTILLA" 
  WHERE  nb_nombre_formato LIKE '%BG0D1%' OR nb_nombre_correo LIKE '%BG0D1%';
  
  OPEN c_kit;
    LOOP
      FETCH c_kit INTO v_kit;
      EXIT WHEN c_kit%NOTFOUND;
       
      INSERT INTO "APDIGB"."TBG001_KIT_PLANTILLA"(CD_KIT,CD_PLANTILLA) VALUES (v_kit,v_plantillaAdicionalFirmas);
      COMMIT;
       
    END LOOP;
  CLOSE c_kit; 
END;
/********* 3. Cargar el adicional de la plantilla en los productos 02 **********/