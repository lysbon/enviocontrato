INSERT INTO APDIGB.TBG010_KIT (CD_KIT,CD_CODIGO,NB_CORREO_ASUNTO,LB_CORREO_PLANTILLA,
CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,ST_ENV_CORREO,ST_FILEUNICO,FL_VAL_HUELLA,NB_CORREO_ENGINE,NB_NOMBRE_KIT,ST_SIGNBOX,ST_HUELLA) 
VALUES 

(KIT_SEQ.NEXTVAL,'020001AH','Bienvenido a Mis Recomendados',TO_CLOB('\<center>
	\<table style="width:600px;border: 0px;color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">
	   \<tr>	
			\<td>
				\<img src="cid:image1.jpg" border="0"/>
			\</td>
		\</tr>
		\<tr>	
			\<td>\<p>
				Estimado (a) <CFG0P05>,
			\</p>\</td>
		\</tr>
		
		\<tr>	
		    
			\<td>
				    \<p style="padding-bottom: 10px;text-align: justify;line-height: 120%">Gracias por depositar tu confianza en nosotros. Tu afiliaci&oacute;n a &#8220;Mis Recomendados&#8221; ha sido satisfactoria. A partir de ahora cumple tus objetivos de ahorro y contar&aacute;s con una mejor tasa en tu cuenta.\</p>
			\</td>

		\</tr>
		
		
		\<tr>	
		    
			\<td>
				    \<p style="padding-bottom: 10px;text-align: justify;line-height: 120%">Detalle de la afiliaci&oacute;n:\</p>
			\</td>
		\</tr>
		
		\<tr>
			\<td>
				\<ul>
				    \<li style="color:#0070C0;">\<span style="color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">Estado: AFILIACI&Oacute;N EXITOSA\</span>\</li>
					
					\<li style="color:#0070C0;">\<span style="color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">Tipo de cuenta: <CFG0P26>\</span>\</li>
					
					\<li style="color:#0070C0;">\<span style="color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">Moneda: <CFG0P10>\</span>\</li>
					
  				    \<li style="color:#0070C0;">\<span style="color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">N&uacute;mero de Cuenta: <CFG0P01>\</span>\</li>
					
					
					\<li style="color:#0070C0;">\<span style="color:#7f7f7f;font-family:''Lucida Grande'', Arial, sans-serif;font-size:13px;line-height:21px;">Fecha y Hora de afiliaci&oacute;n: <CFG0P25>\</span>\</li>
					
				
			

				\</ul>
			\</td>
		\</tr>
		
		
		
		\<tr>
		 
		 \<td style="color:#2F4181;text-align:left; width:65%;">Atentamente,\</td>
		\</tr>
		
		\<tr>
		 
		 \<td >
			\<BR>
			\<div style="color:#2F4181;text-align:left; width:65%;">
				\<h4 style="color:#0064AE; font-size:14px; font-weight:bold;">BBVA Continental.\</h4>
			\</div>
		 \</td>
		\</tr>

	                         \<tr>
			\<td>
				\<img src="cid:image2.jpg" border="0">
			\</td>
		 \</tr>


		
		
		\<tr>
			\<td>\<p style="padding-bottom: 10px;text-align: justify;line-height: 120%">
				\<span style="color:#0070C0;font-size: 14px;">Nota : \</span>Para garantizar la entrega de los emails a su bandeja de entrada, por favor a&ntilde;ada a su libreta de direcciones nuestra direcci&oacute;n procesos@bbva.com 
			\</p>\</td>
		\</tr>
		
		\<tr>
			\<td>\<p style="padding-bottom: 10px;text-align: justify;line-height: 120%">
				  Si no desea que le lleguen nuestras promociones, por favor 
                  \<u style="color:#004c93;">
                                \<a href="https://register.masterbase.com/v0/bbvape/#!serial!#/#!contrasena!#/marcarusu?boletin=9&habilitado=1&opcion=1" 									target="blank">
                                click aqu&iacute;
                                \</a>
                  \</u>
			\</p>\</td>
		\</tr>
		
		
		
		
		\<tr>
			\<td>\<p style="padding-bottom: 10px;text-align: justify;line-height: 120%">
				Por tu seguridad, BBVA Continental te informa que:
			\</p>\</td>
		\</tr>
		
		\<tr>
			\<td>
				\<ul>
					\<li> Nunca solicitaremos tu datos confidenciales por correo, tales como la clave SMS de Internet, clave de cajero, DNI o tu n&uacute;mero de celular.	\</li>
					\<li>Si tienes alguna duda acerca de la autenticidad de este correo env&iacute;alo a la direcci&oacute;n bbvaseguridad.pe@bbva.com y te responderemos.\</li>
					
				\</ul>
			\</td>
		\</tr>
                                      \<tr>
                                            \<!--td style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: center;">
                                           ')||TO_CLOB('     Este correo electr&oacute;nico ha sido enviado a C006 
                                            \</td-->
                                        \</tr>
                                        \<tr>
                                           \<td >
                                                \<hr size="1" color="#becccc" />
                                            \</td>
                                         \</tr>
                                         \<tr>
                                             \<td  style="font-family: Verdana, Geneva, sans-serif; font-size: 10px; line-height: 12px; color: #4553AA; text-align: left;">
                                                Este correo electr&oacute;nico fue enviado por 
                                                \<strong>
                                                    BBVA Continental
                                                \</strong>
                                                \<br/>
                                                    Direcci&oacute;n: Rep&uacute;blica de Panam&aacute;; 3055 - San Isidro, Lima - Per&uacute;;
                                               \<br/>
                                                \<strong>
                                                    &copy; 2015 Derechos Reservados
                                               \</strong>
                                            \</td>
                                       \</tr>

	               
		
	\</table>
\</div>

 \</center>'),NULL,NULL,NULL,NULL,'A','A','A','STRTPL','KIT BG581 - AFILIACION MIS RECOMENDADOS',NULL,NULL)
;