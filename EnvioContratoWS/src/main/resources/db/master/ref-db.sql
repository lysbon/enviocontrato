--------------------------------------------------------
--  DDL for Index TBG001_KIT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG000_KIT_PK" ON "APDIGB"."TBG000_KIT" ("CD_KIT") 
  ;
--------------------------------------------------------
--  DDL for Index TBG001_KIT_PLT_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG001_KIT_PLT_PK" ON "APDIGB"."TBG001_KIT_PLANTILLA" ("CD_KIT", "CD_PLANTILLA") 
  ;
--------------------------------------------------------
--  DDL for Index TBG002_PLANTILLA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG002_PLANTILLA_PK" ON "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG002_PLANTILLA_HIST_PK" ON "APDIGB"."TBG002_PLANTILLA_HIST" ("CD_PLANTILLA", "FH_INICIO")
  ;
  
--------------------------------------------------------
--  DDL for Index TBG003_SECCION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG003_SECCION_PK" ON "APDIGB"."TBG003_SECCION" ("CD_SECCION") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG003_SECCION_HIST_PK" ON "APDIGB"."TBG003_SECCION_HIST" ("CD_SECCION","FH_INICIO") 
  ;
  
--------------------------------------------------------
--  DDL for Index TBG004_ITEM_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG004_ITEM_PK" ON "APDIGB"."TBG004_ITEM" ("CD_ITEM") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG004_ITEM_HIST_PK" ON "APDIGB"."TBG004_ITEM_HIST" ("CD_ITEM","FH_INICIO") 
  ;
--------------------------------------------------------
--  DDL for Index TBG005_FLAG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG005_FLAG_PK" ON "APDIGB"."TBG005_FLAG" ("CD_FLAG") 
  ;
--------------------------------------------------------
--  DDL for Index TBG006_FLAG_PLANTILLA_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG006_FLAG_PLANTILLA_PK" ON "APDIGB"."TBG006_FLAG_PLANTILLA" ("CD_FLAG", "CD_PLANTILLA", "ST_VALOR") 
  ;
--------------------------------------------------------
--  DDL for Index TBG007_ITEM_IMG_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "APDIGB"."TBG007_ITEM_IMG_PK" ON "APDIGB"."TBG007_ITEM_IMG" ("CD_ITEM") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG009_ENV_PK" ON "APDIGB"."TBG009_ENV" ("CD_ENV") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG010_KIT_PK" ON "APDIGB"."TBG010_KIT" ("CD_KIT") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG010_KIT_HIST_PK" ON "APDIGB"."TBG010_KIT_HIST" ("CD_KIT","FH_INICIO") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG011_KIT_PLT_PK" ON "APDIGB"."TBG011_KIT_PLANTILLA" ("CD_KIT","CD_PLANTILLA") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG011_KIT_PLANTILLA_HIST_PK" ON "APDIGB"."TBG011_KIT_PLANTILLA_HIST"  ("CD_KIT", "CD_PLANTILLA", "FH_INICIO") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG012_ENV_KIT_PK" ON "APDIGB"."TBG012_ENV_KIT"  ("CD_ENV", "CD_KIT") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."TBG013_DET_TRAMA_PK" ON "APDIGB"."TBG013_DET_TRAMA" ("ID_DET_TRAMA", "ID_TRAMA") 
  ;
  
  CREATE UNIQUE INDEX "APDIGB"."OAUTH_ACCESS_TKN_PK" ON "APDIGB"."oauth_access_token" ("authentication_id") 
  ;
  
  
--------------------------------------------------------
--  Constraints for Table TBG000_KIT
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG000_KIT" ADD CONSTRAINT "TBG000_KIT_PK" PRIMARY KEY ("CD_KIT");
--------------------------------------------------------
--  Constraints for Table TBG001_KIT_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_KIT_PLT_PK" PRIMARY KEY ("CD_KIT", "CD_PLANTILLA");
--------------------------------------------------------
--  Constraints for Table TBG002_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG002_PLANTILLA"      ADD CONSTRAINT "TBG002_PLANTILLA_PK"      PRIMARY KEY ("CD_PLANTILLA");
  ALTER TABLE "APDIGB"."TBG002_PLANTILLA_HIST" ADD CONSTRAINT "TBG002_PLANTILLA_HIST_PK" PRIMARY KEY ("CD_PLANTILLA","FH_INICIO");
--------------------------------------------------------
--  Constraints for Table TBG003_SECCION
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG003_SECCION"      ADD CONSTRAINT "TBG003_SECCION_PK"      PRIMARY KEY ("CD_SECCION");
  ALTER TABLE "APDIGB"."TBG003_SECCION_HIST" ADD CONSTRAINT "TBG003_SECCION_HIST_PK" PRIMARY KEY ("CD_SECCION","FH_INICIO");
--------------------------------------------------------
--  Constraints for Table TBG004_ITEM
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG004_ITEM"      ADD CONSTRAINT "TBG004_ITEM_PK"      PRIMARY KEY ("CD_ITEM");
  ALTER TABLE "APDIGB"."TBG004_ITEM_HIST" ADD CONSTRAINT "TBG004_ITEM_HIST_PK" PRIMARY KEY ("CD_ITEM","FH_INICIO");
--------------------------------------------------------
--  Constraints for Table TBG005_FLAG
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG005_FLAG" ADD CONSTRAINT "TBG005_FLAG_PK" PRIMARY KEY ("CD_FLAG");
--------------------------------------------------------
--  Constraints for Table TBG006_FLAG_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TBG006_FLAG_PLANTILLA_PK" PRIMARY KEY ("CD_FLAG", "CD_PLANTILLA", "ST_VALOR");
--------------------------------------------------------
--  Constraints for Table TBG007_ITEM_IMG
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG007_ITEM_IMG" ADD CONSTRAINT "TBG007_ITEM_IMG_PK" PRIMARY KEY ("CD_ITEM");
  
  ALTER TABLE "APDIGB"."TBG009_ENV" ADD CONSTRAINT "TBG009_ENV_PK" PRIMARY KEY ("CD_ENV");

  ALTER TABLE "APDIGB"."TBG010_KIT" ADD CONSTRAINT "TBG010_KIT_PK" PRIMARY KEY ("CD_KIT");

  ALTER TABLE "APDIGB"."TBG010_KIT_HIST" ADD CONSTRAINT "TBG010_KIT_HIST_PK" PRIMARY KEY ("CD_KIT","FH_INICIO");
  
  ALTER TABLE "APDIGB"."TBG011_KIT_PLANTILLA" ADD CONSTRAINT "TBG011_KIT_PLT_PK" PRIMARY KEY ("CD_KIT","CD_PLANTILLA");
  
  ALTER TABLE "APDIGB"."TBG011_KIT_PLANTILLA_HIST" ADD CONSTRAINT "TBG011_KIT_PLANTILLA_HIST_PK" PRIMARY KEY ("CD_KIT", "CD_PLANTILLA", "FH_INICIO");
  
  ALTER TABLE "APDIGB"."TBG012_ENV_KIT" ADD CONSTRAINT "TBG012_ENV_KIT_PK" PRIMARY KEY ("CD_ENV", "CD_KIT");
  
  ALTER TABLE "APDIGB"."TBG013_DET_TRAMA" ADD CONSTRAINT "TBG013_DET_TRAMA_PK" PRIMARY KEY ("ID_DET_TRAMA", "ID_TRAMA") ;
  
  ALTER TABLE "APDIGB"."oauth_access_token" ADD CONSTRAINT "OAUTH_ACCESS_TKN_PK" PRIMARY KEY ("authentication_id") ;
  

--------------------------------------------------------
--  Ref Constraints for Table TBG001_KIT_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_PLT_KIT_PLT_FK1" FOREIGN KEY ("CD_KIT")
	  REFERENCES "APDIGB"."TBG000_KIT" ("CD_KIT");
  ALTER TABLE "APDIGB"."TBG001_KIT_PLANTILLA" ADD CONSTRAINT "TBG001_PLT_KIT_PLT_FK2" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");

--------------------------------------------------------
--  Ref Constraints for Table TBG003_SECCION
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG003_SECCION" ADD CONSTRAINT "TBG003_SEC_TBG002_PLT_FK" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");
--------------------------------------------------------
--  Ref Constraints for Table TBG004_ITEM
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG004_ITEM" ADD CONSTRAINT "TBG004_ITM_TBG003_SEC_FK" FOREIGN KEY ("CD_SECCION")
	  REFERENCES "APDIGB"."TBG003_SECCION" ("CD_SECCION");

--------------------------------------------------------
--  Ref Constraints for Table TBG006_FLAG_PLANTILLA
--------------------------------------------------------

  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TABLE_FLG_TBG002_PLT_FK" FOREIGN KEY ("CD_PLANTILLA")
	  REFERENCES "APDIGB"."TBG002_PLANTILLA" ("CD_PLANTILLA");
  ALTER TABLE "APDIGB"."TBG006_FLAG_PLANTILLA" ADD CONSTRAINT "TABLE_PLT_TBG005_FLG_FK" FOREIGN KEY ("CD_FLAG")
	  REFERENCES "APDIGB"."TBG005_FLAG" ("CD_FLAG");


