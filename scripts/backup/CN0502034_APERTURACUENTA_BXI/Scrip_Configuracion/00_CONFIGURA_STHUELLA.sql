/********* Configuracion del flag de huella   **********/
DECLARE

    sql_stmnt varchar2(400) ;
	
BEGIN
    
	sql_stmnt := '   ALTER TABLE APDIGB.TBG009_ENV ADD ST_HUELLA CHAR(1 BYTE)';
	
    dbms_output.put_line(sql_stmnt );
    
	Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite la columna st_huella '); 
    WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* Configuracion del flag de huella   **********/