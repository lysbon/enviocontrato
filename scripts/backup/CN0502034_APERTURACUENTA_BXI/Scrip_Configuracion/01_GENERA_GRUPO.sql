/********* Creacion del Grupo para la apertura digital *********/

insert into "APDIGB"."TBG009_ENV"  (CD_ENV,NB_NOMBRE,NB_DESCRIPCION,NB_REMITENTE,
                                    NB_REMITENTE_CC,CH_TIPO,ST_ACTIVO,
                                    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,
                                    ST_HUELLA)
values( (SELECT (  MAX(CD_ENV) +1 ) AS CD_ENV FROM "APDIGB"."TBG009_ENV" ),'BXI','Banca Por Internet','BBVA Continental <postmaster@bbvacontinental.pe>',
       'apertura_digital@bbva.com','A','A','user','user',(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),'I');
COMMIT;

/********* Creacion del Grupo para la apertura digital *********/