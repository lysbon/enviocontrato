/********* Agregamos la configuracion de los subproductos a la tabla TBG012_ENV_KIT  ******************/
DECLARE

  /********* KIT BG351 - CONTR. CTA GANADORA  020009 020010     *****/
  /********* KIT BG461 - CONTR. CTA SUELDO   020046 020047      *****/
  /********* KIT BGB81 - CONTR. CTA INDEPENDENCIA 020049 020050  *****/
  /********* KIT BG151 - CONTR. CTA FACIL    020088 020089       *****/
    
  CURSOR C_TBG010_KIT IS select CD_KIT from "APDIGB"."TBG010_KIT" where cd_codigo='020009' or cd_codigo='020010' or cd_codigo='020046' 
  or cd_codigo='020047' or cd_codigo='020049' or cd_codigo='020050' or cd_codigo='020088' or cd_codigo='020089'  ;
  
  V_CD_ENV   "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  V_CD_KIT   "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
  cont NUMBER;
   
BEGIN
   
    select CD_ENV into V_CD_ENV from "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE LIKE 'BXI';
      
    cont:=0;
    
    OPEN C_TBG010_KIT;
      LOOP
         FETCH C_TBG010_KIT  INTO V_CD_KIT;
         EXIT WHEN C_TBG010_KIT%NOTFOUND;         
           
           select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT and CD_ENV=V_CD_ENV;
           
           IF cont=0 THEN 
            
                 INSERT INTO "APDIGB"."TBG012_ENV_KIT" (CD_ENV,CD_KIT) 
                 VALUES (V_CD_ENV,V_CD_KIT);
                 commit;
                           
           END IF;
              
    END LOOP;

   CLOSE C_TBG010_KIT;
   
END;
/********* Agregamos la configuracion de los subproductos a la tabla TBG012_ENV_KIT  ******************/