/********* Revertimos la creacion de la columna ST_SIGNBOX ******************/
DECLARE

    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '  ALTER TABLE APDIGB.TBG010_KIT DROP COLUMN ST_SIGNBOX ' ;
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en plantilla para el campo  '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* Revertimos la creacion de la columna ST_SIGNBOX ******************/