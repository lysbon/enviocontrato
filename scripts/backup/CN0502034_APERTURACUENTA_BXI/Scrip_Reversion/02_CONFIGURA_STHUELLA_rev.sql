/********* Revertimos la creacion de la columna ST_HUELLA ******************/
DECLARE

    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '  ALTER TABLE APDIGB.TBG009_ENV DROP COLUMN ST_HUELLA ' ;
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en plantilla para el campo  '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* Revertimos la creacion de la columna ST_HUELLA ******************/
