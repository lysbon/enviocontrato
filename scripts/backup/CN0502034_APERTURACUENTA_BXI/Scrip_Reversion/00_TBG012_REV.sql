/********* Eliminacion de la relacion grupo y kit ******************/
DECLARE
    
  V_CD_ENV   "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
   
BEGIN
   
   SELECT  CD_ENV INTO V_CD_ENV FROM "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE LIKE 'BXI';
     
   DELETE FROM "APDIGB"."TBG012_ENV_KIT"  WHERE CD_ENV=V_CD_ENV;
   
   commit;
   
   
END;
/********* Eliminacion de la relacion grupo y kit ******************/