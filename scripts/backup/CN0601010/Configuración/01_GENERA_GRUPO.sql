/********* Creacion del Grupo para banca movil *********/
declare cont number;
BEGIN

select count(1) into cont from "APDIGB"."TBG009_ENV" where NB_NOMBRE='BXM';

 IF cont=0 THEN
    insert into "APDIGB"."TBG009_ENV"  
    (CD_ENV,
     NB_NOMBRE,
     NB_DESCRIPCION,
     NB_REMITENTE,
     NB_REMITENTE_CC,
     CH_TIPO,
     ST_ACTIVO,
     CD_USU_CREA,
     CD_USU_MODI,
     FH_CREACION,
     FH_MODIFICACION,
     ST_HUELLA)
     values( 
      (SELECT (  MAX(CD_ENV) +1 ) AS CD_ENV FROM "APDIGB"."TBG009_ENV" ),
      'BXM',
      'Banca Movil',
      'BBVA Continental <postmaster@bbvacontinental.pe>',
      'apertura_digital@bbva.com',
      'A',
      'A',
      'user',
      'user',
      (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      'A'
      );
 ELSE
     UPDATE "APDIGB"."TBG009_ENV"
     SET    NB_DESCRIPCION='Banca Movil',
            NB_REMITENTE='BBVA Continental <postmaster@bbvacontinental.pe>',
            NB_REMITENTE_CC='apertura_digital@bbva.com',
            CH_TIPO='A',
            ST_ACTIVO='A',
            CD_USU_CREA= 'user',
            CD_USU_MODI= 'user',
            FH_MODIFICACION= (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
            ST_HUELLA= 'A'
     WHERE NB_NOMBRE='BXM';
 END IF;

COMMIT;

END;
/********* Creacion del Grupo para banca movil *********/