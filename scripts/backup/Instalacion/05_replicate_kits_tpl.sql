DECLARE
  PROCEDURE replicate_kit (cdKitSrc VARCHAR2,cdKitDest VARCHAR2) IS
    iSec NUMBER;
    CURSOR cKit IS
      SELECT * FROM APDIGB.TBG000_KIT WHERE CD_KIT = cdKitSrc;
    CURSOR cKitPlantilla(cdKit VARCHAR2) IS
      SELECT * FROM APDIGB.TBG001_KIT_PLANTILLA WHERE CD_KIT = cdKit;
  BEGIN
    FOR rKit IN cKit LOOP
      INSERT INTO APDIGB.TBG000_KIT(CD_KIT, NB_CORREO_ASUNTO, LB_CORREO_PLANTILLA) VALUES(cdKitDest,rKit.NB_CORREO_ASUNTO,rKit.LB_CORREO_PLANTILLA);
      FOR rKitPlantilla IN cKitPlantilla(rKit.CD_KIT) LOOP
        INSERT INTO APDIGB.TBG001_KIT_PLANTILLA(CD_KIT,CD_PLANTILLA) VALUES(cdKitDest,rKitPlantilla.CD_PLANTILLA);
      END LOOP;
    END LOOP;
  END;
BEGIN
  replicate_kit('020009','020010');--ganadora
  replicate_kit('020009','020023');
  replicate_kit('020009','020024');
  
  replicate_kit('020008','020025');--contiahorro
  
  replicate_kit('020037','020038');--asociado a prestamos
  
  replicate_kit('020043','020044');--remesas
  
  replicate_kit('020046','020047');--sueldo
  
  replicate_kit('020049','020050');--independencia
  
  replicate_kit('020088','020089');--facil
  replicate_kit('020088','020090');--facil
  replicate_kit('020088','020091');--facil
  
  replicate_kit('020092','020093');--vivienda
  
  replicate_kit('020109','020110');--senior
  
  COMMIT;
  
END;