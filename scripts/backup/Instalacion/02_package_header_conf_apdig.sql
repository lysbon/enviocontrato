CREATE OR REPLACE 
PACKAGE APDIGB.PBG_ENV_DIGITAL_CONF AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  TYPE g_cursor IS REF CURSOR;
  
  PROCEDURE PRO_RETURN_KIT_PLANTILLA(
    kit_id     IN     APDIGB.TBG000_KIT.CD_KIT%TYPE,
    o_cursor   IN OUT g_cursor);
    
  PROCEDURE PRO_RETURN_PLANTILLAS(
    kit_id     IN     APDIGB.TBG000_KIT.CD_KIT%TYPE,
    o_cursor   IN OUT g_cursor);
    
  PROCEDURE PRO_RETURN_SECCIONES(
    plantilla_id   IN     APDIGB.TBG003_SECCION.CD_PLANTILLA%TYPE,
    o_cursor       IN OUT g_cursor);
    
  PROCEDURE PRO_RETURN_ELEMENTOS(
    seccion_id   IN     APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
    o_cursor       IN OUT g_cursor);
  
  PROCEDURE PRO_RETURN_FLAG_PLANTILLAS(
    plantilla_id   IN     APDIGB.TBG003_SECCION.CD_PLANTILLA%TYPE,
    o_cursor       IN OUT g_cursor);
    
END PBG_ENV_DIGITAL_CONF;