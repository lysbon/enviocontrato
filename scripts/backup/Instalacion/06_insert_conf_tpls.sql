DECLARE
  itm NUMBER;
  PROCEDURE moverTemplateSeccionY(cdPlantilla NUMBER, nu_page NUMBER, src_y NUMBER) IS
    CURSOR cSec IS
      SELECT * FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = cdPlantilla AND NU_PAGINA = nu_page;
    CURSOR cItm(cdSec NUMBER) IS
      SELECT * FROM APDIGB.TBG004_ITEM    WHERE CD_SECCION = cdSec;
  BEGIN
    FOR rSec IN cSec LOOP
      FOR rItm IN cItm(rSec.CD_SECCION) LOOP
        UPDATE APDIGB.TBG004_ITEM 
        SET    NU_COOR_Y = NU_COOR_Y + src_y
        WHERE  CD_ITEM = rItm.CD_ITEM;
      END LOOP;
    END LOOP;
  END;
  PROCEDURE replicate_template (cdPlantilla NUMBER,cdPlantillaDest NUMBER) IS
    iSec NUMBER;
    CURSOR cSec IS
      SELECT * FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = cdPlantilla;
    CURSOR cItm(cdSec NUMBER) IS
      SELECT * FROM APDIGB.TBG004_ITEM    WHERE CD_SECCION = cdSec;
  BEGIN
    SELECT MAX(CD_SECCION)+1 INTO iSec FROM APDIGB.TBG003_SECCION;
    FOR rSec IN cSec LOOP
      INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(iSec,cdPlantillaDest,rSec.NU_PAGINA);
      FOR rItm IN cItm(rSec.CD_SECCION) LOOP
        INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
        VALUES(APDIGB.item_seq.NEXTVAL,iSec,rItm.ST_ACTIVO,rItm.NU_COOR_X,rItm.NU_COOR_Y,rItm.NB_NOMBRE_VAR,rItm.NB_VALOR_VAR,rItm.NB_FUENTE,rItm.NU_ALINEAR,rItm.CH_TIPO,rItm.NU_ANCHO,rItm.NU_SC_X,rItm.NU_SC_Y,rItm.NU_IDX_CLIENTE);
      END LOOP;
      iSec := iSec + 1;
    END LOOP;
  END;
  PROCEDURE n_item(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                   nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                   nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                   nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                   nbValorVar  APDIGB.TBG004_ITEM.NB_VALOR_VAR%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
    VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,nbValorVar,'ArialBold9','0','T',NULL,NULL,NULL,NULL); 
  END;
  PROCEDURE n_itemb(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                   nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                   nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                   nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                   nbValorVar  APDIGB.TBG004_ITEM.NB_VALOR_VAR%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
    VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,nbValorVar,'ArialBold10_5','0','T',NULL,NULL,NULL,NULL); 
  END;
  PROCEDURE n_item_fa(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                      nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                      nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                      nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                      nbValorVar  APDIGB.TBG004_ITEM.NB_VALOR_VAR%TYPE,
                      nbFuente    APDIGB.TBG004_ITEM.NB_FUENTE%TYPE,
                      nuAlinear   APDIGB.TBG004_ITEM.NU_ALINEAR%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
    VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,nbValorVar,nbFuente,nuAlinear,'T',NULL,NULL,NULL,NULL); 
  END;
  PROCEDURE n_item_x(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                     nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                     nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                     nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                     chTipo      APDIGB.TBG004_ITEM.CH_TIPO%TYPE,
                     nuAncho     APDIGB.TBG004_ITEM.NU_ANCHO%TYPE,
                     nuScX       APDIGB.TBG004_ITEM.NU_SC_X%TYPE,
                     nuScY       APDIGB.TBG004_ITEM.NU_SC_Y%TYPE,
                     idxCli      APDIGB.TBG004_ITEM.NU_IDX_CLIENTE%TYPE) IS
  cItemImg VARCHAR2(1);
  BEGIN
    IF (nbNombreVar = 'JPG1' OR nbNombreVar = 'JPG2') THEN
	    IF nbNombreVar = 'JPG1' THEN
	      cItemImg := '1';
      ELSE
	      cItemImg := '2';
	    END IF;
      INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,cItemImg,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
      --INSERT INTO APDIGB.TBG007_ITEM_IMG(CD_ITEM) VALUES(itemSeq);
    ELSE
	  INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,NULL,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
    END IF;
  END;  
BEGIN
  DELETE FROM APDIGB.TBG007_ITEM_IMG;
  DELETE FROM APDIGB.TBG004_ITEM;
  DELETE FROM APDIGB.TBG003_SECCION;

  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(1,1,1);
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(2,1,4);
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(3,3,1);
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(4,3,2);
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(5,9,1);--RVA
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(6,5,1);--FATCA
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(7,8,1);--cta sueldo
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(8,8,6);--cta sueldo
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(9,12,1);--cta facil
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(10,12,3);--cta facil
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(11,12,4);--cta facil
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(12,13,1);--cta remesas
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(13,13,4);--cta remesas
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(14,17,1);--cta senior
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(15,17,4);--cta senior
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(16,14,1);--cta contiahorro euro
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(17,14,3);--cta contiahorro euro
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(18,15,1);--cta asociada prestamos
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(19,15,4);--cta asociada prestamos
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(20,16,1);--cta independencia
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(21,16,4);--cta independencia
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(22,11,1);--cta ahorro vivienda
  INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(23,11,4);--cta ahorro vivienda
  
  --ganadora
  n_itemb(1,481,784,'CTR0|A01','22/09/2016');
  n_itemb(1, 46,746,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(1,165,718,'CTR0|G03','07821571');
  n_itemb(1,111,698,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(1,111,685,'CTR0|A07','Ancon, Lima');
  n_itemb(1,103,667,'CTR0|A08','SOLES');
  n_itemb(1,498,667,'CTR0|A09','INDIVIDUAL');
  n_itemb(1,412,734,'CTR0|A02','0011');
  n_itemb(1,447,734,'CTR0|A03','0130');
  n_itemb(1,478,734,'CTR0|A04','0209051718');
  n_itemb(1,543,734,'CTR0|A05','22');
  n_item_fa(2,168,768,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(2,162,624,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(2,156,639,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x(2, 89,763,'huella', 'I',130,80,104,0);
  n_item_x(2,200,320,'barcode','B',171,171,25,0);
  --n_item_x(2, 91,510,'JPG1','I',130,130,54,0);
  --n_item_x(2,346,510,'JPG2','I',130,130,54,0);
  
  -------------------------------------------------------------
  --apertura
  n_item(3,390,789,'SOL0|A01','01/10/2016');
  n_item(3,485,789,'SOL0|A02','CODCLIENTE');
  n_item(3,139,725,'SOL0|A03','PRODUCTO');
  n_item(3,411,725,'SOL0|A04','SUBPRODUCTO');
  n_item(3,139,707,'SOL0|A05','NRO_CUENTA');
  n_item(3,411,707,'SOL0|A06','TIPO_CUENTA');
  n_item(3,139,673,'SOL0|A07','NOMBRES');
  n_item(3,411,673,'SOL0|A08','APELLIDOS');
  n_item(3,139,656,'SOL0|A09','TIPO Y NRO DOI');
  n_item(3,411,656,'SOL0|A10','FECHA_NAC');
  n_item(3,139,639,'SOL0|A11','PROFESION');
  n_item(3,411,639,'SOL0|A12','CENTRO_LAB');
  n_item(3,139,622,'SOL0|A13','PUESTO');
  n_item(3,475,621,'SOL0|A14','INGRESO_PROM');
  n_item(3,139,604,'SOL0|A15','LUGAR_NAC');
  n_item(3,411,606,'SOL0|A16','NACIONALIDAD');
  n_item(3,139,589,'SOL0|A17','CORREO');
  
  n_item(3,139,548,'SOL0|A18','IND_VIAL');
  n_item(3,411,548,'SOL0|A19','NOMBRE_AV');
  n_item(3,139,531,'SOL0|A20','N_EXT');
  n_item(3,411,531,'SOL0|A21','DEPARTAMENTO');
  n_item(3,139,515,'SOL0|A22','PROVINCIA');
  n_item(3,411,515,'SOL0|A23','DISTRITO');
  n_item(3,139,498,'SOL0|A24','REF');
  n_item(3,411,497,'SOL0|A25','TELEF.FIJO');
  n_item(3,139,480,'SOL0|A26','TIPO_DIREC_ALTER');
  n_item(3,411,480,'SOL0|A27','DIREC_ALTER');
  n_item(3,139,463,'SOL0|A28','NUM_LOTE');
  n_item(3,411,463,'SOL0|A29','TELEF_ALTER');
  
  n_item(3,178,423,'SOL0|A30','X');
  n_item(3,214,423,'SOL0|A31','X');
  n_item(3,223,408,'SOL0|A32','Ciudad');
  n_item(3,402,408,'SOL0|A33','Pais');
  
  n_item(3,178,395,'SOL0|A34','X');
  n_item(3,214,395,'SOL0|A35','X');
  n_item(3,223,380,'SOL0|A36','Ciudad');
  n_item(3,402,380,'SOL0|A37','Pais');
  n_item(3,88,363,'SOL0|A38','Direccion');
  
  n_item(3,187,294,'SOL0|A39','X');
  n_item(3,220,294,'SOL0|A40','X');
  n_item(3, 78,262,'SOL0|A41','Cargo');
  n_item(3,413,262,'SOL0|A42','Fecha');
  n_item(3,150,249,'SOL0|A43','Institucion');
  n_item(3,182,229,'SOL0|A44','X');
  n_item(3,218,229,'SOL0|A45','X');
  
  n_item(3,194,187,'SOL0|A46','NombreApe');
  n_item(3,451,187,'SOL0|A47','Funcion');
  n_item(3,197,171,'SOL0|A48','Pais');
  n_item(3,403,171,'SOL0|A49','Vinculo');
  
  n_item(3,508,135,'SOL0|A50','X');
  n_item(3,540,135,'SOL0|A51','X');
  n_item(3,508,115,'SOL0|A65','X');
  n_item(3,540,115,'SOL0|A66','X');
  
  n_item(3,211, 96,'SOL0|A54','X');
  n_item(3,336, 96,'SOL0|A55','X');
  
  n_item(4,42,722,'SOL0|A56','X');
  n_item(4,42,700,'SOL0|A57','X');
  n_item(4,42,680,'SOL0|A58','X');
  n_item(4,42,659,'SOL0|A59','X');
  n_item(4,42,637,'SOL0|A60','X');
  n_item(4,320,637,'SOL0|A61','Tarjeta');
  
  n_item(4,45,461,'SOL0|A63','X');
  n_item(4,45,440,'SOL0|A64','X');
  
  n_item_x(4,237,215,'huella','I',130, 57,75,0);
  --n_item_x(4, 85,207,'JPG1','I',130,130,54,0);
  --n_item_x(4, 85,118,'JPG2','I',130,130,54,0);
  
  replicate_template (3,4);
  --replicate_template (1,8);
  --moverTemplateSeccionY (8,1,-10);
  --moverTemplateSeccionY (8,2,-300);
  
  n_item(5,  8,675,'RBA0|P01','90016480');
  n_item(5,128,675,'RBA0|P02','RENATO PAZ CORNEJO');
  n_item(5,385,675,'RBA0|P03','F');
  n_item(5,  8,640,'RBA0|P04','II');
  n_item(5,  8,600,'RBA0|P05','NORMAL');
  n_item(5, 45,580,'RBA0|P06','01');
  n_item(5, 80,534,'RBA0|P07','XXXXXXX');
  n_item(5, 80,524,'RBA0|P08','XXXXXXX');
  n_item(5, 80,514,'RBA0|P09','XXXXXXX');
  n_item(5, 80,504,'RBA0|P10','XXXXXXX');
  n_item(5,530,747,'RBA0|P11','XXXXXXX');
  n_item(5,115,462,'RBA0|P12','IW8C');
  n_item(5,145,462,'RBA0|P13','P111061');
  n_item(5,189,462,'RBA0|P14','17::08:41');
  
  --FATCA
  --DELETE FROM APDIGB.TBG004_ITEM WHERE CD_SECCION  = 6;
  n_item_fa(6,'475','796','FECEMI','XXXXXXX','Arial9','0');
  n_item_fa(6,'93','747','NOMBRE','Carlos Alberto Sosa Navarro','Arial9','0');
  n_item_fa(6,'82','724','TIPODOC','L','Arial9','1');
  n_item_fa(6,'173','724','NRODOC','45482119','Arial9','1');
  n_item_fa(6,'267','724','CIUDADNAC','Lima','Arial9','1');
  n_item_fa(6,'368','724','PAISNAC','Peru','Arial9','1');
  n_item_fa(6,'477','724','FECNAC','11/09/1988','Arial9','1');
  n_item_fa(6,'55','701','DIRECDOM','Av. Canevaro 243, Lince','Arial9','0');
  n_item_fa(6,'403','702','CIUDADDOM','Lima','Arial9','0'); 
  n_item_fa(6,'55','680','DPTODOM','Lima','Arial9','0'); 
  n_item_fa(6,'207','680','CODPOSTAL','Lima 17','Arial9','0'); 
  n_item_fa(6,'403','681','PAISDOM','Lima 17','Arial9','0'); 
  n_item_fa(6,'57','609','RESPAISFISC1','Peru','Arial9','0'); 
  n_item_fa(6,'57','572','RESPAISFISC2','Peru','Arial9','0'); 
  n_item_fa(6,'57','536','RESPAISFISC3','Peru','Arial9','0'); 
  n_item_fa(6,'318','608','NUMIDFISC1','4325436567545645235423423432','Arial9','0'); 
  n_item_fa(6,'318','572','NUMIDFISC2','4325436567545645235423423432','Arial9','0'); 
  n_item_fa(6,'318','536','NUMIDFISC3','4325436567545645235423423432','Arial9','0'); 
  
  --facil
  n_itemb(9,483,781,'CTR0|A01','22/09/2016');
  n_itemb(9, 49,745,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(9,167,714,'CTR0|G03','07821571');
  n_itemb(9,113,692,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(9,113,678,'CTR0|A07','Ancon, Lima');
  n_itemb(9,103,662,'CTR0|A08','SOLES');
  n_itemb(9,498,662,'CTR0|A09','INDIVIDUAL');
  n_itemb(9,410,732,'CTR0|A02','0011');
  n_itemb(9,445,732,'CTR0|A03','0130');
  n_itemb(9,478,732,'CTR0|A04','0209051718');
  n_itemb(9,541,732,'CTR0|A05','22');
  n_item_fa(10,167,253,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(10,162,114,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(10,155,126,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x(10, 88,243,'huella', 'I',130,80,104,0);
  n_item_x(11,200,491,'barcode','B',171,171,25,0);
  --n_item_x(11, 80,675,'JPG1','I',130,130,54,0);
  --n_item_x(11,343,675,'JPG2','I',130,130,54,0);
  
  --sueldo
  n_itemb(7,481,773,'CTR0|A01','22/09/2016');
  n_itemb(7, 46,734,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(7,165,706,'CTR0|G03','07821571');
  n_itemb(7,113,690,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(7,113,675,'CTR0|A07','Ancon, Lima');
  n_itemb(7,103,659,'CTR0|A08','SOLES');
  n_itemb(7,498,659,'CTR0|A09','INDIVIDUAL');
  n_itemb(7,412,723,'CTR0|A02','0011');
  n_itemb(7,446,723,'CTR0|A03','0130');
  n_itemb(7,478,723,'CTR0|A04','0209051718');
  n_itemb(7,542,723,'CTR0|A05','22');
  n_item_fa(8,167,750,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(8,161,602,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(8,156,613,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x(8, 91,736,'huella', 'I',130, 80,104,0);
  n_item_x(8,200,302,'barcode','B',171,171,25,0);
  --n_item_x(8, 90,473,'JPG1','I',130,130,54,0);
  --n_item_x(8,356,473,'JPG2','I',130,130,54,0);
  
  --remesas
  n_itemb(12,483,787,'CTR0|A01','22/09/2016');
  n_itemb(12, 46,756,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(12,171,728,'CTR0|G03','07821571');
  n_itemb(12,113,711,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(12,113,698,'CTR0|A07','Ancon, Lima');
  n_itemb(12,103,685,'CTR0|A08','SOLES');
  n_itemb(12,466,685,'CTR0|A09','INDIVIDUAL');
  n_itemb(12,412,738,'CTR0|A02','0011');
  n_itemb(12,447,738,'CTR0|A03','0130');
  n_itemb(12,477,738,'CTR0|A04','0209051718');
  n_itemb(12,543,738,'CTR0|A05','22');
  n_item_fa(13,169,753,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(13,164,601,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(13,158,618,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x(13, 88,739,'huella', 'I',130, 80,104,0);
  n_item_x(13,200,300,'barcode','B',171,171,25,0);
  --n_item_x(13, 98,490,'JPG1','I',130,130,54,0);
  --n_item_x(13,344,490,'JPG2','I',130,130,54,0);
  
  --senior
  n_itemb(14,480,780,'CTR0|A01','22/09/2016');
  n_itemb(14, 49,745,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(14,167,714,'CTR0|G03','07821571');
  n_itemb(14,113,693,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(14,113,678,'CTR0|A07','Ancon, Lima');
  n_itemb(14,103,662,'CTR0|A08','SOLES');
  n_itemb(14,499,662,'CTR0|A09','INDIVIDUAL');
  n_itemb(14,409,732,'CTR0|A02','0011');
  n_itemb(14,444,732,'CTR0|A03','0130');
  n_itemb(14,477,732,'CTR0|A04','0209051718');
  n_itemb(14,541,732,'CTR0|A05','22');
  n_item_fa(15,170,760,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(15,171,630,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(15,164,645,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x (15, 99,760,'huella', 'I',130, 80,100,0);
  n_item_x (15,200,384,'barcode','B',171,171,25,0);
  --n_item_x (15, 95,535,'JPG1','I',130,130,54,0);
  --n_item_x (15,353,535,'JPG2','I',130,130,54,0);
  
  --contiahorro euros
  n_itemb(16,483,787,'CTR0|A01','22/09/2016');
  n_itemb(16, 46,749,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(16,164,721,'CTR0|G03','07821571');
  n_itemb(16,113,703,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(16,113,689,'CTR0|A07','Ancon, Lima');
  n_itemb(16,103,670,'CTR0|A08','SOLES');
  n_itemb(16,496,670,'CTR0|A09','INDIVIDUAL');
  n_itemb(16,410,738,'CTR0|A02','0011');
  n_itemb(16,445,738,'CTR0|A03','0130');
  n_itemb(16,477,738,'CTR0|A04','0209051718');
  n_itemb(16,542,738,'CTR0|A05','22');
  n_item_fa(17,167,453,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(17,161,324,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(17,156,336,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x (17, 93,452,'huella', 'I',130,80,103,0);
  n_item_x (17,200, 92,'barcode','B',171,171,25,0);
  --n_item_x (17, 89,218,'JPG1','I',130,130,54,0);
  --n_item_x (17,352,218,'JPG2','I',130,130,54,0);
  
  --asociada prestamos
  n_itemb(18,483,780,'CTR0|A01','22/09/2016');
  n_itemb(18, 49,746,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(18,169,714,'CTR0|G03','07821571');
  n_itemb(18,113,693,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(18,113,679,'CTR0|A07','Ancon, Lima');
  n_itemb(18,103,663,'CTR0|A08','SOLES');
  n_itemb(18,500,663,'CTR0|A09','INDIVIDUAL');
  n_itemb(18,412,732,'CTR0|A02','0011');
  n_itemb(18,447,732,'CTR0|A03','0130');
  n_itemb(18,479,732,'CTR0|A04','0209051718');
  n_itemb(18,543,732,'CTR0|A05','22');
  n_item_fa(19,169,746,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(19,161,606,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(19,155,618,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x (19, 88,735,'huella', 'I',130,80,104,0);
  n_item_x (19,200,331,'barcode','B',171,171,25,0);
  --n_item_x (19, 91,494,'JPG1','I',130,130,54,0);
  --n_item_x (19,388,494,'JPG2','I',130,130,54,0);
  
  --independencia
  n_itemb(20,483,786,'CTR0|A01','22/09/2016');
  n_itemb(20, 46,747,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(20,163,719,'CTR0|G03','07821571');
  n_itemb(20,113,701,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(20,113,687,'CTR0|A07','Ancon, Lima');
  n_itemb(20,103,669,'CTR0|A08','SOLES');
  n_itemb(20,493,669,'CTR0|A09','INDIVIDUAL');
  n_itemb(20,408,739,'CTR0|A02','0011');
  n_itemb(20,445,739,'CTR0|A03','0130');
  n_itemb(20,477,739,'CTR0|A04','0209051718');
  n_itemb(20,543,739,'CTR0|A05','22');
  n_item_fa(21,167,535,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(21,160,408,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(21,156,420,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x (21, 92,534,'huella', 'I',130,80,101,0);
  n_item_x (21,200,105,'barcode','B',171,171,25,0);
  --n_item_x (21, 89,272,'JPG1','I',130,130,54,0);
  --n_item_x (21,351,272,'JPG2','I',130,130,54,0);
  
  --ahorro vivienda
  n_itemb(22,483,787,'CTR0|A01','22/09/2016');
  n_itemb(22, 46,756,'CTR0|G01','Carlos Sosa Navarro');
  n_itemb(22,171,728,'CTR0|G03','07821571');
  n_itemb(22,113,710,'CTR0|A06','Av. Canevaro 243, Lince');
  n_itemb(22,113,697,'CTR0|A07','Ancon, Lima');
  n_itemb(22,103,678,'CTR0|A08','SOLES');
  n_itemb(22,466,678,'CTR0|A09','INDIVIDUAL');
  n_itemb(22,412,738,'CTR0|A02','0011');
  n_itemb(22,447,738,'CTR0|A03','0130');
  n_itemb(22,477,738,'CTR0|A04','0209051718');
  n_itemb(22,543,738,'CTR0|A05','22');
  n_item_fa(23,169,513,'CTR0|A01','22/09/2016','Arial11','0');
  n_item_fa(23,164,377,'CTR0|G03','07821571','ArialBold9','0');
  n_item_fa(23,158,393,'CTR0|G01','Carlos Sosa Navarro','ArialBold9','1');
  n_item_x (23, 88,509,'huella', 'I',130,80,104,0);
  n_item_x (23,200, 90,'barcode','B',171,171,25,0);
  --n_item_x (23, 98,246,'JPG1','I',130,130,54,0);
  --n_item_x (23,344,246,'JPG2','I',130,130,54,0);
  
  INSERT INTO APDIGB.TBG007_ITEM_IMG(CD_ITEM) VALUES(1);
  INSERT INTO APDIGB.TBG007_ITEM_IMG(CD_ITEM) VALUES(2);
  
  UPDATE APDIGB.TBG004_ITEM SET NB_VALOR_VAR = NULL WHERE CH_TIPO = 'T';
  
  COMMIT;

END;