/**** PLANTILLA IC961 - SEGURO VIDA RENTA *****/
DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_1 number(1);
 
     
BEGIN
 
   idxCliente:=0;
   v_numpagina_1:=1;
   
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%IC961%' or nb_nombre_correo like '%IC961%' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
  
   delete from "APDIGB"."TBG004_ITEM" where CD_SECCION= v_cd_seccion;
   
   
   
   
   idxCliente:=0;
   v_numpagina_1:=2;
   
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
   delete from "APDIGB"."TBG004_ITEM" where CD_SECCION= v_cd_seccion;
                                       
   COMMIT;
    
END;
/**** PLANTILLA IC961 - SEGURO VIDA RENTA *****/