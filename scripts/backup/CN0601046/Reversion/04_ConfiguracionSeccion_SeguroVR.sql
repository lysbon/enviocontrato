/**** SECCION IC961 - SEGURO VIDA RENTA *****/
DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
     
BEGIN
  
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%IC961%' or nb_nombre_correo like '%IC961%' ; 
   
  delete from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=v_cd_plantilla ;
  
  COMMIT;
      
END;
/**** SECCION IC961 - SEGURO VIDA RENTA *****/