/********* Agregamos la configuracion de los subproductos a la tabla TBG012_ENV_KIT  ******************/
DECLARE
  /********* KIT PE281 - LEY DE PROTECCION DE DATOS   400822        *****/
    
  v_cd_env   "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  v_kit   "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
  cont NUMBER;
   
BEGIN
   
    select CD_ENV into v_cd_env from "APDIGB"."TBG009_ENV" WHERE nb_nombre LIKE 'BXI';
    
    select CD_KIT into v_kit from "APDIGB"."TBG010_KIT" where cd_codigo='400822';
    
    delete from "APDIGB"."TBG012_ENV_KIT" WHERE cd_kit=v_kit and cd_env=v_cd_env;
    
	commit;
	
END;
/********* Agregamos la configuracion de los subproductos a la tabla TBG012_ENV_KIT  ******************/