
DECLARE
  cont NUMBER;
BEGIN

  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%509999%';
  
  IF cont>0 THEN
  
    DELETE FROM "APDIGB"."TBG010_KIT"
    WHERE cd_codigo LIKE '%509999%';
     
  END IF;
  
  COMMIT;
  
END;
