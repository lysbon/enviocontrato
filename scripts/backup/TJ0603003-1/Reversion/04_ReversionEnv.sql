DECLARE

  v_cd_env  "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  v_cd_kit  "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
  
  cont NUMBER;
   
BEGIN

    select CD_ENV into v_cd_env from "APDIGB"."TBG009_ENV" WHERE nb_nombre LIKE '%PROD%';
    select CD_KIT into v_cd_kit from "APDIGB"."TBG010_KIT" WHERE NB_NOMBRE_KIT like '%509999%';
    select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE cd_kit=v_cd_kit and cd_env=v_cd_env;
           
    IF cont>0 OR cont IS NULL THEN 
            
        DELETE FROM "APDIGB"."TBG012_ENV_KIT" 
        WHERE CD_ENV=v_cd_env AND CD_KIT=v_cd_kit;
        commit;
                           
    END IF;
   
END;

