DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(2);
   cont NUMBER;
     
BEGIN
    
  /*****************VOUCHER_PASALO_CUOTAS*******************/
  numpagina_1:=1;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where   nb_nombre_formato like '%VOUCHER_PASALO_CUOTAS%' or nb_nombre_correo like '%VOUCHER_PASALO_CUOTAS%' ;
  
   select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
   CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
   
    DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
    
  end if;
   
  commit;
      
END;
