DECLARE

  v_cd_env  "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  v_cd_kit  "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
  
  cont NUMBER;
   
BEGIN

    select CD_ENV into v_cd_env from "APDIGB"."TBG009_ENV" WHERE nb_nombre LIKE '%PROD%';
    select CD_KIT into v_cd_kit from "APDIGB"."TBG010_KIT" WHERE NB_NOMBRE_KIT like '%509999%';
    select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE cd_kit=v_cd_kit and cd_env=v_cd_env;
           
    IF cont=0 OR cont IS NULL THEN 
            
        INSERT INTO "APDIGB"."TBG012_ENV_KIT" (CD_ENV,CD_KIT,NB_CORREO_ASUNTO,NB_CORREO_ENGINE,NB_REMITENTE,ST_ENV_CORREO) 
        VALUES (v_cd_env,v_cd_kit,'CONSTANCIA PASALO A CUOTAS','STRTPL','BBVA Continental <postmaster@bbvacontinental.pe>','A');
        commit;
                           
    END IF;
   
END;

/*
SELECT * FROM "APDIGB"."TBG012_ENV_KIT"
order by cd_kit desc
WHERE NB_CORREO_ASUNTO='PASALO A CUOTAS'
UPDATE "APDIGB"."TBG012_ENV_KIT"
SET    NB_CORREO_ASUNTO='CONSTANCIA PASALO A CUOTAS'
WHERE NB_CORREO_ASUNTO='PASALO A CUOTAS'
*/