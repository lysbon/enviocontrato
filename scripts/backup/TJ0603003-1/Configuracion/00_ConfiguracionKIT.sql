
DECLARE
  cont NUMBER;
BEGIN

/********* Creacion KIT PASALO A CUOTAS *********/

  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%509999%';
  
  IF cont=0 OR cont IS NULL THEN
    INSERT
    INTO "APDIGB"."TBG010_KIT"
      (
        CD_KIT,
        CD_CODIGO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        NB_NOMBRE_KIT,
        ST_FILEUNICO,
        FL_VAL_HUELLA,
        ST_SIGNBOX,
        ST_HUELLA
      )
      VALUES
      (
        (SELECT ( (TO_NUMBER(SUBSTR(MAX(CD_KIT),0,6)) + 1)) AS CD_KIT
          FROM "APDIGB"."TBG010_KIT"
        )
        ,
        '509999',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'KIT 509999 - PASALO A CUOTAS',
        'A',
        'A',
        'A',
        'A'
      );
  ELSE
    UPDATE "APDIGB"."TBG010_KIT"
    SET 
      CD_USU_CREA       =NULL,
      CD_USU_MODI       =NULL,
      FH_CREACION       =NULL,
      FH_MODIFICACION   =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      NB_NOMBRE_KIT     ='KIT 509999 - PASALO A CUOTAS',
      ST_FILEUNICO      ='A',
      FL_VAL_HUELLA     ='A',
      ST_SIGNBOX        ='A',
      ST_HUELLA         ='A'
    WHERE cd_codigo LIKE  '%509999%';
   
  END IF;
  
  COMMIT;
  
  
END;

/*SELECT * FROM "APDIGB"."TBG010_KIT"
WHERE cd_codigo LIKE  '%509999%';
DELETE FROM "APDIGB"."TBG010_KIT"
WHERE cd_codigo LIKE  '%509999%';
*/