DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(2);
   cont NUMBER;
     
BEGIN
    
  /*****************VOUCHER_PASALO_CUOTAS*******************/
  numpagina_1:=1;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%VOUCHER_PASALO_CUOTAS%' or nb_nombre_correo like '%VOUCHER_PASALO_CUOTAS%' ; 
  
   select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
   CD_PLANTILLA=v_cd_plantilla and NU_PAGINA=numpagina_1;  
  
  IF cont=0 OR cont IS NULL THEN  
   
    INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_1,
    null,null,(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),null);
    
  else
      
    UPDATE  "APDIGB"."TBG003_SECCION" SET CD_PLANTILLA=v_cd_plantilla,NU_PAGINA=numpagina_1,
    CD_USU_CREA=null,CD_USU_MODI=null,FH_MODIFICACION=(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL) where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
  
  end if;
  
  COMMIT;
  /*
   * SELECT * FROM "APDIGB"."TBG003_SECCION" ORDER BY CD_SECCION DESC
   * DELETE FROM "APDIGB"."TBG003_SECCION" WHERE CD_SECCION IN (170,171)
   * 
   */
END;

