DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion_0 "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
  v_cd_seccion_1 "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
  v_numpagina_0 number(2);
  v_numpagina_1 number(2);
  cont NUMBER;
     
BEGIN
    
	v_numpagina_0:=2;
    v_numpagina_1:=4;

	--PE281 - LPDP
   
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
    where  NB_DESCRIPCION='PE281 - LPDP' AND ST_ACTIVO='A';

    select  CD_SECCION into v_cd_seccion_0 from "APDIGB"."TBG003_SECCION" 
    where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_0; 

    select  CD_SECCION into v_cd_seccion_1 from "APDIGB"."TBG003_SECCION" 
    where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion_1;  

    IF cont=0 OR cont IS NULL then  
    	update "APDIGB"."TBG004_ITEM" 
        SET    CD_SECCION=v_cd_seccion_1
    	where  CD_SECCION=v_cd_seccion_0;
    END IF;
	
    update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=215
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G01';

    update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=205
	where  CD_SECCION=v_cd_seccion_1 AND CH_TIPO='H';

    update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=84
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G02';

	update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=65
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G03';

    update "APDIGB"."TBG004_ITEM" 
	SET    NU_COOR_Y=35
	WHERE  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='barcode';

	--PE281 - LPDP - CONTRATOS
	
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
    where  NB_DESCRIPCION='PE281 - LPDP - CONTRATOS' AND ST_ACTIVO='A';

    select  CD_SECCION into v_cd_seccion_0 from "APDIGB"."TBG003_SECCION" 
    where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_0; 

    select  CD_SECCION into v_cd_seccion_1 from "APDIGB"."TBG003_SECCION" 
    where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion_1;  

    IF cont=0 OR cont IS NULL then  
    	update "APDIGB"."TBG004_ITEM" 
        SET    CD_SECCION=v_cd_seccion_1
    	where  CD_SECCION=v_cd_seccion_0;
    END IF;
	
	 update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=215
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G01';

    update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=205
	where  CD_SECCION=v_cd_seccion_1 AND CH_TIPO='H';

    update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=84
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G02';

	update "APDIGB"."TBG004_ITEM" 
	set    NU_COOR_Y=65
	where  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='CLS0|G03';

    update "APDIGB"."TBG004_ITEM" 
	SET    NU_COOR_Y=35
	WHERE  CD_SECCION=v_cd_seccion_1 AND NB_NOMBRE_VAR='barcode';

	COMMIT;
	
END;
/*
SELECT * FROM "APDIGB"."TBG004_ITEM" 
WHERE CD_SECCION IN (158,160)
*/


