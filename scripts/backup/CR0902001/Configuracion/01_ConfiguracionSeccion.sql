DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(2);
   cont NUMBER;
     
BEGIN
   
  numpagina_1:=4;
  
  --PE281 - LPDP
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
  where  NB_DESCRIPCION='PE281 - LPDP' AND ST_ACTIVO='A';
 
  select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
  CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  

  IF cont=0 OR cont IS NULL THEN  
   
    INSERT INTO "APDIGB"."TBG003_SECCION"
    (
     CD_SECCION,
     CD_PLANTILLA,
     NU_PAGINA,
     FH_CREACION
    )
    VALUES 
    (
      (SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),
      v_cd_plantilla,
      numpagina_1,
      (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL)
    );
  
  END IF;
  
  --PE281 - LPDP - CONTRATOS
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
  where  NB_DESCRIPCION='PE281 - LPDP - CONTRATOS' AND ST_ACTIVO='A';
 
  select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
  CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  

  IF cont=0 OR cont IS NULL THEN  
   
    INSERT INTO "APDIGB"."TBG003_SECCION"
    (
     CD_SECCION,
     CD_PLANTILLA,
     NU_PAGINA,
     FH_CREACION
    )
    VALUES 
    (
      (SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),
      v_cd_plantilla,
      numpagina_1,
      (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL)
    );
  
  END IF;

  COMMIT;
      
END;

/*
	select  * from "APDIGB"."TBG003_SECCION"
	where   CD_PLANTILLA=( 
	select  CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" 
    where   NB_DESCRIPCION='PE281 - LPDP - CONTRATOS' AND ST_ACTIVO='A');
    
    select  * from "APDIGB"."TBG003_SECCION"
	where   CD_PLANTILLA=( 
	select  CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" 
    where   NB_DESCRIPCION='PE281 - LPDP' AND ST_ACTIVO='A');
*/


