--DROP TABLE APDIGB.TBG008_TRAMA;
CREATE TABLE APDIGB.TBG008_TRAMA
  (
    CH_MOTI              VARCHAR2 (40),
    CH_TRAMA             VARCHAR2 (4000),
    FH_CREACION          DATE
  ) ;
  
grant select,update,delete,insert on APDIGB.TBG008_TRAMA to APP_APDIGB;