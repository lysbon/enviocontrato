DECLARE
  PROCEDURE n_item(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                   nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                   nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                   nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                   nbValorVar  APDIGB.TBG004_ITEM.NB_VALOR_VAR%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
    VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,nbValorVar,'ArialBold9','0','T',NULL,NULL,NULL,NULL); 
  END;
  PROCEDURE n_item_x(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                     nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                     nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                     nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                     chTipo      APDIGB.TBG004_ITEM.CH_TIPO%TYPE,
                     nuAncho     APDIGB.TBG004_ITEM.NU_ANCHO%TYPE,
                     nuScX       APDIGB.TBG004_ITEM.NU_SC_X%TYPE,
                     nuScY       APDIGB.TBG004_ITEM.NU_SC_Y%TYPE,
                     idxCli      APDIGB.TBG004_ITEM.NU_IDX_CLIENTE%TYPE) IS
  cItemImg VARCHAR2(1);
  BEGIN
    IF (nbNombreVar = 'JPG1' OR nbNombreVar = 'JPG2') THEN
      IF nbNombreVar = 'JPG1' THEN
        cItemImg := '1';
      ELSE
        cItemImg := '2';
      END IF;
      INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,cItemImg,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
      --INSERT INTO APDIGB.TBG007_ITEM_IMG(CD_ITEM) VALUES(itemSeq);
    ELSE
      INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,NULL,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
    END IF;
  END;
  PROCEDURE replicate_template (cdPlantilla NUMBER,cdPlantillaDest NUMBER) IS
    iSec NUMBER;
    CURSOR cSec IS
      SELECT * FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = cdPlantilla;
    CURSOR cItm(cdSec NUMBER) IS
      SELECT * FROM APDIGB.TBG004_ITEM    WHERE CD_SECCION = cdSec;
  BEGIN
    SELECT MAX(CD_SECCION)+1 INTO iSec FROM APDIGB.TBG003_SECCION;
    FOR rSec IN cSec LOOP
      INSERT INTO APDIGB.TBG003_SECCION(CD_SECCION, CD_PLANTILLA, NU_PAGINA) VALUES(iSec,cdPlantillaDest,rSec.NU_PAGINA);
      FOR rItm IN cItm(rSec.CD_SECCION) LOOP
        INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
        VALUES(APDIGB.item_seq.NEXTVAL,iSec,rItm.ST_ACTIVO,rItm.NU_COOR_X,rItm.NU_COOR_Y,rItm.NB_NOMBRE_VAR,rItm.NB_VALOR_VAR,rItm.NB_FUENTE,rItm.NU_ALINEAR,rItm.CH_TIPO,rItm.NU_ANCHO,rItm.NU_SC_X,rItm.NU_SC_Y,rItm.NU_IDX_CLIENTE);
      END LOOP;
      iSec := iSec + 1;
    END LOOP;
  END;
BEGIN
  
  DELETE FROM APDIGB.TBG004_ITEM 
  WHERE  CD_SECCION IN (SELECT CD_SECCION 
                        FROM   APDIGB.TBG003_SECCION
                        WHERE  CD_PLANTILLA IN (3,4));
  DELETE FROM APDIGB.TBG003_SECCION WHERE  CD_PLANTILLA IN (4);

  n_item(3,358,801,'SOL0|A01','');
  n_item(3,471,801,'SOL0|A02','');
  n_item(3,125,737,'SOL0|A03','');
  n_item(3,397,737,'SOL0|A04','');
  n_item(3,125,719,'SOL0|A05','');
  n_item(3,397,719,'SOL0|A06','');
  n_item(3,125,684,'SOL0|A07','');
  n_item(3,397,684,'SOL0|A08','');
  n_item(3,125,665,'SOL0|A09','');
  n_item(3,440,665,'SOL0|A14','');
  n_item(3,397,649,'SOL0|A16','');
  n_item(3,125,649,'SOL0|A17','');
  n_item(3,125,598,'SOL0|A18','');
  n_item(3,398,598,'SOL0|A19','');
  n_item(3,125,580,'SOL0|A20','');
  n_item(3,398,580,'SOL0|A21','');
  n_item(3,125,565,'SOL0|A22','');
  n_item(3,398,565,'SOL0|A23','');
  n_item(3,125,549,'SOL0|A24','');
  n_item(3,398,549,'SOL0|A25','');
  n_item(3,125,529,'SOL0|A26','');
  n_item(3,398,529,'SOL0|A29','');
  n_item(3,170,457,'SOL0|A30','');
  n_item(3,220,457,'SOL0|A31','');
  n_item(3,25,333,'SOL0|A56','');
  n_item(3,25,311,'SOL0|A57','');
  n_item(3,25,291,'SOL0|A58','');
  n_item(3,25,270,'SOL0|A59','');
  n_item(3,222,291,'SOL0|A61','');
  n_item(3,28,96,'SOL0|A63','');
  n_item(3,28,74,'SOL0|A64','');
  
  n_item_x(4,222,570,'huella','I',130,57,75,0);

  UPDATE APDIGB.TBG004_ITEM 
  SET    NU_ANCHO = 26
  WHERE  NB_NOMBRE_VAR = 'SOL0|A17';
  
  replicate_template (3,4);

  commit;
END;