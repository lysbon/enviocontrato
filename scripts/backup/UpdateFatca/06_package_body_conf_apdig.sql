CREATE OR REPLACE 
PACKAGE BODY APDIGB.PBG_ENV_DIGITAL_CONF AS 

  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  PROCEDURE PRO_RETURN_KIT_PLANTILLA(
    kit_id     IN     APDIGB.TBG000_KIT.CD_KIT%TYPE,
    o_cursor   IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT CD_KIT,NB_CORREO_ASUNTO,ST_ENV_CORREO,ST_FILEUNICO,FL_VAL_HUELLA,NB_CORREO_ENGINE
      FROM   APDIGB.TBG000_KIT
      WHERE  CD_KIT = kit_id;
  END PRO_RETURN_KIT_PLANTILLA;

  PROCEDURE PRO_RETURN_PLANTILLAS(
    kit_id     IN     APDIGB.TBG000_KIT.CD_KIT%TYPE,
    o_cursor   IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT PLT.CD_PLANTILLA, PLT.NB_NOMBRE_FORMATO, PLT.NB_TIPO_FILEUNICO, PLT.NB_NOMBRE_CORREO,
             PLT.ST_CONDICION,PLT.ST_FILEUNICO,PLT.ST_ENV_CORREO,PLT.ST_TIPO_CONSTR,PLT.ST_SIGNED,
             KP.CD_KIT
      FROM   APDIGB.TBG002_PLANTILLA PLT, APDIGB.TBG001_KIT_PLANTILLA KP
      WHERE  KP.CD_KIT = kit_id
        AND  KP.CD_PLANTILLA = PLT.CD_PLANTILLA
        AND  ST_ACTIVO = 'A'
      ORDER BY PLT.CD_PLANTILLA;
  END PRO_RETURN_PLANTILLAS;
  
  PROCEDURE PRO_RETURN_PLANTILLA(
    plantilla_id   IN     APDIGB.TBG002_PLANTILLA.CD_PLANTILLA%TYPE,
    o_cursor       IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT PLT.CD_PLANTILLA, PLT.NB_NOMBRE_FORMATO, PLT.NB_TIPO_FILEUNICO, PLT.NB_NOMBRE_CORREO,
             PLT.ST_CONDICION,PLT.ST_FILEUNICO,PLT.ST_ENV_CORREO,PLT.ST_TIPO_CONSTR,PLT.ST_SIGNED,PLT.ST_ACTIVO
      FROM   APDIGB.TBG002_PLANTILLA PLT
      WHERE  PLT.CD_PLANTILLA = plantilla_id
      ORDER  BY PLT.CD_PLANTILLA;
  END PRO_RETURN_PLANTILLA;
  
  PROCEDURE PRO_RETURN_SECCIONES(
    plantilla_id   IN     APDIGB.TBG003_SECCION.CD_PLANTILLA%TYPE,
    o_cursor       IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT CD_SECCION, CD_PLANTILLA, NU_PAGINA
      FROM   APDIGB.TBG003_SECCION
      WHERE  CD_PLANTILLA = plantilla_id;
  END PRO_RETURN_SECCIONES;
  
  PROCEDURE PRO_RETURN_ELEMENTOS(
    seccion_id   IN     APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
    o_cursor     IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT CD_ITEM, CD_SECCION, NU_COOR_X, NU_COOR_Y, NU_ANCHO, NU_SC_X, NU_SC_Y, 
             NB_NOMBRE_VAR , NB_VALOR_VAR, NB_FUENTE, NU_ALINEAR, CH_TIPO, NU_IDX_CLIENTE
      FROM   APDIGB.TBG004_ITEM
      WHERE  CD_SECCION = seccion_id
        AND  ST_ACTIVO = 'A';
  END PRO_RETURN_ELEMENTOS;
  
  PROCEDURE PRO_RETURN_FLAG_PLANTILLAS(
    plantilla_id   IN     APDIGB.TBG003_SECCION.CD_PLANTILLA%TYPE,
    o_cursor       IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT F.CD_FLAG,F.NB_CONDICION_NOMBRE,F.NB_CONDICION_VALOR,F.NU_ORDEN,FP.CD_PLANTILLA,FP.ST_VALOR
      FROM   APDIGB.TBG005_FLAG F, APDIGB.TBG006_FLAG_PLANTILLA FP
      WHERE  FP.CD_PLANTILLA = plantilla_id
        AND  FP.CD_FLAG = F.CD_FLAG
        AND  FP.ST_ACTIVO = 'A';
  END PRO_RETURN_FLAG_PLANTILLAS;

  PROCEDURE PRO_RETURN_FLAG_TIPO(
    ch_tipo_in     IN APDIGB.TBG005_FLAG.CH_TIPO%TYPE,
    o_cursor       IN OUT g_cursor) IS
  BEGIN
    OPEN o_cursor FOR
      SELECT F.CD_FLAG,F.NB_CONDICION_NOMBRE,F.NB_CONDICION_VALOR,F.NU_ORDEN
      FROM   APDIGB.TBG005_FLAG F
      WHERE  F.CH_TIPO = ch_tipo_in;
  END PRO_RETURN_FLAG_TIPO;
  
  PROCEDURE PRO_GUARDAR_TRAMA_ERROR(
    st_moti      IN     APDIGB.TBG008_TRAMA.CH_MOTI%TYPE,
    st_trama     IN     APDIGB.TBG008_TRAMA.CH_TRAMA%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG008_TRAMA(CH_MOTI,CH_TRAMA,FH_CREACION)
    VALUES (SUBSTR(st_moti,0,40),SUBSTR(st_trama,0,4000),SYSDATE);
  END;
  
END PBG_ENV_DIGITAL_CONF;