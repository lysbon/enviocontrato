DECLARE
  PROCEDURE n_item(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                   nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                   nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                   nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                   nbValorVar  APDIGB.TBG004_ITEM.NB_VALOR_VAR%TYPE) IS
  BEGIN
    INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
    VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,nbValorVar,'Arial9','0','T',NULL,NULL,NULL,NULL); 
  END;
  PROCEDURE n_item_x(cdSeccion   APDIGB.TBG004_ITEM.CD_SECCION%TYPE,
                     nuCoorX     APDIGB.TBG004_ITEM.NU_COOR_X%TYPE,
                     nuCoorY     APDIGB.TBG004_ITEM.NU_COOR_Y%TYPE,
                     nbNombreVar APDIGB.TBG004_ITEM.NB_NOMBRE_VAR%TYPE,
                     chTipo      APDIGB.TBG004_ITEM.CH_TIPO%TYPE,
                     nuAncho     APDIGB.TBG004_ITEM.NU_ANCHO%TYPE,
                     nuScX       APDIGB.TBG004_ITEM.NU_SC_X%TYPE,
                     nuScY       APDIGB.TBG004_ITEM.NU_SC_Y%TYPE,
                     idxCli      APDIGB.TBG004_ITEM.NU_IDX_CLIENTE%TYPE) IS
  cItemImg VARCHAR2(1);
  BEGIN
    IF (nbNombreVar = 'JPG1' OR nbNombreVar = 'JPG2') THEN
      IF nbNombreVar = 'JPG1' THEN
        cItemImg := '1';
      ELSE
        cItemImg := '2';
      END IF;
      INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,cItemImg,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
      --INSERT INTO APDIGB.TBG007_ITEM_IMG(CD_ITEM) VALUES(itemSeq);
    ELSE
      INSERT INTO APDIGB.TBG004_ITEM(CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE)
      VALUES(APDIGB.item_seq.NEXTVAL,cdSeccion,'A',nuCoorX,nuCoorY,nbNombreVar,NULL,NULL,'0',chTipo,nuAncho,nuScX,nuScY,idxCli);
    END IF;
  END;  
BEGIN
  
  UPDATE APDIGB.TBG002_PLANTILLA
  SET    ST_ACTIVO = 'I'
  WHERE  CD_PLANTILLA IN (6,7);

  UPDATE APDIGB.TBG002_PLANTILLA
  SET    NB_NOMBRE_CORREO = 'Declaración Jurada de Residencia Fiscal - Personas Naturales - KIT 01'
  WHERE  CD_PLANTILLA IN (5);
  
  UPDATE APDIGB.TBG006_FLAG_PLANTILLA
  SET    ST_ACTIVO = 'I'
  WHERE  CD_FLAG IN (5,6,9);
    
  DELETE FROM APDIGB.TBG004_ITEM 
  WHERE  CD_SECCION IN (SELECT CD_SECCION FROM APDIGB.TBG003_SECCION WHERE CD_PLANTILLA = 5);
  
  n_item(6,481,807,'FAT0|A01','');
  n_item(6, 60,716,'FAT0|A02','');
  n_item(6, 60,688,'FAT0|A03','');
  n_item(6,196,688,'FAT0|A04','');
  n_item(6,341,688,'FAT0|A05','');
  n_item(6,449,688,'FAT0|A06','');
  n_item(6, 60,662,'FAT0|A07','');
  n_item(6,399,662,'FAT0|A08','');
  n_item(6,490,662,'FAT0|A09','');
  n_item(6, 60,636,'FAT0|A10','');
  n_item(6,154,636,'FAT0|A11','');
  n_item(6,254,636,'FAT0|A12','');
  n_item(6,355,636,'FAT0|A13','');
  n_item(6,454,636,'FAT0|A14','');
  n_item(6, 72,537,'FAT0|A15','');
  n_item(6, 72,499,'FAT0|A16','');
  n_item(6, 72,463,'FAT0|A17','');
  n_item(6,222, 85,'FAT0|A18','');
  n_item(6,222, 74,'FAT0|A19','');
  
  n_item(6,334,538,'FAT0|A19','');
  
  n_item(6,239, 31,'FAT0|A20','');
  n_item(6,283, 31,'FAT0|A21','');
  n_item(6,297, 31,'FAT0|A22','');
  n_item(6,222, 96,'FAT0|A23',''); 
  
  n_item(6,417,148,'FAT0|A01','');
  n_item_x(6,72,226,'huella','I',130,80,104,0);

  UPDATE APDIGB.TBG004_ITEM 
  SET    NU_ANCHO = 15 
  WHERE  CD_SECCION = 6 
    AND  nb_nombre_var IN ('FAT0|A05','FAT0|A10','FAT0|A11','FAT0|A12','FAT0|A13','FAT0|A15','FAT0|A16','FAT0|A17');
  
  COMMIT;
  
END;