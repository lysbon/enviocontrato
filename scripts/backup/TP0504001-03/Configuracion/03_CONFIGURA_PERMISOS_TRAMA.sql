CREATE UNIQUE INDEX "APDIGB"."TBG008_TRAMA_PK" ON "APDIGB"."TBG008_TRAMA" ("ID_TRAMA");
ALTER TABLE "APDIGB"."TBG008_TRAMA" ADD CONSTRAINT "TBG008_TRAMA_PK" PRIMARY KEY ("ID_TRAMA");

GRANT SELECT, UPDATE, INSERT, DELETE ON "APDIGB"."TBG008_TRAMA" TO "APP_APDIGB";


commit;
