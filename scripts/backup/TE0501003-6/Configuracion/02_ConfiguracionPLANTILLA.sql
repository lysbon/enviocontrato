DECLARE
  cont NUMBER;
BEGIN
	
/********* Creacion PLANTILLA AFILIACION DE TC *********/
	
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%SOLTC%';
  
  IF cont=0 OR cont IS NULL THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'SOLTC',
        '0505',
        'SOLICITUD DE AFILIACION DE TARJETA DE CREDITO',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'P',
        'SOL_AFILIACION_TC'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='SOLTC',
      NB_TIPO_FILEUNICO='0505',
      NB_NOMBRE_CORREO ='SOLICITUD DE AFILIACION DE TARJETA DE CREDITO',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_MODIFICACION  =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      CH_TIPO_PLANTILLA='P',
      NB_DESCRIPCION   ='SOL_AFILIACION_TC' 
      WHERE NB_NOMBRE_FORMATO LIKE '%SOLTC%';
  END IF;
  
  /********* Creacion PLANTILLA CONTRATO DE TC *********/
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%CONTRATOTC%';
  
  IF cont=0 OR cont IS NULL THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'CONTRATOTC',
        '0506',
        'CONTRATO DE TARJETA DE CREDITO-PN',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'G',
        'CONTRATO_TC'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='CONTRATOTC',
      NB_TIPO_FILEUNICO='0506',
      NB_NOMBRE_CORREO ='CONTRATO DE TARJETA DE CREDITO-PN',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_MODIFICACION  =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      CH_TIPO_PLANTILLA='G',
      NB_DESCRIPCION   ='CONTRATO_TC' 
      WHERE NB_NOMBRE_FORMATO LIKE '%CONTRATOTC%';
  END IF;
  
  /********* Creacion PLANTILLA HRI TC*********/
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%HRITC%';
  
  IF cont=0 OR cont IS NULL THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'HRITC',
        '0509',
        'HOJA RESUMEN INFORMATIVA TC VISA Y MASTECARD',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'G',
        'HRI_TC'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='HRITC',
      NB_TIPO_FILEUNICO='0509',
      NB_NOMBRE_CORREO ='HOJA RESUMEN INFORMATIVA TC VISA Y MASTECARD',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_MODIFICACION  =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      CH_TIPO_PLANTILLA='G',
      NB_DESCRIPCION   ='HRI_TC' 
      WHERE NB_NOMBRE_FORMATO LIKE '%HRITC%';
  END IF;
  
  /********* Creacion PLANTILLA SEGURO TC*********/
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%SEGUROTC%';
  
  IF cont=0 OR cont IS NULL THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'SEGUROTC',
        '0512',
        'SOLICITUD CERTIFICADO SEGURO DESGRAVAMEN TC',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'P',
        'HRI_TC'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='SEGUROTC',
      NB_TIPO_FILEUNICO='0512',
      NB_NOMBRE_CORREO ='SOLICITUD CERTIFICADO SEGURO DESGRAVAMEN TC',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_MODIFICACION  =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      CH_TIPO_PLANTILLA='P',
      NB_DESCRIPCION   ='SEGUROTC' 
      WHERE NB_NOMBRE_FORMATO LIKE '%SEGUROTC%';
  END IF;
  
  /********* Creacion PLANTILLA SEGURO TC*********/
   SELECT COUNT(CD_PLANTILLA)
   INTO cont
   FROM "APDIGB"."TBG002_PLANTILLA"
   WHERE NB_NOMBRE_FORMATO LIKE '%TRAT_DATOS_LIFEMILES%';

  IF cont=0 OR cont IS NULL THEN
  
	  INSERT INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'TRAT_DATOS_LIFEMILES',
        '0510',
        'AUTORIZACION DE TRATAMIENTO DE DATOS PERSONALES LIFEMILES',
        NULL,
        NULL,
         (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
         NULL,
        'P',
        'AUTORIZA_TRAT_DATOS_LIFEMILES'
      );
  ELSE
  		UPDATE "APDIGB"."TBG002_PLANTILLA"
	    SET ST_ACTIVO      ='A',
	      ST_CONDICION     ='F',
	      ST_FILEUNICO     ='A',
	      ST_ENV_CORREO    ='A',
	      ST_TIPO_CONSTR   ='P',
	      ST_SIGNED        ='A',
	      NB_NOMBRE_FORMATO='TRAT_DATOS_LIFEMILES',
	      NB_TIPO_FILEUNICO='0510',
	      NB_NOMBRE_CORREO ='AUTORIZACION DE TRATAMIENTO DE DATOS PERSONALES LIFEMILES',
	      CD_USU_CREA      =NULL,
	      CD_USU_MODI      =NULL,
	      FH_MODIFICACION  =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
	      CH_TIPO_PLANTILLA='P',
	      NB_DESCRIPCION   ='AUTORIZA_TRAT_DATOS_LIFEMILES' 
	      WHERE NB_NOMBRE_FORMATO LIKE '%TRAT_DATOS_LIFEMILES%';
  END IF;
  
  COMMIT;
  
END;
/*
 SELECT *
 FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%SOLTC%' OR  NB_NOMBRE_FORMATO LIKE '%CONTRATOTC%' 
   OR   NB_NOMBRE_FORMATO LIKE '%HRITC%' 
   OR   NB_NOMBRE_FORMATO LIKE '%SEGUROTC%' 
 DELETE
 FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%SOLTC%' OR  NB_NOMBRE_FORMATO LIKE '%CONTRATOTC%' 
   OR   NB_NOMBRE_FORMATO LIKE '%HRITC%' 
   OR   NB_NOMBRE_FORMATO LIKE '%SEGUROTC%' 
*/
