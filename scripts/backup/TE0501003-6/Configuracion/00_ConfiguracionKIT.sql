
DECLARE
  cont NUMBER;
BEGIN

/********* Creacion KIT AFILIACION TC *********/

  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%409999ED%';
  
  IF cont=0 OR cont IS NULL THEN
    INSERT
    INTO "APDIGB"."TBG010_KIT"
      (
        CD_KIT,
        CD_CODIGO,
        NB_CORREO_ASUNTO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        NB_NOMBRE_KIT,
        ST_ENV_CORREO,
        ST_FILEUNICO,
        FL_VAL_HUELLA,
        NB_CORREO_ENGINE,
        ST_SIGNBOX,
        ST_HUELLA
      )
      VALUES
      (
        (SELECT ( (TO_NUMBER(SUBSTR(MAX(CD_KIT),0,6)) + 1)) AS CD_KIT
          FROM "APDIGB"."TBG010_KIT"
        )
        ,
        '409999ED',
        '�Tu Tarjeta de Cr�dito est� lista!',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'KIT 409999ED - AFILIACION TC',
        'A',
        'A',
        'A',
        'STRTPL',
        'A',
        'A'
      );
  ELSE
    UPDATE "APDIGB"."TBG010_KIT"
    SET NB_CORREO_ASUNTO='�Tu Tarjeta de Cr�dito est� lista!',
      CD_USU_CREA       =NULL,
      CD_USU_MODI       =NULL,
      FH_CREACION       =NULL,
      FH_MODIFICACION   =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      NB_NOMBRE_KIT     ='KIT 409999ED - AFILIACION TC',
      ST_ENV_CORREO     ='A',
      ST_FILEUNICO      ='A',
      FL_VAL_HUELLA     ='A',
      NB_CORREO_ENGINE  ='STRTPL',
      ST_SIGNBOX        ='A',
      ST_HUELLA         ='A'
    WHERE cd_codigo LIKE  '%409999ED%';
   
  END IF;
  
/********* Creacion KIT AFILIACION TC-ENVIO OFICINA *********/
  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%409999EO%';
  
  IF cont=0 OR cont IS NULL THEN
    INSERT
    INTO "APDIGB"."TBG010_KIT"
      (
        CD_KIT,
        CD_CODIGO,
        NB_CORREO_ASUNTO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        NB_NOMBRE_KIT,
        ST_ENV_CORREO,
        ST_FILEUNICO,
        FL_VAL_HUELLA,
        NB_CORREO_ENGINE,
        ST_SIGNBOX,
        ST_HUELLA
      )
      VALUES
      (
        (SELECT ( (TO_NUMBER(SUBSTR(MAX(CD_KIT),0,6)) + 1)) AS CD_KIT
          FROM "APDIGB"."TBG010_KIT"
        )
        ,
        '409999EO',
        '�Tu Tarjeta de Cr�dito est� lista!',
        NULL,
        NULL,
        (SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
        NULL,
        'KIT 409999EO - AFILIACION TC',
        'A',
        'A',
        'A',
        'STRTPL',
        'A',
        'A'
      );
  ELSE
    UPDATE "APDIGB"."TBG010_KIT"
    SET NB_CORREO_ASUNTO='�Tu Tarjeta de Cr�dito est� lista!',
      CD_USU_CREA       =NULL,
      CD_USU_MODI       =NULL,
      FH_CREACION       =NULL,
      FH_MODIFICACION   =(SELECT TO_DATE(SYSDATE,'DD/MM/YYYY') FROM DUAL),
      NB_NOMBRE_KIT     ='KIT 409999EO - AFILIACION TC',
      ST_ENV_CORREO     ='A',
      ST_FILEUNICO      ='A',
      FL_VAL_HUELLA     ='A',
      NB_CORREO_ENGINE  ='STRTPL',
      ST_SIGNBOX        ='A',
      ST_HUELLA         ='A'
    WHERE cd_codigo LIKE  '%409999EO%';
   
  END IF;

  COMMIT;
  
  
END;

/*SELECT * FROM "APDIGB"."TBG010_KIT"
WHERE cd_codigo LIKE  '%409999%';
DELETE FROM "APDIGB"."TBG010_KIT"
WHERE cd_codigo LIKE  '%409999%';
*/