DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(2);
   cont NUMBER;
     
BEGIN
    
  /*****************PLANTILLA AFILIACION DE TC *******************/
  numpagina_1:=1;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%SOLTC%' or nb_nombre_correo like '%SOLTC%' ; 
  
    select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
   
    DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
    
  end if;
  
 /*****************PLANTILLA CONTRATO DE TC*******************/
  numpagina_1:=12;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%CONTRATOTC%' or nb_nombre_correo like '%CONTRATOTC%' ; 
  
    select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
   
    DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
  
  end if;

   /*****************PLANTILLA HRI TC*******************/
  numpagina_1:=6;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%HRITC%' or nb_nombre_correo like '%HRITC%' ; 
  
    select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
  
    DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;

  end if;

  /*****************PLANTILLA SEGURO TC*******************/
  numpagina_1:=1;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%SEGUROTC%' or nb_nombre_correo like '%SEGUROTC%' ; 
  
    select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
   
    DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
  
  end if;

  numpagina_1:=17;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%SEGUROTC%' or nb_nombre_correo like '%SEGUROTC%' ; 
  
    select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont>0 THEN  
   
   	DELETE FROM "APDIGB"."TBG003_SECCION" 
    WHERE CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
    
  end if;
  
  COMMIT;
      
END;
