
DECLARE
  cont NUMBER;
BEGIN

/********* Creacion KIT AFILIACION TC *********/

  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%409999ED%';
  
  IF cont>0 THEN
  
    DELETE FROM "APDIGB"."TBG010_KIT"
    WHERE cd_codigo LIKE '%409999ED%';
     
  END IF;
  
/********* Creacion KIT AFILIACION TC-ENVIO OFICINA *********/
  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%409999EO%';
  
  IF cont>0 THEN
  	
    DELETE FROM "APDIGB"."TBG010_KIT"
    WHERE cd_codigo LIKE '%409999EO%';
   
  END IF;

  COMMIT;
  
  
END;
