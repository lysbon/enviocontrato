DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_1 number(2);
  cont NUMBER;
     
BEGIN
	
	/*SOL_AFILIACION_TC*/
	
	idxCliente:=0;
    v_numpagina_1:=1;
	
     select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
     where  nb_nombre_formato like '%SOLTC%' or nb_nombre_correo like '%SOLTC%';

    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" 
    where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;  

    IF cont>0 THEN  
    	DELETE FROM "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
    END IF;
	
    /*CONTRATO_TC*/
    
	idxCliente:=0;
    v_numpagina_1:=12;
	
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
    where   nb_nombre_formato like '%CONTRATOTC%' or nb_nombre_correo like '%CONTRATOTC%' ; 

    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;  

    IF cont>0 THEN  
    	DELETE FROM "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
    END IF;
	
    /*HRI_TC_6*/
    idxCliente:=0;
    v_numpagina_1:=6;
	
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where nb_nombre_formato like '%HRITC%' or nb_nombre_correo like '%HRITC%' ; 

    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;  

    IF cont>0 THEN  
    	DELETE FROM "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
    END IF;
	
    /*SEGUROTC_1*/
    idxCliente:=0;
    v_numpagina_1:=1;
	
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where nb_nombre_formato like '%SEGUROTC%' or nb_nombre_correo like '%SEGUROTC%' ; 
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;  

    IF cont>0 THEN  
    	DELETE FROM "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
    END IF;
	
    /*SEGUROTC_17*/
	
    idxCliente:=0;
    v_numpagina_1:=17;
	
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where nb_nombre_formato like '%SEGUROTC%' or nb_nombre_correo like '%SEGUROTC%' ; 
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
	
    select  COUNT(CD_ITEM) into cont from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;  

    IF cont>0 THEN  
    	DELETE FROM "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
    END IF;
	
	COMMIT;

END;


