/********* 4. Reversion del TBG005_FLAG **********/
set serveroutput on
DECLARE

    CURSOR C_FLAG IS select CD_FLAG from "APDIGB"."TBG005_FLAG" where  nb_condicion_nombre like '%CFG0|P22%' or nb_condicion_nombre like '%CTR0|P22%' ; 
     CURSOR C_FLAG_2 IS select CD_FLAG from "APDIGB"."TBG005_FLAG" where  nb_condicion_nombre like '%CFG0|P24%' or nb_condicion_nombre like '%CTR0|P24%' ;   
    
    v_flag "APDIGB"."TBG005_FLAG".CD_FLAG%TYPE;
    v_cont number;
BEGIN

  v_cont:=0;
  
   OPEN C_FLAG;
      LOOP
         FETCH C_FLAG  INTO v_flag;
         EXIT WHEN C_FLAG%NOTFOUND;         
              select count(CD_FLAG) into v_cont from "APDIGB"."TBG006_FLAG_PLANTILLA" where  CD_FLAG=v_flag;   
               
             
               
              IF v_cont=0 THEN 
               
                delete from "APDIGB"."TBG005_FLAG" where CD_FLAG= v_flag;  
                commit; 
              
              END IF;
               
               
			   
      END LOOP;
    CLOSE C_FLAG;
    
    
     OPEN C_FLAG_2;
      LOOP
         FETCH C_FLAG_2  INTO v_flag;
         EXIT WHEN C_FLAG_2%NOTFOUND;         
              select count(CD_FLAG) into v_cont from "APDIGB"."TBG006_FLAG_PLANTILLA" where  CD_FLAG=v_flag;   
               
               
               
              IF v_cont=0 THEN 
               
                delete from "APDIGB"."TBG005_FLAG" where CD_FLAG= v_flag;  
                commit; 
              
              END IF;
               
               
			   
      END LOOP;
    CLOSE C_FLAG_2;
    
  

   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en plantilla para el BG0D1 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
  
END;
/********* 4. Reversion del adicional de la plantilla en los productos 02 **********/