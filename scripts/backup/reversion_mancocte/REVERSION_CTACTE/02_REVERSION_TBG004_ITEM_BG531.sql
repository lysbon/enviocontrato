/* PLANTILLA BG481 - CONTR. CTA CTE (EUR) - PAGINA 2*/
set serveroutput on
DECLARE

   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  
   numpagina_2 number(2);
   numpagina_1 number(1);
     
BEGIN
  
  
  
  numpagina_2:=4;  
  
  numpagina_1:=1;	 
       
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG531%' or nb_nombre_correo like '%BG531%' ; 
    
  select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_1; 
	
	delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
	
	select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_2; 
	
	delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
  	
	COMMIT;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
    
END;
/* PLANTILLA BG481 - CONTR. CTA CTE (EUR) - PAGINA 2*/