/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
DECLARE
    
    CURSOR C_TBG001_KIT_PLANTILLA IS select CD_KIT,CD_PLANTILLA  from "APDIGB"."TBG011_KIT_PLANTILLA" ;
    
    V_CD_KIT  "APDIGB"."TBG011_KIT_PLANTILLA".CD_KIT%TYPE;
    V_CD_PLANTILLA   "APDIGB"."TBG011_KIT_PLANTILLA".CD_PLANTILLA%TYPE;
    V_CD_KIT_NEW  "APDIGB"."TBG001_KIT_PLANTILLA".CD_KIT%TYPE; 
     
    cont NUMBER;
   
BEGIN
   
    cont:=0;
    OPEN C_TBG001_KIT_PLANTILLA;
      LOOP
         FETCH C_TBG001_KIT_PLANTILLA  INTO V_CD_KIT,V_CD_PLANTILLA;
         EXIT WHEN C_TBG001_KIT_PLANTILLA%NOTFOUND;         
           
            select CD_CODIGO into V_CD_KIT_NEW from "APDIGB"."TBG010_KIT" WHERE CD_KIT=V_CD_KIT;
           
           select COUNT(*) into cont from "APDIGB"."TBG001_KIT_PLANTILLA" WHERE CD_KIT=V_CD_KIT_NEW AND CD_PLANTILLA=V_CD_PLANTILLA;
            
           IF cont=0 THEN 
            
                 DELETE FROM "APDIGB"."TBG011_KIT_PLANTILLA" WHERE CD_KIT=V_CD_KIT AND CD_PLANTILLA=V_CD_PLANTILLA ; 
                 commit;
                  
           END IF;
        
    END LOOP;

  CLOSE C_TBG001_KIT_PLANTILLA;

   
END;

/********* 5. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
