/********* 4. Reversion del adicional de la plantilla en los productos 02 **********/
set serveroutput on
DECLARE

    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '  ALTER TABLE "APDIGB"."TBG002_PLANTILLA" DROP COLUMN TIPO_PLANTILLA ' ;
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite la columna tipo plantilla '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* 4. Reversion del adicional de la plantilla en los productos 02 **********/