/********* 1. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
set serveroutput on
DECLARE
    
    CURSOR C_PLANTILLA IS select CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG0A3%' or nb_nombre_correo like '%BG0A3%' order by CD_PLANTILLA ;   
    v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   
BEGIN
   
    OPEN C_PLANTILLA;
      LOOP
         FETCH C_PLANTILLA  INTO v_plantilla;
         EXIT WHEN C_PLANTILLA%NOTFOUND;         
          
                delete from "APDIGB"."TBG006_FLAG_PLANTILLA" where CD_PLANTILLA=v_plantilla; 		  
                commit; 
			   
      END LOOP;

  CLOSE C_PLANTILLA;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en seccion o plantilla para el BG0A3 '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;

/********* 1. Agregamos la configuracion de mancomunadas a la tabla TBG006_FLAG_PLANTILLA ******************/
