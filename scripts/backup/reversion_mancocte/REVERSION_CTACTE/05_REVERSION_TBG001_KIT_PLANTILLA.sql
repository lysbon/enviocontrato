/*KIT BG011 - CONTR. CTA CTE (PEN)*/
set serveroutput on
DECLARE

   v_kit TBG000_KIT.cd_kit%TYPE;

     
BEGIN
   v_kit:='010001';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010002';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010008';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010091';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010092';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010041';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
      
   v_kit:='010042';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
    v_kit:='010031';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
    v_kit:='010032';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;

    v_kit:='010034';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
   
   v_kit:='010093';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;

   v_kit:='010094';
   
   delete from "APDIGB"."TBG001_KIT_PLANTILLA" where CD_KIT=v_kit;
    
   commit;	
   
   
  EXCEPTION 
 
   WHEN no_data_found THEN 
      dbms_output.put_line('No exite datos en los TBG001_KIT_PLANTILLA '); 
   WHEN others THEN 
      dbms_output.put_line('Error!');   
  
END;

/*KIT BG011 - CONTR. CTA CTE (PEN)*/

