/* RELACIONA KITS CON PLANTILLAS  */

DECLARE

   V_KIT "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
   V_CD_PLANTILLA "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
     
BEGIN
   
   select  CD_KIT into V_KIT from "APDIGB"."TBG010_KIT" WHERE NB_NOMBRE_KIT like '%99001CLS%'; 
  
   select  CD_PLANTILLA into V_CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE281%'  ; 
   
   Insert into "APDIGB"."TBG011_KIT_PLANTILLA"(CD_KIT,CD_PLANTILLA) values (V_KIT,V_CD_PLANTILLA);
   
   
   select  CD_KIT into V_KIT from "APDIGB"."TBG010_KIT" WHERE NB_NOMBRE_KIT like '%99001CLS%'; 
  
   select  CD_PLANTILLA into V_CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE101%'  ; 
   
   Insert into "APDIGB"."TBG011_KIT_PLANTILLA"(CD_KIT,CD_PLANTILLA) values (V_KIT,V_CD_PLANTILLA);
   
   commit;
  
END;

/* RELACIONA KITS CON PLANTILLAS  */