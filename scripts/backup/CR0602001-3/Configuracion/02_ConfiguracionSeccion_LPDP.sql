
DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   numpagina_1 number(1);
   cont NUMBER;
     
BEGIN
    
    /*****************PE281*******************/  
    
    numpagina_1:=2;
   
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE281%' or nb_nombre_correo like '%PE281%' ;
   
    select  COUNT(CD_SECCION) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1; 
  
  IF cont=0 THEN  
   
    INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_1,
    null,null,null,null);
  
  else
  
    UPDATE  "APDIGB"."TBG003_SECCION" SET CD_PLANTILLA=v_cd_plantilla,NU_PAGINA=numpagina_1,
    CD_USU_CREA=null,CD_USU_MODI=null,FH_CREACION=null,FH_MODIFICACION=null where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
    
 end if;
 
  /*****************PE281*******************/
  
  /*****************PE101*******************/
	numpagina_1:=1;
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" 
   where  nb_nombre_formato like '%PE101%' or nb_nombre_correo like '%PE101%' ; 
  
  select  COUNT(CD_PLANTILLA) into cont from "APDIGB"."TBG003_SECCION" where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;  
  
  IF cont=0 THEN  
   
    INSERT INTO "APDIGB"."TBG003_SECCION" (CD_SECCION,CD_PLANTILLA,NU_PAGINA,
    CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES ((SELECT MAX(CD_SECCION)+1 FROM "APDIGB"."TBG003_SECCION"),v_cd_plantilla,numpagina_1,
    null,null,null,null);
    
  else
      
    UPDATE  "APDIGB"."TBG003_SECCION" SET CD_PLANTILLA=v_cd_plantilla,NU_PAGINA=numpagina_1,
    CD_USU_CREA=null,CD_USU_MODI=null,FH_CREACION=null,FH_MODIFICACION=null where  
    CD_PLANTILLA=v_cd_plantilla and  NU_PAGINA=numpagina_1;
  
  end if;
  
 /*****************PE101*******************/
  
  COMMIT;
      
END;
/**** SECCION PE281 - AUTORIZACION PARA EL USO TRATAMIENTO DE DATOS PERSONALES *****/