DECLARE
    
  CURSOR C_FLAG IS 
    SELECT CD_FLAG  
    FROM   "APDIGB"."TBG005_FLAG" 
    WHERE  nb_condicion_nombre LIKE '%CFG0|P33%' 
    ORDER BY CD_FLAG;
    
  CURSOR C_PLANTILLA IS 
    SELECT CD_PLANTILLA 
    FROM   "APDIGB"."TBG002_PLANTILLA" 
    WHERE  NB_DESCRIPCION LIKE '%PE281 - LPDP - CONTRATOS%' 
    ORDER BY CD_PLANTILLA;
  
  v_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_flag "APDIGB"."TBG005_FLAG".CD_FLAG%TYPE;
    
  TYPE t_plantilla IS TABLE OF NUMBER(4) INDEX BY BINARY_INTEGER;
  arrayPlantilla t_plantilla;
  cont NUMBER;
   
BEGIN
   
  cont:=1;
  OPEN C_PLANTILLA;
  LOOP
    FETCH C_PLANTILLA  INTO v_plantilla;
    EXIT WHEN C_PLANTILLA%NOTFOUND;         
    arrayPlantilla(cont):=v_plantilla;       
    cont:=cont+1;
  END LOOP;
  CLOSE C_PLANTILLA;

  cont:=1;
  OPEN C_FLAG;
  LOOP
    FETCH C_FLAG  INTO v_flag;
    EXIT WHEN C_FLAG%NOTFOUND;         
    INSERT INTO "APDIGB"."TBG006_FLAG_PLANTILLA" (
      CD_FLAG,CD_PLANTILLA,ST_VALOR,ST_ACTIVO,
      CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION) 
    VALUES (v_flag,arrayPlantilla(cont),'A','A',
      null,null,null,null);
    cont:=cont+1;
  END LOOP;
  CLOSE C_FLAG;
  
  COMMIT;

END;


