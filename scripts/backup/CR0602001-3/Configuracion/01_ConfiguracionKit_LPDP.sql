
DECLARE
  cont NUMBER;
BEGIN

/********* Creacion KIT LPDP *********/
  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%99001CLS%';
  
  IF cont=0 THEN
    INSERT
    INTO "APDIGB"."TBG010_KIT"
      (
        CD_KIT,
        CD_CODIGO,
        NB_CORREO_ASUNTO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        NB_NOMBRE_KIT,
        ST_ENV_CORREO,
        ST_FILEUNICO,
        FL_VAL_HUELLA,
        NB_CORREO_ENGINE,
        ST_SIGNBOX,
        ST_HUELLA
      )
      VALUES
      (
        (SELECT ( (TO_NUMBER(SUBSTR(MAX(CD_KIT),0,6)) + 1)) AS CD_KIT
          FROM "APDIGB"."TBG010_KIT"
        )
        ,
        '99001CLS',
        'BBVA - Tratamiento de datos personales',
        NULL,
        NULL,
        NULL,
        NULL,
        'KIT 99001CLS -  KIT LPDP',
        'A',
        'A',
        'A',
        'STRTPL',
        'A',
        'A'
      );
  ELSE
    UPDATE "APDIGB"."TBG010_KIT"
    SET NB_CORREO_ASUNTO='BBVA - Tratamiento de datos personales',
      CD_USU_CREA       =NULL,
      CD_USU_MODI       =NULL,
      FH_CREACION       =NULL,
      FH_MODIFICACION   =NULL,
      NB_NOMBRE_KIT     ='KIT 99001CLS -  KIT LPDP',
      ST_ENV_CORREO     ='A',
      ST_FILEUNICO      ='A',
      FL_VAL_HUELLA     ='A',
      NB_CORREO_ENGINE  ='STRTPL',
      ST_SIGNBOX        ='A',
      ST_HUELLA         ='A'
    WHERE cd_codigo LIKE  '%99001CLS%';
   
  END IF;
 /********* Creacion KIT LPDP *********/ 
  
  
/********* Creacion de plantillas para AUTORIZACION PARA EL USO TRATAMIENTO DE DATOS PERSONALES - PE281 *********/
      
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%PE281%';
  
  
  IF cont=0 THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'PE281',
        '0498',
        'PE281 - ACEPTACION DE LEY DE PROTECCION DE DATOS',
        NULL,
        NULL,
        NULL,
        '05/06/17',
        'G',
        'PE281 - LPDP'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='PE281',
      NB_TIPO_FILEUNICO='0498',
      NB_NOMBRE_CORREO ='PE281 - ACEPTACION DE LEY DE PROTECCION DE DATOS',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_CREACION      =NULL,
      FH_MODIFICACION  ='05/06/17',
      CH_TIPO_PLANTILLA='G',
      NB_DESCRIPCION   ='PE281 - LPDP' 
      WHERE NB_NOMBRE_FORMATO LIKE '%PE281%';
  END IF;
  
  /********* Creacion de plantillas para AUTORIZACION PARA EL USO TRATAMIENTO DE DATOS PERSONALES - PE281 *********/
  
  /********* Creacion de plantillas para ACTUALIZACION DE DATOS DE CONTACTO DE CLIENTE *********/
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%PE101%';
  
  
  IF cont=0 THEN
    
    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'PE101',
        '0499',
        'PE101 - ACTUALIZACION DE DATOS PERSONALES',
        NULL,
        NULL,
        NULL,
        '05/06/17',
        'P',
        'PE101 - LPDP'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO          ='A',
      ST_CONDICION         ='F',
      ST_FILEUNICO         ='A',
      ST_ENV_CORREO        ='A',
      ST_TIPO_CONSTR       ='P',
      ST_SIGNED            ='A',
      NB_NOMBRE_FORMATO    ='PE101',
      NB_TIPO_FILEUNICO    ='0499',
      NB_NOMBRE_CORREO     ='PE101 - ACTUALIZACION DE DATOS PERSONALES',
      CD_USU_CREA          =NULL,
      CD_USU_MODI          =NULL,
      FH_CREACION          =NULL,
      FH_MODIFICACION      ='05/06/17',
      CH_TIPO_PLANTILLA    ='P',
      NB_DESCRIPCION       ='PE101 - LPDP'
    WHERE NB_NOMBRE_FORMATO LIKE '%PE101%';
  END IF;
   /********* Creacion de plantillas para ACTUALIZACION DE DATOS DE CONTACTO DE CLIENTE *********/ 
  
  COMMIT;
  
  
END;