DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_1 number(1);
 
     
BEGIN
 
    /***********************************PE281*********************************/
 
   idxCliente:=0;
   v_numpagina_1:=2;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where    NB_DESCRIPCION LIKE '%PE281 - LPDP - CONTRATOS%'; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
   /***************************************************************************/
   
   
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','353','669','CLS0|G01',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
									   			  								   
                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','520','CLS0|G02',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       

                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','260','490','CLS0|G03',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
	                                  
                                       
  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
  CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
  values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','220','650','huella',null,'null','0','I','130','80','104',idxCliente,
  null,null,null,null,null);
          
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','450','barcode',null,null,'0','B','171','171','25',idxCliente,
   null,null,null,null,null);  
    
   /***********************************PE281*********************************/ 

                                       
    COMMIT;
    
END;
