/********* Creacion de plantillas para AUTORIZACION PARA EL USO TRATAMIENTO DE DATOS PERSONALES - PE281 *********/
  DECLARE
  
  cont NUMBER;
  
  BEGIN
      
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_DESCRIPCION LIKE '%PE281 - LPDP - CONTRATOS%';
  
  
  IF cont=0 THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'V',
        'A',
        'A',
        'P',
        'A',
        'PE281',
        '0498',
        'PE281 - ACEPTACION DE LEY DE PROTECCION DE DATOS',
        NULL,
        NULL,
        NULL,
        '06/03/18',
        'P',
        'PE281 - LPDP - CONTRATOS'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='V',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='PE281',
      NB_TIPO_FILEUNICO='0498',
      NB_NOMBRE_CORREO ='PE281 - ACEPTACION DE LEY DE PROTECCION DE DATOS',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_CREACION      =NULL,
      FH_MODIFICACION  ='06/03/18',
      CH_TIPO_PLANTILLA='P',
      NB_DESCRIPCION   ='PE281 - LPDP - CONTRATOS'
      WHERE NB_DESCRIPCION LIKE '%PE281 - LPDP - CONTRATOS%';
  END IF;
  
  commit;
  END;
  