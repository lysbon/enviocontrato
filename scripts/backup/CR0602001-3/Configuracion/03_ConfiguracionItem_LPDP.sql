/**** PLANTILLA BG561 - AFILIZACION MULTIPLICA TU INTERES *****/
DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_1 number(1);
 
     
BEGIN
 
    /***********************************PE281*********************************/
 
   idxCliente:=0;
   v_numpagina_1:=2;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE281%' or nb_nombre_correo like '%PE281%' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','353','669','CLS0|G01',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','520','CLS0|G02',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       

                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','260','490','CLS0|G03',null,'ArialBold9','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
	
                                       
                                       
  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
  CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
  values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','220','650','huella',null,'null','0','I','130','80','104',idxCliente,
  null,null,null,null,null);
          
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','450','barcode',null,null,'0','B','171','171','25',idxCliente,
   null,null,null,null,null);  
    
   /***********************************PE281*********************************/ 
  
   /***********************************PE101*********************************/
   
   idxCliente:=0;
   
   v_numpagina_1:=1;
   
   
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE101%' or nb_nombre_correo like '%PE101%' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','77','738','HAD0|G01',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
									   
                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','77','725','HAD0|G02',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       

                                       
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','77','710','HAD0|G03',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       
      Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','475','710','HAD0|G04',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);                                    
									   
									   
	Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','697','HAD0|G11',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
									   

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','684','HAD0|G12',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','671','HAD0|G13',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','658','HAD0|G14',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','645','HAD0|G15',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','632','HAD0|G16',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','619','HAD0|G17',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','606','HAD0|G18',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','593','HAD0|G19',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','95','576','HAD0|G05',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);


Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','95','566','HAD0|G06',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
                                       
                                       

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','90','548','HAD0|G07',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','537','HAD0|G08',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);		

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','526','HAD0|G09',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','515','HAD0|G20',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	  

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','504','HAD0|G21',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','493','HAD0|G22',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	


Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','482','HAD0|G23',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','471','HAD0|G24',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','460','HAD0|G25',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	

Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','33','449','HAD0|G26',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);


 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','247','249','HAD0|G01',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	  

                                       
 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','215','414','HAD0|G27',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	                      
                                       
    Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','230','414','HAD0|G28',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	                                   
									   
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','145','206','HAD0|G10',null,'ArialBold8','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);	                  
	
                                       
                                       
  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
  CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
  values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','230','395','huella',null,'null','0','I','130','80','104',idxCliente,
  null,null,null,null,null);
          
   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','160','barcode',null,null,'0','B','171','171','25',idxCliente,
   null,null,null,null,null);
   
      /***********************************PE101*********************************/
                                       
    COMMIT;
    
END;
/**** PLANTILLA BG561 - AFILIZACION MULTIPLICA TU INTERES *****/