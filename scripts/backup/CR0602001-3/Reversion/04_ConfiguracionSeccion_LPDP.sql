
DECLARE
 
   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
     
BEGIN
  
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE281%' or nb_nombre_correo like '%PE281%' ; 
   
  delete from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=v_cd_plantilla ;
  
  select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%PE101%' or nb_nombre_correo like '%PE101%' ; 
  
  delete from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=v_cd_plantilla ;
  
  COMMIT;
      
END;
/**** SECCION PE281 - AUTORIZACION PARA EL USO TRATAMIENTO DE DATOS PERSONALES *****/