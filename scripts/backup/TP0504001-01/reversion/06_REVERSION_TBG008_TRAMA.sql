/********* REVERSION Configuracion DET_STEP   **********/
DECLARE
    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '  ALTER TABLE APDIGB.TBG008_TRAMA DROP COLUMN DET_STEP ' ;
 
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite la columna DET_STEP '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* REVERSION Configuracion DET_STEP   **********/