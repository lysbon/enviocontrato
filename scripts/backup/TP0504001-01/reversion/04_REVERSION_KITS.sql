/********* reversion  de plantillas para cancelacion de cuentas *********/

delete from  "APDIGB"."TBG002_PLANTILLA" where NB_NOMBRE_FORMATO LIKE '%VOUCHER_CANCEL_CTA_AHORRO%';
delete from  "APDIGB"."TBG002_PLANTILLA" where NB_NOMBRE_FORMATO LIKE '%VOUCHER_CANCEL_CTA_CTE%';
delete from  "APDIGB"."TBG002_PLANTILLA" where NB_NOMBRE_FORMATO LIKE '%VOUCHER_CANCEL_CTS%';

COMMIT;

/********* reversion  de plantillas para cancelacion de cuentas *********/



/********* reversion de VOUCHER *********/

delete from  "APDIGB"."TBG010_KIT" where NB_NOMBRE_KIT LIKE '%KIT CONSTANCIA DE CANCELACION AHORRO%';

delete from  "APDIGB"."TBG010_KIT" where NB_NOMBRE_KIT LIKE '%KIT CONSTANCIA DE CANCELACION CTA CTE%';

delete from  "APDIGB"."TBG010_KIT" where NB_NOMBRE_KIT LIKE '%KIT CONSTANCIA DE CANCELACION CTS%';

COMMIT;

/********* reversion de VOUCHER *********/

