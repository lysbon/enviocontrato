/********* REVERSION Configuracion del flag de sign box   **********/
DECLARE
    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '  ALTER TABLE APDIGB.TBG010_KIT DROP COLUMN ST_HUELLA ' ;
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite la columna ST_HUELLA '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* REVERSION Configuracion del flag de sign box   **********/