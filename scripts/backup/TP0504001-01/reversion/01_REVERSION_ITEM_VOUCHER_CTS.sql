/* REVERSION DE APDIGB.TBG004_ITEM */
DECLARE

   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;

   numpagina_2 number(1);
     
BEGIN
  
    numpagina_2:=1;     
       
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%VOUCHER_CANCEL_CTS%' or nb_nombre_correo like '%VOUCHER_CANCEL_CTS%' ; 
    
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_2;
    
    delete from APDIGB.TBG004_ITEM where CD_SECCION=v_cd_seccion   ; 
    
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%VOUCHER_CANCEL_CTA_CTE%' or nb_nombre_correo like '%VOUCHER_CANCEL_CTA_CTE%' ; 
    
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_2; 
    
    delete from APDIGB.TBG004_ITEM where CD_SECCION=v_cd_seccion   ; 
     
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%VOUCHER_CANCEL_CTA_AHORRO%' or nb_nombre_correo like '%VOUCHER_CANCEL_CTA_AHORRO%' ; 
    
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=numpagina_2;  
     
    delete from APDIGB.TBG004_ITEM where CD_SECCION=v_cd_seccion   ;  
   
    COMMIT;
    
END;

/* REVERSION DE APDIGB.TBG004_ITEM */