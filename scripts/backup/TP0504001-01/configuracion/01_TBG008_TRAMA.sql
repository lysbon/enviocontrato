/********* Configuracion del flag de sign box   **********/
DECLARE
    sql_stmnt varchar2(400) ;
BEGIN

    sql_stmnt := '   ALTER TABLE APDIGB.TBG008_TRAMA ADD DET_STEP VARCHAR(50 BYTE)' ;
    dbms_output.put_line(sql_stmnt );
    Execute immediate  sql_stmnt ;
  
   EXCEPTION 
 
    WHEN no_data_found THEN 
      dbms_output.put_line('No exite la columna sing box '); 
   WHEN others THEN 
      dbms_output.put_line('Error!'); 
   
END;
/********* Configuracion del flag de sign box   **********/