/*

Proyecto: File Unico
Cliente: BBVA
Descripción: Acualización de Catálogo 03-11-2017

*/

SET LINESIZE 4000;
SET PAGESIZE 9999;
SET ECHO OFF;

/* spool &logname */
SPOOL FU_GESDOC.LOG

PROMPT "**************INICIO SCRIPT**************"
PROMPT "************** CARGANDO VALORES TB_CATALOGO **************"

INSERT INTO FILEUN.TB_CATALOGO (ID,DOCUMENTO,DESCRIPCION,AGRUPACION,ORIGEN,REUTILIZABLE,FISCALIZABLE,CUSTODIA,COMUN_MULTIPRODUCTO,COMUN_MANCOMUNADO,DETALLE_FISCALIZACION,DETALLE_DOCUMENTO,VALORADO,CALCULO_VIGENCIA) values ('465','0486','Voucher de cancelación de cuenta','OTR','1','NO','NO','NO','NO','SI',null,null,'NO','1');
COMMIT;

PROMPT "**************FIN SCRIPT CARGA TB_CATALOGO **************"
PROMPT "**************FIN SCRIPT**************"

SPOOL OFF;