
  CREATE TABLE "APDIGB"."oauth_access_token"
  (
    "token_id" 				VARCHAR2 (256) ,
    "token" 				BLOB ,
    "authentication_id" 	VARCHAR2 (256) PRIMARY KEY,
    "user_name"         	VARCHAR2 (256) ,
    "client_id"         	VARCHAR2 (256) ,
    "authentication" 		BLOB ,
    "refresh_token" 		VARCHAR2 (256)
  ) ;