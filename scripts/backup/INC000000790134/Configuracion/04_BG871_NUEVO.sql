 /**** PLANTILLA BG871 -  *****/
DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_1 number(1);
 
     
BEGIN
 
 idxCliente:=0;
  v_numpagina_1:=1;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato = 'BG871_NUEVO' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   


UPDATE TBG004_ITEM SET NU_COOR_Y = 667
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A07','SOL0|A08');

UPDATE TBG004_ITEM SET NU_COOR_Y = 650
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A09','SOL0|A14');
   
UPDATE TBG004_ITEM SET NU_COOR_Y = 633
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A16','SOL0|A17');   
   
UPDATE TBG004_ITEM SET NU_COOR_Y = 589
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A18','SOL0|A19');

UPDATE TBG004_ITEM SET NU_COOR_Y = 571
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A20','SOL0|A21');

UPDATE TBG004_ITEM SET NU_COOR_Y = 554
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A22','SOL0|A23');
   
UPDATE TBG004_ITEM SET NU_COOR_Y = 538
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A24','SOL0|A25');   
   
UPDATE TBG004_ITEM SET NU_COOR_Y = 520
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A26','SOL0|A29');  

UPDATE TBG004_ITEM SET NU_COOR_Y = 458
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A39','SOL0|A40');

UPDATE TBG004_ITEM SET NU_COOR_Y = 303
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A56');

UPDATE TBG004_ITEM SET NU_COOR_Y = 273
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A57');

UPDATE TBG004_ITEM SET NU_COOR_Y = 247
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A58');

UPDATE TBG004_ITEM SET NU_COOR_Y = 218
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A59');

UPDATE TBG004_ITEM SET NU_COOR_Y = 251
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('SOL0|A61');

DELETE FROM TBG004_ITEM
WHERE CD_SECCION = v_cd_seccion  AND   NB_NOMBRE_VAR IN ('SOL0|A63', 'SOL0|A64');
COMMIT;

END;
