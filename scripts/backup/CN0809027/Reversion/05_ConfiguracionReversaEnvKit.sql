DECLARE

  CURSOR C_TBG010_KIT IS select CD_KIT from "APDIGB"."TBG010_KIT" where CD_CODIGO IN ('010001','010002','010008',
  '010091','010092') AND  LENGTH(CD_CODIGO)<=6;
  
  V_CD_ENV   "APDIGB"."TBG012_ENV_KIT".CD_ENV%TYPE;
  V_CD_KIT   "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
  cont NUMBER;
   
BEGIN
   
    cont:=0;
    
    OPEN C_TBG010_KIT;
      LOOP
         FETCH C_TBG010_KIT  INTO V_CD_KIT;
         EXIT WHEN C_TBG010_KIT%NOTFOUND;         
           
		   --Configuracion CTA.CTE y BXM
		   
		   select CD_ENV into V_CD_ENV from "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE='BXM';

           select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT and CD_ENV=V_CD_ENV;
           
           IF cont>0 THEN 
            
                 DELETE FROM "APDIGB"."TBG012_ENV_KIT" 
                 WHERE CD_ENV=V_CD_ENV AND CD_KIT=V_CD_KIT;
                 commit;
                           
           END IF;

           --Configuracion CTA.CTE y BXI
           
           select CD_ENV into V_CD_ENV from "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE='BXI';

           select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT and CD_ENV=V_CD_ENV;
           
            IF cont>0 THEN 
            
                 DELETE FROM "APDIGB"."TBG012_ENV_KIT" 
                 WHERE CD_ENV=V_CD_ENV AND CD_KIT=V_CD_KIT;
                 commit;
                           
           END IF;
              
    END LOOP;

   CLOSE C_TBG010_KIT;

   --Configuracion LPDP y PIC
   
   select CD_ENV into V_CD_ENV from "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE='PIC';
   select CD_KIT into V_CD_KIT from "APDIGB"."TBG010_KIT" WHERE CD_CODIGO='99001CLS';
   
   select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT and CD_ENV=V_CD_ENV;
   
  IF cont>0 THEN 
            
         DELETE FROM "APDIGB"."TBG012_ENV_KIT" 
         WHERE CD_ENV=V_CD_ENV AND CD_KIT=V_CD_KIT;
         commit;
                           
   END IF;
   --Configuracion Seguro Renta y BXM
   
   select CD_ENV into V_CD_ENV from "APDIGB"."TBG009_ENV" WHERE NB_NOMBRE='BXM';
   select CD_KIT into V_CD_KIT from "APDIGB"."TBG010_KIT" WHERE CD_CODIGO='400822';
   
   select COUNT(CD_KIT) into cont from "APDIGB"."TBG012_ENV_KIT" WHERE CD_KIT=V_CD_KIT and CD_ENV=V_CD_ENV;
   
   IF cont>0 THEN 
            
         DELETE FROM "APDIGB"."TBG012_ENV_KIT" 
         WHERE CD_ENV=V_CD_ENV AND CD_KIT=V_CD_KIT;
         commit;
                           
   END IF;
   
END;
/*
SELECT * FROM "APDIGB"."TBG012_ENV_KIT"
WHERE FH_CREACION IS NOT NULL
ORDER BY FH_CREACION DESC
*/
