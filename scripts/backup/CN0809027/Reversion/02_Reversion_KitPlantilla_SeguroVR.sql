/* RELACIONA KITS CON PLANTILLAS  */

DECLARE

   V_KIT "APDIGB"."TBG010_KIT".CD_KIT%TYPE;
   V_CD_PLANTILLA "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
     
BEGIN
   
   select  CD_KIT into V_KIT from "APDIGB"."TBG010_KIT" WHERE NB_NOMBRE_KIT like '%400822%'; 
  
   select  CD_PLANTILLA into V_CD_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%IC961%'  ; 
   
   delete from "APDIGB"."TBG011_KIT_PLANTILLA" where CD_KIT=V_KIT  and   CD_PLANTILLA=V_CD_PLANTILLA;
   
   
  
   
   commit;
  
END;

/* RELACIONA KITS CON PLANTILLAS  */