
DECLARE
  cont NUMBER;
BEGIN

/********* Creacion KIT LPDP *********/
  SELECT COUNT(CD_KIT)
  INTO cont
  FROM "APDIGB"."TBG010_KIT"
  WHERE cd_codigo LIKE '%400822%';
  
  IF cont=0 THEN
    INSERT
    INTO "APDIGB"."TBG010_KIT"
      (
        CD_KIT,
        CD_CODIGO,
        NB_CORREO_ASUNTO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        NB_NOMBRE_KIT,
        ST_ENV_CORREO,
        ST_FILEUNICO,
        FL_VAL_HUELLA,
        NB_CORREO_ENGINE,
        ST_SIGNBOX,
        ST_HUELLA
      )
      VALUES
      (
        (SELECT ( (TO_NUMBER(SUBSTR(MAX(CD_KIT),0,6)) + 1)) AS CD_KIT
          FROM "APDIGB"."TBG010_KIT"
        )
        ,
        '400822',
        ' �<CFG0P05> ya est�s protegido con tu Seguro Vida Renta!',
        NULL,
        NULL,
        NULL,
        NULL,
        'KIT 400822 - SEGURO VIDA RENTA',
        'A',
        'A',
        'A',
        'STRTPL',
        'A',
        'A'
      );
  ELSE
    UPDATE "APDIGB"."TBG010_KIT"
    SET NB_CORREO_ASUNTO=' �<CFG0P05> ya est�s protegido con tu Seguro Vida Renta!',
      CD_USU_CREA       =NULL,
      CD_USU_MODI       =NULL,
      FH_CREACION       =NULL,
      FH_MODIFICACION   =NULL,
      NB_NOMBRE_KIT     ='KIT 400822 - SEGURO VIDA RENTA',
      ST_ENV_CORREO     ='A',
      ST_FILEUNICO      ='A',
      FL_VAL_HUELLA     ='A',
      NB_CORREO_ENGINE  ='STRTPL',
      ST_SIGNBOX        ='A',
      ST_HUELLA         ='A'
    WHERE cd_codigo LIKE  '%400822%';
   
  END IF;
 /********* Creacion KIT LPDP *********/ 
  
  
/********* Creacion de plantillas para SOLICITUD SEGURI VIDA RENTA - IC961 *********/
      
  SELECT COUNT(CD_PLANTILLA)
  INTO cont
  FROM "APDIGB"."TBG002_PLANTILLA"
  WHERE NB_NOMBRE_FORMATO LIKE '%IC961%';
  
  
  IF cont=0 THEN

    INSERT
    INTO "APDIGB"."TBG002_PLANTILLA"
      (
        CD_PLANTILLA,
        LB_PDF_PLANTILLA,
        ST_ACTIVO,
        ST_CONDICION,
        ST_FILEUNICO,
        ST_ENV_CORREO,
        ST_TIPO_CONSTR,
        ST_SIGNED,
        NB_NOMBRE_FORMATO,
        NB_TIPO_FILEUNICO,
        NB_NOMBRE_CORREO,
        CD_USU_CREA,
        CD_USU_MODI,
        FH_CREACION,
        FH_MODIFICACION,
        CH_TIPO_PLANTILLA,
        NB_DESCRIPCION
      )
      VALUES
      (
        (SELECT MAX(CD_PLANTILLA)+1 FROM "APDIGB"."TBG002_PLANTILLA"
        )
        ,
        NULL,
        'A',
        'F',
        'A',
        'A',
        'P',
        'A',
        'IC961',
        '0500',
        'IC961 - SOLICITUD Y CONDICIONADO PARTICULAR-SEGURO VIDA RENTA BBVA',
        NULL,
        NULL,
        NULL,
        '05/06/17',
        'G',
        'IC961 - SEGURO VIDA RENTA'
      );
  
  ELSE
    UPDATE "APDIGB"."TBG002_PLANTILLA"
    SET ST_ACTIVO      ='A',
      ST_CONDICION     ='F',
      ST_FILEUNICO     ='A',
      ST_ENV_CORREO    ='A',
      ST_TIPO_CONSTR   ='P',
      ST_SIGNED        ='A',
      NB_NOMBRE_FORMATO='IC961',
      NB_TIPO_FILEUNICO='0500',
      NB_NOMBRE_CORREO ='IC961 - SOLICITUD Y CONDICIONADO PARTICULAR-SEGURO VIDA RENTA BBVA',
      CD_USU_CREA      =NULL,
      CD_USU_MODI      =NULL,
      FH_CREACION      =NULL,
      FH_MODIFICACION  ='05/06/17',
      CH_TIPO_PLANTILLA='G',
      NB_DESCRIPCION   ='IC961 - SEGURO VIDA RENTA' 
      WHERE NB_NOMBRE_FORMATO LIKE '%IC961%';
  END IF;
  
  /********* Creacion de plantillas para SOLICITUD SEGURI VIDA RENTA - IC961 *********/
  
 
  COMMIT;
  
  
END;