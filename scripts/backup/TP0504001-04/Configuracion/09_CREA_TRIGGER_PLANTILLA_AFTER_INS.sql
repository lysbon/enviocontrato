CREATE OR REPLACE TRIGGER APDIGB.TBG002_PLANTILLA_AFTER_INS
AFTER INSERT 
    ON APDIGB.TBG002_PLANTILLA
    FOR EACH ROW
BEGIN
    INSERT INTO APDIGB.TBG002_PLANTILLA_HIST
    (
          CD_PLANTILLA, 
          FH_INICIO,
          FH_FIN,
          LB_PDF_PLANTILLA, 
          ST_ACTIVO , 
          ST_CONDICION, 
          ST_FILEUNICO, 
          ST_ENV_CORREO, 
          ST_TIPO_CONSTR, 
          ST_SIGNED, 
          NB_NOMBRE_FORMATO, 
          NB_TIPO_FILEUNICO, 
          NB_NOMBRE_CORREO, 
          CD_USU_CREA, 
          CD_USU_MODI, 
          FH_CREACION, 
          FH_MODIFICACION, 
          CH_TIPO_PLANTILLA, 
          NB_DESCRIPCION
    )
    VALUES
    (
          :new.CD_PLANTILLA , 
          SYSDATE,
          NULL,
          :new.LB_PDF_PLANTILLA, 
          :new.ST_ACTIVO , 
          :new.ST_CONDICION , 
          :new.ST_FILEUNICO , 
          :new.ST_ENV_CORREO , 
          :new.ST_TIPO_CONSTR , 
          :new.ST_SIGNED , 
          :new.NB_NOMBRE_FORMATO, 
          :new.NB_TIPO_FILEUNICO, 
          :new.NB_NOMBRE_CORREO, 
          :new.CD_USU_CREA, 
          :new.CD_USU_MODI, 
          :new.FH_CREACION, 
          :new.FH_MODIFICACION, 
          :new.CH_TIPO_PLANTILLA , 
          :new.NB_DESCRIPCION
    );
END;