CREATE OR REPLACE TRIGGER APDIGB.TBG011_KIT_PLANTILLA_AFTER_DEL
AFTER DELETE 
    ON APDIGB.TBG011_KIT_PLANTILLA
    FOR EACH ROW
BEGIN

    UPDATE APDIGB.TBG011_KIT_PLANTILLA_HIST
    SET    FH_FIN=SYSDATE-1 / (24 * 60 * 60)
    WHERE  CD_KIT=:old.CD_KIT AND CD_PLANTILLA=:old.CD_PLANTILLA AND FH_FIN IS NULL;
    
END;