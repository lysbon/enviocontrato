CREATE OR REPLACE TRIGGER APDIGB.TBG011_KIT_PLANTILLA_AFTER_INS
AFTER INSERT
    ON APDIGB.TBG011_KIT_PLANTILLA
    FOR EACH ROW
BEGIN
    INSERT INTO APDIGB.TBG011_KIT_PLANTILLA_HIST
    (
          CD_KIT,
          CD_PLANTILLA, 
          FH_INICIO,
          FH_FIN,
          CD_USU_CREA,
          CD_USU_MODI,
          FH_CREACION,
          FH_MODIFICACION
    )
    VALUES
    (
          :new.CD_KIT , 
          :new.CD_PLANTILLA,
          SYSDATE,
          NULL,
          :new.CD_USU_CREA, 
          :new.CD_USU_MODI,
          :new.FH_CREACION,
          :new.FH_MODIFICACION
    );
END;