/********* Agregamos la configuracion para eliminar seccion y  actualizar la seccion del BG151 ******************/
DECLARE

    V_PLANTILLA "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
    V_SECCION "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
   
BEGIN

 /* Una vez terminada la eliminacion de los items de la maxima seccion de la plantilla  BG151 se
    actualiza el dato de la pagina de la mayor seccion restante 
 */

  select CD_PLANTILLA into V_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG151%' or nb_nombre_correo like '%BG151%';
  
  select MAX(CD_SECCION) into V_SECCION from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=V_PLANTILLA;
  
  DELETE FROM "APDIGB".tbg004_item where cd_seccion=V_SECCION;
  
  DELETE FROM "APDIGB".TBG003_SECCION where cd_plantilla=V_PLANTILLA and cd_seccion=V_SECCION;
  
  select MAX(CD_SECCION) into V_SECCION from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=V_PLANTILLA;
  
  UPDATE APDIGB.tbg003_seccion SET NU_PAGINA='4' where cd_plantilla=V_PLANTILLA and cd_seccion=V_SECCION;

  COMMIT;
  
END;
/********* Agregamos la configuracion para eliminar seccion y  actualizar la seccion del BG151 ******************/