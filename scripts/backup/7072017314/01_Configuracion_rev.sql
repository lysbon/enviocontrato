/********* Reversa de la configuracion para eliminar seccion y  actualizar la seccion del BG151 ******************/

DECLARE

    V_PLANTILLA "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
    V_SECCION "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
    V_SECCION_ANT "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
   
BEGIN


 select CD_PLANTILLA into V_PLANTILLA from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG151%' or nb_nombre_correo like '%BG151%';
 
 select max(CD_SECCION) into V_SECCION from "APDIGB"."TBG003_SECCION" where CD_PLANTILLA=V_PLANTILLA ;

 UPDATE "APDIGB"."TBG003_SECCION" SET NU_PAGINA='3' where cd_plantilla=V_PLANTILLA and cd_seccion=V_SECCION;
 
 commit;

END;

/********* Reversa de la configuracion para eliminar seccion y  actualizar la seccion del BG151 ******************/