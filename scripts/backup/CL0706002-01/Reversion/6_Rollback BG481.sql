/**** PLANTILLA BG481 - REVERSA *****/
DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_4 number(1);
 
     
BEGIN
 
 idxCliente:=0;
  v_numpagina_4:=4;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG481%' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_4; 
   

UPDATE TBG004_ITEM SET NU_COOR_Y = 340  
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('barcode');

UPDATE TBG004_ITEM SET NU_COOR_Y = 754
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G04');

UPDATE TBG004_ITEM SET NU_COOR_Y = 622
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G03');

UPDATE TBG004_ITEM SET  NU_COOR_Y = 622
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G05');

UPDATE TBG004_ITEM SET  NU_COOR_Y = 633  
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G01');

UPDATE TBG004_ITEM SET NU_COOR_Y = 633
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G02');

COMMIT;

END;