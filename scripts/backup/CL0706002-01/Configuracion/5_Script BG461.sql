 /**** PLANTILLA BG461 -  *****/
DECLARE
  --"APDIGB".
  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_6 number(1);
 
     
BEGIN
 
 idxCliente:=0;
  v_numpagina_6:=6;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG461%' ; 

   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_6; 
   

UPDATE TBG004_ITEM SET NU_COOR_Y = 536  
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G03');

UPDATE TBG004_ITEM SET NU_COOR_Y = 546
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G01');

UPDATE TBG004_ITEM SET NU_COOR_Y = 536
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G05');

UPDATE TBG004_ITEM SET   NU_COOR_Y = 546
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G02');

COMMIT;

END;