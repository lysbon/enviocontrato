 /**** PLANTILLA BG111 -  *****/
DECLARE
  --"APDIGB".
  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  v_numpagina_4 number(1);
 
     
BEGIN
 
 idxCliente:=0;
  v_numpagina_4:=4;
   
  
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG111%' ; 

   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_4; 
   

UPDATE TBG004_ITEM SET NU_COOR_X = 169, NU_COOR_Y = 571
WHERE CD_SECCION = v_cd_seccion
and NB_NOMBRE_VAR in ('CTR0|G04');


COMMIT;

END;