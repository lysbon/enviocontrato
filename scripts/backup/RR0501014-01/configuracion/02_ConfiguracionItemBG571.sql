/**** PLANTILLA BG561 - DESAFILIZACION MULTIPLICA TU INTERES *****/
DECLARE

   v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
   v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
   idxCliente number(1);
   v_numpagina_1 number(1);
   
   cont number;
     
BEGIN
   
   idxCliente:=0;
   v_numpagina_1:=1;
   
   select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG571%' or nb_nombre_correo like '%BG571%' ; 
    
   select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
   delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
   
   
	
	        Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','180','722','BNF0|A01',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
	
	
	
	
	
	
	       Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','180','706','BNF0|A02',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

	
	
	
  

		Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
		values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','180','692','BNF0|G01',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null); 
		    
	 
                                        
  
   
   	
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','82','412','BNF0|A03',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
   
   
                                       
    
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','116','412','BNF0|A04',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
   
    					   
                                        
   
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','412','BNF0|A05',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
  						   
                                       
   
	 
		    Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
            values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','224','BNF0|A06',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
   
   				   
	
	
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','209','BNF0|A07',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null); 
   
   				   
                                       
   
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','195','BNF0|A08',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
   
  								   
	                                   
  
	 
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','230','195','BNF0|A09',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
   
   								   
									   
                              
   
	
			Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','200','181','BNF0|G01',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);

   
   
                                       
                                       
  
	
		Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                      CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
		values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','100','400','huella',null,'null','0','I','130','80','104',idxCliente,
		null,null,null,null,null);
   
   
  
   
	
		Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
			CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
		values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','80','140','barcode',null,null,'0','B','171','171','25',idxCliente,
			null,null,null,null,null);   
   
    
                                       
   COMMIT;
    
END;
/**** PLANTILLA BG561 - AFILIZACION MULTIPLICA TU INTERES *****/