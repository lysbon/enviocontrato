/**** PLANTILLA BG561 - AFILIZACION MULTIPLICA TU INTERES *****/
DECLARE

  v_cd_plantilla "APDIGB"."TBG002_PLANTILLA".CD_PLANTILLA%TYPE;
  v_cd_seccion "APDIGB"."TBG003_SECCION".CD_SECCION%TYPE;
      
  idxCliente number(1);
  numpagina_1 number(1);
     
  v_numpagina_1 number(1);
  v_numpagina_2 number(1); 
  
  cont number;
     
BEGIN
 
   idxCliente:=0;
   numpagina_1:=1;
   
   v_numpagina_1:=1;
   v_numpagina_2:=2;
   
    select  CD_PLANTILLA into v_cd_plantilla from "APDIGB"."TBG002_PLANTILLA" where  nb_nombre_formato like '%BG561%' or nb_nombre_correo like '%BG561%' ; 
    
    select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_1; 
   
    delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;
	
  
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','183','709','BNF0|G01',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
											   
								   
			
				 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','183','694','BNF0|G02',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
								   
												   
												   
			 

					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','183','679','BNF0|G03',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
			
		
											   

			
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','487','565','BNF0|A01',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
            	        
			
											   
			
											   
				   Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','93','554','BNF0|A02',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
			
			
											   
	
												   
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','441','BNF0|A03',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
										   
												   
											   
			
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','418','BNF0|A04',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
			
 									   
													   
											   
			
			
			       Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			        values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','394','BNF0|A05',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
								   
											   
											   
		
				  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','369','BNF0|A06',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
													   
			 					   
													   
											   
	

				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','346','BNF0|A07',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
										   
	

 			    Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','153','321','BNF0|A08',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
		

			
											   
		
											   
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','441','BNF0|G04',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
								   
												   
												   

	
			  Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			   values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','418','BNF0|A09',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
			
		
												   
											   
		

				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','394','BNF0|A10',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
									   

			
		
											   
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','369','BNF0|A11',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
			
			
													   
													   
													   
		
			
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','346','BNF0|A12',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
								   
											   
	
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','287','321','BNF0|A13',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
			
			
		
			
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','415','382','BNF0|A14',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
										   
												   
												   

		
			
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','375','292','BNF0|G05',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
											   
										   
											   
			
			
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','342','232','BNF0|A15',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
									   
			
			
        			
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
													   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','423','232','BNF0|G02',null,'Arial10','0','T',null,null,null,
													   idxCliente,null,null,null,null,null);
													   
													   
													   
													   
			
			
				 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','193','184','BNF0|G04',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
										   
												   

		

											   
				 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
												   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','490','88','BNF0|G05',null,'Arial10','0','T',null,null,null,
												   idxCliente,null,null,null,null,null);
												   
			
      /****************seccion 2***************/
			
			select  CD_SECCION into v_cd_seccion from "APDIGB"."TBG003_SECCION" where  cd_plantilla=v_cd_plantilla  and nu_pagina=v_numpagina_2; 
			 
			delete from "APDIGB"."TBG004_ITEM" where CD_SECCION=v_cd_seccion;

			 
 	
		   
			 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
			 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','50','593','BNF0|G01',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
											   
										   
	 

				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','50','533','BNF0|G01',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);

								   
   
   
        	

				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
                                       CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','82','495','BNF0|A16',null,'Arial10','0','T',null,null,null,
                                       idxCliente,null,null,null,null,null);
            
     
									   
									   
									   
			
 									   
				 Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','117','495','BNF0|A17',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
										   
							   

									   
     
                                       
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				 values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','208','495','BNF0|A18',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
			
			
			
										   
								   
	      
			                           
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','170','309','BNF0|A19',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
										   
						   
										   
			
         
			 
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','170','294','BNF0|A20',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
						
									
                                       
        
                                       
					Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
											   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
					values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','170','279','BNF0|A21',null,'Arial10','0','T',null,null,null,
											   idxCliente,null,null,null,null,null);
											   
											   
											   
									   
		
                                       
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','184','279','BNF0|A22',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
								   
			

			
		
	                                   
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										   CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','170','265','BNF0|G03',null,'Arial10','0','T',null,null,null,
										   idxCliente,null,null,null,null,null);
	
									   
			
									   
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
										  CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','100','480','huella',null,'null','0','I','130','80','104',idxCliente,
				null,null,null,null,null);
				
			
		 
		 
		   
          
				Insert into "APDIGB"."TBG004_ITEM" (CD_ITEM,CD_SECCION,ST_ACTIVO,NU_COOR_X,NU_COOR_Y,NB_NOMBRE_VAR,NB_VALOR_VAR,NB_FUENTE,NU_ALINEAR,
				CH_TIPO,NU_ANCHO,NU_SC_X,NU_SC_Y,NU_IDX_CLIENTE,CD_USU_CREA,CD_USU_MODI,FH_CREACION,FH_MODIFICACION,NB_MASK) 
				values ((SELECT MAX(CD_ITEM)+1 FROM "APDIGB"."TBG004_ITEM"),v_cd_seccion,'A','84','187','barcode',null,null,'0','B','171','171','25',idxCliente,
				null,null,null,null,null);    								   
				
			
	                                   
    COMMIT;
    
END;
/**** PLANTILLA BG561 - AFILIZACION MULTIPLICA TU INTERES *****/