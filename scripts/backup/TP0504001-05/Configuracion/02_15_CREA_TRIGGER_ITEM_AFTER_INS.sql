CREATE OR REPLACE TRIGGER APDIGB.TBG004_ITEM_AFTER_INS
AFTER INSERT 
    ON APDIGB.TBG004_ITEM
    FOR EACH ROW
BEGIN

    INSERT INTO APDIGB.TBG004_ITEM_HIST
    (
          CD_ITEM , 
          FH_INICIO,
          FH_FIN,
          CD_SECCION, 
          ST_ACTIVO, 
          NU_COOR_X, 
          NU_COOR_Y, 
          NB_NOMBRE_VAR, 
          NB_VALOR_VAR, 
          NB_FUENTE, 
          NU_ALINEAR, 
          CH_TIPO, 
          NU_ANCHO, 
          NU_SC_X, 
          NU_SC_Y, 
          NU_IDX_CLIENTE, 
          CD_USU_CREA, 
          CD_USU_MODI, 
          FH_CREACION, 
          FH_MODIFICACION, 
          NB_MASK
    )
    VALUES
    (
          :new.CD_ITEM , 
          SYSDATE,
          NULL,
          :new.CD_SECCION, 
          :new.ST_ACTIVO, 
          :new.NU_COOR_X, 
          :new.NU_COOR_Y, 
          :new.NB_NOMBRE_VAR, 
          :new.NB_VALOR_VAR, 
          :new.NB_FUENTE, 
          :new.NU_ALINEAR, 
          :new.CH_TIPO, 
          :new.NU_ANCHO, 
          :new.NU_SC_X, 
          :new.NU_SC_Y, 
          :new.NU_IDX_CLIENTE, 
          :new.CD_USU_CREA, 
          :new.CD_USU_MODI, 
          :new.FH_CREACION, 
          :new.FH_MODIFICACION, 
          :new.NB_MASK
    );

END;