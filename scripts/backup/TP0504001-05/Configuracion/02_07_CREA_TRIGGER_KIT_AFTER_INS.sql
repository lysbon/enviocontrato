CREATE OR REPLACE TRIGGER APDIGB.TBG010_KIT_AFTER_INS
AFTER INSERT 
    ON APDIGB.TBG010_KIT
    FOR EACH ROW
BEGIN
    INSERT INTO TBG010_KIT_HIST
    (
      CD_KIT, 
      FH_INICIO,
      FH_FIN,
      CD_CODIGO, 
      NB_CORREO_ASUNTO, 
      LB_CORREO_PLANTILLA, 
      CD_USU_CREA, 
      CD_USU_MODI, 
      FH_CREACION, 
      FH_MODIFICACION, 
      ST_ENV_CORREO, 
      ST_FILEUNICO, 
      FL_VAL_HUELLA, 
      NB_CORREO_ENGINE, 
      NB_NOMBRE_KIT, 
      ST_SIGNBOX, 
      ST_HUELLA
    )
    VALUES
    (
      :new.CD_KIT, 
      SYSDATE,
      NULL,
      :new.CD_CODIGO, 
      :new.NB_CORREO_ASUNTO, 
      :new.LB_CORREO_PLANTILLA, 
      :new.CD_USU_CREA, 
      :new.CD_USU_MODI, 
      :new.FH_CREACION, 
      :new.FH_MODIFICACION, 
      :new.ST_ENV_CORREO, 
      :new.ST_FILEUNICO, 
      :new.FL_VAL_HUELLA, 
      :new.NB_CORREO_ENGINE, 
      :new.NB_NOMBRE_KIT, 
      :new.ST_SIGNBOX, 
      :new.ST_HUELLA
    );
END;