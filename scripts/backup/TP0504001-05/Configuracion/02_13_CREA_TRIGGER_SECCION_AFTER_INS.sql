CREATE OR REPLACE TRIGGER APDIGB.TBG003_SECCION_AFTER_INS
AFTER INSERT 
    ON APDIGB.TBG003_SECCION
    FOR EACH ROW
BEGIN

    INSERT INTO APDIGB.TBG003_SECCION_HIST
    (
          CD_SECCION , 
          FH_INICIO,
          FH_FIN,
          CD_PLANTILLA, 
          NU_PAGINA, 
          CD_USU_CREA, 
          CD_USU_MODI, 
          FH_CREACION, 
          FH_MODIFICACION
    )
    VALUES
    (
          :new.CD_SECCION , 
          SYSDATE,
          NULL,
          :new.CD_PLANTILLA, 
          :new.NU_PAGINA, 
          :new.CD_USU_CREA, 
          :new.CD_USU_MODI, 
          :new.FH_CREACION, 
          :new.FH_MODIFICACION
    );

END;